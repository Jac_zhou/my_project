/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50547
Source Host           : localhost:3306
Source Database       : duobao

Target Server Type    : MYSQL
Target Server Version : 50547
File Encoding         : 65001

Date: 2016-08-29 15:13:26
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `go_pay`
-- ----------------------------
DROP TABLE IF EXISTS `go_pay`;
CREATE TABLE `go_pay` (
  `pay_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `pay_name` char(20) NOT NULL,
  `pay_class` char(20) NOT NULL,
  `pay_type` tinyint(3) NOT NULL,
  `pay_thumb` varchar(255) DEFAULT NULL,
  `pay_des` text,
  `pay_start` tinyint(4) NOT NULL,
  `pay_key` text,
  `wap` tinyint(1) NOT NULL DEFAULT '1' COMMENT '是否在H5开启本支付',
  `app` tinyint(1) NOT NULL DEFAULT '1' COMMENT '是否在APP开启本支付',
  `web` tinyint(1) NOT NULL DEFAULT '1' COMMENT '是否在网站开启本支付',
  PRIMARY KEY (`pay_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of go_pay
-- ----------------------------
INSERT INTO `go_pay` VALUES ('1', '财付通', 'tenpay', '1', 'photo/cft.gif', '腾讯财付通	', '0', 'a:2:{s:2:\"id\";a:2:{s:4:\"name\";s:19:\"财付通商户号:\";s:3:\"val\";s:0:\"\";}s:3:\"key\";a:2:{s:4:\"name\";s:16:\"财付通密钥:\";s:3:\"val\";s:0:\"\";}}', '1', '1', '1');
INSERT INTO `go_pay` VALUES ('2', '支付宝', 'alipay', '1', 'photo/20160524/14894484083512.png', '支付宝支付', '0', 'a:3:{s:2:\"id\";a:2:{s:4:\"name\";s:19:\"支付宝商户号:\";s:3:\"val\";s:16:\"2088901039824985\";}s:3:\"key\";a:2:{s:4:\"name\";s:16:\"支付宝密钥:\";s:3:\"val\";s:32:\"vudx4el82wvdvmteu84320h1231vxo5n\";}s:4:\"user\";a:2:{s:4:\"name\";s:16:\"支付宝账号:\";s:3:\"val\";s:16:\"329174302@qq.com\";}}', '1', '1', '1');
INSERT INTO `go_pay` VALUES ('3', '易宝支付', 'yeepay', '1', 'photo/20151127/60682151594709.png', '易宝支付', '0', 'a:2:{s:2:\"id\";a:2:{s:4:\"name\";s:16:\"易宝商户号:\";s:3:\"val\";s:0:\"\";}s:3:\"key\";a:2:{s:4:\"name\";s:13:\"易宝密钥:\";s:3:\"val\";s:0:\"\";}}', '1', '1', '1');
INSERT INTO `go_pay` VALUES ('4', '微信支付', 'wxpay', '1', 'photo/20160524/51192066084869.jpg', '微信支付 ', '1', 'a:2:{s:2:\"id\";a:2:{s:4:\"name\";s:9:\"商户号\";s:3:\"val\";s:10:\"1356569802\";}s:3:\"key\";a:2:{s:4:\"name\";s:6:\"密匙\";s:3:\"val\";s:32:\"shenzhenshiYiyuancaishen1973weng\";}}', '1', '0', '1');
INSERT INTO `go_pay` VALUES ('5', '爱贝支付宝（支付金额不少于2元）', 'iapppay', '0', 'photo/20160812/93955272989126.png', '爱贝支付', '1', 'a:2:{s:2:\"id\";a:2:{s:4:\"name\";s:9:\"商户号\";s:3:\"val\";s:17:\"2753019851@qq.com\";}s:3:\"key\";a:2:{s:4:\"name\";s:6:\"密匙\";s:3:\"val\";s:216:\"MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQC/qaY/WWjOaHBwTjVeib/ntOJrK6XAe3NPC2exRKzlffqRz8DJwyWdUNsqE35Ua2R8wc7abdIwGHsLB62FIhXUOvRS8YtN8K0awzHavvGH14y5ylYCO6Gw8UtjFlf3erUZGYm6xTq7/EtkE6JA1Njzg4XnEYoPI9jNRbUf83EF3QIDAQAB\";}}', '1', '1', '0');
INSERT INTO `go_pay` VALUES ('6', '京东网关', 'chinabank', '1', 'photo/chinabank.gif', '京东网关	', '1', 'a:2:{s:2:\"id\";a:2:{s:4:\"name\";s:9:\"商户号\";s:3:\"val\";s:12:\"110207617001\";}s:3:\"key\";a:2:{s:4:\"name\";s:6:\"密匙\";s:3:\"val\";s:32:\"MWl4djFbygzYMOWz0aduqvktQbmwvU7M\";}}', '0', '0', '1');
