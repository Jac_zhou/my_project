/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50505
Source Host           : localhost:3306
Source Database       : duobao

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2016-05-06 13:59:36
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `go_active`
-- ----------------------------
DROP TABLE IF EXISTS `go_active`;
CREATE TABLE `go_active` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL DEFAULT '0' COMMENT '用户名',
  `type` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1为现金券，2位红包',
  `cash_value` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '红包或者券的值',
  `is_active` tinyint(4) NOT NULL DEFAULT '0' COMMENT '激活，1为激活的',
  `r_time` int(11) NOT NULL DEFAULT '0',
  `mobile` varchar(20) NOT NULL DEFAULT '0' COMMENT '手机号码',
  `is_use` tinyint(4) NOT NULL DEFAULT '0' COMMENT '1为使用完了',
  `money` decimal(8,2) NOT NULL DEFAULT '0.00' COMMENT '充值的金额',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of go_active
-- ----------------------------

-- ----------------------------
-- Table structure for `go_admin`
-- ----------------------------
DROP TABLE IF EXISTS `go_admin`;
CREATE TABLE `go_admin` (
  `uid` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `mid` tinyint(3) unsigned NOT NULL,
  `username` char(15) NOT NULL,
  `userpass` char(32) NOT NULL,
  `useremail` varchar(100) DEFAULT NULL,
  `addtime` int(10) unsigned DEFAULT NULL,
  `logintime` int(10) unsigned DEFAULT NULL,
  `loginip` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`uid`),
  KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8 COMMENT='管理员表';

-- ----------------------------
-- Records of go_admin
-- ----------------------------
INSERT INTO `go_admin` VALUES ('1', '1', 'sunsk', '63ee451939ed580ef3c4b6f0109d1fd0', null, null, '1462511217', '218.241.196.226');

-- ----------------------------
-- Table structure for `go_admin_group`
-- ----------------------------
DROP TABLE IF EXISTS `go_admin_group`;
CREATE TABLE `go_admin_group` (
  `mid` int(3) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL COMMENT '组名称',
  `auth` varchar(300) NOT NULL COMMENT '权限',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '状态',
  PRIMARY KEY (`mid`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of go_admin_group
-- ----------------------------
INSERT INTO `go_admin_group` VALUES ('1', '超级管理员', 'setting,content,shop,user,datas,yunapp,applist,activelist,order', '1');
INSERT INTO `go_admin_group` VALUES ('2', '产品管理', 'shop,order', '1');
INSERT INTO `go_admin_group` VALUES ('3', '用户管理', 'user', '1');
INSERT INTO `go_admin_group` VALUES ('4', '数据统计', 'datas', '1');
INSERT INTO `go_admin_group` VALUES ('8', '管理员', 'content,shop,user,datas,applist,activelist,order', '1');
INSERT INTO `go_admin_group` VALUES ('10', '开发部', 'setting,content,shop,user,datas,yunapp,applist,activelist,order', '1');
INSERT INTO `go_admin_group` VALUES ('11', '订单管理', 'order', '1');

-- ----------------------------
-- Table structure for `go_ad_area`
-- ----------------------------
DROP TABLE IF EXISTS `go_ad_area`;
CREATE TABLE `go_ad_area` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL,
  `width` smallint(6) unsigned DEFAULT NULL,
  `height` smallint(6) unsigned DEFAULT NULL,
  `des` varchar(255) DEFAULT NULL,
  `checked` tinyint(1) DEFAULT '0' COMMENT '1表示通过',
  PRIMARY KEY (`id`),
  KEY `checked` (`checked`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COMMENT='广告位';

-- ----------------------------
-- Records of go_ad_area
-- ----------------------------
INSERT INTO `go_ad_area` VALUES ('2', '&lt;div&gt;453453451&lt;/div&gt;', '750', '60', 'sd', '1');
INSERT INTO `go_ad_area` VALUES ('3', '546', '456', '456', '456', '1');
INSERT INTO `go_ad_area` VALUES ('4', '111', '500', '300', '11', '1');

-- ----------------------------
-- Table structure for `go_ad_data`
-- ----------------------------
DROP TABLE IF EXISTS `go_ad_data`;
CREATE TABLE `go_ad_data` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `aid` int(10) unsigned NOT NULL,
  `title` varchar(100) NOT NULL,
  `type` char(10) DEFAULT NULL COMMENT 'code,text,img',
  `content` text,
  `checked` tinyint(1) DEFAULT '0' COMMENT '1表示通过',
  `addtime` int(10) unsigned NOT NULL,
  `endtime` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `type` (`type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='广告';

-- ----------------------------
-- Records of go_ad_data
-- ----------------------------

-- ----------------------------
-- Table structure for `go_appimg`
-- ----------------------------
DROP TABLE IF EXISTS `go_appimg`;
CREATE TABLE `go_appimg` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL COMMENT '图标名称',
  `url` varchar(100) NOT NULL COMMENT '链接地址',
  `img_path` varchar(100) NOT NULL COMMENT '图片的路径',
  `is_show` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否启用，1为启用，0为不启用',
  `order_num` int(11) NOT NULL DEFAULT '1' COMMENT '排序',
  `handel` varchar(100) NOT NULL COMMENT '操作',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of go_appimg
-- ----------------------------
INSERT INTO `go_appimg` VALUES ('17', '分类', '1', 'appimg/20151127/56664714592791.png', '1', '4', 'get_category_list');
INSERT INTO `go_appimg` VALUES ('18', '10元专区', '2', 'appimg/20151127/11848005592801.png', '1', '3', 'get_ten_yuan_list');
INSERT INTO `go_appimg` VALUES ('19', '晒单', '3', 'appimg/20151127/31611899592816.png', '1', '2', 'get_shaidan_list');
INSERT INTO `go_appimg` VALUES ('20', '常见问题', '4', 'appimg/20151127/90491567592825.png', '1', '1', 'get_article_list');

-- ----------------------------
-- Table structure for `go_app_article`
-- ----------------------------
DROP TABLE IF EXISTS `go_app_article`;
CREATE TABLE `go_app_article` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(60) NOT NULL COMMENT '标题',
  `img_path` varchar(100) DEFAULT NULL COMMENT '图片',
  `content` text COMMENT '内容描写',
  `order_num` mediumint(8) unsigned NOT NULL DEFAULT '1' COMMENT '排序',
  `is_show` tinyint(4) NOT NULL DEFAULT '1' COMMENT '是否显示，1为是，0为否',
  `type` tinyint(2) NOT NULL DEFAULT '1',
  `is_new` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否最新',
  `time` int(10) NOT NULL,
  `description` varchar(255) NOT NULL COMMENT '文章摘要',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of go_app_article
-- ----------------------------

-- ----------------------------
-- Table structure for `go_app_cart`
-- ----------------------------
DROP TABLE IF EXISTS `go_app_cart`;
CREATE TABLE `go_app_cart` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `shopid` int(10) DEFAULT NULL COMMENT '商品ID',
  `qishu` smallint(6) DEFAULT NULL,
  `title` varchar(100) DEFAULT NULL COMMENT '商品标题',
  `thumb` varchar(255) DEFAULT NULL COMMENT '图片地址',
  `zongrenshu` int(10) DEFAULT NULL COMMENT '总需人数',
  `canyurenshu` int(10) DEFAULT NULL,
  `shenyurenshu` int(10) DEFAULT NULL COMMENT '剩余人数',
  `yunjiage` decimal(4,2) DEFAULT NULL,
  `uid` int(10) DEFAULT NULL COMMENT '用户ID',
  `cache_id` varchar(255) DEFAULT NULL COMMENT '客户端唯一标识(uid和cache_id必填一个)',
  `gonumber` int(10) DEFAULT NULL COMMENT '购买人次',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of go_app_cart
-- ----------------------------

-- ----------------------------
-- Table structure for `go_article`
-- ----------------------------
DROP TABLE IF EXISTS `go_article`;
CREATE TABLE `go_article` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '文章id',
  `cateid` char(30) NOT NULL COMMENT '文章父ID',
  `author` char(20) DEFAULT NULL,
  `title` char(100) NOT NULL COMMENT '标题',
  `title_style` varchar(100) DEFAULT NULL,
  `thumb` varchar(3) DEFAULT NULL,
  `picarr` text,
  `keywords` varchar(100) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `content` mediumtext COMMENT '内容',
  `hit` int(10) unsigned DEFAULT '0',
  `order` tinyint(3) unsigned DEFAULT NULL,
  `posttime` int(10) unsigned DEFAULT NULL COMMENT '添加时间',
  PRIMARY KEY (`id`),
  KEY `cateid` (`cateid`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of go_article
-- ----------------------------
INSERT INTO `go_article` VALUES ('1', '2', '', '如何神马购', '', '', 'a:2:{i:0;s:33:\"photo/20130902/41484375056924.jpg\";i:1;s:33:\"photo/20130902/26578125056924.jpg\";}', '一元神马购,新手指南', '一元神马购是一个引入众筹理念的新型电子商务平台，手机电脑、数码影音、潮流新品、家居电器，只需1元即有机会获取，凭借技术实力和算法公平，确保100%公平公正、100%正品保障，引领集娱乐、购物于一体的网购潮流。', '<p>	</p><p><br/></p><p><br/></p><p><br/></p><p><br/></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">&nbsp; &nbsp; 一元神马购是一个引入众筹理念的新型电子商务平台，手机电脑、数码影音、潮流新品、家居电器，只需1元即有机会获取，凭借强大的技术实力和<span style=\"font-family: 宋体, SimSun; font-size: 14px;\">公平公正的</span>算法，确保100%公平公正、100%正品保障，<span style=\"font-family: 宋体, SimSun; font-size: 14px;\">一元神马购</span>将引领集娱乐、购物于一体的网购潮流。</span></p><p><br/></p><p><strong>神马购步骤：</strong></p><p><span style=\"font-size: 14px; font-family: 宋体, SimSun;\">&nbsp; &nbsp; 选购下单→一秒注册→快捷支付→等待开奖</span></p><p><br/></p><p><img src=\"http://www.1ysmg.com/statics/uploads/shopimg/20160425/18113148563664.jpg\" title=\"49502101135885副本.jpg\"/></p><p><br/></p><p><br/></p><p><br/></p><p style=\"text-align: left;\"><span style=\"text-align: right;\"></span><span style=\"text-align: right; font-size: 14px; font-family: 宋体, SimSun;\">&nbsp;</span><span style=\"font-family: 宋体, SimSun; font-size: 14px; text-align: right;\">①选择心动商品，点击“立即神马购”即可下单。</span></p><p><br/></p><p><br/></p><p><br/></p><p><br/></p><p><br/></p><p><img src=\"http://yiyuanzhongle.com/statics/uploads/shopimg/20160107/63070106135903.jpg\" title=\"注册.jpg\" width=\"400\" height=\"235\" border=\"0\" hspace=\"2\" vspace=\"2\" style=\"width: 400px; height: 235px; float: right;\"/></p><p><br/></p><p><br/></p><p><br/></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">②使用有效手机号码注册登陆，即可参加一元神马购。</span></p><p><br/></p><p><br/></p><p><br/></p><p><img src=\"http://yiyuanzhongle.com/statics/uploads/shopimg/20160107/96028734135949.png\" style=\"width: 300px; height: 300px; float: left;\" title=\"支付.png\" width=\"300\" height=\"300\" border=\"0\" hspace=\"2\" vspace=\"2\"/></p><p><br/></p><p><br/></p><p><br/></p><p><br/></p><p><br/></p><p style=\"white-space: normal;\"><span style=\"font-size: 14px; font-family: 宋体, SimSun;\">③下单支付，每件商品按价格均分若干等分，花费1元兑换一枚神马币，</span></p><p style=\"white-space: normal;\"><span style=\"font-size: 14px; font-family: 宋体, SimSun;\">支付1枚<span style=\"font-family: 宋体, SimSun; font-size: 14px;\">神马币</span>可获得1个神马号（系统随机分配）。</span></p><p><br/></p><p><br/></p><p><br/></p><p><br/></p><p><br/></p><p><img src=\"http://www.1ysmg.com/statics/uploads/shopimg/20160425/36084982563830.jpg\" title=\"47381642135982.jpg\" style=\"float: right;\"/></p><p><br/><span style=\"font-family: 宋体, SimSun; font-size: 14px;\"></span></p><p><br/></p><p style=\"text-align: left;\"><span style=\"font-family: 宋体, SimSun; font-size: 14px; text-align: right;\">Ps：充值获得神马币，参与更快捷哦！</span></p><p><br/></p><p><br/></p><p><br/></p><p style=\"text-align: right;\"><br/></p><p style=\"text-align: right;\"><span style=\"font-size: 14px; font-family: 宋体, SimSun;\"></span></p><p><img src=\"http://yiyuanzhongle.com/statics/uploads/shopimg/20160107/33220548135998.jpg\" style=\"width: 400px; height: 212px; float: left;\" title=\"中奖.jpg\" width=\"400\" height=\"212\" border=\"0\" hspace=\"2\" vspace=\"2\"/></p><p style=\"text-align: right;\"><br/></p><p><br/></p><p><br/></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">&nbsp;④当所有商品份额认购完毕，系统将自动按程序设定算法计算出幸运中奖用户。</span></p><p><br/></p><p><br/></p><p><br/></p><p><br/></p><p><br/></p><p><br/></p><p><span style=\"font-size: 16px;\"><strong><span style=\"font-family: 宋体, SimSun;\">神马购规则：</span></strong></span></p><p><br/></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">每件商品参考市场价均分成若干“等份”，每份1元，对应1个神马币，支付一个神马币即可获得一个神马号。</span></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">同一件商品可以购买多次，或者一次购买多份。</span></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">当一件商品所有“等份”全部售罄，按既定算法计算出幸运神马号，拥有幸运神马号者即可获得该商品。</span></p><p><br/></p><p><strong><span style=\"font-family: 宋体, SimSun;\">幸运神马号计算方式：</span></strong></p><p><br/></p><p><span style=\"font-size: 14px; font-family: 宋体, SimSun;\">1、取该商品最后购买时间前网站所有商品的最后100条购买时间记录（限时购买商品取截止时间前网站所有商品100条购买时间记录）；</span></p><p><span style=\"font-size: 14px; font-family: 宋体, SimSun;\">2、每个时间记录按时、分、秒、毫秒依次排列取数值并将这100个数值求和（A）；</span></p><p><span style=\"font-size: 14px; font-family: 宋体, SimSun;\">3、为保证公平公正公开，系统还会等待一小段时间，取最近一期中国乐彩彩票“时时彩”的开奖结果（一个五位数值B）；</span></p><p><span style=\"font-size: 14px; font-family: 宋体, SimSun;\">注：最后一个号码分配时间距离中国乐彩彩票“时时彩”最近下一期开奖大于15分钟，默认“时时彩”开奖结果为00000。</span></p><p><span style=\"font-size: 14px; font-family: 宋体, SimSun;\">4、将A与B之和除以该商品总参与人次后取余数，余数加上10000001 即为“幸运神马号”。</span></p><p><span style=\"font-size: 14px; font-family: 宋体, SimSun;\">如果网站未满100条购买记录，则按照【10000001+(揭晓时间总和与时时彩中奖号码求和结果*100/参与人数)的余数】即为“幸运神马号”。</span></p>', '1', '1', '1375862513');
INSERT INTO `go_article` VALUES ('2', '2', '阿染', '如何支付', 'color:#000;', '', 'a:0:{}', '一元神马购,新手指南,如何支付', '', '<p>	</p><p><br/></p><p><br/></p><p><br/></p><p><br/></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">快捷支付，只需一步：</span><br/></p><p style=\"white-space: normal;\"><br/></p><p style=\"white-space: normal; text-align: center;\"><img src=\"http://www.1ysmg.com/statics/uploads/shopimg/20160425/65256722559679.jpg\" title=\"45274669136070.jpg\"/></p><p style=\"white-space: normal;\"><span style=\"font-family: 宋体, SimSun; font-size: 14px;\"><br/></span></p><p style=\"white-space: normal;\"><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">&nbsp; &nbsp; 提交订单后选择支付方式，支付相应金额，确认支付即可。</span></p><p style=\"white-space: normal;\"><br/></p><p style=\"white-space: normal;\"><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">·为让您的神马购更快捷，还可在您的一元神马购个人中心进行充值，充值金额可立即用于消费，亦可用于下次消费。</span></p><p style=\"white-space: normal;\"><br/></p><p style=\"white-space: normal; text-align: right;\"><img src=\"http://www.1ysmg.com/statics/uploads/shopimg/20160425/73573349559899.jpg\" title=\"84004598136104副本.jpg\"/></p><p style=\"white-space: normal; text-align: right;\"><br/></p><p style=\"white-space: normal;\"><strong><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">充值步骤：</span></strong></p><p style=\"white-space: normal;\"><br/><span style=\"font-family: 宋体, SimSun; font-size: 14px;\"></span></p><p style=\"white-space: normal;\"><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">1.进入“我的神马购”，点击“账户充值”按钮；</span></p><p style=\"white-space: normal;\"><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">2.选择一个充值金额，或直接输入金额；</span></p><p style=\"white-space: normal;\"><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">3.可选择支付宝或微信支付进行支付；</span></p><p style=\"white-space: normal;\"><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">4.点击“立即充值”按钮。</span></p><p style=\"white-space: normal;\"><br/></p><p style=\"white-space: normal;\"><br/></p><p style=\"white-space: normal;\"><strong><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">温馨提示：</span></strong></p><p style=\"white-space: normal;\"><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">1.如果微信支付不显示二维码，请关闭页面并返回充值页重选“支付方式”，再次选择微信支付；</span></p><p style=\"white-space: normal;\"><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">2.使用以上任意一种支付方式，金额如没有即时到达“账户余额”，请别担心，联系我们的客服即可。</span></p>', '50', '3', '1375862591');
INSERT INTO `go_article` VALUES ('3', '2', '', '常见问题', '', '', 'a:0:{}', '一元神马购,常见问题', '', '<p>	</p><p><br/></p><p><strong><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">1、怎样参加一元神马购？</span></strong><br/></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">使用有效手机号码注册登陆，就可以参加一元神马购。</span></p><p><br/></p><p><strong><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">参加方式</span></strong><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">：</span></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">1、选择心动商品，以及需要参与的人次，根据系统提示完成支付即可；</span></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">2、充值获得神马币，然后选择喜欢的商品开始神马购，充值之后参加神马购更方便。</span></p><p><br/></p><p><strong><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">2、一元神马购是怎么计算中奖号码的？</span></strong></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">（1）商品所有份额认购完毕后，取该商品最后购买时间前网站所有商品的最后100条购买时间记录；</span></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\"><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">（2）</span>每条时间记录按时、分、秒、毫秒依次排列取数值并将这100个数值求和（A）；</span></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\"><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">（3）</span>为保证公平公正公开，系统还会等待一小段时间，取最近一期中国乐彩彩票“时时彩”的开奖结果（一个五位数值B）；</span></p><p><a href=\"http://baidu.lecai.com/lottery/draw/view/200/2015-12-26?\" target=\"_blank\" title=\"\"><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">中国乐彩彩票“时时彩”详细信息&gt;&gt;</span></a></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">注：若最后一个众乐号码分配时间距离中国乐彩彩票“时时彩”最近下一期开奖大于15分钟，默认“时时彩”开奖结果为00000。&nbsp;</span></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\"><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">（4）</span>将A与B之和除以该商品总参与人次后取余数，余数加上10000001 即为“幸运神马号”。</span></p><p><br/></p><p><strong><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">3、为什么要引入“时时彩”开奖结果？</span></strong></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">引入第三方数据——“时时彩”开奖结果是为了保证幸运神马号计算结果的绝对公平公正，确保幸运神马号的随机性。</span></p><p><br/></p><p><strong><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">4、中奖号码的计算结果可信吗？</span></strong></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">开奖算法引入“时时彩”开奖结果作为外部参数，因此幸运号码肯定是未知的，</span><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">您可以绝对相信计算结果的真实性和可靠性。</span></p><p><br/></p><p><strong><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">5、怎样查看是否中奖？</span></strong></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">中奖信息会第一时间在平台公开，用户可在个人中心查看自己的神马购及中奖纪录，还可在“最新揭晓”栏目查看他人交易、中奖以及晒单记录。如果您获奖，一元神马购官方将会以邮件、短信等方式进行通知。</span></p><p><br/></p><p><strong><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">6、如何领奖？</span></strong></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">在您获奖后会收到一元神马购官方邮件、短信通知，收到通知后，请到个人中心填写真实的收货地址，完善、确认您的个人信息，以便我们为您派发中奖的商品。超过7天未确认收货地址和联系方式者，将视为自动放弃该商品。</span></p><p><br/></p><p><strong><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">7、商品是正品吗？怎么保证？</span></strong></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">一元神马购所有商品均从品牌官方、正规渠道采购，100%正品保障，保证所有商品均为正品行货，可享受厂家所提供的全国联保服务。</span></p><p><br/></p><p><strong><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">8、我收到的商品可以换货或者退货吗？</span></strong></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">非质量问题，不在三包范围内，不给予退换货。</span></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">请尽量亲自签收并当面拆箱验货，如果发现运输途中造成的商品损坏，请不要签收，可以拒签退回。</span></p><p><br/></p><p><strong><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">9、如果一件商品很久都没达到总需人次怎么办？</span></strong></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">若某件商品的所有众乐号码从开始分配之日起90天未分配完毕，一元神马购官方有权取消该件商品的神马购活动，并向用户退还神马币，所退还神马币将在3个工作日内退还至用户账户余额中。</span></p><p><br/></p><p><strong><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">10、参与一元神马购需要注意什么？</span></strong></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">为了确保在您获奖后第一时间收到通知，请务必正确填写真实有效的联系电话和收货地址。</span></p><p><br/></p><p><strong><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">11、网上支付未及时到帐怎么办？</span></strong></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">网上支付未及时到帐可能有以下几个原因造成：</span></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">第一，由于网速或者支付接口等问题，支付数据没有及时传送到支付系统造成的；</span></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">第二，网速过慢，数据传输超时使银行后台支付信息不能成功对接，导致银行交易成功而支付后台显示失败；</span></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">第三，如果使用某些防火墙软件，有时会屏蔽银行接口的弹出窗口。</span></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">如果支付过程问题遇到问题，请与我们联系。</span></p><p><br/></p><p><strong><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">12、什么是神马币？</span></strong></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">神马币是一元神马购平台的代币，用户每充值1元，即可获得1枚神马币；</span></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">1枚神马币可以直接购买1人次的神马号。</span></p><p><br/></p><p><strong><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">13、如何进行神马币充值？</span></strong></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">在注册用户的个人中心，可以找到入口进行充值；</span><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">需要注意的是，充值之后获得的是神马币，可以直接用于参与神马购。</span></p><p><br/></p><p><strong><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">14、神马币是否可以提现？</span></strong></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">很抱歉，神马币无法提现。</span></p><p><br/></p><p><strong><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">15、如何晒单分享？</span></strong></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">在您收到商品后，登陆一元神马购平台，进入“我的神马购”，在“晒单分享”区发布晒单信息，通过审核后，您的晒单会出现在平台“晒单分享”区，与大家分享喜悦。</span></p><p><br/></p><p><strong><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">16、建议反馈</span></strong></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">如果您有任何意见或建议，请与客服QQ（1164099224）与我们联系，谢谢！</span></p><p><br/></p>', '0', '0', '1375862644');
INSERT INTO `go_article` VALUES ('4', '3', '', '算法公平', '', '', 'a:0:{}', '算法公平,一元神马购', '一元神马购乐幸运号码计算公式，加入了“时时彩”开奖结果作为外部随机因子，确保计算结果具有绝对的随机性、可靠性和公正性。', '<p>	</p><p><br/></p><p><br/></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">&nbsp; &nbsp; 一元神马购幸运号码计算公式，加入了“时时彩”开奖结果作为外部随机因子，确保计算结果具有绝对的随机性、可靠性和公正性。</span><br/></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">&nbsp;</span></p><p style=\"text-align:center\"><img src=\"http://www.1ysmg.com/statics/uploads/shopimg/20160425/29105998557316.jpg\" title=\"24675417136275.jpg\"/></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\"></span><br/></p><p><strong><span style=\"font-family: 宋体, SimSun; font-size: 14px;\"><br/></span></strong></p><p><strong><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">运算法则：</span></strong></p><p><br/></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">1、取该商品最后购买时间前网站所有商品的最后100条交易时间记录（限时购买商品取截止时间前网站所有商品100条购买时间记录）；</span></p><p><br/></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">2、每个时间记录按时、分、秒、毫秒依次排列取数值并将这100个数值求和（A）；</span></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">（例如20：15：12：312 则为201512312）</span></p><p><br/></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">3、为保证公平公正公开，系统还会等待一小段时间，取最近一期中国乐彩彩票“时时彩”的开奖结果（一个五位数值B）；&nbsp;</span></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">注：最后一个神马号分配时间距离中国乐彩彩票“时时彩”最近下一期开奖大于15分钟，默认“时时彩”开奖结果为00000。&nbsp;</span></p><p><br/></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">4、将A与B之和除以该商品总参与人次后取余数，余数加上10000001 即为“幸运神马号”。&nbsp;</span></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">如果网站未满100条购买记录，则按照【10000001+(揭晓时间总和与时时彩开奖号码求和结果*100/参与人数)的余数】即为“幸运神马号”。</span><br/></p><p><br/></p><p><strong><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">公式解读：</span></strong></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\"><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">幸运神马号=</span>(100条交易时间总和+时时彩中奖号码)%参与人数+10000001 ，≥100条交易记录</span></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">幸运神马号=</span><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">(100条交易时间总和+时时彩中奖号码)*100%参与人数+10000001 ，＜100条交易记录</span></p><p><br/></p>', '0', '0', '1375862690');
INSERT INTO `go_article` VALUES ('5', '3', '', '正品保障', '', '', 'a:0:{}', '正品保障,一元神马购', '一元神马购严格控制供应渠道，全部商品均从品牌官方以及品牌授权经的销商直接采购供货。如果您认为一元神马购的商品是假货，并能提供国家相关质检机构的证明文件，经确认后，在返还商品金额的同时并提供假一赔三服务保障。', '<p></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">&nbsp; &nbsp; 一元神马购严格控制供应渠道，全部商品均从品牌官方以及品牌授权经的销商直接采购供货。如果您认为一元神马购的商品是假货，并能提供国家相关质检机构的证明文件，经确认后，在返还商品金额的同时并提供假一赔三服务保障。为了保障您的利益，对<span style=\"font-family: 宋体, SimSun; font-size: 14px;\">一元神马购</span>的商品，做如下说明：</span></p><p><br/></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">1、<span style=\"font-family: 宋体, SimSun; font-size: 14px;\">一元神马购</span>对所有商品均保证正品行货，正规渠道发货，所有商品都可以享受生产厂家的全国联保服务，按照国家三包政策，针对所售商品履行保修、换货和退货的义务。</span></p><p><br/></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">2、出现国家三包所规定的功能性故障时，经由生产厂家指定或特约售后服务中心检测确认故障属实，您可以选择换货或者维修；超过15日且在保修期内，您只能在保修期内享受免费维修服务。为了不耽误您使用，缩短故障商品的维修时间，我们建议您直接联系生产厂家售后服务中心进行处理。您也可以直接在商品的保修卡中查找该商品对应的全国各地生产厂家售后服务中心联系处理。</span></p><p><br/></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">3、<span style=\"font-family: 宋体, SimSun; font-size: 14px;\">一元神马购</span>真诚提醒广大幸运用户在您收到中奖商品的时候，请尽量亲自签收并当面拆箱验货，如果有问题(如运输途中的损坏等情况)请不要签收，可以拒签退回。</span></p><p><br/></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">4、在收到商品后发现有质量问题，请您不要私自处理，妥善保留好原包装，第一时间联系客服人员，由<span style=\"font-family: 宋体, SimSun; font-size: 14px;\">一元神马购</span>同发货厂商进行协调，并在48小时内为您解决。</span></p><p><br/></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">5、如对协商处理结果存在异议，请您自行到当地生产厂家售后服务中心进行检测，并开具正规检测报告（对于有些生产厂家售后服务中心无法提供检测报告的，需提供维修检验单据），如果检测报告确认属于质量问题，然后将检测报告、问题商品及完整包装附件，一并返还发货厂商办理退换货手续，产生的费用由相关责任方承担。</span></p><p><br/></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">6、<span style=\"font-family: 宋体, SimSun; font-size: 14px;\">一元神马购</span>上的电子产品及配件因为生产工艺或仓储物流等原因，可能会存在收到或使用过程中出现故障的几率，<span style=\"font-family: 宋体, SimSun; font-size: 14px;\">一元神马购</span>不能保证所有的商品都没有故障，但我们保证所售商品都是全新正品行货，能够承诺正规的售后保障。我们保证商品的正规进货渠道和质量，如果您对收到的商品质量表示怀疑，请提供生产厂家或官方出具的书面鉴定，我们会按照国家法律规定予以处理。但对于任何欺诈性行为，<span style=\"font-family: 宋体, SimSun; font-size: 14px;\">一元神马购</span>将保留依法追究法律责任的权利。</span></p><p><br/></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">&nbsp; &nbsp; 本规则最终解释权由<span style=\"font-family: 宋体, SimSun; font-size: 14px;\">一元神马购</span>所有。</span></p><p><br/></p>', '0', '0', '1375862702');
INSERT INTO `go_article` VALUES ('6', '3', '', '交易保障', '', '', 'a:0:{}', '交易保障,一元神马购', '一元神马购支持支付宝、微信支付等支付方式，严格遵循网络购物的安全准则，充分保证用户在线支付的安全性。支付金额即时到账，可在“我的神马购-账户余额”中查询。', '<p>	</p><p><br/></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">&nbsp; &nbsp; <span style=\"font-family: 宋体, SimSun; font-size: 14px;\">一元神马购</span>支持支付宝、微信支付等支付方式，严格遵循网络购物的安全准则，充分保证用户在线支付的安全性。支付金额即时到账，可在“我的神马购-账户余额”中查询。</span></p><p><br/></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">&nbsp; &nbsp; 充值金额如果没有即时到达“账户余额”，请别担心，联系我们的客服即可。</span></p><p><br/></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">&nbsp; &nbsp; 所充值的金额可立即消费，如当前不用于消费，金额将存于您的<span style=\"font-family: 宋体, SimSun; font-size: 14px;\">一元神马购</span>账户余额中，以便下次消费支付。</span></p><p><br/></p>', '0', '0', '1375862712');
INSERT INTO `go_article` VALUES ('7', '4', '', '商品配送', '', '', 'a:0:{}', '商品配送,一元神马购', '一元神马购会在开奖后的第一时间，根据用户注册信息中的手机号码、邮箱地址，以短信、邮件的形式通知中奖用户，请中奖者在收到通知后尽快登陆“我的神马购”提交并确认收货地址和联系方式，超过7天未确认收货地址和联系方式者，将视为自动放弃该商品。', '<p>	</p><p><br/></p><p><br/></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">&nbsp; &nbsp; 中奖信息会第一时间在平台公开，用户可在个人中心查看自己的众乐及中奖纪录，还可在“最新揭晓”栏目查看他人交易、中奖以及晒单记录。</span></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\"><br/></span></p><p style=\"text-align: center;\"><img src=\"http://www.1ysmg.com/statics/uploads/shopimg/20160425/75729470556406.jpg\" title=\"15565929135812.jpg\"/></p><p style=\"text-align:center\"><br/></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\"><br/></span></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">&nbsp; &nbsp; 一元神马购会在开奖后的第一时间，根据用户注册信息中的手机号码、邮箱地址，以短信、邮件的形式通知中奖用户，请中奖者在收到通知后尽快登陆“我的神马购”提交并确认收货地址和联系方式，超过7天未确认收货地址和联系方式者，将视为自动放弃该商品。</span></p><p><br/></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">&nbsp; &nbsp; 若遇商品暂时缺货或者是有其他方面的问题，一元神马购客服将会及时与您沟通处理。</span><span style=\"font-family: 宋体, SimSun; font-size: 14px;\"></span></p><p><br/></p><p><strong><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">温馨提示：</span></strong><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">请中奖者尽量亲自签收并当面拆箱验货，如果发现运输途中造成了商品的损坏，请不要签收，可以拒签退回。</span></p><p><br/></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">&nbsp; &nbsp; 中奖用户在收到中奖商品后，可以在“晒单分享”中拍照晒单、填写中奖感言，与网友们分享中奖的喜悦，帮助大家更充分的了解一元神马购平台的乐趣！</span></p><p><br/></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">&nbsp; &nbsp; 晒单分享的时候，我们提醒您注意以下几点：</span></p><p><br/></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">1、详细的内容，是晒单的精华，因此，建议您与我们多多分享您的神马购感言，并且配上3张以上不同的照片（跪求高清）；</span></p><p><br/></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">2、真实，是晒单的灵魂，因此，您可以将快递单号也晒上来哦（快递单上的个人隐私部分，建议您马赛克处理下）；</span></p><p><br/></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">3、一般大家都喜欢沾沾仙气，因此，您能够出镜和商品合个影儿什么的，给大家一起膜拜，就再好不过了；</span></p><p><br/></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">4、如果同一类商品多次获奖，请勿简单的复制粘贴之前的晒单内容哦。</span></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\"><br/></span></p>', '0', '1', '1375862725');
INSERT INTO `go_article` VALUES ('8', '4', '', '物流费用', '', '', 'a:0:{}', '配送费用,一元神马购', '一元神马购的所有商品全国范围免快递费用。（港澳台地区除外）', '<p>	</p><p><br/></p><p><br/></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">&nbsp; &nbsp; <span style=\"font-family: 宋体, SimSun; font-size: 14px;\">一元神马购</span>的所有商品全国范围免快递费用。（港澳台地区除外）</span></p><p><br/></p>', '0', '2', '1375862737');
INSERT INTO `go_article` VALUES ('9', '4', '', '物流安全', '', '', 'a:0:{}', '物流安全,一元神马购', '商品通过京东、顺丰等物流方式送达中奖用户，物流安全有保障；商品发货后，中奖用户可登陆“我的神马购”查询物流单号，并据此追踪物流进程。', '<p>	</p><p><br/></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">&nbsp; &nbsp; 商品通过京东、顺丰等物流方式送达中奖用户，物流安全有保障；商品发货后，中奖用户可登陆“我的神马购”查询物流单号，并据此追踪物流进程。</span><br/></p><p><br/></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">&nbsp; &nbsp; 如果长时间未收到商品，可能出现的问题：</span></p><p><br/></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">1、确保收货地址、邮编、电话、Email、地址等个人信息的准确性；</span></p><p><br/></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">2、当您的商品送达目的地之后，如快递人员在快件保管期（一般3-7天）未能联系到您，快件被退回，由此产生的费用将由您自行承担。</span></p><p><br/></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">3、如遇不可预测的自然原因，如地震、洪水、冰冻雪灾等，可能会造成商品配送的延迟。</span></p><p><br/></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">4、如快递无法配送至中奖用户提供的送货地址，将默认配送到距离最近的送货地，由中奖者本人自提。</span></p><p><br/></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">&nbsp; 中奖用户若长时间未收到商品，有可能存在货到未提的情况，建议中奖用户根据物流单号，及时关注物流进程，亦可向相应的快递/物流公司直接咨询。</span></p><p><br/></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">6、快递/物流公司的相关规定所造成的商品配送延误（建议中奖用户查看相应快递/物流公司官方网站或致电咨询）。</span></p><p><br/></p>', '0', '3', '1375862747');
INSERT INTO `go_article` VALUES ('10', '4', '', '温馨提示', '', '', 'a:0:{}', '温馨提示,一元神马购', '若有任何疑问，请及时拨打客服电话（400-601-6372）反馈信息，若因用户未仔细检查商品即签收后产生的纠纷，一元神马购概不负责，仅承担协调处理的义务。', '<p></p><p><br/></p><p>1、详情页面展示信息仅供参考，商品以实物为准。<br/></p><p><br/></p><p>2、非质量问题，不在三包范围内，不给予退换货。</p><p><br/></p><p>3、签收时请尽量亲自签收，务必仔细检查商品，如：外包装是否被开封，商品是否破损，配件是否缺失，功能是否正常。如有损坏，可以拒签退回，在确保无误后再签收，以免产生不必要的损失和纠纷。</p><p><br/></p><p>&nbsp; &nbsp; 若有任何疑问，请及时与客服QQ（1164099224）反馈信息，若因用户未仔细检查商品即签收后产生的纠纷，一元神马购概不负责，仅承担协调处理的义务。</p><p><br/></p><p>4、用户所获商品出现国家三包所规定的功能性故障时，为不耽误使用，缩短故障商品的维修时间，我们建议您直接联系生产厂家售后服务中心进行处理。</p><p><br/></p><p>5、如快递无法配送至中奖用户提供的送货地址，将默认配送到距离最近的送货地，由中奖者本人自提。</p><p><br/></p><p>6、若因地址、联系方式填写有误等情况造成商品无法完成投递或被退回，所产生的额外费用及后果由中奖用户负责。</p><p><br/></p><p>7、如因不可抗力因素（战争、不可预测的自然因素、政府或社会行为），所造成的商品配送延迟、商品运输途中的损毁等，一元神马购不承担责任。</p><p><br/></p><p>8、若商品已签收，则说明商品配送正确无误且不存在影响使用的因素，一元神马购有权不受理换货申请。</p><p><br/></p>', '0', '4', '1375862760');
INSERT INTO `go_article` VALUES ('12', '3', '', '隐私声明', '', '', 'a:0:{}', '隐私声明,一元神马购', '您注册成为一元神马购用户后，将会收到以电子邮件、短信的形式为您发送的最新活动与通知。为此，我们保留为用户发送最新活动与通知等告知服务的权利。当您注册成为一元神马购用户后即表明您已同意接受此项服务。', '<p>	</p><p><br/></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">&nbsp; &nbsp; 欢迎您访问并使用充满乐趣和惊喜的新型购物网站：一元神马购（</span><a href=\"http://www.1ysmg.com\" target=\"_self\" title=\"\" textvalue=\"www.1ysmg.com\">www.1ysmg.com</a><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">），我们以本隐私声明申明对访问者隐私保护的许诺。在未经您同意之下，我们绝不会将您的个人数据提供予任何与本网站服务无关的第三人。如您访问本网站，那么您便接受了本隐私声明。以下文字公开本站（</span><a href=\"http://www.1ysmg.com\" target=\"_self\" title=\"\" textvalue=\"www.1ysmg.com\">www.1ysmg.com</a><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">）对信息收集和使用的情况。本站的隐私声明会不断改进，随着我站服务的增加，我们会随时更新我们的隐私声明。欢迎您随时查看本声明，并可向kf@1ysmg.com反馈您的意见和建议。</span></p><p><br/></p><p><strong><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">一、未成年人的特别注意事项</span></strong></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">&nbsp; &nbsp; 如果您未满18周岁，您无权使用本站服务，因此我们希望您不要向我们提供任何个人信息。如果您未满18周岁，您只能在父母或监护人的陪同下才可以使用本站服务。</span></p><p><br/></p><p><strong><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">二、用户名和密码</span></strong></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">&nbsp; &nbsp; 当您注册为本站用户时，您需要根据提示填写用户名和密码，并设置密码验证手机和邮箱，以便在您丢失密码时用于您的身份确认。您只能通过您的密码来使用您的账户。如果您泄漏了密码，您可能会丢失您的个人识别信息，并可能导致对您不利的司法行为。因此无论任何原因使您的密码安全受到危及时，您应该立即通过<span style=\"font-family: 宋体, SimSun; font-size: 14px;\">kf@1ysmg</span><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">.com</span>和我们取得联系。</span></p><p><br/></p><p><strong><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">三、完善信息</span></strong></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">&nbsp; &nbsp; 在您注册成为本站用户后或参与购物并获得商品后，您需要在“个人设置”里完善您的个人信息，根据填写要求提供您的真实姓名、详细地址与电子邮件地址。您还有权选择填写更多信息，包括身份证号码、电话号码、QQ号等。我们使用注册信息来保护用户账户安全，在您的账户利益受到危害的情况下。同时我们使用注册信息来获得用户统计资料，以便为用户提供更多更新的服务。我们会通过短信、电子邮件等方式通知您有关新的服务。</span></p><p><br/></p><p><strong><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">四、什么是Cookies？</span></strong></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">&nbsp; &nbsp; Cookies是一种能够让网站服务器把少量数据储存到客户端的硬盘或内存，或是从客户端的硬盘读取数据的一种技术。Cookies是当你浏览某网站时，由Web服务器置于你硬盘上的一个非常小的文本文件，它可以记录你的用户ID、密码、浏览过的网页、停留的时间等信息。</span></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">&nbsp; &nbsp; 当你再次来到该网站时，网站通过读取Cookies，得知你的相关信息，就可以做出相应的动作，如在页面显示欢迎你的标语，或者让你不用输入ID、密码就直接登录等等。从本质上讲，它可以看作是你的身份证。但Cookies不能作为代码执行，也不会传送病毒，且为你所专有，并只能由提供它的服务器来读取。</span></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">&nbsp; &nbsp; 本站为您提供Cookies使用功能，以便为您提供周到的个性服务，使您再次访问时更加快捷、方便。当然，您有权选择关闭此服务，本站可为您停止Cookies服务。</span></p><p><br/></p><p><strong><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">五、信息披露</span></strong></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">&nbsp; &nbsp; 我们不会向任何第三方提供，出售，出租，分享和交易用户的个人信息。当在以下情况下，用户的个人信息将部分或全部被善意披露：</span></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">1、经用户同意，向第三方披露；</span></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">2、如用户是符合资格的知识产权投诉人并已提起投诉，应被投诉人要求，向被投诉人披露，以便双方处理可能的权利纠纷；</span></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">3、根据法律的有关规定，或者行政或司法机构的要求，向第三方或者行政、司法机构披露；</span></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">4、如果用户出现违反中国有关法律或者网站政策的情况，需要向第三方披露；</span></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">5、为提供你所要求的产品和服务，而必须和第三方分享用户的个人信息；</span></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">6、其它本站根据法律或者网站政策认为合适的披露。</span></p><p><br/></p><p><strong><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">六、安全</span></strong></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">&nbsp; &nbsp; 我们网站有相应的安全措施来确保我们掌握的信息不丢失，不被滥用和变造。这些安全措施包括向其它服务器备份数据和对用户密码加密。尽管我们有这些安全措施，但请注意在因特网上不存在“完善的安全措施”。</span></p><p><br/></p><p><strong><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">七、查阅和修改个人信息</span></strong></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">&nbsp; &nbsp; 您使用一元神马购服务时，您可以随时查阅并随时修改您的个人信息。同时，在您登录后我们会提供登录日志功能，我们会将您每次登陆的IP地址公布供您查看，以便保证您账号的安全性。因此提供查看IP地址仅仅是安全的必要。</span></p><p><br/></p><p><strong><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">八、邮件/短信服务</span></strong></p><p><span id=\"_baidu_bookmark_start_15\" style=\"display: none; line-height: 0px;\">‍</span></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">&nbsp; &nbsp; 您注册成为<span style=\"font-family: 宋体, SimSun; font-size: 14px;\">一元神马购</span>用户后，将会收到以电子邮件、短信的形式为您发送的最新活动与通知。为此，我们保留为用户发送最新活动与通知等告知服务的权利。当您注册成为<span style=\"font-family: 宋体, SimSun; font-size: 14px;\">一元神马购</span>用户后即表明您已同意接受此项服务。如您不想接受来自<span style=\"font-family: 宋体, SimSun; font-size: 14px;\">一元神马购</span>的邮件、短信与通知，您可向<span style=\"font-family: 宋体, SimSun; font-size: 14px;\">一元神马购</span>客服提出退阅申请，并注明您的电子邮件地址、手机号码等相关信息，<span style=\"font-family: 宋体, SimSun; font-size: 14px;\">一元神马购</span>将在收到您的申请后为您及时办理。</span></p><p><br/></p>', '71', '4', '1378451819');
INSERT INTO `go_article` VALUES ('14', '1', '5115', '1512', '', 'pho', 'a:0:{}', '2', '', '<p>欢迎使用云购系统!</p>', '23', '1', '1417767065');
INSERT INTO `go_article` VALUES ('15', '1', '5115', '1512', '', 'pho', 'a:0:{}', '2', '', '<p>欢迎使用云购系统!</p>', '23', '1', '1417767065');
INSERT INTO `go_article` VALUES ('20', '16', '', '了解一元神马购', '', '', 'a:0:{}', '了解神马购,一元神马购', '一元神马购（www.1ysmg.com）引入众筹理念重新构建网购消费新体验，凭借强大的技术和公平的算法，100%公平公正、100%正品保障，引领一种集娱乐、购物于一体的网购潮流。', '<p>	</p><p><br/></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">&nbsp; &nbsp; 一元神马购（</span><a href=\"http://www.1ysmg.com\" target=\"_self\" title=\"\" textvalue=\"www.1ysmg.com\">www.1ysmg.com</a><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">）引入众筹理念重新构建网购消费新体验，凭借强大的技术和公平的算法，100%公平公正、100%正品保障，引领一种集娱乐、购物于一体的网购潮流。</span><br/></p><p style=\"white-space: normal;\"><br/></p><p style=\"white-space: normal;\"><strong><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">理念：</span></strong><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">充满乐趣与惊喜，用一元实现你的所有梦想</span></p><p style=\"white-space: normal;\"><br/></p><p style=\"text-align:center\"><img src=\"http://www.1ysmg.com/statics/uploads/shopimg/20160425/53288499564680.jpg\" title=\"90297770136334.jpg\"/></p><p style=\"white-space: normal;\"><strong><span style=\"font-family: 宋体, SimSun; font-size: 14px;\"><br/></span></strong></p><p style=\"white-space: normal;\"><strong><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">特色：</span></strong></p><p style=\"white-space: normal;\"><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">1、一元神马购，惊喜无限</span></p><p style=\"white-space: normal;\"><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">一元神马购的用户可花费1元兑换一枚神马币，凭神马币即有机会在投入1元钱的成本下获得价值百万倍的心动商品。</span></p><p style=\"white-space: normal;\"><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">2、小投入，大回报</span></p><p style=\"white-space: normal;\"><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">每件商品按价格均分成若干等份，当所有份额认购完毕，一元神马购根据既定算法计算出幸运神马号，持有该幸运神马号的用户将获得该商品。</span></p><p style=\"white-space: normal;\"><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">3、公平公正公开</span></p><p style=\"white-space: normal;\"><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">1）一元神马购作为一个平台，保证所有参与神马购的用户行为都享受同样的权利，100%保证每个用户所购买的每人次神马号，都具有相同的中奖概率。</span></p><p style=\"white-space: normal;\"><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">2）一元神马购承诺开奖结果100%的公平公正。一元神马购幸运神马号计算公式，加入了“时时彩”中奖数据作为外部随机因子，确保计算结果具有绝对的随机性、可靠性和公正性。</span></p><p style=\"white-space: normal;\"><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">3）一元神马购的众乐过程完全公开透明，用户可以随时查看每个商品的神马购记录、算法规则、开奖结果等信息，也可点击查看他人神马购记录、中奖记录等。</span></p><p style=\"white-space: normal;\"><br/></p><p style=\"white-space: normal;\"><strong><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">企业愿景</span></strong></p><p style=\"white-space: normal;\"><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">&nbsp; &nbsp; 随着互联网的发展及网购消费模式的多样化，一元神马购致力于成为中国电子商务网站中最具活力的生力军，为广大用户提供更加优质的服务。</span></p>', '36', '1', '1450247251');
INSERT INTO `go_article` VALUES ('21', '16', '', '公司简介', '', '', 'a:0:{}', '一元神马购,公司简介', '', '<p>	</p><p><br/></p><p><img src=\"http://yiyuanzhongle.com/statics/uploads/shopimg/20160121/22664642341642.jpg\" title=\"912先93.jpg\"/></p><p><br/></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">&nbsp; <br/></span></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">&nbsp; &nbsp; 深圳大唐讯士有限公司是一家专注于移动互联网产品和服务的创新型企业，公司成立于2005年，研发中心和运营中心设在深圳。</span></p><p style=\"white-space: normal;\"><br/></p><p style=\"white-space: normal;\"><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">&nbsp; &nbsp; 公司拥有顶尖的互联网精英团队，凭借强大的技术研发实力和市场运营能力，致力于通过技术和创新让人们的生活更美好，逐步打造了一元众乐、米荚、大屏触控终端等系列新型产品，业务覆盖：众筹/电商、移动营销/广告分发、智能终端/行业解决方案。</span></p><p style=\"white-space: normal;\"><br/></p><p style=\"white-space: normal;\"><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">&nbsp; &nbsp; 一元神马购是公司旗下创新型互联网项目，通过引入众筹理念重新构建网购消费新体验，凭借强大的技术和公平的算法，100%公平公正、100%正品保障，为用户打造一个集娱乐、购物于一体的新型电子商务平台。</span></p><p style=\"white-space: normal;\"><br/></p><p style=\"white-space: normal;\"><br/><span style=\"font-family: 宋体, SimSun; font-size: 14px;\"></span></p><p style=\"white-space: normal;\"><span style=\"font-family: 宋体, SimSun; font-size: 14px;\"><img src=\"http://yiyuanzhongle.com/statics/uploads/shopimg/20160121/67932793341728.jpg\" title=\"912先93.jpg\"/></span></p><p><br/></p>', '100', '1', '1450247322');
INSERT INTO `go_article` VALUES ('22', '2', '', '服务协议', '', '', 'a:0:{}', '服务协议,一元神马购', '如用户通过进入注册程序并勾选“我同意一元神马购服务协议”，即表示用户与深圳市大唐讯士电子科技有限公司已达成协议，自愿接受本服务条款的所有内容。此后，用户不得以未阅读本服务条款内容作任何形式的抗辩。', '<p>	</p><p><br/></p><p><br/></p><p><br/></p><p style=\"white-space: normal; text-align: center;\"><strong><span style=\"font-family: 宋体, SimSun; font-size: 20px;\">一元神马购服务协议</span></strong></p><p style=\"white-space: normal;\"><span style=\"font-family: 宋体, SimSun; font-size: 14px;\"><br/></span></p><p style=\"white-space: normal;\"><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">&nbsp; &nbsp; 欢迎访问一元神马购（<a href=\"http://www.1ysmg.com\" target=\"_self\" title=\"\" textvalue=\"www.1ysmg.com\">www.1ysmg.com</a>），申请使用公司提供的一元神马购服务（以下简称“<span style=\"font-family: 宋体, SimSun; font-size: 14px;\">一元神马购</span>”，“<span style=\"font-family: 宋体, SimSun; font-size: 14px;\">一元神马购</span>”指用户花费1元兑换一个神马币，用户可凭神马币使用<span style=\"font-family: 宋体, SimSun; font-size: 14px;\">一元神马购</span>的服务），请您（下列简称为“用户”）仔细阅读以下全部内容（<span style=\"text-decoration: underline;\"><strong>特别是粗体下划线标注的内容</strong></span>）。如用户不同意本服务条款任意内容，请勿注册或使用一元众乐。如用户通过进入注册程序并勾选“我同意<span style=\"font-family: 宋体, SimSun; font-size: 14px;\">一元神马购</span>服务协议”，即表示用户与公司已达成协议，自愿接受本服务条款的所有内容。此后，用户不得以未阅读本服务条款内容作任何形式的抗辩。</span></p><p style=\"white-space: normal;\"><span style=\"font-family: 宋体, SimSun; font-size: 14px;\"><br/></span></p><p style=\"white-space: normal;\"><strong><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">一、用户使用<span style=\"font-family: 宋体, SimSun; font-size: 14px;\">一元神马购</span>的前提条件</span></strong></p><p style=\"white-space: normal;\"><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">&nbsp; &nbsp; 用户在使用<span style=\"font-family: 宋体, SimSun; font-size: 14px;\">一元神马购</span>时须具备相应的权利能力和行为能力，能够独立承担法律责任，如果用户在18周岁以下，必须在父母或监护人的监护参与下才能使用本站。</span></p><p style=\"white-space: normal;\"><span style=\"font-family: 宋体, SimSun; font-size: 14px;\"><br/></span></p><p style=\"white-space: normal;\"><strong><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">二、用户管理</span></strong></p><p style=\"white-space: normal;\"><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">1、用户ID</span></p><p style=\"white-space: normal;\"><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">&nbsp; &nbsp;用户首次登陆<span style=\"font-family: 宋体, SimSun; font-size: 14px;\">一元神马购</span>时，<span style=\"font-family: 宋体, SimSun; font-size: 14px;\">一元神马购</span>会为每位用户生成一个帐户ID，作为其使用<span style=\"font-family: 宋体, SimSun; font-size: 14px;\">一元神马购</span>服务的唯一身份标识，用户需要对其帐户项下发生的所有行为负责。</span></p><p style=\"white-space: normal;\"><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">2、用户资料完善</span></p><p style=\"white-space: normal;\"><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">&nbsp; &nbsp;用户应当在使用<span style=\"font-family: 宋体, SimSun; font-size: 14px;\">一元神马购</span>时完善个人资料，用户资料包括但不限于个人手机号码、邮箱地址、收货地址、帐号名称、头像、密码、注册或更新邮箱帐号时输入的所有信息。</span></p><p style=\"white-space: normal;\"><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">&nbsp; &nbsp;用户在完善个人资料时承诺遵守法律法规、社会主义制度、国家利益、公民合法权益、公共秩序、社会道德风尚和信息真实性等七条底线，不得在资料中出现违法和不良信息，且用户保证其在完善个人资料和使用帐号时，不得有以下情形：</span></p><p style=\"white-space: normal;\"><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">（1）违反宪法或法律法规规定的；</span></p><p style=\"white-space: normal;\"><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">（2）危害国家安全，泄露国家秘密，颠覆国家政权，破坏国家统一的；</span></p><p style=\"white-space: normal;\"><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">（3）损害国家荣誉和利益的，损害公共利益的；</span></p><p style=\"white-space: normal;\"><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">（4）煽动民族仇恨、民族歧视，破坏民族团结的；</span></p><p style=\"white-space: normal;\"><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">（5）破坏国家宗教政策，宣扬邪教和封建迷信的；</span></p><p style=\"white-space: normal;\"><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">（6）散布谣言，扰乱社会秩序，破坏社会稳定的</span></p><p style=\"white-space: normal;\"><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">（7）散布淫秽、色情、赌博、暴力、凶杀、恐怖或者教唆犯罪的；</span></p><p style=\"white-space: normal;\"><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">（8）侮辱或者诽谤他人，侵害他人合法权益的；</span></p><p style=\"white-space: normal;\"><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">（9）含有法律、行政法规禁止的其他内容的。</span></p><p style=\"white-space: normal;\"><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">&nbsp; &nbsp;若用户提供给公司的资料不准确，不真实，含有违法或不良信息的，公司有权不予完善，并保留终止用户使用<span style=\"font-family: 宋体, SimSun; font-size: 14px;\">一元神马购</span>的权利。若用户以虚假信息骗取帐号ID或帐号头像、个人简介等注册资料存在违法和不良信息的，公司有权采取通知限期改正、暂停使用、注销登记等措施。对于冒用关联机构或社会名人注册帐号名称的，公司有权注销该帐号，并向政府主管部门进行报告。</span></p><p style=\"white-space: normal;\"><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">&nbsp; &nbsp;根据相关法律、法规规定以及考虑到<span style=\"font-family: 宋体, SimSun; font-size: 14px;\">一元神马购</span>服务的重要性，用户同意：</span></p><p style=\"white-space: normal;\"><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">（1）在完善资料时提交个人有效身份信息进行实名认证；</span></p><p style=\"white-space: normal;\"><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">（2）提供及时、详尽及准确的用户资料；</span></p><p style=\"white-space: normal;\"><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">（3）不断更新用户资料，符合及时、详尽准确的要求，对完善个人资料时填写的身份证件信息不能修改。</span></p><p style=\"white-space: normal;\"><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">3、神马币</span></p><p style=\"white-space: normal;\"><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">（1）<span style=\"font-family: 宋体, SimSun; font-size: 14px;\">神马币</span>必须通过公司提供或认可的平台获得，从非公司提供或认可的平台所获得的<span style=\"font-family: 宋体, SimSun; font-size: 14px;\">神马币</span>将被认定为来源不符合本服务协议，公司有权拒绝从非公司提供或认可的平台所获得的<span style=\"font-family: 宋体, SimSun; font-size: 14px;\">神马币</span>在一元神马购中使用。</span></p><p style=\"white-space: normal;\"><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">（2）<span style=\"text-decoration: underline;\"><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">神马币</span>不能用于购买或兑换公司其它收费服务或者转移给其他用户。</span></span></p><p style=\"white-space: normal;\"><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">4、用户应当保证在使用一元神马购的过程中遵守诚实信用原则，不扰乱<span style=\"font-family: 宋体, SimSun; font-size: 14px;\">一元神马购</span>的正常秩序，不得通过使用他人帐户、一人注册多个帐户、使用程序自动处理等非法方式损害他人或公司的利益。</span></p><p style=\"white-space: normal;\"><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">5、若用户存在任何违法或违反本服务协议约定的行为，公司有权视用户的违法或违规情况适用以下一项或多项处罚措施：</span></p><p style=\"white-space: normal;\"><strong><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">（1）<span style=\"text-decoration: underline;\">责令用户改正违法或违规行为</span><span style=\"text-decoration: underline;\">；</span></span></strong></p><p style=\"white-space: normal;\"><strong><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">（2）<span style=\"text-decoration: underline;\">中止、终止部分或全部服务</span><span style=\"text-decoration: underline;\">；</span></span></strong></p><p style=\"white-space: normal;\"><strong><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">（3）<span style=\"text-decoration: underline;\">取消用户神马购订单并取消商品发放（若用户已获得商品）， 且用户已获得的神马币不予退回</span><span style=\"text-decoration: underline;\">；</span></span></strong></p><p style=\"white-space: normal;\"><strong><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">（4）<span style=\"text-decoration: underline;\">冻结或注销用户帐号及其帐号中的神马币（如有）</span>；</span></strong></p><p style=\"white-space: normal;\"><strong><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">（5）<span style=\"text-decoration: underline;\">其他公司认为合适在符合法律法规规定的情况下的处罚措施</span></span></strong><span style=\"text-decoration: underline;\"><strong><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">。</span></strong></span></p><p style=\"white-space: normal;\"><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">&nbsp; &nbsp;<span style=\"text-decoration: underline;\"><strong>若用户的行为造成公司及其关联公司损失的，用户还应承担赔偿责任<span style=\"text-decoration: none;\"></span></strong><span style=\"text-decoration: none;\"><strong><span style=\"text-decoration: none;\">。</span></strong></span></span></span></p><p style=\"white-space: normal;\"><strong><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">6、</span></strong><span style=\"font-family: 宋体, SimSun; font-size: 14px;\"><strong><span style=\"text-decoration: underline;\">若用户发表侵犯他人权利或违反法律规定的言论，公司有权停止传输并删除其言论、禁止该用户发言、注销用户帐号及其帐号中的神马币（如有），同时，公司保留根据国家法律法规、相关政策向有关机关报告的权利。</span></strong></span></p><p style=\"white-space: normal;\"><span style=\"font-family: 宋体, SimSun; font-size: 14px;\"><br/></span></p><p style=\"white-space: normal;\"><strong><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">三、一元神马购的规则</span></strong></p><p style=\"white-space: normal;\"><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">1、释义</span></p><p style=\"white-space: normal;\"><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">（1）神马币：指用户为获得商品所支付并由公司预收货款后获得的使用一元神马购的凭据。</span></p><p style=\"white-space: normal;\"><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">（2）神马号：指用户使用神马币参与<span style=\"font-family: 宋体, SimSun; font-size: 14px;\">一元神马购</span>服务时所获取的随机分配号码。</span></p><p style=\"white-space: normal;\"><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">（3）幸运<span style=\"font-family: 宋体, SimSun; font-size: 14px;\">神马号</span>：指与某件商品的全部<span style=\"font-family: 宋体, SimSun; font-size: 14px;\">神马购号码</span>分配完毕后，<span style=\"font-family: 宋体, SimSun; font-size: 14px;\">一元神马购</span>根据神马购规则（详见<span style=\"font-family: 宋体, SimSun; font-size: 14px;\">一元神马购</span>官方页面）计算出的一个号码。持有该幸运号码的用户可直接获得该商品。</span></p><p style=\"white-space: normal;\"><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">2、公司承诺遵循公平、公正、公开的原则运营<span style=\"font-family: 宋体, SimSun; font-size: 14px;\">一元神马购</span>，确保所有用户在<span style=\"font-family: 宋体, SimSun; font-size: 14px;\">一元神马购</span>中享受同等的权利与义务，中奖结果向所有用户公示。</span></p><p style=\"white-space: normal;\"><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">3、用户知悉，除本协议另有约定外，无论是否获得商品，用户用于参与<span style=\"font-family: 宋体, SimSun; font-size: 14px;\">一元神马购</span>的神马币不能退回；其完全了解参与<span style=\"font-family: 宋体, SimSun; font-size: 14px;\">一元神马购</span>活动的存在的风险，公司不保证用户参与<span style=\"font-family: 宋体, SimSun; font-size: 14px;\">一元神马购</span>一定会获得商品。</span></p><p style=\"white-space: normal;\"><strong><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">4、</span></strong><span style=\"font-family: 宋体, SimSun; font-size: 14px; text-decoration: underline;\"><strong>用户通过参与<span style=\"font-family: 宋体, SimSun; font-size: 14px;\">一元神马购</span>获得商品后，应在7天内登录<span style=\"font-family: 宋体, SimSun; font-size: 14px;\">一元神马购</span>提交并确认收货地址，否则视为放弃该商品，用户因此行为造成的损失，公司不承担任何责任。</strong></span></p><p style=\"white-space: normal;\"><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">5、用户通过参与<span style=\"font-family: 宋体, SimSun; font-size: 14px;\">一元神马购</span>获得的商品，享受该商品生产厂家提供的三包服务，具体三包规定以该商品生产厂家公布的为准。</span></p><p style=\"white-space: normal;\"><strong><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">6、<span style=\"text-decoration: underline;\">如果下列情形发生，公司有权取消用户神马购订单：</span></span></strong></p><p style=\"white-space: normal;\"><strong><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">（1）<span style=\"text-decoration: underline;\">因不可抗力、<span style=\"font-family: 宋体, SimSun; font-size: 14px;\">一元神马购</span>系统发生故障或遭受第三方攻击，或发生其他公司无法控制的情形；</span></span></strong></p><p style=\"white-space: normal;\"><strong><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">（2）<span style=\"text-decoration: underline;\">根据公司已经发布的或将来可能发布或更新的各类规则、公告的规定，公司有权取消用户订单的情形。</span></span></strong></p><p style=\"white-space: normal;\"><strong><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">&nbsp; &nbsp;<span style=\"text-decoration: underline;\">公司有权取消用户订单后，用户可申请退还神马币，所退神马币将在3个工作日内退还至用户帐户中。</span></span></strong></p><p style=\"white-space: normal;\"><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">7、若某件商品的神马购号码从开始分配之日起90天未分配完毕，则公司有权取消该件商品的神马购活动，并向用户退还神马币，所退还神马币将在3个工作日内退还至用户帐户中。</span></p><p style=\"white-space: normal;\"><span style=\"font-family: 宋体, SimSun; font-size: 14px;\"><br/></span></p><p style=\"white-space: normal;\"><strong><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">四、本服务协议的修改</span></strong></p><p style=\"white-space: normal;\"><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">&nbsp; &nbsp; 用户知晓公司不时公布或修改的与本服务协议有关的其他规则、条款及公告等是本服务协议的组成部分。公司有权在必要时通过在<span style=\"font-family: 宋体, SimSun; font-size: 14px;\">一元神马购</span>内发出公告等合理方式修改本服务协议，用户在享受各项服务时，应当及时查阅了解修改的内容，并自觉遵守本服务协议。用户如继续使用本服务协议涉及的服务，则视为对修改内容的同意，当发生有关争议时，以最新的服务协议为准；用户在不同意修改内容的情况下，有权停止使用本服务协议涉及的服务。</span></p><p style=\"white-space: normal;\"><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">&nbsp; &nbsp; 如用户对本规则内容有任何疑问，可客服QQ（1106714289</span><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">）进行查询。</span></p>', '75', '2', '1451123081');
INSERT INTO `go_article` VALUES ('23', '16', '', '加入我们', '', '', 'a:0:{}', '加入我们,一元神马购', '', '<p>	</p><p><br/></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">加入我们</span></p><p><strong><span style=\"font-family: 宋体, SimSun; font-size: 14px;\"><br/></span></strong></p><p><strong><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">Java软件工程师</span></strong><br/></p><p><br/></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">工作年限：2年 &nbsp; &nbsp; &nbsp;学历要求：大专&nbsp;</span></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">专业要求：软件工程 计算机科学与技术</span></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">薪酬福利：五险一金 绩效奖金 弹性工作 年终奖金 下午茶</span></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">职能类别：软件工程师 &nbsp;互联网软件开发工程师&nbsp;</span></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">职位标签：java &nbsp;java开发 &nbsp;微信</span></p><p><br/></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">岗位职责：</span></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">1、负责微信公众平台消息接口开发，负责系统的技术架构和概要设计；</span></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">2、负责微信公众平台所需要的接口和数据管理，API数据接口开发工作；</span></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">3、完成详细设计及编码、进行代码审查；</span></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">4、负责公司微信公众号/服务号的接口设计、开发、数据管理、日常维护和其它后台维护管理工作。</span></p><p><br/></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">任职要求：</span></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">1、有基于微信或第三方公众号平台相关项目开发，2年左右经验；</span></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">2、精通Java语言，对软件工程有较深的理解，熟悉软件开发流程；</span></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">3、熟悉SpringMVC、Mybatis等开源框架，对其原理了解；</span></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">4、熟练使用MySQL数据库，JQeury、AJAX对CSS者优先；</span></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">5、熟悉web中间件jetty、tomcat的配置和管理，熟悉Eclipse等开发工具和SVN等版本管理工具；</span></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">6、具有较强的逻辑思维、思路清晰，能独立分析和解决问题，强烈的责任心与团队奉献精神。</span></p><p><br/></p><p><br/></p><p><strong><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">运维工程师</span></strong></p><p><strong><span style=\"font-family: 宋体, SimSun; font-size: 14px;\"><br/></span></strong></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">工作年限：1年 学历要求：大专 薪资范围：8000-9999/月&nbsp;</span></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">专业要求：计算机科学与技术 通信工程</span></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">薪酬福利：五险一金 弹性工作 绩效奖金 年终奖金 下午茶</span></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">职能类别：技术支持/维护工程师&nbsp;</span></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">职位标签：运维 &nbsp;维护 &nbsp;服务器</span></p><p><br/></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">岗位职责：</span></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">1. 负责网站应用系统日常运维工作，包括：应用程序上线、下线、升级、应用配置参数修改等应用系统变更；</span></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">2. 负责网站应用系统支持平台（包括Apache Web Server层面，Tomcat应用服务器层面，不包括操作系统层面与DBMS层面）日常运维工作，包括patch更新、系统配置参数修改等，维护其高可用性及高性能；</span></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">3. 负责网站服务器系统可用性与性能监控，包括自动监控与人工监控；</span></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">4. 负责网站运维的知识管理体系、流程与文档建设。</span></p><p><br/></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">任职要求：</span></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">1. 大学专科及以上学历，计算机、电子、信息及相关专业者优先；</span></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">2. 互联网、金融或者电信行业两年以上系统运维经验者优先，至少三年相关经验；</span></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">3. 熟悉Linux,Apache，Tomcat，MySql等Web服务器并且有运维经验；</span></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">4. 熟悉J2SE/J2EE架构概念，有Tomcat中间件运维经验；</span></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">5. 能够运用linux命令及其他常用开源软件，例如Shell、Python，通过编写简单脚本实现应用程序批量分发、自动化监控、日志分析等日常运维工作。</span></p><p><br/></p><p><br/></p><p><strong><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">新媒体运营经理</span></strong></p><p><br/></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">工作年限：2年 学历要求：本科</span></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">专业要求：市场营销</span></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">薪酬福利：五险一金 绩效奖金 年终奖金 股票期权 茶点零食 定期聚会</span></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">职能类别：媒介经理 &nbsp;活动执行&nbsp;</span></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">职位标签：新媒体 &nbsp;活动执行 &nbsp;微信推广 &nbsp;app</span></p><p><br/></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">岗位职责：</span></p><p><br/></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">1. 负责公司新媒体运营，包括内容编辑、拉新、发布、维护、互动等；</span></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">2. 负责活动、事件/话题的策划、执行，及业务需求的功能迭代、跟进；</span></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">3. 利用微信平台推广公司的APP，提高APP用户数量；</span></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">4. 跟踪推广效果，分析运营数据，建立有效运营手段实现运营目标；</span></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">5. 整合媒介和推广资源，制定媒介合作计划，开展互推/导流/异业合作等。</span></p><p><br/></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">任职条件：</span></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">1. 了解互联网、移动互联网的运营、营销趋势，尤其是社会化媒体营销；</span></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">2. 乐于创新，了解用户体验的要素；</span></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">3. 具备良好的文字功底及策划能力，思维清晰而有条理，会图片处理尤佳；</span></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">4. 对移动互联网行业有浓厚兴趣，具备较强的沟通协调能力；</span></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">5. 有较强的商业敏感性，具备良好的业务分析判断能力。</span></p><p><br/></p><p><br/></p><p><strong><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">android开发工程师</span></strong></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\"><br/></span></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">工作年限：3-4年 &nbsp; &nbsp; 学历要求：大专</span></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">专业要求：软件工程 计算机科学与技术</span></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">薪酬福利：五险一金 绩效奖金 弹性工作 年终奖金</span></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">职能类别：手机应用开发工程师 &nbsp;互联网软件开发工程师&nbsp;</span></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">职位标签：android &nbsp;安卓 &nbsp;手机</span></p><p><br/></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">岗位职责：</span></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">1. 负责Android Framework核心机制的修改和定制；</span></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">2. 负责Android 系统底层研发，Android系统级服务扩展、APP修改；</span></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">3. 负责Launcher的定制。</span></p><p><br/></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">任职要求：</span></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">1. 大专以上学历，3年以上Android开发经验，本岗位偏向底层；</span></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">2. 做过分屏显示或多屏互动相关工作，熟悉并使用过DLAN或Miracast协议；</span></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">3. 掌握C/C++/Java中的一种或多种，能够熟练使用语言在Linux平台实现具体的算法和数据结构；</span></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">4. 熟悉Android系统框架，熟悉Android Framework层架构；</span></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">5. 有过Android底层驱动开发者优先，面试时请携带相关作品。</span></p><p><br/></p><p><br/></p><p><strong><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">财务助理</span></strong></p><p><br/></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">工作年限：2年 &nbsp; &nbsp; 学历要求：本科&nbsp;</span></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">专业要求：会计学 财务管理</span></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">薪酬福利：做五休二 周末双休 弹性工作 五险一金 带薪年假 全勤奖</span></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">职能类别：出纳员&nbsp;</span></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">职位标签：出纳</span></p><p><br/></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">岗位职责：</span></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">1、办理日常现金资金收支等业务，根据相关单据登记现金日记账，做到日清月结；</span></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">2、负责现金账的核对，现金保管等的管理工作；</span></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">3、负责完成和银行的对账工作，做到账款相符；</span></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">4、负责发放员工的报销款，做到准确无误；</span></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">5、负责发放员工的个人工资、奖金等，做到准确无误；</span></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">6、妥善保管单据，票据。日常账务处理；</span></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">7、负责保存财务相关资料（如证照等）；</span></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">8、负责公司销售报表的统计工作。</span></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">9、完成临时布置的各项任务。</span></p><p><br/></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">任职要求：</span></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">1、本科以上学历，财务、金融、会计相关专业；</span></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">2、两年以上出纳工作经验；</span></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">3、熟悉国家相关财务制度，税收法规；熟悉出纳人员业务流程；</span></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">4、能熟练运用计算机办公软件及熟悉金蝶K3软件；</span></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">5、积极的工作态度和极强的学习能力；</span></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">6、乐观外向，较高的心理素质。</span></p><p><br/></p><p><br/></p><p><strong><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">互联网产品经理</span></strong></p><p><br/></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">工作年限：3-4年 &nbsp; &nbsp; 学历要求：本科&nbsp;</span></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">专业要求：计算机科学与技术 软件工程</span></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">薪酬福利：五险一金 弹性工作 绩效奖金 年终奖金</span></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">职能类别：产品总监 &nbsp;产品经理/主管&nbsp;</span></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">职位标签：互联网产品 &nbsp;产品经理</span></p><p><br/></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">岗位职责：</span></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">1、负责公司产品的生命周期管理,制定产品迭代目标；</span></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">2、分析用户需求，分析竞争对手动态和市场动态，规划产品路线图，提出产品需求或改进意见；</span></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">3、负责新产品/功能的需求设计和原型制作，推动跨部门合作；</span></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">4、对产品需求进行版本管理，产品上线后改进等相关工作。</span></p><p><br/></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">任职要求：</span></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">1、本科及以上学历，3年以上互联网 / 移动互联网产品工作经验；</span></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">2、具备做产品的潜质和特质以及兴趣，喜欢并热爱研究APP，对时间和数据敏感；</span></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">3、对工作充满激情，具有强烈的创新精神，富于责任心，能承受工作压力；</span></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">4、思路清晰，较强的表达能力，善于协调资源推动项目进展。</span></p><p><br/></p><p><br/></p><p><strong><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">HTML5前端工程师</span></strong></p><p><br/></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">工作年限：2年 &nbsp; &nbsp; 学历要求：大专&nbsp;</span></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">专业要求：软件工程 计算机科学与技术</span></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">薪酬福利：五险一金 绩效奖金 弹性工作 年终奖金 下午茶</span></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">职能类别：Web前端开发 &nbsp;手机应用开发工程师&nbsp;</span></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">职位标签：前端 &nbsp;web前端 &nbsp;H5 &nbsp;h5</span></p><p><br/></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">岗位职责：</span></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">1、根据需求完成软件 / 系统的设计，开发，测试和维护；</span></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">2、独立完成相应软件模块的架构设计和编程任务，或对个人所负责的应用模块进行充分的单元测试；</span></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">3、对已有系统提供必要的技术支持，并承担相关功能改进任务；</span></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">4、负责相关技术文档编写。</span></p><p><br/></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">任职要求：</span></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">1、计算机、软件工程本科以上毕业，2年以上Web或者手机前端开发经验；</span></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">2、熟练掌握并使用符合W3C标准的Html5、JavaScript和CSS3相关技术；精通原生js，jQuery/ExtJS等框架，并能熟练运用；</span></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">3、对移动设备的操作特性有了解，包括手机和平板；</span></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">4、对网站及页面性能优化有一定认识和经验，了解不同移动操作系统的浏览器特性，熟悉浏览器兼容性；</span></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">5、精通html5协议，了解常规网络通信协议；</span></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">6、了解数据可视化，web绘图，使用过开源产品；</span></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">7、有良好的沟通表达能力和团队合作能力。</span></p><p><br/></p><p><br/></p><p><br/></p>', '17', '1', '1451124854');
INSERT INTO `go_article` VALUES ('24', '16', '', '联系我们', '', '', 'a:0:{}', '联系我们,一元神马购', '一元神马购诚邀各品牌供应商与我们达成商务合作，推动互联网的发展及网购消费模式的多样化，共创中国电子商务的美好明天。', '<p>	</p><p><br/></p><p><strong><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">联系方式</span></strong><br/></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">公司名称：深圳大唐讯士电子科技有限公司</span></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">联系电话：400-601-6372</span></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">地 &nbsp; 址：深圳市龙岗区龙岗中心城城投商务中心1003 &nbsp; &nbsp;</span></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">邮 &nbsp; 编：518000</span></p><p><br/></p><p><strong><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">客服</span></strong></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">客服QQ：1164099224</span></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">邮 &nbsp;箱：hi@1ysmg.com</span></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\"><br/></span></p><p><strong><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">商务合作</span></strong></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">&nbsp; &nbsp; 一元神马购诚邀各品牌供应商与我们达成商务合作，推动互联网的发展及网购消费模式的多样化，共创中国电子商务的美好明天。</span></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">&nbsp; &nbsp; 欢迎各企业在流量、广告、资源、活动等方面展开合作，我们将热忱对待。</span><br/></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">&nbsp; &nbsp; 邮 &nbsp;箱：<span style=\"font-family: 宋体, SimSun; font-size: 14px;\">hi@1ysmg.com</span></span><br/></p><p><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">&nbsp; &nbsp; 客服 QQ：<span style=\"font-family: 宋体, SimSun; font-size: 14px;\"><span style=\"font-family: 宋体, SimSun; font-size: 14px;\">1164099224</span></span></span></p><p><br/></p>', '23', '1', '1451125011');

-- ----------------------------
-- Table structure for `go_auth_list`
-- ----------------------------
DROP TABLE IF EXISTS `go_auth_list`;
CREATE TABLE `go_auth_list` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pid` int(10) NOT NULL COMMENT '父级id',
  `name` varchar(100) DEFAULT NULL COMMENT '名称',
  `code_one` varchar(100) DEFAULT NULL,
  `code_two` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of go_auth_list
-- ----------------------------
INSERT INTO `go_auth_list` VALUES ('1', '0', '系统设置', 'setting', null);
INSERT INTO `go_auth_list` VALUES ('2', '0', '内容管理', 'content', null);
INSERT INTO `go_auth_list` VALUES ('3', '0', '商品管理', 'shop', null);
INSERT INTO `go_auth_list` VALUES ('4', '0', '用户管理', 'user', null);
INSERT INTO `go_auth_list` VALUES ('5', '0', '数据统计', 'datas', null);
INSERT INTO `go_auth_list` VALUES ('7', '0', '云应用', 'yunapp', null);
INSERT INTO `go_auth_list` VALUES ('8', '0', 'APP管理', 'applist', null);
INSERT INTO `go_auth_list` VALUES ('9', '0', '活动管理', 'activelist', null);
INSERT INTO `go_auth_list` VALUES ('10', '0', '订单管理', 'order', '');

-- ----------------------------
-- Table structure for `go_brand`
-- ----------------------------
DROP TABLE IF EXISTS `go_brand`;
CREATE TABLE `go_brand` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `cateid` varchar(255) DEFAULT NULL COMMENT '所属栏目ID',
  `status` char(1) DEFAULT 'Y' COMMENT '显示隐藏',
  `name` varchar(255) DEFAULT NULL,
  `thumb` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `order` int(11) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `status` (`status`),
  KEY `order` (`order`)
) ENGINE=InnoDB AUTO_INCREMENT=147 DEFAULT CHARSET=utf8 COMMENT='品牌表';

-- ----------------------------
-- Records of go_brand
-- ----------------------------
INSERT INTO `go_brand` VALUES ('2', '13,5,14', 'Y', '苹果', null, null, '100');
INSERT INTO `go_brand` VALUES ('5', '6', 'Y', '佳能', null, null, '2');
INSERT INTO `go_brand` VALUES ('8', '15,14,6,5', 'Y', '小米', null, null, '1');
INSERT INTO `go_brand` VALUES ('12', '5,14', 'Y', '三星', null, null, '1');
INSERT INTO `go_brand` VALUES ('13', '14,13', 'Y', '微软', null, null, '1');
INSERT INTO `go_brand` VALUES ('15', '13', 'Y', '戴尔', null, null, '1');
INSERT INTO `go_brand` VALUES ('19', '14', 'Y', '周大福', null, null, '1');
INSERT INTO `go_brand` VALUES ('23', '14', 'Y', '大疆', null, null, '2');
INSERT INTO `go_brand` VALUES ('26', '17', 'Y', '蔻驰', null, null, '2');
INSERT INTO `go_brand` VALUES ('27', '6', 'Y', 'LG', null, null, '1');
INSERT INTO `go_brand` VALUES ('28', '6', 'Y', '索尼', null, null, '1');
INSERT INTO `go_brand` VALUES ('29', '17,18', 'Y', '阿玛尼', null, null, '1');
INSERT INTO `go_brand` VALUES ('30', '14,15,6', 'Y', '乐视', null, null, '1');
INSERT INTO `go_brand` VALUES ('31', '15', 'Y', '飞利浦', null, null, '1');
INSERT INTO `go_brand` VALUES ('32', '15', 'Y', '科沃斯', null, null, '1');
INSERT INTO `go_brand` VALUES ('33', '15', 'Y', '爱尔威', null, null, '1');
INSERT INTO `go_brand` VALUES ('34', '15', 'Y', '美的', null, null, '1');
INSERT INTO `go_brand` VALUES ('35', '17', 'Y', '雅诗兰黛', null, null, '1');
INSERT INTO `go_brand` VALUES ('36', '18', 'Y', '大众', null, null, '1');
INSERT INTO `go_brand` VALUES ('37', '18', 'Y', '福特', null, null, '1');
INSERT INTO `go_brand` VALUES ('38', '15', 'Y', '伊吉康', null, null, '1');
INSERT INTO `go_brand` VALUES ('39', '15', 'Y', '爱玛', null, null, '1');
INSERT INTO `go_brand` VALUES ('40', '6', 'Y', '魔声', null, null, '1');
INSERT INTO `go_brand` VALUES ('41', '6', 'Y', '漫步者', null, null, '1');
INSERT INTO `go_brand` VALUES ('42', '5', 'Y', 'Kindle', null, null, '1');
INSERT INTO `go_brand` VALUES ('43', '18', 'Y', '中国黄金', null, null, '1');
INSERT INTO `go_brand` VALUES ('44', '17', 'Y', 'MK', null, null, '1');
INSERT INTO `go_brand` VALUES ('47', '17', 'Y', '飞亚达', null, null, '1');
INSERT INTO `go_brand` VALUES ('48', '18', 'Y', '话费充值', null, null, '1');
INSERT INTO `go_brand` VALUES ('49', '6', 'Y', '卡西欧', null, null, '2');
INSERT INTO `go_brand` VALUES ('50', '14', 'Y', 'ninebot', null, null, '1');
INSERT INTO `go_brand` VALUES ('51', '12', 'Y', '三只松鼠', null, null, '1');
INSERT INTO `go_brand` VALUES ('52', '12', 'Y', '百草味', null, null, '1');
INSERT INTO `go_brand` VALUES ('53', '12', 'Y', '徐福记', null, null, '1');
INSERT INTO `go_brand` VALUES ('54', '12', 'Y', '丹麦蓝罐', null, null, '1');
INSERT INTO `go_brand` VALUES ('55', '12', 'Y', '费列罗', null, null, '1');
INSERT INTO `go_brand` VALUES ('56', '15', 'Y', '康佳', null, null, '5');
INSERT INTO `go_brand` VALUES ('57', '17', 'Y', '雅漾', null, null, '1');
INSERT INTO `go_brand` VALUES ('58', '17', 'Y', 'CK', null, null, '1');
INSERT INTO `go_brand` VALUES ('59', '17', 'Y', '纪梵希', null, null, '1');
INSERT INTO `go_brand` VALUES ('60', '17', 'Y', 'SK-II', null, null, '1');
INSERT INTO `go_brand` VALUES ('61', '13', 'Y', '雷柏', null, null, '1');
INSERT INTO `go_brand` VALUES ('62', '14', 'Y', '坚果', null, null, '1');
INSERT INTO `go_brand` VALUES ('63', '14', 'Y', '美佳朗', null, null, '1');
INSERT INTO `go_brand` VALUES ('64', '14', 'Y', '360', null, null, '1');
INSERT INTO `go_brand` VALUES ('65', '15', 'Y', '惠人', null, null, '1');
INSERT INTO `go_brand` VALUES ('66', '15', 'Y', '象印', null, null, '1');
INSERT INTO `go_brand` VALUES ('67', '6', 'Y', '乐途客', null, null, '1');
INSERT INTO `go_brand` VALUES ('68', '14', 'Y', '有品', null, null, '1');
INSERT INTO `go_brand` VALUES ('69', '6', 'Y', '叮咚', null, null, '1');
INSERT INTO `go_brand` VALUES ('70', '14', 'Y', '欧格森', null, null, '1');
INSERT INTO `go_brand` VALUES ('71', '15', 'Y', '公牛', null, null, '1');
INSERT INTO `go_brand` VALUES ('72', '17', 'Y', '凯特·丝蓓', null, null, '1');
INSERT INTO `go_brand` VALUES ('73', '18', 'Y', '京东E卡', null, null, '1');
INSERT INTO `go_brand` VALUES ('74', '5', 'Y', '魅族', null, null, '1');
INSERT INTO `go_brand` VALUES ('75', '14', 'Y', '耐尔金', null, null, '1');
INSERT INTO `go_brand` VALUES ('76', '13', 'Y', '华硕', null, null, '1');
INSERT INTO `go_brand` VALUES ('77', '14', 'Y', 'ZIPPO', null, null, '1');
INSERT INTO `go_brand` VALUES ('78', '5', 'Y', '美图', null, null, '1');
INSERT INTO `go_brand` VALUES ('79', '5', 'Y', 'OPPO', null, null, '1');
INSERT INTO `go_brand` VALUES ('80', '5', 'Y', '乐视', null, null, '1');
INSERT INTO `go_brand` VALUES ('81', '5,6', 'Y', '华为', null, null, '1');
INSERT INTO `go_brand` VALUES ('82', '18', 'Y', '支付宝', null, null, '1');
INSERT INTO `go_brand` VALUES ('83', '13', 'Y', '罗技', null, null, '1');
INSERT INTO `go_brand` VALUES ('84', '18,17', 'Y', '天梭', null, null, '1');
INSERT INTO `go_brand` VALUES ('85', '15', 'Y', 'SKG', null, null, '1');
INSERT INTO `go_brand` VALUES ('86', '15', 'Y', '沁园', null, null, '1');
INSERT INTO `go_brand` VALUES ('87', '15', 'Y', '小熊', null, null, '1');
INSERT INTO `go_brand` VALUES ('88', '5', 'Y', '努比亚', null, null, '1');
INSERT INTO `go_brand` VALUES ('89', '5', 'Y', '锤子', null, null, '1');
INSERT INTO `go_brand` VALUES ('90', '13', 'Y', '联想', null, null, '1');
INSERT INTO `go_brand` VALUES ('91', '6', 'Y', '富士', null, null, '1');
INSERT INTO `go_brand` VALUES ('92', '14', 'Y', 'WowWee', null, null, '1');
INSERT INTO `go_brand` VALUES ('93', '6', 'Y', '尼康', null, null, '1');
INSERT INTO `go_brand` VALUES ('94', '14', 'Y', '爱国者', null, null, '1');
INSERT INTO `go_brand` VALUES ('95', '13', 'Y', '樱桃', null, null, '1');
INSERT INTO `go_brand` VALUES ('96', '13', 'Y', '雷蛇', null, null, '1');
INSERT INTO `go_brand` VALUES ('97', '15', 'Y', '微囧贡米', null, null, '1');
INSERT INTO `go_brand` VALUES ('98', '12', 'Y', '微囧梨公子', null, null, '1');
INSERT INTO `go_brand` VALUES ('99', '12', 'Y', '高丽刺果GRARIVA', null, null, '1');
INSERT INTO `go_brand` VALUES ('100', '17', 'Y', '沙棘美肤霜', null, null, '1');
INSERT INTO `go_brand` VALUES ('101', '12', 'Y', '微囧菠菠小姐', null, null, '1');
INSERT INTO `go_brand` VALUES ('102', '13', 'Y', '宏碁', null, null, '1');
INSERT INTO `go_brand` VALUES ('103', '6', 'Y', 'Apple iPod Shuffle', null, null, '12222');
INSERT INTO `go_brand` VALUES ('104', '6', 'Y', 'Beats Studio Wireless', null, null, '1');
INSERT INTO `go_brand` VALUES ('105', '6', 'Y', '坚果', null, null, '1');
INSERT INTO `go_brand` VALUES ('106', '6', 'Y', '风格派 ', null, null, '1');
INSERT INTO `go_brand` VALUES ('107', '12', 'Y', '茅台', null, null, '1');
INSERT INTO `go_brand` VALUES ('108', '12', 'Y', '马爹利', null, null, '1');
INSERT INTO `go_brand` VALUES ('109', '12', 'Y', '百草味', null, null, '1');
INSERT INTO `go_brand` VALUES ('110', '12', 'Y', '洋河', null, null, '1');
INSERT INTO `go_brand` VALUES ('111', '12', 'Y', ' 剑南春', null, null, '1');
INSERT INTO `go_brand` VALUES ('112', '12', 'Y', '百威', null, null, '1');
INSERT INTO `go_brand` VALUES ('113', '12', 'Y', '科罗娜', null, null, '1');
INSERT INTO `go_brand` VALUES ('114', '12', 'Y', '苹果', null, null, '1');
INSERT INTO `go_brand` VALUES ('115', '12', 'Y', '福佳', null, null, '1');
INSERT INTO `go_brand` VALUES ('116', '12', 'Y', '五粮液', null, null, '1');
INSERT INTO `go_brand` VALUES ('117', '12', 'Y', '英伯伦 ', null, null, '1');
INSERT INTO `go_brand` VALUES ('118', '12', 'Y', '一农', null, null, '1');
INSERT INTO `go_brand` VALUES ('119', '12', 'Y', '奇华', null, null, '1');
INSERT INTO `go_brand` VALUES ('120', '12', 'Y', '尊尼获加', null, null, '1');
INSERT INTO `go_brand` VALUES ('121', '12', 'Y', '酩悦 粉红香槟 ', null, null, '1');
INSERT INTO `go_brand` VALUES ('122', '13', 'Y', '外星人', null, null, '1');
INSERT INTO `go_brand` VALUES ('123', '13', 'Y', '西部数据', null, null, '1');
INSERT INTO `go_brand` VALUES ('124', '13', 'Y', '罗技(Logitech) ', null, null, '1');
INSERT INTO `go_brand` VALUES ('125', '13', 'Y', '微插座', null, null, '1');
INSERT INTO `go_brand` VALUES ('126', '13', 'Y', '希捷', null, null, '1');
INSERT INTO `go_brand` VALUES ('127', '13', 'Y', '金士顿', null, null, '1');
INSERT INTO `go_brand` VALUES ('128', '13', 'Y', '闪迪 sandisk', null, null, '1');
INSERT INTO `go_brand` VALUES ('129', '13', 'Y', '特洛克', null, null, '1');
INSERT INTO `go_brand` VALUES ('130', '13', 'Y', '罗尔思 (ROSS) ', null, null, '1');
INSERT INTO `go_brand` VALUES ('131', '13', 'Y', '荷兰 Allocacoc', null, null, '1');
INSERT INTO `go_brand` VALUES ('132', '13', 'Y', '华硕(ASUS) ', null, null, '1');
INSERT INTO `go_brand` VALUES ('133', '18', 'Y', '西门子', null, null, '1');
INSERT INTO `go_brand` VALUES ('134', '18', 'Y', '冈本', null, null, '1');
INSERT INTO `go_brand` VALUES ('135', '18', 'Y', 'ZIPPO火机', null, null, '1');
INSERT INTO `go_brand` VALUES ('136', '18', 'Y', '“法拉利”儿童电动车 ', null, null, '1');
INSERT INTO `go_brand` VALUES ('137', '18', 'Y', '三菱重工', null, null, '1');
INSERT INTO `go_brand` VALUES ('138', '18', 'Y', '兰博基尼', null, null, '1');
INSERT INTO `go_brand` VALUES ('139', '14', 'Y', '亿觅 emie ', null, null, '1');
INSERT INTO `go_brand` VALUES ('140', '17', 'Y', '周生生 ', null, null, '1');
INSERT INTO `go_brand` VALUES ('141', '17', 'Y', '韩国777修甲套装', null, null, '1');
INSERT INTO `go_brand` VALUES ('142', '15', 'Y', '康夫', null, null, '1');
INSERT INTO `go_brand` VALUES ('143', '17', 'Y', '苏泊尔', null, null, '1');
INSERT INTO `go_brand` VALUES ('144', '15', 'Y', '苏泊尔', null, null, '1');
INSERT INTO `go_brand` VALUES ('145', '15', 'Y', '美的', null, null, '1');
INSERT INTO `go_brand` VALUES ('146', '18', 'Y', '话费充值', null, null, '1');

-- ----------------------------
-- Table structure for `go_caches`
-- ----------------------------
DROP TABLE IF EXISTS `go_caches`;
CREATE TABLE `go_caches` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `key` varchar(100) NOT NULL,
  `value` text,
  PRIMARY KEY (`id`),
  KEY `key` (`key`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of go_caches
-- ----------------------------
INSERT INTO `go_caches` VALUES ('1', 'member_name_key', 'admin,administrator,一元神马购,神马购');
INSERT INTO `go_caches` VALUES ('2', 'shopcodes_table', '1');
INSERT INTO `go_caches` VALUES ('3', 'goods_count_num', '93352');
INSERT INTO `go_caches` VALUES ('4', 'template_mobile_reg', '你好,你的注册验证码是:000000 ');
INSERT INTO `go_caches` VALUES ('5', 'template_mobile_shop', '恭喜您一元神马购用户！您购买的商品：{商品名称} 已获奖,获奖神马购码为：00000000 请尽快登录确认收货地址，7日内未确认地址奖品将失效！');
INSERT INTO `go_caches` VALUES ('6', 'template_email_reg', '你好,请在24小时内激活注册邮件，点击连接激活邮件：{地址}');
INSERT INTO `go_caches` VALUES ('7', 'template_email_shop', '恭喜您：{用户名}，你在一元神马购买的商品：{商品名称} 已获奖，获奖码是:{中奖码}');
INSERT INTO `go_caches` VALUES ('8', 'pay_bank_type', 'yeepay');
INSERT INTO `go_caches` VALUES ('9', 'template_mobile_pwd', '你好,你现在正在找回密码，你的验证码是000000。');
INSERT INTO `go_caches` VALUES ('10', 'template_email_pwd', '请在24小时内激活邮件，点击连接激活邮件：{地址}');
INSERT INTO `go_caches` VALUES ('11', 'ssclottery', '1');
INSERT INTO `go_caches` VALUES ('12', 'template_mobile_change', '您的验证码是：【000000】。请不要把验证码泄露给其他人。');
INSERT INTO `go_caches` VALUES ('13', 'is_auto_buy', '0');

-- ----------------------------
-- Table structure for `go_category`
-- ----------------------------
DROP TABLE IF EXISTS `go_category`;
CREATE TABLE `go_category` (
  `cateid` smallint(6) unsigned NOT NULL AUTO_INCREMENT COMMENT '栏目id',
  `parentid` smallint(6) DEFAULT NULL COMMENT '父ID',
  `channel` tinyint(4) NOT NULL DEFAULT '0',
  `model` tinyint(1) DEFAULT NULL COMMENT '栏目模型',
  `name` varchar(255) DEFAULT NULL COMMENT '栏目名称',
  `catdir` char(20) DEFAULT NULL COMMENT '英文名',
  `url` varchar(255) DEFAULT NULL,
  `info` text,
  `order` smallint(6) unsigned DEFAULT '1' COMMENT '排序',
  PRIMARY KEY (`cateid`),
  KEY `name` (`name`),
  KEY `order` (`order`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8 COMMENT='栏目表';

-- ----------------------------
-- Records of go_category
-- ----------------------------
INSERT INTO `go_category` VALUES ('1', '0', '0', '2', '帮助', 'help', '', 'a:7:{s:5:\"thumb\";s:0:\"\";s:3:\"des\";s:0:\"\";s:8:\"template\";N;s:7:\"content\";N;s:10:\"meta_title\";s:0:\"\";s:13:\"meta_keywords\";s:0:\"\";s:16:\"meta_description\";s:0:\"\";}', '1');
INSERT INTO `go_category` VALUES ('2', '1', '0', '2', '新手指南', 'xinshouzhinan', '', 'a:9:{s:5:\"thumb\";s:35:\"cateimg/20151226/60914681121853.png\";s:3:\"des\";s:0:\"\";s:8:\"template\";s:0:\"\";s:7:\"content\";s:0:\"\";s:10:\"meta_title\";s:0:\"\";s:13:\"meta_keywords\";s:0:\"\";s:16:\"meta_description\";s:0:\"\";s:13:\"template_list\";s:0:\"\";s:13:\"template_show\";s:0:\"\";}', '1');
INSERT INTO `go_category` VALUES ('3', '1', '0', '2', '神马购保障', 'zhonglebaozhang', '', 'a:9:{s:5:\"thumb\";s:35:\"cateimg/20151226/37088713121872.png\";s:3:\"des\";s:30:\"司法所发射点发射得分\";s:8:\"template\";s:0:\"\";s:7:\"content\";s:0:\"\";s:10:\"meta_title\";s:0:\"\";s:13:\"meta_keywords\";s:0:\"\";s:16:\"meta_description\";s:0:\"\";s:13:\"template_list\";s:0:\"\";s:13:\"template_show\";s:0:\"\";}', '1');
INSERT INTO `go_category` VALUES ('4', '1', '0', '2', '商品配送', 'peisong', '', 'a:9:{s:5:\"thumb\";s:35:\"cateimg/20151226/73013144121890.png\";s:3:\"des\";s:0:\"\";s:8:\"template\";s:0:\"\";s:7:\"content\";s:0:\"\";s:10:\"meta_title\";s:0:\"\";s:13:\"meta_keywords\";s:0:\"\";s:16:\"meta_description\";s:0:\"\";s:13:\"template_list\";s:0:\"\";s:13:\"template_show\";s:0:\"\";}', '1');
INSERT INTO `go_category` VALUES ('5', '0', '0', '1', '手机平板', 'shouji', '', 'a:9:{s:5:\"thumb\";s:35:\"cateimg/20160102/47732961704255.png\";s:3:\"des\";s:0:\"\";s:8:\"template\";s:0:\"\";s:7:\"content\";s:0:\"\";s:10:\"meta_title\";s:0:\"\";s:13:\"meta_keywords\";s:0:\"\";s:16:\"meta_description\";s:0:\"\";s:13:\"template_list\";s:0:\"\";s:13:\"template_show\";s:0:\"\";}', '6');
INSERT INTO `go_category` VALUES ('6', '0', '0', '1', '数码影音', 'shuma', '', 'a:9:{s:5:\"thumb\";s:35:\"cateimg/20160102/44532210704283.png\";s:3:\"des\";s:0:\"\";s:8:\"template\";s:0:\"\";s:7:\"content\";s:0:\"\";s:10:\"meta_title\";s:0:\"\";s:13:\"meta_keywords\";s:0:\"\";s:16:\"meta_description\";s:0:\"\";s:13:\"template_list\";s:0:\"\";s:13:\"template_show\";s:0:\"\";}', '4');
INSERT INTO `go_category` VALUES ('7', '0', '0', '-1', '新手指南', 'newbie', '', 'a:7:{s:5:\"thumb\";s:0:\"\";s:3:\"des\";s:0:\"\";s:8:\"template\";s:22:\"single_web.newbie.html\";s:7:\"content\";s:0:\"\";s:10:\"meta_title\";s:0:\"\";s:13:\"meta_keywords\";s:0:\"\";s:16:\"meta_description\";s:0:\"\";}', '1');
INSERT INTO `go_category` VALUES ('8', '0', '0', '-1', '合作专区', 'business', '', 'a:7:{s:5:\"thumb\";s:0:\"\";s:3:\"des\";s:0:\"\";s:8:\"template\";s:24:\"single_web.business.html\";s:7:\"content\";s:24:\"PHA+MTExMTE8YnIvPjwvcD4=\";s:10:\"meta_title\";s:0:\"\";s:13:\"meta_keywords\";s:0:\"\";s:16:\"meta_description\";s:0:\"\";}', '1');
INSERT INTO `go_category` VALUES ('9', '0', '0', '-1', '公益基金', 'fund', '', 'a:7:{s:5:\"thumb\";s:0:\"\";s:3:\"des\";s:0:\"\";s:8:\"template\";s:20:\"single_web.fund.html\";s:7:\"content\";s:28:\"<p>输入栏目内容...</p>\";s:10:\"meta_title\";s:0:\"\";s:13:\"meta_keywords\";s:0:\"\";s:16:\"meta_description\";s:0:\"\";}', '1');
INSERT INTO `go_category` VALUES ('12', '0', '0', '1', '美食天地', 'lingshi', '', 'a:9:{s:5:\"thumb\";s:35:\"cateimg/20160106/14951845048961.png\";s:3:\"des\";s:12:\"零食天地\";s:8:\"template\";s:0:\"\";s:7:\"content\";s:0:\"\";s:10:\"meta_title\";s:12:\"数码相机\";s:13:\"meta_keywords\";s:12:\"数码相机\";s:16:\"meta_description\";s:12:\"数码相机\";s:13:\"template_list\";s:0:\"\";s:13:\"template_show\";s:0:\"\";}', '3');
INSERT INTO `go_category` VALUES ('13', '0', '0', '1', '电脑办公', 'diannao', '', 'a:9:{s:5:\"thumb\";s:35:\"cateimg/20160102/23460986704269.png\";s:3:\"des\";s:6:\"电脑\";s:8:\"template\";s:0:\"\";s:7:\"content\";s:0:\"\";s:10:\"meta_title\";s:6:\"电脑\";s:13:\"meta_keywords\";s:6:\"电脑\";s:16:\"meta_description\";s:6:\"电脑\";s:13:\"template_list\";s:0:\"\";s:13:\"template_show\";s:0:\"\";}', '5');
INSERT INTO `go_category` VALUES ('14', '0', '0', '1', '潮流新品', 'chaoliu', '', 'a:9:{s:5:\"thumb\";s:35:\"cateimg/20160102/13903483704324.png\";s:3:\"des\";s:12:\"潮流新品\";s:8:\"template\";s:0:\"\";s:7:\"content\";s:0:\"\";s:10:\"meta_title\";s:12:\"钟表首饰\";s:13:\"meta_keywords\";s:12:\"钟表首饰\";s:16:\"meta_description\";s:12:\"钟表首饰\";s:13:\"template_list\";s:0:\"\";s:13:\"template_show\";s:0:\"\";}', '2');
INSERT INTO `go_category` VALUES ('15', '0', '0', '1', '家居生活', 'jiaju', '', 'a:9:{s:5:\"thumb\";s:35:\"cateimg/20160102/54049511704341.png\";s:3:\"des\";s:6:\"家居\";s:8:\"template\";s:0:\"\";s:7:\"content\";s:0:\"\";s:10:\"meta_title\";s:12:\"其他商品\";s:13:\"meta_keywords\";s:12:\"其他商品\";s:16:\"meta_description\";s:12:\"其他商品\";s:13:\"template_list\";s:0:\"\";s:13:\"template_show\";s:0:\"\";}', '1');
INSERT INTO `go_category` VALUES ('16', '1', '0', '2', '关于我们', 'about', '', 'a:9:{s:5:\"thumb\";s:35:\"cateimg/20151226/52062376121908.png\";s:3:\"des\";s:0:\"\";s:8:\"template\";s:0:\"\";s:7:\"content\";s:0:\"\";s:10:\"meta_title\";s:0:\"\";s:13:\"meta_keywords\";s:0:\"\";s:16:\"meta_description\";s:0:\"\";s:13:\"template_list\";s:0:\"\";s:13:\"template_show\";s:0:\"\";}', '1');
INSERT INTO `go_category` VALUES ('17', '0', '0', '1', '女性时尚', 'nvxing', '', 'a:9:{s:5:\"thumb\";s:35:\"cateimg/20160102/64669933704353.png\";s:3:\"des\";s:6:\"女性\";s:8:\"template\";s:0:\"\";s:7:\"content\";s:0:\"\";s:10:\"meta_title\";s:0:\"\";s:13:\"meta_keywords\";s:0:\"\";s:16:\"meta_description\";s:0:\"\";s:13:\"template_list\";s:20:\"goods_list.list.html\";s:13:\"template_show\";s:20:\"goods_show.show.html\";}', '1');
INSERT INTO `go_category` VALUES ('18', '0', '0', '1', '其它商品', 'qita', '', 'a:9:{s:5:\"thumb\";s:35:\"cateimg/20160102/94723960704368.png\";s:3:\"des\";s:6:\"其它\";s:8:\"template\";s:0:\"\";s:7:\"content\";s:0:\"\";s:10:\"meta_title\";s:0:\"\";s:13:\"meta_keywords\";s:0:\"\";s:16:\"meta_description\";s:0:\"\";s:13:\"template_list\";s:20:\"goods_list.list.html\";s:13:\"template_show\";s:20:\"goods_show.show.html\";}', '1');

-- ----------------------------
-- Table structure for `go_config`
-- ----------------------------
DROP TABLE IF EXISTS `go_config`;
CREATE TABLE `go_config` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL,
  `value` mediumtext,
  `zhushi` text,
  PRIMARY KEY (`id`),
  KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of go_config
-- ----------------------------
INSERT INTO `go_config` VALUES ('1', 'web_name', '一元云购', '网站名');
INSERT INTO `go_config` VALUES ('2', 'web_key', '1元,一元,一元云购,云购,1元云购,一元云购,1元云购,一元云购,1元云购官网,一元云购官网,云购奇兵,云购,一元购,1元购,云购商城', '网站关键字');
INSERT INTO `go_config` VALUES ('3', 'web_des', '一元云购是一个引入众筹理念的新型电子商务平台，手机电脑、数码科技、潮流新品、家居电器，只需1元即有机会获取，凭借技术实力和算法公平，确保100%公平公正、100%正品保障，引领集娱乐、购物于一体的网购潮流。', '网站介绍');
INSERT INTO `go_config` VALUES ('4', 'web_path', '', '网站地址');
INSERT INTO `go_config` VALUES ('5', 'templates_edit', '1', '是否允许在线编辑模板');
INSERT INTO `go_config` VALUES ('6', 'templates_name', 'templet2', '当前模板方案');
INSERT INTO `go_config` VALUES ('7', 'charset', 'utf-8', '网站字符集');
INSERT INTO `go_config` VALUES ('8', 'timezone', 'Asia/Shanghai', '网站时区');
INSERT INTO `go_config` VALUES ('9', 'error', '1', '1、保存错误日志到 cache/error_log.php | 0、在页面直接显示');
INSERT INTO `go_config` VALUES ('10', 'gzip', '0', '是否Gzip压缩后输出,服务器没有gzip请不要启用');
INSERT INTO `go_config` VALUES ('11', 'lang', 'zh-cn', '网站语言包');
INSERT INTO `go_config` VALUES ('12', 'cache', '3600', '默认缓存时间');
INSERT INTO `go_config` VALUES ('13', 'web_off', '1', '网站是否开启');
INSERT INTO `go_config` VALUES ('14', 'web_off_text', '网站维护中....', '关闭原因');
INSERT INTO `go_config` VALUES ('15', 'tablepre', 'QCNf', null);
INSERT INTO `go_config` VALUES ('16', 'index_name', 'index.php', '隐藏首页文件名');
INSERT INTO `go_config` VALUES ('17', 'expstr', '/', 'url分隔符号');
INSERT INTO `go_config` VALUES ('18', 'admindir', 'admin', '后台管理文件夹');
INSERT INTO `go_config` VALUES ('19', 'qq', '888888', 'qq');
INSERT INTO `go_config` VALUES ('20', 'cell', '888888', '联系电话');
INSERT INTO `go_config` VALUES ('21', 'web_logo', 'banner/20160506/79210811511273.png', 'logo');
INSERT INTO `go_config` VALUES ('22', 'web_copyright', 'Copyright © 2015 - 2016, 粤ICP备15105062号-2', '版权');
INSERT INTO `go_config` VALUES ('23', 'web_name_two', '一元云购', '短网站名');
INSERT INTO `go_config` VALUES ('24', 'qq_qun', '', 'QQ群');
INSERT INTO `go_config` VALUES ('25', 'goods_end_time', '180', '开奖动画秒数(单位秒)');

-- ----------------------------
-- Table structure for `go_data_statistics`
-- ----------------------------
DROP TABLE IF EXISTS `go_data_statistics`;
CREATE TABLE `go_data_statistics` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `s_id` int(10) DEFAULT NULL,
  `s_name` varchar(100) DEFAULT NULL,
  `s_ename` varchar(200) DEFAULT NULL,
  `s_words` varchar(100) DEFAULT NULL,
  `uid` int(10) DEFAULT NULL,
  `addtime` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `s_id` (`s_id`),
  KEY `s_words` (`s_words`),
  KEY `addtime` (`addtime`),
  KEY `s_ename` (`s_ename`),
  KEY `uid` (`uid`)
) ENGINE=InnoDB AUTO_INCREMENT=4254 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of go_data_statistics
-- ----------------------------
INSERT INTO `go_data_statistics` VALUES ('3', '4', '百度推广', 'zlbdtg', 'SEM00000117', '3868', '1452320911');
INSERT INTO `go_data_statistics` VALUES ('4', '4', '百度推广', 'zlbdtg', 'SEM00000005', '3877', '1452321480');
INSERT INTO `go_data_statistics` VALUES ('5', '4', '百度推广', 'zlbdtg', 'SEM00000330', '3885', '1452321821');
INSERT INTO `go_data_statistics` VALUES ('6', '4', '百度推广', 'zlbdtg', 'SEM00000121', '3886', '1452321890');
INSERT INTO `go_data_statistics` VALUES ('7', '4', '百度推广', 'zlbdtg', 'SEM00000115', '3887', '1452321893');
INSERT INTO `go_data_statistics` VALUES ('8', '4', '百度推广', 'zlbdtg', 'SEM00000117', '3913', '1452323483');
INSERT INTO `go_data_statistics` VALUES ('9', '4', '百度推广', 'zlbdtg', 'SEM00000121', '3918', '1452323842');
INSERT INTO `go_data_statistics` VALUES ('10', '4', '百度推广', 'zlbdtg', 'SEM00000115', '3926', '1452324454');
INSERT INTO `go_data_statistics` VALUES ('11', '4', '百度推广', 'zlbdtg', 'SEM00000005', '3930', '1452324705');
INSERT INTO `go_data_statistics` VALUES ('12', '4', '百度推广', 'zlbdtg', 'SEM00000115', '3935', '1452324848');
INSERT INTO `go_data_statistics` VALUES ('13', '4', '百度推广', 'zlbdtg', 'SEM00000135', '3940', '1452325376');
INSERT INTO `go_data_statistics` VALUES ('14', '4', '百度推广', 'zlbdtg', 'SEM00000121', '3960', '1452326171');
INSERT INTO `go_data_statistics` VALUES ('15', '4', '百度推广', 'zlbdtg', 'SEM00000121', '3990', '1452327377');
INSERT INTO `go_data_statistics` VALUES ('16', '4', '百度推广', 'zlbdtg', 'YDSEM00000002', '3997', '1452327714');
INSERT INTO `go_data_statistics` VALUES ('17', '4', '百度推广', 'zlbdtg', 'SEM00000309', '4010', '1452328143');
INSERT INTO `go_data_statistics` VALUES ('18', '4', '百度推广', 'zlbdtg', 'SEM00000122', '4022', '1452328708');
INSERT INTO `go_data_statistics` VALUES ('19', '4', '百度推广', 'zlbdtg', 'SEM00000115', '4028', '1452329064');
INSERT INTO `go_data_statistics` VALUES ('20', '4', '百度推广', 'zlbdtg', 'SEM00000122', '4030', '1452329079');
INSERT INTO `go_data_statistics` VALUES ('21', '4', '百度推广', 'zlbdtg', 'SEM00000115', '4032', '1452329150');
INSERT INTO `go_data_statistics` VALUES ('22', '4', '百度推广', 'zlbdtg', 'SEM00000320', '4053', '1452330269');
INSERT INTO `go_data_statistics` VALUES ('23', '4', '百度推广', 'zlbdtg', 'SEM00000122', '4062', '1452330719');
INSERT INTO `go_data_statistics` VALUES ('24', '4', '百度推广', 'zlbdtg', 'SEM00000115', '4077', '1452331056');
INSERT INTO `go_data_statistics` VALUES ('25', '4', '百度推广', 'zlbdtg', 'SEM00000122', '4089', '1452331536');
INSERT INTO `go_data_statistics` VALUES ('26', '4', '百度推广', 'zlbdtg', 'SEM00000163', '4108', '1452332093');
INSERT INTO `go_data_statistics` VALUES ('27', '4', '百度推广', 'zlbdtg', 'SEM00000142', '4243', '1452338501');
INSERT INTO `go_data_statistics` VALUES ('28', '4', '百度推广', 'zlbdtg', 'SEM00000396', '4263', '1452339426');
INSERT INTO `go_data_statistics` VALUES ('29', '4', '百度推广', 'zlbdtg', 'SEM00000163', '4343', '1452342849');
INSERT INTO `go_data_statistics` VALUES ('30', '4', '百度推广', 'zlbdtg', 'SEM00000244', '4359', '1452343377');
INSERT INTO `go_data_statistics` VALUES ('31', '4', '百度推广', 'zlbdtg', 'SEM00000163', '4416', '1452345819');
INSERT INTO `go_data_statistics` VALUES ('32', '4', '百度推广', 'zlbdtg', 'SEM00000163', '4424', '1452346141');
INSERT INTO `go_data_statistics` VALUES ('33', '4', '百度推广', 'zlbdtg', 'SEM00000111', '4485', '1452348486');
INSERT INTO `go_data_statistics` VALUES ('34', '4', '百度推广', 'zlbdtg', 'SEM00000141', '4497', '1452348825');
INSERT INTO `go_data_statistics` VALUES ('35', '4', '百度推广', 'zlbdtg', 'SEM00000094', '4507', '1452349299');
INSERT INTO `go_data_statistics` VALUES ('36', '4', '百度推广', 'zlbdtg', 'SEM00000097', '4543', '1452350842');
INSERT INTO `go_data_statistics` VALUES ('37', '4', '百度推广', 'zlbdtg', 'SEM00000246', '4555', '1452351335');
INSERT INTO `go_data_statistics` VALUES ('38', '4', '百度推广', 'zlbdtg', 'SEM00000113', '4566', '1452352085');
INSERT INTO `go_data_statistics` VALUES ('39', '4', '百度推广', 'zlbdtg', 'SEM00000097', '4571', '1452352218');
INSERT INTO `go_data_statistics` VALUES ('40', '4', '百度推广', 'zlbdtg', 'SEM00000196', '4601', '1452353996');
INSERT INTO `go_data_statistics` VALUES ('41', '4', '百度推广', 'zlbdtg', 'SEM00000122', '4653', '1452358506');
INSERT INTO `go_data_statistics` VALUES ('42', '4', '百度推广', 'zlbdtg', 'SEM00000114', '4660', '1452359161');
INSERT INTO `go_data_statistics` VALUES ('43', '4', '百度推广', 'zlbdtg', 'SEM00000115', '4666', '1452359981');
INSERT INTO `go_data_statistics` VALUES ('44', '4', '百度推广', 'zlbdtg', 'SEM00000118', '4673', '1452361040');
INSERT INTO `go_data_statistics` VALUES ('45', '4', '百度推广', 'zlbdtg', 'SEM00000118', '4676', '1452361207');
INSERT INTO `go_data_statistics` VALUES ('46', '4', '百度推广', 'zlbdtg', 'SEM00000118', '4677', '1452361223');
INSERT INTO `go_data_statistics` VALUES ('47', '4', '百度推广', 'zlbdtg', 'SEM00000118', '4678', '1452361236');
INSERT INTO `go_data_statistics` VALUES ('48', '4', '百度推广', 'zlbdtg', 'SEM00000118', '4679', '1452361240');
INSERT INTO `go_data_statistics` VALUES ('49', '4', '百度推广', 'zlbdtg', 'SEM00000118', '4682', '1452361349');
INSERT INTO `go_data_statistics` VALUES ('50', '4', '百度推广', 'zlbdtg', 'SEM00000118', '4683', '1452361715');
INSERT INTO `go_data_statistics` VALUES ('51', '4', '百度推广', 'zlbdtg', 'SEM00000201', '4704', '1452366428');
INSERT INTO `go_data_statistics` VALUES ('52', '4', '百度推广', 'zlbdtg', 'SEM00000115', '4709', '1452368611');
INSERT INTO `go_data_statistics` VALUES ('53', '4', '百度推广', 'zlbdtg', 'SEM00000115', '4721', '1452375847');
INSERT INTO `go_data_statistics` VALUES ('54', '4', '百度推广', 'zlbdtg', 'SEM00000115', '4729', '1452378669');
INSERT INTO `go_data_statistics` VALUES ('55', '4', '百度推广', 'zlbdtg', 'SEM00000115', '4734', '1452381105');
INSERT INTO `go_data_statistics` VALUES ('56', '4', '百度推广', 'zlbdtg', 'SEM00000115', '4748', '1452382249');
INSERT INTO `go_data_statistics` VALUES ('57', '4', '百度推广', 'zlbdtg', 'SEM00000117', '4781', '1452386783');
INSERT INTO `go_data_statistics` VALUES ('58', '4', '百度推广', 'zlbdtg', 'SEM00000414', '4793', '1452387951');
INSERT INTO `go_data_statistics` VALUES ('59', '4', '百度推广', 'zlbdtg', 'SEM00000178', '4799', '1452388548');
INSERT INTO `go_data_statistics` VALUES ('60', '4', '百度推广', 'zlbdtg', 'SEM00000122', '4819', '1452390037');
INSERT INTO `go_data_statistics` VALUES ('61', '4', '百度推广', 'zlbdtg', 'SEM00000394', '4828', '1452390610');
INSERT INTO `go_data_statistics` VALUES ('62', '4', '百度推广', 'zlbdtg', 'SEM00000122', '4834', '1452391163');
INSERT INTO `go_data_statistics` VALUES ('63', '4', '百度推广', 'zlbdtg', 'SEM00000117', '4841', '1452391603');
INSERT INTO `go_data_statistics` VALUES ('64', '4', '百度推广', 'zlbdtg', 'SEM00000118', '4845', '1452391821');
INSERT INTO `go_data_statistics` VALUES ('65', '4', '百度推广', 'zlbdtg', 'SEM00000114', '4858', '1452393459');
INSERT INTO `go_data_statistics` VALUES ('66', '4', '百度推广', 'zlbdtg', 'SEM00000396', '4901', '1452396958');
INSERT INTO `go_data_statistics` VALUES ('67', '4', '百度推广', 'zlbdtg', 'SEM00000115', '4906', '1452397130');
INSERT INTO `go_data_statistics` VALUES ('68', '4', '百度推广', 'zlbdtg', 'SEM00000114', '4916', '1452397531');
INSERT INTO `go_data_statistics` VALUES ('69', '4', '百度推广', 'zlbdtg', 'SEM00000122', '4922', '1452397713');
INSERT INTO `go_data_statistics` VALUES ('70', '4', '百度推广', 'zlbdtg', 'SEM00000115', '4938', '1452398164');
INSERT INTO `go_data_statistics` VALUES ('71', '4', '百度推广', 'zlbdtg', 'SEM00000117', '4939', '1452398186');
INSERT INTO `go_data_statistics` VALUES ('72', '4', '百度推广', 'zlbdtg', 'SEM00000323', '4941', '1452398246');
INSERT INTO `go_data_statistics` VALUES ('73', '4', '百度推广', 'zlbdtg', 'SEM00000119', '4943', '1452398279');
INSERT INTO `go_data_statistics` VALUES ('74', '4', '百度推广', 'zlbdtg', 'SEM00000117', '4947', '1452398670');
INSERT INTO `go_data_statistics` VALUES ('75', '4', '百度推广', 'zlbdtg', 'SEM00000121', '4954', '1452398993');
INSERT INTO `go_data_statistics` VALUES ('76', '4', '百度推广', 'zlbdtg', 'SEM00000118', '4969', '1452399969');
INSERT INTO `go_data_statistics` VALUES ('77', '4', '百度推广', 'zlbdtg', 'SEM00000118', '4974', '1452400340');
INSERT INTO `go_data_statistics` VALUES ('78', '4', '百度推广', 'zlbdtg', 'SEM00000206', '4997', '1452401999');
INSERT INTO `go_data_statistics` VALUES ('79', '4', '百度推广', 'zlbdtg', 'SEM00000115', '5003', '1452402227');
INSERT INTO `go_data_statistics` VALUES ('80', '4', '百度推广', 'zlbdtg', 'SEM00000114', '5004', '1452402235');
INSERT INTO `go_data_statistics` VALUES ('81', '4', '百度推广', 'zlbdtg', 'SEM00000122', '5008', '1452402603');
INSERT INTO `go_data_statistics` VALUES ('82', '4', '百度推广', 'zlbdtg', 'SEM00000122', '5015', '1452402928');
INSERT INTO `go_data_statistics` VALUES ('83', '4', '百度推广', 'zlbdtg', 'SEM00000121', '5026', '1452403442');
INSERT INTO `go_data_statistics` VALUES ('84', '4', '百度推广', 'zlbdtg', 'SEM00000114', '5027', '1452403481');
INSERT INTO `go_data_statistics` VALUES ('85', '4', '百度推广', 'zlbdtg', 'SEM00000122', '5055', '1452404948');
INSERT INTO `go_data_statistics` VALUES ('86', '4', '百度推广', 'zlbdtg', 'SEM00000115', '5070', '1452405999');
INSERT INTO `go_data_statistics` VALUES ('87', '4', '百度推广', 'zlbdtg', 'SEM00000180', '5072', '1452406094');
INSERT INTO `go_data_statistics` VALUES ('88', '4', '百度推广', 'zlbdtg', 'SEM00000118', '5076', '1452406494');
INSERT INTO `go_data_statistics` VALUES ('89', '4', '百度推广', 'zlbdtg', 'SEM00000121', '5082', '1452406905');
INSERT INTO `go_data_statistics` VALUES ('90', '4', '百度推广', 'zlbdtg', 'SEM00000196', '5086', '1452407159');
INSERT INTO `go_data_statistics` VALUES ('91', '4', '百度推广', 'zlbdtg', 'SEM00000114', '5092', '1452407340');
INSERT INTO `go_data_statistics` VALUES ('92', '4', '百度推广', 'zlbdtg', 'SEM00000333', '5094', '1452407421');
INSERT INTO `go_data_statistics` VALUES ('93', '4', '百度推广', 'zlbdtg', 'SEM00000309', '5095', '1452407531');
INSERT INTO `go_data_statistics` VALUES ('94', '4', '百度推广', 'zlbdtg', 'SEM00000122', '5097', '1452407737');
INSERT INTO `go_data_statistics` VALUES ('95', '4', '百度推广', 'zlbdtg', 'SEM00000122', '5099', '1452407858');
INSERT INTO `go_data_statistics` VALUES ('96', '4', '百度推广', 'zlbdtg', 'SEM00000115', '5101', '1452407949');
INSERT INTO `go_data_statistics` VALUES ('97', '4', '百度推广', 'zlbdtg', 'SEM00000118', '5132', '1452409013');
INSERT INTO `go_data_statistics` VALUES ('98', '4', '百度推广', 'zlbdtg', 'SEM00000121', '5137', '1452409283');
INSERT INTO `go_data_statistics` VALUES ('99', '4', '百度推广', 'zlbdtg', 'SEM00000121', '5142', '1452409454');
INSERT INTO `go_data_statistics` VALUES ('100', '4', '百度推广', 'zlbdtg', 'SEM00000118', '5158', '1452410400');
INSERT INTO `go_data_statistics` VALUES ('101', '4', '百度推广', 'zlbdtg', 'SEM00000121', '5161', '1452410765');
INSERT INTO `go_data_statistics` VALUES ('102', '4', '百度推广', 'zlbdtg', 'SEM00000115', '5165', '1452411119');
INSERT INTO `go_data_statistics` VALUES ('103', '4', '百度推广', 'zlbdtg', 'SEM00000114', '5172', '1452411662');
INSERT INTO `go_data_statistics` VALUES ('104', '4', '百度推广', 'zlbdtg', 'SEM00000308', '5175', '1452411716');
INSERT INTO `go_data_statistics` VALUES ('105', '4', '百度推广', 'zlbdtg', 'SEM00000118', '5183', '1452412300');
INSERT INTO `go_data_statistics` VALUES ('106', '4', '百度推广', 'zlbdtg', 'SEM00000115', '5185', '1452413294');
INSERT INTO `go_data_statistics` VALUES ('107', '4', '百度推广', 'zlbdtg', 'SEM00000122', '5188', '1452413368');
INSERT INTO `go_data_statistics` VALUES ('108', '4', '百度推广', 'zlbdtg', 'SEM00000309', '5204', '1452414338');
INSERT INTO `go_data_statistics` VALUES ('109', '4', '百度推广', 'zlbdtg', 'SEM00000113', '5219', '1452415699');
INSERT INTO `go_data_statistics` VALUES ('110', '4', '百度推广', 'zlbdtg', 'SEM00000319', '5223', '1452415823');
INSERT INTO `go_data_statistics` VALUES ('111', '4', '百度推广', 'zlbdtg', 'SEM00000395', '5229', '1452416417');
INSERT INTO `go_data_statistics` VALUES ('112', '4', '百度推广', 'zlbdtg', 'SEM00000409', '5255', '1452418169');
INSERT INTO `go_data_statistics` VALUES ('113', '4', '百度推广', 'zlbdtg', 'SEM00000196', '5259', '1452418476');
INSERT INTO `go_data_statistics` VALUES ('114', '4', '百度推广', 'zlbdtg', 'SEM00000395', '5272', '1452419412');
INSERT INTO `go_data_statistics` VALUES ('115', '4', '百度推广', 'zlbdtg', 'SEM00000408', '5287', '1452420964');
INSERT INTO `go_data_statistics` VALUES ('116', '4', '百度推广', 'zlbdtg', 'SEM00000246', '5310', '1452423351');
INSERT INTO `go_data_statistics` VALUES ('117', '4', '百度推广', 'zlbdtg', 'SEM00000088', '5327', '1452425402');
INSERT INTO `go_data_statistics` VALUES ('118', '4', '百度推广', 'zlbdtg', 'SEM00000150', '5338', '1452426866');
INSERT INTO `go_data_statistics` VALUES ('119', '4', '百度推广', 'zlbdtg', 'SEM00000097', '5341', '1452428122');
INSERT INTO `go_data_statistics` VALUES ('120', '4', '百度推广', 'zlbdtg', 'SEM00000127', '5347', '1452429024');
INSERT INTO `go_data_statistics` VALUES ('121', '4', '百度推广', 'zlbdtg', 'SEM00000163', '5349', '1452429289');
INSERT INTO `go_data_statistics` VALUES ('122', '4', '百度推广', 'zlbdtg', 'SEM00000201', '5363', '1452431280');
INSERT INTO `go_data_statistics` VALUES ('123', '4', '百度推广', 'zlbdtg', 'SEM00000096', '5368', '1452431852');
INSERT INTO `go_data_statistics` VALUES ('124', '4', '百度推广', 'zlbdtg', 'SEM00000395', '5372', '1452432078');
INSERT INTO `go_data_statistics` VALUES ('125', '4', '百度推广', 'zlbdtg', 'SEM00000084', '5374', '1452432293');
INSERT INTO `go_data_statistics` VALUES ('126', '4', '百度推广', 'zlbdtg', 'SEM00000178', '5381', '1452433263');
INSERT INTO `go_data_statistics` VALUES ('127', '4', '百度推广', 'zlbdtg', 'SEM00000043', '5382', '1452433448');
INSERT INTO `go_data_statistics` VALUES ('128', '4', '百度推广', 'zlbdtg', 'SEM00000094', '5399', '1452434622');
INSERT INTO `go_data_statistics` VALUES ('129', '4', '百度推广', 'zlbdtg', 'SEM00000163', '5411', '1452435870');
INSERT INTO `go_data_statistics` VALUES ('130', '4', '百度推广', 'zlbdtg', 'SEM00000396', '5416', '1452436174');
INSERT INTO `go_data_statistics` VALUES ('131', '4', '百度推广', 'zlbdtg', 'SEM00000203', '5422', '1452436945');
INSERT INTO `go_data_statistics` VALUES ('132', '4', '百度推广', 'zlbdtg', 'SEM00000244', '5432', '1452437700');
INSERT INTO `go_data_statistics` VALUES ('133', '4', '百度推广', 'zlbdtg', 'SEM00000097', '5434', '1452437777');
INSERT INTO `go_data_statistics` VALUES ('134', '4', '百度推广', 'zlbdtg', 'SEM00000196', '5447', '1452438560');
INSERT INTO `go_data_statistics` VALUES ('135', '4', '百度推广', 'zlbdtg', 'SEM00000097', '5467', '1452440778');
INSERT INTO `go_data_statistics` VALUES ('136', '4', '百度推广', 'zlbdtg', 'SEM00000122', '5483', '1452444233');
INSERT INTO `go_data_statistics` VALUES ('137', '4', '百度推广', 'zlbdtg', 'SEM00000115', '5489', '1452445502');
INSERT INTO `go_data_statistics` VALUES ('138', '4', '百度推广', 'zlbdtg', 'SEM00000115', '5491', '1452446091');
INSERT INTO `go_data_statistics` VALUES ('139', '4', '百度推广', 'zlbdtg', 'SEM00000118', '5496', '1452446905');
INSERT INTO `go_data_statistics` VALUES ('140', '4', '百度推广', 'zlbdtg', 'SEM00000115', '5497', '1452447352');
INSERT INTO `go_data_statistics` VALUES ('141', '4', '百度推广', 'zlbdtg', 'SEM00000244', '5500', '1452448026');
INSERT INTO `go_data_statistics` VALUES ('142', '4', '百度推广', 'zlbdtg', 'SEM00000118', '5501', '1452448313');
INSERT INTO `go_data_statistics` VALUES ('143', '4', '百度推广', 'zlbdtg', 'SEM00000120', '5504', '1452449489');
INSERT INTO `go_data_statistics` VALUES ('144', '4', '百度推广', 'zlbdtg', 'SEM00000115', '5505', '1452449861');
INSERT INTO `go_data_statistics` VALUES ('145', '4', '百度推广', 'zlbdtg', 'SEM00000118', '5507', '1452451117');
INSERT INTO `go_data_statistics` VALUES ('146', '4', '百度推广', 'zlbdtg', 'SEM00000122', '5511', '1452453898');
INSERT INTO `go_data_statistics` VALUES ('147', '4', '百度推广', 'zlbdtg', 'SEM00000115', '5513', '1452455417');
INSERT INTO `go_data_statistics` VALUES ('148', '4', '百度推广', 'zlbdtg', 'SEM00000118', '5514', '1452455766');
INSERT INTO `go_data_statistics` VALUES ('149', '4', '百度推广', 'zlbdtg', 'SEM00000121', '5515', '1452456947');
INSERT INTO `go_data_statistics` VALUES ('150', '4', '百度推广', 'zlbdtg', 'SEM00000114', '5521', '1452462826');
INSERT INTO `go_data_statistics` VALUES ('151', '4', '百度推广', 'zlbdtg', 'SEM00000122', '5532', '1452468333');
INSERT INTO `go_data_statistics` VALUES ('152', '4', '百度推广', 'zlbdtg', 'SEM00000115', '5539', '1452469534');
INSERT INTO `go_data_statistics` VALUES ('153', '4', '百度推广', 'zlbdtg', 'SEM00000118', '5554', '1452472330');
INSERT INTO `go_data_statistics` VALUES ('154', '4', '百度推广', 'zlbdtg', 'SEM00000118', '5556', '1452472466');
INSERT INTO `go_data_statistics` VALUES ('155', '4', '百度推广', 'zlbdtg', 'SEM00000242', '5560', '1452473173');
INSERT INTO `go_data_statistics` VALUES ('156', '4', '百度推广', 'zlbdtg', 'SEM00000309', '5568', '1452473925');
INSERT INTO `go_data_statistics` VALUES ('157', '4', '百度推广', 'zlbdtg', 'SEM00000118', '5583', '1452475997');
INSERT INTO `go_data_statistics` VALUES ('158', '4', '百度推广', 'zlbdtg', 'SEM00000118', '5587', '1452476483');
INSERT INTO `go_data_statistics` VALUES ('159', '4', '百度推广', 'zlbdtg', 'SEM00000118', '5591', '1452476829');
INSERT INTO `go_data_statistics` VALUES ('160', '4', '百度推广', 'zlbdtg', 'SEM00000121', '5599', '1452477295');
INSERT INTO `go_data_statistics` VALUES ('161', '4', '百度推广', 'zlbdtg', 'SEM00000122', '5602', '1452477597');
INSERT INTO `go_data_statistics` VALUES ('162', '4', '百度推广', 'zlbdtg', 'SEM00000122', '5605', '1452477754');
INSERT INTO `go_data_statistics` VALUES ('163', '4', '百度推广', 'zlbdtg', 'SEM00000122', '5610', '1452478807');
INSERT INTO `go_data_statistics` VALUES ('164', '4', '百度推广', 'zlbdtg', 'SEM00000122', '5618', '1452479335');
INSERT INTO `go_data_statistics` VALUES ('165', '4', '百度推广', 'zlbdtg', 'SEM00000115', '5632', '1452480249');
INSERT INTO `go_data_statistics` VALUES ('166', '4', '百度推广', 'zlbdtg', 'SEM00000118', '5647', '1452481401');
INSERT INTO `go_data_statistics` VALUES ('167', '4', '百度推广', 'zlbdtg', 'SEM00000061', '5648', '1452481470');
INSERT INTO `go_data_statistics` VALUES ('168', '4', '百度推广', 'zlbdtg', 'SEM00000128', '5652', '1452481643');
INSERT INTO `go_data_statistics` VALUES ('169', '4', '百度推广', 'zlbdtg', 'SEM00000121', '5653', '1452481648');
INSERT INTO `go_data_statistics` VALUES ('170', '4', '百度推广', 'zlbdtg', 'SEM00000114', '5658', '1452482508');
INSERT INTO `go_data_statistics` VALUES ('171', '4', '百度推广', 'zlbdtg', 'SEM00000115', '5659', '1452482511');
INSERT INTO `go_data_statistics` VALUES ('172', '4', '百度推广', 'zlbdtg', 'SEM00000121', '5666', '1452482819');
INSERT INTO `go_data_statistics` VALUES ('173', '4', '百度推广', 'zlbdtg', 'SEM00000121', '5681', '1452483851');
INSERT INTO `go_data_statistics` VALUES ('174', '4', '百度推广', 'zlbdtg', 'SEM00000114', '5683', '1452484058');
INSERT INTO `go_data_statistics` VALUES ('175', '4', '百度推广', 'zlbdtg', 'SEM00000115', '5686', '1452484135');
INSERT INTO `go_data_statistics` VALUES ('176', '4', '百度推广', 'zlbdtg', 'SEM00000115', '5690', '1452484286');
INSERT INTO `go_data_statistics` VALUES ('177', '4', '百度推广', 'zlbdtg', 'SEM00000390', '5694', '1452484610');
INSERT INTO `go_data_statistics` VALUES ('178', '4', '百度推广', 'zlbdtg', 'SEM00000118', '5699', '1452484964');
INSERT INTO `go_data_statistics` VALUES ('179', '4', '百度推广', 'zlbdtg', 'SEM00000309', '5705', '1452485622');
INSERT INTO `go_data_statistics` VALUES ('180', '4', '百度推广', 'zlbdtg', 'SEM00000117', '5715', '1452486403');
INSERT INTO `go_data_statistics` VALUES ('181', '4', '百度推广', 'zlbdtg', 'SEM00000121', '5717', '1452486540');
INSERT INTO `go_data_statistics` VALUES ('182', '4', '百度推广', 'zlbdtg', 'SEM00000244', '5722', '1452486687');
INSERT INTO `go_data_statistics` VALUES ('183', '4', '百度推广', 'zlbdtg', 'SEM00000122', '5734', '1452487372');
INSERT INTO `go_data_statistics` VALUES ('184', '4', '百度推广', 'zlbdtg', 'SEM00000122', '5738', '1452487565');
INSERT INTO `go_data_statistics` VALUES ('185', '4', '百度推广', 'zlbdtg', 'SEM00000121', '5743', '1452487877');
INSERT INTO `go_data_statistics` VALUES ('186', '4', '百度推广', 'zlbdtg', 'SEM00000114', '5759', '1452489094');
INSERT INTO `go_data_statistics` VALUES ('187', '4', '百度推广', 'zlbdtg', 'SEM00000115', '5765', '1452489631');
INSERT INTO `go_data_statistics` VALUES ('188', '4', '百度推广', 'zlbdtg', 'SEM00000114', '5772', '1452490492');
INSERT INTO `go_data_statistics` VALUES ('189', '4', '百度推广', 'zlbdtg', 'SEM00000115', '5774', '1452490854');
INSERT INTO `go_data_statistics` VALUES ('190', '4', '百度推广', 'zlbdtg', 'SEM00000121', '5776', '1452491124');
INSERT INTO `go_data_statistics` VALUES ('191', '4', '百度推广', 'zlbdtg', 'SEM00000115', '5777', '1452491214');
INSERT INTO `go_data_statistics` VALUES ('192', '4', '百度推广', 'zlbdtg', 'SEM00000122', '5779', '1452491316');
INSERT INTO `go_data_statistics` VALUES ('193', '4', '百度推广', 'zlbdtg', 'SEM00000121', '5780', '1452491344');
INSERT INTO `go_data_statistics` VALUES ('194', '4', '百度推广', 'zlbdtg', 'SEM00000117', '5794', '1452492252');
INSERT INTO `go_data_statistics` VALUES ('195', '4', '百度推广', 'zlbdtg', 'SEM00000121', '5798', '1452492489');
INSERT INTO `go_data_statistics` VALUES ('196', '4', '百度推广', 'zlbdtg', 'SEM00000178', '5804', '1452492872');
INSERT INTO `go_data_statistics` VALUES ('197', '4', '百度推广', 'zlbdtg', 'SEM00000118', '5808', '1452493046');
INSERT INTO `go_data_statistics` VALUES ('198', '4', '百度推广', 'zlbdtg', 'SEM00000121', '5809', '1452493158');
INSERT INTO `go_data_statistics` VALUES ('199', '4', '百度推广', 'zlbdtg', 'SEM00000120', '5810', '1452493159');
INSERT INTO `go_data_statistics` VALUES ('200', '4', '百度推广', 'zlbdtg', 'SEM00000309', '5814', '1452493770');
INSERT INTO `go_data_statistics` VALUES ('201', '4', '百度推广', 'zlbdtg', 'SEM00000199', '5816', '1452494165');
INSERT INTO `go_data_statistics` VALUES ('202', '4', '百度推广', 'zlbdtg', 'SEM00000206', '5817', '1452494180');
INSERT INTO `go_data_statistics` VALUES ('203', '4', '百度推广', 'zlbdtg', 'SEM00000396', '5826', '1452495155');
INSERT INTO `go_data_statistics` VALUES ('204', '4', '百度推广', 'zlbdtg', 'SEM00000122', '5827', '1452495177');
INSERT INTO `go_data_statistics` VALUES ('205', '4', '百度推广', 'zlbdtg', 'SEM00000121', '5832', '1452495822');
INSERT INTO `go_data_statistics` VALUES ('206', '4', '百度推广', 'zlbdtg', 'SEM00000133', '5845', '1452496958');
INSERT INTO `go_data_statistics` VALUES ('207', '4', '百度推广', 'zlbdtg', 'SEM00000127', '5898', '1452502860');
INSERT INTO `go_data_statistics` VALUES ('208', '4', '百度推广', 'zlbdtg', 'SEM00000083', '5909', '1452503676');
INSERT INTO `go_data_statistics` VALUES ('209', '4', '百度推广', 'zlbdtg', 'SEM00000396', '5911', '1452503952');
INSERT INTO `go_data_statistics` VALUES ('210', '6', '信号增强器', 'zlxhzqq', '', '5960', '1452510228');
INSERT INTO `go_data_statistics` VALUES ('211', '4', '百度推广', 'zlbdtg', 'SEM00000115', '5962', '1452510503');
INSERT INTO `go_data_statistics` VALUES ('212', '4', '百度推广', 'zlbdtg', 'SEM00000120', '5987', '1452512996');
INSERT INTO `go_data_statistics` VALUES ('213', '4', '百度推广', 'zlbdtg', 'SEM00000122', '5988', '1452513133');
INSERT INTO `go_data_statistics` VALUES ('214', '4', '百度推广', 'zlbdtg', 'SEM00000114', '6018', '1452515726');
INSERT INTO `go_data_statistics` VALUES ('215', '4', '百度推广', 'zlbdtg', 'SEM00000115', '6022', '1452516166');
INSERT INTO `go_data_statistics` VALUES ('216', '4', '百度推广', 'zlbdtg', 'SEM00000163', '6023', '1452516178');
INSERT INTO `go_data_statistics` VALUES ('217', '6', '信号增强器', 'zlxhzqq', '', '6031', '1452516779');
INSERT INTO `go_data_statistics` VALUES ('218', '4', '百度推广', 'zlbdtg', 'SEM00000117', '6041', '1452517682');
INSERT INTO `go_data_statistics` VALUES ('219', '4', '百度推广', 'zlbdtg', 'SEM00000199', '6046', '1452518453');
INSERT INTO `go_data_statistics` VALUES ('220', '4', '百度推广', 'zlbdtg', 'SEM00000118', '6051', '1452518889');
INSERT INTO `go_data_statistics` VALUES ('221', '4', '百度推广', 'zlbdtg', 'SEM00000115', '6057', '1452519207');
INSERT INTO `go_data_statistics` VALUES ('222', '4', '百度推广', 'zlbdtg', 'SEM00000121', '6063', '1452520010');
INSERT INTO `go_data_statistics` VALUES ('223', '4', '百度推广', 'zlbdtg', 'SEM00000115', '6073', '1452521326');
INSERT INTO `go_data_statistics` VALUES ('224', '4', '百度推广', 'zlbdtg', 'SEM00000118', '6079', '1452521765');
INSERT INTO `go_data_statistics` VALUES ('225', '4', '百度推广', 'zlbdtg', 'SEM00000122', '6084', '1452521964');
INSERT INTO `go_data_statistics` VALUES ('226', '4', '百度推广', 'zlbdtg', 'SEM00000115', '6087', '1452522046');
INSERT INTO `go_data_statistics` VALUES ('227', '4', '百度推广', 'zlbdtg', 'SEM00000115', '6088', '1452522060');
INSERT INTO `go_data_statistics` VALUES ('228', '6', '信号增强器', 'zlxhzqq', '', '6098', '1452522768');
INSERT INTO `go_data_statistics` VALUES ('229', '4', '百度推广', 'zlbdtg', 'SEM00000121', '6101', '1452522876');
INSERT INTO `go_data_statistics` VALUES ('230', '4', '百度推广', 'zlbdtg', 'SEM00000117', '6102', '1452523060');
INSERT INTO `go_data_statistics` VALUES ('231', '4', '百度推广', 'zlbdtg', 'SEM00000120', '6111', '1452523616');
INSERT INTO `go_data_statistics` VALUES ('232', '4', '百度推广', 'zlbdtg', 'SEM00000114', '6115', '1452523903');
INSERT INTO `go_data_statistics` VALUES ('233', '4', '百度推广', 'zlbdtg', 'SEM00000118', '6120', '1452524550');
INSERT INTO `go_data_statistics` VALUES ('234', '4', '百度推广', 'zlbdtg', 'SEM00000115', '6121', '1452524709');
INSERT INTO `go_data_statistics` VALUES ('235', '4', '百度推广', 'zlbdtg', 'SEM00000121', '6129', '1452525225');
INSERT INTO `go_data_statistics` VALUES ('236', '4', '百度推广', 'zlbdtg', 'SEM00000115', '6138', '1452525924');
INSERT INTO `go_data_statistics` VALUES ('237', '4', '百度推广', 'zlbdtg', 'SEM00000114', '6144', '1452526512');
INSERT INTO `go_data_statistics` VALUES ('238', '4', '百度推广', 'zlbdtg', 'SEM00000097', '6152', '1452527549');
INSERT INTO `go_data_statistics` VALUES ('239', '4', '百度推广', 'zlbdtg', 'SEM00000115', '6187', '1452536488');
INSERT INTO `go_data_statistics` VALUES ('240', '4', '百度推广', 'zlbdtg', 'SEM00000118', '6188', '1452539771');
INSERT INTO `go_data_statistics` VALUES ('241', '4', '百度推广', 'zlbdtg', 'SEM00000115', '6190', '1452540102');
INSERT INTO `go_data_statistics` VALUES ('242', '4', '百度推广', 'zlbdtg', 'SEM00000115', '6191', '1452541756');
INSERT INTO `go_data_statistics` VALUES ('243', '4', '百度推广', 'zlbdtg', 'SEM00000118', '6193', '1452544730');
INSERT INTO `go_data_statistics` VALUES ('244', '4', '百度推广', 'zlbdtg', 'SEM00000328', '6194', '1452545885');
INSERT INTO `go_data_statistics` VALUES ('245', '6', '信号增强器', 'zlxhzqq', '', '6208', '1452557659');
INSERT INTO `go_data_statistics` VALUES ('246', '4', '百度推广', 'zlbdtg', 'SEM00000320', '6240', '1452565164');
INSERT INTO `go_data_statistics` VALUES ('247', '4', '百度推广', 'zlbdtg', 'SEM00000115', '6241', '1452565293');
INSERT INTO `go_data_statistics` VALUES ('248', '4', '百度推广', 'zlbdtg', 'SEM00000118', '6244', '1452565954');
INSERT INTO `go_data_statistics` VALUES ('249', '4', '百度推广', 'zlbdtg', 'SEM00000115', '6247', '1452566608');
INSERT INTO `go_data_statistics` VALUES ('250', '4', '百度推广', 'zlbdtg', 'SEM00000115', '6248', '1452566839');
INSERT INTO `go_data_statistics` VALUES ('251', '4', '百度推广', 'zlbdtg', 'SEM00000115', '6250', '1452567303');
INSERT INTO `go_data_statistics` VALUES ('252', '4', '百度推广', 'zlbdtg', 'SEM00000432', '6257', '1452568488');
INSERT INTO `go_data_statistics` VALUES ('253', '4', '百度推广', 'zlbdtg', 'SEM00000122', '6288', '1452570159');
INSERT INTO `go_data_statistics` VALUES ('254', '4', '百度推广', 'zlbdtg', 'SEM00000118', '6320', '1452571271');
INSERT INTO `go_data_statistics` VALUES ('255', '4', '百度推广', 'zlbdtg', 'SEM00000115', '6379', '1452573209');
INSERT INTO `go_data_statistics` VALUES ('256', '4', '百度推广', 'zlbdtg', 'SEM00000115', '6403', '1452574138');
INSERT INTO `go_data_statistics` VALUES ('257', '4', '百度推广', 'zlbdtg', 'SEM00000122', '6442', '1452574992');
INSERT INTO `go_data_statistics` VALUES ('258', '4', '百度推广', 'zlbdtg', 'SEM00000115', '6455', '1452575501');
INSERT INTO `go_data_statistics` VALUES ('259', '4', '百度推广', 'zlbdtg', 'SEM00000395', '6457', '1452575625');
INSERT INTO `go_data_statistics` VALUES ('260', '4', '百度推广', 'zlbdtg', 'SEM00000122', '6468', '1452575876');
INSERT INTO `go_data_statistics` VALUES ('261', '4', '百度推广', 'zlbdtg', 'SEM00000422', '6489', '1452576846');
INSERT INTO `go_data_statistics` VALUES ('262', '4', '百度推广', 'zlbdtg', 'SEM00000330', '6495', '1452577057');
INSERT INTO `go_data_statistics` VALUES ('263', '4', '百度推广', 'zlbdtg', 'SEM00000121', '6518', '1452578279');
INSERT INTO `go_data_statistics` VALUES ('264', '4', '百度推广', 'zlbdtg', 'SEM00000115', '6532', '1452578600');
INSERT INTO `go_data_statistics` VALUES ('265', '4', '百度推广', 'zlbdtg', 'SEM00000115', '6533', '1452578623');
INSERT INTO `go_data_statistics` VALUES ('266', '4', '百度推广', 'zlbdtg', 'SEM00000422', '6541', '1452578992');
INSERT INTO `go_data_statistics` VALUES ('267', '4', '百度推广', 'zlbdtg', 'SEM00000115', '6548', '1452579191');
INSERT INTO `go_data_statistics` VALUES ('268', '4', '百度推广', 'zlbdtg', 'SEM00000115', '6549', '1452579208');
INSERT INTO `go_data_statistics` VALUES ('269', '4', '百度推广', 'zlbdtg', 'SEM00000122', '6580', '1452579434');
INSERT INTO `go_data_statistics` VALUES ('270', '4', '百度推广', 'zlbdtg', 'SEM00000115', '6591', '1452579509');
INSERT INTO `go_data_statistics` VALUES ('271', '4', '百度推广', 'zlbdtg', 'SEM00000115', '6630', '1452579738');
INSERT INTO `go_data_statistics` VALUES ('272', '4', '百度推广', 'zlbdtg', 'SEM00000122', '6658', '1452579965');
INSERT INTO `go_data_statistics` VALUES ('273', '4', '百度推广', 'zlbdtg', 'SEM00000122', '6731', '1452581157');
INSERT INTO `go_data_statistics` VALUES ('274', '4', '百度推广', 'zlbdtg', 'SEM00000118', '6736', '1452581420');
INSERT INTO `go_data_statistics` VALUES ('275', '4', '百度推广', 'zlbdtg', 'SEM00000178', '6757', '1452582115');
INSERT INTO `go_data_statistics` VALUES ('276', '4', '百度推广', 'zlbdtg', 'SEM00000122', '6774', '1452583370');
INSERT INTO `go_data_statistics` VALUES ('277', '4', '百度推广', 'zlbdtg', 'SEM00000122', '6794', '1452584298');
INSERT INTO `go_data_statistics` VALUES ('278', '4', '百度推广', 'zlbdtg', 'SEM00000118', '6809', '1452584956');
INSERT INTO `go_data_statistics` VALUES ('279', '8', '一元众乐', 'yyzl', '', '6822', '1452586007');
INSERT INTO `go_data_statistics` VALUES ('280', '4', '百度推广', 'zlbdtg', 'SEM00000370', '6823', '1452586014');
INSERT INTO `go_data_statistics` VALUES ('281', '8', '一元众乐', 'yyzl', '', '6826', '1452586162');
INSERT INTO `go_data_statistics` VALUES ('282', '8', '一元众乐', 'yyzl', '', '6827', '1452586177');
INSERT INTO `go_data_statistics` VALUES ('283', '8', '一元众乐', 'yyzl', '', '6828', '1452586228');
INSERT INTO `go_data_statistics` VALUES ('284', '8', '一元众乐', 'yyzl', '', '6829', '1452586246');
INSERT INTO `go_data_statistics` VALUES ('285', '8', '一元众乐', 'yyzl', '', '6830', '1452586282');
INSERT INTO `go_data_statistics` VALUES ('286', '8', '一元众乐', 'yyzl', '', '6831', '1452586289');
INSERT INTO `go_data_statistics` VALUES ('287', '8', '一元众乐', 'yyzl', '', '6832', '1452586332');
INSERT INTO `go_data_statistics` VALUES ('288', '8', '一元众乐', 'yyzl', '', '6833', '1452586452');
INSERT INTO `go_data_statistics` VALUES ('289', '8', '一元众乐', 'yyzl', '', '6836', '1452586666');
INSERT INTO `go_data_statistics` VALUES ('290', '4', '百度推广', 'zlbdtg', 'YDSEM00000117', '6837', '1452586678');
INSERT INTO `go_data_statistics` VALUES ('291', '8', '一元众乐', 'yyzl', '', '6838', '1452586711');
INSERT INTO `go_data_statistics` VALUES ('292', '8', '一元众乐', 'yyzl', '', '6839', '1452586843');
INSERT INTO `go_data_statistics` VALUES ('293', '8', '一元众乐', 'yyzl', '', '6840', '1452586890');
INSERT INTO `go_data_statistics` VALUES ('294', '8', '一元众乐', 'yyzl', '', '6841', '1452586913');
INSERT INTO `go_data_statistics` VALUES ('295', '8', '一元众乐', 'yyzl', '', '6842', '1452586963');
INSERT INTO `go_data_statistics` VALUES ('296', '8', '一元众乐', 'yyzl', '', '6843', '1452586982');
INSERT INTO `go_data_statistics` VALUES ('297', '8', '一元众乐', 'yyzl', '', '6844', '1452587020');
INSERT INTO `go_data_statistics` VALUES ('298', '8', '一元众乐', 'yyzl', '', '6846', '1452587083');
INSERT INTO `go_data_statistics` VALUES ('299', '8', '一元众乐', 'yyzl', '', '6848', '1452587261');
INSERT INTO `go_data_statistics` VALUES ('300', '8', '一元众乐', 'yyzl', '', '6849', '1452587279');
INSERT INTO `go_data_statistics` VALUES ('301', '8', '一元众乐', 'yyzl', '', '6850', '1452587453');
INSERT INTO `go_data_statistics` VALUES ('302', '8', '一元众乐', 'yyzl', '', '6851', '1452587484');
INSERT INTO `go_data_statistics` VALUES ('303', '8', '一元众乐', 'yyzl', '', '6854', '1452587597');
INSERT INTO `go_data_statistics` VALUES ('304', '8', '一元众乐', 'yyzl', '', '6856', '1452587667');
INSERT INTO `go_data_statistics` VALUES ('305', '8', '一元众乐', 'yyzl', '', '6857', '1452587716');
INSERT INTO `go_data_statistics` VALUES ('306', '8', '一元众乐', 'yyzl', '', '6858', '1452587752');
INSERT INTO `go_data_statistics` VALUES ('307', '8', '一元众乐', 'yyzl', '', '6859', '1452587807');
INSERT INTO `go_data_statistics` VALUES ('309', '8', '一元众乐', 'yyzl', '', '6863', '1452587905');
INSERT INTO `go_data_statistics` VALUES ('310', '8', '一元众乐', 'yyzl', '', '6864', '1452588016');
INSERT INTO `go_data_statistics` VALUES ('311', '8', '一元众乐', 'yyzl', '', '6865', '1452588034');
INSERT INTO `go_data_statistics` VALUES ('312', '8', '一元众乐', 'yyzl', '', '6867', '1452588069');
INSERT INTO `go_data_statistics` VALUES ('313', '8', '一元众乐', 'yyzl', '', '6869', '1452588079');
INSERT INTO `go_data_statistics` VALUES ('314', '8', '一元众乐', 'yyzl', '', '6870', '1452588098');
INSERT INTO `go_data_statistics` VALUES ('316', '8', '一元众乐', 'yyzl', '', '6874', '1452588215');
INSERT INTO `go_data_statistics` VALUES ('317', '8', '一元众乐', 'yyzl', '', '6875', '1452588271');
INSERT INTO `go_data_statistics` VALUES ('318', '8', '一元众乐', 'yyzl', '', '6877', '1452588380');
INSERT INTO `go_data_statistics` VALUES ('319', '8', '一元众乐', 'yyzl', '', '6881', '1452588543');
INSERT INTO `go_data_statistics` VALUES ('320', '8', '一元众乐', 'yyzl', '', '6884', '1452588638');
INSERT INTO `go_data_statistics` VALUES ('321', '8', '一元众乐', 'yyzl', '', '6885', '1452588658');
INSERT INTO `go_data_statistics` VALUES ('322', '8', '一元众乐', 'yyzl', '', '6886', '1452588692');
INSERT INTO `go_data_statistics` VALUES ('323', '8', '一元众乐', 'yyzl', '', '6887', '1452588716');
INSERT INTO `go_data_statistics` VALUES ('324', '8', '一元众乐', 'yyzl', '', '6888', '1452588733');
INSERT INTO `go_data_statistics` VALUES ('325', '8', '一元众乐', 'yyzl', '', '6889', '1452588738');
INSERT INTO `go_data_statistics` VALUES ('326', '8', '一元众乐', 'yyzl', '', '6890', '1452588752');
INSERT INTO `go_data_statistics` VALUES ('327', '8', '一元众乐', 'yyzl', '', '6891', '1452588787');
INSERT INTO `go_data_statistics` VALUES ('328', '8', '一元众乐', 'yyzl', '', '6892', '1452588826');
INSERT INTO `go_data_statistics` VALUES ('329', '8', '一元众乐', 'yyzl', '', '6893', '1452588832');
INSERT INTO `go_data_statistics` VALUES ('330', '8', '一元众乐', 'yyzl', '', '6894', '1452588879');
INSERT INTO `go_data_statistics` VALUES ('331', '8', '一元众乐', 'yyzl', '', '6898', '1452589237');
INSERT INTO `go_data_statistics` VALUES ('332', '4', '百度推广', 'zlbdtg', 'SEM00000122', '6899', '1452589261');
INSERT INTO `go_data_statistics` VALUES ('333', '4', '百度推广', 'zlbdtg', 'SEM00000121', '6900', '1452589324');
INSERT INTO `go_data_statistics` VALUES ('334', '8', '一元众乐', 'yyzl', '', '6901', '1452589420');
INSERT INTO `go_data_statistics` VALUES ('335', '8', '一元众乐', 'yyzl', '', '6902', '1452589447');
INSERT INTO `go_data_statistics` VALUES ('336', '4', '百度推广', 'zlbdtg', 'SEM00000115', '6903', '1452589517');
INSERT INTO `go_data_statistics` VALUES ('337', '8', '一元众乐', 'yyzl', '', '6904', '1452589521');
INSERT INTO `go_data_statistics` VALUES ('338', '5', '酷赚锁屏', 'zlkzsp', '', '6905', '1452589565');
INSERT INTO `go_data_statistics` VALUES ('339', '5', '酷赚锁屏', 'zlkzsp', '', '6908', '1452589732');
INSERT INTO `go_data_statistics` VALUES ('340', '8', '一元众乐', 'yyzl', '', '6909', '1452589966');
INSERT INTO `go_data_statistics` VALUES ('341', '8', '一元众乐', 'yyzl', '', '6910', '1452589992');
INSERT INTO `go_data_statistics` VALUES ('342', '8', '一元众乐', 'yyzl', '', '6911', '1452589992');
INSERT INTO `go_data_statistics` VALUES ('343', '8', '一元众乐', 'yyzl', '', '6912', '1452590079');
INSERT INTO `go_data_statistics` VALUES ('344', '8', '一元众乐', 'yyzl', '', '6913', '1452590104');
INSERT INTO `go_data_statistics` VALUES ('345', '8', '一元众乐', 'yyzl', '', '6914', '1452590132');
INSERT INTO `go_data_statistics` VALUES ('346', '8', '一元众乐', 'yyzl', '', '6915', '1452590191');
INSERT INTO `go_data_statistics` VALUES ('347', '8', '一元众乐', 'yyzl', '', '6916', '1452590229');
INSERT INTO `go_data_statistics` VALUES ('348', '8', '一元众乐', 'yyzl', '', '6917', '1452590292');
INSERT INTO `go_data_statistics` VALUES ('349', '8', '一元众乐', 'yyzl', '', '6918', '1452590403');
INSERT INTO `go_data_statistics` VALUES ('350', '8', '一元众乐', 'yyzl', '', '6919', '1452590562');
INSERT INTO `go_data_statistics` VALUES ('351', '4', '百度推广', 'zlbdtg', 'SEM00000119', '6920', '1452590589');
INSERT INTO `go_data_statistics` VALUES ('352', '8', '一元众乐', 'yyzl', '', '6921', '1452590627');
INSERT INTO `go_data_statistics` VALUES ('353', '8', '一元众乐', 'yyzl', '', '6922', '1452590632');
INSERT INTO `go_data_statistics` VALUES ('354', '8', '一元众乐', 'yyzl', '', '6923', '1452590691');
INSERT INTO `go_data_statistics` VALUES ('355', '8', '一元众乐', 'yyzl', '', '6924', '1452590783');
INSERT INTO `go_data_statistics` VALUES ('356', '8', '一元众乐', 'yyzl', '', '6925', '1452590802');
INSERT INTO `go_data_statistics` VALUES ('357', '8', '一元众乐', 'yyzl', '', '6926', '1452591012');
INSERT INTO `go_data_statistics` VALUES ('358', '8', '一元众乐', 'yyzl', '', '6927', '1452591101');
INSERT INTO `go_data_statistics` VALUES ('359', '8', '一元众乐', 'yyzl', '', '6928', '1452591274');
INSERT INTO `go_data_statistics` VALUES ('360', '8', '一元众乐', 'yyzl', '', '6929', '1452591274');
INSERT INTO `go_data_statistics` VALUES ('361', '8', '一元众乐', 'yyzl', '', '6930', '1452591298');
INSERT INTO `go_data_statistics` VALUES ('362', '8', '一元众乐', 'yyzl', '', '6931', '1452591413');
INSERT INTO `go_data_statistics` VALUES ('363', '4', '百度推广', 'zlbdtg', 'SEM00000114', '6934', '1452591590');
INSERT INTO `go_data_statistics` VALUES ('364', '8', '一元众乐', 'yyzl', '', '6935', '1452591644');
INSERT INTO `go_data_statistics` VALUES ('365', '8', '一元众乐', 'yyzl', '', '6936', '1452591829');
INSERT INTO `go_data_statistics` VALUES ('366', '8', '一元众乐', 'yyzl', '', '6937', '1452591882');
INSERT INTO `go_data_statistics` VALUES ('367', '4', '百度推广', 'zlbdtg', 'SEM00000115', '6939', '1452591922');
INSERT INTO `go_data_statistics` VALUES ('368', '4', '百度推广', 'zlbdtg', 'SEM00000115', '6940', '1452591923');
INSERT INTO `go_data_statistics` VALUES ('369', '8', '一元众乐', 'yyzl', '', '6941', '1452591992');
INSERT INTO `go_data_statistics` VALUES ('370', '8', '一元众乐', 'yyzl', '', '6943', '1452592134');
INSERT INTO `go_data_statistics` VALUES ('371', '8', '一元众乐', 'yyzl', '', '6944', '1452592141');
INSERT INTO `go_data_statistics` VALUES ('372', '8', '一元众乐', 'yyzl', '', '6945', '1452592161');
INSERT INTO `go_data_statistics` VALUES ('373', '8', '一元众乐', 'yyzl', '', '6946', '1452592165');
INSERT INTO `go_data_statistics` VALUES ('374', '8', '一元众乐', 'yyzl', '', '6947', '1452592181');
INSERT INTO `go_data_statistics` VALUES ('375', '8', '一元众乐', 'yyzl', '', '6948', '1452592206');
INSERT INTO `go_data_statistics` VALUES ('376', '4', '百度推广', 'zlbdtg', 'SEM00000118', '6949', '1452592211');
INSERT INTO `go_data_statistics` VALUES ('377', '4', '百度推广', 'zlbdtg', 'SEM00000115', '6950', '1452592242');
INSERT INTO `go_data_statistics` VALUES ('378', '8', '一元众乐', 'yyzl', '', '6951', '1452592267');
INSERT INTO `go_data_statistics` VALUES ('379', '8', '一元众乐', 'yyzl', '', '6952', '1452592314');
INSERT INTO `go_data_statistics` VALUES ('380', '4', '百度推广', 'zlbdtg', 'SEM00000121', '6953', '1452592580');
INSERT INTO `go_data_statistics` VALUES ('381', '8', '一元众乐', 'yyzl', '', '6954', '1452592591');
INSERT INTO `go_data_statistics` VALUES ('382', '4', '百度推广', 'zlbdtg', 'SEM00000117', '6955', '1452592646');
INSERT INTO `go_data_statistics` VALUES ('383', '4', '百度推广', 'zlbdtg', 'SEM00000115', '6956', '1452592722');
INSERT INTO `go_data_statistics` VALUES ('384', '8', '一元众乐', 'yyzl', '', '6958', '1452592842');
INSERT INTO `go_data_statistics` VALUES ('385', '8', '一元众乐', 'yyzl', '', '6959', '1452592914');
INSERT INTO `go_data_statistics` VALUES ('386', '8', '一元众乐', 'yyzl', '', '6960', '1452593026');
INSERT INTO `go_data_statistics` VALUES ('387', '8', '一元众乐', 'yyzl', '', '6961', '1452593046');
INSERT INTO `go_data_statistics` VALUES ('388', '8', '一元众乐', 'yyzl', '', '6962', '1452593274');
INSERT INTO `go_data_statistics` VALUES ('389', '8', '一元众乐', 'yyzl', '', '6963', '1452593418');
INSERT INTO `go_data_statistics` VALUES ('390', '8', '一元众乐', 'yyzl', '', '6964', '1452593429');
INSERT INTO `go_data_statistics` VALUES ('391', '8', '一元众乐', 'yyzl', '', '6965', '1452593541');
INSERT INTO `go_data_statistics` VALUES ('392', '8', '一元众乐', 'yyzl', '', '6966', '1452593761');
INSERT INTO `go_data_statistics` VALUES ('393', '4', '百度推广', 'zlbdtg', 'SEM00000115', '6972', '1452594067');
INSERT INTO `go_data_statistics` VALUES ('394', '6', '信号增强器', 'zlxhzqq', '', '6973', '1452594153');
INSERT INTO `go_data_statistics` VALUES ('395', '6', '信号增强器', 'zlxhzqq', '', '6975', '1452594199');
INSERT INTO `go_data_statistics` VALUES ('396', '8', '一元众乐', 'yyzl', '', '6976', '1452594250');
INSERT INTO `go_data_statistics` VALUES ('397', '8', '一元众乐', 'yyzl', '', '6977', '1452594288');
INSERT INTO `go_data_statistics` VALUES ('398', '8', '一元众乐', 'yyzl', '', '6978', '1452594395');
INSERT INTO `go_data_statistics` VALUES ('399', '8', '一元众乐', 'yyzl', '', '6979', '1452594413');
INSERT INTO `go_data_statistics` VALUES ('400', '8', '一元众乐', 'yyzl', '', '6980', '1452594592');
INSERT INTO `go_data_statistics` VALUES ('401', '8', '一元众乐', 'yyzl', '', '6981', '1452594622');
INSERT INTO `go_data_statistics` VALUES ('402', '8', '一元众乐', 'yyzl', '', '6982', '1452594636');
INSERT INTO `go_data_statistics` VALUES ('403', '8', '一元众乐', 'yyzl', '', '6983', '1452594662');
INSERT INTO `go_data_statistics` VALUES ('404', '8', '一元众乐', 'yyzl', '', '6984', '1452595267');
INSERT INTO `go_data_statistics` VALUES ('405', '8', '一元众乐', 'yyzl', '', '6985', '1452595334');
INSERT INTO `go_data_statistics` VALUES ('406', '8', '一元众乐', 'yyzl', '', '6986', '1452595343');
INSERT INTO `go_data_statistics` VALUES ('407', '8', '一元众乐', 'yyzl', '', '6987', '1452595483');
INSERT INTO `go_data_statistics` VALUES ('408', '8', '一元众乐', 'yyzl', '', '6988', '1452595516');
INSERT INTO `go_data_statistics` VALUES ('409', '4', '百度推广', 'zlbdtg', 'SEM00000115', '6990', '1452595796');
INSERT INTO `go_data_statistics` VALUES ('410', '8', '一元众乐', 'yyzl', '', '6991', '1452595845');
INSERT INTO `go_data_statistics` VALUES ('411', '4', '百度推广', 'zlbdtg', 'SEM00000122', '6992', '1452595972');
INSERT INTO `go_data_statistics` VALUES ('412', '8', '一元众乐', 'yyzl', '', '6994', '1452596401');
INSERT INTO `go_data_statistics` VALUES ('413', '4', '百度推广', 'zlbdtg', 'SEM00000122', '6995', '1452596479');
INSERT INTO `go_data_statistics` VALUES ('414', '6', '信号增强器', 'zlxhzqq', '', '6996', '1452596499');
INSERT INTO `go_data_statistics` VALUES ('415', '8', '一元众乐', 'yyzl', '', '6997', '1452596508');
INSERT INTO `go_data_statistics` VALUES ('416', '8', '一元众乐', 'yyzl', '', '6998', '1452596623');
INSERT INTO `go_data_statistics` VALUES ('417', '8', '一元众乐', 'yyzl', '', '6999', '1452596673');
INSERT INTO `go_data_statistics` VALUES ('418', '4', '百度推广', 'zlbdtg', 'SEM00000115', '7000', '1452596880');
INSERT INTO `go_data_statistics` VALUES ('419', '4', '百度推广', 'zlbdtg', 'SEM00000120', '7001', '1452596902');
INSERT INTO `go_data_statistics` VALUES ('420', '8', '一元众乐', 'yyzl', '', '7002', '1452596912');
INSERT INTO `go_data_statistics` VALUES ('421', '4', '百度推广', 'zlbdtg', 'SEM00000115', '7004', '1452596974');
INSERT INTO `go_data_statistics` VALUES ('422', '8', '一元众乐', 'yyzl', '', '7005', '1452596999');
INSERT INTO `go_data_statistics` VALUES ('423', '8', '一元众乐', 'yyzl', '', '7006', '1452597017');
INSERT INTO `go_data_statistics` VALUES ('424', '8', '一元众乐', 'yyzl', '', '7007', '1452597039');
INSERT INTO `go_data_statistics` VALUES ('425', '8', '一元众乐', 'yyzl', '', '7008', '1452597063');
INSERT INTO `go_data_statistics` VALUES ('426', '8', '一元众乐', 'yyzl', '', '7009', '1452597075');
INSERT INTO `go_data_statistics` VALUES ('427', '8', '一元众乐', 'yyzl', '', '7010', '1452597078');
INSERT INTO `go_data_statistics` VALUES ('428', '4', '百度推广', 'zlbdtg', 'SEM00000122', '7011', '1452597246');
INSERT INTO `go_data_statistics` VALUES ('429', '8', '一元众乐', 'yyzl', '', '7012', '1452597321');
INSERT INTO `go_data_statistics` VALUES ('430', '8', '一元众乐', 'yyzl', '', '7014', '1452597485');
INSERT INTO `go_data_statistics` VALUES ('431', '8', '一元众乐', 'yyzl', '', '7016', '1452597545');
INSERT INTO `go_data_statistics` VALUES ('432', '8', '一元众乐', 'yyzl', '', '7017', '1452597562');
INSERT INTO `go_data_statistics` VALUES ('433', '8', '一元众乐', 'yyzl', '', '7018', '1452597585');
INSERT INTO `go_data_statistics` VALUES ('434', '8', '一元众乐', 'yyzl', '', '7020', '1452597721');
INSERT INTO `go_data_statistics` VALUES ('435', '8', '一元众乐', 'yyzl', '', '7021', '1452597803');
INSERT INTO `go_data_statistics` VALUES ('436', '8', '一元众乐', 'yyzl', '', '7023', '1452597867');
INSERT INTO `go_data_statistics` VALUES ('437', '8', '一元众乐', 'yyzl', '', '7024', '1452597971');
INSERT INTO `go_data_statistics` VALUES ('438', '8', '一元众乐', 'yyzl', '', '7025', '1452597987');
INSERT INTO `go_data_statistics` VALUES ('439', '4', '百度推广', 'zlbdtg', 'SEM00000115', '7026', '1452598038');
INSERT INTO `go_data_statistics` VALUES ('440', '8', '一元众乐', 'yyzl', '', '7027', '1452598065');
INSERT INTO `go_data_statistics` VALUES ('441', '9', '午夜艳视', 'wuyeyanshi', '', '7028', '1452598202');
INSERT INTO `go_data_statistics` VALUES ('442', '8', '一元众乐', 'yyzl', '', '7030', '1452598328');
INSERT INTO `go_data_statistics` VALUES ('443', '10', '腾讯应用宝', 'qq', '', '7031', '1452598376');
INSERT INTO `go_data_statistics` VALUES ('444', '8', '一元众乐', 'yyzl', '', '7032', '1452598472');
INSERT INTO `go_data_statistics` VALUES ('445', '8', '一元众乐', 'yyzl', '', '7033', '1452598632');
INSERT INTO `go_data_statistics` VALUES ('446', '8', '一元众乐', 'yyzl', '', '7034', '1452598674');
INSERT INTO `go_data_statistics` VALUES ('447', '8', '一元众乐', 'yyzl', '', '7035', '1452598699');
INSERT INTO `go_data_statistics` VALUES ('448', '8', '一元众乐', 'yyzl', '', '7037', '1452598777');
INSERT INTO `go_data_statistics` VALUES ('449', '8', '一元众乐', 'yyzl', '', '7038', '1452598813');
INSERT INTO `go_data_statistics` VALUES ('450', '4', '百度推广', 'zlbdtg', 'SEM00000163', '7039', '1452598885');
INSERT INTO `go_data_statistics` VALUES ('451', '4', '百度推广', 'zlbdtg', 'SEM00000122', '7040', '1452598926');
INSERT INTO `go_data_statistics` VALUES ('452', '4', '百度推广', 'zlbdtg', 'SEM00000121', '7041', '1452599011');
INSERT INTO `go_data_statistics` VALUES ('453', '4', '百度推广', 'zlbdtg', 'SEM00000115', '7042', '1452599232');
INSERT INTO `go_data_statistics` VALUES ('454', '8', '一元众乐', 'yyzl', '', '7043', '1452599348');
INSERT INTO `go_data_statistics` VALUES ('455', '8', '一元众乐', 'yyzl', '', '7044', '1452599518');
INSERT INTO `go_data_statistics` VALUES ('456', '8', '一元众乐', 'yyzl', '', '7045', '1452599564');
INSERT INTO `go_data_statistics` VALUES ('457', '8', '一元众乐', 'yyzl', '', '7046', '1452599626');
INSERT INTO `go_data_statistics` VALUES ('458', '8', '一元众乐', 'yyzl', '', '7048', '1452599842');
INSERT INTO `go_data_statistics` VALUES ('459', '8', '一元众乐', 'yyzl', '', '7049', '1452599957');
INSERT INTO `go_data_statistics` VALUES ('460', '8', '一元众乐', 'yyzl', '', '7050', '1452599984');
INSERT INTO `go_data_statistics` VALUES ('461', '8', '一元众乐', 'yyzl', '', '7051', '1452600091');
INSERT INTO `go_data_statistics` VALUES ('462', '8', '一元众乐', 'yyzl', '', '7052', '1452600205');
INSERT INTO `go_data_statistics` VALUES ('463', '8', '一元众乐', 'yyzl', '', '7054', '1452600413');
INSERT INTO `go_data_statistics` VALUES ('464', '8', '一元众乐', 'yyzl', '', '7055', '1452600598');
INSERT INTO `go_data_statistics` VALUES ('465', '8', '一元众乐', 'yyzl', '', '7056', '1452600655');
INSERT INTO `go_data_statistics` VALUES ('466', '4', '百度推广', 'zlbdtg', 'SEM00000115', '7057', '1452600672');
INSERT INTO `go_data_statistics` VALUES ('467', '8', '一元众乐', 'yyzl', '', '7058', '1452600681');
INSERT INTO `go_data_statistics` VALUES ('468', '6', '信号增强器', 'zlxhzqq', '', '7059', '1452600915');
INSERT INTO `go_data_statistics` VALUES ('469', '8', '一元众乐', 'yyzl', '', '7060', '1452600954');
INSERT INTO `go_data_statistics` VALUES ('470', '10', '腾讯应用宝', 'qq', '', '7062', '1452601301');
INSERT INTO `go_data_statistics` VALUES ('471', '8', '一元众乐', 'yyzl', '', '7063', '1452601572');
INSERT INTO `go_data_statistics` VALUES ('472', '8', '一元众乐', 'yyzl', '', '7064', '1452601578');
INSERT INTO `go_data_statistics` VALUES ('473', '8', '一元众乐', 'yyzl', '', '7065', '1452601583');
INSERT INTO `go_data_statistics` VALUES ('474', '8', '一元众乐', 'yyzl', '', '7066', '1452601656');
INSERT INTO `go_data_statistics` VALUES ('475', '8', '一元众乐', 'yyzl', '', '7068', '1452601694');
INSERT INTO `go_data_statistics` VALUES ('476', '4', '百度推广', 'zlbdtg', 'SEM00000432', '7069', '1452601723');
INSERT INTO `go_data_statistics` VALUES ('477', '8', '一元众乐', 'yyzl', '', '7070', '1452601767');
INSERT INTO `go_data_statistics` VALUES ('478', '8', '一元众乐', 'yyzl', '', '7071', '1452601808');
INSERT INTO `go_data_statistics` VALUES ('479', '10', '腾讯应用宝', 'qq', '', '7072', '1452601950');
INSERT INTO `go_data_statistics` VALUES ('480', '8', '一元众乐', 'yyzl', '', '7073', '1452602036');
INSERT INTO `go_data_statistics` VALUES ('481', '4', '百度推广', 'zlbdtg', 'SEM00000115', '7074', '1452602106');
INSERT INTO `go_data_statistics` VALUES ('482', '8', '一元众乐', 'yyzl', '', '7075', '1452602128');
INSERT INTO `go_data_statistics` VALUES ('483', '8', '一元众乐', 'yyzl', '', '7076', '1452602282');
INSERT INTO `go_data_statistics` VALUES ('484', '8', '一元众乐', 'yyzl', '', '7077', '1452602375');
INSERT INTO `go_data_statistics` VALUES ('485', '8', '一元众乐', 'yyzl', '', '7079', '1452602439');
INSERT INTO `go_data_statistics` VALUES ('486', '8', '一元众乐', 'yyzl', '', '7080', '1452602439');
INSERT INTO `go_data_statistics` VALUES ('487', '4', '百度推广', 'zlbdtg', 'SEM00000115', '7082', '1452602793');
INSERT INTO `go_data_statistics` VALUES ('488', '8', '一元众乐', 'yyzl', '', '7083', '1452602876');
INSERT INTO `go_data_statistics` VALUES ('489', '4', '百度推广', 'zlbdtg', 'SEM00000122', '7084', '1452602917');
INSERT INTO `go_data_statistics` VALUES ('490', '8', '一元众乐', 'yyzl', '', '7085', '1452603299');
INSERT INTO `go_data_statistics` VALUES ('491', '8', '一元众乐', 'yyzl', '', '7086', '1452603314');
INSERT INTO `go_data_statistics` VALUES ('492', '8', '一元众乐', 'yyzl', '', '7087', '1452603334');
INSERT INTO `go_data_statistics` VALUES ('493', '6', '信号增强器', 'zlxhzqq', '', '7088', '1452603446');
INSERT INTO `go_data_statistics` VALUES ('494', '8', '一元众乐', 'yyzl', '', '7089', '1452603587');
INSERT INTO `go_data_statistics` VALUES ('495', '8', '一元众乐', 'yyzl', '', '7090', '1452603600');
INSERT INTO `go_data_statistics` VALUES ('496', '8', '一元众乐', 'yyzl', '', '7091', '1452603705');
INSERT INTO `go_data_statistics` VALUES ('497', '8', '一元众乐', 'yyzl', '', '7092', '1452603740');
INSERT INTO `go_data_statistics` VALUES ('498', '5', '酷赚锁屏', 'zlkzsp', '', '7095', '1452603934');
INSERT INTO `go_data_statistics` VALUES ('499', '4', '百度推广', 'zlbdtg', 'SEM00000122', '7096', '1452603942');
INSERT INTO `go_data_statistics` VALUES ('500', '8', '一元众乐', 'yyzl', '', '7097', '1452603961');
INSERT INTO `go_data_statistics` VALUES ('501', '8', '一元众乐', 'yyzl', '', '7098', '1452604000');
INSERT INTO `go_data_statistics` VALUES ('502', '8', '一元众乐', 'yyzl', '', '7100', '1452604204');
INSERT INTO `go_data_statistics` VALUES ('503', '8', '一元众乐', 'yyzl', '', '7102', '1452604275');
INSERT INTO `go_data_statistics` VALUES ('504', '8', '一元众乐', 'yyzl', '', '7103', '1452604284');
INSERT INTO `go_data_statistics` VALUES ('505', '8', '一元众乐', 'yyzl', '', '7104', '1452604314');
INSERT INTO `go_data_statistics` VALUES ('506', '8', '一元众乐', 'yyzl', '', '7105', '1452604321');
INSERT INTO `go_data_statistics` VALUES ('507', '8', '一元众乐', 'yyzl', '', '7106', '1452604349');
INSERT INTO `go_data_statistics` VALUES ('508', '8', '一元众乐', 'yyzl', '', '7107', '1452604488');
INSERT INTO `go_data_statistics` VALUES ('509', '8', '一元众乐', 'yyzl', '', '7108', '1452604598');
INSERT INTO `go_data_statistics` VALUES ('510', '8', '一元众乐', 'yyzl', '', '7109', '1452604604');
INSERT INTO `go_data_statistics` VALUES ('511', '8', '一元众乐', 'yyzl', '', '7110', '1452604622');
INSERT INTO `go_data_statistics` VALUES ('512', '8', '一元众乐', 'yyzl', '', '7111', '1452604843');
INSERT INTO `go_data_statistics` VALUES ('513', '8', '一元众乐', 'yyzl', '', '7112', '1452604998');
INSERT INTO `go_data_statistics` VALUES ('514', '8', '一元众乐', 'yyzl', '', '7114', '1452605103');
INSERT INTO `go_data_statistics` VALUES ('515', '4', '百度推广', 'zlbdtg', 'SEM00000122', '7115', '1452605171');
INSERT INTO `go_data_statistics` VALUES ('516', '6', '信号增强器', 'zlxhzqq', '', '7116', '1452605196');
INSERT INTO `go_data_statistics` VALUES ('517', '8', '一元众乐', 'yyzl', '', '7117', '1452605221');
INSERT INTO `go_data_statistics` VALUES ('518', '8', '一元众乐', 'yyzl', '', '7118', '1452605513');
INSERT INTO `go_data_statistics` VALUES ('519', '8', '一元众乐', 'yyzl', '', '7119', '1452605641');
INSERT INTO `go_data_statistics` VALUES ('520', '8', '一元众乐', 'yyzl', '', '7120', '1452605651');
INSERT INTO `go_data_statistics` VALUES ('521', '8', '一元众乐', 'yyzl', '', '7124', '1452605678');
INSERT INTO `go_data_statistics` VALUES ('522', '8', '一元众乐', 'yyzl', '', '7125', '1452605686');
INSERT INTO `go_data_statistics` VALUES ('523', '8', '一元众乐', 'yyzl', '', '7126', '1452605740');
INSERT INTO `go_data_statistics` VALUES ('524', '6', '信号增强器', 'zlxhzqq', '', '7127', '1452605759');
INSERT INTO `go_data_statistics` VALUES ('525', '8', '一元众乐', 'yyzl', '', '7128', '1452605851');
INSERT INTO `go_data_statistics` VALUES ('526', '8', '一元众乐', 'yyzl', '', '7129', '1452605858');
INSERT INTO `go_data_statistics` VALUES ('527', '8', '一元众乐', 'yyzl', '', '7130', '1452605886');
INSERT INTO `go_data_statistics` VALUES ('528', '8', '一元众乐', 'yyzl', '', '7131', '1452605963');
INSERT INTO `go_data_statistics` VALUES ('529', '8', '一元众乐', 'yyzl', '', '7133', '1452606183');
INSERT INTO `go_data_statistics` VALUES ('530', '8', '一元众乐', 'yyzl', '', '7134', '1452606249');
INSERT INTO `go_data_statistics` VALUES ('531', '8', '一元众乐', 'yyzl', '', '7135', '1452606336');
INSERT INTO `go_data_statistics` VALUES ('532', '4', '百度推广', 'zlbdtg', 'SEM00000115', '7137', '1452606358');
INSERT INTO `go_data_statistics` VALUES ('533', '4', '百度推广', 'zlbdtg', 'SEM00000118', '7138', '1452606376');
INSERT INTO `go_data_statistics` VALUES ('534', '8', '一元众乐', 'yyzl', '', '7140', '1452606533');
INSERT INTO `go_data_statistics` VALUES ('535', '8', '一元众乐', 'yyzl', '', '7141', '1452606688');
INSERT INTO `go_data_statistics` VALUES ('536', '8', '一元众乐', 'yyzl', '', '7142', '1452606786');
INSERT INTO `go_data_statistics` VALUES ('537', '8', '一元众乐', 'yyzl', '', '7143', '1452606911');
INSERT INTO `go_data_statistics` VALUES ('538', '8', '一元众乐', 'yyzl', '', '7144', '1452606939');
INSERT INTO `go_data_statistics` VALUES ('539', '6', '信号增强器', 'zlxhzqq', '', '7145', '1452607043');
INSERT INTO `go_data_statistics` VALUES ('540', '8', '一元众乐', 'yyzl', '', '7147', '1452607106');
INSERT INTO `go_data_statistics` VALUES ('541', '8', '一元众乐', 'yyzl', '', '7148', '1452607155');
INSERT INTO `go_data_statistics` VALUES ('542', '8', '一元众乐', 'yyzl', '', '7149', '1452607205');
INSERT INTO `go_data_statistics` VALUES ('543', '8', '一元众乐', 'yyzl', '', '7150', '1452607285');
INSERT INTO `go_data_statistics` VALUES ('544', '4', '百度推广', 'zlbdtg', 'SEM00000121', '7151', '1452607300');
INSERT INTO `go_data_statistics` VALUES ('545', '8', '一元众乐', 'yyzl', '', '7153', '1452607805');
INSERT INTO `go_data_statistics` VALUES ('546', '8', '一元众乐', 'yyzl', '', '7154', '1452607854');
INSERT INTO `go_data_statistics` VALUES ('547', '6', '信号增强器', 'zlxhzqq', '', '7155', '1452608180');
INSERT INTO `go_data_statistics` VALUES ('548', '8', '一元众乐', 'yyzl', '', '7156', '1452608233');
INSERT INTO `go_data_statistics` VALUES ('549', '8', '一元众乐', 'yyzl', '', '7157', '1452608368');
INSERT INTO `go_data_statistics` VALUES ('550', '8', '一元众乐', 'yyzl', '', '7159', '1452608582');
INSERT INTO `go_data_statistics` VALUES ('551', '10', '腾讯应用宝', 'qq', '', '7160', '1452608585');
INSERT INTO `go_data_statistics` VALUES ('552', '8', '一元众乐', 'yyzl', '', '7161', '1452608639');
INSERT INTO `go_data_statistics` VALUES ('553', '8', '一元众乐', 'yyzl', '', '7162', '1452608640');
INSERT INTO `go_data_statistics` VALUES ('554', '8', '一元众乐', 'yyzl', '', '7163', '1452608657');
INSERT INTO `go_data_statistics` VALUES ('555', '8', '一元众乐', 'yyzl', '', '7164', '1452608838');
INSERT INTO `go_data_statistics` VALUES ('556', '8', '一元众乐', 'yyzl', '', '7165', '1452608970');
INSERT INTO `go_data_statistics` VALUES ('557', '4', '百度推广', 'zlbdtg', 'SEM00000121', '7166', '1452608980');
INSERT INTO `go_data_statistics` VALUES ('558', '8', '一元众乐', 'yyzl', '', '7167', '1452608999');
INSERT INTO `go_data_statistics` VALUES ('559', '4', '百度推广', 'zlbdtg', 'SEM00000122', '7168', '1452609101');
INSERT INTO `go_data_statistics` VALUES ('560', '6', '信号增强器', 'zlxhzqq', '', '7169', '1452609176');
INSERT INTO `go_data_statistics` VALUES ('561', '10', '腾讯应用宝', 'qq', '', '7170', '1452609181');
INSERT INTO `go_data_statistics` VALUES ('562', '8', '一元众乐', 'yyzl', '', '7171', '1452609216');
INSERT INTO `go_data_statistics` VALUES ('563', '8', '一元众乐', 'yyzl', '', '7172', '1452609357');
INSERT INTO `go_data_statistics` VALUES ('564', '4', '百度推广', 'zlbdtg', 'SEM00000114', '7173', '1452609419');
INSERT INTO `go_data_statistics` VALUES ('565', '8', '一元众乐', 'yyzl', '', '7174', '1452609469');
INSERT INTO `go_data_statistics` VALUES ('566', '4', '百度推广', 'zlbdtg', 'SEM00000117', '7175', '1452609472');
INSERT INTO `go_data_statistics` VALUES ('567', '8', '一元众乐', 'yyzl', '', '7176', '1452609579');
INSERT INTO `go_data_statistics` VALUES ('568', '8', '一元众乐', 'yyzl', '', '7177', '1452609640');
INSERT INTO `go_data_statistics` VALUES ('569', '8', '一元众乐', 'yyzl', '', '7180', '1452609715');
INSERT INTO `go_data_statistics` VALUES ('570', '10', '腾讯应用宝', 'qq', '', '7181', '1452609715');
INSERT INTO `go_data_statistics` VALUES ('571', '4', '百度推广', 'zlbdtg', 'SEM00000115', '7182', '1452609799');
INSERT INTO `go_data_statistics` VALUES ('572', '8', '一元众乐', 'yyzl', '', '7183', '1452609848');
INSERT INTO `go_data_statistics` VALUES ('573', '8', '一元众乐', 'yyzl', '', '7184', '1452610174');
INSERT INTO `go_data_statistics` VALUES ('574', '8', '一元众乐', 'yyzl', '', '7185', '1452610200');
INSERT INTO `go_data_statistics` VALUES ('575', '8', '一元众乐', 'yyzl', '', '7186', '1452610237');
INSERT INTO `go_data_statistics` VALUES ('576', '8', '一元众乐', 'yyzl', '', '7187', '1452610409');
INSERT INTO `go_data_statistics` VALUES ('577', '8', '一元众乐', 'yyzl', '', '7188', '1452610410');
INSERT INTO `go_data_statistics` VALUES ('578', '10', '腾讯应用宝', 'qq', '', '7190', '1452610442');
INSERT INTO `go_data_statistics` VALUES ('579', '8', '一元众乐', 'yyzl', '', '7191', '1452610729');
INSERT INTO `go_data_statistics` VALUES ('580', '4', '百度推广', 'zlbdtg', 'SEM00000118', '7192', '1452610731');
INSERT INTO `go_data_statistics` VALUES ('581', '8', '一元众乐', 'yyzl', '', '7193', '1452610742');
INSERT INTO `go_data_statistics` VALUES ('582', '8', '一元众乐', 'yyzl', '', '7194', '1452610754');
INSERT INTO `go_data_statistics` VALUES ('583', '8', '一元众乐', 'yyzl', '', '7195', '1452610879');
INSERT INTO `go_data_statistics` VALUES ('584', '8', '一元众乐', 'yyzl', '', '7196', '1452610932');
INSERT INTO `go_data_statistics` VALUES ('585', '8', '一元众乐', 'yyzl', '', '7198', '1452611146');
INSERT INTO `go_data_statistics` VALUES ('586', '8', '一元众乐', 'yyzl', '', '7199', '1452611149');
INSERT INTO `go_data_statistics` VALUES ('587', '8', '一元众乐', 'yyzl', '', '7200', '1452611207');
INSERT INTO `go_data_statistics` VALUES ('588', '8', '一元众乐', 'yyzl', '', '7201', '1452611229');
INSERT INTO `go_data_statistics` VALUES ('589', '8', '一元众乐', 'yyzl', '', '7202', '1452611300');
INSERT INTO `go_data_statistics` VALUES ('590', '8', '一元众乐', 'yyzl', '', '7203', '1452611574');
INSERT INTO `go_data_statistics` VALUES ('591', '8', '一元众乐', 'yyzl', '', '7204', '1452611675');
INSERT INTO `go_data_statistics` VALUES ('592', '6', '信号增强器', 'zlxhzqq', '', '7205', '1452611743');
INSERT INTO `go_data_statistics` VALUES ('593', '8', '一元众乐', 'yyzl', '', '7206', '1452611761');
INSERT INTO `go_data_statistics` VALUES ('594', '8', '一元众乐', 'yyzl', '', '7207', '1452611904');
INSERT INTO `go_data_statistics` VALUES ('595', '8', '一元众乐', 'yyzl', '', '7208', '1452611934');
INSERT INTO `go_data_statistics` VALUES ('596', '8', '一元众乐', 'yyzl', '', '7209', '1452612100');
INSERT INTO `go_data_statistics` VALUES ('597', '8', '一元众乐', 'yyzl', '', '7210', '1452612167');
INSERT INTO `go_data_statistics` VALUES ('598', '4', '百度推广', 'zlbdtg', 'SEM00000115', '7211', '1452612180');
INSERT INTO `go_data_statistics` VALUES ('599', '8', '一元众乐', 'yyzl', '', '7212', '1452612199');
INSERT INTO `go_data_statistics` VALUES ('600', '8', '一元众乐', 'yyzl', '', '7213', '1452612243');
INSERT INTO `go_data_statistics` VALUES ('601', '8', '一元众乐', 'yyzl', '', '7214', '1452612257');
INSERT INTO `go_data_statistics` VALUES ('602', '4', '百度推广', 'zlbdtg', 'SEM00000122', '7216', '1452612598');
INSERT INTO `go_data_statistics` VALUES ('603', '8', '一元众乐', 'yyzl', '', '7217', '1452612605');
INSERT INTO `go_data_statistics` VALUES ('604', '8', '一元众乐', 'yyzl', '', '7218', '1452612812');
INSERT INTO `go_data_statistics` VALUES ('605', '8', '一元众乐', 'yyzl', '', '7219', '1452613044');
INSERT INTO `go_data_statistics` VALUES ('606', '8', '一元众乐', 'yyzl', '', '7221', '1452613132');
INSERT INTO `go_data_statistics` VALUES ('607', '8', '一元众乐', 'yyzl', '', '7224', '1452613339');
INSERT INTO `go_data_statistics` VALUES ('608', '8', '一元众乐', 'yyzl', '', '7226', '1452614011');
INSERT INTO `go_data_statistics` VALUES ('609', '8', '一元众乐', 'yyzl', '', '7229', '1452614364');
INSERT INTO `go_data_statistics` VALUES ('610', '8', '一元众乐', 'yyzl', '', '7230', '1452614402');
INSERT INTO `go_data_statistics` VALUES ('611', '4', '百度推广', 'zlbdtg', 'SEM00000115', '7231', '1452614408');
INSERT INTO `go_data_statistics` VALUES ('612', '8', '一元众乐', 'yyzl', '', '7232', '1452614511');
INSERT INTO `go_data_statistics` VALUES ('613', '4', '百度推广', 'zlbdtg', 'SEM00000118', '7233', '1452614657');
INSERT INTO `go_data_statistics` VALUES ('614', '4', '百度推广', 'zlbdtg', 'SEM00000121', '7234', '1452614733');
INSERT INTO `go_data_statistics` VALUES ('615', '8', '一元众乐', 'yyzl', '', '7236', '1452614785');
INSERT INTO `go_data_statistics` VALUES ('616', '8', '一元众乐', 'yyzl', '', '7237', '1452614799');
INSERT INTO `go_data_statistics` VALUES ('617', '8', '一元众乐', 'yyzl', '', '7238', '1452614862');
INSERT INTO `go_data_statistics` VALUES ('618', '4', '百度推广', 'zlbdtg', 'SEM00000118', '7239', '1452614951');
INSERT INTO `go_data_statistics` VALUES ('619', '8', '一元众乐', 'yyzl', '', '7240', '1452615261');
INSERT INTO `go_data_statistics` VALUES ('620', '4', '百度推广', 'zlbdtg', 'SEM00000121', '7241', '1452615535');
INSERT INTO `go_data_statistics` VALUES ('621', '8', '一元众乐', 'yyzl', '', '7242', '1452615546');
INSERT INTO `go_data_statistics` VALUES ('622', '8', '一元众乐', 'yyzl', '', '7243', '1452615614');
INSERT INTO `go_data_statistics` VALUES ('623', '8', '一元众乐', 'yyzl', '', '7245', '1452616033');
INSERT INTO `go_data_statistics` VALUES ('624', '4', '百度推广', 'zlbdtg', 'SEM00000118', '7246', '1452616635');
INSERT INTO `go_data_statistics` VALUES ('625', '8', '一元众乐', 'yyzl', '', '7247', '1452616642');
INSERT INTO `go_data_statistics` VALUES ('626', '8', '一元众乐', 'yyzl', '', '7248', '1452616813');
INSERT INTO `go_data_statistics` VALUES ('627', '8', '一元众乐', 'yyzl', '', '7249', '1452616973');
INSERT INTO `go_data_statistics` VALUES ('628', '6', '信号增强器', 'zlxhzqq', '', '7252', '1452617024');
INSERT INTO `go_data_statistics` VALUES ('629', '4', '百度推广', 'zlbdtg', 'SEM00000115', '7253', '1452617042');
INSERT INTO `go_data_statistics` VALUES ('630', '8', '一元众乐', 'yyzl', '', '7256', '1452618000');
INSERT INTO `go_data_statistics` VALUES ('631', '8', '一元众乐', 'yyzl', '', '7258', '1452618325');
INSERT INTO `go_data_statistics` VALUES ('632', '4', '百度推广', 'zlbdtg', 'SEM00000118', '7259', '1452618342');
INSERT INTO `go_data_statistics` VALUES ('633', '8', '一元众乐', 'yyzl', '', '7260', '1452619573');
INSERT INTO `go_data_statistics` VALUES ('634', '8', '一元众乐', 'yyzl', '', '7261', '1452621707');
INSERT INTO `go_data_statistics` VALUES ('635', '8', '一元众乐', 'yyzl', '', '7262', '1452622660');
INSERT INTO `go_data_statistics` VALUES ('636', '8', '一元众乐', 'yyzl', '', '7263', '1452623188');
INSERT INTO `go_data_statistics` VALUES ('637', '8', '一元众乐', 'yyzl', '', '7264', '1452623327');
INSERT INTO `go_data_statistics` VALUES ('638', '8', '一元众乐', 'yyzl', '', '7265', '1452624729');
INSERT INTO `go_data_statistics` VALUES ('639', '4', '百度推广', 'zlbdtg', 'SEM00000115', '7266', '1452627910');
INSERT INTO `go_data_statistics` VALUES ('640', '6', '信号增强器', 'zlxhzqq', '', '7267', '1452635849');
INSERT INTO `go_data_statistics` VALUES ('641', '8', '一元众乐', 'yyzl', '', '7268', '1452636585');
INSERT INTO `go_data_statistics` VALUES ('642', '8', '一元众乐', 'yyzl', '', '7269', '1452638642');
INSERT INTO `go_data_statistics` VALUES ('643', '8', '一元众乐', 'yyzl', '', '7270', '1452638778');
INSERT INTO `go_data_statistics` VALUES ('644', '8', '一元众乐', 'yyzl', '', '7271', '1452638805');
INSERT INTO `go_data_statistics` VALUES ('645', '8', '一元众乐', 'yyzl', '', '7272', '1452639434');
INSERT INTO `go_data_statistics` VALUES ('646', '8', '一元众乐', 'yyzl', '', '7273', '1452640148');
INSERT INTO `go_data_statistics` VALUES ('647', '6', '信号增强器', 'zlxhzqq', '', '7274', '1452640326');
INSERT INTO `go_data_statistics` VALUES ('648', '8', '一元众乐', 'yyzl', '', '7275', '1452640818');
INSERT INTO `go_data_statistics` VALUES ('649', '8', '一元众乐', 'yyzl', '', '7276', '1452640845');
INSERT INTO `go_data_statistics` VALUES ('650', '6', '信号增强器', 'zlxhzqq', '', '7277', '1452641845');
INSERT INTO `go_data_statistics` VALUES ('651', '6', '信号增强器', 'zlxhzqq', '', '7278', '1452641995');
INSERT INTO `go_data_statistics` VALUES ('652', '8', '一元众乐', 'yyzl', '', '7279', '1452641998');
INSERT INTO `go_data_statistics` VALUES ('653', '8', '一元众乐', 'yyzl', '', '7280', '1452643111');
INSERT INTO `go_data_statistics` VALUES ('654', '8', '一元众乐', 'yyzl', '', '7281', '1452643136');
INSERT INTO `go_data_statistics` VALUES ('655', '6', '信号增强器', 'zlxhzqq', '', '7283', '1452644064');
INSERT INTO `go_data_statistics` VALUES ('656', '9', '午夜艳视', 'wuyeyanshi', '', '7284', '1452644144');
INSERT INTO `go_data_statistics` VALUES ('657', '10', '腾讯应用宝', 'qq', '', '7285', '1452644246');
INSERT INTO `go_data_statistics` VALUES ('658', '8', '一元众乐', 'yyzl', '', '7286', '1452644628');
INSERT INTO `go_data_statistics` VALUES ('659', '8', '一元众乐', 'yyzl', '', '7287', '1452645776');
INSERT INTO `go_data_statistics` VALUES ('660', '4', '百度推广', 'zlbdtg', 'SEM00000121', '7288', '1452645828');
INSERT INTO `go_data_statistics` VALUES ('661', '8', '一元众乐', 'yyzl', '', '7290', '1452646135');
INSERT INTO `go_data_statistics` VALUES ('662', '8', '一元众乐', 'yyzl', '', '7291', '1452646186');
INSERT INTO `go_data_statistics` VALUES ('663', '8', '一元众乐', 'yyzl', '', '7292', '1452646258');
INSERT INTO `go_data_statistics` VALUES ('664', '4', '百度推广', 'zlbdtg', 'SEM00000122', '7293', '1452646550');
INSERT INTO `go_data_statistics` VALUES ('665', '6', '信号增强器', 'zlxhzqq', '', '7294', '1452646647');
INSERT INTO `go_data_statistics` VALUES ('666', '8', '一元众乐', 'yyzl', '', '7295', '1452646850');
INSERT INTO `go_data_statistics` VALUES ('667', '5', '酷赚锁屏', 'zlkzsp', '', '7297', '1452646960');
INSERT INTO `go_data_statistics` VALUES ('668', '5', '酷赚锁屏', 'zlkzsp', '', '7298', '1452646982');
INSERT INTO `go_data_statistics` VALUES ('669', '5', '酷赚锁屏', 'zlkzsp', '', '7299', '1452647009');
INSERT INTO `go_data_statistics` VALUES ('670', '5', '酷赚锁屏', 'zlkzsp', '', '7300', '1452647010');
INSERT INTO `go_data_statistics` VALUES ('671', '5', '酷赚锁屏', 'zlkzsp', '', '7301', '1452647024');
INSERT INTO `go_data_statistics` VALUES ('672', '5', '酷赚锁屏', 'zlkzsp', '', '7302', '1452647031');
INSERT INTO `go_data_statistics` VALUES ('673', '5', '酷赚锁屏', 'zlkzsp', '', '7303', '1452647032');
INSERT INTO `go_data_statistics` VALUES ('674', '5', '酷赚锁屏', 'zlkzsp', '', '7305', '1452647052');
INSERT INTO `go_data_statistics` VALUES ('675', '5', '酷赚锁屏', 'zlkzsp', '', '7306', '1452647054');
INSERT INTO `go_data_statistics` VALUES ('676', '5', '酷赚锁屏', 'zlkzsp', '', '7307', '1452647060');
INSERT INTO `go_data_statistics` VALUES ('677', '5', '酷赚锁屏', 'zlkzsp', '', '7308', '1452647060');
INSERT INTO `go_data_statistics` VALUES ('678', '5', '酷赚锁屏', 'zlkzsp', '', '7309', '1452647068');
INSERT INTO `go_data_statistics` VALUES ('679', '5', '酷赚锁屏', 'zlkzsp', '', '7310', '1452647071');
INSERT INTO `go_data_statistics` VALUES ('680', '5', '酷赚锁屏', 'zlkzsp', '', '7311', '1452647072');
INSERT INTO `go_data_statistics` VALUES ('681', '5', '酷赚锁屏', 'zlkzsp', '', '7312', '1452647080');
INSERT INTO `go_data_statistics` VALUES ('682', '5', '酷赚锁屏', 'zlkzsp', '', '7313', '1452647081');
INSERT INTO `go_data_statistics` VALUES ('683', '5', '酷赚锁屏', 'zlkzsp', '', '7314', '1452647082');
INSERT INTO `go_data_statistics` VALUES ('684', '6', '信号增强器', 'zlxhzqq', '', '7315', '1452647083');
INSERT INTO `go_data_statistics` VALUES ('685', '5', '酷赚锁屏', 'zlkzsp', '', '7316', '1452647084');
INSERT INTO `go_data_statistics` VALUES ('686', '5', '酷赚锁屏', 'zlkzsp', '', '7317', '1452647097');
INSERT INTO `go_data_statistics` VALUES ('687', '5', '酷赚锁屏', 'zlkzsp', '', '7318', '1452647098');
INSERT INTO `go_data_statistics` VALUES ('688', '5', '酷赚锁屏', 'zlkzsp', '', '7319', '1452647102');
INSERT INTO `go_data_statistics` VALUES ('689', '5', '酷赚锁屏', 'zlkzsp', '', '7320', '1452647112');
INSERT INTO `go_data_statistics` VALUES ('690', '5', '酷赚锁屏', 'zlkzsp', '', '7321', '1452647126');
INSERT INTO `go_data_statistics` VALUES ('691', '5', '酷赚锁屏', 'zlkzsp', '', '7322', '1452647134');
INSERT INTO `go_data_statistics` VALUES ('692', '5', '酷赚锁屏', 'zlkzsp', '', '7323', '1452647134');
INSERT INTO `go_data_statistics` VALUES ('693', '5', '酷赚锁屏', 'zlkzsp', '', '7324', '1452647136');
INSERT INTO `go_data_statistics` VALUES ('694', '5', '酷赚锁屏', 'zlkzsp', '', '7325', '1452647142');
INSERT INTO `go_data_statistics` VALUES ('695', '5', '酷赚锁屏', 'zlkzsp', '', '7326', '1452647146');
INSERT INTO `go_data_statistics` VALUES ('696', '5', '酷赚锁屏', 'zlkzsp', '', '7327', '1452647158');
INSERT INTO `go_data_statistics` VALUES ('697', '5', '酷赚锁屏', 'zlkzsp', '', '7328', '1452647170');
INSERT INTO `go_data_statistics` VALUES ('698', '5', '酷赚锁屏', 'zlkzsp', '', '7329', '1452647173');
INSERT INTO `go_data_statistics` VALUES ('699', '8', '一元众乐', 'yyzl', '', '7330', '1452647190');
INSERT INTO `go_data_statistics` VALUES ('700', '5', '酷赚锁屏', 'zlkzsp', '', '7331', '1452647196');
INSERT INTO `go_data_statistics` VALUES ('701', '5', '酷赚锁屏', 'zlkzsp', '', '7332', '1452647202');
INSERT INTO `go_data_statistics` VALUES ('702', '5', '酷赚锁屏', 'zlkzsp', '', '7333', '1452647206');
INSERT INTO `go_data_statistics` VALUES ('703', '5', '酷赚锁屏', 'zlkzsp', '', '7334', '1452647208');
INSERT INTO `go_data_statistics` VALUES ('704', '5', '酷赚锁屏', 'zlkzsp', '', '7335', '1452647221');
INSERT INTO `go_data_statistics` VALUES ('705', '5', '酷赚锁屏', 'zlkzsp', '', '7336', '1452647228');
INSERT INTO `go_data_statistics` VALUES ('706', '5', '酷赚锁屏', 'zlkzsp', '', '7337', '1452647235');
INSERT INTO `go_data_statistics` VALUES ('707', '5', '酷赚锁屏', 'zlkzsp', '', '7338', '1452647237');
INSERT INTO `go_data_statistics` VALUES ('708', '5', '酷赚锁屏', 'zlkzsp', '', '7339', '1452647250');
INSERT INTO `go_data_statistics` VALUES ('709', '5', '酷赚锁屏', 'zlkzsp', '', '7340', '1452647256');
INSERT INTO `go_data_statistics` VALUES ('710', '5', '酷赚锁屏', 'zlkzsp', '', '7341', '1452647258');
INSERT INTO `go_data_statistics` VALUES ('711', '5', '酷赚锁屏', 'zlkzsp', '', '7342', '1452647259');
INSERT INTO `go_data_statistics` VALUES ('712', '5', '酷赚锁屏', 'zlkzsp', '', '7343', '1452647261');
INSERT INTO `go_data_statistics` VALUES ('713', '5', '酷赚锁屏', 'zlkzsp', '', '7344', '1452647266');
INSERT INTO `go_data_statistics` VALUES ('714', '5', '酷赚锁屏', 'zlkzsp', '', '7345', '1452647267');
INSERT INTO `go_data_statistics` VALUES ('715', '5', '酷赚锁屏', 'zlkzsp', '', '7346', '1452647267');
INSERT INTO `go_data_statistics` VALUES ('716', '5', '酷赚锁屏', 'zlkzsp', '', '7347', '1452647274');
INSERT INTO `go_data_statistics` VALUES ('717', '5', '酷赚锁屏', 'zlkzsp', '', '7348', '1452647276');
INSERT INTO `go_data_statistics` VALUES ('718', '5', '酷赚锁屏', 'zlkzsp', '', '7349', '1452647282');
INSERT INTO `go_data_statistics` VALUES ('719', '5', '酷赚锁屏', 'zlkzsp', '', '7350', '1452647282');
INSERT INTO `go_data_statistics` VALUES ('720', '5', '酷赚锁屏', 'zlkzsp', '', '7351', '1452647307');
INSERT INTO `go_data_statistics` VALUES ('721', '5', '酷赚锁屏', 'zlkzsp', '', '7352', '1452647320');
INSERT INTO `go_data_statistics` VALUES ('722', '5', '酷赚锁屏', 'zlkzsp', '', '7353', '1452647327');
INSERT INTO `go_data_statistics` VALUES ('723', '5', '酷赚锁屏', 'zlkzsp', '', '7354', '1452647327');
INSERT INTO `go_data_statistics` VALUES ('724', '5', '酷赚锁屏', 'zlkzsp', '', '7355', '1452647341');
INSERT INTO `go_data_statistics` VALUES ('725', '5', '酷赚锁屏', 'zlkzsp', '', '7356', '1452647361');
INSERT INTO `go_data_statistics` VALUES ('726', '5', '酷赚锁屏', 'zlkzsp', '', '7357', '1452647374');
INSERT INTO `go_data_statistics` VALUES ('727', '5', '酷赚锁屏', 'zlkzsp', '', '7358', '1452647380');
INSERT INTO `go_data_statistics` VALUES ('728', '5', '酷赚锁屏', 'zlkzsp', '', '7359', '1452647408');
INSERT INTO `go_data_statistics` VALUES ('729', '5', '酷赚锁屏', 'zlkzsp', '', '7360', '1452647408');
INSERT INTO `go_data_statistics` VALUES ('730', '5', '酷赚锁屏', 'zlkzsp', '', '7361', '1452647420');
INSERT INTO `go_data_statistics` VALUES ('731', '5', '酷赚锁屏', 'zlkzsp', '', '7362', '1452647427');
INSERT INTO `go_data_statistics` VALUES ('732', '5', '酷赚锁屏', 'zlkzsp', '', '7363', '1452647451');
INSERT INTO `go_data_statistics` VALUES ('733', '5', '酷赚锁屏', 'zlkzsp', '', '7364', '1452647456');
INSERT INTO `go_data_statistics` VALUES ('734', '5', '酷赚锁屏', 'zlkzsp', '', '7365', '1452647457');
INSERT INTO `go_data_statistics` VALUES ('735', '5', '酷赚锁屏', 'zlkzsp', '', '7366', '1452647465');
INSERT INTO `go_data_statistics` VALUES ('736', '8', '一元众乐', 'yyzl', '', '7367', '1452647466');
INSERT INTO `go_data_statistics` VALUES ('737', '5', '酷赚锁屏', 'zlkzsp', '', '7368', '1452647473');
INSERT INTO `go_data_statistics` VALUES ('738', '5', '酷赚锁屏', 'zlkzsp', '', '7369', '1452647474');
INSERT INTO `go_data_statistics` VALUES ('739', '5', '酷赚锁屏', 'zlkzsp', '', '7370', '1452647478');
INSERT INTO `go_data_statistics` VALUES ('740', '5', '酷赚锁屏', 'zlkzsp', '', '7371', '1452647482');
INSERT INTO `go_data_statistics` VALUES ('741', '5', '酷赚锁屏', 'zlkzsp', '', '7372', '1452647489');
INSERT INTO `go_data_statistics` VALUES ('742', '5', '酷赚锁屏', 'zlkzsp', '', '7373', '1452647495');
INSERT INTO `go_data_statistics` VALUES ('743', '5', '酷赚锁屏', 'zlkzsp', '', '7374', '1452647496');
INSERT INTO `go_data_statistics` VALUES ('744', '5', '酷赚锁屏', 'zlkzsp', '', '7375', '1452647497');
INSERT INTO `go_data_statistics` VALUES ('745', '5', '酷赚锁屏', 'zlkzsp', '', '7376', '1452647501');
INSERT INTO `go_data_statistics` VALUES ('746', '5', '酷赚锁屏', 'zlkzsp', '', '7377', '1452647508');
INSERT INTO `go_data_statistics` VALUES ('747', '5', '酷赚锁屏', 'zlkzsp', '', '7378', '1452647515');
INSERT INTO `go_data_statistics` VALUES ('748', '5', '酷赚锁屏', 'zlkzsp', '', '7379', '1452647529');
INSERT INTO `go_data_statistics` VALUES ('749', '5', '酷赚锁屏', 'zlkzsp', '', '7380', '1452647541');
INSERT INTO `go_data_statistics` VALUES ('750', '5', '酷赚锁屏', 'zlkzsp', '', '7381', '1452647554');
INSERT INTO `go_data_statistics` VALUES ('751', '8', '一元众乐', 'yyzl', '', '7382', '1452647562');
INSERT INTO `go_data_statistics` VALUES ('752', '5', '酷赚锁屏', 'zlkzsp', '', '7383', '1452647585');
INSERT INTO `go_data_statistics` VALUES ('753', '5', '酷赚锁屏', 'zlkzsp', '', '7384', '1452647592');
INSERT INTO `go_data_statistics` VALUES ('754', '5', '酷赚锁屏', 'zlkzsp', '', '7385', '1452647595');
INSERT INTO `go_data_statistics` VALUES ('755', '5', '酷赚锁屏', 'zlkzsp', '', '7386', '1452647596');
INSERT INTO `go_data_statistics` VALUES ('756', '5', '酷赚锁屏', 'zlkzsp', '', '7387', '1452647604');
INSERT INTO `go_data_statistics` VALUES ('757', '5', '酷赚锁屏', 'zlkzsp', '', '7388', '1452647606');
INSERT INTO `go_data_statistics` VALUES ('758', '5', '酷赚锁屏', 'zlkzsp', '', '7389', '1452647613');
INSERT INTO `go_data_statistics` VALUES ('759', '5', '酷赚锁屏', 'zlkzsp', '', '7390', '1452647629');
INSERT INTO `go_data_statistics` VALUES ('760', '5', '酷赚锁屏', 'zlkzsp', '', '7391', '1452647630');
INSERT INTO `go_data_statistics` VALUES ('761', '5', '酷赚锁屏', 'zlkzsp', '', '7392', '1452647657');
INSERT INTO `go_data_statistics` VALUES ('762', '5', '酷赚锁屏', 'zlkzsp', '', '7393', '1452647658');
INSERT INTO `go_data_statistics` VALUES ('763', '5', '酷赚锁屏', 'zlkzsp', '', '7394', '1452647665');
INSERT INTO `go_data_statistics` VALUES ('764', '5', '酷赚锁屏', 'zlkzsp', '', '7395', '1452647707');
INSERT INTO `go_data_statistics` VALUES ('765', '5', '酷赚锁屏', 'zlkzsp', '', '7396', '1452647728');
INSERT INTO `go_data_statistics` VALUES ('766', '5', '酷赚锁屏', 'zlkzsp', '', '7397', '1452647732');
INSERT INTO `go_data_statistics` VALUES ('767', '5', '酷赚锁屏', 'zlkzsp', '', '7398', '1452647740');
INSERT INTO `go_data_statistics` VALUES ('768', '5', '酷赚锁屏', 'zlkzsp', '', '7399', '1452647754');
INSERT INTO `go_data_statistics` VALUES ('769', '5', '酷赚锁屏', 'zlkzsp', '', '7400', '1452647774');
INSERT INTO `go_data_statistics` VALUES ('770', '5', '酷赚锁屏', 'zlkzsp', '', '7401', '1452647807');
INSERT INTO `go_data_statistics` VALUES ('771', '5', '酷赚锁屏', 'zlkzsp', '', '7402', '1452647879');
INSERT INTO `go_data_statistics` VALUES ('772', '5', '酷赚锁屏', 'zlkzsp', '', '7403', '1452647886');
INSERT INTO `go_data_statistics` VALUES ('773', '5', '酷赚锁屏', 'zlkzsp', '', '7404', '1452647903');
INSERT INTO `go_data_statistics` VALUES ('774', '5', '酷赚锁屏', 'zlkzsp', '', '7405', '1452647985');
INSERT INTO `go_data_statistics` VALUES ('775', '5', '酷赚锁屏', 'zlkzsp', '', '7406', '1452648091');
INSERT INTO `go_data_statistics` VALUES ('776', '5', '酷赚锁屏', 'zlkzsp', '', '7407', '1452648108');
INSERT INTO `go_data_statistics` VALUES ('777', '5', '酷赚锁屏', 'zlkzsp', '', '7408', '1452648161');
INSERT INTO `go_data_statistics` VALUES ('778', '5', '酷赚锁屏', 'zlkzsp', '', '7409', '1452648288');
INSERT INTO `go_data_statistics` VALUES ('779', '5', '酷赚锁屏', 'zlkzsp', '', '7410', '1452648498');
INSERT INTO `go_data_statistics` VALUES ('780', '5', '酷赚锁屏', 'zlkzsp', '', '7411', '1452648537');
INSERT INTO `go_data_statistics` VALUES ('781', '5', '酷赚锁屏', 'zlkzsp', '', '7412', '1452648892');
INSERT INTO `go_data_statistics` VALUES ('782', '5', '酷赚锁屏', 'zlkzsp', '', '7413', '1452648942');
INSERT INTO `go_data_statistics` VALUES ('783', '8', '一元众乐', 'yyzl', '', '7414', '1452649768');
INSERT INTO `go_data_statistics` VALUES ('784', '8', '一元众乐', 'yyzl', '', '7415', '1452649910');
INSERT INTO `go_data_statistics` VALUES ('785', '8', '一元众乐', 'yyzl', '', '7416', '1452650257');
INSERT INTO `go_data_statistics` VALUES ('786', '5', '酷赚锁屏', 'zlkzsp', '', '7418', '1452650743');
INSERT INTO `go_data_statistics` VALUES ('787', '8', '一元众乐', 'yyzl', '', '7419', '1452651261');
INSERT INTO `go_data_statistics` VALUES ('788', '5', '酷赚锁屏', 'zlkzsp', '', '7420', '1452651313');
INSERT INTO `go_data_statistics` VALUES ('789', '8', '一元众乐', 'yyzl', '', '7421', '1452651598');
INSERT INTO `go_data_statistics` VALUES ('790', '4', '百度推广', 'zlbdtg', 'SEM00000115', '7422', '1452651619');
INSERT INTO `go_data_statistics` VALUES ('791', '8', '一元众乐', 'yyzl', '', '7423', '1452651793');
INSERT INTO `go_data_statistics` VALUES ('792', '8', '一元众乐', 'yyzl', '', '7424', '1452652061');
INSERT INTO `go_data_statistics` VALUES ('793', '8', '一元众乐', 'yyzl', '', '7425', '1452652149');
INSERT INTO `go_data_statistics` VALUES ('794', '8', '一元众乐', 'yyzl', '', '7426', '1452652556');
INSERT INTO `go_data_statistics` VALUES ('795', '8', '一元众乐', 'yyzl', '', '7427', '1452652885');
INSERT INTO `go_data_statistics` VALUES ('796', '8', '一元众乐', 'yyzl', '', '7429', '1452652910');
INSERT INTO `go_data_statistics` VALUES ('797', '4', '百度推广', 'zlbdtg', 'SEM00000115', '7430', '1452653356');
INSERT INTO `go_data_statistics` VALUES ('798', '8', '一元众乐', 'yyzl', '', '7438', '1452653517');
INSERT INTO `go_data_statistics` VALUES ('799', '8', '一元众乐', 'yyzl', '', '7439', '1452653902');
INSERT INTO `go_data_statistics` VALUES ('800', '8', '一元众乐', 'yyzl', '', '7440', '1452653907');
INSERT INTO `go_data_statistics` VALUES ('801', '4', '百度推广', 'zlbdtg', 'SEM00000115', '7441', '1452654452');
INSERT INTO `go_data_statistics` VALUES ('802', '4', '百度推广', 'zlbdtg', 'SEM00000115', '7442', '1452654452');
INSERT INTO `go_data_statistics` VALUES ('803', '8', '一元众乐', 'yyzl', '', '7444', '1452654988');
INSERT INTO `go_data_statistics` VALUES ('804', '8', '一元众乐', 'yyzl', '', '7445', '1452655274');
INSERT INTO `go_data_statistics` VALUES ('805', '6', '信号增强器', 'zlxhzqq', '', '7446', '1452655541');
INSERT INTO `go_data_statistics` VALUES ('806', '8', '一元众乐', 'yyzl', '', '7447', '1452655560');
INSERT INTO `go_data_statistics` VALUES ('807', '8', '一元众乐', 'yyzl', '', '7448', '1452655825');
INSERT INTO `go_data_statistics` VALUES ('808', '8', '一元众乐', 'yyzl', '', '7449', '1452655942');
INSERT INTO `go_data_statistics` VALUES ('809', '4', '百度推广', 'zlbdtg', 'SEM00000356', '7451', '1452656248');
INSERT INTO `go_data_statistics` VALUES ('810', '8', '一元众乐', 'yyzl', '', '7452', '1452656340');
INSERT INTO `go_data_statistics` VALUES ('811', '8', '一元众乐', 'yyzl', '', '7453', '1452656456');
INSERT INTO `go_data_statistics` VALUES ('812', '8', '一元众乐', 'yyzl', '', '7454', '1452656508');
INSERT INTO `go_data_statistics` VALUES ('813', '8', '一元众乐', 'yyzl', '', '7455', '1452656586');
INSERT INTO `go_data_statistics` VALUES ('814', '8', '一元众乐', 'yyzl', '', '7456', '1452656842');
INSERT INTO `go_data_statistics` VALUES ('815', '8', '一元众乐', 'yyzl', '', '7457', '1452656881');
INSERT INTO `go_data_statistics` VALUES ('816', '8', '一元众乐', 'yyzl', '', '7458', '1452657002');
INSERT INTO `go_data_statistics` VALUES ('817', '8', '一元众乐', 'yyzl', '', '7460', '1452657214');
INSERT INTO `go_data_statistics` VALUES ('818', '8', '一元众乐', 'yyzl', '', '7461', '1452657293');
INSERT INTO `go_data_statistics` VALUES ('819', '8', '一元众乐', 'yyzl', '', '7462', '1452657566');
INSERT INTO `go_data_statistics` VALUES ('820', '8', '一元众乐', 'yyzl', '', '7464', '1452658167');
INSERT INTO `go_data_statistics` VALUES ('821', '8', '一元众乐', 'yyzl', '', '7465', '1452658217');
INSERT INTO `go_data_statistics` VALUES ('822', '8', '一元众乐', 'yyzl', '', '7466', '1452658219');
INSERT INTO `go_data_statistics` VALUES ('823', '8', '一元众乐', 'yyzl', '', '7468', '1452658428');
INSERT INTO `go_data_statistics` VALUES ('824', '8', '一元众乐', 'yyzl', '', '7469', '1452658767');
INSERT INTO `go_data_statistics` VALUES ('825', '6', '信号增强器', 'zlxhzqq', '', '7470', '1452658886');
INSERT INTO `go_data_statistics` VALUES ('826', '6', '信号增强器', 'zlxhzqq', '', '7471', '1452658936');
INSERT INTO `go_data_statistics` VALUES ('827', '8', '一元众乐', 'yyzl', '', '7472', '1452659349');
INSERT INTO `go_data_statistics` VALUES ('828', '8', '一元众乐', 'yyzl', '', '7474', '1452659498');
INSERT INTO `go_data_statistics` VALUES ('829', '8', '一元众乐', 'yyzl', '', '7475', '1452659576');
INSERT INTO `go_data_statistics` VALUES ('830', '8', '一元众乐', 'yyzl', '', '7477', '1452659700');
INSERT INTO `go_data_statistics` VALUES ('831', '4', '百度推广', 'zlbdtg', 'SEM00000118', '7478', '1452659886');
INSERT INTO `go_data_statistics` VALUES ('832', '8', '一元众乐', 'yyzl', '', '7479', '1452659957');
INSERT INTO `go_data_statistics` VALUES ('833', '8', '一元众乐', 'yyzl', '', '7480', '1452660040');
INSERT INTO `go_data_statistics` VALUES ('834', '8', '一元众乐', 'yyzl', '', '7481', '1452660121');
INSERT INTO `go_data_statistics` VALUES ('835', '8', '一元众乐', 'yyzl', '', '7482', '1452660290');
INSERT INTO `go_data_statistics` VALUES ('836', '8', '一元众乐', 'yyzl', '', '7483', '1452660360');
INSERT INTO `go_data_statistics` VALUES ('837', '8', '一元众乐', 'yyzl', '', '7484', '1452660508');
INSERT INTO `go_data_statistics` VALUES ('838', '8', '一元众乐', 'yyzl', '', '7486', '1452661089');
INSERT INTO `go_data_statistics` VALUES ('839', '8', '一元众乐', 'yyzl', '', '7487', '1452661220');
INSERT INTO `go_data_statistics` VALUES ('840', '8', '一元众乐', 'yyzl', '', '7488', '1452661735');
INSERT INTO `go_data_statistics` VALUES ('841', '8', '一元众乐', 'yyzl', '', '7489', '1452662223');
INSERT INTO `go_data_statistics` VALUES ('842', '8', '一元众乐', 'yyzl', '', '7491', '1452662625');
INSERT INTO `go_data_statistics` VALUES ('843', '6', '信号增强器', 'zlxhzqq', '', '7492', '1452662661');
INSERT INTO `go_data_statistics` VALUES ('844', '8', '一元众乐', 'yyzl', '', '7493', '1452662779');
INSERT INTO `go_data_statistics` VALUES ('845', '8', '一元众乐', 'yyzl', '', '7494', '1452663085');
INSERT INTO `go_data_statistics` VALUES ('846', '8', '一元众乐', 'yyzl', '', '7497', '1452663317');
INSERT INTO `go_data_statistics` VALUES ('847', '6', '信号增强器', 'zlxhzqq', '', '7498', '1452663747');
INSERT INTO `go_data_statistics` VALUES ('848', '8', '一元众乐', 'yyzl', '', '7499', '1452663809');
INSERT INTO `go_data_statistics` VALUES ('849', '8', '一元众乐', 'yyzl', '', '7500', '1452664007');
INSERT INTO `go_data_statistics` VALUES ('850', '8', '一元众乐', 'yyzl', '', '7502', '1452664921');
INSERT INTO `go_data_statistics` VALUES ('851', '4', '百度推广', 'zlbdtg', 'SEM00000115', '7503', '1452664925');
INSERT INTO `go_data_statistics` VALUES ('852', '4', '百度推广', 'zlbdtg', 'SEM00000122', '7504', '1452664995');
INSERT INTO `go_data_statistics` VALUES ('853', '4', '百度推广', 'zlbdtg', 'SEM00000115', '7505', '1452665022');
INSERT INTO `go_data_statistics` VALUES ('854', '4', '百度推广', 'zlbdtg', 'SEM00000122', '7507', '1452665697');
INSERT INTO `go_data_statistics` VALUES ('855', '8', '一元众乐', 'yyzl', '', '7508', '1452665734');
INSERT INTO `go_data_statistics` VALUES ('856', '8', '一元众乐', 'yyzl', '', '7509', '1452665827');
INSERT INTO `go_data_statistics` VALUES ('857', '8', '一元众乐', 'yyzl', '', '7510', '1452665926');
INSERT INTO `go_data_statistics` VALUES ('858', '8', '一元众乐', 'yyzl', '', '7511', '1452665946');
INSERT INTO `go_data_statistics` VALUES ('859', '4', '百度推广', 'zlbdtg', 'SEM00000114', '7516', '1452666422');
INSERT INTO `go_data_statistics` VALUES ('860', '6', '信号增强器', 'zlxhzqq', '', '7521', '1452666593');
INSERT INTO `go_data_statistics` VALUES ('861', '4', '百度推广', 'zlbdtg', 'SEM00000115', '7522', '1452666594');
INSERT INTO `go_data_statistics` VALUES ('862', '8', '一元众乐', 'yyzl', '', '7523', '1452666642');
INSERT INTO `go_data_statistics` VALUES ('863', '4', '百度推广', 'zlbdtg', 'SEM00000115', '7524', '1452666649');
INSERT INTO `go_data_statistics` VALUES ('864', '8', '一元众乐', 'yyzl', '', '7525', '1452666685');
INSERT INTO `go_data_statistics` VALUES ('865', '4', '百度推广', 'zlbdtg', 'SEM00000061', '7528', '1452666923');
INSERT INTO `go_data_statistics` VALUES ('866', '4', '百度推广', 'zlbdtg', 'SEM00000115', '7531', '1452667337');
INSERT INTO `go_data_statistics` VALUES ('867', '8', '一元众乐', 'yyzl', '', '7532', '1452667419');
INSERT INTO `go_data_statistics` VALUES ('868', '8', '一元众乐', 'yyzl', '', '7534', '1452667483');
INSERT INTO `go_data_statistics` VALUES ('869', '4', '百度推广', 'zlbdtg', 'SEM00000395', '7536', '1452667614');
INSERT INTO `go_data_statistics` VALUES ('870', '4', '百度推广', 'zlbdtg', 'SEM00000439', '7537', '1452667788');
INSERT INTO `go_data_statistics` VALUES ('871', '8', '一元众乐', 'yyzl', '', '7538', '1452668045');
INSERT INTO `go_data_statistics` VALUES ('872', '8', '一元众乐', 'yyzl', '', '7541', '1452668173');
INSERT INTO `go_data_statistics` VALUES ('873', '8', '一元众乐', 'yyzl', '', '7542', '1452668354');
INSERT INTO `go_data_statistics` VALUES ('874', '8', '一元众乐', 'yyzl', '', '7543', '1452668482');
INSERT INTO `go_data_statistics` VALUES ('875', '4', '百度推广', 'zlbdtg', 'SEM00000122', '7544', '1452668671');
INSERT INTO `go_data_statistics` VALUES ('876', '8', '一元众乐', 'yyzl', '', '7545', '1452668767');
INSERT INTO `go_data_statistics` VALUES ('877', '4', '百度推广', 'zlbdtg', 'SEM00000121', '7546', '1452668836');
INSERT INTO `go_data_statistics` VALUES ('878', '8', '一元众乐', 'yyzl', '', '7547', '1452668965');
INSERT INTO `go_data_statistics` VALUES ('879', '8', '一元众乐', 'yyzl', '', '7548', '1452669194');
INSERT INTO `go_data_statistics` VALUES ('880', '4', '百度推广', 'zlbdtg', 'SEM00000120', '7549', '1452669433');
INSERT INTO `go_data_statistics` VALUES ('881', '4', '百度推广', 'zlbdtg', 'SEM00000120', '7550', '1452669469');
INSERT INTO `go_data_statistics` VALUES ('882', '4', '百度推广', 'zlbdtg', 'SEM00000117', '7551', '1452669663');
INSERT INTO `go_data_statistics` VALUES ('883', '8', '一元众乐', 'yyzl', '', '7552', '1452669713');
INSERT INTO `go_data_statistics` VALUES ('884', '8', '一元众乐', 'yyzl', '', '7553', '1452670031');
INSERT INTO `go_data_statistics` VALUES ('885', '4', '百度推广', 'zlbdtg', 'SEM00000114', '7555', '1452670700');
INSERT INTO `go_data_statistics` VALUES ('886', '4', '百度推广', 'zlbdtg', 'SEM00000115', '7556', '1452670765');
INSERT INTO `go_data_statistics` VALUES ('887', '8', '一元众乐', 'yyzl', '', '7557', '1452670811');
INSERT INTO `go_data_statistics` VALUES ('888', '4', '百度推广', 'zlbdtg', 'SEM00000121', '7558', '1452670919');
INSERT INTO `go_data_statistics` VALUES ('889', '10', '腾讯应用宝', 'qq', '', '7559', '1452671308');
INSERT INTO `go_data_statistics` VALUES ('890', '4', '百度推广', 'zlbdtg', 'SEM00000121', '7561', '1452671991');
INSERT INTO `go_data_statistics` VALUES ('891', '8', '一元众乐', 'yyzl', '', '7562', '1452672087');
INSERT INTO `go_data_statistics` VALUES ('892', '8', '一元众乐', 'yyzl', '', '7563', '1452672241');
INSERT INTO `go_data_statistics` VALUES ('893', '8', '一元众乐', 'yyzl', '', '7564', '1452672284');
INSERT INTO `go_data_statistics` VALUES ('894', '4', '百度推广', 'zlbdtg', 'SEM00000163', '7565', '1452672392');
INSERT INTO `go_data_statistics` VALUES ('895', '8', '一元众乐', 'yyzl', '', '7566', '1452672464');
INSERT INTO `go_data_statistics` VALUES ('896', '8', '一元众乐', 'yyzl', '', '7569', '1452673163');
INSERT INTO `go_data_statistics` VALUES ('897', '8', '一元众乐', 'yyzl', '', '7570', '1452673195');
INSERT INTO `go_data_statistics` VALUES ('898', '8', '一元众乐', 'yyzl', '', '7571', '1452673413');
INSERT INTO `go_data_statistics` VALUES ('899', '8', '一元众乐', 'yyzl', '', '7574', '1452673627');
INSERT INTO `go_data_statistics` VALUES ('900', '8', '一元众乐', 'yyzl', '', '7577', '1452674280');
INSERT INTO `go_data_statistics` VALUES ('901', '8', '一元众乐', 'yyzl', '', '7578', '1452674727');
INSERT INTO `go_data_statistics` VALUES ('902', '6', '信号增强器', 'zlxhzqq', '', '7579', '1452675095');
INSERT INTO `go_data_statistics` VALUES ('903', '8', '一元众乐', 'yyzl', '', '7580', '1452675105');
INSERT INTO `go_data_statistics` VALUES ('904', '8', '一元众乐', 'yyzl', '', '7581', '1452675170');
INSERT INTO `go_data_statistics` VALUES ('905', '8', '一元众乐', 'yyzl', '', '7582', '1452675247');
INSERT INTO `go_data_statistics` VALUES ('906', '8', '一元众乐', 'yyzl', '', '7583', '1452675683');
INSERT INTO `go_data_statistics` VALUES ('907', '8', '一元众乐', 'yyzl', '', '7584', '1452676097');
INSERT INTO `go_data_statistics` VALUES ('908', '8', '一元众乐', 'yyzl', '', '7585', '1452676215');
INSERT INTO `go_data_statistics` VALUES ('909', '8', '一元众乐', 'yyzl', '', '7586', '1452676306');
INSERT INTO `go_data_statistics` VALUES ('910', '8', '一元众乐', 'yyzl', '', '7587', '1452676581');
INSERT INTO `go_data_statistics` VALUES ('911', '8', '一元众乐', 'yyzl', '', '7588', '1452677075');
INSERT INTO `go_data_statistics` VALUES ('912', '8', '一元众乐', 'yyzl', '', '7589', '1452677170');
INSERT INTO `go_data_statistics` VALUES ('913', '8', '一元众乐', 'yyzl', '', '7590', '1452677230');
INSERT INTO `go_data_statistics` VALUES ('914', '8', '一元众乐', 'yyzl', '', '7591', '1452677251');
INSERT INTO `go_data_statistics` VALUES ('915', '8', '一元众乐', 'yyzl', '', '7592', '1452677421');
INSERT INTO `go_data_statistics` VALUES ('916', '4', '百度推广', 'zlbdtg', 'SEM00000118', '7593', '1452677927');
INSERT INTO `go_data_statistics` VALUES ('917', '4', '百度推广', 'zlbdtg', 'SEM00000122', '7594', '1452678135');
INSERT INTO `go_data_statistics` VALUES ('918', '4', '百度推广', 'zlbdtg', 'SEM00000115', '7595', '1452678257');
INSERT INTO `go_data_statistics` VALUES ('919', '8', '一元众乐', 'yyzl', '', '7596', '1452678292');
INSERT INTO `go_data_statistics` VALUES ('920', '8', '一元众乐', 'yyzl', '', '7600', '1452678643');
INSERT INTO `go_data_statistics` VALUES ('921', '8', '一元众乐', 'yyzl', '', '7601', '1452679183');
INSERT INTO `go_data_statistics` VALUES ('922', '4', '百度推广', 'zlbdtg', 'SEM00000122', '7602', '1452679263');
INSERT INTO `go_data_statistics` VALUES ('923', '8', '一元众乐', 'yyzl', '', '7603', '1452679346');
INSERT INTO `go_data_statistics` VALUES ('924', '4', '百度推广', 'zlbdtg', 'SEM00000118', '7604', '1452679560');
INSERT INTO `go_data_statistics` VALUES ('925', '8', '一元众乐', 'yyzl', '', '7606', '1452679851');
INSERT INTO `go_data_statistics` VALUES ('926', '4', '百度推广', 'zlbdtg', 'SEM00000115', '7608', '1452679890');
INSERT INTO `go_data_statistics` VALUES ('927', '8', '一元众乐', 'yyzl', '', '7609', '1452680316');
INSERT INTO `go_data_statistics` VALUES ('928', '8', '一元众乐', 'yyzl', '', '7610', '1452680565');
INSERT INTO `go_data_statistics` VALUES ('929', '6', '信号增强器', 'zlxhzqq', '', '7611', '1452680624');
INSERT INTO `go_data_statistics` VALUES ('930', '4', '百度推广', 'zlbdtg', 'SEM00000115', '7612', '1452680844');
INSERT INTO `go_data_statistics` VALUES ('931', '8', '一元众乐', 'yyzl', '', '7613', '1452681203');
INSERT INTO `go_data_statistics` VALUES ('932', '6', '信号增强器', 'zlxhzqq', '', '7614', '1452681324');
INSERT INTO `go_data_statistics` VALUES ('933', '4', '百度推广', 'zlbdtg', 'SEM00000115', '7615', '1452681346');
INSERT INTO `go_data_statistics` VALUES ('934', '8', '一元众乐', 'yyzl', '', '7616', '1452681348');
INSERT INTO `go_data_statistics` VALUES ('935', '4', '百度推广', 'zlbdtg', 'SEM00000611', '7617', '1452681474');
INSERT INTO `go_data_statistics` VALUES ('936', '8', '一元众乐', 'yyzl', '', '7618', '1452681552');
INSERT INTO `go_data_statistics` VALUES ('937', '4', '百度推广', 'zlbdtg', 'SEM00000121', '7619', '1452681690');
INSERT INTO `go_data_statistics` VALUES ('938', '8', '一元众乐', 'yyzl', '', '7620', '1452681849');
INSERT INTO `go_data_statistics` VALUES ('939', '4', '百度推广', 'zlbdtg', 'SEM00000121', '7621', '1452682195');
INSERT INTO `go_data_statistics` VALUES ('940', '8', '一元众乐', 'yyzl', '', '7622', '1452682203');
INSERT INTO `go_data_statistics` VALUES ('941', '4', '百度推广', 'zlbdtg', 'SEM00000121', '7623', '1452682594');
INSERT INTO `go_data_statistics` VALUES ('942', '8', '一元众乐', 'yyzl', '', '7624', '1452682671');
INSERT INTO `go_data_statistics` VALUES ('943', '9', '午夜艳视', 'wuyeyanshi', '', '7625', '1452682861');
INSERT INTO `go_data_statistics` VALUES ('944', '4', '百度推广', 'zlbdtg', 'SEM00000582', '7626', '1452683108');
INSERT INTO `go_data_statistics` VALUES ('945', '4', '百度推广', 'zlbdtg', 'SEM00000626', '7627', '1452683246');
INSERT INTO `go_data_statistics` VALUES ('946', '9', '午夜艳视', 'wuyeyanshi', '', '7628', '1452683284');
INSERT INTO `go_data_statistics` VALUES ('947', '4', '百度推广', 'zlbdtg', 'SEM00000115', '7629', '1452683288');
INSERT INTO `go_data_statistics` VALUES ('948', '8', '一元众乐', 'yyzl', '', '7631', '1452683465');
INSERT INTO `go_data_statistics` VALUES ('949', '10', '腾讯应用宝', 'qq', '', '7632', '1452683851');
INSERT INTO `go_data_statistics` VALUES ('950', '10', '腾讯应用宝', 'qq', '', '7633', '1452683863');
INSERT INTO `go_data_statistics` VALUES ('951', '8', '一元众乐', 'yyzl', '', '7635', '1452684215');
INSERT INTO `go_data_statistics` VALUES ('952', '4', '百度推广', 'zlbdtg', 'SEM00000639', '7636', '1452684635');
INSERT INTO `go_data_statistics` VALUES ('953', '14', '360手机助手', 'dev360', '', '7639', '1452684963');
INSERT INTO `go_data_statistics` VALUES ('954', '8', '一元众乐', 'yyzl', '', '7640', '1452684994');
INSERT INTO `go_data_statistics` VALUES ('955', '8', '一元众乐', 'yyzl', '', '7641', '1452685060');
INSERT INTO `go_data_statistics` VALUES ('956', '8', '一元众乐', 'yyzl', '', '7642', '1452685084');
INSERT INTO `go_data_statistics` VALUES ('957', '8', '一元众乐', 'yyzl', '', '7643', '1452685107');
INSERT INTO `go_data_statistics` VALUES ('958', '4', '百度推广', 'zlbdtg', 'SEM00000626', '7644', '1452685141');
INSERT INTO `go_data_statistics` VALUES ('959', '8', '一元众乐', 'yyzl', '', '7645', '1452685164');
INSERT INTO `go_data_statistics` VALUES ('960', '4', '百度推广', 'zlbdtg', 'SEM00000623', '7646', '1452685166');
INSERT INTO `go_data_statistics` VALUES ('961', '4', '百度推广', 'zlbdtg', 'SEM00000122', '7647', '1452685171');
INSERT INTO `go_data_statistics` VALUES ('962', '8', '一元众乐', 'yyzl', '', '7648', '1452685193');
INSERT INTO `go_data_statistics` VALUES ('963', '4', '百度推广', 'zlbdtg', 'SEM00000118', '7650', '1452685274');
INSERT INTO `go_data_statistics` VALUES ('964', '4', '百度推广', 'zlbdtg', 'SEM00000582', '7651', '1452685901');
INSERT INTO `go_data_statistics` VALUES ('965', '8', '一元众乐', 'yyzl', '', '7655', '1452686304');
INSERT INTO `go_data_statistics` VALUES ('966', '8', '一元众乐', 'yyzl', '', '7656', '1452686378');
INSERT INTO `go_data_statistics` VALUES ('967', '4', '百度推广', 'zlbdtg', 'SEM00000626', '7657', '1452686854');
INSERT INTO `go_data_statistics` VALUES ('968', '8', '一元众乐', 'yyzl', '', '7658', '1452686893');
INSERT INTO `go_data_statistics` VALUES ('969', '8', '一元众乐', 'yyzl', '', '7659', '1452686980');
INSERT INTO `go_data_statistics` VALUES ('970', '8', '一元众乐', 'yyzl', '', '7661', '1452686991');
INSERT INTO `go_data_statistics` VALUES ('971', '8', '一元众乐', 'yyzl', '', '7662', '1452687125');
INSERT INTO `go_data_statistics` VALUES ('972', '8', '一元众乐', 'yyzl', '', '7663', '1452687135');
INSERT INTO `go_data_statistics` VALUES ('973', '4', '百度推广', 'zlbdtg', 'SEM00000627', '7665', '1452687220');
INSERT INTO `go_data_statistics` VALUES ('974', '4', '百度推广', 'zlbdtg', 'SEM00000121', '7666', '1452687232');
INSERT INTO `go_data_statistics` VALUES ('975', '8', '一元众乐', 'yyzl', '', '7667', '1452687237');
INSERT INTO `go_data_statistics` VALUES ('976', '9', '午夜艳视', 'wuyeyanshi', '', '7668', '1452687537');
INSERT INTO `go_data_statistics` VALUES ('977', '8', '一元众乐', 'yyzl', '', '7669', '1452687848');
INSERT INTO `go_data_statistics` VALUES ('978', '4', '百度推广', 'zlbdtg', 'SEM00000626', '7670', '1452688126');
INSERT INTO `go_data_statistics` VALUES ('979', '6', '信号增强器', 'zlxhzqq', '', '7671', '1452688260');
INSERT INTO `go_data_statistics` VALUES ('980', '4', '百度推广', 'zlbdtg', 'SEM00000115', '7675', '1452688617');
INSERT INTO `go_data_statistics` VALUES ('981', '4', '百度推广', 'zlbdtg', 'SEM00000122', '7676', '1452688772');
INSERT INTO `go_data_statistics` VALUES ('982', '8', '一元众乐', 'yyzl', '', '7677', '1452688864');
INSERT INTO `go_data_statistics` VALUES ('983', '8', '一元众乐', 'yyzl', '', '7678', '1452688884');
INSERT INTO `go_data_statistics` VALUES ('984', '8', '一元众乐', 'yyzl', '', '7680', '1452689104');
INSERT INTO `go_data_statistics` VALUES ('985', '8', '一元众乐', 'yyzl', '', '7682', '1452689115');
INSERT INTO `go_data_statistics` VALUES ('986', '6', '信号增强器', 'zlxhzqq', '', '7683', '1452689164');
INSERT INTO `go_data_statistics` VALUES ('987', '6', '信号增强器', 'zlxhzqq', '', '7684', '1452689184');
INSERT INTO `go_data_statistics` VALUES ('988', '4', '百度推广', 'zlbdtg', 'SEM00000115', '7685', '1452689264');
INSERT INTO `go_data_statistics` VALUES ('989', '8', '一元众乐', 'yyzl', '', '7686', '1452689271');
INSERT INTO `go_data_statistics` VALUES ('990', '9', '午夜艳视', 'wuyeyanshi', '', '7687', '1452689343');
INSERT INTO `go_data_statistics` VALUES ('991', '8', '一元众乐', 'yyzl', '', '7689', '1452689426');
INSERT INTO `go_data_statistics` VALUES ('992', '4', '百度推广', 'zlbdtg', 'SEM00000178', '7693', '1452689658');
INSERT INTO `go_data_statistics` VALUES ('993', '4', '百度推广', 'zlbdtg', 'SEM00000117', '7694', '1452689779');
INSERT INTO `go_data_statistics` VALUES ('994', '8', '一元众乐', 'yyzl', '', '7695', '1452689800');
INSERT INTO `go_data_statistics` VALUES ('995', '4', '百度推广', 'zlbdtg', 'SEM00000121', '7698', '1452689931');
INSERT INTO `go_data_statistics` VALUES ('996', '10', '腾讯应用宝', 'qq', '', '7699', '1452690235');
INSERT INTO `go_data_statistics` VALUES ('997', '4', '百度推广', 'zlbdtg', 'SEM00000573', '7700', '1452690264');
INSERT INTO `go_data_statistics` VALUES ('998', '4', '百度推广', 'zlbdtg', 'SEM00000582', '7702', '1452690470');
INSERT INTO `go_data_statistics` VALUES ('999', '8', '一元众乐', 'yyzl', '', '7703', '1452690596');
INSERT INTO `go_data_statistics` VALUES ('1000', '6', '信号增强器', 'zlxhzqq', '', '7704', '1452690629');
INSERT INTO `go_data_statistics` VALUES ('1001', '8', '一元众乐', 'yyzl', '', '7705', '1452690893');
INSERT INTO `go_data_statistics` VALUES ('1002', '8', '一元众乐', 'yyzl', '', '7709', '1452691263');
INSERT INTO `go_data_statistics` VALUES ('1003', '8', '一元众乐', 'yyzl', '', '7710', '1452691602');
INSERT INTO `go_data_statistics` VALUES ('1004', '4', '百度推广', 'zlbdtg', 'SEM00000626', '7712', '1452691906');
INSERT INTO `go_data_statistics` VALUES ('1005', '8', '一元众乐', 'yyzl', '', '7713', '1452691940');
INSERT INTO `go_data_statistics` VALUES ('1006', '4', '百度推广', 'zlbdtg', 'SEM00000121', '7714', '1452692022');
INSERT INTO `go_data_statistics` VALUES ('1007', '4', '百度推广', 'zlbdtg', 'SEM00000122', '7715', '1452692169');
INSERT INTO `go_data_statistics` VALUES ('1008', '4', '百度推广', 'zlbdtg', 'SEM00000605', '7716', '1452692355');
INSERT INTO `go_data_statistics` VALUES ('1009', '8', '一元众乐', 'yyzl', '', '7717', '1452692494');
INSERT INTO `go_data_statistics` VALUES ('1010', '4', '百度推广', 'zlbdtg', 'SEM00000115', '7718', '1452692552');
INSERT INTO `go_data_statistics` VALUES ('1011', '8', '一元众乐', 'yyzl', '', '7719', '1452692681');
INSERT INTO `go_data_statistics` VALUES ('1012', '4', '百度推广', 'zlbdtg', 'SEM00000121', '7720', '1452693052');
INSERT INTO `go_data_statistics` VALUES ('1013', '10', '腾讯应用宝', 'qq', '', '7721', '1452693255');
INSERT INTO `go_data_statistics` VALUES ('1014', '8', '一元众乐', 'yyzl', '', '7722', '1452693331');
INSERT INTO `go_data_statistics` VALUES ('1015', '4', '百度推广', 'zlbdtg', 'SEM00000115', '7723', '1452693392');
INSERT INTO `go_data_statistics` VALUES ('1016', '4', '百度推广', 'zlbdtg', 'SEM00000114', '7724', '1452693517');
INSERT INTO `go_data_statistics` VALUES ('1017', '8', '一元众乐', 'yyzl', '', '7725', '1452693658');
INSERT INTO `go_data_statistics` VALUES ('1018', '4', '百度推广', 'zlbdtg', 'SEM00000626', '7727', '1452693868');
INSERT INTO `go_data_statistics` VALUES ('1019', '8', '一元众乐', 'yyzl', '', '7729', '1452693958');
INSERT INTO `go_data_statistics` VALUES ('1020', '4', '百度推广', 'zlbdtg', 'SEM00000121', '7730', '1452694002');
INSERT INTO `go_data_statistics` VALUES ('1021', '4', '百度推广', 'zlbdtg', 'SEM00000115', '7731', '1452694062');
INSERT INTO `go_data_statistics` VALUES ('1022', '4', '百度推广', 'zlbdtg', 'SEM00000621', '7732', '1452694087');
INSERT INTO `go_data_statistics` VALUES ('1023', '4', '百度推广', 'zlbdtg', 'SEM00000122', '7733', '1452694131');
INSERT INTO `go_data_statistics` VALUES ('1024', '8', '一元众乐', 'yyzl', '', '7734', '1452694140');
INSERT INTO `go_data_statistics` VALUES ('1025', '4', '百度推广', 'zlbdtg', 'SEM00000121', '7735', '1452694200');
INSERT INTO `go_data_statistics` VALUES ('1026', '8', '一元众乐', 'yyzl', '', '7736', '1452694309');
INSERT INTO `go_data_statistics` VALUES ('1027', '8', '一元众乐', 'yyzl', '', '7737', '1452694370');
INSERT INTO `go_data_statistics` VALUES ('1028', '4', '百度推广', 'zlbdtg', 'SEM00000121', '7738', '1452694451');
INSERT INTO `go_data_statistics` VALUES ('1029', '8', '一元众乐', 'yyzl', '', '7739', '1452694622');
INSERT INTO `go_data_statistics` VALUES ('1030', '8', '一元众乐', 'yyzl', '', '7740', '1452694740');
INSERT INTO `go_data_statistics` VALUES ('1031', '8', '一元众乐', 'yyzl', '', '7742', '1452694810');
INSERT INTO `go_data_statistics` VALUES ('1032', '9', '午夜艳视', 'wuyeyanshi', '', '7743', '1452694835');
INSERT INTO `go_data_statistics` VALUES ('1033', '8', '一元众乐', 'yyzl', '', '7746', '1452694910');
INSERT INTO `go_data_statistics` VALUES ('1034', '4', '百度推广', 'zlbdtg', 'SEM00000601', '7747', '1452695221');
INSERT INTO `go_data_statistics` VALUES ('1035', '4', '百度推广', 'zlbdtg', 'SEM00000121', '7748', '1452695273');
INSERT INTO `go_data_statistics` VALUES ('1036', '8', '一元众乐', 'yyzl', '', '7749', '1452695401');
INSERT INTO `go_data_statistics` VALUES ('1037', '8', '一元众乐', 'yyzl', '', '7750', '1452695567');
INSERT INTO `go_data_statistics` VALUES ('1038', '4', '百度推广', 'zlbdtg', 'SEM00000626', '7752', '1452695992');
INSERT INTO `go_data_statistics` VALUES ('1039', '8', '一元众乐', 'yyzl', '', '7753', '1452696010');
INSERT INTO `go_data_statistics` VALUES ('1040', '8', '一元众乐', 'yyzl', '', '7754', '1452696033');
INSERT INTO `go_data_statistics` VALUES ('1041', '4', '百度推广', 'zlbdtg', 'SEM00000583', '7755', '1452696085');
INSERT INTO `go_data_statistics` VALUES ('1042', '8', '一元众乐', 'yyzl', '', '7756', '1452696441');
INSERT INTO `go_data_statistics` VALUES ('1043', '6', '信号增强器', 'zlxhzqq', '', '7757', '1452696470');
INSERT INTO `go_data_statistics` VALUES ('1044', '9', '午夜艳视', 'wuyeyanshi', '', '7758', '1452696544');
INSERT INTO `go_data_statistics` VALUES ('1045', '8', '一元众乐', 'yyzl', '', '7760', '1452696613');
INSERT INTO `go_data_statistics` VALUES ('1046', '8', '一元众乐', 'yyzl', '', '7761', '1452696649');
INSERT INTO `go_data_statistics` VALUES ('1047', '4', '百度推广', 'zlbdtg', 'SEM00000121', '7762', '1452696667');
INSERT INTO `go_data_statistics` VALUES ('1048', '6', '信号增强器', 'zlxhzqq', '', '7764', '1452696796');
INSERT INTO `go_data_statistics` VALUES ('1049', '4', '百度推广', 'zlbdtg', 'SEM00000575', '7765', '1452696799');
INSERT INTO `go_data_statistics` VALUES ('1050', '8', '一元众乐', 'yyzl', '', '7766', '1452697375');
INSERT INTO `go_data_statistics` VALUES ('1051', '4', '百度推广', 'zlbdtg', 'SEM00000121', '7767', '1452697383');
INSERT INTO `go_data_statistics` VALUES ('1052', '4', '百度推广', 'zlbdtg', 'SEM00000121', '7768', '1452697465');
INSERT INTO `go_data_statistics` VALUES ('1053', '8', '一元众乐', 'yyzl', '', '7769', '1452697609');
INSERT INTO `go_data_statistics` VALUES ('1054', '8', '一元众乐', 'yyzl', '', '7771', '1452698043');
INSERT INTO `go_data_statistics` VALUES ('1055', '8', '一元众乐', 'yyzl', '', '7772', '1452698140');
INSERT INTO `go_data_statistics` VALUES ('1056', '4', '百度推广', 'zlbdtg', 'SEM00000626', '7773', '1452698188');
INSERT INTO `go_data_statistics` VALUES ('1057', '8', '一元众乐', 'yyzl', '', '7774', '1452698729');
INSERT INTO `go_data_statistics` VALUES ('1058', '9', '午夜艳视', 'wuyeyanshi', '', '7775', '1452698801');
INSERT INTO `go_data_statistics` VALUES ('1059', '8', '一元众乐', 'yyzl', '', '7776', '1452699114');
INSERT INTO `go_data_statistics` VALUES ('1060', '8', '一元众乐', 'yyzl', '', '7779', '1452699570');
INSERT INTO `go_data_statistics` VALUES ('1061', '4', '百度推广', 'zlbdtg', 'SEM00000121', '7780', '1452699926');
INSERT INTO `go_data_statistics` VALUES ('1062', '8', '一元众乐', 'yyzl', '', '7782', '1452700091');
INSERT INTO `go_data_statistics` VALUES ('1063', '6', '信号增强器', 'zlxhzqq', '', '7783', '1452700305');
INSERT INTO `go_data_statistics` VALUES ('1064', '8', '一元众乐', 'yyzl', '', '7786', '1452700864');
INSERT INTO `go_data_statistics` VALUES ('1065', '4', '百度推广', 'zlbdtg', 'SEM00000121', '7787', '1452700952');
INSERT INTO `go_data_statistics` VALUES ('1066', '8', '一元众乐', 'yyzl', '', '7788', '1452700993');
INSERT INTO `go_data_statistics` VALUES ('1067', '8', '一元众乐', 'yyzl', '', '7789', '1452701091');
INSERT INTO `go_data_statistics` VALUES ('1068', '4', '百度推广', 'zlbdtg', 'SEM00000580', '7790', '1452701113');
INSERT INTO `go_data_statistics` VALUES ('1069', '8', '一元众乐', 'yyzl', '', '7791', '1452701279');
INSERT INTO `go_data_statistics` VALUES ('1070', '8', '一元众乐', 'yyzl', '', '7792', '1452701292');
INSERT INTO `go_data_statistics` VALUES ('1071', '8', '一元众乐', 'yyzl', '', '7793', '1452701299');
INSERT INTO `go_data_statistics` VALUES ('1072', '9', '午夜艳视', 'wuyeyanshi', '', '7794', '1452701734');
INSERT INTO `go_data_statistics` VALUES ('1073', '4', '百度推广', 'zlbdtg', 'SEM00000620', '7795', '1452701978');
INSERT INTO `go_data_statistics` VALUES ('1074', '8', '一元众乐', 'yyzl', '', '7796', '1452702623');
INSERT INTO `go_data_statistics` VALUES ('1075', '8', '一元众乐', 'yyzl', '', '7797', '1452702721');
INSERT INTO `go_data_statistics` VALUES ('1076', '6', '信号增强器', 'zlxhzqq', '', '7798', '1452702866');
INSERT INTO `go_data_statistics` VALUES ('1077', '8', '一元众乐', 'yyzl', '', '7799', '1452703684');
INSERT INTO `go_data_statistics` VALUES ('1078', '4', '百度推广', 'zlbdtg', 'SEM00000115', '7800', '1452703745');
INSERT INTO `go_data_statistics` VALUES ('1079', '8', '一元众乐', 'yyzl', '', '7801', '1452705380');
INSERT INTO `go_data_statistics` VALUES ('1080', '6', '信号增强器', 'zlxhzqq', '', '7802', '1452705961');
INSERT INTO `go_data_statistics` VALUES ('1081', '4', '百度推广', 'zlbdtg', 'SEM00000115', '7803', '1452708712');
INSERT INTO `go_data_statistics` VALUES ('1082', '6', '信号增强器', 'zlxhzqq', '', '7804', '1452712532');
INSERT INTO `go_data_statistics` VALUES ('1083', '4', '百度推广', 'zlbdtg', 'SEM00000121', '7805', '1452712846');
INSERT INTO `go_data_statistics` VALUES ('1084', '6', '信号增强器', 'zlxhzqq', '', '7807', '1452719888');
INSERT INTO `go_data_statistics` VALUES ('1085', '8', '一元众乐', 'yyzl', '', '7808', '1452720927');
INSERT INTO `go_data_statistics` VALUES ('1086', '8', '一元众乐', 'yyzl', '', '7810', '1452723550');
INSERT INTO `go_data_statistics` VALUES ('1087', '8', '一元众乐', 'yyzl', '', '7811', '1452725364');
INSERT INTO `go_data_statistics` VALUES ('1088', '6', '信号增强器', 'zlxhzqq', '', '7812', '1452725729');
INSERT INTO `go_data_statistics` VALUES ('1089', '8', '一元众乐', 'yyzl', '', '7813', '1452726334');
INSERT INTO `go_data_statistics` VALUES ('1090', '8', '一元众乐', 'yyzl', '', '7814', '1452726885');
INSERT INTO `go_data_statistics` VALUES ('1091', '8', '一元众乐', 'yyzl', '', '7815', '1452728279');
INSERT INTO `go_data_statistics` VALUES ('1092', '8', '一元众乐', 'yyzl', '', '7816', '1452729164');
INSERT INTO `go_data_statistics` VALUES ('1093', '6', '信号增强器', 'zlxhzqq', '', '7817', '1452729318');
INSERT INTO `go_data_statistics` VALUES ('1094', '9', '午夜艳视', 'wuyeyanshi', '', '7818', '1452729834');
INSERT INTO `go_data_statistics` VALUES ('1095', '8', '一元众乐', 'yyzl', '', '7819', '1452730167');
INSERT INTO `go_data_statistics` VALUES ('1096', '9', '午夜艳视', 'wuyeyanshi', '', '7820', '1452730371');
INSERT INTO `go_data_statistics` VALUES ('1097', '4', '百度推广', 'zlbdtg', 'SEM00000115', '7821', '1452730920');
INSERT INTO `go_data_statistics` VALUES ('1098', '8', '一元众乐', 'yyzl', '', '7822', '1452730937');
INSERT INTO `go_data_statistics` VALUES ('1099', '8', '一元众乐', 'yyzl', '', '7823', '1452730938');
INSERT INTO `go_data_statistics` VALUES ('1100', '8', '一元众乐', 'yyzl', '', '7824', '1452731113');
INSERT INTO `go_data_statistics` VALUES ('1101', '4', '百度推广', 'zlbdtg', 'SEM00000084', '7825', '1452731546');
INSERT INTO `go_data_statistics` VALUES ('1102', '6', '信号增强器', 'zlxhzqq', '', '7826', '1452731716');
INSERT INTO `go_data_statistics` VALUES ('1103', '9', '午夜艳视', 'wuyeyanshi', '', '7827', '1452732141');
INSERT INTO `go_data_statistics` VALUES ('1104', '8', '一元众乐', 'yyzl', '', '7828', '1452732288');
INSERT INTO `go_data_statistics` VALUES ('1105', '6', '信号增强器', 'zlxhzqq', '', '7829', '1452732661');
INSERT INTO `go_data_statistics` VALUES ('1106', '6', '信号增强器', 'zlxhzqq', '', '7830', '1452732837');
INSERT INTO `go_data_statistics` VALUES ('1107', '4', '百度推广', 'zlbdtg', 'SEM00000115', '7831', '1452733359');
INSERT INTO `go_data_statistics` VALUES ('1108', '8', '一元众乐', 'yyzl', '', '7832', '1452733476');
INSERT INTO `go_data_statistics` VALUES ('1109', '6', '信号增强器', 'zlxhzqq', '', '7834', '1452733773');
INSERT INTO `go_data_statistics` VALUES ('1110', '8', '一元众乐', 'yyzl', '', '7835', '1452734420');
INSERT INTO `go_data_statistics` VALUES ('1111', '8', '一元众乐', 'yyzl', '', '7836', '1452734501');
INSERT INTO `go_data_statistics` VALUES ('1112', '8', '一元众乐', 'yyzl', '', '7837', '1452734600');
INSERT INTO `go_data_statistics` VALUES ('1113', '8', '一元众乐', 'yyzl', '', '7840', '1452734988');
INSERT INTO `go_data_statistics` VALUES ('1114', '8', '一元众乐', 'yyzl', '', '7841', '1452735296');
INSERT INTO `go_data_statistics` VALUES ('1115', '8', '一元众乐', 'yyzl', '', '7842', '1452735326');
INSERT INTO `go_data_statistics` VALUES ('1116', '8', '一元众乐', 'yyzl', '', '7844', '1452736594');
INSERT INTO `go_data_statistics` VALUES ('1117', '8', '一元众乐', 'yyzl', '', '7845', '1452736595');
INSERT INTO `go_data_statistics` VALUES ('1118', '8', '一元众乐', 'yyzl', '', '7846', '1452736598');
INSERT INTO `go_data_statistics` VALUES ('1119', '8', '一元众乐', 'yyzl', '', '7847', '1452736800');
INSERT INTO `go_data_statistics` VALUES ('1120', '8', '一元众乐', 'yyzl', '', '7848', '1452736808');
INSERT INTO `go_data_statistics` VALUES ('1121', '5', '酷赚锁屏', 'zlkzsp', '', '7849', '1452736864');
INSERT INTO `go_data_statistics` VALUES ('1122', '5', '酷赚锁屏', 'zlkzsp', '', '7850', '1452736865');
INSERT INTO `go_data_statistics` VALUES ('1123', '5', '酷赚锁屏', 'zlkzsp', '', '7851', '1452736898');
INSERT INTO `go_data_statistics` VALUES ('1124', '5', '酷赚锁屏', 'zlkzsp', '', '7852', '1452736901');
INSERT INTO `go_data_statistics` VALUES ('1125', '5', '酷赚锁屏', 'zlkzsp', '', '7853', '1452736908');
INSERT INTO `go_data_statistics` VALUES ('1126', '5', '酷赚锁屏', 'zlkzsp', '', '7854', '1452737036');
INSERT INTO `go_data_statistics` VALUES ('1127', '5', '酷赚锁屏', 'zlkzsp', '', '7855', '1452737037');
INSERT INTO `go_data_statistics` VALUES ('1128', '9', '午夜艳视', 'wuyeyanshi', '', '7856', '1452737053');
INSERT INTO `go_data_statistics` VALUES ('1129', '5', '酷赚锁屏', 'zlkzsp', '', '7857', '1452737056');
INSERT INTO `go_data_statistics` VALUES ('1130', '5', '酷赚锁屏', 'zlkzsp', '', '7858', '1452737069');
INSERT INTO `go_data_statistics` VALUES ('1131', '5', '酷赚锁屏', 'zlkzsp', '', '7859', '1452737086');
INSERT INTO `go_data_statistics` VALUES ('1132', '5', '酷赚锁屏', 'zlkzsp', '', '7860', '1452737087');
INSERT INTO `go_data_statistics` VALUES ('1133', '5', '酷赚锁屏', 'zlkzsp', '', '7861', '1452737107');
INSERT INTO `go_data_statistics` VALUES ('1134', '5', '酷赚锁屏', 'zlkzsp', '', '7862', '1452737125');
INSERT INTO `go_data_statistics` VALUES ('1135', '5', '酷赚锁屏', 'zlkzsp', '', '7863', '1452737127');
INSERT INTO `go_data_statistics` VALUES ('1136', '5', '酷赚锁屏', 'zlkzsp', '', '7864', '1452737128');
INSERT INTO `go_data_statistics` VALUES ('1137', '5', '酷赚锁屏', 'zlkzsp', '', '7865', '1452737131');
INSERT INTO `go_data_statistics` VALUES ('1138', '5', '酷赚锁屏', 'zlkzsp', '', '7866', '1452737133');
INSERT INTO `go_data_statistics` VALUES ('1139', '19', '木蚂蚁', 'mumayi', '', '7868', '1452737138');
INSERT INTO `go_data_statistics` VALUES ('1140', '5', '酷赚锁屏', 'zlkzsp', '', '7869', '1452737145');
INSERT INTO `go_data_statistics` VALUES ('1141', '5', '酷赚锁屏', 'zlkzsp', '', '7870', '1452737154');
INSERT INTO `go_data_statistics` VALUES ('1142', '5', '酷赚锁屏', 'zlkzsp', '', '7871', '1452737156');
INSERT INTO `go_data_statistics` VALUES ('1143', '5', '酷赚锁屏', 'zlkzsp', '', '7872', '1452737178');
INSERT INTO `go_data_statistics` VALUES ('1144', '5', '酷赚锁屏', 'zlkzsp', '', '7873', '1452737179');
INSERT INTO `go_data_statistics` VALUES ('1145', '5', '酷赚锁屏', 'zlkzsp', '', '7874', '1452737195');
INSERT INTO `go_data_statistics` VALUES ('1146', '5', '酷赚锁屏', 'zlkzsp', '', '7875', '1452737208');
INSERT INTO `go_data_statistics` VALUES ('1147', '5', '酷赚锁屏', 'zlkzsp', '', '7876', '1452737225');
INSERT INTO `go_data_statistics` VALUES ('1148', '5', '酷赚锁屏', 'zlkzsp', '', '7877', '1452737230');
INSERT INTO `go_data_statistics` VALUES ('1149', '5', '酷赚锁屏', 'zlkzsp', '', '7878', '1452737239');
INSERT INTO `go_data_statistics` VALUES ('1150', '5', '酷赚锁屏', 'zlkzsp', '', '7879', '1452737244');
INSERT INTO `go_data_statistics` VALUES ('1151', '5', '酷赚锁屏', 'zlkzsp', '', '7880', '1452737252');
INSERT INTO `go_data_statistics` VALUES ('1152', '5', '酷赚锁屏', 'zlkzsp', '', '7881', '1452737252');
INSERT INTO `go_data_statistics` VALUES ('1153', '5', '酷赚锁屏', 'zlkzsp', '', '7882', '1452737303');
INSERT INTO `go_data_statistics` VALUES ('1154', '5', '酷赚锁屏', 'zlkzsp', '', '7883', '1452737318');
INSERT INTO `go_data_statistics` VALUES ('1155', '5', '酷赚锁屏', 'zlkzsp', '', '7884', '1452737331');
INSERT INTO `go_data_statistics` VALUES ('1156', '5', '酷赚锁屏', 'zlkzsp', '', '7885', '1452737338');
INSERT INTO `go_data_statistics` VALUES ('1157', '5', '酷赚锁屏', 'zlkzsp', '', '7886', '1452737342');
INSERT INTO `go_data_statistics` VALUES ('1158', '5', '酷赚锁屏', 'zlkzsp', '', '7887', '1452737361');
INSERT INTO `go_data_statistics` VALUES ('1159', '5', '酷赚锁屏', 'zlkzsp', '', '7888', '1452737374');
INSERT INTO `go_data_statistics` VALUES ('1160', '5', '酷赚锁屏', 'zlkzsp', '', '7889', '1452737390');
INSERT INTO `go_data_statistics` VALUES ('1161', '5', '酷赚锁屏', 'zlkzsp', '', '7890', '1452737397');
INSERT INTO `go_data_statistics` VALUES ('1162', '5', '酷赚锁屏', 'zlkzsp', '', '7891', '1452737401');
INSERT INTO `go_data_statistics` VALUES ('1163', '5', '酷赚锁屏', 'zlkzsp', '', '7892', '1452737405');
INSERT INTO `go_data_statistics` VALUES ('1164', '5', '酷赚锁屏', 'zlkzsp', '', '7893', '1452737427');
INSERT INTO `go_data_statistics` VALUES ('1165', '5', '酷赚锁屏', 'zlkzsp', '', '7894', '1452737432');
INSERT INTO `go_data_statistics` VALUES ('1166', '5', '酷赚锁屏', 'zlkzsp', '', '7895', '1452737443');
INSERT INTO `go_data_statistics` VALUES ('1167', '5', '酷赚锁屏', 'zlkzsp', '', '7896', '1452737450');
INSERT INTO `go_data_statistics` VALUES ('1168', '5', '酷赚锁屏', 'zlkzsp', '', '7897', '1452737472');
INSERT INTO `go_data_statistics` VALUES ('1169', '5', '酷赚锁屏', 'zlkzsp', '', '7898', '1452737472');
INSERT INTO `go_data_statistics` VALUES ('1170', '5', '酷赚锁屏', 'zlkzsp', '', '7899', '1452737483');
INSERT INTO `go_data_statistics` VALUES ('1171', '5', '酷赚锁屏', 'zlkzsp', '', '7900', '1452737494');
INSERT INTO `go_data_statistics` VALUES ('1172', '5', '酷赚锁屏', 'zlkzsp', '', '7901', '1452737497');
INSERT INTO `go_data_statistics` VALUES ('1173', '5', '酷赚锁屏', 'zlkzsp', '', '7902', '1452737512');
INSERT INTO `go_data_statistics` VALUES ('1174', '5', '酷赚锁屏', 'zlkzsp', '', '7903', '1452737535');
INSERT INTO `go_data_statistics` VALUES ('1175', '5', '酷赚锁屏', 'zlkzsp', '', '7904', '1452737546');
INSERT INTO `go_data_statistics` VALUES ('1176', '5', '酷赚锁屏', 'zlkzsp', '', '7905', '1452737547');
INSERT INTO `go_data_statistics` VALUES ('1177', '5', '酷赚锁屏', 'zlkzsp', '', '7906', '1452737547');
INSERT INTO `go_data_statistics` VALUES ('1178', '8', '一元众乐', 'yyzl', '', '7907', '1452737575');
INSERT INTO `go_data_statistics` VALUES ('1179', '5', '酷赚锁屏', 'zlkzsp', '', '7908', '1452737577');
INSERT INTO `go_data_statistics` VALUES ('1180', '5', '酷赚锁屏', 'zlkzsp', '', '7909', '1452737577');
INSERT INTO `go_data_statistics` VALUES ('1181', '5', '酷赚锁屏', 'zlkzsp', '', '7910', '1452737599');
INSERT INTO `go_data_statistics` VALUES ('1182', '5', '酷赚锁屏', 'zlkzsp', '', '7911', '1452737737');
INSERT INTO `go_data_statistics` VALUES ('1183', '8', '一元众乐', 'yyzl', '', '7912', '1452737993');
INSERT INTO `go_data_statistics` VALUES ('1184', '5', '酷赚锁屏', 'zlkzsp', '', '7913', '1452738365');
INSERT INTO `go_data_statistics` VALUES ('1185', '4', '百度推广', 'zlbdtg', 'SEM00000573', '7914', '1452738764');
INSERT INTO `go_data_statistics` VALUES ('1186', '6', '信号增强器', 'zlxhzqq', '', '7915', '1452738772');
INSERT INTO `go_data_statistics` VALUES ('1187', '8', '一元众乐', 'yyzl', '', '7916', '1452738987');
INSERT INTO `go_data_statistics` VALUES ('1188', '4', '百度推广', 'zlbdtg', 'SEM00000127', '7917', '1452739286');
INSERT INTO `go_data_statistics` VALUES ('1189', '8', '一元众乐', 'yyzl', '', '7918', '1452739480');
INSERT INTO `go_data_statistics` VALUES ('1190', '8', '一元众乐', 'yyzl', '', '7919', '1452739486');
INSERT INTO `go_data_statistics` VALUES ('1191', '19', '木蚂蚁', 'mumayi', '', '7921', '1452740026');
INSERT INTO `go_data_statistics` VALUES ('1192', '4', '百度推广', 'zlbdtg', 'SEM00000575', '7922', '1452740655');
INSERT INTO `go_data_statistics` VALUES ('1193', '4', '百度推广', 'zlbdtg', 'SEM00000121', '7923', '1452740661');
INSERT INTO `go_data_statistics` VALUES ('1194', '8', '一元众乐', 'yyzl', '', '7924', '1452740911');
INSERT INTO `go_data_statistics` VALUES ('1195', '4', '百度推广', 'zlbdtg', 'SEM00000573', '7925', '1452741072');
INSERT INTO `go_data_statistics` VALUES ('1196', '8', '一元众乐', 'yyzl', '', '7926', '1452741189');
INSERT INTO `go_data_statistics` VALUES ('1197', '4', '百度推广', 'zlbdtg', 'SEM00000119', '7927', '1452741447');
INSERT INTO `go_data_statistics` VALUES ('1198', '8', '一元众乐', 'yyzl', '', '7928', '1452741511');
INSERT INTO `go_data_statistics` VALUES ('1199', '4', '百度推广', 'zlbdtg', 'SEM00000163', '7929', '1452741557');
INSERT INTO `go_data_statistics` VALUES ('1200', '4', '百度推广', 'zlbdtg', 'SEM00000163', '3848', '1452741557');
INSERT INTO `go_data_statistics` VALUES ('1201', '4', '百度推广', 'zlbdtg', 'SEM00000115', '7930', '1452742345');
INSERT INTO `go_data_statistics` VALUES ('1202', '4', '百度推广', 'zlbdtg', 'SEM00000122', '7931', '1452742452');
INSERT INTO `go_data_statistics` VALUES ('1203', '4', '百度推广', 'zlbdtg', 'SEM00000626', '7932', '1452742938');
INSERT INTO `go_data_statistics` VALUES ('1204', '19', '木蚂蚁', 'mumayi', '', '7933', '1452743076');
INSERT INTO `go_data_statistics` VALUES ('1205', '4', '百度推广', 'zlbdtg', 'SEM00000115', '7935', '1452743756');
INSERT INTO `go_data_statistics` VALUES ('1206', '24', '魅族', 'meizu', '', '7937', '1452743967');
INSERT INTO `go_data_statistics` VALUES ('1207', '4', '百度推广', 'zlbdtg', 'SEM00000626', '7938', '1452744027');
INSERT INTO `go_data_statistics` VALUES ('1208', '8', '一元众乐', 'yyzl', '', '7939', '1452744401');
INSERT INTO `go_data_statistics` VALUES ('1209', '8', '一元众乐', 'yyzl', '', '7942', '1452744741');
INSERT INTO `go_data_statistics` VALUES ('1210', '8', '一元众乐', 'yyzl', '', '7943', '1452744797');
INSERT INTO `go_data_statistics` VALUES ('1211', '4', '百度推广', 'zlbdtg', 'SEM00000626', '7944', '1452745657');
INSERT INTO `go_data_statistics` VALUES ('1212', '8', '一元众乐', 'yyzl', '', '7945', '1452746193');
INSERT INTO `go_data_statistics` VALUES ('1213', '4', '百度推广', 'zlbdtg', 'SEM00000121', '7946', '1452746313');
INSERT INTO `go_data_statistics` VALUES ('1214', '8', '一元众乐', 'yyzl', '', '7947', '1452746411');
INSERT INTO `go_data_statistics` VALUES ('1215', '9', '午夜艳视', 'wuyeyanshi', '', '7948', '1452746435');
INSERT INTO `go_data_statistics` VALUES ('1216', '4', '百度推广', 'zlbdtg', 'SEM00000587', '7950', '1452746553');
INSERT INTO `go_data_statistics` VALUES ('1217', '8', '一元众乐', 'yyzl', '', '7951', '1452747256');
INSERT INTO `go_data_statistics` VALUES ('1218', '4', '百度推广', 'zlbdtg', 'SEM00000626', '7952', '1452747429');
INSERT INTO `go_data_statistics` VALUES ('1219', '19', '木蚂蚁', 'mumayi', '', '7953', '1452747799');
INSERT INTO `go_data_statistics` VALUES ('1220', '4', '百度推广', 'zlbdtg', 'YDSEM00000117', '7954', '1452748091');
INSERT INTO `go_data_statistics` VALUES ('1221', '8', '一元众乐', 'yyzl', '', '7956', '1452749192');
INSERT INTO `go_data_statistics` VALUES ('1222', '19', '木蚂蚁', 'mumayi', '', '7957', '1452749455');
INSERT INTO `go_data_statistics` VALUES ('1223', '17', 'PP助手', 'pp', '', '7958', '1452749704');
INSERT INTO `go_data_statistics` VALUES ('1224', '6', '信号增强器', 'zlxhzqq', '', '7959', '1452749798');
INSERT INTO `go_data_statistics` VALUES ('1225', '8', '一元众乐', 'yyzl', '', '7960', '1452749821');
INSERT INTO `go_data_statistics` VALUES ('1226', '4', '百度推广', 'zlbdtg', 'SEM00000115', '7961', '1452749993');
INSERT INTO `go_data_statistics` VALUES ('1227', '6', '信号增强器', 'zlxhzqq', '', '7962', '1452751094');
INSERT INTO `go_data_statistics` VALUES ('1228', '8', '一元众乐', 'yyzl', '', '7963', '1452751225');
INSERT INTO `go_data_statistics` VALUES ('1229', '4', '百度推广', 'zlbdtg', 'SEM00000115', '7964', '1452751426');
INSERT INTO `go_data_statistics` VALUES ('1230', '8', '一元众乐', 'yyzl', '', '7966', '1452751981');
INSERT INTO `go_data_statistics` VALUES ('1231', '8', '一元众乐', 'yyzl', '', '7967', '1452752108');
INSERT INTO `go_data_statistics` VALUES ('1232', '4', '百度推广', 'zlbdtg', 'SEM00000114', '7968', '1452752140');
INSERT INTO `go_data_statistics` VALUES ('1233', '4', '百度推广', 'zlbdtg', 'SEM00000117', '7973', '1452752501');
INSERT INTO `go_data_statistics` VALUES ('1234', '8', '一元众乐', 'yyzl', '', '7974', '1452752918');
INSERT INTO `go_data_statistics` VALUES ('1235', '19', '木蚂蚁', 'mumayi', '', '7975', '1452753106');
INSERT INTO `go_data_statistics` VALUES ('1236', '6', '信号增强器', 'zlxhzqq', '', '7976', '1452753227');
INSERT INTO `go_data_statistics` VALUES ('1237', '4', '百度推广', 'zlbdtg', 'SEM00000119', '7977', '1452753228');
INSERT INTO `go_data_statistics` VALUES ('1238', '4', '百度推广', 'zlbdtg', 'YDSEM00000117', '7978', '1452753311');
INSERT INTO `go_data_statistics` VALUES ('1239', '4', '百度推广', 'zlbdtg', 'YDSEM00000118', '7979', '1452753577');
INSERT INTO `go_data_statistics` VALUES ('1240', '6', '信号增强器', 'zlxhzqq', '', '7981', '1452753711');
INSERT INTO `go_data_statistics` VALUES ('1241', '19', '木蚂蚁', 'mumayi', '', '7982', '1452753963');
INSERT INTO `go_data_statistics` VALUES ('1242', '30', '开心看', 'zlkxk', '', '7983', '1452755514');
INSERT INTO `go_data_statistics` VALUES ('1243', '6', '信号增强器', 'zlxhzqq', '', '7984', '1452755670');
INSERT INTO `go_data_statistics` VALUES ('1244', '4', '百度推广', 'zlbdtg', 'SEM00000418', '7985', '1452755772');
INSERT INTO `go_data_statistics` VALUES ('1245', '30', '开心看', 'zlkxk', '', '7986', '1452755793');
INSERT INTO `go_data_statistics` VALUES ('1246', '8', '一元众乐', 'yyzl', '', '7988', '1452756339');
INSERT INTO `go_data_statistics` VALUES ('1247', '30', '开心看', 'zlkxk', '', '7989', '1452756532');
INSERT INTO `go_data_statistics` VALUES ('1248', '8', '一元众乐', 'yyzl', '', '7990', '1452756775');
INSERT INTO `go_data_statistics` VALUES ('1249', '4', '百度推广', 'zlbdtg', 'SEM00000122', '7991', '1452757130');
INSERT INTO `go_data_statistics` VALUES ('1250', '8', '一元众乐', 'yyzl', '', '7992', '1452757236');
INSERT INTO `go_data_statistics` VALUES ('1251', '30', '开心看', 'zlkxk', '', '7993', '1452757446');
INSERT INTO `go_data_statistics` VALUES ('1252', '30', '开心看', 'zlkxk', '', '7994', '1452757569');
INSERT INTO `go_data_statistics` VALUES ('1253', '8', '一元众乐', 'yyzl', '', '7995', '1452757980');
INSERT INTO `go_data_statistics` VALUES ('1254', '30', '开心看', 'zlkxk', '', '7996', '1452758597');
INSERT INTO `go_data_statistics` VALUES ('1255', '6', '信号增强器', 'zlxhzqq', '', '7997', '1452758620');
INSERT INTO `go_data_statistics` VALUES ('1256', '4', '百度推广', 'zlbdtg', 'YDSEM00000163', '7998', '1452758685');
INSERT INTO `go_data_statistics` VALUES ('1257', '4', '百度推广', 'zlbdtg', 'YDSEM00000628', '7999', '1452758733');
INSERT INTO `go_data_statistics` VALUES ('1258', '4', '百度推广', 'zlbdtg', 'SEM00000628', '8000', '1452758965');
INSERT INTO `go_data_statistics` VALUES ('1259', '30', '开心看', 'zlkxk', '', '8001', '1452759356');
INSERT INTO `go_data_statistics` VALUES ('1260', '8', '一元众乐', 'yyzl', '', '8002', '1452759369');
INSERT INTO `go_data_statistics` VALUES ('1261', '8', '一元众乐', 'yyzl', '', '8003', '1452759688');
INSERT INTO `go_data_statistics` VALUES ('1262', '4', '百度推广', 'zlbdtg', 'SEM00000626', '8004', '1452759760');
INSERT INTO `go_data_statistics` VALUES ('1263', '4', '百度推广', 'zlbdtg', 'SEM00000163', '8005', '1452760004');
INSERT INTO `go_data_statistics` VALUES ('1264', '4', '百度推广', 'zlbdtg', 'SEM00000115', '8006', '1452760184');
INSERT INTO `go_data_statistics` VALUES ('1265', '4', '百度推广', 'zlbdtg', 'SEM00000121', '8007', '1452760244');
INSERT INTO `go_data_statistics` VALUES ('1266', '4', '百度推广', 'zlbdtg', 'SEM00000601', '8008', '1452760284');
INSERT INTO `go_data_statistics` VALUES ('1267', '4', '百度推广', 'zlbdtg', 'SEM00000620', '8009', '1452760418');
INSERT INTO `go_data_statistics` VALUES ('1268', '8', '一元众乐', 'yyzl', '', '8011', '1452760784');
INSERT INTO `go_data_statistics` VALUES ('1269', '8', '一元众乐', 'yyzl', '', '8012', '1452760885');
INSERT INTO `go_data_statistics` VALUES ('1270', '8', '一元众乐', 'yyzl', '', '8013', '1452761159');
INSERT INTO `go_data_statistics` VALUES ('1271', '4', '百度推广', 'zlbdtg', 'SEM00000573', '8015', '1452761320');
INSERT INTO `go_data_statistics` VALUES ('1272', '4', '百度推广', 'zlbdtg', 'SEM00000114', '8016', '1452761369');
INSERT INTO `go_data_statistics` VALUES ('1273', '4', '百度推广', 'zlbdtg', 'SEM00000115', '8017', '1452761462');
INSERT INTO `go_data_statistics` VALUES ('1274', '4', '百度推广', 'zlbdtg', 'SEM00000115', '8018', '1452761503');
INSERT INTO `go_data_statistics` VALUES ('1275', '30', '开心看', 'zlkxk', '', '8019', '1452761621');
INSERT INTO `go_data_statistics` VALUES ('1276', '4', '百度推广', 'zlbdtg', 'SEM00000115', '8020', '1452761765');
INSERT INTO `go_data_statistics` VALUES ('1277', '6', '信号增强器', 'zlxhzqq', '', '8022', '1452761864');
INSERT INTO `go_data_statistics` VALUES ('1278', '30', '开心看', 'zlkxk', '', '8023', '1452761887');
INSERT INTO `go_data_statistics` VALUES ('1279', '30', '开心看', 'zlkxk', '', '8024', '1452762543');
INSERT INTO `go_data_statistics` VALUES ('1280', '8', '一元众乐', 'yyzl', '', '8025', '1452762633');
INSERT INTO `go_data_statistics` VALUES ('1281', '30', '开心看', 'zlkxk', '', '8027', '1452762700');
INSERT INTO `go_data_statistics` VALUES ('1282', '8', '一元众乐', 'yyzl', '', '8028', '1452762764');
INSERT INTO `go_data_statistics` VALUES ('1283', '30', '开心看', 'zlkxk', '', '8029', '1452762944');
INSERT INTO `go_data_statistics` VALUES ('1284', '4', '百度推广', 'zlbdtg', 'SEM00000396', '8030', '1452762986');
INSERT INTO `go_data_statistics` VALUES ('1285', '4', '百度推广', 'zlbdtg', 'YDSEM00000229', '8031', '1452763116');
INSERT INTO `go_data_statistics` VALUES ('1286', '8', '一元众乐', 'yyzl', '', '8032', '1452763171');
INSERT INTO `go_data_statistics` VALUES ('1287', '9', '午夜艳视', 'wuyeyanshi', '', '8033', '1452763339');
INSERT INTO `go_data_statistics` VALUES ('1288', '8', '一元众乐', 'yyzl', '', '8034', '1452763510');
INSERT INTO `go_data_statistics` VALUES ('1289', '30', '开心看', 'zlkxk', '', '8036', '1452763747');
INSERT INTO `go_data_statistics` VALUES ('1290', '4', '百度推广', 'zlbdtg', 'SEM00000163', '8037', '1452763864');
INSERT INTO `go_data_statistics` VALUES ('1291', '4', '百度推广', 'zlbdtg', 'SEM00000163', '8038', '1452763924');
INSERT INTO `go_data_statistics` VALUES ('1292', '8', '一元众乐', 'yyzl', '', '8039', '1452763999');
INSERT INTO `go_data_statistics` VALUES ('1293', '4', '百度推广', 'zlbdtg', 'YDSEM00000626', '8040', '1452764071');
INSERT INTO `go_data_statistics` VALUES ('1294', '9', '午夜艳视', 'wuyeyanshi', '', '8041', '1452764092');
INSERT INTO `go_data_statistics` VALUES ('1295', '6', '信号增强器', 'zlxhzqq', '', '8042', '1452764156');
INSERT INTO `go_data_statistics` VALUES ('1296', '8', '一元众乐', 'yyzl', '', '8043', '1452764244');
INSERT INTO `go_data_statistics` VALUES ('1297', '8', '一元众乐', 'yyzl', '', '8044', '1452764309');
INSERT INTO `go_data_statistics` VALUES ('1298', '21', 'oppo', 'oppo', '', '8045', '1452764567');
INSERT INTO `go_data_statistics` VALUES ('1299', '8', '一元众乐', 'yyzl', '', '8046', '1452764605');
INSERT INTO `go_data_statistics` VALUES ('1300', '30', '开心看', 'zlkxk', '', '8048', '1452765117');
INSERT INTO `go_data_statistics` VALUES ('1301', '19', '木蚂蚁', 'mumayi', '', '8050', '1452765164');
INSERT INTO `go_data_statistics` VALUES ('1302', '30', '开心看', 'zlkxk', '', '8051', '1452766246');
INSERT INTO `go_data_statistics` VALUES ('1303', '30', '开心看', 'zlkxk', '', '8052', '1452766372');
INSERT INTO `go_data_statistics` VALUES ('1304', '4', '百度推广', 'zlbdtg', 'YDSEM00000573', '8053', '1452766647');
INSERT INTO `go_data_statistics` VALUES ('1305', '8', '一元众乐', 'yyzl', '', '8054', '1452766870');
INSERT INTO `go_data_statistics` VALUES ('1306', '8', '一元众乐', 'yyzl', '', '8055', '1452766996');
INSERT INTO `go_data_statistics` VALUES ('1307', '8', '一元众乐', 'yyzl', '', '8056', '1452767144');
INSERT INTO `go_data_statistics` VALUES ('1308', '8', '一元众乐', 'yyzl', '', '8057', '1452767238');
INSERT INTO `go_data_statistics` VALUES ('1309', '4', '百度推广', 'zlbdtg', 'SEM00000115', '8058', '1452767241');
INSERT INTO `go_data_statistics` VALUES ('1310', '4', '百度推广', 'zlbdtg', 'SEM00000121', '8059', '1452767414');
INSERT INTO `go_data_statistics` VALUES ('1311', '4', '百度推广', 'zlbdtg', 'YDSEM00000572', '8060', '1452767476');
INSERT INTO `go_data_statistics` VALUES ('1312', '30', '开心看', 'zlkxk', '', '8061', '1452767623');
INSERT INTO `go_data_statistics` VALUES ('1313', '30', '开心看', 'zlkxk', '', '8063', '1452767770');
INSERT INTO `go_data_statistics` VALUES ('1314', '8', '一元众乐', 'yyzl', '', '8066', '1452768154');
INSERT INTO `go_data_statistics` VALUES ('1315', '30', '开心看', 'zlkxk', '', '8067', '1452768165');
INSERT INTO `go_data_statistics` VALUES ('1316', '4', '百度推广', 'zlbdtg', 'YDSEM00000339', '8068', '1452768368');
INSERT INTO `go_data_statistics` VALUES ('1317', '30', '开心看', 'zlkxk', '', '8069', '1452768625');
INSERT INTO `go_data_statistics` VALUES ('1318', '4', '百度推广', 'zlbdtg', 'YDSEM00000626', '8070', '1452768705');
INSERT INTO `go_data_statistics` VALUES ('1319', '8', '一元众乐', 'yyzl', '', '8071', '1452768728');
INSERT INTO `go_data_statistics` VALUES ('1320', '30', '开心看', 'zlkxk', '', '8072', '1452768911');
INSERT INTO `go_data_statistics` VALUES ('1321', '30', '开心看', 'zlkxk', '', '8073', '1452769185');
INSERT INTO `go_data_statistics` VALUES ('1322', '4', '百度推广', 'zlbdtg', 'SEM00000626', '8074', '1452769207');
INSERT INTO `go_data_statistics` VALUES ('1323', '4', '百度推广', 'zlbdtg', 'YDSEM00000117', '8075', '1452769429');
INSERT INTO `go_data_statistics` VALUES ('1324', '4', '百度推广', 'zlbdtg', 'SEM00000610', '8076', '1452769453');
INSERT INTO `go_data_statistics` VALUES ('1325', '4', '百度推广', 'zlbdtg', 'SEM00000396', '8077', '1452769517');
INSERT INTO `go_data_statistics` VALUES ('1326', '4', '百度推广', 'zlbdtg', 'YDSEM00000626', '8078', '1452769701');
INSERT INTO `go_data_statistics` VALUES ('1327', '8', '一元众乐', 'yyzl', '', '8079', '1452770192');
INSERT INTO `go_data_statistics` VALUES ('1328', '30', '开心看', 'zlkxk', '', '8080', '1452770402');
INSERT INTO `go_data_statistics` VALUES ('1329', '4', '百度推广', 'zlbdtg', 'SEM00000628', '8081', '1452770521');
INSERT INTO `go_data_statistics` VALUES ('1330', '2', '儒豹浏览器', 'rbower', '', '8083', '1452770977');
INSERT INTO `go_data_statistics` VALUES ('1331', '9', '午夜艳视', 'wuyeyanshi', '', '8084', '1452771073');
INSERT INTO `go_data_statistics` VALUES ('1332', '4', '百度推广', 'zlbdtg', 'YDSEM00000120', '8085', '1452771101');
INSERT INTO `go_data_statistics` VALUES ('1333', '6', '信号增强器', 'zlxhzqq', '', '8086', '1452771292');
INSERT INTO `go_data_statistics` VALUES ('1334', '8', '一元众乐', 'yyzl', '', '8087', '1452771475');
INSERT INTO `go_data_statistics` VALUES ('1335', '6', '信号增强器', 'zlxhzqq', '', '8088', '1452771912');
INSERT INTO `go_data_statistics` VALUES ('1336', '6', '信号增强器', 'zlxhzqq', '', '8089', '1452771913');
INSERT INTO `go_data_statistics` VALUES ('1337', '30', '开心看', 'zlkxk', '', '8090', '1452772225');
INSERT INTO `go_data_statistics` VALUES ('1338', '4', '百度推广', 'zlbdtg', 'SEM00000115', '8091', '1452772350');
INSERT INTO `go_data_statistics` VALUES ('1339', '4', '百度推广', 'zlbdtg', 'SEM00000626', '8092', '1452772712');
INSERT INTO `go_data_statistics` VALUES ('1340', '6', '信号增强器', 'zlxhzqq', '', '8093', '1452772781');
INSERT INTO `go_data_statistics` VALUES ('1341', '4', '百度推广', 'zlbdtg', 'YDSEM00000588', '8094', '1452773189');
INSERT INTO `go_data_statistics` VALUES ('1342', '30', '开心看', 'zlkxk', '', '8095', '1452773242');
INSERT INTO `go_data_statistics` VALUES ('1343', '4', '百度推广', 'zlbdtg', 'YDSEM00000563', '8096', '1452773456');
INSERT INTO `go_data_statistics` VALUES ('1344', '19', '木蚂蚁', 'mumayi', '', '8097', '1452773458');
INSERT INTO `go_data_statistics` VALUES ('1345', '30', '开心看', 'zlkxk', '', '8098', '1452773597');
INSERT INTO `go_data_statistics` VALUES ('1346', '30', '开心看', 'zlkxk', '', '8099', '1452773935');
INSERT INTO `go_data_statistics` VALUES ('1347', '30', '开心看', 'zlkxk', '', '8100', '1452774069');
INSERT INTO `go_data_statistics` VALUES ('1348', '30', '开心看', 'zlkxk', '', '8101', '1452774075');
INSERT INTO `go_data_statistics` VALUES ('1349', '8', '一元众乐', 'yyzl', '', '8104', '1452774300');
INSERT INTO `go_data_statistics` VALUES ('1350', '8', '一元众乐', 'yyzl', '', '8107', '1452774579');
INSERT INTO `go_data_statistics` VALUES ('1351', '8', '一元众乐', 'yyzl', '', '8108', '1452774758');
INSERT INTO `go_data_statistics` VALUES ('1352', '4', '百度推广', 'zlbdtg', 'YDSEM00000117', '8109', '1452774927');
INSERT INTO `go_data_statistics` VALUES ('1353', '19', '木蚂蚁', 'mumayi', '', '8110', '1452774932');
INSERT INTO `go_data_statistics` VALUES ('1354', '4', '百度推广', 'zlbdtg', 'YDSEM00000573', '8111', '1452775000');
INSERT INTO `go_data_statistics` VALUES ('1355', '4', '百度推广', 'zlbdtg', 'SEM00000639', '8112', '1452775070');
INSERT INTO `go_data_statistics` VALUES ('1356', '8', '一元众乐', 'yyzl', '', '8114', '1452775754');
INSERT INTO `go_data_statistics` VALUES ('1357', '4', '百度推广', 'zlbdtg', 'SEM00000626', '8115', '1452775787');
INSERT INTO `go_data_statistics` VALUES ('1358', '4', '百度推广', 'zlbdtg', 'SEM00000611', '8116', '1452775845');
INSERT INTO `go_data_statistics` VALUES ('1359', '8', '一元众乐', 'yyzl', '', '8117', '1452775954');
INSERT INTO `go_data_statistics` VALUES ('1360', '4', '百度推广', 'zlbdtg', 'SEM00000114', '8118', '1452776208');
INSERT INTO `go_data_statistics` VALUES ('1361', '8', '一元众乐', 'yyzl', '', '8119', '1452776399');
INSERT INTO `go_data_statistics` VALUES ('1362', '4', '百度推广', 'zlbdtg', 'SEM00000115', '8120', '1452776551');
INSERT INTO `go_data_statistics` VALUES ('1363', '4', '百度推广', 'zlbdtg', 'SEM00000626', '8121', '1452776649');
INSERT INTO `go_data_statistics` VALUES ('1364', '8', '一元众乐', 'yyzl', '', '8122', '1452776680');
INSERT INTO `go_data_statistics` VALUES ('1365', '8', '一元众乐', 'yyzl', '', '8123', '1452776905');
INSERT INTO `go_data_statistics` VALUES ('1366', '8', '一元众乐', 'yyzl', '', '8124', '1452777072');
INSERT INTO `go_data_statistics` VALUES ('1367', '30', '开心看', 'zlkxk', '', '8125', '1452777117');
INSERT INTO `go_data_statistics` VALUES ('1368', '30', '开心看', 'zlkxk', '', '8126', '1452777211');
INSERT INTO `go_data_statistics` VALUES ('1369', '30', '开心看', 'zlkxk', '', '8127', '1452777250');
INSERT INTO `go_data_statistics` VALUES ('1370', '4', '百度推广', 'zlbdtg', 'YDSEM00000626', '8128', '1452777531');
INSERT INTO `go_data_statistics` VALUES ('1371', '5', '酷赚锁屏', 'zlkzsp', '', '8129', '1452777574');
INSERT INTO `go_data_statistics` VALUES ('1372', '30', '开心看', 'zlkxk', '', '8130', '1452777665');
INSERT INTO `go_data_statistics` VALUES ('1373', '4', '百度推广', 'zlbdtg', 'SEM00000122', '8132', '1452777723');
INSERT INTO `go_data_statistics` VALUES ('1374', '4', '百度推广', 'zlbdtg', 'SEM00000121', '8135', '1452777934');
INSERT INTO `go_data_statistics` VALUES ('1375', '4', '百度推广', 'zlbdtg', 'YDSEM00000626', '8136', '1452777985');
INSERT INTO `go_data_statistics` VALUES ('1376', '30', '开心看', 'zlkxk', '', '8137', '1452778127');
INSERT INTO `go_data_statistics` VALUES ('1377', '8', '一元众乐', 'yyzl', '', '8138', '1452778231');
INSERT INTO `go_data_statistics` VALUES ('1378', '4', '百度推广', 'zlbdtg', 'YDSEM00000626', '8139', '1452778569');
INSERT INTO `go_data_statistics` VALUES ('1379', '30', '开心看', 'zlkxk', '', '8140', '1452778592');
INSERT INTO `go_data_statistics` VALUES ('1380', '4', '百度推广', 'zlbdtg', 'YDSEM00000582', '8141', '1452778597');
INSERT INTO `go_data_statistics` VALUES ('1381', '8', '一元众乐', 'yyzl', '', '8142', '1452778879');
INSERT INTO `go_data_statistics` VALUES ('1382', '19', '木蚂蚁', 'mumayi', '', '8143', '1452778936');
INSERT INTO `go_data_statistics` VALUES ('1383', '19', '木蚂蚁', 'mumayi', '', '8144', '1452778936');
INSERT INTO `go_data_statistics` VALUES ('1384', '19', '木蚂蚁', 'mumayi', '', '8145', '1452778946');
INSERT INTO `go_data_statistics` VALUES ('1385', '4', '百度推广', 'zlbdtg', 'YDSEM00000606', '8148', '1452778993');
INSERT INTO `go_data_statistics` VALUES ('1386', '4', '百度推广', 'zlbdtg', 'YDSEM00000626', '8149', '1452779064');
INSERT INTO `go_data_statistics` VALUES ('1387', '30', '开心看', 'zlkxk', '', '8153', '1452779559');
INSERT INTO `go_data_statistics` VALUES ('1388', '6', '信号增强器', 'zlxhzqq', '', '8154', '1452779576');
INSERT INTO `go_data_statistics` VALUES ('1389', '8', '一元众乐', 'yyzl', '', '8155', '1452779576');
INSERT INTO `go_data_statistics` VALUES ('1390', '8', '一元众乐', 'yyzl', '', '8156', '1452779576');
INSERT INTO `go_data_statistics` VALUES ('1391', '4', '百度推广', 'zlbdtg', 'YDSEM00000598', '8157', '1452779753');
INSERT INTO `go_data_statistics` VALUES ('1392', '4', '百度推广', 'zlbdtg', 'YDSEM00000628', '8159', '1452779921');
INSERT INTO `go_data_statistics` VALUES ('1393', '4', '百度推广', 'zlbdtg', 'YDSEM00000626', '8160', '1452779941');
INSERT INTO `go_data_statistics` VALUES ('1394', '9', '午夜艳视', 'wuyeyanshi', '', '8161', '1452779982');
INSERT INTO `go_data_statistics` VALUES ('1395', '6', '信号增强器', 'zlxhzqq', '', '8162', '1452780193');
INSERT INTO `go_data_statistics` VALUES ('1396', '8', '一元众乐', 'yyzl', '', '8163', '1452780271');
INSERT INTO `go_data_statistics` VALUES ('1397', '30', '开心看', 'zlkxk', '', '8164', '1452780314');
INSERT INTO `go_data_statistics` VALUES ('1398', '4', '百度推广', 'zlbdtg', 'SEM00000644', '8165', '1452780353');
INSERT INTO `go_data_statistics` VALUES ('1399', '8', '一元众乐', 'yyzl', '', '8168', '1452780641');
INSERT INTO `go_data_statistics` VALUES ('1400', '30', '开心看', 'zlkxk', '', '8169', '1452780648');
INSERT INTO `go_data_statistics` VALUES ('1401', '8', '一元众乐', 'yyzl', '', '8170', '1452780684');
INSERT INTO `go_data_statistics` VALUES ('1402', '6', '信号增强器', 'zlxhzqq', '', '8171', '1452780697');
INSERT INTO `go_data_statistics` VALUES ('1403', '8', '一元众乐', 'yyzl', '', '8172', '1452780738');
INSERT INTO `go_data_statistics` VALUES ('1404', '4', '百度推广', 'zlbdtg', 'YDSEM00000626', '8173', '1452780757');
INSERT INTO `go_data_statistics` VALUES ('1405', '30', '开心看', 'zlkxk', '', '8175', '1452780821');
INSERT INTO `go_data_statistics` VALUES ('1406', '8', '一元众乐', 'yyzl', '', '8176', '1452780838');
INSERT INTO `go_data_statistics` VALUES ('1407', '8', '一元众乐', 'yyzl', '', '8177', '1452780864');
INSERT INTO `go_data_statistics` VALUES ('1408', '30', '开心看', 'zlkxk', '', '8178', '1452780999');
INSERT INTO `go_data_statistics` VALUES ('1409', '4', '百度推广', 'zlbdtg', 'YDSEM00000626', '8180', '1452781129');
INSERT INTO `go_data_statistics` VALUES ('1410', '8', '一元众乐', 'yyzl', '', '8181', '1452781147');
INSERT INTO `go_data_statistics` VALUES ('1411', '30', '开心看', 'zlkxk', '', '8182', '1452781150');
INSERT INTO `go_data_statistics` VALUES ('1412', '4', '百度推广', 'zlbdtg', 'SEM00000536', '8183', '1452781232');
INSERT INTO `go_data_statistics` VALUES ('1413', '30', '开心看', 'zlkxk', '', '8185', '1452781337');
INSERT INTO `go_data_statistics` VALUES ('1414', '4', '百度推广', 'zlbdtg', 'SEM00000122', '8187', '1452781517');
INSERT INTO `go_data_statistics` VALUES ('1415', '4', '百度推广', 'zlbdtg', 'SEM00000117', '8188', '1452781686');
INSERT INTO `go_data_statistics` VALUES ('1416', '4', '百度推广', 'zlbdtg', 'SEM00000121', '8190', '1452781822');
INSERT INTO `go_data_statistics` VALUES ('1417', '6', '信号增强器', 'zlxhzqq', '', '8191', '1452782004');
INSERT INTO `go_data_statistics` VALUES ('1418', '4', '百度推广', 'zlbdtg', 'SEM00000121', '8192', '1452782017');
INSERT INTO `go_data_statistics` VALUES ('1419', '8', '一元众乐', 'yyzl', '', '8193', '1452782030');
INSERT INTO `go_data_statistics` VALUES ('1420', '8', '一元众乐', 'yyzl', '', '8194', '1452782038');
INSERT INTO `go_data_statistics` VALUES ('1421', '19', '木蚂蚁', 'mumayi', '', '8195', '1452782140');
INSERT INTO `go_data_statistics` VALUES ('1422', '6', '信号增强器', 'zlxhzqq', '', '8196', '1452782261');
INSERT INTO `go_data_statistics` VALUES ('1423', '8', '一元众乐', 'yyzl', '', '8198', '1452782418');
INSERT INTO `go_data_statistics` VALUES ('1424', '30', '开心看', 'zlkxk', '', '8199', '1452782514');
INSERT INTO `go_data_statistics` VALUES ('1425', '4', '百度推广', 'zlbdtg', 'SEM00000121', '8200', '1452782635');
INSERT INTO `go_data_statistics` VALUES ('1426', '30', '开心看', 'zlkxk', '', '8201', '1452782671');
INSERT INTO `go_data_statistics` VALUES ('1427', '30', '开心看', 'zlkxk', '', '8202', '1452782811');
INSERT INTO `go_data_statistics` VALUES ('1428', '4', '百度推广', 'zlbdtg', 'SEM00000583', '8203', '1452782863');
INSERT INTO `go_data_statistics` VALUES ('1429', '8', '一元众乐', 'yyzl', '', '8204', '1452782877');
INSERT INTO `go_data_statistics` VALUES ('1430', '4', '百度推广', 'zlbdtg', 'SEM00000573', '8205', '1452783071');
INSERT INTO `go_data_statistics` VALUES ('1431', '4', '百度推广', 'zlbdtg', 'YDSEM00000117', '8206', '1452783102');
INSERT INTO `go_data_statistics` VALUES ('1432', '8', '一元众乐', 'yyzl', '', '8208', '1452783567');
INSERT INTO `go_data_statistics` VALUES ('1433', '30', '开心看', 'zlkxk', '', '8209', '1452783732');
INSERT INTO `go_data_statistics` VALUES ('1434', '4', '百度推广', 'zlbdtg', 'YDSEM00000588', '8210', '1452783937');
INSERT INTO `go_data_statistics` VALUES ('1435', '4', '百度推广', 'zlbdtg', 'YDSEM00000626', '8211', '1452784103');
INSERT INTO `go_data_statistics` VALUES ('1436', '9', '午夜艳视', 'wuyeyanshi', '', '8212', '1452784172');
INSERT INTO `go_data_statistics` VALUES ('1437', '4', '百度推广', 'zlbdtg', 'YDSEM00000572', '8213', '1452784174');
INSERT INTO `go_data_statistics` VALUES ('1438', '4', '百度推广', 'zlbdtg', 'YDSEM00000585', '8214', '1452784420');
INSERT INTO `go_data_statistics` VALUES ('1439', '8', '一元众乐', 'yyzl', '', '8215', '1452784436');
INSERT INTO `go_data_statistics` VALUES ('1440', '8', '一元众乐', 'yyzl', '', '8216', '1452784625');
INSERT INTO `go_data_statistics` VALUES ('1441', '4', '百度推广', 'zlbdtg', 'SEM00000572', '8217', '1452784705');
INSERT INTO `go_data_statistics` VALUES ('1442', '8', '一元众乐', 'yyzl', '', '8218', '1452784745');
INSERT INTO `go_data_statistics` VALUES ('1443', '4', '百度推广', 'zlbdtg', 'YDSEM00000626', '8219', '1452785106');
INSERT INTO `go_data_statistics` VALUES ('1444', '4', '百度推广', 'zlbdtg', 'SEM00000115', '8220', '1452785235');
INSERT INTO `go_data_statistics` VALUES ('1445', '6', '信号增强器', 'zlxhzqq', '', '8221', '1452785273');
INSERT INTO `go_data_statistics` VALUES ('1446', '30', '开心看', 'zlkxk', '', '8222', '1452785325');
INSERT INTO `go_data_statistics` VALUES ('1447', '30', '开心看', 'zlkxk', '', '8224', '1452785526');
INSERT INTO `go_data_statistics` VALUES ('1448', '4', '百度推广', 'zlbdtg', 'YDSEM00000626', '8225', '1452785725');
INSERT INTO `go_data_statistics` VALUES ('1449', '4', '百度推广', 'zlbdtg', 'SEM00000606', '8226', '1452785863');
INSERT INTO `go_data_statistics` VALUES ('1450', '4', '百度推广', 'zlbdtg', 'YDSEM00000628', '8227', '1452785873');
INSERT INTO `go_data_statistics` VALUES ('1451', '19', '木蚂蚁', 'mumayi', '', '8228', '1452785890');
INSERT INTO `go_data_statistics` VALUES ('1452', '15', '百度', 'baidu', '', '8229', '1452785963');
INSERT INTO `go_data_statistics` VALUES ('1453', '4', '百度推广', 'zlbdtg', 'SEM00000115', '8231', '1452786105');
INSERT INTO `go_data_statistics` VALUES ('1454', '4', '百度推广', 'zlbdtg', 'YDSEM00000626', '8232', '1452786251');
INSERT INTO `go_data_statistics` VALUES ('1455', '6', '信号增强器', 'zlxhzqq', '', '8233', '1452786414');
INSERT INTO `go_data_statistics` VALUES ('1456', '30', '开心看', 'zlkxk', '', '8234', '1452786452');
INSERT INTO `go_data_statistics` VALUES ('1457', '4', '百度推广', 'zlbdtg', 'SEM00000121', '8235', '1452786456');
INSERT INTO `go_data_statistics` VALUES ('1458', '6', '信号增强器', 'zlxhzqq', '', '8236', '1452786553');
INSERT INTO `go_data_statistics` VALUES ('1459', '4', '百度推广', 'zlbdtg', 'YDSEM00000628', '8237', '1452786590');
INSERT INTO `go_data_statistics` VALUES ('1460', '30', '开心看', 'zlkxk', '', '8238', '1452786604');
INSERT INTO `go_data_statistics` VALUES ('1461', '8', '一元众乐', 'yyzl', '', '8239', '1452786832');
INSERT INTO `go_data_statistics` VALUES ('1462', '8', '一元众乐', 'yyzl', '', '8240', '1452787062');
INSERT INTO `go_data_statistics` VALUES ('1463', '4', '百度推广', 'zlbdtg', 'SEM00000574', '8241', '1452787453');
INSERT INTO `go_data_statistics` VALUES ('1464', '19', '木蚂蚁', 'mumayi', '', '8242', '1452787818');
INSERT INTO `go_data_statistics` VALUES ('1465', '4', '百度推广', 'zlbdtg', 'YDSEM00000626', '8243', '1452787974');
INSERT INTO `go_data_statistics` VALUES ('1466', '4', '百度推广', 'zlbdtg', 'SEM00000600', '8244', '1452787988');
INSERT INTO `go_data_statistics` VALUES ('1467', '8', '一元众乐', 'yyzl', '', '8245', '1452788051');
INSERT INTO `go_data_statistics` VALUES ('1468', '6', '信号增强器', 'zlxhzqq', '', '8246', '1452788295');
INSERT INTO `go_data_statistics` VALUES ('1469', '30', '开心看', 'zlkxk', '', '8247', '1452788397');
INSERT INTO `go_data_statistics` VALUES ('1470', '8', '一元众乐', 'yyzl', '', '8249', '1452789709');
INSERT INTO `go_data_statistics` VALUES ('1471', '30', '开心看', 'zlkxk', '', '8251', '1452790357');
INSERT INTO `go_data_statistics` VALUES ('1472', '6', '信号增强器', 'zlxhzqq', '', '8252', '1452790695');
INSERT INTO `go_data_statistics` VALUES ('1473', '6', '信号增强器', 'zlxhzqq', '', '8253', '1452791637');
INSERT INTO `go_data_statistics` VALUES ('1474', '4', '百度推广', 'zlbdtg', 'SEM00000122', '8254', '1452793801');
INSERT INTO `go_data_statistics` VALUES ('1475', '8', '一元众乐', 'yyzl', '', '8255', '1452794452');
INSERT INTO `go_data_statistics` VALUES ('1476', '4', '百度推广', 'zlbdtg', 'YDSEM00000626', '8256', '1452794851');
INSERT INTO `go_data_statistics` VALUES ('1477', '4', '百度推广', 'zlbdtg', 'SEM00000121', '8257', '1452796374');
INSERT INTO `go_data_statistics` VALUES ('1478', '4', '百度推广', 'zlbdtg', 'SEM00000121', '8258', '1452796413');
INSERT INTO `go_data_statistics` VALUES ('1479', '4', '百度推广', 'zlbdtg', 'SEM00000396', '8262', '1452798630');
INSERT INTO `go_data_statistics` VALUES ('1480', '8', '一元众乐', 'yyzl', '', '8267', '1452800637');
INSERT INTO `go_data_statistics` VALUES ('1481', '4', '百度推广', 'zlbdtg', 'SEM00000121', '8268', '1452802019');
INSERT INTO `go_data_statistics` VALUES ('1482', '4', '百度推广', 'zlbdtg', 'SEM00000626', '8269', '1452802548');
INSERT INTO `go_data_statistics` VALUES ('1483', '8', '一元众乐', 'yyzl', '', '8270', '1452802678');
INSERT INTO `go_data_statistics` VALUES ('1484', '30', '开心看', 'zlkxk', '', '8271', '1452804079');
INSERT INTO `go_data_statistics` VALUES ('1485', '4', '百度推广', 'zlbdtg', 'YDSEM00000626', '8272', '1452804726');
INSERT INTO `go_data_statistics` VALUES ('1486', '4', '百度推广', 'zlbdtg', 'YDSEM00000573', '8273', '1452805123');
INSERT INTO `go_data_statistics` VALUES ('1487', '6', '信号增强器', 'zlxhzqq', '', '8274', '1452807508');
INSERT INTO `go_data_statistics` VALUES ('1488', '4', '百度推广', 'zlbdtg', 'SEM00000115', '8275', '1452808237');
INSERT INTO `go_data_statistics` VALUES ('1489', '6', '信号增强器', 'zlxhzqq', '', '8276', '1452811787');
INSERT INTO `go_data_statistics` VALUES ('1490', '4', '百度推广', 'zlbdtg', 'SEM00000115', '8277', '1452813679');
INSERT INTO `go_data_statistics` VALUES ('1491', '8', '一元众乐', 'yyzl', '', '8278', '1452813870');
INSERT INTO `go_data_statistics` VALUES ('1492', '30', '开心看', 'zlkxk', '', '8280', '1452815720');
INSERT INTO `go_data_statistics` VALUES ('1493', '4', '百度推广', 'zlbdtg', 'SEM00000115', '8281', '1452816249');
INSERT INTO `go_data_statistics` VALUES ('1494', '30', '开心看', 'zlkxk', '', '8282', '1452817069');
INSERT INTO `go_data_statistics` VALUES ('1495', '4', '百度推广', 'zlbdtg', 'SEM00000141', '8284', '1452817257');
INSERT INTO `go_data_statistics` VALUES ('1496', '30', '开心看', 'zlkxk', '', '8285', '1452817331');
INSERT INTO `go_data_statistics` VALUES ('1497', '30', '开心看', 'zlkxk', '', '8286', '1452817420');
INSERT INTO `go_data_statistics` VALUES ('1498', '30', '开心看', 'zlkxk', '', '8288', '1452817618');
INSERT INTO `go_data_statistics` VALUES ('1499', '30', '开心看', 'zlkxk', '', '8289', '1452817767');
INSERT INTO `go_data_statistics` VALUES ('1500', '4', '百度推广', 'zlbdtg', 'SEM00000610', '8290', '1452818602');
INSERT INTO `go_data_statistics` VALUES ('1501', '30', '开心看', 'zlkxk', '', '8291', '1452818614');
INSERT INTO `go_data_statistics` VALUES ('1502', '8', '一元众乐', 'yyzl', '', '8293', '1452818671');
INSERT INTO `go_data_statistics` VALUES ('1503', '8', '一元众乐', 'yyzl', '', '8294', '1452819320');
INSERT INTO `go_data_statistics` VALUES ('1504', '8', '一元众乐', 'yyzl', '', '8295', '1452820160');
INSERT INTO `go_data_statistics` VALUES ('1505', '8', '一元众乐', 'yyzl', '', '8296', '1452820421');
INSERT INTO `go_data_statistics` VALUES ('1506', '4', '百度推广', 'zlbdtg', 'SEM00000626', '8297', '1452820423');
INSERT INTO `go_data_statistics` VALUES ('1507', '30', '开心看', 'zlkxk', '', '8298', '1452820673');
INSERT INTO `go_data_statistics` VALUES ('1508', '4', '百度推广', 'zlbdtg', 'SEM00000573', '8299', '1452820679');
INSERT INTO `go_data_statistics` VALUES ('1509', '4', '百度推广', 'zlbdtg', 'YDSEM00000404', '8300', '1452820743');
INSERT INTO `go_data_statistics` VALUES ('1510', '6', '信号增强器', 'zlxhzqq', '', '8301', '1452820823');
INSERT INTO `go_data_statistics` VALUES ('1511', '30', '开心看', 'zlkxk', '', '8302', '1452821292');
INSERT INTO `go_data_statistics` VALUES ('1512', '4', '百度推广', 'zlbdtg', 'YDSEM00000339', '8303', '1452821356');
INSERT INTO `go_data_statistics` VALUES ('1513', '8', '一元众乐', 'yyzl', '', '8304', '1452822076');
INSERT INTO `go_data_statistics` VALUES ('1514', '4', '百度推广', 'zlbdtg', 'SEM00000639', '8305', '1452822442');
INSERT INTO `go_data_statistics` VALUES ('1515', '6', '信号增强器', 'zlxhzqq', '', '8306', '1452822540');
INSERT INTO `go_data_statistics` VALUES ('1516', '8', '一元众乐', 'yyzl', '', '8307', '1452822590');
INSERT INTO `go_data_statistics` VALUES ('1517', '30', '开心看', 'zlkxk', '', '8308', '1452822658');
INSERT INTO `go_data_statistics` VALUES ('1518', '4', '百度推广', 'zlbdtg', 'SEM00000122', '8309', '1452822852');
INSERT INTO `go_data_statistics` VALUES ('1519', '30', '开心看', 'zlkxk', '', '8310', '1452822875');
INSERT INTO `go_data_statistics` VALUES ('1520', '4', '百度推广', 'zlbdtg', 'SEM00000122', '8311', '1452822907');
INSERT INTO `go_data_statistics` VALUES ('1521', '8', '一元众乐', 'yyzl', '', '8312', '1452823456');
INSERT INTO `go_data_statistics` VALUES ('1522', '8', '一元众乐', 'yyzl', '', '8313', '1452823622');
INSERT INTO `go_data_statistics` VALUES ('1523', '4', '百度推广', 'zlbdtg', 'YDSEM00000339', '8314', '1452824101');
INSERT INTO `go_data_statistics` VALUES ('1524', '4', '百度推广', 'zlbdtg', 'SEM00000115', '8315', '1452824341');
INSERT INTO `go_data_statistics` VALUES ('1525', '4', '百度推广', 'zlbdtg', 'SEM00000122', '8316', '1452824560');
INSERT INTO `go_data_statistics` VALUES ('1526', '30', '开心看', 'zlkxk', '', '8317', '1452824675');
INSERT INTO `go_data_statistics` VALUES ('1527', '4', '百度推广', 'zlbdtg', 'SEM00000115', '8318', '1452825358');
INSERT INTO `go_data_statistics` VALUES ('1528', '4', '百度推广', 'zlbdtg', 'SEM00000114', '8319', '1452825365');
INSERT INTO `go_data_statistics` VALUES ('1529', '8', '一元众乐', 'yyzl', '', '8320', '1452825842');
INSERT INTO `go_data_statistics` VALUES ('1530', '4', '百度推广', 'zlbdtg', 'YDSEM00000210', '8321', '1452826127');
INSERT INTO `go_data_statistics` VALUES ('1531', '8', '一元众乐', 'yyzl', '', '8322', '1452826316');
INSERT INTO `go_data_statistics` VALUES ('1532', '4', '百度推广', 'zlbdtg', 'SEM00000587', '8323', '1452826454');
INSERT INTO `go_data_statistics` VALUES ('1533', '30', '开心看', 'zlkxk', '', '8324', '1452826749');
INSERT INTO `go_data_statistics` VALUES ('1534', '21', 'oppo', 'oppo', '', '8325', '1452826854');
INSERT INTO `go_data_statistics` VALUES ('1535', '19', '木蚂蚁', 'mumayi', '', '8326', '1452826993');
INSERT INTO `go_data_statistics` VALUES ('1536', '30', '开心看', 'zlkxk', '', '8327', '1452827293');
INSERT INTO `go_data_statistics` VALUES ('1537', '8', '一元众乐', 'yyzl', '', '8328', '1452827589');
INSERT INTO `go_data_statistics` VALUES ('1538', '8', '一元众乐', 'yyzl', '', '8329', '1452827732');
INSERT INTO `go_data_statistics` VALUES ('1539', '8', '一元众乐', 'yyzl', '', '8330', '1452827886');
INSERT INTO `go_data_statistics` VALUES ('1540', '4', '百度推广', 'zlbdtg', 'SEM00000587', '8331', '1452828317');
INSERT INTO `go_data_statistics` VALUES ('1541', '30', '开心看', 'zlkxk', '', '8332', '1452828451');
INSERT INTO `go_data_statistics` VALUES ('1542', '4', '百度推广', 'zlbdtg', 'SEM00000115', '8333', '1452828513');
INSERT INTO `go_data_statistics` VALUES ('1543', '8', '一元众乐', 'yyzl', '', '8334', '1452828972');
INSERT INTO `go_data_statistics` VALUES ('1544', '8', '一元众乐', 'yyzl', '', '8335', '1452829096');
INSERT INTO `go_data_statistics` VALUES ('1545', '4', '百度推广', 'zlbdtg', 'SEM00000635', '8336', '1452829153');
INSERT INTO `go_data_statistics` VALUES ('1546', '4', '百度推广', 'zlbdtg', 'SEM00000121', '8337', '1452829556');
INSERT INTO `go_data_statistics` VALUES ('1547', '30', '开心看', 'zlkxk', '', '8338', '1452829758');
INSERT INTO `go_data_statistics` VALUES ('1548', '8', '一元众乐', 'yyzl', '', '8339', '1452829948');
INSERT INTO `go_data_statistics` VALUES ('1549', '8', '一元众乐', 'yyzl', '', '8341', '1452830365');
INSERT INTO `go_data_statistics` VALUES ('1550', '6', '信号增强器', 'zlxhzqq', '', '8342', '1452830976');
INSERT INTO `go_data_statistics` VALUES ('1551', '8', '一元众乐', 'yyzl', '', '8343', '1452831020');
INSERT INTO `go_data_statistics` VALUES ('1552', '19', '木蚂蚁', 'mumayi', '', '8344', '1452831644');
INSERT INTO `go_data_statistics` VALUES ('1553', '6', '信号增强器', 'zlxhzqq', '', '8345', '1452831664');
INSERT INTO `go_data_statistics` VALUES ('1554', '6', '信号增强器', 'zlxhzqq', '', '8346', '1452831965');
INSERT INTO `go_data_statistics` VALUES ('1555', '8', '一元众乐', 'yyzl', '', '8347', '1452832051');
INSERT INTO `go_data_statistics` VALUES ('1556', '17', 'PP助手', 'pp', '', '8348', '1452832068');
INSERT INTO `go_data_statistics` VALUES ('1557', '4', '百度推广', 'zlbdtg', 'SEM00000626', '8349', '1452832077');
INSERT INTO `go_data_statistics` VALUES ('1558', '4', '百度推广', 'zlbdtg', 'SEM00000572', '8350', '1452832254');
INSERT INTO `go_data_statistics` VALUES ('1559', '8', '一元众乐', 'yyzl', '', '8351', '1452832413');
INSERT INTO `go_data_statistics` VALUES ('1560', '9', '午夜艳视', 'wuyeyanshi', '', '8353', '1452832637');
INSERT INTO `go_data_statistics` VALUES ('1561', '4', '百度推广', 'zlbdtg', 'YDSEM00000573', '8354', '1452832988');
INSERT INTO `go_data_statistics` VALUES ('1562', '4', '百度推广', 'zlbdtg', 'YDSEM00000626', '8355', '1452833027');
INSERT INTO `go_data_statistics` VALUES ('1563', '4', '百度推广', 'zlbdtg', 'YDSEM00000626', '8356', '1452833871');
INSERT INTO `go_data_statistics` VALUES ('1564', '30', '开心看', 'zlkxk', '', '8357', '1452833971');
INSERT INTO `go_data_statistics` VALUES ('1565', '4', '百度推广', 'zlbdtg', 'SEM00000582', '8358', '1452834340');
INSERT INTO `go_data_statistics` VALUES ('1566', '8', '一元众乐', 'yyzl', '', '8359', '1452834567');
INSERT INTO `go_data_statistics` VALUES ('1567', '6', '信号增强器', 'zlxhzqq', '', '8360', '1452834572');
INSERT INTO `go_data_statistics` VALUES ('1568', '17', 'PP助手', 'pp', '', '8361', '1452835322');
INSERT INTO `go_data_statistics` VALUES ('1569', '6', '信号增强器', 'zlxhzqq', '', '8362', '1452835427');
INSERT INTO `go_data_statistics` VALUES ('1570', '8', '一元众乐', 'yyzl', '', '8363', '1452835434');
INSERT INTO `go_data_statistics` VALUES ('1571', '8', '一元众乐', 'yyzl', '', '8364', '1452835518');
INSERT INTO `go_data_statistics` VALUES ('1572', '8', '一元众乐', 'yyzl', '', '8365', '1452835860');
INSERT INTO `go_data_statistics` VALUES ('1573', '30', '开心看', 'zlkxk', '', '8366', '1452836453');
INSERT INTO `go_data_statistics` VALUES ('1574', '4', '百度推广', 'zlbdtg', 'YDSEM00000626', '8367', '1452836484');
INSERT INTO `go_data_statistics` VALUES ('1575', '19', '木蚂蚁', 'mumayi', '', '8368', '1452836828');
INSERT INTO `go_data_statistics` VALUES ('1576', '6', '信号增强器', 'zlxhzqq', '', '8369', '1452836998');
INSERT INTO `go_data_statistics` VALUES ('1577', '8', '一元众乐', 'yyzl', '', '8370', '1452837096');
INSERT INTO `go_data_statistics` VALUES ('1578', '8', '一元众乐', 'yyzl', '', '8371', '1452837330');
INSERT INTO `go_data_statistics` VALUES ('1579', '30', '开心看', 'zlkxk', '', '8372', '1452837420');
INSERT INTO `go_data_statistics` VALUES ('1580', '6', '信号增强器', 'zlxhzqq', '', '8373', '1452837606');
INSERT INTO `go_data_statistics` VALUES ('1581', '4', '百度推广', 'zlbdtg', 'SEM00000320', '8374', '1452837803');
INSERT INTO `go_data_statistics` VALUES ('1582', '17', 'PP助手', 'pp', '', '8376', '1452838055');
INSERT INTO `go_data_statistics` VALUES ('1583', '4', '百度推广', 'zlbdtg', 'SEM00000163', '8377', '1452838827');
INSERT INTO `go_data_statistics` VALUES ('1584', '30', '开心看', 'zlkxk', '', '8379', '1452839390');
INSERT INTO `go_data_statistics` VALUES ('1585', '30', '开心看', 'zlkxk', '', '8380', '1452839446');
INSERT INTO `go_data_statistics` VALUES ('1586', '4', '百度推广', 'zlbdtg', 'SEM00000120', '8381', '1452839898');
INSERT INTO `go_data_statistics` VALUES ('1587', '4', '百度推广', 'zlbdtg', 'YDSEM00000116', '8382', '1452839959');
INSERT INTO `go_data_statistics` VALUES ('1588', '8', '一元众乐', 'yyzl', '', '8383', '1452840025');
INSERT INTO `go_data_statistics` VALUES ('1589', '4', '百度推广', 'zlbdtg', 'SEM00000522', '8384', '1452840540');
INSERT INTO `go_data_statistics` VALUES ('1590', '4', '百度推广', 'zlbdtg', 'SEM00000115', '8386', '1452840684');
INSERT INTO `go_data_statistics` VALUES ('1591', '4', '百度推广', 'zlbdtg', 'SEM00000328', '8387', '1452841348');
INSERT INTO `go_data_statistics` VALUES ('1592', '30', '开心看', 'zlkxk', '', '8389', '1452841961');
INSERT INTO `go_data_statistics` VALUES ('1593', '4', '百度推广', 'zlbdtg', 'YDSEM00000573', '8391', '1452842215');
INSERT INTO `go_data_statistics` VALUES ('1594', '19', '木蚂蚁', 'mumayi', '', '8392', '1452842909');
INSERT INTO `go_data_statistics` VALUES ('1595', '4', '百度推广', 'zlbdtg', 'YDSEM00000588', '8393', '1452842914');
INSERT INTO `go_data_statistics` VALUES ('1596', '4', '百度推广', 'zlbdtg', 'SEM00000579', '8394', '1452843218');
INSERT INTO `go_data_statistics` VALUES ('1597', '8', '一元众乐', 'yyzl', '', '8395', '1452843370');
INSERT INTO `go_data_statistics` VALUES ('1598', '24', '魅族', 'meizu', '', '8397', '1452843821');
INSERT INTO `go_data_statistics` VALUES ('1599', '4', '百度推广', 'zlbdtg', 'SEM00000120', '8398', '1452843937');
INSERT INTO `go_data_statistics` VALUES ('1600', '30', '开心看', 'zlkxk', '', '8400', '1452844693');
INSERT INTO `go_data_statistics` VALUES ('1601', '8', '一元众乐', 'yyzl', '', '8402', '1452844880');
INSERT INTO `go_data_statistics` VALUES ('1602', '4', '百度推广', 'zlbdtg', 'SEM00000642', '8403', '1452845509');
INSERT INTO `go_data_statistics` VALUES ('1603', '30', '开心看', 'zlkxk', '', '8404', '1452846058');
INSERT INTO `go_data_statistics` VALUES ('1604', '30', '开心看', 'zlkxk', '', '8405', '1452846314');
INSERT INTO `go_data_statistics` VALUES ('1605', '4', '百度推广', 'zlbdtg', 'SEM00000115', '8408', '1452846528');
INSERT INTO `go_data_statistics` VALUES ('1606', '6', '信号增强器', 'zlxhzqq', '', '8409', '1452846682');
INSERT INTO `go_data_statistics` VALUES ('1607', '30', '开心看', 'zlkxk', '', '8410', '1452846823');
INSERT INTO `go_data_statistics` VALUES ('1608', '4', '百度推广', 'zlbdtg', 'SEM00000320', '8411', '1452847259');
INSERT INTO `go_data_statistics` VALUES ('1609', '30', '开心看', 'zlkxk', '', '8412', '1452847290');
INSERT INTO `go_data_statistics` VALUES ('1610', '8', '一元众乐', 'yyzl', '', '8413', '1452847305');
INSERT INTO `go_data_statistics` VALUES ('1611', '29', '小米开发者平台', 'xiaomi', '', '8414', '1452847531');
INSERT INTO `go_data_statistics` VALUES ('1612', '4', '百度推广', 'zlbdtg', 'YDSEM00000115', '8415', '1452847678');
INSERT INTO `go_data_statistics` VALUES ('1613', '4', '百度推广', 'zlbdtg', 'YDSEM00000117', '8416', '1452848185');
INSERT INTO `go_data_statistics` VALUES ('1614', '30', '开心看', 'zlkxk', '', '8417', '1452848264');
INSERT INTO `go_data_statistics` VALUES ('1615', '30', '开心看', 'zlkxk', '', '8418', '1452848607');
INSERT INTO `go_data_statistics` VALUES ('1616', '4', '百度推广', 'zlbdtg', 'SEM00000115', '8419', '1452848645');
INSERT INTO `go_data_statistics` VALUES ('1617', '4', '百度推广', 'zlbdtg', 'SEM00000225', '8420', '1452849009');
INSERT INTO `go_data_statistics` VALUES ('1618', '30', '开心看', 'zlkxk', '', '8421', '1452849091');
INSERT INTO `go_data_statistics` VALUES ('1619', '4', '百度推广', 'zlbdtg', 'SEM00000225', '8422', '1452849141');
INSERT INTO `go_data_statistics` VALUES ('1620', '8', '一元众乐', 'yyzl', '', '8423', '1452849206');
INSERT INTO `go_data_statistics` VALUES ('1621', '30', '开心看', 'zlkxk', '', '8424', '1452849579');
INSERT INTO `go_data_statistics` VALUES ('1622', '4', '百度推广', 'zlbdtg', 'SEM00000587', '8425', '1452849759');
INSERT INTO `go_data_statistics` VALUES ('1623', '30', '开心看', 'zlkxk', '', '8426', '1452850851');
INSERT INTO `go_data_statistics` VALUES ('1624', '6', '信号增强器', 'zlxhzqq', '', '8427', '1452851003');
INSERT INTO `go_data_statistics` VALUES ('1625', '30', '开心看', 'zlkxk', '', '8428', '1452851222');
INSERT INTO `go_data_statistics` VALUES ('1626', '8', '一元众乐', 'yyzl', '', '8429', '1452851443');
INSERT INTO `go_data_statistics` VALUES ('1627', '6', '信号增强器', 'zlxhzqq', '', '8430', '1452851737');
INSERT INTO `go_data_statistics` VALUES ('1628', '4', '百度推广', 'zlbdtg', 'SEM00000121', '8431', '1452851979');
INSERT INTO `go_data_statistics` VALUES ('1629', '4', '百度推广', 'zlbdtg', 'YDSEM00000573', '8432', '1452852134');
INSERT INTO `go_data_statistics` VALUES ('1630', '4', '百度推广', 'zlbdtg', 'SEM00000573', '8433', '1452852135');
INSERT INTO `go_data_statistics` VALUES ('1631', '8', '一元众乐', 'yyzl', '', '8434', '1452852193');
INSERT INTO `go_data_statistics` VALUES ('1632', '6', '信号增强器', 'zlxhzqq', '', '8435', '1452852555');
INSERT INTO `go_data_statistics` VALUES ('1633', '4', '百度推广', 'zlbdtg', 'YDSEM00000339', '8436', '1452852800');
INSERT INTO `go_data_statistics` VALUES ('1634', '4', '百度推广', 'zlbdtg', 'SEM00000115', '8437', '1452853165');
INSERT INTO `go_data_statistics` VALUES ('1635', '8', '一元众乐', 'yyzl', '', '8438', '1452853272');
INSERT INTO `go_data_statistics` VALUES ('1636', '30', '开心看', 'zlkxk', '', '8439', '1452853686');
INSERT INTO `go_data_statistics` VALUES ('1637', '4', '百度推广', 'zlbdtg', 'YDSEM00000573', '8440', '1452853810');
INSERT INTO `go_data_statistics` VALUES ('1638', '19', '木蚂蚁', 'mumayi', '', '8441', '1452853880');
INSERT INTO `go_data_statistics` VALUES ('1639', '8', '一元众乐', 'yyzl', '', '8442', '1452854131');
INSERT INTO `go_data_statistics` VALUES ('1640', '4', '百度推广', 'zlbdtg', 'YDSEM00000628', '8443', '1452854387');
INSERT INTO `go_data_statistics` VALUES ('1641', '4', '百度推广', 'zlbdtg', 'SEM00000635', '8444', '1452854398');
INSERT INTO `go_data_statistics` VALUES ('1642', '4', '百度推广', 'zlbdtg', 'YDSEM00000628', '8445', '1452854437');
INSERT INTO `go_data_statistics` VALUES ('1643', '4', '百度推广', 'zlbdtg', 'YDSEM00000518', '8446', '1452854797');
INSERT INTO `go_data_statistics` VALUES ('1644', '8', '一元众乐', 'yyzl', '', '8447', '1452854879');
INSERT INTO `go_data_statistics` VALUES ('1645', '4', '百度推广', 'zlbdtg', 'YDSEM00000121', '8448', '1452855200');
INSERT INTO `go_data_statistics` VALUES ('1646', '15', '百度', 'baidu', '', '8449', '1452855328');
INSERT INTO `go_data_statistics` VALUES ('1647', '30', '开心看', 'zlkxk', '', '8450', '1452855521');
INSERT INTO `go_data_statistics` VALUES ('1648', '30', '开心看', 'zlkxk', '', '8451', '1452855757');
INSERT INTO `go_data_statistics` VALUES ('1649', '4', '百度推广', 'zlbdtg', 'YDSEM00000115', '8464', '1452856018');
INSERT INTO `go_data_statistics` VALUES ('1650', '8', '一元众乐', 'yyzl', '', '8465', '1452856268');
INSERT INTO `go_data_statistics` VALUES ('1651', '4', '百度推广', 'zlbdtg', 'YDSEM00000121', '8466', '1452856517');
INSERT INTO `go_data_statistics` VALUES ('1652', '4', '百度推广', 'zlbdtg', 'YDSEM00000339', '8467', '1452856522');
INSERT INTO `go_data_statistics` VALUES ('1653', '4', '百度推广', 'zlbdtg', 'SEM00000622', '8468', '1452856654');
INSERT INTO `go_data_statistics` VALUES ('1654', '4', '百度推广', 'zlbdtg', 'YDSEM00000116', '8469', '1452856738');
INSERT INTO `go_data_statistics` VALUES ('1655', '8', '一元众乐', 'yyzl', '', '8470', '1452856765');
INSERT INTO `go_data_statistics` VALUES ('1656', '4', '百度推广', 'zlbdtg', 'SEM00000114', '8471', '1452856771');
INSERT INTO `go_data_statistics` VALUES ('1657', '8', '一元众乐', 'yyzl', '', '8473', '1452857012');
INSERT INTO `go_data_statistics` VALUES ('1658', '8', '一元众乐', 'yyzl', '', '8474', '1452857046');
INSERT INTO `go_data_statistics` VALUES ('1659', '4', '百度推广', 'zlbdtg', 'YDSEM00000607', '8475', '1452857104');
INSERT INTO `go_data_statistics` VALUES ('1660', '30', '开心看', 'zlkxk', '', '8476', '1452857186');
INSERT INTO `go_data_statistics` VALUES ('1661', '4', '百度推广', 'zlbdtg', 'SEM00000320', '8477', '1452857209');
INSERT INTO `go_data_statistics` VALUES ('1662', '4', '百度推广', 'zlbdtg', 'SEM00000121', '8478', '1452857495');
INSERT INTO `go_data_statistics` VALUES ('1663', '4', '百度推广', 'zlbdtg', 'SEM00000522', '8479', '1452857543');
INSERT INTO `go_data_statistics` VALUES ('1664', '4', '百度推广', 'zlbdtg', 'SEM00000122', '8480', '1452857731');
INSERT INTO `go_data_statistics` VALUES ('1665', '30', '开心看', 'zlkxk', '', '8482', '1452857934');
INSERT INTO `go_data_statistics` VALUES ('1666', '4', '百度推广', 'zlbdtg', 'SEM00000115', '8483', '1452858221');
INSERT INTO `go_data_statistics` VALUES ('1667', '4', '百度推广', 'zlbdtg', 'SEM00000115', '8484', '1452858331');
INSERT INTO `go_data_statistics` VALUES ('1668', '4', '百度推广', 'zlbdtg', 'YDSEM00000116', '8485', '1452858755');
INSERT INTO `go_data_statistics` VALUES ('1669', '6', '信号增强器', 'zlxhzqq', '', '8487', '1452858852');
INSERT INTO `go_data_statistics` VALUES ('1670', '8', '一元众乐', 'yyzl', '', '8488', '1452859150');
INSERT INTO `go_data_statistics` VALUES ('1671', '30', '开心看', 'zlkxk', '', '8489', '1452859255');
INSERT INTO `go_data_statistics` VALUES ('1672', '30', '开心看', 'zlkxk', '', '8490', '1452859435');
INSERT INTO `go_data_statistics` VALUES ('1673', '4', '百度推广', 'zlbdtg', 'SEM00000122', '8491', '1452859606');
INSERT INTO `go_data_statistics` VALUES ('1674', '8', '一元众乐', 'yyzl', '', '8492', '1452859845');
INSERT INTO `go_data_statistics` VALUES ('1675', '8', '一元众乐', 'yyzl', '', '8493', '1452859847');
INSERT INTO `go_data_statistics` VALUES ('1676', '4', '百度推广', 'zlbdtg', 'YDSEM00000115', '8494', '1452860065');
INSERT INTO `go_data_statistics` VALUES ('1677', '8', '一元众乐', 'yyzl', '', '8496', '1452860496');
INSERT INTO `go_data_statistics` VALUES ('1678', '8', '一元众乐', 'yyzl', '', '8498', '1452860795');
INSERT INTO `go_data_statistics` VALUES ('1679', '8', '一元众乐', 'yyzl', '', '8499', '1452860883');
INSERT INTO `go_data_statistics` VALUES ('1680', '4', '百度推广', 'zlbdtg', 'SEM00000122', '8500', '1452861206');
INSERT INTO `go_data_statistics` VALUES ('1681', '30', '开心看', 'zlkxk', '', '8501', '1452861605');
INSERT INTO `go_data_statistics` VALUES ('1682', '4', '百度推广', 'zlbdtg', 'SEM00000097', '8502', '1452861710');
INSERT INTO `go_data_statistics` VALUES ('1683', '6', '信号增强器', 'zlxhzqq', '', '8503', '1452861840');
INSERT INTO `go_data_statistics` VALUES ('1684', '8', '一元众乐', 'yyzl', '', '8504', '1452862072');
INSERT INTO `go_data_statistics` VALUES ('1685', '4', '百度推广', 'zlbdtg', 'SEM00000339', '8505', '1452862379');
INSERT INTO `go_data_statistics` VALUES ('1686', '8', '一元众乐', 'yyzl', '', '8506', '1452862592');
INSERT INTO `go_data_statistics` VALUES ('1687', '4', '百度推广', 'zlbdtg', 'YDSEM00000117', '8507', '1452862602');
INSERT INTO `go_data_statistics` VALUES ('1688', '4', '百度推广', 'zlbdtg', 'YDSEM00000117', '8508', '1452862651');
INSERT INTO `go_data_statistics` VALUES ('1689', '8', '一元众乐', 'yyzl', '', '8509', '1452862659');
INSERT INTO `go_data_statistics` VALUES ('1690', '4', '百度推广', 'zlbdtg', 'YDSEM00000142', '8510', '1452863231');
INSERT INTO `go_data_statistics` VALUES ('1691', '4', '百度推广', 'zlbdtg', 'YDSEM00000117', '8511', '1452863536');
INSERT INTO `go_data_statistics` VALUES ('1692', '4', '百度推广', 'zlbdtg', 'SEM00000117', '8512', '1452863578');
INSERT INTO `go_data_statistics` VALUES ('1693', '4', '百度推广', 'zlbdtg', 'YDSEM00000117', '8513', '1452863634');
INSERT INTO `go_data_statistics` VALUES ('1694', '4', '百度推广', 'zlbdtg', 'YDSEM00000121', '8515', '1452863782');
INSERT INTO `go_data_statistics` VALUES ('1695', '30', '开心看', 'zlkxk', '', '8516', '1452863865');
INSERT INTO `go_data_statistics` VALUES ('1696', '30', '开心看', 'zlkxk', '', '8517', '1452863974');
INSERT INTO `go_data_statistics` VALUES ('1697', '4', '百度推广', 'zlbdtg', 'SEM00000572', '8518', '1452863988');
INSERT INTO `go_data_statistics` VALUES ('1698', '4', '百度推广', 'zlbdtg', 'SEM00000531', '8519', '1452864094');
INSERT INTO `go_data_statistics` VALUES ('1699', '8', '一元众乐', 'yyzl', '', '8520', '1452864254');
INSERT INTO `go_data_statistics` VALUES ('1700', '4', '百度推广', 'zlbdtg', 'SEM00000531', '8521', '1452864449');
INSERT INTO `go_data_statistics` VALUES ('1701', '4', '百度推广', 'zlbdtg', 'YDSEM00000339', '8522', '1452864829');
INSERT INTO `go_data_statistics` VALUES ('1702', '4', '百度推广', 'zlbdtg', 'SEM00000122', '8524', '1452865111');
INSERT INTO `go_data_statistics` VALUES ('1703', '30', '开心看', 'zlkxk', '', '8525', '1452865466');
INSERT INTO `go_data_statistics` VALUES ('1704', '19', '木蚂蚁', 'mumayi', '', '8526', '1452865604');
INSERT INTO `go_data_statistics` VALUES ('1705', '8', '一元众乐', 'yyzl', '', '8527', '1452865847');
INSERT INTO `go_data_statistics` VALUES ('1706', '4', '百度推广', 'zlbdtg', 'SEM00000121', '8528', '1452866046');
INSERT INTO `go_data_statistics` VALUES ('1707', '4', '百度推广', 'zlbdtg', 'YDSEM00000628', '8529', '1452866113');
INSERT INTO `go_data_statistics` VALUES ('1708', '8', '一元众乐', 'yyzl', '', '8530', '1452866148');
INSERT INTO `go_data_statistics` VALUES ('1709', '30', '开心看', 'zlkxk', '', '8531', '1452866633');
INSERT INTO `go_data_statistics` VALUES ('1710', '8', '一元众乐', 'yyzl', '', '8532', '1452866652');
INSERT INTO `go_data_statistics` VALUES ('1711', '8', '一元众乐', 'yyzl', '', '8534', '1452866849');
INSERT INTO `go_data_statistics` VALUES ('1712', '30', '开心看', 'zlkxk', '', '8535', '1452866890');
INSERT INTO `go_data_statistics` VALUES ('1713', '4', '百度推广', 'zlbdtg', 'SEM00000115', '8536', '1452867348');
INSERT INTO `go_data_statistics` VALUES ('1714', '21', 'oppo', 'oppo', '', '8538', '1452867727');
INSERT INTO `go_data_statistics` VALUES ('1715', '4', '百度推广', 'zlbdtg', 'YDSEM00000114', '8539', '1452867811');
INSERT INTO `go_data_statistics` VALUES ('1716', '15', '百度', 'baidu', '', '8540', '1452867841');
INSERT INTO `go_data_statistics` VALUES ('1717', '4', '百度推广', 'zlbdtg', 'SEM00000643', '8541', '1452867858');
INSERT INTO `go_data_statistics` VALUES ('1718', '4', '百度推广', 'zlbdtg', 'SEM00000573', '8542', '1452867966');
INSERT INTO `go_data_statistics` VALUES ('1719', '4', '百度推广', 'zlbdtg', 'SEM00000115', '8543', '1452868303');
INSERT INTO `go_data_statistics` VALUES ('1720', '4', '百度推广', 'zlbdtg', 'SEM00000122', '8544', '1452868494');
INSERT INTO `go_data_statistics` VALUES ('1721', '6', '信号增强器', 'zlxhzqq', '', '8545', '1452868517');
INSERT INTO `go_data_statistics` VALUES ('1722', '4', '百度推广', 'zlbdtg', 'SEM00000178', '8546', '1452868657');
INSERT INTO `go_data_statistics` VALUES ('1723', '4', '百度推广', 'zlbdtg', 'YDSEM00000573', '8547', '1452869101');
INSERT INTO `go_data_statistics` VALUES ('1724', '8', '一元众乐', 'yyzl', '', '8549', '1452869670');
INSERT INTO `go_data_statistics` VALUES ('1725', '30', '开心看', 'zlkxk', '', '8551', '1452870210');
INSERT INTO `go_data_statistics` VALUES ('1726', '8', '一元众乐', 'yyzl', '', '8552', '1452870391');
INSERT INTO `go_data_statistics` VALUES ('1727', '8', '一元众乐', 'yyzl', '', '8553', '1452870401');
INSERT INTO `go_data_statistics` VALUES ('1728', '4', '百度推广', 'zlbdtg', 'YDSEM00000628', '8554', '1452870459');
INSERT INTO `go_data_statistics` VALUES ('1729', '4', '百度推广', 'zlbdtg', 'YDSEM00000573', '8555', '1452870555');
INSERT INTO `go_data_statistics` VALUES ('1730', '4', '百度推广', 'zlbdtg', 'SEM00000622', '8556', '1452870657');
INSERT INTO `go_data_statistics` VALUES ('1731', '4', '百度推广', 'zlbdtg', 'YDSEM00000142', '8557', '1452871085');
INSERT INTO `go_data_statistics` VALUES ('1732', '8', '一元众乐', 'yyzl', '', '8558', '1452871115');
INSERT INTO `go_data_statistics` VALUES ('1733', '19', '木蚂蚁', 'mumayi', '', '8559', '1452871120');
INSERT INTO `go_data_statistics` VALUES ('1734', '8', '一元众乐', 'yyzl', '', '8560', '1452871128');
INSERT INTO `go_data_statistics` VALUES ('1735', '30', '开心看', 'zlkxk', '', '8561', '1452871378');
INSERT INTO `go_data_statistics` VALUES ('1736', '30', '开心看', 'zlkxk', '', '8562', '1452872162');
INSERT INTO `go_data_statistics` VALUES ('1737', '6', '信号增强器', 'zlxhzqq', '', '8563', '1452873393');
INSERT INTO `go_data_statistics` VALUES ('1738', '4', '百度推广', 'zlbdtg', 'SEM00000121', '8564', '1452873916');
INSERT INTO `go_data_statistics` VALUES ('1739', '8', '一元众乐', 'yyzl', '', '8565', '1452874547');
INSERT INTO `go_data_statistics` VALUES ('1740', '4', '百度推广', 'zlbdtg', 'YDSEM00000115', '8566', '1452874629');
INSERT INTO `go_data_statistics` VALUES ('1741', '6', '信号增强器', 'zlxhzqq', '', '8567', '1452874756');
INSERT INTO `go_data_statistics` VALUES ('1742', '30', '开心看', 'zlkxk', '', '8568', '1452874757');
INSERT INTO `go_data_statistics` VALUES ('1743', '29', '小米开发者平台', 'xiaomi', '', '8569', '1452875469');
INSERT INTO `go_data_statistics` VALUES ('1744', '19', '木蚂蚁', 'mumayi', '', '8571', '1452876621');
INSERT INTO `go_data_statistics` VALUES ('1745', '30', '开心看', 'zlkxk', '', '8572', '1452876684');
INSERT INTO `go_data_statistics` VALUES ('1746', '4', '百度推广', 'zlbdtg', 'YDSEM00000115', '8573', '1452876999');
INSERT INTO `go_data_statistics` VALUES ('1747', '4', '百度推广', 'zlbdtg', 'YDSEM00000339', '8574', '1452877551');
INSERT INTO `go_data_statistics` VALUES ('1748', '8', '一元众乐', 'yyzl', '', '8575', '1452877659');
INSERT INTO `go_data_statistics` VALUES ('1749', '30', '开心看', 'zlkxk', '', '8576', '1452878378');
INSERT INTO `go_data_statistics` VALUES ('1750', '4', '百度推广', 'zlbdtg', 'SEM00000244', '8577', '1452878918');
INSERT INTO `go_data_statistics` VALUES ('1751', '4', '百度推广', 'zlbdtg', 'YDSEM00000628', '8578', '1452880102');
INSERT INTO `go_data_statistics` VALUES ('1752', '4', '百度推广', 'zlbdtg', 'YDSEM00000628', '8579', '1452880369');
INSERT INTO `go_data_statistics` VALUES ('1753', '4', '百度推广', 'zlbdtg', 'SEM00000117', '8580', '1452880438');
INSERT INTO `go_data_statistics` VALUES ('1754', '4', '百度推广', 'zlbdtg', 'SEM00000056', '8581', '1452881230');
INSERT INTO `go_data_statistics` VALUES ('1755', '4', '百度推广', 'zlbdtg', 'SEM00000117', '8582', '1452881724');
INSERT INTO `go_data_statistics` VALUES ('1756', '4', '百度推广', 'zlbdtg', 'SEM00000341', '8583', '1452883168');
INSERT INTO `go_data_statistics` VALUES ('1757', '4', '百度推广', 'zlbdtg', 'YDSEM00000628', '8584', '1452883556');
INSERT INTO `go_data_statistics` VALUES ('1758', '4', '百度推广', 'zlbdtg', 'YDSEM00000628', '8585', '1452883571');
INSERT INTO `go_data_statistics` VALUES ('1759', '4', '百度推广', 'zlbdtg', 'SEM00000622', '8586', '1452883627');
INSERT INTO `go_data_statistics` VALUES ('1760', '4', '百度推广', 'zlbdtg', 'SEM00000528', '8587', '1452886507');
INSERT INTO `go_data_statistics` VALUES ('1761', '4', '百度推广', 'zlbdtg', 'SEM00000622', '8588', '1452887622');
INSERT INTO `go_data_statistics` VALUES ('1762', '6', '信号增强器', 'zlxhzqq', '', '8589', '1452887826');
INSERT INTO `go_data_statistics` VALUES ('1763', '4', '百度推广', 'zlbdtg', 'YDSEM00000114', '8590', '1452889240');
INSERT INTO `go_data_statistics` VALUES ('1764', '8', '一元众乐', 'yyzl', '', '8591', '1452890999');
INSERT INTO `go_data_statistics` VALUES ('1765', '4', '百度推广', 'zlbdtg', 'YDSEM00000635', '8594', '1452892867');
INSERT INTO `go_data_statistics` VALUES ('1766', '4', '百度推广', 'zlbdtg', 'SEM00000115', '8595', '1452893602');
INSERT INTO `go_data_statistics` VALUES ('1767', '6', '信号增强器', 'zlxhzqq', '', '8596', '1452895353');
INSERT INTO `go_data_statistics` VALUES ('1768', '4', '百度推广', 'zlbdtg', 'SEM00000625', '8597', '1452895551');
INSERT INTO `go_data_statistics` VALUES ('1769', '4', '百度推广', 'zlbdtg', 'YDSEM00000117', '8598', '1452895761');
INSERT INTO `go_data_statistics` VALUES ('1770', '4', '百度推广', 'zlbdtg', 'SEM00000573', '8600', '1452898724');
INSERT INTO `go_data_statistics` VALUES ('1771', '6', '信号增强器', 'zlxhzqq', '', '8601', '1452899745');
INSERT INTO `go_data_statistics` VALUES ('1772', '8', '一元众乐', 'yyzl', '', '8602', '1452900011');
INSERT INTO `go_data_statistics` VALUES ('1773', '6', '信号增强器', 'zlxhzqq', '', '8603', '1452901534');
INSERT INTO `go_data_statistics` VALUES ('1774', '4', '百度推广', 'zlbdtg', 'SEM00000622', '8604', '1452901553');
INSERT INTO `go_data_statistics` VALUES ('1775', '4', '百度推广', 'zlbdtg', 'SEM00000622', '8605', '1452901682');
INSERT INTO `go_data_statistics` VALUES ('1776', '4', '百度推广', 'zlbdtg', 'SEM00000622', '8607', '1452901798');
INSERT INTO `go_data_statistics` VALUES ('1777', '4', '百度推广', 'zlbdtg', 'YDSEM00000114', '8608', '1452902135');
INSERT INTO `go_data_statistics` VALUES ('1778', '4', '百度推广', 'zlbdtg', 'SEM00000121', '8609', '1452902248');
INSERT INTO `go_data_statistics` VALUES ('1779', '30', '开心看', 'zlkxk', '', '8610', '1452902597');
INSERT INTO `go_data_statistics` VALUES ('1780', '4', '百度推广', 'zlbdtg', 'SEM00000572', '8611', '1452903045');
INSERT INTO `go_data_statistics` VALUES ('1781', '30', '开心看', 'zlkxk', '', '8612', '1452904545');
INSERT INTO `go_data_statistics` VALUES ('1782', '8', '一元众乐', 'yyzl', '', '8613', '1452904912');
INSERT INTO `go_data_statistics` VALUES ('1783', '6', '信号增强器', 'zlxhzqq', '', '8614', '1452905059');
INSERT INTO `go_data_statistics` VALUES ('1784', '6', '信号增强器', 'zlxhzqq', '', '8617', '1452905829');
INSERT INTO `go_data_statistics` VALUES ('1785', '8', '一元众乐', 'yyzl', '', '8618', '1452906116');
INSERT INTO `go_data_statistics` VALUES ('1786', '8', '一元众乐', 'yyzl', '', '8619', '1452906625');
INSERT INTO `go_data_statistics` VALUES ('1787', '8', '一元众乐', 'yyzl', '', '8620', '1452906802');
INSERT INTO `go_data_statistics` VALUES ('1788', '2', '儒豹浏览器', 'rbower', '', '8621', '1452906849');
INSERT INTO `go_data_statistics` VALUES ('1789', '8', '一元众乐', 'yyzl', '', '8622', '1452907057');
INSERT INTO `go_data_statistics` VALUES ('1790', '30', '开心看', 'zlkxk', '', '8623', '1452907271');
INSERT INTO `go_data_statistics` VALUES ('1791', '8', '一元众乐', 'yyzl', '', '8624', '1452907600');
INSERT INTO `go_data_statistics` VALUES ('1792', '4', '百度推广', 'zlbdtg', 'YDSEM00000142', '8625', '1452907630');
INSERT INTO `go_data_statistics` VALUES ('1793', '8', '一元众乐', 'yyzl', '', '8626', '1452907749');
INSERT INTO `go_data_statistics` VALUES ('1794', '30', '开心看', 'zlkxk', '', '8627', '1452907821');
INSERT INTO `go_data_statistics` VALUES ('1795', '8', '一元众乐', 'yyzl', '', '8628', '1452907875');
INSERT INTO `go_data_statistics` VALUES ('1796', '30', '开心看', 'zlkxk', '', '8629', '1452907971');
INSERT INTO `go_data_statistics` VALUES ('1797', '6', '信号增强器', 'zlxhzqq', '', '8631', '1452908314');
INSERT INTO `go_data_statistics` VALUES ('1798', '30', '开心看', 'zlkxk', '', '8632', '1452908497');
INSERT INTO `go_data_statistics` VALUES ('1799', '8', '一元众乐', 'yyzl', '', '8634', '1452908647');
INSERT INTO `go_data_statistics` VALUES ('1800', '4', '百度推广', 'zlbdtg', 'SEM00000115', '8635', '1452908663');
INSERT INTO `go_data_statistics` VALUES ('1801', '4', '百度推广', 'zlbdtg', 'SEM00000573', '8636', '1452909342');
INSERT INTO `go_data_statistics` VALUES ('1802', '30', '开心看', 'zlkxk', '', '8637', '1452909522');
INSERT INTO `go_data_statistics` VALUES ('1803', '4', '百度推广', 'zlbdtg', 'SEM00000587', '8638', '1452909644');
INSERT INTO `go_data_statistics` VALUES ('1804', '30', '开心看', 'zlkxk', '', '8639', '1452909662');
INSERT INTO `go_data_statistics` VALUES ('1805', '4', '百度推广', 'zlbdtg', 'YDSEM00000115', '8640', '1452909674');
INSERT INTO `go_data_statistics` VALUES ('1806', '8', '一元众乐', 'yyzl', '', '8641', '1452909730');
INSERT INTO `go_data_statistics` VALUES ('1807', '6', '信号增强器', 'zlxhzqq', '', '8642', '1452909817');
INSERT INTO `go_data_statistics` VALUES ('1808', '30', '开心看', 'zlkxk', '', '8643', '1452909996');
INSERT INTO `go_data_statistics` VALUES ('1809', '4', '百度推广', 'zlbdtg', 'SEM00000121', '8644', '1452910141');
INSERT INTO `go_data_statistics` VALUES ('1810', '30', '开心看', 'zlkxk', '', '8645', '1452910397');
INSERT INTO `go_data_statistics` VALUES ('1811', '30', '开心看', 'zlkxk', '', '8646', '1452910505');
INSERT INTO `go_data_statistics` VALUES ('1812', '30', '开心看', 'zlkxk', '', '8647', '1452910521');
INSERT INTO `go_data_statistics` VALUES ('1813', '8', '一元众乐', 'yyzl', '', '8648', '1452910592');
INSERT INTO `go_data_statistics` VALUES ('1814', '19', '木蚂蚁', 'mumayi', '', '8649', '1452910652');
INSERT INTO `go_data_statistics` VALUES ('1815', '30', '开心看', 'zlkxk', '', '8650', '1452910690');
INSERT INTO `go_data_statistics` VALUES ('1816', '8', '一元众乐', 'yyzl', '', '8651', '1452910809');
INSERT INTO `go_data_statistics` VALUES ('1817', '8', '一元众乐', 'yyzl', '', '8652', '1452910823');
INSERT INTO `go_data_statistics` VALUES ('1818', '30', '开心看', 'zlkxk', '', '8653', '1452910903');
INSERT INTO `go_data_statistics` VALUES ('1819', '30', '开心看', 'zlkxk', '', '8654', '1452910908');
INSERT INTO `go_data_statistics` VALUES ('1820', '30', '开心看', 'zlkxk', '', '8655', '1452910977');
INSERT INTO `go_data_statistics` VALUES ('1821', '8', '一元众乐', 'yyzl', '', '8656', '1452911058');
INSERT INTO `go_data_statistics` VALUES ('1822', '16', '豌豆荚', 'wandoujia', '', '8657', '1452911165');
INSERT INTO `go_data_statistics` VALUES ('1823', '4', '百度推广', 'zlbdtg', 'SEM00000115', '8659', '1452911417');
INSERT INTO `go_data_statistics` VALUES ('1824', '24', '魅族', 'meizu', '', '8660', '1452911640');
INSERT INTO `go_data_statistics` VALUES ('1825', '30', '开心看', 'zlkxk', '', '8661', '1452911694');
INSERT INTO `go_data_statistics` VALUES ('1826', '30', '开心看', 'zlkxk', '', '8662', '1452911935');
INSERT INTO `go_data_statistics` VALUES ('1827', '30', '开心看', 'zlkxk', '', '8663', '1452912018');
INSERT INTO `go_data_statistics` VALUES ('1828', '30', '开心看', 'zlkxk', '', '8664', '1452912168');
INSERT INTO `go_data_statistics` VALUES ('1829', '8', '一元众乐', 'yyzl', '', '8666', '1452912272');
INSERT INTO `go_data_statistics` VALUES ('1830', '8', '一元众乐', 'yyzl', '', '8667', '1452912357');
INSERT INTO `go_data_statistics` VALUES ('1831', '8', '一元众乐', 'yyzl', '', '8668', '1452912440');
INSERT INTO `go_data_statistics` VALUES ('1832', '30', '开心看', 'zlkxk', '', '8669', '1452912822');
INSERT INTO `go_data_statistics` VALUES ('1833', '30', '开心看', 'zlkxk', '', '8670', '1452913235');
INSERT INTO `go_data_statistics` VALUES ('1834', '30', '开心看', 'zlkxk', '', '8671', '1452913770');
INSERT INTO `go_data_statistics` VALUES ('1835', '30', '开心看', 'zlkxk', '', '8672', '1452913816');
INSERT INTO `go_data_statistics` VALUES ('1836', '4', '百度推广', 'zlbdtg', 'YDSEM00000402', '8673', '1452913871');
INSERT INTO `go_data_statistics` VALUES ('1837', '8', '一元众乐', 'yyzl', '', '8674', '1452914149');
INSERT INTO `go_data_statistics` VALUES ('1838', '30', '开心看', 'zlkxk', '', '8675', '1452914171');
INSERT INTO `go_data_statistics` VALUES ('1839', '30', '开心看', 'zlkxk', '', '8676', '1452914182');
INSERT INTO `go_data_statistics` VALUES ('1840', '8', '一元众乐', 'yyzl', '', '8677', '1452914194');
INSERT INTO `go_data_statistics` VALUES ('1841', '8', '一元众乐', 'yyzl', '', '8678', '1452914212');
INSERT INTO `go_data_statistics` VALUES ('1842', '4', '百度推广', 'zlbdtg', 'SEM00000122', '8679', '1452914418');
INSERT INTO `go_data_statistics` VALUES ('1843', '8', '一元众乐', 'yyzl', '', '8680', '1452914522');
INSERT INTO `go_data_statistics` VALUES ('1844', '8', '一元众乐', 'yyzl', '', '8681', '1452914658');
INSERT INTO `go_data_statistics` VALUES ('1845', '8', '一元众乐', 'yyzl', '', '8682', '1452914840');
INSERT INTO `go_data_statistics` VALUES ('1846', '4', '百度推广', 'zlbdtg', 'YDSEM00000115', '8683', '1452915124');
INSERT INTO `go_data_statistics` VALUES ('1847', '30', '开心看', 'zlkxk', '', '8684', '1452915193');
INSERT INTO `go_data_statistics` VALUES ('1848', '30', '开心看', 'zlkxk', '', '8685', '1452915636');
INSERT INTO `go_data_statistics` VALUES ('1849', '21', 'oppo', 'oppo', '', '8686', '1452915805');
INSERT INTO `go_data_statistics` VALUES ('1850', '4', '百度推广', 'zlbdtg', 'SEM00000121', '8687', '1452915823');
INSERT INTO `go_data_statistics` VALUES ('1851', '4', '百度推广', 'zlbdtg', 'YDSEM00000163', '8689', '1452916162');
INSERT INTO `go_data_statistics` VALUES ('1852', '4', '百度推广', 'zlbdtg', 'YDSEM00000163', '8691', '1452916578');
INSERT INTO `go_data_statistics` VALUES ('1853', '4', '百度推广', 'zlbdtg', 'YDSEM00000142', '8692', '1452916611');
INSERT INTO `go_data_statistics` VALUES ('1854', '19', '木蚂蚁', 'mumayi', '', '8693', '1452916940');
INSERT INTO `go_data_statistics` VALUES ('1855', '8', '一元众乐', 'yyzl', '', '8694', '1452917009');
INSERT INTO `go_data_statistics` VALUES ('1856', '4', '百度推广', 'zlbdtg', 'YDSEM00000573', '8695', '1452917176');
INSERT INTO `go_data_statistics` VALUES ('1857', '8', '一元众乐', 'yyzl', '', '8696', '1452917240');
INSERT INTO `go_data_statistics` VALUES ('1858', '30', '开心看', 'zlkxk', '', '8698', '1452917542');
INSERT INTO `go_data_statistics` VALUES ('1859', '8', '一元众乐', 'yyzl', '', '8699', '1452917607');
INSERT INTO `go_data_statistics` VALUES ('1860', '30', '开心看', 'zlkxk', '', '8700', '1452917910');
INSERT INTO `go_data_statistics` VALUES ('1861', '8', '一元众乐', 'yyzl', '', '8701', '1452918529');
INSERT INTO `go_data_statistics` VALUES ('1862', '4', '百度推广', 'zlbdtg', 'YDSEM00000116', '8702', '1452918661');
INSERT INTO `go_data_statistics` VALUES ('1863', '8', '一元众乐', 'yyzl', '', '8703', '1452918922');
INSERT INTO `go_data_statistics` VALUES ('1864', '4', '百度推广', 'zlbdtg', 'SEM00000622', '8704', '1452919085');
INSERT INTO `go_data_statistics` VALUES ('1865', '30', '开心看', 'zlkxk', '', '8705', '1452919102');
INSERT INTO `go_data_statistics` VALUES ('1866', '8', '一元众乐', 'yyzl', '', '8706', '1452919326');
INSERT INTO `go_data_statistics` VALUES ('1867', '4', '百度推广', 'zlbdtg', 'YDSEM00000339', '8707', '1452919444');
INSERT INTO `go_data_statistics` VALUES ('1868', '4', '百度推广', 'zlbdtg', 'SEM00000622', '8708', '1452919757');
INSERT INTO `go_data_statistics` VALUES ('1869', '4', '百度推广', 'zlbdtg', 'SEM00000115', '8709', '1452919907');
INSERT INTO `go_data_statistics` VALUES ('1870', '4', '百度推广', 'zlbdtg', 'SEM00000121', '8710', '1452920042');
INSERT INTO `go_data_statistics` VALUES ('1871', '6', '信号增强器', 'zlxhzqq', '', '8711', '1452920152');
INSERT INTO `go_data_statistics` VALUES ('1872', '4', '百度推广', 'zlbdtg', 'YDSEM00000573', '8712', '1452920159');
INSERT INTO `go_data_statistics` VALUES ('1873', '8', '一元众乐', 'yyzl', '', '8713', '1452920358');
INSERT INTO `go_data_statistics` VALUES ('1874', '8', '一元众乐', 'yyzl', '', '8714', '1452920391');
INSERT INTO `go_data_statistics` VALUES ('1875', '30', '开心看', 'zlkxk', '', '8715', '1452920437');
INSERT INTO `go_data_statistics` VALUES ('1876', '4', '百度推广', 'zlbdtg', 'SEM00000320', '8716', '1452920484');
INSERT INTO `go_data_statistics` VALUES ('1877', '4', '百度推广', 'zlbdtg', 'YDSEM00000339', '8717', '1452920617');
INSERT INTO `go_data_statistics` VALUES ('1878', '4', '百度推广', 'zlbdtg', 'SEM00000115', '8718', '1452920795');
INSERT INTO `go_data_statistics` VALUES ('1879', '30', '开心看', 'zlkxk', '', '8719', '1452920933');
INSERT INTO `go_data_statistics` VALUES ('1880', '4', '百度推广', 'zlbdtg', 'SEM00000601', '8720', '1452921644');
INSERT INTO `go_data_statistics` VALUES ('1881', '30', '开心看', 'zlkxk', '', '8721', '1452921706');
INSERT INTO `go_data_statistics` VALUES ('1882', '8', '一元众乐', 'yyzl', '', '8722', '1452921710');
INSERT INTO `go_data_statistics` VALUES ('1883', '30', '开心看', 'zlkxk', '', '8724', '1452922151');
INSERT INTO `go_data_statistics` VALUES ('1884', '4', '百度推广', 'zlbdtg', 'SEM00000121', '8726', '1452922484');
INSERT INTO `go_data_statistics` VALUES ('1885', '30', '开心看', 'zlkxk', '', '8727', '1452922533');
INSERT INTO `go_data_statistics` VALUES ('1886', '30', '开心看', 'zlkxk', '', '8728', '1452923206');
INSERT INTO `go_data_statistics` VALUES ('1887', '8', '一元众乐', 'yyzl', '', '8729', '1452923272');
INSERT INTO `go_data_statistics` VALUES ('1888', '16', '豌豆荚', 'wandoujia', '', '8730', '1452923460');
INSERT INTO `go_data_statistics` VALUES ('1889', '4', '百度推广', 'zlbdtg', 'SEM00000572', '8731', '1452923583');
INSERT INTO `go_data_statistics` VALUES ('1890', '30', '开心看', 'zlkxk', '', '8732', '1452923597');
INSERT INTO `go_data_statistics` VALUES ('1891', '8', '一元众乐', 'yyzl', '', '8733', '1452923720');
INSERT INTO `go_data_statistics` VALUES ('1892', '8', '一元众乐', 'yyzl', '', '8734', '1452923907');
INSERT INTO `go_data_statistics` VALUES ('1893', '4', '百度推广', 'zlbdtg', 'SEM00000115', '8735', '1452924042');
INSERT INTO `go_data_statistics` VALUES ('1894', '4', '百度推广', 'zlbdtg', 'SEM00000115', '8736', '1452924596');
INSERT INTO `go_data_statistics` VALUES ('1895', '8', '一元众乐', 'yyzl', '', '8737', '1452925029');
INSERT INTO `go_data_statistics` VALUES ('1896', '8', '一元众乐', 'yyzl', '', '8738', '1452925238');
INSERT INTO `go_data_statistics` VALUES ('1897', '14', '360手机助手', 'dev360', '', '8739', '1452925427');
INSERT INTO `go_data_statistics` VALUES ('1898', '30', '开心看', 'zlkxk', '', '8740', '1452925461');
INSERT INTO `go_data_statistics` VALUES ('1899', '4', '百度推广', 'zlbdtg', 'SEM00000115', '8742', '1452925694');
INSERT INTO `go_data_statistics` VALUES ('1900', '30', '开心看', 'zlkxk', '', '8743', '1452925732');
INSERT INTO `go_data_statistics` VALUES ('1901', '8', '一元众乐', 'yyzl', '', '8744', '1452925829');
INSERT INTO `go_data_statistics` VALUES ('1902', '4', '百度推广', 'zlbdtg', 'SEM00000572', '8745', '1452925918');
INSERT INTO `go_data_statistics` VALUES ('1903', '30', '开心看', 'zlkxk', '', '8746', '1452926016');
INSERT INTO `go_data_statistics` VALUES ('1904', '4', '百度推广', 'zlbdtg', 'SEM00000115', '8747', '1452926251');
INSERT INTO `go_data_statistics` VALUES ('1905', '6', '信号增强器', 'zlxhzqq', '', '8748', '1452926459');
INSERT INTO `go_data_statistics` VALUES ('1906', '30', '开心看', 'zlkxk', '', '8749', '1452926460');
INSERT INTO `go_data_statistics` VALUES ('1907', '30', '开心看', 'zlkxk', '', '8750', '1452926470');
INSERT INTO `go_data_statistics` VALUES ('1908', '6', '信号增强器', 'zlxhzqq', '', '8751', '1452926613');
INSERT INTO `go_data_statistics` VALUES ('1909', '30', '开心看', 'zlkxk', '', '8752', '1452926834');
INSERT INTO `go_data_statistics` VALUES ('1910', '4', '百度推广', 'zlbdtg', 'YDSEM00000572', '8753', '1452927442');
INSERT INTO `go_data_statistics` VALUES ('1911', '29', '小米开发者平台', 'xiaomi', '', '8754', '1452927648');
INSERT INTO `go_data_statistics` VALUES ('1912', '30', '开心看', 'zlkxk', '', '8755', '1452927848');
INSERT INTO `go_data_statistics` VALUES ('1913', '4', '百度推广', 'zlbdtg', 'SEM00000115', '8756', '1452928034');
INSERT INTO `go_data_statistics` VALUES ('1914', '8', '一元众乐', 'yyzl', '', '8758', '1452928382');
INSERT INTO `go_data_statistics` VALUES ('1915', '4', '百度推广', 'zlbdtg', 'SEM00000115', '8759', '1452928386');
INSERT INTO `go_data_statistics` VALUES ('1916', '4', '百度推广', 'zlbdtg', 'SEM00000363', '8760', '1452928528');
INSERT INTO `go_data_statistics` VALUES ('1917', '8', '一元众乐', 'yyzl', '', '8761', '1452928902');
INSERT INTO `go_data_statistics` VALUES ('1918', '8', '一元众乐', 'yyzl', '', '8762', '1452929059');
INSERT INTO `go_data_statistics` VALUES ('1919', '4', '百度推广', 'zlbdtg', 'SEM00000122', '8763', '1452929088');
INSERT INTO `go_data_statistics` VALUES ('1920', '4', '百度推广', 'zlbdtg', 'SEM00000639', '8764', '1452929134');
INSERT INTO `go_data_statistics` VALUES ('1921', '4', '百度推广', 'zlbdtg', 'SEM00000573', '8765', '1452929369');
INSERT INTO `go_data_statistics` VALUES ('1922', '4', '百度推广', 'zlbdtg', 'SEM00000531', '8766', '1452929548');
INSERT INTO `go_data_statistics` VALUES ('1923', '30', '开心看', 'zlkxk', '', '8767', '1452929643');
INSERT INTO `go_data_statistics` VALUES ('1924', '4', '百度推广', 'zlbdtg', 'SEM00000117', '8768', '1452929710');
INSERT INTO `go_data_statistics` VALUES ('1925', '30', '开心看', 'zlkxk', '', '8769', '1452929786');
INSERT INTO `go_data_statistics` VALUES ('1926', '6', '信号增强器', 'zlxhzqq', '', '8770', '1452930257');
INSERT INTO `go_data_statistics` VALUES ('1927', '30', '开心看', 'zlkxk', '', '8771', '1452930297');
INSERT INTO `go_data_statistics` VALUES ('1928', '8', '一元众乐', 'yyzl', '', '8772', '1452930713');
INSERT INTO `go_data_statistics` VALUES ('1929', '8', '一元众乐', 'yyzl', '', '8773', '1452930758');
INSERT INTO `go_data_statistics` VALUES ('1930', '6', '信号增强器', 'zlxhzqq', '', '8777', '1452931291');
INSERT INTO `go_data_statistics` VALUES ('1931', '30', '开心看', 'zlkxk', '', '8778', '1452931580');
INSERT INTO `go_data_statistics` VALUES ('1932', '30', '开心看', 'zlkxk', '', '8780', '1452932327');
INSERT INTO `go_data_statistics` VALUES ('1933', '4', '百度推广', 'zlbdtg', 'SEM00000639', '8781', '1452932636');
INSERT INTO `go_data_statistics` VALUES ('1934', '30', '开心看', 'zlkxk', '', '8782', '1452932830');
INSERT INTO `go_data_statistics` VALUES ('1935', '4', '百度推广', 'zlbdtg', 'SEM00000572', '8783', '1452933286');
INSERT INTO `go_data_statistics` VALUES ('1936', '30', '开心看', 'zlkxk', '', '8785', '1452933959');
INSERT INTO `go_data_statistics` VALUES ('1937', '8', '一元众乐', 'yyzl', '', '8786', '1452933987');
INSERT INTO `go_data_statistics` VALUES ('1938', '8', '一元众乐', 'yyzl', '', '8787', '1452934348');
INSERT INTO `go_data_statistics` VALUES ('1939', '8', '一元众乐', 'yyzl', '', '8788', '1452934492');
INSERT INTO `go_data_statistics` VALUES ('1940', '4', '百度推广', 'zlbdtg', 'YDSEM00000606', '8789', '1452934557');
INSERT INTO `go_data_statistics` VALUES ('1941', '16', '豌豆荚', 'wandoujia', '', '8790', '1452934750');
INSERT INTO `go_data_statistics` VALUES ('1942', '30', '开心看', 'zlkxk', '', '8792', '1452935001');
INSERT INTO `go_data_statistics` VALUES ('1943', '8', '一元众乐', 'yyzl', '', '8793', '1452935036');
INSERT INTO `go_data_statistics` VALUES ('1944', '8', '一元众乐', 'yyzl', '', '8794', '1452935625');
INSERT INTO `go_data_statistics` VALUES ('1945', '8', '一元众乐', 'yyzl', '', '8796', '1452936029');
INSERT INTO `go_data_statistics` VALUES ('1946', '2', '儒豹浏览器', 'rbower', '', '8797', '1452936183');
INSERT INTO `go_data_statistics` VALUES ('1947', '4', '百度推广', 'zlbdtg', 'SEM00000122', '8798', '1452936236');
INSERT INTO `go_data_statistics` VALUES ('1948', '8', '一元众乐', 'yyzl', '', '8799', '1452936273');
INSERT INTO `go_data_statistics` VALUES ('1949', '4', '百度推广', 'zlbdtg', 'SEM00000115', '8800', '1452936443');
INSERT INTO `go_data_statistics` VALUES ('1950', '8', '一元众乐', 'yyzl', '', '8801', '1452936509');
INSERT INTO `go_data_statistics` VALUES ('1951', '4', '百度推广', 'zlbdtg', 'SEM00000573', '8802', '1452936703');
INSERT INTO `go_data_statistics` VALUES ('1952', '4', '百度推广', 'zlbdtg', 'SEM00000121', '8803', '1452936944');
INSERT INTO `go_data_statistics` VALUES ('1953', '8', '一元众乐', 'yyzl', '', '8804', '1452937005');
INSERT INTO `go_data_statistics` VALUES ('1954', '19', '木蚂蚁', 'mumayi', '', '8805', '1452937609');
INSERT INTO `go_data_statistics` VALUES ('1955', '8', '一元众乐', 'yyzl', '', '8806', '1452937911');
INSERT INTO `go_data_statistics` VALUES ('1956', '8', '一元众乐', 'yyzl', '', '8807', '1452938034');
INSERT INTO `go_data_statistics` VALUES ('1957', '4', '百度推广', 'zlbdtg', 'SEM00000587', '8808', '1452938078');
INSERT INTO `go_data_statistics` VALUES ('1958', '29', '小米开发者平台', 'xiaomi', '', '8810', '1452938617');
INSERT INTO `go_data_statistics` VALUES ('1959', '8', '一元众乐', 'yyzl', '', '8811', '1452938843');
INSERT INTO `go_data_statistics` VALUES ('1960', '8', '一元众乐', 'yyzl', '', '8812', '1452938961');
INSERT INTO `go_data_statistics` VALUES ('1961', '4', '百度推广', 'zlbdtg', 'SEM00000115', '8814', '1452939053');
INSERT INTO `go_data_statistics` VALUES ('1962', '4', '百度推广', 'zlbdtg', 'SEM00000574', '8815', '1452939069');
INSERT INTO `go_data_statistics` VALUES ('1963', '8', '一元众乐', 'yyzl', '', '8816', '1452939514');
INSERT INTO `go_data_statistics` VALUES ('1964', '4', '百度推广', 'zlbdtg', 'SEM00000115', '8817', '1452939652');
INSERT INTO `go_data_statistics` VALUES ('1965', '4', '百度推广', 'zlbdtg', 'SEM00000115', '8818', '1452939662');
INSERT INTO `go_data_statistics` VALUES ('1966', '4', '百度推广', 'zlbdtg', 'YDSEM00000642', '8819', '1452939742');
INSERT INTO `go_data_statistics` VALUES ('1967', '4', '百度推广', 'zlbdtg', 'SEM00000635', '8820', '1452939946');
INSERT INTO `go_data_statistics` VALUES ('1968', '8', '一元众乐', 'yyzl', '', '8821', '1452940345');
INSERT INTO `go_data_statistics` VALUES ('1969', '30', '开心看', 'zlkxk', '', '8822', '1452940755');
INSERT INTO `go_data_statistics` VALUES ('1970', '4', '百度推广', 'zlbdtg', 'SEM00000115', '8823', '1452940812');
INSERT INTO `go_data_statistics` VALUES ('1971', '8', '一元众乐', 'yyzl', '', '8824', '1452940815');
INSERT INTO `go_data_statistics` VALUES ('1972', '19', '木蚂蚁', 'mumayi', '', '8825', '1452941335');
INSERT INTO `go_data_statistics` VALUES ('1973', '8', '一元众乐', 'yyzl', '', '8826', '1452941439');
INSERT INTO `go_data_statistics` VALUES ('1974', '4', '百度推广', 'zlbdtg', 'SEM00000531', '8827', '1452941588');
INSERT INTO `go_data_statistics` VALUES ('1975', '4', '百度推广', 'zlbdtg', 'YDSEM00000572', '8828', '1452941604');
INSERT INTO `go_data_statistics` VALUES ('1976', '8', '一元众乐', 'yyzl', '', '8829', '1452941724');
INSERT INTO `go_data_statistics` VALUES ('1977', '8', '一元众乐', 'yyzl', '', '8830', '1452941846');
INSERT INTO `go_data_statistics` VALUES ('1978', '4', '百度推广', 'zlbdtg', 'SEM00000371', '8831', '1452941882');
INSERT INTO `go_data_statistics` VALUES ('1979', '30', '开心看', 'zlkxk', '', '8832', '1452941938');
INSERT INTO `go_data_statistics` VALUES ('1980', '8', '一元众乐', 'yyzl', '', '8833', '1452942083');
INSERT INTO `go_data_statistics` VALUES ('1981', '8', '一元众乐', 'yyzl', '', '8836', '1452942273');
INSERT INTO `go_data_statistics` VALUES ('1982', '8', '一元众乐', 'yyzl', '', '8837', '1452942308');
INSERT INTO `go_data_statistics` VALUES ('1983', '4', '百度推广', 'zlbdtg', 'SEM00000607', '8839', '1452942657');
INSERT INTO `go_data_statistics` VALUES ('1984', '8', '一元众乐', 'yyzl', '', '8840', '1452942915');
INSERT INTO `go_data_statistics` VALUES ('1985', '4', '百度推广', 'zlbdtg', 'SEM00000121', '8841', '1452943096');
INSERT INTO `go_data_statistics` VALUES ('1986', '30', '开心看', 'zlkxk', '', '8842', '1452943143');
INSERT INTO `go_data_statistics` VALUES ('1987', '8', '一元众乐', 'yyzl', '', '8843', '1452943186');
INSERT INTO `go_data_statistics` VALUES ('1988', '4', '百度推广', 'zlbdtg', 'YDSEM00000117', '8845', '1452943334');
INSERT INTO `go_data_statistics` VALUES ('1989', '30', '开心看', 'zlkxk', '', '8846', '1452943351');
INSERT INTO `go_data_statistics` VALUES ('1990', '4', '百度推广', 'zlbdtg', 'SEM00000572', '8847', '1452943626');
INSERT INTO `go_data_statistics` VALUES ('1991', '4', '百度推广', 'zlbdtg', 'YDSEM00000122', '8849', '1452944164');
INSERT INTO `go_data_statistics` VALUES ('1992', '4', '百度推广', 'zlbdtg', 'YDSEM00000121', '8850', '1452944289');
INSERT INTO `go_data_statistics` VALUES ('1993', '4', '百度推广', 'zlbdtg', 'YDSEM00000573', '8851', '1452944301');
INSERT INTO `go_data_statistics` VALUES ('1994', '8', '一元众乐', 'yyzl', '', '8852', '1452944362');
INSERT INTO `go_data_statistics` VALUES ('1995', '4', '百度推广', 'zlbdtg', 'YDSEM00000572', '8853', '1452944509');
INSERT INTO `go_data_statistics` VALUES ('1996', '4', '百度推广', 'zlbdtg', 'SEM00000607', '8856', '1452944787');
INSERT INTO `go_data_statistics` VALUES ('1997', '8', '一元众乐', 'yyzl', '', '8857', '1452944828');
INSERT INTO `go_data_statistics` VALUES ('1998', '8', '一元众乐', 'yyzl', '', '8858', '1452944862');
INSERT INTO `go_data_statistics` VALUES ('1999', '4', '百度推广', 'zlbdtg', 'SEM00000115', '8859', '1452944863');
INSERT INTO `go_data_statistics` VALUES ('2000', '30', '开心看', 'zlkxk', '', '8860', '1452944939');
INSERT INTO `go_data_statistics` VALUES ('2001', '8', '一元众乐', 'yyzl', '', '8861', '1452944992');
INSERT INTO `go_data_statistics` VALUES ('2002', '19', '木蚂蚁', 'mumayi', '', '8862', '1452945449');
INSERT INTO `go_data_statistics` VALUES ('2003', '4', '百度推广', 'zlbdtg', 'SEM00000115', '8863', '1452945633');
INSERT INTO `go_data_statistics` VALUES ('2004', '8', '一元众乐', 'yyzl', '', '8864', '1452945849');
INSERT INTO `go_data_statistics` VALUES ('2005', '4', '百度推广', 'zlbdtg', 'YDSEM00000114', '8865', '1452945952');
INSERT INTO `go_data_statistics` VALUES ('2006', '8', '一元众乐', 'yyzl', '', '8866', '1452946026');
INSERT INTO `go_data_statistics` VALUES ('2007', '30', '开心看', 'zlkxk', '', '8867', '1452946260');
INSERT INTO `go_data_statistics` VALUES ('2008', '4', '百度推广', 'zlbdtg', 'YDSEM00000122', '8868', '1452946476');
INSERT INTO `go_data_statistics` VALUES ('2009', '4', '百度推广', 'zlbdtg', 'YDSEM00000116', '8869', '1452946534');
INSERT INTO `go_data_statistics` VALUES ('2010', '30', '开心看', 'zlkxk', '', '8870', '1452946661');
INSERT INTO `go_data_statistics` VALUES ('2011', '6', '信号增强器', 'zlxhzqq', '', '8871', '1452946744');
INSERT INTO `go_data_statistics` VALUES ('2012', '8', '一元众乐', 'yyzl', '', '8873', '1452946868');
INSERT INTO `go_data_statistics` VALUES ('2013', '4', '百度推广', 'zlbdtg', 'YDSEM00000574', '8874', '1452947137');
INSERT INTO `go_data_statistics` VALUES ('2014', '4', '百度推广', 'zlbdtg', 'YDSEM00000573', '8875', '1452947188');
INSERT INTO `go_data_statistics` VALUES ('2015', '4', '百度推广', 'zlbdtg', 'YDSEM00000573', '8876', '1452947200');
INSERT INTO `go_data_statistics` VALUES ('2016', '4', '百度推广', 'zlbdtg', 'SEM00000115', '8877', '1452947419');
INSERT INTO `go_data_statistics` VALUES ('2017', '4', '百度推广', 'zlbdtg', 'YDSEM00000121', '8878', '1452947525');
INSERT INTO `go_data_statistics` VALUES ('2018', '4', '百度推广', 'zlbdtg', 'SEM00000114', '8879', '1452947608');
INSERT INTO `go_data_statistics` VALUES ('2019', '4', '百度推广', 'zlbdtg', 'YDSEM00000574', '8880', '1452947609');
INSERT INTO `go_data_statistics` VALUES ('2020', '4', '百度推广', 'zlbdtg', 'YDSEM00000121', '8881', '1452947857');
INSERT INTO `go_data_statistics` VALUES ('2021', '8', '一元众乐', 'yyzl', '', '8882', '1452947869');
INSERT INTO `go_data_statistics` VALUES ('2022', '8', '一元众乐', 'yyzl', '', '8883', '1452947883');
INSERT INTO `go_data_statistics` VALUES ('2023', '8', '一元众乐', 'yyzl', '', '8884', '1452948181');
INSERT INTO `go_data_statistics` VALUES ('2024', '30', '开心看', 'zlkxk', '', '8886', '1452948202');
INSERT INTO `go_data_statistics` VALUES ('2025', '30', '开心看', 'zlkxk', '', '8887', '1452948540');
INSERT INTO `go_data_statistics` VALUES ('2026', '4', '百度推广', 'zlbdtg', 'SEM00000121', '8888', '1452948542');
INSERT INTO `go_data_statistics` VALUES ('2027', '8', '一元众乐', 'yyzl', '', '8889', '1452948562');
INSERT INTO `go_data_statistics` VALUES ('2028', '30', '开心看', 'zlkxk', '', '8890', '1452948843');
INSERT INTO `go_data_statistics` VALUES ('2029', '8', '一元众乐', 'yyzl', '', '8891', '1452948854');
INSERT INTO `go_data_statistics` VALUES ('2030', '4', '百度推广', 'zlbdtg', 'YDSEM00000339', '8892', '1452948970');
INSERT INTO `go_data_statistics` VALUES ('2031', '8', '一元众乐', 'yyzl', '', '8893', '1452949008');
INSERT INTO `go_data_statistics` VALUES ('2032', '6', '信号增强器', 'zlxhzqq', '', '8894', '1452949198');
INSERT INTO `go_data_statistics` VALUES ('2033', '30', '开心看', 'zlkxk', '', '8895', '1452949567');
INSERT INTO `go_data_statistics` VALUES ('2034', '8', '一元众乐', 'yyzl', '', '8896', '1452949743');
INSERT INTO `go_data_statistics` VALUES ('2035', '4', '百度推广', 'zlbdtg', 'YDSEM00000117', '8898', '1452950133');
INSERT INTO `go_data_statistics` VALUES ('2036', '4', '百度推广', 'zlbdtg', 'YDSEM00000163', '8899', '1452950156');
INSERT INTO `go_data_statistics` VALUES ('2037', '4', '百度推广', 'zlbdtg', 'SEM00000579', '8900', '1452950211');
INSERT INTO `go_data_statistics` VALUES ('2038', '30', '开心看', 'zlkxk', '', '8901', '1452950401');
INSERT INTO `go_data_statistics` VALUES ('2039', '24', '魅族', 'meizu', '', '8902', '1452950584');
INSERT INTO `go_data_statistics` VALUES ('2040', '4', '百度推广', 'zlbdtg', 'SEM00000587', '8903', '1452950670');
INSERT INTO `go_data_statistics` VALUES ('2041', '30', '开心看', 'zlkxk', '', '8904', '1452950699');
INSERT INTO `go_data_statistics` VALUES ('2042', '30', '开心看', 'zlkxk', '', '8905', '1452950971');
INSERT INTO `go_data_statistics` VALUES ('2043', '4', '百度推广', 'zlbdtg', 'SEM00000115', '8906', '1452951117');
INSERT INTO `go_data_statistics` VALUES ('2044', '4', '百度推广', 'zlbdtg', 'YDSEM00000142', '8907', '1452951380');
INSERT INTO `go_data_statistics` VALUES ('2045', '4', '百度推广', 'zlbdtg', 'YDSEM00000115', '8909', '1452951440');
INSERT INTO `go_data_statistics` VALUES ('2046', '8', '一元众乐', 'yyzl', '', '8910', '1452951814');
INSERT INTO `go_data_statistics` VALUES ('2047', '4', '百度推广', 'zlbdtg', 'YDSEM00000142', '8911', '1452951832');
INSERT INTO `go_data_statistics` VALUES ('2048', '4', '百度推广', 'zlbdtg', 'YDSEM00000339', '8914', '1452952288');
INSERT INTO `go_data_statistics` VALUES ('2049', '8', '一元众乐', 'yyzl', '', '8915', '1452952374');
INSERT INTO `go_data_statistics` VALUES ('2050', '8', '一元众乐', 'yyzl', '', '8916', '1452952545');
INSERT INTO `go_data_statistics` VALUES ('2051', '4', '百度推广', 'zlbdtg', 'SEM00000572', '8917', '1452952577');
INSERT INTO `go_data_statistics` VALUES ('2052', '4', '百度推广', 'zlbdtg', 'YDSEM00000121', '8918', '1452952731');
INSERT INTO `go_data_statistics` VALUES ('2053', '4', '百度推广', 'zlbdtg', 'YDSEM00000116', '8919', '1452952850');
INSERT INTO `go_data_statistics` VALUES ('2054', '4', '百度推广', 'zlbdtg', 'YDSEM00000578', '8920', '1452952964');
INSERT INTO `go_data_statistics` VALUES ('2055', '30', '开心看', 'zlkxk', '', '8921', '1452953048');
INSERT INTO `go_data_statistics` VALUES ('2056', '6', '信号增强器', 'zlxhzqq', '', '8922', '1452953105');
INSERT INTO `go_data_statistics` VALUES ('2057', '30', '开心看', 'zlkxk', '', '8923', '1452953255');
INSERT INTO `go_data_statistics` VALUES ('2058', '8', '一元众乐', 'yyzl', '', '8924', '1452953257');
INSERT INTO `go_data_statistics` VALUES ('2059', '30', '开心看', 'zlkxk', '', '8925', '1452953286');
INSERT INTO `go_data_statistics` VALUES ('2060', '4', '百度推广', 'zlbdtg', 'YDSEM00000607', '8926', '1452953336');
INSERT INTO `go_data_statistics` VALUES ('2061', '4', '百度推广', 'zlbdtg', 'SEM00000122', '8927', '1452953438');
INSERT INTO `go_data_statistics` VALUES ('2062', '6', '信号增强器', 'zlxhzqq', '', '8928', '1452953495');
INSERT INTO `go_data_statistics` VALUES ('2063', '30', '开心看', 'zlkxk', '', '8929', '1452953616');
INSERT INTO `go_data_statistics` VALUES ('2064', '30', '开心看', 'zlkxk', '', '8930', '1452953657');
INSERT INTO `go_data_statistics` VALUES ('2065', '30', '开心看', 'zlkxk', '', '8932', '1452953779');
INSERT INTO `go_data_statistics` VALUES ('2066', '4', '百度推广', 'zlbdtg', 'SEM00000619', '8933', '1452953816');
INSERT INTO `go_data_statistics` VALUES ('2067', '4', '百度推广', 'zlbdtg', 'YDSEM00000180', '8934', '1452953909');
INSERT INTO `go_data_statistics` VALUES ('2068', '4', '百度推广', 'zlbdtg', 'YDSEM00000339', '8935', '1452953991');
INSERT INTO `go_data_statistics` VALUES ('2069', '8', '一元众乐', 'yyzl', '', '8936', '1452954012');
INSERT INTO `go_data_statistics` VALUES ('2070', '30', '开心看', 'zlkxk', '', '8937', '1452954200');
INSERT INTO `go_data_statistics` VALUES ('2071', '8', '一元众乐', 'yyzl', '', '8938', '1452954474');
INSERT INTO `go_data_statistics` VALUES ('2072', '4', '百度推广', 'zlbdtg', 'YDSEM00000628', '8939', '1452954489');
INSERT INTO `go_data_statistics` VALUES ('2073', '30', '开心看', 'zlkxk', '', '8940', '1452954520');
INSERT INTO `go_data_statistics` VALUES ('2074', '4', '百度推广', 'zlbdtg', 'YDSEM00000135', '8941', '1452954662');
INSERT INTO `go_data_statistics` VALUES ('2075', '8', '一元众乐', 'yyzl', '', '8942', '1452954828');
INSERT INTO `go_data_statistics` VALUES ('2076', '30', '开心看', 'zlkxk', '', '8943', '1452954852');
INSERT INTO `go_data_statistics` VALUES ('2077', '4', '百度推广', 'zlbdtg', 'YDSEM00000115', '8944', '1452954938');
INSERT INTO `go_data_statistics` VALUES ('2078', '4', '百度推广', 'zlbdtg', 'YDSEM00000573', '8945', '1452955198');
INSERT INTO `go_data_statistics` VALUES ('2079', '8', '一元众乐', 'yyzl', '', '8946', '1452955287');
INSERT INTO `go_data_statistics` VALUES ('2080', '4', '百度推广', 'zlbdtg', 'YDSEM00000639', '8947', '1452955313');
INSERT INTO `go_data_statistics` VALUES ('2081', '30', '开心看', 'zlkxk', '', '8948', '1452955335');
INSERT INTO `go_data_statistics` VALUES ('2082', '4', '百度推广', 'zlbdtg', 'SEM00000628', '8949', '1452955378');
INSERT INTO `go_data_statistics` VALUES ('2083', '4', '百度推广', 'zlbdtg', 'YDSEM00000516', '8950', '1452955442');
INSERT INTO `go_data_statistics` VALUES ('2084', '30', '开心看', 'zlkxk', '', '8951', '1452955517');
INSERT INTO `go_data_statistics` VALUES ('2085', '8', '一元众乐', 'yyzl', '', '8952', '1452955570');
INSERT INTO `go_data_statistics` VALUES ('2086', '30', '开心看', 'zlkxk', '', '8953', '1452955711');
INSERT INTO `go_data_statistics` VALUES ('2087', '8', '一元众乐', 'yyzl', '', '8954', '1452955930');
INSERT INTO `go_data_statistics` VALUES ('2088', '8', '一元众乐', 'yyzl', '', '8955', '1452955942');
INSERT INTO `go_data_statistics` VALUES ('2089', '4', '百度推广', 'zlbdtg', 'SEM00000119', '8956', '1452955981');
INSERT INTO `go_data_statistics` VALUES ('2090', '4', '百度推广', 'zlbdtg', 'SEM00000639', '8957', '1452956241');
INSERT INTO `go_data_statistics` VALUES ('2091', '8', '一元众乐', 'yyzl', '', '8959', '1452956342');
INSERT INTO `go_data_statistics` VALUES ('2092', '8', '一元众乐', 'yyzl', '', '8960', '1452956462');
INSERT INTO `go_data_statistics` VALUES ('2093', '4', '百度推广', 'zlbdtg', 'YDSEM00000121', '8961', '1452956464');
INSERT INTO `go_data_statistics` VALUES ('2094', '4', '百度推广', 'zlbdtg', 'SEM00000117', '8962', '1452956678');
INSERT INTO `go_data_statistics` VALUES ('2095', '4', '百度推广', 'zlbdtg', 'SEM00000117', '8963', '1452956687');
INSERT INTO `go_data_statistics` VALUES ('2096', '4', '百度推广', 'zlbdtg', 'SEM00000122', '8964', '1452956854');
INSERT INTO `go_data_statistics` VALUES ('2097', '8', '一元众乐', 'yyzl', '', '8965', '1452957210');
INSERT INTO `go_data_statistics` VALUES ('2098', '4', '百度推广', 'zlbdtg', 'YDSEM00000628', '8966', '1452957299');
INSERT INTO `go_data_statistics` VALUES ('2099', '4', '百度推广', 'zlbdtg', 'YDSEM00000573', '8967', '1452957449');
INSERT INTO `go_data_statistics` VALUES ('2100', '8', '一元众乐', 'yyzl', '', '8968', '1452957553');
INSERT INTO `go_data_statistics` VALUES ('2101', '4', '百度推广', 'zlbdtg', 'YDSEM00000115', '8969', '1452957555');
INSERT INTO `go_data_statistics` VALUES ('2102', '30', '开心看', 'zlkxk', '', '8970', '1452957636');
INSERT INTO `go_data_statistics` VALUES ('2103', '30', '开心看', 'zlkxk', '', '8971', '1452957690');
INSERT INTO `go_data_statistics` VALUES ('2104', '19', '木蚂蚁', 'mumayi', '', '8972', '1452957701');
INSERT INTO `go_data_statistics` VALUES ('2105', '8', '一元众乐', 'yyzl', '', '8973', '1452957827');
INSERT INTO `go_data_statistics` VALUES ('2106', '17', 'PP助手', 'pp', '', '8974', '1452957852');
INSERT INTO `go_data_statistics` VALUES ('2107', '4', '百度推广', 'zlbdtg', 'YDSEM00000180', '8975', '1452957878');
INSERT INTO `go_data_statistics` VALUES ('2108', '6', '信号增强器', 'zlxhzqq', '', '8977', '1452958005');
INSERT INTO `go_data_statistics` VALUES ('2109', '6', '信号增强器', 'zlxhzqq', '', '8978', '1452958120');
INSERT INTO `go_data_statistics` VALUES ('2110', '8', '一元众乐', 'yyzl', '', '8979', '1452958139');
INSERT INTO `go_data_statistics` VALUES ('2111', '4', '百度推广', 'zlbdtg', 'SEM00000371', '8980', '1452958178');
INSERT INTO `go_data_statistics` VALUES ('2112', '4', '百度推广', 'zlbdtg', 'YDSEM00000339', '8981', '1452958269');
INSERT INTO `go_data_statistics` VALUES ('2113', '4', '百度推广', 'zlbdtg', 'SEM00000573', '8982', '1452958337');
INSERT INTO `go_data_statistics` VALUES ('2114', '30', '开心看', 'zlkxk', '', '8983', '1452958339');
INSERT INTO `go_data_statistics` VALUES ('2115', '8', '一元众乐', 'yyzl', '', '8985', '1452959196');
INSERT INTO `go_data_statistics` VALUES ('2116', '30', '开心看', 'zlkxk', '', '8986', '1452959440');
INSERT INTO `go_data_statistics` VALUES ('2117', '6', '信号增强器', 'zlxhzqq', '', '8987', '1452959945');
INSERT INTO `go_data_statistics` VALUES ('2118', '8', '一元众乐', 'yyzl', '', '8989', '1452960393');
INSERT INTO `go_data_statistics` VALUES ('2119', '8', '一元众乐', 'yyzl', '', '8990', '1452960642');
INSERT INTO `go_data_statistics` VALUES ('2120', '4', '百度推广', 'zlbdtg', 'YDSEM00000115', '8991', '1452961164');
INSERT INTO `go_data_statistics` VALUES ('2121', '30', '开心看', 'zlkxk', '', '8992', '1452961530');
INSERT INTO `go_data_statistics` VALUES ('2122', '6', '信号增强器', 'zlxhzqq', '', '8993', '1452961783');
INSERT INTO `go_data_statistics` VALUES ('2123', '8', '一元众乐', 'yyzl', '', '8994', '1452961951');
INSERT INTO `go_data_statistics` VALUES ('2124', '4', '百度推广', 'zlbdtg', 'SEM00000635', '8995', '1452962423');
INSERT INTO `go_data_statistics` VALUES ('2125', '8', '一元众乐', 'yyzl', '', '8996', '1452962664');
INSERT INTO `go_data_statistics` VALUES ('2126', '4', '百度推广', 'zlbdtg', 'SEM00000115', '8997', '1452962826');
INSERT INTO `go_data_statistics` VALUES ('2127', '4', '百度推广', 'zlbdtg', 'SEM00000115', '8998', '1452963748');
INSERT INTO `go_data_statistics` VALUES ('2128', '4', '百度推广', 'zlbdtg', 'YDSEM00000573', '9000', '1452964002');
INSERT INTO `go_data_statistics` VALUES ('2129', '4', '百度推广', 'zlbdtg', 'YDSEM00000573', '9001', '1452964746');
INSERT INTO `go_data_statistics` VALUES ('2130', '30', '开心看', 'zlkxk', '', '9002', '1452965463');
INSERT INTO `go_data_statistics` VALUES ('2131', '4', '百度推广', 'zlbdtg', 'SEM00000339', '9003', '1452965874');
INSERT INTO `go_data_statistics` VALUES ('2132', '8', '一元众乐', 'yyzl', '', '9005', '1452966843');
INSERT INTO `go_data_statistics` VALUES ('2133', '4', '百度推广', 'zlbdtg', 'YDSEM00000117', '9006', '1452967295');
INSERT INTO `go_data_statistics` VALUES ('2134', '8', '一元众乐', 'yyzl', '', '9007', '1452968006');
INSERT INTO `go_data_statistics` VALUES ('2135', '30', '开心看', 'zlkxk', '', '9008', '1452968156');
INSERT INTO `go_data_statistics` VALUES ('2136', '30', '开心看', 'zlkxk', '', '9009', '1452968245');
INSERT INTO `go_data_statistics` VALUES ('2137', '8', '一元众乐', 'yyzl', '', '9010', '1452968521');
INSERT INTO `go_data_statistics` VALUES ('2138', '4', '百度推广', 'zlbdtg', 'YDSEM00000115', '9011', '1452968597');
INSERT INTO `go_data_statistics` VALUES ('2139', '4', '百度推广', 'zlbdtg', 'SEM00000115', '9012', '1452969820');
INSERT INTO `go_data_statistics` VALUES ('2140', '4', '百度推广', 'zlbdtg', 'YDSEM00000122', '9013', '1452972791');
INSERT INTO `go_data_statistics` VALUES ('2141', '6', '信号增强器', 'zlxhzqq', '', '9014', '1452973138');
INSERT INTO `go_data_statistics` VALUES ('2142', '4', '百度推广', 'zlbdtg', 'SEM00000115', '9015', '1452976401');
INSERT INTO `go_data_statistics` VALUES ('2143', '8', '一元众乐', 'yyzl', '', '9016', '1452976941');
INSERT INTO `go_data_statistics` VALUES ('2144', '4', '百度推广', 'zlbdtg', 'SEM00000121', '9017', '1452976973');
INSERT INTO `go_data_statistics` VALUES ('2145', '4', '百度推广', 'zlbdtg', 'SEM00000523', '9018', '1452977135');
INSERT INTO `go_data_statistics` VALUES ('2146', '4', '百度推广', 'zlbdtg', 'YDSEM00000115', '9019', '1452978310');
INSERT INTO `go_data_statistics` VALUES ('2147', '30', '开心看', 'zlkxk', '', '9020', '1452980776');
INSERT INTO `go_data_statistics` VALUES ('2148', '6', '信号增强器', 'zlxhzqq', '', '9021', '1452981074');
INSERT INTO `go_data_statistics` VALUES ('2149', '4', '百度推广', 'zlbdtg', 'SEM00000122', '9022', '1452981865');
INSERT INTO `go_data_statistics` VALUES ('2150', '4', '百度推广', 'zlbdtg', 'YDSEM00000339', '9023', '1452982100');
INSERT INTO `go_data_statistics` VALUES ('2151', '30', '开心看', 'zlkxk', '', '9024', '1452985730');
INSERT INTO `go_data_statistics` VALUES ('2152', '4', '百度推广', 'zlbdtg', 'SEM00000572', '9025', '1452986517');
INSERT INTO `go_data_statistics` VALUES ('2153', '4', '百度推广', 'zlbdtg', 'SEM00000115', '9026', '1452987637');
INSERT INTO `go_data_statistics` VALUES ('2154', '6', '信号增强器', 'zlxhzqq', '', '9027', '1452987980');
INSERT INTO `go_data_statistics` VALUES ('2155', '30', '开心看', 'zlkxk', '', '9028', '1452988158');
INSERT INTO `go_data_statistics` VALUES ('2156', '4', '百度推广', 'zlbdtg', 'SEM00000122', '9030', '1452988688');
INSERT INTO `go_data_statistics` VALUES ('2157', '6', '信号增强器', 'zlxhzqq', '', '9031', '1452988771');
INSERT INTO `go_data_statistics` VALUES ('2158', '8', '一元众乐', 'yyzl', '', '9033', '1452990103');
INSERT INTO `go_data_statistics` VALUES ('2159', '4', '百度推广', 'zlbdtg', 'SEM00000122', '9034', '1452990382');
INSERT INTO `go_data_statistics` VALUES ('2160', '4', '百度推广', 'zlbdtg', 'YDSEM00000573', '9035', '1452990610');
INSERT INTO `go_data_statistics` VALUES ('2161', '8', '一元众乐', 'yyzl', '', '9036', '1452990864');
INSERT INTO `go_data_statistics` VALUES ('2162', '8', '一元众乐', 'yyzl', '', '9037', '1452990909');
INSERT INTO `go_data_statistics` VALUES ('2163', '8', '一元众乐', 'yyzl', '', '9038', '1452991210');
INSERT INTO `go_data_statistics` VALUES ('2164', '8', '一元众乐', 'yyzl', '', '9039', '1452991353');
INSERT INTO `go_data_statistics` VALUES ('2165', '30', '开心看', 'zlkxk', '', '9040', '1452991540');
INSERT INTO `go_data_statistics` VALUES ('2166', '8', '一元众乐', 'yyzl', '', '9041', '1452991720');
INSERT INTO `go_data_statistics` VALUES ('2167', '30', '开心看', 'zlkxk', '', '9042', '1452991765');
INSERT INTO `go_data_statistics` VALUES ('2168', '6', '信号增强器', 'zlxhzqq', '', '9043', '1452992669');
INSERT INTO `go_data_statistics` VALUES ('2169', '8', '一元众乐', 'yyzl', '', '9044', '1452993196');
INSERT INTO `go_data_statistics` VALUES ('2170', '30', '开心看', 'zlkxk', '', '9045', '1452993543');
INSERT INTO `go_data_statistics` VALUES ('2171', '6', '信号增强器', 'zlxhzqq', '', '9046', '1452993547');
INSERT INTO `go_data_statistics` VALUES ('2172', '19', '木蚂蚁', 'mumayi', '', '9047', '1452993733');
INSERT INTO `go_data_statistics` VALUES ('2173', '8', '一元众乐', 'yyzl', '', '9048', '1452994131');
INSERT INTO `go_data_statistics` VALUES ('2174', '8', '一元众乐', 'yyzl', '', '9049', '1452994391');
INSERT INTO `go_data_statistics` VALUES ('2175', '4', '百度推广', 'zlbdtg', 'YDSEM00000573', '9050', '1452994394');
INSERT INTO `go_data_statistics` VALUES ('2176', '4', '百度推广', 'zlbdtg', 'SEM00000163', '9051', '1452994402');
INSERT INTO `go_data_statistics` VALUES ('2177', '6', '信号增强器', 'zlxhzqq', '', '9052', '1452994873');
INSERT INTO `go_data_statistics` VALUES ('2178', '30', '开心看', 'zlkxk', '', '9053', '1452995080');
INSERT INTO `go_data_statistics` VALUES ('2179', '30', '开心看', 'zlkxk', '', '9054', '1452995212');
INSERT INTO `go_data_statistics` VALUES ('2180', '4', '百度推广', 'zlbdtg', 'SEM00000622', '9055', '1452995527');
INSERT INTO `go_data_statistics` VALUES ('2181', '8', '一元众乐', 'yyzl', '', '9056', '1452995599');
INSERT INTO `go_data_statistics` VALUES ('2182', '4', '百度推广', 'zlbdtg', 'SEM00000121', '9057', '1452995632');
INSERT INTO `go_data_statistics` VALUES ('2183', '8', '一元众乐', 'yyzl', '', '9058', '1452995975');
INSERT INTO `go_data_statistics` VALUES ('2184', '8', '一元众乐', 'yyzl', '', '9060', '1452996469');
INSERT INTO `go_data_statistics` VALUES ('2185', '4', '百度推广', 'zlbdtg', 'SEM00000339', '9061', '1452996631');
INSERT INTO `go_data_statistics` VALUES ('2186', '4', '百度推广', 'zlbdtg', 'SEM00000635', '9062', '1452996939');
INSERT INTO `go_data_statistics` VALUES ('2187', '8', '一元众乐', 'yyzl', '', '9063', '1452997102');
INSERT INTO `go_data_statistics` VALUES ('2188', '4', '百度推广', 'zlbdtg', 'SEM00000622', '9064', '1452997281');
INSERT INTO `go_data_statistics` VALUES ('2189', '4', '百度推广', 'zlbdtg', 'YDSEM00000573', '9065', '1452997388');
INSERT INTO `go_data_statistics` VALUES ('2190', '4', '百度推广', 'zlbdtg', 'SEM00000115', '9067', '1452998118');
INSERT INTO `go_data_statistics` VALUES ('2191', '30', '开心看', 'zlkxk', '', '9068', '1452998308');
INSERT INTO `go_data_statistics` VALUES ('2192', '6', '信号增强器', 'zlxhzqq', '', '9069', '1452998735');
INSERT INTO `go_data_statistics` VALUES ('2193', '4', '百度推广', 'zlbdtg', 'YDSEM00000117', '9070', '1452999109');
INSERT INTO `go_data_statistics` VALUES ('2194', '4', '百度推广', 'zlbdtg', 'SEM00000635', '9071', '1452999340');
INSERT INTO `go_data_statistics` VALUES ('2195', '6', '信号增强器', 'zlxhzqq', '', '9072', '1452999377');
INSERT INTO `go_data_statistics` VALUES ('2196', '6', '信号增强器', 'zlxhzqq', '', '9073', '1452999562');
INSERT INTO `go_data_statistics` VALUES ('2197', '30', '开心看', 'zlkxk', '', '9074', '1452999703');
INSERT INTO `go_data_statistics` VALUES ('2198', '6', '信号增强器', 'zlxhzqq', '', '9075', '1452999825');
INSERT INTO `go_data_statistics` VALUES ('2199', '19', '木蚂蚁', 'mumayi', '', '9076', '1452999890');
INSERT INTO `go_data_statistics` VALUES ('2200', '8', '一元众乐', 'yyzl', '', '9077', '1453000190');
INSERT INTO `go_data_statistics` VALUES ('2201', '4', '百度推广', 'zlbdtg', 'YDSEM00000339', '9078', '1453001169');
INSERT INTO `go_data_statistics` VALUES ('2202', '4', '百度推广', 'zlbdtg', 'SEM00000115', '9079', '1453001179');
INSERT INTO `go_data_statistics` VALUES ('2203', '8', '一元众乐', 'yyzl', '', '9080', '1453001202');
INSERT INTO `go_data_statistics` VALUES ('2204', '30', '开心看', 'zlkxk', '', '9081', '1453001558');
INSERT INTO `go_data_statistics` VALUES ('2205', '8', '一元众乐', 'yyzl', '', '9082', '1453001765');
INSERT INTO `go_data_statistics` VALUES ('2206', '30', '开心看', 'zlkxk', '', '9083', '1453001936');
INSERT INTO `go_data_statistics` VALUES ('2207', '30', '开心看', 'zlkxk', '', '9085', '1453002403');
INSERT INTO `go_data_statistics` VALUES ('2208', '6', '信号增强器', 'zlxhzqq', '', '9086', '1453002404');
INSERT INTO `go_data_statistics` VALUES ('2209', '8', '一元众乐', 'yyzl', '', '9088', '1453002801');
INSERT INTO `go_data_statistics` VALUES ('2210', '30', '开心看', 'zlkxk', '', '9089', '1453002801');
INSERT INTO `go_data_statistics` VALUES ('2211', '30', '开心看', 'zlkxk', '', '9090', '1453002958');
INSERT INTO `go_data_statistics` VALUES ('2212', '4', '百度推广', 'zlbdtg', 'YDSEM00000635', '9091', '1453003089');
INSERT INTO `go_data_statistics` VALUES ('2213', '4', '百度推广', 'zlbdtg', 'SEM00000115', '9092', '1453003135');
INSERT INTO `go_data_statistics` VALUES ('2214', '8', '一元众乐', 'yyzl', '', '9093', '1453003177');
INSERT INTO `go_data_statistics` VALUES ('2215', '4', '百度推广', 'zlbdtg', 'SEM00000121', '9094', '1453003371');
INSERT INTO `go_data_statistics` VALUES ('2216', '8', '一元众乐', 'yyzl', '', '9095', '1453003483');
INSERT INTO `go_data_statistics` VALUES ('2217', '4', '百度推广', 'zlbdtg', 'YDSEM00000339', '9096', '1453003632');
INSERT INTO `go_data_statistics` VALUES ('2218', '4', '百度推广', 'zlbdtg', 'YDSEM00000628', '9097', '1453003707');
INSERT INTO `go_data_statistics` VALUES ('2219', '30', '开心看', 'zlkxk', '', '9098', '1453003816');
INSERT INTO `go_data_statistics` VALUES ('2220', '8', '一元众乐', 'yyzl', '', '9099', '1453003949');
INSERT INTO `go_data_statistics` VALUES ('2221', '8', '一元众乐', 'yyzl', '', '9100', '1453003951');
INSERT INTO `go_data_statistics` VALUES ('2222', '30', '开心看', 'zlkxk', '', '9101', '1453004208');
INSERT INTO `go_data_statistics` VALUES ('2223', '30', '开心看', 'zlkxk', '', '9102', '1453004423');
INSERT INTO `go_data_statistics` VALUES ('2224', '8', '一元众乐', 'yyzl', '', '9103', '1453004431');
INSERT INTO `go_data_statistics` VALUES ('2225', '30', '开心看', 'zlkxk', '', '9104', '1453004444');
INSERT INTO `go_data_statistics` VALUES ('2226', '30', '开心看', 'zlkxk', '', '9105', '1453004445');
INSERT INTO `go_data_statistics` VALUES ('2227', '4', '百度推广', 'zlbdtg', 'SEM00000163', '9106', '1453004509');
INSERT INTO `go_data_statistics` VALUES ('2228', '30', '开心看', 'zlkxk', '', '9107', '1453004554');
INSERT INTO `go_data_statistics` VALUES ('2229', '30', '开心看', 'zlkxk', '', '9108', '1453004577');
INSERT INTO `go_data_statistics` VALUES ('2230', '8', '一元众乐', 'yyzl', '', '9109', '1453004772');
INSERT INTO `go_data_statistics` VALUES ('2231', '30', '开心看', 'zlkxk', '', '9110', '1453004823');
INSERT INTO `go_data_statistics` VALUES ('2232', '4', '百度推广', 'zlbdtg', 'YDSEM00000573', '9111', '1453004823');
INSERT INTO `go_data_statistics` VALUES ('2233', '30', '开心看', 'zlkxk', '', '9112', '1453004880');
INSERT INTO `go_data_statistics` VALUES ('2234', '8', '一元众乐', 'yyzl', '', '9113', '1453004885');
INSERT INTO `go_data_statistics` VALUES ('2235', '8', '一元众乐', 'yyzl', '', '9114', '1453004897');
INSERT INTO `go_data_statistics` VALUES ('2236', '30', '开心看', 'zlkxk', '', '9115', '1453004929');
INSERT INTO `go_data_statistics` VALUES ('2237', '4', '百度推广', 'zlbdtg', 'SEM00000573', '9116', '1453005003');
INSERT INTO `go_data_statistics` VALUES ('2238', '30', '开心看', 'zlkxk', '', '9117', '1453005036');
INSERT INTO `go_data_statistics` VALUES ('2239', '30', '开心看', 'zlkxk', '', '9118', '1453005271');
INSERT INTO `go_data_statistics` VALUES ('2240', '8', '一元众乐', 'yyzl', '', '9119', '1453005336');
INSERT INTO `go_data_statistics` VALUES ('2241', '8', '一元众乐', 'yyzl', '', '9120', '1453005352');
INSERT INTO `go_data_statistics` VALUES ('2242', '30', '开心看', 'zlkxk', '', '9121', '1453005481');
INSERT INTO `go_data_statistics` VALUES ('2243', '4', '百度推广', 'zlbdtg', 'YDSEM00000163', '9122', '1453005814');
INSERT INTO `go_data_statistics` VALUES ('2244', '8', '一元众乐', 'yyzl', '', '9123', '1453005908');
INSERT INTO `go_data_statistics` VALUES ('2245', '30', '开心看', 'zlkxk', '', '9124', '1453006039');
INSERT INTO `go_data_statistics` VALUES ('2246', '30', '开心看', 'zlkxk', '', '9126', '1453006067');
INSERT INTO `go_data_statistics` VALUES ('2247', '4', '百度推广', 'zlbdtg', 'SEM00000622', '9127', '1453006074');
INSERT INTO `go_data_statistics` VALUES ('2248', '8', '一元众乐', 'yyzl', '', '9129', '1453006222');
INSERT INTO `go_data_statistics` VALUES ('2249', '8', '一元众乐', 'yyzl', '', '9130', '1453006409');
INSERT INTO `go_data_statistics` VALUES ('2250', '8', '一元众乐', 'yyzl', '', '9131', '1453006476');
INSERT INTO `go_data_statistics` VALUES ('2251', '8', '一元众乐', 'yyzl', '', '9132', '1453006523');
INSERT INTO `go_data_statistics` VALUES ('2252', '8', '一元众乐', 'yyzl', '', '9133', '1453006790');
INSERT INTO `go_data_statistics` VALUES ('2253', '4', '百度推广', 'zlbdtg', 'YDSEM00000573', '9134', '1453006800');
INSERT INTO `go_data_statistics` VALUES ('2254', '8', '一元众乐', 'yyzl', '', '9135', '1453006918');
INSERT INTO `go_data_statistics` VALUES ('2255', '30', '开心看', 'zlkxk', '', '9136', '1453007348');
INSERT INTO `go_data_statistics` VALUES ('2256', '30', '开心看', 'zlkxk', '', '9137', '1453007404');
INSERT INTO `go_data_statistics` VALUES ('2257', '4', '百度推广', 'zlbdtg', 'YDSEM00000115', '9138', '1453007569');
INSERT INTO `go_data_statistics` VALUES ('2258', '30', '开心看', 'zlkxk', '', '9139', '1453007691');
INSERT INTO `go_data_statistics` VALUES ('2259', '8', '一元众乐', 'yyzl', '', '9140', '1453007697');
INSERT INTO `go_data_statistics` VALUES ('2260', '8', '一元众乐', 'yyzl', '', '9141', '1453007785');
INSERT INTO `go_data_statistics` VALUES ('2261', '19', '木蚂蚁', 'mumayi', '', '9142', '1453007800');
INSERT INTO `go_data_statistics` VALUES ('2262', '6', '信号增强器', 'zlxhzqq', '', '9143', '1453007810');
INSERT INTO `go_data_statistics` VALUES ('2263', '30', '开心看', 'zlkxk', '', '9144', '1453007969');
INSERT INTO `go_data_statistics` VALUES ('2264', '30', '开心看', 'zlkxk', '', '9145', '1453008014');
INSERT INTO `go_data_statistics` VALUES ('2265', '30', '开心看', 'zlkxk', '', '9146', '1453008144');
INSERT INTO `go_data_statistics` VALUES ('2266', '4', '百度推广', 'zlbdtg', 'SEM00000115', '9147', '1453008610');
INSERT INTO `go_data_statistics` VALUES ('2267', '30', '开心看', 'zlkxk', '', '9148', '1453008616');
INSERT INTO `go_data_statistics` VALUES ('2268', '30', '开心看', 'zlkxk', '', '9150', '1453008675');
INSERT INTO `go_data_statistics` VALUES ('2269', '4', '百度推广', 'zlbdtg', 'SEM00000115', '9151', '1453008730');
INSERT INTO `go_data_statistics` VALUES ('2270', '8', '一元众乐', 'yyzl', '', '9152', '1453008796');
INSERT INTO `go_data_statistics` VALUES ('2271', '30', '开心看', 'zlkxk', '', '9153', '1453008929');
INSERT INTO `go_data_statistics` VALUES ('2272', '30', '开心看', 'zlkxk', '', '9154', '1453009336');
INSERT INTO `go_data_statistics` VALUES ('2273', '4', '百度推广', 'zlbdtg', 'YDSEM00000573', '9155', '1453009343');
INSERT INTO `go_data_statistics` VALUES ('2274', '30', '开心看', 'zlkxk', '', '9156', '1453009897');
INSERT INTO `go_data_statistics` VALUES ('2275', '30', '开心看', 'zlkxk', '', '9157', '1453010199');
INSERT INTO `go_data_statistics` VALUES ('2276', '8', '一元众乐', 'yyzl', '', '9159', '1453010505');
INSERT INTO `go_data_statistics` VALUES ('2277', '30', '开心看', 'zlkxk', '', '9160', '1453010517');
INSERT INTO `go_data_statistics` VALUES ('2278', '6', '信号增强器', 'zlxhzqq', '', '9161', '1453010660');
INSERT INTO `go_data_statistics` VALUES ('2279', '30', '开心看', 'zlkxk', '', '9162', '1453010792');
INSERT INTO `go_data_statistics` VALUES ('2280', '30', '开心看', 'zlkxk', '', '9163', '1453011010');
INSERT INTO `go_data_statistics` VALUES ('2281', '8', '一元众乐', 'yyzl', '', '9164', '1453011022');
INSERT INTO `go_data_statistics` VALUES ('2282', '4', '百度推广', 'zlbdtg', 'SEM00000622', '9165', '1453011092');
INSERT INTO `go_data_statistics` VALUES ('2283', '30', '开心看', 'zlkxk', '', '9166', '1453011101');
INSERT INTO `go_data_statistics` VALUES ('2284', '4', '百度推广', 'zlbdtg', 'SEM00000572', '9167', '1453011330');
INSERT INTO `go_data_statistics` VALUES ('2285', '8', '一元众乐', 'yyzl', '', '9168', '1453011343');
INSERT INTO `go_data_statistics` VALUES ('2286', '6', '信号增强器', 'zlxhzqq', '', '9170', '1453011514');
INSERT INTO `go_data_statistics` VALUES ('2287', '8', '一元众乐', 'yyzl', '', '9171', '1453011518');
INSERT INTO `go_data_statistics` VALUES ('2288', '17', 'PP助手', 'pp', '', '9172', '1453011787');
INSERT INTO `go_data_statistics` VALUES ('2289', '8', '一元众乐', 'yyzl', '', '9173', '1453011909');
INSERT INTO `go_data_statistics` VALUES ('2290', '8', '一元众乐', 'yyzl', '', '9174', '1453011919');
INSERT INTO `go_data_statistics` VALUES ('2291', '30', '开心看', 'zlkxk', '', '9175', '1453012018');
INSERT INTO `go_data_statistics` VALUES ('2292', '4', '百度推广', 'zlbdtg', 'SEM00000122', '9176', '1453012617');
INSERT INTO `go_data_statistics` VALUES ('2293', '8', '一元众乐', 'yyzl', '', '9177', '1453012652');
INSERT INTO `go_data_statistics` VALUES ('2294', '4', '百度推广', 'zlbdtg', 'SEM00000572', '9178', '1453012827');
INSERT INTO `go_data_statistics` VALUES ('2295', '4', '百度推广', 'zlbdtg', 'SEM00000530', '9179', '1453012837');
INSERT INTO `go_data_statistics` VALUES ('2296', '29', '小米开发者平台', 'xiaomi', '', '9180', '1453013216');
INSERT INTO `go_data_statistics` VALUES ('2297', '4', '百度推广', 'zlbdtg', 'SEM00000114', '9181', '1453013300');
INSERT INTO `go_data_statistics` VALUES ('2298', '4', '百度推广', 'zlbdtg', 'SEM00000114', '9182', '1453013310');
INSERT INTO `go_data_statistics` VALUES ('2299', '30', '开心看', 'zlkxk', '', '9183', '1453013504');
INSERT INTO `go_data_statistics` VALUES ('2300', '29', '小米开发者平台', 'xiaomi', '', '9184', '1453013777');
INSERT INTO `go_data_statistics` VALUES ('2301', '8', '一元众乐', 'yyzl', '', '9185', '1453013923');
INSERT INTO `go_data_statistics` VALUES ('2302', '30', '开心看', 'zlkxk', '', '9186', '1453014211');
INSERT INTO `go_data_statistics` VALUES ('2303', '30', '开心看', 'zlkxk', '', '9187', '1453014224');
INSERT INTO `go_data_statistics` VALUES ('2304', '30', '开心看', 'zlkxk', '', '9188', '1453014233');
INSERT INTO `go_data_statistics` VALUES ('2305', '8', '一元众乐', 'yyzl', '', '9189', '1453014375');
INSERT INTO `go_data_statistics` VALUES ('2306', '8', '一元众乐', 'yyzl', '', '9190', '1453014797');
INSERT INTO `go_data_statistics` VALUES ('2307', '6', '信号增强器', 'zlxhzqq', '', '9191', '1453014869');
INSERT INTO `go_data_statistics` VALUES ('2308', '8', '一元众乐', 'yyzl', '', '9192', '1453014949');
INSERT INTO `go_data_statistics` VALUES ('2309', '30', '开心看', 'zlkxk', '', '9194', '1453015120');
INSERT INTO `go_data_statistics` VALUES ('2310', '30', '开心看', 'zlkxk', '', '9196', '1453015985');
INSERT INTO `go_data_statistics` VALUES ('2311', '30', '开心看', 'zlkxk', '', '9197', '1453016258');
INSERT INTO `go_data_statistics` VALUES ('2312', '8', '一元众乐', 'yyzl', '', '9198', '1453016526');
INSERT INTO `go_data_statistics` VALUES ('2313', '4', '百度推广', 'zlbdtg', 'SEM00000625', '9199', '1453016563');
INSERT INTO `go_data_statistics` VALUES ('2314', '4', '百度推广', 'zlbdtg', 'SEM00000200', '9200', '1453016795');
INSERT INTO `go_data_statistics` VALUES ('2315', '6', '信号增强器', 'zlxhzqq', '', '9201', '1453016906');
INSERT INTO `go_data_statistics` VALUES ('2316', '15', '百度', 'baidu', '', '9202', '1453016950');
INSERT INTO `go_data_statistics` VALUES ('2317', '30', '开心看', 'zlkxk', '', '9203', '1453017464');
INSERT INTO `go_data_statistics` VALUES ('2318', '8', '一元众乐', 'yyzl', '', '9204', '1453017602');
INSERT INTO `go_data_statistics` VALUES ('2319', '8', '一元众乐', 'yyzl', '', '9205', '1453017796');
INSERT INTO `go_data_statistics` VALUES ('2320', '4', '百度推广', 'zlbdtg', 'YDSEM00000320', '9206', '1453017947');
INSERT INTO `go_data_statistics` VALUES ('2321', '4', '百度推广', 'zlbdtg', 'YDSEM00000574', '9207', '1453018147');
INSERT INTO `go_data_statistics` VALUES ('2322', '21', 'oppo', 'oppo', '', '9208', '1453018273');
INSERT INTO `go_data_statistics` VALUES ('2323', '4', '百度推广', 'zlbdtg', 'YDSEM00000578', '9209', '1453018339');
INSERT INTO `go_data_statistics` VALUES ('2324', '4', '百度推广', 'zlbdtg', 'SEM00000115', '9210', '1453018492');
INSERT INTO `go_data_statistics` VALUES ('2325', '17', 'PP助手', 'pp', '', '9211', '1453018543');
INSERT INTO `go_data_statistics` VALUES ('2326', '4', '百度推广', 'zlbdtg', 'YDSEM00000117', '9232', '1453018565');
INSERT INTO `go_data_statistics` VALUES ('2327', '4', '百度推广', 'zlbdtg', 'SEM00000622', '9233', '1453018585');
INSERT INTO `go_data_statistics` VALUES ('2328', '4', '百度推广', 'zlbdtg', 'SEM00000572', '9234', '1453018931');
INSERT INTO `go_data_statistics` VALUES ('2329', '30', '开心看', 'zlkxk', '', '9235', '1453019111');
INSERT INTO `go_data_statistics` VALUES ('2330', '8', '一元众乐', 'yyzl', '', '9236', '1453019114');
INSERT INTO `go_data_statistics` VALUES ('2331', '8', '一元众乐', 'yyzl', '', '9237', '1453019138');
INSERT INTO `go_data_statistics` VALUES ('2332', '8', '一元众乐', 'yyzl', '', '9238', '1453019466');
INSERT INTO `go_data_statistics` VALUES ('2333', '8', '一元众乐', 'yyzl', '', '9239', '1453019487');
INSERT INTO `go_data_statistics` VALUES ('2334', '6', '信号增强器', 'zlxhzqq', '', '9240', '1453019588');
INSERT INTO `go_data_statistics` VALUES ('2335', '30', '开心看', 'zlkxk', '', '9241', '1453019614');
INSERT INTO `go_data_statistics` VALUES ('2336', '8', '一元众乐', 'yyzl', '', '9242', '1453020538');
INSERT INTO `go_data_statistics` VALUES ('2337', '30', '开心看', 'zlkxk', '', '9243', '1453020594');
INSERT INTO `go_data_statistics` VALUES ('2338', '30', '开心看', 'zlkxk', '', '9245', '1453020819');
INSERT INTO `go_data_statistics` VALUES ('2339', '8', '一元众乐', 'yyzl', '', '9247', '1453021519');
INSERT INTO `go_data_statistics` VALUES ('2340', '30', '开心看', 'zlkxk', '', '9248', '1453021519');
INSERT INTO `go_data_statistics` VALUES ('2341', '8', '一元众乐', 'yyzl', '', '9249', '1453021668');
INSERT INTO `go_data_statistics` VALUES ('2342', '4', '百度推广', 'zlbdtg', 'SEM00000115', '9250', '1453021954');
INSERT INTO `go_data_statistics` VALUES ('2343', '4', '百度推广', 'zlbdtg', 'YDSEM00000573', '9251', '1453022044');
INSERT INTO `go_data_statistics` VALUES ('2344', '30', '开心看', 'zlkxk', '', '9252', '1453022261');
INSERT INTO `go_data_statistics` VALUES ('2345', '8', '一元众乐', 'yyzl', '', '9253', '1453022273');
INSERT INTO `go_data_statistics` VALUES ('2346', '4', '百度推广', 'zlbdtg', 'SEM00000639', '9254', '1453022301');
INSERT INTO `go_data_statistics` VALUES ('2347', '4', '百度推广', 'zlbdtg', 'SEM00000518', '9256', '1453022845');
INSERT INTO `go_data_statistics` VALUES ('2348', '24', '魅族', 'meizu', '', '9257', '1453023056');
INSERT INTO `go_data_statistics` VALUES ('2349', '4', '百度推广', 'zlbdtg', 'SEM00000573', '9258', '1453023361');
INSERT INTO `go_data_statistics` VALUES ('2350', '16', '豌豆荚', 'wandoujia', '', '9259', '1453023727');
INSERT INTO `go_data_statistics` VALUES ('2351', '4', '百度推广', 'zlbdtg', 'YDSEM00000532', '9260', '1453024098');
INSERT INTO `go_data_statistics` VALUES ('2352', '4', '百度推广', 'zlbdtg', 'YDSEM00000115', '9261', '1453024352');
INSERT INTO `go_data_statistics` VALUES ('2353', '30', '开心看', 'zlkxk', '', '9262', '1453024380');
INSERT INTO `go_data_statistics` VALUES ('2354', '30', '开心看', 'zlkxk', '', '9263', '1453024637');
INSERT INTO `go_data_statistics` VALUES ('2355', '8', '一元众乐', 'yyzl', '', '9264', '1453024701');
INSERT INTO `go_data_statistics` VALUES ('2356', '8', '一元众乐', 'yyzl', '', '9265', '1453024706');
INSERT INTO `go_data_statistics` VALUES ('2357', '8', '一元众乐', 'yyzl', '', '9266', '1453024952');
INSERT INTO `go_data_statistics` VALUES ('2358', '30', '开心看', 'zlkxk', '', '9267', '1453025399');
INSERT INTO `go_data_statistics` VALUES ('2359', '8', '一元众乐', 'yyzl', '', '9268', '1453025465');
INSERT INTO `go_data_statistics` VALUES ('2360', '30', '开心看', 'zlkxk', '', '9269', '1453025674');
INSERT INTO `go_data_statistics` VALUES ('2361', '6', '信号增强器', 'zlxhzqq', '', '9270', '1453025746');
INSERT INTO `go_data_statistics` VALUES ('2362', '30', '开心看', 'zlkxk', '', '9271', '1453026219');
INSERT INTO `go_data_statistics` VALUES ('2363', '8', '一元众乐', 'yyzl', '', '9272', '1453026276');
INSERT INTO `go_data_statistics` VALUES ('2364', '6', '信号增强器', 'zlxhzqq', '', '9273', '1453026477');
INSERT INTO `go_data_statistics` VALUES ('2365', '4', '百度推广', 'zlbdtg', 'YDSEM00000573', '9274', '1453026534');
INSERT INTO `go_data_statistics` VALUES ('2366', '8', '一元众乐', 'yyzl', '', '9276', '1453026835');
INSERT INTO `go_data_statistics` VALUES ('2367', '30', '开心看', 'zlkxk', '', '9277', '1453027595');
INSERT INTO `go_data_statistics` VALUES ('2368', '4', '百度推广', 'zlbdtg', 'SEM00000595', '9278', '1453027614');
INSERT INTO `go_data_statistics` VALUES ('2369', '30', '开心看', 'zlkxk', '', '9280', '1453028098');
INSERT INTO `go_data_statistics` VALUES ('2370', '8', '一元众乐', 'yyzl', '', '9281', '1453028501');
INSERT INTO `go_data_statistics` VALUES ('2371', '30', '开心看', 'zlkxk', '', '9283', '1453028597');
INSERT INTO `go_data_statistics` VALUES ('2372', '4', '百度推广', 'zlbdtg', 'SEM00000573', '9284', '1453029091');
INSERT INTO `go_data_statistics` VALUES ('2373', '30', '开心看', 'zlkxk', '', '9285', '1453029250');
INSERT INTO `go_data_statistics` VALUES ('2374', '8', '一元众乐', 'yyzl', '', '9286', '1453029266');
INSERT INTO `go_data_statistics` VALUES ('2375', '4', '百度推广', 'zlbdtg', 'SEM00000117', '9287', '1453029366');
INSERT INTO `go_data_statistics` VALUES ('2376', '4', '百度推广', 'zlbdtg', 'YDSEM00000574', '9288', '1453029499');
INSERT INTO `go_data_statistics` VALUES ('2377', '4', '百度推广', 'zlbdtg', 'YDSEM00000588', '9289', '1453029778');
INSERT INTO `go_data_statistics` VALUES ('2378', '30', '开心看', 'zlkxk', '', '9290', '1453029795');
INSERT INTO `go_data_statistics` VALUES ('2379', '4', '百度推广', 'zlbdtg', 'YDSEM00000588', '9291', '1453029952');
INSERT INTO `go_data_statistics` VALUES ('2380', '4', '百度推广', 'zlbdtg', 'YDSEM00000121', '9292', '1453030008');
INSERT INTO `go_data_statistics` VALUES ('2381', '8', '一元众乐', 'yyzl', '', '9293', '1453030189');
INSERT INTO `go_data_statistics` VALUES ('2382', '4', '百度推广', 'zlbdtg', 'YDSEM00000339', '9294', '1453030215');
INSERT INTO `go_data_statistics` VALUES ('2383', '4', '百度推广', 'zlbdtg', 'YDSEM00000246', '9295', '1453030579');
INSERT INTO `go_data_statistics` VALUES ('2384', '8', '一元众乐', 'yyzl', '', '9296', '1453030623');
INSERT INTO `go_data_statistics` VALUES ('2385', '30', '开心看', 'zlkxk', '', '9297', '1453030746');
INSERT INTO `go_data_statistics` VALUES ('2386', '30', '开心看', 'zlkxk', '', '9298', '1453030785');
INSERT INTO `go_data_statistics` VALUES ('2387', '8', '一元众乐', 'yyzl', '', '9299', '1453030843');
INSERT INTO `go_data_statistics` VALUES ('2388', '6', '信号增强器', 'zlxhzqq', '', '9300', '1453030972');
INSERT INTO `go_data_statistics` VALUES ('2389', '30', '开心看', 'zlkxk', '', '9301', '1453031090');
INSERT INTO `go_data_statistics` VALUES ('2390', '4', '百度推广', 'zlbdtg', 'SEM00000120', '9302', '1453031220');
INSERT INTO `go_data_statistics` VALUES ('2391', '8', '一元众乐', 'yyzl', '', '9303', '1453031473');
INSERT INTO `go_data_statistics` VALUES ('2392', '19', '木蚂蚁', 'mumayi', '', '9304', '1453031624');
INSERT INTO `go_data_statistics` VALUES ('2393', '4', '百度推广', 'zlbdtg', 'YDSEM00000339', '9305', '1453031677');
INSERT INTO `go_data_statistics` VALUES ('2394', '4', '百度推广', 'zlbdtg', 'SEM00000635', '9306', '1453032180');
INSERT INTO `go_data_statistics` VALUES ('2395', '30', '开心看', 'zlkxk', '', '9307', '1453032191');
INSERT INTO `go_data_statistics` VALUES ('2396', '19', '木蚂蚁', 'mumayi', '', '9308', '1453032351');
INSERT INTO `go_data_statistics` VALUES ('2397', '4', '百度推广', 'zlbdtg', 'SEM00000122', '9309', '1453032430');
INSERT INTO `go_data_statistics` VALUES ('2398', '4', '百度推广', 'zlbdtg', 'YDSEM00000117', '9310', '1453032441');
INSERT INTO `go_data_statistics` VALUES ('2399', '4', '百度推广', 'zlbdtg', 'YDSEM00000115', '9311', '1453032468');
INSERT INTO `go_data_statistics` VALUES ('2400', '30', '开心看', 'zlkxk', '', '9312', '1453032575');
INSERT INTO `go_data_statistics` VALUES ('2401', '4', '百度推广', 'zlbdtg', 'SEM00000121', '9313', '1453032798');
INSERT INTO `go_data_statistics` VALUES ('2402', '4', '百度推广', 'zlbdtg', 'YDSEM00000611', '9314', '1453033396');
INSERT INTO `go_data_statistics` VALUES ('2403', '4', '百度推广', 'zlbdtg', 'YDSEM00000115', '9315', '1453033421');
INSERT INTO `go_data_statistics` VALUES ('2404', '4', '百度推广', 'zlbdtg', 'YDSEM00000339', '9316', '1453033432');
INSERT INTO `go_data_statistics` VALUES ('2405', '8', '一元众乐', 'yyzl', '', '9317', '1453033485');
INSERT INTO `go_data_statistics` VALUES ('2406', '19', '木蚂蚁', 'mumayi', '', '9318', '1453033595');
INSERT INTO `go_data_statistics` VALUES ('2407', '8', '一元众乐', 'yyzl', '', '9319', '1453033599');
INSERT INTO `go_data_statistics` VALUES ('2408', '4', '百度推广', 'zlbdtg', 'YDSEM00000121', '9320', '1453033801');
INSERT INTO `go_data_statistics` VALUES ('2409', '4', '百度推广', 'zlbdtg', 'SEM00000121', '9321', '1453034025');
INSERT INTO `go_data_statistics` VALUES ('2410', '8', '一元众乐', 'yyzl', '', '9322', '1453034025');
INSERT INTO `go_data_statistics` VALUES ('2411', '4', '百度推广', 'zlbdtg', 'YDSEM00000117', '9323', '1453034202');
INSERT INTO `go_data_statistics` VALUES ('2412', '30', '开心看', 'zlkxk', '', '9324', '1453034282');
INSERT INTO `go_data_statistics` VALUES ('2413', '4', '百度推广', 'zlbdtg', 'SEM00000619', '9325', '1453034284');
INSERT INTO `go_data_statistics` VALUES ('2414', '8', '一元众乐', 'yyzl', '', '9326', '1453034395');
INSERT INTO `go_data_statistics` VALUES ('2415', '15', '百度', 'baidu', '', '9327', '1453034439');
INSERT INTO `go_data_statistics` VALUES ('2416', '30', '开心看', 'zlkxk', '', '9328', '1453034461');
INSERT INTO `go_data_statistics` VALUES ('2417', '30', '开心看', 'zlkxk', '', '9329', '1453034679');
INSERT INTO `go_data_statistics` VALUES ('2418', '4', '百度推广', 'zlbdtg', 'YDSEM00000573', '9330', '1453034896');
INSERT INTO `go_data_statistics` VALUES ('2419', '19', '木蚂蚁', 'mumayi', '', '9331', '1453034967');
INSERT INTO `go_data_statistics` VALUES ('2420', '30', '开心看', 'zlkxk', '', '9332', '1453035246');
INSERT INTO `go_data_statistics` VALUES ('2421', '6', '信号增强器', 'zlxhzqq', '', '9333', '1453035479');
INSERT INTO `go_data_statistics` VALUES ('2422', '4', '百度推广', 'zlbdtg', 'SEM00000115', '9334', '1453035586');
INSERT INTO `go_data_statistics` VALUES ('2423', '8', '一元众乐', 'yyzl', '', '9335', '1453035589');
INSERT INTO `go_data_statistics` VALUES ('2424', '4', '百度推广', 'zlbdtg', 'YDSEM00000339', '9336', '1453035891');
INSERT INTO `go_data_statistics` VALUES ('2425', '6', '信号增强器', 'zlxhzqq', '', '9337', '1453035894');
INSERT INTO `go_data_statistics` VALUES ('2426', '8', '一元众乐', 'yyzl', '', '9338', '1453036003');
INSERT INTO `go_data_statistics` VALUES ('2427', '8', '一元众乐', 'yyzl', '', '9339', '1453036449');
INSERT INTO `go_data_statistics` VALUES ('2428', '4', '百度推广', 'zlbdtg', 'YDSEM00000343', '9340', '1453036508');
INSERT INTO `go_data_statistics` VALUES ('2429', '30', '开心看', 'zlkxk', '', '9341', '1453036553');
INSERT INTO `go_data_statistics` VALUES ('2430', '8', '一元众乐', 'yyzl', '', '9342', '1453036686');
INSERT INTO `go_data_statistics` VALUES ('2431', '4', '百度推广', 'zlbdtg', 'SEM00000572', '9343', '1453036755');
INSERT INTO `go_data_statistics` VALUES ('2432', '4', '百度推广', 'zlbdtg', 'YDSEM00000121', '9344', '1453036849');
INSERT INTO `go_data_statistics` VALUES ('2433', '30', '开心看', 'zlkxk', '', '9345', '1453036872');
INSERT INTO `go_data_statistics` VALUES ('2434', '30', '开心看', 'zlkxk', '', '9346', '1453036896');
INSERT INTO `go_data_statistics` VALUES ('2435', '30', '开心看', 'zlkxk', '', '9347', '1453037159');
INSERT INTO `go_data_statistics` VALUES ('2436', '4', '百度推广', 'zlbdtg', 'SEM00000598', '9348', '1453037217');
INSERT INTO `go_data_statistics` VALUES ('2437', '4', '百度推广', 'zlbdtg', 'YDSEM00000573', '9349', '1453037257');
INSERT INTO `go_data_statistics` VALUES ('2438', '30', '开心看', 'zlkxk', '', '9350', '1453037324');
INSERT INTO `go_data_statistics` VALUES ('2439', '8', '一元众乐', 'yyzl', '', '9351', '1453037495');
INSERT INTO `go_data_statistics` VALUES ('2440', '6', '信号增强器', 'zlxhzqq', '', '9352', '1453038012');
INSERT INTO `go_data_statistics` VALUES ('2441', '4', '百度推广', 'zlbdtg', 'YDSEM00000121', '9353', '1453038194');
INSERT INTO `go_data_statistics` VALUES ('2442', '8', '一元众乐', 'yyzl', '', '9354', '1453038203');
INSERT INTO `go_data_statistics` VALUES ('2443', '19', '木蚂蚁', 'mumayi', '', '9355', '1453038264');
INSERT INTO `go_data_statistics` VALUES ('2444', '30', '开心看', 'zlkxk', '', '9356', '1453038270');
INSERT INTO `go_data_statistics` VALUES ('2445', '30', '开心看', 'zlkxk', '', '9357', '1453038420');
INSERT INTO `go_data_statistics` VALUES ('2446', '4', '百度推广', 'zlbdtg', 'SEM00000610', '9358', '1453038423');
INSERT INTO `go_data_statistics` VALUES ('2447', '4', '百度推广', 'zlbdtg', 'SEM00000201', '9359', '1453038467');
INSERT INTO `go_data_statistics` VALUES ('2448', '30', '开心看', 'zlkxk', '', '9360', '1453038597');
INSERT INTO `go_data_statistics` VALUES ('2449', '30', '开心看', 'zlkxk', '', '9361', '1453039091');
INSERT INTO `go_data_statistics` VALUES ('2450', '30', '开心看', 'zlkxk', '', '9362', '1453039126');
INSERT INTO `go_data_statistics` VALUES ('2451', '30', '开心看', 'zlkxk', '', '9363', '1453039166');
INSERT INTO `go_data_statistics` VALUES ('2452', '30', '开心看', 'zlkxk', '', '9364', '1453039469');
INSERT INTO `go_data_statistics` VALUES ('2453', '30', '开心看', 'zlkxk', '', '9365', '1453039665');
INSERT INTO `go_data_statistics` VALUES ('2454', '4', '百度推广', 'zlbdtg', 'SEM00000115', '9367', '1453039850');
INSERT INTO `go_data_statistics` VALUES ('2455', '4', '百度推广', 'zlbdtg', 'YDSEM00000203', '9368', '1453039888');
INSERT INTO `go_data_statistics` VALUES ('2456', '4', '百度推广', 'zlbdtg', 'SEM00000115', '9369', '1453039909');
INSERT INTO `go_data_statistics` VALUES ('2457', '4', '百度推广', 'zlbdtg', 'YDSEM00000611', '9370', '1453039951');
INSERT INTO `go_data_statistics` VALUES ('2458', '4', '百度推广', 'zlbdtg', 'SEM00000115', '9371', '1453040082');
INSERT INTO `go_data_statistics` VALUES ('2459', '30', '开心看', 'zlkxk', '', '9372', '1453040321');
INSERT INTO `go_data_statistics` VALUES ('2460', '8', '一元众乐', 'yyzl', '', '9373', '1453040412');
INSERT INTO `go_data_statistics` VALUES ('2461', '30', '开心看', 'zlkxk', '', '9375', '1453040692');
INSERT INTO `go_data_statistics` VALUES ('2462', '8', '一元众乐', 'yyzl', '', '9376', '1453041018');
INSERT INTO `go_data_statistics` VALUES ('2463', '8', '一元众乐', 'yyzl', '', '9377', '1453041138');
INSERT INTO `go_data_statistics` VALUES ('2464', '4', '百度推广', 'zlbdtg', 'SEM00000119', '9378', '1453041175');
INSERT INTO `go_data_statistics` VALUES ('2465', '4', '百度推广', 'zlbdtg', 'YDSEM00000386', '9379', '1453041417');
INSERT INTO `go_data_statistics` VALUES ('2466', '4', '百度推广', 'zlbdtg', 'SEM00000121', '9380', '1453041945');
INSERT INTO `go_data_statistics` VALUES ('2467', '30', '开心看', 'zlkxk', '', '9381', '1453041957');
INSERT INTO `go_data_statistics` VALUES ('2468', '6', '信号增强器', 'zlxhzqq', '', '9382', '1453042061');
INSERT INTO `go_data_statistics` VALUES ('2469', '4', '百度推广', 'zlbdtg', 'YDSEM00000635', '9383', '1453042111');
INSERT INTO `go_data_statistics` VALUES ('2470', '4', '百度推广', 'zlbdtg', 'YDSEM00000328', '9384', '1453042184');
INSERT INTO `go_data_statistics` VALUES ('2471', '30', '开心看', 'zlkxk', '', '9385', '1453042352');
INSERT INTO `go_data_statistics` VALUES ('2472', '8', '一元众乐', 'yyzl', '', '9386', '1453042520');
INSERT INTO `go_data_statistics` VALUES ('2473', '4', '百度推广', 'zlbdtg', 'SEM00000180', '9387', '1453042629');
INSERT INTO `go_data_statistics` VALUES ('2474', '8', '一元众乐', 'yyzl', '', '9388', '1453042665');
INSERT INTO `go_data_statistics` VALUES ('2475', '8', '一元众乐', 'yyzl', '', '9389', '1453042767');
INSERT INTO `go_data_statistics` VALUES ('2476', '30', '开心看', 'zlkxk', '', '9390', '1453042852');
INSERT INTO `go_data_statistics` VALUES ('2477', '4', '百度推广', 'zlbdtg', 'YDSEM00000573', '9391', '1453043024');
INSERT INTO `go_data_statistics` VALUES ('2478', '4', '百度推广', 'zlbdtg', 'YDSEM00000121', '9392', '1453043053');
INSERT INTO `go_data_statistics` VALUES ('2479', '30', '开心看', 'zlkxk', '', '9393', '1453043437');
INSERT INTO `go_data_statistics` VALUES ('2480', '30', '开心看', 'zlkxk', '', '9394', '1453043469');
INSERT INTO `go_data_statistics` VALUES ('2481', '8', '一元众乐', 'yyzl', '', '9395', '1453043973');
INSERT INTO `go_data_statistics` VALUES ('2482', '8', '一元众乐', 'yyzl', '', '9396', '1453044348');
INSERT INTO `go_data_statistics` VALUES ('2483', '8', '一元众乐', 'yyzl', '', '9398', '1453044811');
INSERT INTO `go_data_statistics` VALUES ('2484', '8', '一元众乐', 'yyzl', '', '9399', '1453045889');
INSERT INTO `go_data_statistics` VALUES ('2485', '8', '一元众乐', 'yyzl', '', '9400', '1453046010');
INSERT INTO `go_data_statistics` VALUES ('2486', '4', '百度推广', 'zlbdtg', 'SEM00000536', '9401', '1453047492');
INSERT INTO `go_data_statistics` VALUES ('2487', '6', '信号增强器', 'zlxhzqq', '', '9402', '1453047614');
INSERT INTO `go_data_statistics` VALUES ('2488', '4', '百度推广', 'zlbdtg', 'SEM00000572', '9403', '1453047651');
INSERT INTO `go_data_statistics` VALUES ('2489', '30', '开心看', 'zlkxk', '', '9404', '1453048263');
INSERT INTO `go_data_statistics` VALUES ('2490', '8', '一元众乐', 'yyzl', '', '9405', '1453050522');
INSERT INTO `go_data_statistics` VALUES ('2491', '4', '百度推广', 'zlbdtg', 'YDSEM00000607', '9406', '1453050835');
INSERT INTO `go_data_statistics` VALUES ('2492', '8', '一元众乐', 'yyzl', '', '9407', '1453051702');
INSERT INTO `go_data_statistics` VALUES ('2493', '4', '百度推广', 'zlbdtg', 'YDSEM00000611', '9408', '1453052115');
INSERT INTO `go_data_statistics` VALUES ('2494', '6', '信号增强器', 'zlxhzqq', '', '9409', '1453052692');
INSERT INTO `go_data_statistics` VALUES ('2495', '4', '百度推广', 'zlbdtg', 'YDSEM00000339', '9410', '1453053103');
INSERT INTO `go_data_statistics` VALUES ('2496', '4', '百度推广', 'zlbdtg', 'YDSEM00000328', '9411', '1453053104');
INSERT INTO `go_data_statistics` VALUES ('2497', '4', '百度推广', 'zlbdtg', 'SEM00000121', '9413', '1453055272');
INSERT INTO `go_data_statistics` VALUES ('2498', '8', '一元众乐', 'yyzl', '', '9414', '1453055581');
INSERT INTO `go_data_statistics` VALUES ('2499', '4', '百度推广', 'zlbdtg', 'YDSEM00000339', '9415', '1453055699');
INSERT INTO `go_data_statistics` VALUES ('2500', '4', '百度推广', 'zlbdtg', 'YDSEM00000115', '9416', '1453056923');
INSERT INTO `go_data_statistics` VALUES ('2501', '30', '开心看', 'zlkxk', '', '9417', '1453057266');
INSERT INTO `go_data_statistics` VALUES ('2502', '4', '百度推广', 'zlbdtg', 'SEM00000573', '9418', '1453058070');
INSERT INTO `go_data_statistics` VALUES ('2503', '4', '百度推广', 'zlbdtg', 'SEM00000619', '9419', '1453059772');
INSERT INTO `go_data_statistics` VALUES ('2504', '8', '一元众乐', 'yyzl', '', '9421', '1453063501');
INSERT INTO `go_data_statistics` VALUES ('2505', '8', '一元众乐', 'yyzl', '', '9422', '1453064570');
INSERT INTO `go_data_statistics` VALUES ('2506', '16', '豌豆荚', 'wandoujia', '', '9423', '1453065919');
INSERT INTO `go_data_statistics` VALUES ('2507', '4', '百度推广', 'zlbdtg', 'SEM00000115', '9424', '1453068681');
INSERT INTO `go_data_statistics` VALUES ('2508', '4', '百度推广', 'zlbdtg', 'SEM00000115', '9425', '1453068804');
INSERT INTO `go_data_statistics` VALUES ('2509', '6', '信号增强器', 'zlxhzqq', '', '9426', '1453069821');
INSERT INTO `go_data_statistics` VALUES ('2510', '4', '百度推广', 'zlbdtg', 'YDSEM00000573', '9427', '1453070759');
INSERT INTO `go_data_statistics` VALUES ('2511', '4', '百度推广', 'zlbdtg', 'YDSEM00000572', '9428', '1453072556');
INSERT INTO `go_data_statistics` VALUES ('2512', '8', '一元众乐', 'yyzl', '', '9429', '1453073406');
INSERT INTO `go_data_statistics` VALUES ('2513', '30', '开心看', 'zlkxk', '', '9430', '1453074258');
INSERT INTO `go_data_statistics` VALUES ('2514', '8', '一元众乐', 'yyzl', '', '9431', '1453074815');
INSERT INTO `go_data_statistics` VALUES ('2515', '4', '百度推广', 'zlbdtg', 'YDSEM00000573', '9432', '1453074825');
INSERT INTO `go_data_statistics` VALUES ('2516', '8', '一元众乐', 'yyzl', '', '9433', '1453075066');
INSERT INTO `go_data_statistics` VALUES ('2517', '6', '信号增强器', 'zlxhzqq', '', '9434', '1453075173');
INSERT INTO `go_data_statistics` VALUES ('2518', '30', '开心看', 'zlkxk', '', '9435', '1453075651');
INSERT INTO `go_data_statistics` VALUES ('2519', '4', '百度推广', 'zlbdtg', 'SEM00000610', '9436', '1453076937');
INSERT INTO `go_data_statistics` VALUES ('2520', '14', '360手机助手', 'dev360', '', '9437', '1453077303');
INSERT INTO `go_data_statistics` VALUES ('2521', '4', '百度推广', 'zlbdtg', 'SEM00000572', '9438', '1453077730');
INSERT INTO `go_data_statistics` VALUES ('2522', '8', '一元众乐', 'yyzl', '', '9439', '1453077744');
INSERT INTO `go_data_statistics` VALUES ('2523', '4', '百度推广', 'zlbdtg', 'SEM00000573', '9441', '1453078331');
INSERT INTO `go_data_statistics` VALUES ('2524', '4', '百度推广', 'zlbdtg', 'YDSEM00000117', '9442', '1453078486');
INSERT INTO `go_data_statistics` VALUES ('2525', '4', '百度推广', 'zlbdtg', 'YDSEM00000114', '9443', '1453078560');
INSERT INTO `go_data_statistics` VALUES ('2526', '4', '百度推广', 'zlbdtg', 'YDSEM00000114', '9444', '1453078863');
INSERT INTO `go_data_statistics` VALUES ('2527', '4', '百度推广', 'zlbdtg', 'YDSEM00000573', '9445', '1453078978');
INSERT INTO `go_data_statistics` VALUES ('2528', '8', '一元众乐', 'yyzl', '', '9446', '1453079324');
INSERT INTO `go_data_statistics` VALUES ('2529', '4', '百度推广', 'zlbdtg', 'SEM00000115', '9447', '1453079652');
INSERT INTO `go_data_statistics` VALUES ('2530', '4', '百度推广', 'zlbdtg', 'YDSEM00000573', '9448', '1453079693');
INSERT INTO `go_data_statistics` VALUES ('2531', '8', '一元众乐', 'yyzl', '', '9449', '1453079920');
INSERT INTO `go_data_statistics` VALUES ('2532', '4', '百度推广', 'zlbdtg', 'SEM00000012', '9450', '1453080341');
INSERT INTO `go_data_statistics` VALUES ('2533', '8', '一元众乐', 'yyzl', '', '9451', '1453080718');
INSERT INTO `go_data_statistics` VALUES ('2534', '8', '一元众乐', 'yyzl', '', '9452', '1453080718');
INSERT INTO `go_data_statistics` VALUES ('2535', '4', '百度推广', 'zlbdtg', 'YDSEM00000628', '9453', '1453081212');
INSERT INTO `go_data_statistics` VALUES ('2536', '4', '百度推广', 'zlbdtg', 'SEM00000627', '9454', '1453082187');
INSERT INTO `go_data_statistics` VALUES ('2537', '4', '百度推广', 'zlbdtg', 'YDSEM00000628', '9455', '1453082211');
INSERT INTO `go_data_statistics` VALUES ('2538', '4', '百度推广', 'zlbdtg', 'SEM00000121', '9456', '1453082281');
INSERT INTO `go_data_statistics` VALUES ('2539', '4', '百度推广', 'zlbdtg', 'SEM00000622', '9457', '1453082351');
INSERT INTO `go_data_statistics` VALUES ('2540', '4', '百度推广', 'zlbdtg', 'YDSEM00000628', '9458', '1453082565');
INSERT INTO `go_data_statistics` VALUES ('2541', '30', '开心看', 'zlkxk', '', '9459', '1453082711');
INSERT INTO `go_data_statistics` VALUES ('2542', '8', '一元众乐', 'yyzl', '', '9460', '1453082921');
INSERT INTO `go_data_statistics` VALUES ('2543', '30', '开心看', 'zlkxk', '', '9461', '1453083065');
INSERT INTO `go_data_statistics` VALUES ('2544', '8', '一元众乐', 'yyzl', '', '9462', '1453083188');
INSERT INTO `go_data_statistics` VALUES ('2545', '4', '百度推广', 'zlbdtg', 'SEM00000114', '9463', '1453083725');
INSERT INTO `go_data_statistics` VALUES ('2546', '4', '百度推广', 'zlbdtg', 'SEM00000622', '9464', '1453083893');
INSERT INTO `go_data_statistics` VALUES ('2547', '30', '开心看', 'zlkxk', '', '9465', '1453084221');
INSERT INTO `go_data_statistics` VALUES ('2548', '4', '百度推广', 'zlbdtg', 'YDSEM00000628', '9466', '1453084335');
INSERT INTO `go_data_statistics` VALUES ('2549', '4', '百度推广', 'zlbdtg', 'YDSEM00000628', '9467', '1453084344');
INSERT INTO `go_data_statistics` VALUES ('2550', '4', '百度推广', 'zlbdtg', 'SEM00000643', '9468', '1453084406');
INSERT INTO `go_data_statistics` VALUES ('2551', '8', '一元众乐', 'yyzl', '', '9469', '1453084429');
INSERT INTO `go_data_statistics` VALUES ('2552', '30', '开心看', 'zlkxk', '', '9470', '1453084432');
INSERT INTO `go_data_statistics` VALUES ('2553', '30', '开心看', 'zlkxk', '', '9471', '1453084561');
INSERT INTO `go_data_statistics` VALUES ('2554', '8', '一元众乐', 'yyzl', '', '9472', '1453084631');
INSERT INTO `go_data_statistics` VALUES ('2555', '4', '百度推广', 'zlbdtg', 'YDSEM00000573', '9473', '1453085243');
INSERT INTO `go_data_statistics` VALUES ('2556', '4', '百度推广', 'zlbdtg', 'YDSEM00000121', '9474', '1453085616');
INSERT INTO `go_data_statistics` VALUES ('2557', '4', '百度推广', 'zlbdtg', 'YDSEM00000572', '9475', '1453085673');
INSERT INTO `go_data_statistics` VALUES ('2558', '4', '百度推广', 'zlbdtg', 'YDSEM00000628', '9476', '1453085799');
INSERT INTO `go_data_statistics` VALUES ('2559', '4', '百度推广', 'zlbdtg', 'YDSEM00000628', '9478', '1453086035');
INSERT INTO `go_data_statistics` VALUES ('2560', '4', '百度推广', 'zlbdtg', 'SEM00000120', '9479', '1453086342');
INSERT INTO `go_data_statistics` VALUES ('2561', '30', '开心看', 'zlkxk', '', '9480', '1453086446');
INSERT INTO `go_data_statistics` VALUES ('2562', '30', '开心看', 'zlkxk', '', '9482', '1453086646');
INSERT INTO `go_data_statistics` VALUES ('2563', '6', '信号增强器', 'zlxhzqq', '', '9483', '1453086696');
INSERT INTO `go_data_statistics` VALUES ('2564', '6', '信号增强器', 'zlxhzqq', '', '9484', '1453087020');
INSERT INTO `go_data_statistics` VALUES ('2565', '8', '一元众乐', 'yyzl', '', '9486', '1453088600');
INSERT INTO `go_data_statistics` VALUES ('2566', '8', '一元众乐', 'yyzl', '', '9487', '1453089203');
INSERT INTO `go_data_statistics` VALUES ('2567', '4', '百度推广', 'zlbdtg', 'YDSEM00000573', '9488', '1453089445');
INSERT INTO `go_data_statistics` VALUES ('2568', '19', '木蚂蚁', 'mumayi', '', '9490', '1453089875');
INSERT INTO `go_data_statistics` VALUES ('2569', '17', 'PP助手', 'pp', '', '9491', '1453090511');
INSERT INTO `go_data_statistics` VALUES ('2570', '8', '一元众乐', 'yyzl', '', '9492', '1453090592');
INSERT INTO `go_data_statistics` VALUES ('2571', '8', '一元众乐', 'yyzl', '', '9493', '1453090602');
INSERT INTO `go_data_statistics` VALUES ('2572', '30', '开心看', 'zlkxk', '', '9494', '1453090918');
INSERT INTO `go_data_statistics` VALUES ('2573', '6', '信号增强器', 'zlxhzqq', '', '9495', '1453091071');
INSERT INTO `go_data_statistics` VALUES ('2574', '8', '一元众乐', 'yyzl', '', '9496', '1453091680');
INSERT INTO `go_data_statistics` VALUES ('2575', '8', '一元众乐', 'yyzl', '', '9497', '1453091720');
INSERT INTO `go_data_statistics` VALUES ('2576', '30', '开心看', 'zlkxk', '', '9499', '1453092350');
INSERT INTO `go_data_statistics` VALUES ('2577', '4', '百度推广', 'zlbdtg', 'SEM00000121', '9500', '1453092395');
INSERT INTO `go_data_statistics` VALUES ('2578', '30', '开心看', 'zlkxk', '', '9501', '1453092423');
INSERT INTO `go_data_statistics` VALUES ('2579', '30', '开心看', 'zlkxk', '', '9502', '1453092607');
INSERT INTO `go_data_statistics` VALUES ('2580', '4', '百度推广', 'zlbdtg', 'YDSEM00000573', '9503', '1453092832');
INSERT INTO `go_data_statistics` VALUES ('2581', '8', '一元众乐', 'yyzl', '', '9504', '1453093037');
INSERT INTO `go_data_statistics` VALUES ('2582', '8', '一元众乐', 'yyzl', '', '9505', '1453093054');
INSERT INTO `go_data_statistics` VALUES ('2583', '8', '一元众乐', 'yyzl', '', '9506', '1453093549');
INSERT INTO `go_data_statistics` VALUES ('2584', '8', '一元众乐', 'yyzl', '', '9507', '1453093988');
INSERT INTO `go_data_statistics` VALUES ('2585', '30', '开心看', 'zlkxk', '', '9508', '1453094140');
INSERT INTO `go_data_statistics` VALUES ('2586', '4', '百度推广', 'zlbdtg', 'YDSEM00000339', '9509', '1453094601');
INSERT INTO `go_data_statistics` VALUES ('2587', '4', '百度推广', 'zlbdtg', 'YDSEM00000339', '9510', '1453094717');
INSERT INTO `go_data_statistics` VALUES ('2588', '4', '百度推广', 'zlbdtg', 'SEM00000114', '9511', '1453094723');
INSERT INTO `go_data_statistics` VALUES ('2589', '8', '一元众乐', 'yyzl', '', '9512', '1453094816');
INSERT INTO `go_data_statistics` VALUES ('2590', '30', '开心看', 'zlkxk', '', '9513', '1453095014');
INSERT INTO `go_data_statistics` VALUES ('2591', '4', '百度推广', 'zlbdtg', 'SEM00000619', '9514', '1453095696');
INSERT INTO `go_data_statistics` VALUES ('2592', '30', '开心看', 'zlkxk', '', '9515', '1453096188');
INSERT INTO `go_data_statistics` VALUES ('2593', '30', '开心看', 'zlkxk', '', '9516', '1453096598');
INSERT INTO `go_data_statistics` VALUES ('2594', '8', '一元众乐', 'yyzl', '', '9519', '1453096936');
INSERT INTO `go_data_statistics` VALUES ('2595', '30', '开心看', 'zlkxk', '', '9521', '1453097793');
INSERT INTO `go_data_statistics` VALUES ('2596', '30', '开心看', 'zlkxk', '', '9522', '1453098302');
INSERT INTO `go_data_statistics` VALUES ('2597', '30', '开心看', 'zlkxk', '', '9523', '1453099018');
INSERT INTO `go_data_statistics` VALUES ('2598', '8', '一元众乐', 'yyzl', '', '9524', '1453099147');
INSERT INTO `go_data_statistics` VALUES ('2599', '8', '一元众乐', 'yyzl', '', '9525', '1453099217');
INSERT INTO `go_data_statistics` VALUES ('2600', '29', '小米开发者平台', 'xiaomi', '', '9526', '1453100492');
INSERT INTO `go_data_statistics` VALUES ('2601', '8', '一元众乐', 'yyzl', '', '9527', '1453100568');
INSERT INTO `go_data_statistics` VALUES ('2602', '30', '开心看', 'zlkxk', '', '9528', '1453100604');
INSERT INTO `go_data_statistics` VALUES ('2603', '30', '开心看', 'zlkxk', '', '9529', '1453100726');
INSERT INTO `go_data_statistics` VALUES ('2604', '8', '一元众乐', 'yyzl', '', '9530', '1453100734');
INSERT INTO `go_data_statistics` VALUES ('2605', '30', '开心看', 'zlkxk', '', '9531', '1453100738');
INSERT INTO `go_data_statistics` VALUES ('2606', '21', 'oppo', 'oppo', '', '9532', '1453101068');
INSERT INTO `go_data_statistics` VALUES ('2607', '6', '信号增强器', 'zlxhzqq', '', '9533', '1453101129');
INSERT INTO `go_data_statistics` VALUES ('2608', '30', '开心看', 'zlkxk', '', '9534', '1453101669');
INSERT INTO `go_data_statistics` VALUES ('2609', '8', '一元众乐', 'yyzl', '', '9535', '1453102832');
INSERT INTO `go_data_statistics` VALUES ('2610', '30', '开心看', 'zlkxk', '', '9536', '1453103487');
INSERT INTO `go_data_statistics` VALUES ('2611', '30', '开心看', 'zlkxk', '', '9537', '1453103854');
INSERT INTO `go_data_statistics` VALUES ('2612', '4', '百度推广', 'zlbdtg', 'SEM00000610', '9538', '1453104479');
INSERT INTO `go_data_statistics` VALUES ('2613', '8', '一元众乐', 'yyzl', '', '9539', '1453104498');
INSERT INTO `go_data_statistics` VALUES ('2614', '8', '一元众乐', 'yyzl', '', '9540', '1453104654');
INSERT INTO `go_data_statistics` VALUES ('2615', '16', '豌豆荚', 'wandoujia', '', '9541', '1453104723');
INSERT INTO `go_data_statistics` VALUES ('2616', '8', '一元众乐', 'yyzl', '', '9542', '1453105446');
INSERT INTO `go_data_statistics` VALUES ('2617', '14', '360手机助手', 'dev360', '', '9543', '1453105881');
INSERT INTO `go_data_statistics` VALUES ('2618', '30', '开心看', 'zlkxk', '', '9544', '1453106594');
INSERT INTO `go_data_statistics` VALUES ('2619', '17', 'PP助手', 'pp', '', '9545', '1453107242');
INSERT INTO `go_data_statistics` VALUES ('2620', '30', '开心看', 'zlkxk', '', '9546', '1453107275');
INSERT INTO `go_data_statistics` VALUES ('2621', '19', '木蚂蚁', 'mumayi', '', '9547', '1453107439');
INSERT INTO `go_data_statistics` VALUES ('2622', '30', '开心看', 'zlkxk', '', '9548', '1453107462');
INSERT INTO `go_data_statistics` VALUES ('2623', '30', '开心看', 'zlkxk', '', '9549', '1453107503');
INSERT INTO `go_data_statistics` VALUES ('2624', '30', '开心看', 'zlkxk', '', '9550', '1453107683');
INSERT INTO `go_data_statistics` VALUES ('2625', '30', '开心看', 'zlkxk', '', '9551', '1453108027');
INSERT INTO `go_data_statistics` VALUES ('2626', '30', '开心看', 'zlkxk', '', '9552', '1453108279');
INSERT INTO `go_data_statistics` VALUES ('2627', '30', '开心看', 'zlkxk', '', '9553', '1453108669');
INSERT INTO `go_data_statistics` VALUES ('2628', '30', '开心看', 'zlkxk', '', '9554', '1453109141');
INSERT INTO `go_data_statistics` VALUES ('2629', '30', '开心看', 'zlkxk', '', '9555', '1453109458');
INSERT INTO `go_data_statistics` VALUES ('2630', '8', '一元众乐', 'yyzl', '', '9556', '1453109643');
INSERT INTO `go_data_statistics` VALUES ('2631', '19', '木蚂蚁', 'mumayi', '', '9557', '1453109923');
INSERT INTO `go_data_statistics` VALUES ('2632', '19', '木蚂蚁', 'mumayi', '', '9558', '1453110044');
INSERT INTO `go_data_statistics` VALUES ('2633', '30', '开心看', 'zlkxk', '', '9559', '1453110054');
INSERT INTO `go_data_statistics` VALUES ('2634', '6', '信号增强器', 'zlxhzqq', '', '9560', '1453111098');
INSERT INTO `go_data_statistics` VALUES ('2635', '8', '一元众乐', 'yyzl', '', '9561', '1453111422');
INSERT INTO `go_data_statistics` VALUES ('2636', '30', '开心看', 'zlkxk', '', '9562', '1453111533');
INSERT INTO `go_data_statistics` VALUES ('2637', '30', '开心看', 'zlkxk', '', '9563', '1453111579');
INSERT INTO `go_data_statistics` VALUES ('2638', '30', '开心看', 'zlkxk', '', '9565', '1453111705');
INSERT INTO `go_data_statistics` VALUES ('2639', '30', '开心看', 'zlkxk', '', '9566', '1453111905');
INSERT INTO `go_data_statistics` VALUES ('2640', '16', '豌豆荚', 'wandoujia', '', '9567', '1453112141');
INSERT INTO `go_data_statistics` VALUES ('2641', '30', '开心看', 'zlkxk', '', '9568', '1453112604');
INSERT INTO `go_data_statistics` VALUES ('2642', '30', '开心看', 'zlkxk', '', '9569', '1453113103');
INSERT INTO `go_data_statistics` VALUES ('2643', '8', '一元众乐', 'yyzl', '', '9572', '1453113201');
INSERT INTO `go_data_statistics` VALUES ('2644', '30', '开心看', 'zlkxk', '', '9573', '1453113506');
INSERT INTO `go_data_statistics` VALUES ('2645', '8', '一元众乐', 'yyzl', '', '9574', '1453113530');
INSERT INTO `go_data_statistics` VALUES ('2646', '30', '开心看', 'zlkxk', '', '9575', '1453113534');
INSERT INTO `go_data_statistics` VALUES ('2647', '8', '一元众乐', 'yyzl', '', '9576', '1453114662');
INSERT INTO `go_data_statistics` VALUES ('2648', '30', '开心看', 'zlkxk', '', '9577', '1453114975');
INSERT INTO `go_data_statistics` VALUES ('2649', '8', '一元众乐', 'yyzl', '', '9578', '1453114991');
INSERT INTO `go_data_statistics` VALUES ('2650', '30', '开心看', 'zlkxk', '', '9579', '1453115062');
INSERT INTO `go_data_statistics` VALUES ('2651', '19', '木蚂蚁', 'mumayi', '', '9580', '1453115251');
INSERT INTO `go_data_statistics` VALUES ('2652', '30', '开心看', 'zlkxk', '', '9581', '1453115582');
INSERT INTO `go_data_statistics` VALUES ('2653', '30', '开心看', 'zlkxk', '', '9582', '1453115844');
INSERT INTO `go_data_statistics` VALUES ('2654', '13', '安卓市场', 'androidshichang', '', '9583', '1453115992');
INSERT INTO `go_data_statistics` VALUES ('2655', '30', '开心看', 'zlkxk', '', '9584', '1453116045');
INSERT INTO `go_data_statistics` VALUES ('2656', '30', '开心看', 'zlkxk', '', '9585', '1453116147');
INSERT INTO `go_data_statistics` VALUES ('2657', '30', '开心看', 'zlkxk', '', '9586', '1453116671');
INSERT INTO `go_data_statistics` VALUES ('2658', '19', '木蚂蚁', 'mumayi', '', '9587', '1453116906');
INSERT INTO `go_data_statistics` VALUES ('2659', '30', '开心看', 'zlkxk', '', '9588', '1453117081');
INSERT INTO `go_data_statistics` VALUES ('2660', '8', '一元众乐', 'yyzl', '', '9589', '1453117109');
INSERT INTO `go_data_statistics` VALUES ('2661', '29', '小米开发者平台', 'xiaomi', '', '9590', '1453117235');
INSERT INTO `go_data_statistics` VALUES ('2662', '24', '魅族', 'meizu', '', '9591', '1453117806');
INSERT INTO `go_data_statistics` VALUES ('2663', '19', '木蚂蚁', 'mumayi', '', '9592', '1453117861');
INSERT INTO `go_data_statistics` VALUES ('2664', '8', '一元众乐', 'yyzl', '', '9593', '1453117909');
INSERT INTO `go_data_statistics` VALUES ('2665', '30', '开心看', 'zlkxk', '', '9594', '1453118575');
INSERT INTO `go_data_statistics` VALUES ('2666', '8', '一元众乐', 'yyzl', '', '9595', '1453119301');
INSERT INTO `go_data_statistics` VALUES ('2667', '8', '一元众乐', 'yyzl', '', '9596', '1453119654');
INSERT INTO `go_data_statistics` VALUES ('2668', '30', '开心看', 'zlkxk', '', '9597', '1453120112');
INSERT INTO `go_data_statistics` VALUES ('2669', '8', '一元众乐', 'yyzl', '', '9599', '1453120370');
INSERT INTO `go_data_statistics` VALUES ('2670', '30', '开心看', 'zlkxk', '', '9600', '1453120547');
INSERT INTO `go_data_statistics` VALUES ('2671', '19', '木蚂蚁', 'mumayi', '', '9601', '1453120705');
INSERT INTO `go_data_statistics` VALUES ('2672', '30', '开心看', 'zlkxk', '', '9602', '1453120878');
INSERT INTO `go_data_statistics` VALUES ('2673', '8', '一元众乐', 'yyzl', '', '9603', '1453121494');
INSERT INTO `go_data_statistics` VALUES ('2674', '8', '一元众乐', 'yyzl', '', '9604', '1453121573');
INSERT INTO `go_data_statistics` VALUES ('2675', '6', '信号增强器', 'zlxhzqq', '', '9605', '1453122228');
INSERT INTO `go_data_statistics` VALUES ('2676', '30', '开心看', 'zlkxk', '', '9606', '1453122829');
INSERT INTO `go_data_statistics` VALUES ('2677', '19', '木蚂蚁', 'mumayi', '', '9607', '1453123089');
INSERT INTO `go_data_statistics` VALUES ('2678', '30', '开心看', 'zlkxk', '', '9608', '1453123214');
INSERT INTO `go_data_statistics` VALUES ('2679', '15', '百度', 'baidu', '', '9609', '1453123432');
INSERT INTO `go_data_statistics` VALUES ('2680', '8', '一元众乐', 'yyzl', '', '9610', '1453123716');
INSERT INTO `go_data_statistics` VALUES ('2681', '8', '一元众乐', 'yyzl', '', '9613', '1453124603');
INSERT INTO `go_data_statistics` VALUES ('2682', '8', '一元众乐', 'yyzl', '', '9615', '1453124782');
INSERT INTO `go_data_statistics` VALUES ('2683', '8', '一元众乐', 'yyzl', '', '9616', '1453124986');
INSERT INTO `go_data_statistics` VALUES ('2684', '8', '一元众乐', 'yyzl', '', '9617', '1453125043');
INSERT INTO `go_data_statistics` VALUES ('2685', '14', '360手机助手', 'dev360', '', '9618', '1453125282');
INSERT INTO `go_data_statistics` VALUES ('2686', '8', '一元众乐', 'yyzl', '', '9619', '1453125982');
INSERT INTO `go_data_statistics` VALUES ('2687', '30', '开心看', 'zlkxk', '', '9620', '1453126496');
INSERT INTO `go_data_statistics` VALUES ('2688', '19', '木蚂蚁', 'mumayi', '', '9621', '1453126631');
INSERT INTO `go_data_statistics` VALUES ('2689', '6', '信号增强器', 'zlxhzqq', '', '9622', '1453126731');
INSERT INTO `go_data_statistics` VALUES ('2690', '8', '一元众乐', 'yyzl', '', '9623', '1453126896');
INSERT INTO `go_data_statistics` VALUES ('2691', '6', '信号增强器', 'zlxhzqq', '', '9624', '1453127181');
INSERT INTO `go_data_statistics` VALUES ('2692', '8', '一元众乐', 'yyzl', '', '9625', '1453127400');
INSERT INTO `go_data_statistics` VALUES ('2693', '30', '开心看', 'zlkxk', '', '9626', '1453127729');
INSERT INTO `go_data_statistics` VALUES ('2694', '19', '木蚂蚁', 'mumayi', '', '9627', '1453127945');
INSERT INTO `go_data_statistics` VALUES ('2695', '16', '豌豆荚', 'wandoujia', '', '9628', '1453128268');
INSERT INTO `go_data_statistics` VALUES ('2696', '8', '一元众乐', 'yyzl', '', '9629', '1453128891');
INSERT INTO `go_data_statistics` VALUES ('2697', '24', '魅族', 'meizu', '', '9631', '1453129380');
INSERT INTO `go_data_statistics` VALUES ('2698', '30', '开心看', 'zlkxk', '', '9632', '1453129811');
INSERT INTO `go_data_statistics` VALUES ('2699', '17', 'PP助手', 'pp', '', '9633', '1453130040');
INSERT INTO `go_data_statistics` VALUES ('2700', '30', '开心看', 'zlkxk', '', '9634', '1453130112');
INSERT INTO `go_data_statistics` VALUES ('2701', '30', '开心看', 'zlkxk', '', '9635', '1453130158');
INSERT INTO `go_data_statistics` VALUES ('2702', '8', '一元众乐', 'yyzl', '', '9636', '1453130242');
INSERT INTO `go_data_statistics` VALUES ('2703', '19', '木蚂蚁', 'mumayi', '', '9637', '1453130466');
INSERT INTO `go_data_statistics` VALUES ('2704', '8', '一元众乐', 'yyzl', '', '9638', '1453130811');
INSERT INTO `go_data_statistics` VALUES ('2705', '30', '开心看', 'zlkxk', '', '9639', '1453131084');
INSERT INTO `go_data_statistics` VALUES ('2706', '8', '一元众乐', 'yyzl', '', '9640', '1453131207');
INSERT INTO `go_data_statistics` VALUES ('2707', '8', '一元众乐', 'yyzl', '', '9641', '1453132456');
INSERT INTO `go_data_statistics` VALUES ('2708', '15', '百度', 'baidu', '', '9642', '1453132594');
INSERT INTO `go_data_statistics` VALUES ('2709', '30', '开心看', 'zlkxk', '', '9643', '1453132759');
INSERT INTO `go_data_statistics` VALUES ('2710', '14', '360手机助手', 'dev360', '', '9644', '1453132765');
INSERT INTO `go_data_statistics` VALUES ('2711', '8', '一元众乐', 'yyzl', '', '9645', '1453133173');
INSERT INTO `go_data_statistics` VALUES ('2712', '8', '一元众乐', 'yyzl', '', '9646', '1453135334');
INSERT INTO `go_data_statistics` VALUES ('2713', '19', '木蚂蚁', 'mumayi', '', '9647', '1453136632');
INSERT INTO `go_data_statistics` VALUES ('2714', '19', '木蚂蚁', 'mumayi', '', '9648', '1453143376');
INSERT INTO `go_data_statistics` VALUES ('2715', '6', '信号增强器', 'zlxhzqq', '', '9649', '1453146046');
INSERT INTO `go_data_statistics` VALUES ('2716', '6', '信号增强器', 'zlxhzqq', '', '9650', '1453148915');
INSERT INTO `go_data_statistics` VALUES ('2717', '8', '一元众乐', 'yyzl', '', '9651', '1453149817');
INSERT INTO `go_data_statistics` VALUES ('2718', '8', '一元众乐', 'yyzl', '', '9652', '1453155280');
INSERT INTO `go_data_statistics` VALUES ('2719', '8', '一元众乐', 'yyzl', '', '9653', '1453157248');
INSERT INTO `go_data_statistics` VALUES ('2720', '15', '百度', 'baidu', '', '9654', '1453158677');
INSERT INTO `go_data_statistics` VALUES ('2721', '30', '开心看', 'zlkxk', '', '9655', '1453159853');
INSERT INTO `go_data_statistics` VALUES ('2722', '8', '一元众乐', 'yyzl', '', '9662', '1453160132');
INSERT INTO `go_data_statistics` VALUES ('2723', '8', '一元众乐', 'yyzl', '', '9663', '1453160574');
INSERT INTO `go_data_statistics` VALUES ('2724', '8', '一元众乐', 'yyzl', '', '9664', '1453160620');
INSERT INTO `go_data_statistics` VALUES ('2725', '30', '开心看', 'zlkxk', '', '9665', '1453161081');
INSERT INTO `go_data_statistics` VALUES ('2726', '19', '木蚂蚁', 'mumayi', '', '9666', '1453161321');
INSERT INTO `go_data_statistics` VALUES ('2727', '14', '360手机助手', 'dev360', '', '9667', '1453161359');
INSERT INTO `go_data_statistics` VALUES ('2728', '8', '一元众乐', 'yyzl', '', '9668', '1453161423');
INSERT INTO `go_data_statistics` VALUES ('2729', '8', '一元众乐', 'yyzl', '', '9669', '1453161583');
INSERT INTO `go_data_statistics` VALUES ('2730', '29', '小米开发者平台', 'xiaomi', '', '9670', '1453161706');
INSERT INTO `go_data_statistics` VALUES ('2731', '8', '一元众乐', 'yyzl', '', '9671', '1453162321');
INSERT INTO `go_data_statistics` VALUES ('2732', '8', '一元众乐', 'yyzl', '', '9672', '1453162323');
INSERT INTO `go_data_statistics` VALUES ('2733', '14', '360手机助手', 'dev360', '', '9673', '1453163105');
INSERT INTO `go_data_statistics` VALUES ('2734', '19', '木蚂蚁', 'mumayi', '', '9674', '1453163204');
INSERT INTO `go_data_statistics` VALUES ('2735', '21', 'oppo', 'oppo', '', '9675', '1453163461');
INSERT INTO `go_data_statistics` VALUES ('2736', '16', '豌豆荚', 'wandoujia', '', '9677', '1453164219');
INSERT INTO `go_data_statistics` VALUES ('2737', '8', '一元众乐', 'yyzl', '', '9678', '1453164308');
INSERT INTO `go_data_statistics` VALUES ('2738', '8', '一元众乐', 'yyzl', '', '9679', '1453164574');
INSERT INTO `go_data_statistics` VALUES ('2739', '17', 'PP助手', 'pp', '', '9680', '1453164657');
INSERT INTO `go_data_statistics` VALUES ('2740', '17', 'PP助手', 'pp', '', '9681', '1453165566');
INSERT INTO `go_data_statistics` VALUES ('2741', '28', '安智开发者联盟', 'anzhi', '', '9682', '1453166171');
INSERT INTO `go_data_statistics` VALUES ('2742', '8', '一元众乐', 'yyzl', '', '9683', '1453166175');
INSERT INTO `go_data_statistics` VALUES ('2743', '19', '木蚂蚁', 'mumayi', '', '9684', '1453166491');
INSERT INTO `go_data_statistics` VALUES ('2744', '15', '百度', 'baidu', '', '9685', '1453166746');
INSERT INTO `go_data_statistics` VALUES ('2745', '8', '一元众乐', 'yyzl', '', '9686', '1453166890');
INSERT INTO `go_data_statistics` VALUES ('2746', '14', '360手机助手', 'dev360', '', '9687', '1453167076');
INSERT INTO `go_data_statistics` VALUES ('2747', '30', '开心看', 'zlkxk', '', '9688', '1453167341');
INSERT INTO `go_data_statistics` VALUES ('2748', '8', '一元众乐', 'yyzl', '', '9689', '1453167653');
INSERT INTO `go_data_statistics` VALUES ('2749', '8', '一元众乐', 'yyzl', '', '9690', '1453167788');
INSERT INTO `go_data_statistics` VALUES ('2750', '19', '木蚂蚁', 'mumayi', '', '9691', '1453167959');
INSERT INTO `go_data_statistics` VALUES ('2751', '8', '一元众乐', 'yyzl', '', '9692', '1453168345');
INSERT INTO `go_data_statistics` VALUES ('2752', '8', '一元众乐', 'yyzl', '', '9693', '1453168525');
INSERT INTO `go_data_statistics` VALUES ('2753', '8', '一元众乐', 'yyzl', '', '9694', '1453168525');
INSERT INTO `go_data_statistics` VALUES ('2754', '30', '开心看', 'zlkxk', '', '9695', '1453168796');
INSERT INTO `go_data_statistics` VALUES ('2755', '30', '开心看', 'zlkxk', '', '9696', '1453168900');
INSERT INTO `go_data_statistics` VALUES ('2756', '8', '一元众乐', 'yyzl', '', '9698', '1453169975');
INSERT INTO `go_data_statistics` VALUES ('2757', '29', '小米开发者平台', 'xiaomi', '', '9699', '1453170033');
INSERT INTO `go_data_statistics` VALUES ('2758', '8', '一元众乐', 'yyzl', '', '9700', '1453170046');
INSERT INTO `go_data_statistics` VALUES ('2759', '21', 'oppo', 'oppo', '', '9701', '1453170098');
INSERT INTO `go_data_statistics` VALUES ('2760', '30', '开心看', 'zlkxk', '', '9702', '1453170143');
INSERT INTO `go_data_statistics` VALUES ('2761', '15', '百度', 'baidu', '', '9703', '1453170812');
INSERT INTO `go_data_statistics` VALUES ('2762', '19', '木蚂蚁', 'mumayi', '', '9704', '1453171075');
INSERT INTO `go_data_statistics` VALUES ('2763', '8', '一元众乐', 'yyzl', '', '9705', '1453171276');
INSERT INTO `go_data_statistics` VALUES ('2764', '8', '一元众乐', 'yyzl', '', '9706', '1453171323');
INSERT INTO `go_data_statistics` VALUES ('2765', '19', '木蚂蚁', 'mumayi', '', '9707', '1453171711');
INSERT INTO `go_data_statistics` VALUES ('2766', '30', '开心看', 'zlkxk', '', '9708', '1453171955');
INSERT INTO `go_data_statistics` VALUES ('2767', '30', '开心看', 'zlkxk', '', '9709', '1453172059');
INSERT INTO `go_data_statistics` VALUES ('2768', '8', '一元众乐', 'yyzl', '', '9710', '1453172149');
INSERT INTO `go_data_statistics` VALUES ('2769', '30', '开心看', 'zlkxk', '', '9711', '1453172459');
INSERT INTO `go_data_statistics` VALUES ('2770', '30', '开心看', 'zlkxk', '', '9713', '1453172641');
INSERT INTO `go_data_statistics` VALUES ('2771', '8', '一元众乐', 'yyzl', '', '9715', '1453172948');
INSERT INTO `go_data_statistics` VALUES ('2772', '8', '一元众乐', 'yyzl', '', '9716', '1453173402');
INSERT INTO `go_data_statistics` VALUES ('2773', '19', '木蚂蚁', 'mumayi', '', '9717', '1453173683');
INSERT INTO `go_data_statistics` VALUES ('2774', '30', '开心看', 'zlkxk', '', '9718', '1453173958');
INSERT INTO `go_data_statistics` VALUES ('2775', '8', '一元众乐', 'yyzl', '', '9719', '1453173975');
INSERT INTO `go_data_statistics` VALUES ('2776', '8', '一元众乐', 'yyzl', '', '9721', '1453174212');
INSERT INTO `go_data_statistics` VALUES ('2777', '30', '开心看', 'zlkxk', '', '9723', '1453174336');
INSERT INTO `go_data_statistics` VALUES ('2778', '8', '一元众乐', 'yyzl', '', '9724', '1453174506');
INSERT INTO `go_data_statistics` VALUES ('2779', '30', '开心看', 'zlkxk', '', '9725', '1453174514');
INSERT INTO `go_data_statistics` VALUES ('2780', '30', '开心看', 'zlkxk', '', '9727', '1453175020');
INSERT INTO `go_data_statistics` VALUES ('2781', '8', '一元众乐', 'yyzl', '', '9728', '1453175059');
INSERT INTO `go_data_statistics` VALUES ('2782', '8', '一元众乐', 'yyzl', '', '9729', '1453175288');
INSERT INTO `go_data_statistics` VALUES ('2783', '19', '木蚂蚁', 'mumayi', '', '9730', '1453175385');
INSERT INTO `go_data_statistics` VALUES ('2784', '8', '一元众乐', 'yyzl', '', '9732', '1453175638');
INSERT INTO `go_data_statistics` VALUES ('2785', '30', '开心看', 'zlkxk', '', '9733', '1453175649');
INSERT INTO `go_data_statistics` VALUES ('2786', '19', '木蚂蚁', 'mumayi', '', '9734', '1453175804');
INSERT INTO `go_data_statistics` VALUES ('2787', '17', 'PP助手', 'pp', '', '9735', '1453176203');
INSERT INTO `go_data_statistics` VALUES ('2788', '16', '豌豆荚', 'wandoujia', '', '9736', '1453176261');
INSERT INTO `go_data_statistics` VALUES ('2789', '29', '小米开发者平台', 'xiaomi', '', '9737', '1453176305');
INSERT INTO `go_data_statistics` VALUES ('2790', '15', '百度', 'baidu', '', '9738', '1453177101');
INSERT INTO `go_data_statistics` VALUES ('2791', '8', '一元众乐', 'yyzl', '', '9739', '1453177419');
INSERT INTO `go_data_statistics` VALUES ('2792', '19', '木蚂蚁', 'mumayi', '', '9740', '1453177436');
INSERT INTO `go_data_statistics` VALUES ('2793', '30', '开心看', 'zlkxk', '', '9741', '1453177593');
INSERT INTO `go_data_statistics` VALUES ('2794', '30', '开心看', 'zlkxk', '', '9742', '1453177799');
INSERT INTO `go_data_statistics` VALUES ('2795', '19', '木蚂蚁', 'mumayi', '', '9743', '1453177984');
INSERT INTO `go_data_statistics` VALUES ('2796', '20', '联想', 'lenovo', '', '9744', '1453178464');
INSERT INTO `go_data_statistics` VALUES ('2797', '8', '一元众乐', 'yyzl', '', '9745', '1453178527');
INSERT INTO `go_data_statistics` VALUES ('2798', '30', '开心看', 'zlkxk', '', '9750', '1453179343');
INSERT INTO `go_data_statistics` VALUES ('2799', '30', '开心看', 'zlkxk', '', '9751', '1453179346');
INSERT INTO `go_data_statistics` VALUES ('2800', '8', '一元众乐', 'yyzl', '', '9752', '1453179506');
INSERT INTO `go_data_statistics` VALUES ('2801', '8', '一元众乐', 'yyzl', '', '9753', '1453179532');
INSERT INTO `go_data_statistics` VALUES ('2802', '8', '一元众乐', 'yyzl', '', '9754', '1453179863');
INSERT INTO `go_data_statistics` VALUES ('2803', '8', '一元众乐', 'yyzl', '', '9755', '1453179891');
INSERT INTO `go_data_statistics` VALUES ('2804', '30', '开心看', 'zlkxk', '', '9756', '1453179976');
INSERT INTO `go_data_statistics` VALUES ('2805', '8', '一元众乐', 'yyzl', '', '9758', '1453180151');
INSERT INTO `go_data_statistics` VALUES ('2806', '30', '开心看', 'zlkxk', '', '9759', '1453180830');
INSERT INTO `go_data_statistics` VALUES ('2807', '30', '开心看', 'zlkxk', '', '9761', '1453181194');
INSERT INTO `go_data_statistics` VALUES ('2808', '8', '一元众乐', 'yyzl', '', '9762', '1453181570');
INSERT INTO `go_data_statistics` VALUES ('2809', '8', '一元众乐', 'yyzl', '', '9763', '1453181659');
INSERT INTO `go_data_statistics` VALUES ('2810', '30', '开心看', 'zlkxk', '', '9764', '1453181783');
INSERT INTO `go_data_statistics` VALUES ('2811', '30', '开心看', 'zlkxk', '', '9765', '1453182069');
INSERT INTO `go_data_statistics` VALUES ('2812', '30', '开心看', 'zlkxk', '', '9766', '1453182218');
INSERT INTO `go_data_statistics` VALUES ('2813', '17', 'PP助手', 'pp', '', '9767', '1453182485');
INSERT INTO `go_data_statistics` VALUES ('2814', '8', '一元众乐', 'yyzl', '', '9768', '1453182716');
INSERT INTO `go_data_statistics` VALUES ('2815', '30', '开心看', 'zlkxk', '', '9769', '1453182894');
INSERT INTO `go_data_statistics` VALUES ('2816', '30', '开心看', 'zlkxk', '', '9770', '1453183153');
INSERT INTO `go_data_statistics` VALUES ('2817', '8', '一元众乐', 'yyzl', '', '9771', '1453183321');
INSERT INTO `go_data_statistics` VALUES ('2818', '8', '一元众乐', 'yyzl', '', '9773', '1453183788');
INSERT INTO `go_data_statistics` VALUES ('2819', '8', '一元众乐', 'yyzl', '', '9774', '1453184283');
INSERT INTO `go_data_statistics` VALUES ('2820', '30', '开心看', 'zlkxk', '', '9775', '1453184317');
INSERT INTO `go_data_statistics` VALUES ('2821', '8', '一元众乐', 'yyzl', '', '9776', '1453184664');
INSERT INTO `go_data_statistics` VALUES ('2822', '19', '木蚂蚁', 'mumayi', '', '9777', '1453184740');
INSERT INTO `go_data_statistics` VALUES ('2823', '17', 'PP助手', 'pp', '', '9778', '1453184914');
INSERT INTO `go_data_statistics` VALUES ('2824', '30', '开心看', 'zlkxk', '', '9779', '1453185000');
INSERT INTO `go_data_statistics` VALUES ('2825', '8', '一元众乐', 'yyzl', '', '9780', '1453185177');
INSERT INTO `go_data_statistics` VALUES ('2826', '30', '开心看', 'zlkxk', '', '9781', '1453185285');
INSERT INTO `go_data_statistics` VALUES ('2827', '30', '开心看', 'zlkxk', '', '9783', '1453185436');
INSERT INTO `go_data_statistics` VALUES ('2828', '8', '一元众乐', 'yyzl', '', '9784', '1453185439');
INSERT INTO `go_data_statistics` VALUES ('2829', '21', 'oppo', 'oppo', '', '9786', '1453185580');
INSERT INTO `go_data_statistics` VALUES ('2830', '30', '开心看', 'zlkxk', '', '9787', '1453185607');
INSERT INTO `go_data_statistics` VALUES ('2831', '30', '开心看', 'zlkxk', '', '9788', '1453185634');
INSERT INTO `go_data_statistics` VALUES ('2832', '6', '信号增强器', 'zlxhzqq', '', '9789', '1453185761');
INSERT INTO `go_data_statistics` VALUES ('2833', '8', '一元众乐', 'yyzl', '', '9795', '1453186280');
INSERT INTO `go_data_statistics` VALUES ('2834', '29', '小米开发者平台', 'xiaomi', '', '9796', '1453186409');
INSERT INTO `go_data_statistics` VALUES ('2835', '30', '开心看', 'zlkxk', '', '9797', '1453186522');
INSERT INTO `go_data_statistics` VALUES ('2836', '30', '开心看', 'zlkxk', '', '9799', '1453186594');
INSERT INTO `go_data_statistics` VALUES ('2837', '8', '一元众乐', 'yyzl', '', '9800', '1453186955');
INSERT INTO `go_data_statistics` VALUES ('2838', '8', '一元众乐', 'yyzl', '', '9801', '1453187192');
INSERT INTO `go_data_statistics` VALUES ('2839', '8', '一元众乐', 'yyzl', '', '9802', '1453187820');
INSERT INTO `go_data_statistics` VALUES ('2840', '19', '木蚂蚁', 'mumayi', '', '9803', '1453187922');
INSERT INTO `go_data_statistics` VALUES ('2841', '8', '一元众乐', 'yyzl', '', '9804', '1453188005');
INSERT INTO `go_data_statistics` VALUES ('2842', '6', '信号增强器', 'zlxhzqq', '', '9806', '1453188691');
INSERT INTO `go_data_statistics` VALUES ('2843', '6', '信号增强器', 'zlxhzqq', '', '9807', '1453188716');
INSERT INTO `go_data_statistics` VALUES ('2844', '30', '开心看', 'zlkxk', '', '9808', '1453188746');
INSERT INTO `go_data_statistics` VALUES ('2845', '8', '一元众乐', 'yyzl', '', '9809', '1453188906');
INSERT INTO `go_data_statistics` VALUES ('2846', '30', '开心看', 'zlkxk', '', '9810', '1453189254');
INSERT INTO `go_data_statistics` VALUES ('2847', '19', '木蚂蚁', 'mumayi', '', '9811', '1453189316');
INSERT INTO `go_data_statistics` VALUES ('2848', '8', '一元众乐', 'yyzl', '', '9812', '1453189371');
INSERT INTO `go_data_statistics` VALUES ('2849', '8', '一元众乐', 'yyzl', '', '9814', '1453189569');
INSERT INTO `go_data_statistics` VALUES ('2850', '8', '一元众乐', 'yyzl', '', '9815', '1453189709');
INSERT INTO `go_data_statistics` VALUES ('2851', '30', '开心看', 'zlkxk', '', '9816', '1453189857');
INSERT INTO `go_data_statistics` VALUES ('2852', '15', '百度', 'baidu', '', '9817', '1453190122');
INSERT INTO `go_data_statistics` VALUES ('2853', '16', '豌豆荚', 'wandoujia', '', '9818', '1453190208');
INSERT INTO `go_data_statistics` VALUES ('2854', '8', '一元众乐', 'yyzl', '', '9819', '1453190395');
INSERT INTO `go_data_statistics` VALUES ('2855', '30', '开心看', 'zlkxk', '', '9820', '1453190511');
INSERT INTO `go_data_statistics` VALUES ('2856', '19', '木蚂蚁', 'mumayi', '', '9821', '1453190557');
INSERT INTO `go_data_statistics` VALUES ('2857', '6', '信号增强器', 'zlxhzqq', '', '9822', '1453190588');
INSERT INTO `go_data_statistics` VALUES ('2858', '8', '一元众乐', 'yyzl', '', '9823', '1453190630');
INSERT INTO `go_data_statistics` VALUES ('2859', '6', '信号增强器', 'zlxhzqq', '', '9824', '1453190654');
INSERT INTO `go_data_statistics` VALUES ('2860', '8', '一元众乐', 'yyzl', '', '9825', '1453190860');
INSERT INTO `go_data_statistics` VALUES ('2861', '8', '一元众乐', 'yyzl', '', '9826', '1453190930');
INSERT INTO `go_data_statistics` VALUES ('2862', '14', '360手机助手', 'dev360', '', '9828', '1453191857');
INSERT INTO `go_data_statistics` VALUES ('2863', '8', '一元众乐', 'yyzl', '', '9829', '1453191883');
INSERT INTO `go_data_statistics` VALUES ('2864', '8', '一元众乐', 'yyzl', '', '9830', '1453191918');
INSERT INTO `go_data_statistics` VALUES ('2865', '8', '一元众乐', 'yyzl', '', '9831', '1453192066');
INSERT INTO `go_data_statistics` VALUES ('2866', '17', 'PP助手', 'pp', '', '9832', '1453192142');
INSERT INTO `go_data_statistics` VALUES ('2867', '8', '一元众乐', 'yyzl', '', '9836', '1453192485');
INSERT INTO `go_data_statistics` VALUES ('2868', '19', '木蚂蚁', 'mumayi', '', '9837', '1453192648');
INSERT INTO `go_data_statistics` VALUES ('2869', '8', '一元众乐', 'yyzl', '', '9838', '1453192653');
INSERT INTO `go_data_statistics` VALUES ('2870', '6', '信号增强器', 'zlxhzqq', '', '9841', '1453193148');
INSERT INTO `go_data_statistics` VALUES ('2871', '19', '木蚂蚁', 'mumayi', '', '9842', '1453193296');
INSERT INTO `go_data_statistics` VALUES ('2872', '8', '一元众乐', 'yyzl', '', '9843', '1453193539');
INSERT INTO `go_data_statistics` VALUES ('2873', '17', 'PP助手', 'pp', '', '9844', '1453194138');
INSERT INTO `go_data_statistics` VALUES ('2874', '8', '一元众乐', 'yyzl', '', '9845', '1453194267');
INSERT INTO `go_data_statistics` VALUES ('2875', '8', '一元众乐', 'yyzl', '', '9846', '1453194366');
INSERT INTO `go_data_statistics` VALUES ('2876', '19', '木蚂蚁', 'mumayi', '', '9847', '1453195221');
INSERT INTO `go_data_statistics` VALUES ('2877', '8', '一元众乐', 'yyzl', '', '9848', '1453195247');
INSERT INTO `go_data_statistics` VALUES ('2878', '30', '开心看', 'zlkxk', '', '9849', '1453195344');
INSERT INTO `go_data_statistics` VALUES ('2879', '30', '开心看', 'zlkxk', '', '9850', '1453195723');
INSERT INTO `go_data_statistics` VALUES ('2880', '8', '一元众乐', 'yyzl', '', '9851', '1453195739');
INSERT INTO `go_data_statistics` VALUES ('2881', '19', '木蚂蚁', 'mumayi', '', '9852', '1453195969');
INSERT INTO `go_data_statistics` VALUES ('2882', '8', '一元众乐', 'yyzl', '', '9853', '1453196436');
INSERT INTO `go_data_statistics` VALUES ('2883', '30', '开心看', 'zlkxk', '', '9854', '1453197278');
INSERT INTO `go_data_statistics` VALUES ('2884', '19', '木蚂蚁', 'mumayi', '', '9855', '1453197398');
INSERT INTO `go_data_statistics` VALUES ('2885', '19', '木蚂蚁', 'mumayi', '', '9856', '1453197432');
INSERT INTO `go_data_statistics` VALUES ('2886', '30', '开心看', 'zlkxk', '', '9857', '1453197464');
INSERT INTO `go_data_statistics` VALUES ('2887', '8', '一元众乐', 'yyzl', '', '9858', '1453197854');
INSERT INTO `go_data_statistics` VALUES ('2888', '6', '信号增强器', 'zlxhzqq', '', '9859', '1453198138');
INSERT INTO `go_data_statistics` VALUES ('2889', '8', '一元众乐', 'yyzl', '', '9860', '1453198330');
INSERT INTO `go_data_statistics` VALUES ('2890', '30', '开心看', 'zlkxk', '', '9861', '1453198352');
INSERT INTO `go_data_statistics` VALUES ('2891', '19', '木蚂蚁', 'mumayi', '', '9862', '1453198845');
INSERT INTO `go_data_statistics` VALUES ('2892', '21', 'oppo', 'oppo', '', '9863', '1453199215');
INSERT INTO `go_data_statistics` VALUES ('2893', '8', '一元众乐', 'yyzl', '', '9864', '1453199267');
INSERT INTO `go_data_statistics` VALUES ('2894', '30', '开心看', 'zlkxk', '', '9865', '1453199708');
INSERT INTO `go_data_statistics` VALUES ('2895', '16', '豌豆荚', 'wandoujia', '', '9866', '1453199784');
INSERT INTO `go_data_statistics` VALUES ('2896', '14', '360手机助手', 'dev360', '', '9867', '1453199849');
INSERT INTO `go_data_statistics` VALUES ('2897', '15', '百度', 'baidu', '', '9868', '1453200046');
INSERT INTO `go_data_statistics` VALUES ('2898', '17', 'PP助手', 'pp', '', '9869', '1453200269');
INSERT INTO `go_data_statistics` VALUES ('2899', '15', '百度', 'baidu', '', '9870', '1453200321');
INSERT INTO `go_data_statistics` VALUES ('2900', '17', 'PP助手', 'pp', '', '9871', '1453200645');
INSERT INTO `go_data_statistics` VALUES ('2901', '6', '信号增强器', 'zlxhzqq', '', '9872', '1453200834');
INSERT INTO `go_data_statistics` VALUES ('2902', '20', '联想', 'lenovo', '', '9873', '1453201146');
INSERT INTO `go_data_statistics` VALUES ('2903', '19', '木蚂蚁', 'mumayi', '', '9874', '1453201198');
INSERT INTO `go_data_statistics` VALUES ('2904', '8', '一元众乐', 'yyzl', '', '9875', '1453201296');
INSERT INTO `go_data_statistics` VALUES ('2905', '19', '木蚂蚁', 'mumayi', '', '9876', '1453201304');
INSERT INTO `go_data_statistics` VALUES ('2906', '17', 'PP助手', 'pp', '', '9877', '1453201371');
INSERT INTO `go_data_statistics` VALUES ('2907', '21', 'oppo', 'oppo', '', '9878', '1453201407');
INSERT INTO `go_data_statistics` VALUES ('2908', '8', '一元众乐', 'yyzl', '', '9880', '1453201661');
INSERT INTO `go_data_statistics` VALUES ('2909', '8', '一元众乐', 'yyzl', '', '9881', '1453201705');
INSERT INTO `go_data_statistics` VALUES ('2910', '8', '一元众乐', 'yyzl', '', '9882', '1453202074');
INSERT INTO `go_data_statistics` VALUES ('2911', '8', '一元众乐', 'yyzl', '', '9883', '1453202381');
INSERT INTO `go_data_statistics` VALUES ('2912', '14', '360手机助手', 'dev360', '', '9884', '1453202420');
INSERT INTO `go_data_statistics` VALUES ('2913', '8', '一元众乐', 'yyzl', '', '9885', '1453202436');
INSERT INTO `go_data_statistics` VALUES ('2914', '6', '信号增强器', 'zlxhzqq', '', '9886', '1453202633');
INSERT INTO `go_data_statistics` VALUES ('2915', '8', '一元众乐', 'yyzl', '', '9887', '1453202710');
INSERT INTO `go_data_statistics` VALUES ('2916', '8', '一元众乐', 'yyzl', '', '9888', '1453202748');
INSERT INTO `go_data_statistics` VALUES ('2917', '8', '一元众乐', 'yyzl', '', '9889', '1453202889');
INSERT INTO `go_data_statistics` VALUES ('2918', '8', '一元众乐', 'yyzl', '', '9890', '1453202933');
INSERT INTO `go_data_statistics` VALUES ('2919', '30', '开心看', 'zlkxk', '', '9891', '1453202949');
INSERT INTO `go_data_statistics` VALUES ('2920', '24', '魅族', 'meizu', '', '9892', '1453202992');
INSERT INTO `go_data_statistics` VALUES ('2921', '17', 'PP助手', 'pp', '', '9893', '1453203146');
INSERT INTO `go_data_statistics` VALUES ('2922', '8', '一元众乐', 'yyzl', '', '9894', '1453203177');
INSERT INTO `go_data_statistics` VALUES ('2923', '8', '一元众乐', 'yyzl', '', '9895', '1453203177');
INSERT INTO `go_data_statistics` VALUES ('2924', '8', '一元众乐', 'yyzl', '', '9896', '1453203177');
INSERT INTO `go_data_statistics` VALUES ('2925', '8', '一元众乐', 'yyzl', '', '9897', '1453203536');
INSERT INTO `go_data_statistics` VALUES ('2926', '30', '开心看', 'zlkxk', '', '9898', '1453203691');
INSERT INTO `go_data_statistics` VALUES ('2927', '17', 'PP助手', 'pp', '', '9899', '1453203872');
INSERT INTO `go_data_statistics` VALUES ('2928', '8', '一元众乐', 'yyzl', '', '9900', '1453203903');
INSERT INTO `go_data_statistics` VALUES ('2929', '30', '开心看', 'zlkxk', '', '9901', '1453204234');
INSERT INTO `go_data_statistics` VALUES ('2930', '30', '开心看', 'zlkxk', '', '9902', '1453204260');
INSERT INTO `go_data_statistics` VALUES ('2931', '8', '一元众乐', 'yyzl', '', '9903', '1453204576');
INSERT INTO `go_data_statistics` VALUES ('2932', '30', '开心看', 'zlkxk', '', '9905', '1453204768');
INSERT INTO `go_data_statistics` VALUES ('2933', '14', '360手机助手', 'dev360', '', '9906', '1453204839');
INSERT INTO `go_data_statistics` VALUES ('2934', '8', '一元众乐', 'yyzl', '', '9907', '1453204940');
INSERT INTO `go_data_statistics` VALUES ('2935', '30', '开心看', 'zlkxk', '', '9909', '1453205140');
INSERT INTO `go_data_statistics` VALUES ('2936', '6', '信号增强器', 'zlxhzqq', '', '9910', '1453205387');
INSERT INTO `go_data_statistics` VALUES ('2937', '14', '360手机助手', 'dev360', '', '9911', '1453205640');
INSERT INTO `go_data_statistics` VALUES ('2938', '8', '一元众乐', 'yyzl', '', '9912', '1453206019');
INSERT INTO `go_data_statistics` VALUES ('2939', '8', '一元众乐', 'yyzl', '', '9913', '1453206265');
INSERT INTO `go_data_statistics` VALUES ('2940', '17', 'PP助手', 'pp', '', '9914', '1453206429');
INSERT INTO `go_data_statistics` VALUES ('2941', '17', 'PP助手', 'pp', '', '9915', '1453206434');
INSERT INTO `go_data_statistics` VALUES ('2942', '17', 'PP助手', 'pp', '', '9916', '1453206436');
INSERT INTO `go_data_statistics` VALUES ('2943', '30', '开心看', 'zlkxk', '', '9917', '1453206539');
INSERT INTO `go_data_statistics` VALUES ('2944', '8', '一元众乐', 'yyzl', '', '9918', '1453206551');
INSERT INTO `go_data_statistics` VALUES ('2945', '19', '木蚂蚁', 'mumayi', '', '9919', '1453207380');
INSERT INTO `go_data_statistics` VALUES ('2946', '30', '开心看', 'zlkxk', '', '9923', '1453207433');
INSERT INTO `go_data_statistics` VALUES ('2947', '8', '一元众乐', 'yyzl', '', '9924', '1453207487');
INSERT INTO `go_data_statistics` VALUES ('2948', '17', 'PP助手', 'pp', '', '9925', '1453207545');
INSERT INTO `go_data_statistics` VALUES ('2949', '8', '一元众乐', 'yyzl', '', '9926', '1453208117');
INSERT INTO `go_data_statistics` VALUES ('2950', '15', '百度', 'baidu', '', '9927', '1453208264');
INSERT INTO `go_data_statistics` VALUES ('2951', '8', '一元众乐', 'yyzl', '', '9928', '1453208458');
INSERT INTO `go_data_statistics` VALUES ('2952', '8', '一元众乐', 'yyzl', '', '9929', '1453208577');
INSERT INTO `go_data_statistics` VALUES ('2953', '30', '开心看', 'zlkxk', '', '9930', '1453208622');
INSERT INTO `go_data_statistics` VALUES ('2954', '6', '信号增强器', 'zlxhzqq', '', '9931', '1453209435');
INSERT INTO `go_data_statistics` VALUES ('2955', '15', '百度', 'baidu', '', '9932', '1453209724');
INSERT INTO `go_data_statistics` VALUES ('2956', '17', 'PP助手', 'pp', '', '9933', '1453209752');
INSERT INTO `go_data_statistics` VALUES ('2957', '30', '开心看', 'zlkxk', '', '9935', '1453210411');
INSERT INTO `go_data_statistics` VALUES ('2958', '20', '联想', 'lenovo', '', '9936', '1453210764');
INSERT INTO `go_data_statistics` VALUES ('2959', '8', '一元众乐', 'yyzl', '', '9937', '1453210978');
INSERT INTO `go_data_statistics` VALUES ('2960', '8', '一元众乐', 'yyzl', '', '9938', '1453211006');
INSERT INTO `go_data_statistics` VALUES ('2961', '8', '一元众乐', 'yyzl', '', '9939', '1453211462');
INSERT INTO `go_data_statistics` VALUES ('2962', '8', '一元众乐', 'yyzl', '', '9941', '1453211666');
INSERT INTO `go_data_statistics` VALUES ('2963', '24', '魅族', 'meizu', '', '9942', '1453211828');
INSERT INTO `go_data_statistics` VALUES ('2964', '8', '一元众乐', 'yyzl', '', '9943', '1453212054');
INSERT INTO `go_data_statistics` VALUES ('2965', '8', '一元众乐', 'yyzl', '', '9944', '1453212556');
INSERT INTO `go_data_statistics` VALUES ('2966', '30', '开心看', 'zlkxk', '', '9945', '1453212802');
INSERT INTO `go_data_statistics` VALUES ('2967', '8', '一元众乐', 'yyzl', '', '9946', '1453213066');
INSERT INTO `go_data_statistics` VALUES ('2968', '6', '信号增强器', 'zlxhzqq', '', '9947', '1453213216');
INSERT INTO `go_data_statistics` VALUES ('2969', '8', '一元众乐', 'yyzl', '', '9948', '1453213339');
INSERT INTO `go_data_statistics` VALUES ('2970', '30', '开心看', 'zlkxk', '', '9950', '1453213679');
INSERT INTO `go_data_statistics` VALUES ('2971', '8', '一元众乐', 'yyzl', '', '9951', '1453213728');
INSERT INTO `go_data_statistics` VALUES ('2972', '14', '360手机助手', 'dev360', '', '9952', '1453213743');
INSERT INTO `go_data_statistics` VALUES ('2973', '30', '开心看', 'zlkxk', '', '9953', '1453213759');
INSERT INTO `go_data_statistics` VALUES ('2974', '17', 'PP助手', 'pp', '', '9954', '1453213882');
INSERT INTO `go_data_statistics` VALUES ('2975', '30', '开心看', 'zlkxk', '', '9956', '1453214214');
INSERT INTO `go_data_statistics` VALUES ('2976', '30', '开心看', 'zlkxk', '', '9957', '1453214379');
INSERT INTO `go_data_statistics` VALUES ('2977', '30', '开心看', 'zlkxk', '', '9958', '1453214755');
INSERT INTO `go_data_statistics` VALUES ('2978', '8', '一元众乐', 'yyzl', '', '9959', '1453214905');
INSERT INTO `go_data_statistics` VALUES ('2979', '8', '一元众乐', 'yyzl', '', '9960', '1453214920');
INSERT INTO `go_data_statistics` VALUES ('2980', '6', '信号增强器', 'zlxhzqq', '', '9961', '1453215182');
INSERT INTO `go_data_statistics` VALUES ('2981', '30', '开心看', 'zlkxk', '', '9962', '1453215373');
INSERT INTO `go_data_statistics` VALUES ('2982', '30', '开心看', 'zlkxk', '', '9963', '1453215410');
INSERT INTO `go_data_statistics` VALUES ('2983', '8', '一元众乐', 'yyzl', '', '9964', '1453215415');
INSERT INTO `go_data_statistics` VALUES ('2984', '30', '开心看', 'zlkxk', '', '9965', '1453216450');
INSERT INTO `go_data_statistics` VALUES ('2985', '8', '一元众乐', 'yyzl', '', '9966', '1453216459');
INSERT INTO `go_data_statistics` VALUES ('2986', '8', '一元众乐', 'yyzl', '', '9967', '1453216831');
INSERT INTO `go_data_statistics` VALUES ('2987', '30', '开心看', 'zlkxk', '', '9968', '1453217026');
INSERT INTO `go_data_statistics` VALUES ('2988', '8', '一元众乐', 'yyzl', '', '9969', '1453217091');
INSERT INTO `go_data_statistics` VALUES ('2989', '6', '信号增强器', 'zlxhzqq', '', '9970', '1453217238');
INSERT INTO `go_data_statistics` VALUES ('2990', '15', '百度', 'baidu', '', '9971', '1453217707');
INSERT INTO `go_data_statistics` VALUES ('2991', '6', '信号增强器', 'zlxhzqq', '', '9972', '1453217878');
INSERT INTO `go_data_statistics` VALUES ('2992', '30', '开心看', 'zlkxk', '', '9973', '1453217910');
INSERT INTO `go_data_statistics` VALUES ('2993', '8', '一元众乐', 'yyzl', '', '9974', '1453218716');
INSERT INTO `go_data_statistics` VALUES ('2994', '21', 'oppo', 'oppo', '', '9975', '1453220209');
INSERT INTO `go_data_statistics` VALUES ('2995', '6', '信号增强器', 'zlxhzqq', '', '9976', '1453220273');
INSERT INTO `go_data_statistics` VALUES ('2996', '17', 'PP助手', 'pp', '', '9977', '1453221094');
INSERT INTO `go_data_statistics` VALUES ('2997', '30', '开心看', 'zlkxk', '', '9978', '1453221937');
INSERT INTO `go_data_statistics` VALUES ('2998', '21', 'oppo', 'oppo', '', '9979', '1453222540');
INSERT INTO `go_data_statistics` VALUES ('2999', '30', '开心看', 'zlkxk', '', '9980', '1453224612');
INSERT INTO `go_data_statistics` VALUES ('3000', '8', '一元众乐', 'yyzl', '', '9981', '1453224666');
INSERT INTO `go_data_statistics` VALUES ('3001', '6', '信号增强器', 'zlxhzqq', '', '9982', '1453227309');
INSERT INTO `go_data_statistics` VALUES ('3002', '8', '一元众乐', 'yyzl', '', '9983', '1453227600');
INSERT INTO `go_data_statistics` VALUES ('3003', '20', '联想', 'lenovo', '', '9985', '1453233924');
INSERT INTO `go_data_statistics` VALUES ('3004', '8', '一元众乐', 'yyzl', '', '9986', '1453234443');
INSERT INTO `go_data_statistics` VALUES ('3005', '8', '一元众乐', 'yyzl', '', '9987', '1453236713');
INSERT INTO `go_data_statistics` VALUES ('3006', '8', '一元众乐', 'yyzl', '', '9988', '1453240909');
INSERT INTO `go_data_statistics` VALUES ('3007', '30', '开心看', 'zlkxk', '', '9989', '1453241534');
INSERT INTO `go_data_statistics` VALUES ('3008', '8', '一元众乐', 'yyzl', '', '9990', '1453243561');
INSERT INTO `go_data_statistics` VALUES ('3009', '15', '百度', 'baidu', '', '9991', '1453245718');
INSERT INTO `go_data_statistics` VALUES ('3010', '6', '信号增强器', 'zlxhzqq', '', '9992', '1453247222');
INSERT INTO `go_data_statistics` VALUES ('3011', '19', '木蚂蚁', 'mumayi', '', '9993', '1453247741');
INSERT INTO `go_data_statistics` VALUES ('3012', '19', '木蚂蚁', 'mumayi', '', '9994', '1453247785');
INSERT INTO `go_data_statistics` VALUES ('3013', '6', '信号增强器', 'zlxhzqq', '', '9995', '1453248605');
INSERT INTO `go_data_statistics` VALUES ('3014', '14', '360手机助手', 'dev360', '', '9996', '1453248997');
INSERT INTO `go_data_statistics` VALUES ('3015', '14', '360手机助手', 'dev360', '', '9997', '1453249394');
INSERT INTO `go_data_statistics` VALUES ('3016', '23', '搜狗', 'sogou', '', '9998', '1453249407');
INSERT INTO `go_data_statistics` VALUES ('3017', '8', '一元众乐', 'yyzl', '', '9999', '1453249570');
INSERT INTO `go_data_statistics` VALUES ('3018', '8', '一元众乐', 'yyzl', '', '10000', '1453249788');
INSERT INTO `go_data_statistics` VALUES ('3019', '30', '开心看', 'zlkxk', '', '10001', '1453250302');
INSERT INTO `go_data_statistics` VALUES ('3020', '8', '一元众乐', 'yyzl', '', '10002', '1453250444');
INSERT INTO `go_data_statistics` VALUES ('3021', '6', '信号增强器', 'zlxhzqq', '', '10003', '1453250565');
INSERT INTO `go_data_statistics` VALUES ('3022', '21', 'oppo', 'oppo', '', '10004', '1453250657');
INSERT INTO `go_data_statistics` VALUES ('3023', '14', '360手机助手', 'dev360', '', '10005', '1453250785');
INSERT INTO `go_data_statistics` VALUES ('3024', '8', '一元众乐', 'yyzl', '', '10006', '1453251036');
INSERT INTO `go_data_statistics` VALUES ('3025', '30', '开心看', 'zlkxk', '', '10007', '1453251724');
INSERT INTO `go_data_statistics` VALUES ('3026', '29', '小米开发者平台', 'xiaomi', '', '10008', '1453252011');
INSERT INTO `go_data_statistics` VALUES ('3027', '8', '一元众乐', 'yyzl', '', '10011', '1453252348');
INSERT INTO `go_data_statistics` VALUES ('3028', '19', '木蚂蚁', 'mumayi', '', '10012', '1453252726');
INSERT INTO `go_data_statistics` VALUES ('3029', '8', '一元众乐', 'yyzl', '', '10013', '1453252942');
INSERT INTO `go_data_statistics` VALUES ('3030', '8', '一元众乐', 'yyzl', '', '10014', '1453253013');
INSERT INTO `go_data_statistics` VALUES ('3031', '17', 'PP助手', 'pp', '', '10015', '1453253273');
INSERT INTO `go_data_statistics` VALUES ('3032', '30', '开心看', 'zlkxk', '', '10016', '1453253897');
INSERT INTO `go_data_statistics` VALUES ('3033', '6', '信号增强器', 'zlxhzqq', '', '10017', '1453253972');
INSERT INTO `go_data_statistics` VALUES ('3034', '8', '一元众乐', 'yyzl', '', '10018', '1453254058');
INSERT INTO `go_data_statistics` VALUES ('3035', '8', '一元众乐', 'yyzl', '', '10019', '1453255564');
INSERT INTO `go_data_statistics` VALUES ('3036', '30', '开心看', 'zlkxk', '', '10020', '1453255952');
INSERT INTO `go_data_statistics` VALUES ('3037', '8', '一元众乐', 'yyzl', '', '10021', '1453255970');
INSERT INTO `go_data_statistics` VALUES ('3038', '29', '小米开发者平台', 'xiaomi', '', '10022', '1453256293');
INSERT INTO `go_data_statistics` VALUES ('3039', '6', '信号增强器', 'zlxhzqq', '', '10023', '1453256293');
INSERT INTO `go_data_statistics` VALUES ('3040', '8', '一元众乐', 'yyzl', '', '10024', '1453256634');
INSERT INTO `go_data_statistics` VALUES ('3041', '14', '360手机助手', 'dev360', '', '10026', '1453256891');
INSERT INTO `go_data_statistics` VALUES ('3042', '8', '一元众乐', 'yyzl', '', '10027', '1453257713');
INSERT INTO `go_data_statistics` VALUES ('3043', '30', '开心看', 'zlkxk', '', '10028', '1453257719');
INSERT INTO `go_data_statistics` VALUES ('3044', '19', '木蚂蚁', 'mumayi', '', '10029', '1453257854');
INSERT INTO `go_data_statistics` VALUES ('3045', '24', '魅族', 'meizu', '', '10030', '1453258280');
INSERT INTO `go_data_statistics` VALUES ('3046', '29', '小米开发者平台', 'xiaomi', '', '10031', '1453258368');
INSERT INTO `go_data_statistics` VALUES ('3047', '8', '一元众乐', 'yyzl', '', '10032', '1453258643');
INSERT INTO `go_data_statistics` VALUES ('3048', '28', '安智开发者联盟', 'anzhi', '', '10033', '1453259282');
INSERT INTO `go_data_statistics` VALUES ('3049', '8', '一元众乐', 'yyzl', '', '10034', '1453259312');
INSERT INTO `go_data_statistics` VALUES ('3050', '8', '一元众乐', 'yyzl', '', '10035', '1453259368');
INSERT INTO `go_data_statistics` VALUES ('3051', '19', '木蚂蚁', 'mumayi', '', '10037', '1453259520');
INSERT INTO `go_data_statistics` VALUES ('3052', '30', '开心看', 'zlkxk', '', '10038', '1453259553');
INSERT INTO `go_data_statistics` VALUES ('3053', '8', '一元众乐', 'yyzl', '', '10039', '1453259657');
INSERT INTO `go_data_statistics` VALUES ('3054', '30', '开心看', 'zlkxk', '', '10040', '1453259998');
INSERT INTO `go_data_statistics` VALUES ('3055', '30', '开心看', 'zlkxk', '', '10041', '1453260023');
INSERT INTO `go_data_statistics` VALUES ('3056', '19', '木蚂蚁', 'mumayi', '', '10042', '1453260131');
INSERT INTO `go_data_statistics` VALUES ('3057', '8', '一元众乐', 'yyzl', '', '10043', '1453260585');
INSERT INTO `go_data_statistics` VALUES ('3058', '8', '一元众乐', 'yyzl', '', '10044', '1453260729');
INSERT INTO `go_data_statistics` VALUES ('3059', '8', '一元众乐', 'yyzl', '', '10045', '1453261120');
INSERT INTO `go_data_statistics` VALUES ('3060', '8', '一元众乐', 'yyzl', '', '10046', '1453261179');
INSERT INTO `go_data_statistics` VALUES ('3061', '19', '木蚂蚁', 'mumayi', '', '10047', '1453261642');
INSERT INTO `go_data_statistics` VALUES ('3062', '8', '一元众乐', 'yyzl', '', '10048', '1453261728');
INSERT INTO `go_data_statistics` VALUES ('3063', '17', 'PP助手', 'pp', '', '10049', '1453261803');
INSERT INTO `go_data_statistics` VALUES ('3064', '8', '一元众乐', 'yyzl', '', '10050', '1453261841');
INSERT INTO `go_data_statistics` VALUES ('3065', '17', 'PP助手', 'pp', '', '10051', '1453261918');
INSERT INTO `go_data_statistics` VALUES ('3066', '30', '开心看', 'zlkxk', '', '10052', '1453262038');
INSERT INTO `go_data_statistics` VALUES ('3067', '19', '木蚂蚁', 'mumayi', '', '10053', '1453262257');
INSERT INTO `go_data_statistics` VALUES ('3068', '14', '360手机助手', 'dev360', '', '10054', '1453262439');
INSERT INTO `go_data_statistics` VALUES ('3069', '30', '开心看', 'zlkxk', '', '10056', '1453262775');
INSERT INTO `go_data_statistics` VALUES ('3070', '8', '一元众乐', 'yyzl', '', '10057', '1453263111');
INSERT INTO `go_data_statistics` VALUES ('3071', '8', '一元众乐', 'yyzl', '', '10058', '1453263403');
INSERT INTO `go_data_statistics` VALUES ('3072', '6', '信号增强器', 'zlxhzqq', '', '10059', '1453264893');
INSERT INTO `go_data_statistics` VALUES ('3073', '8', '一元众乐', 'yyzl', '', '10060', '1453265309');
INSERT INTO `go_data_statistics` VALUES ('3074', '8', '一元众乐', 'yyzl', '', '10061', '1453265557');
INSERT INTO `go_data_statistics` VALUES ('3075', '8', '一元众乐', 'yyzl', '', '10062', '1453265690');
INSERT INTO `go_data_statistics` VALUES ('3076', '29', '小米开发者平台', 'xiaomi', '', '10063', '1453266221');
INSERT INTO `go_data_statistics` VALUES ('3077', '19', '木蚂蚁', 'mumayi', '', '10064', '1453266514');
INSERT INTO `go_data_statistics` VALUES ('3078', '30', '开心看', 'zlkxk', '', '10065', '1453266578');
INSERT INTO `go_data_statistics` VALUES ('3079', '8', '一元众乐', 'yyzl', '', '10066', '1453266689');
INSERT INTO `go_data_statistics` VALUES ('3080', '30', '开心看', 'zlkxk', '', '10067', '1453266725');
INSERT INTO `go_data_statistics` VALUES ('3081', '19', '木蚂蚁', 'mumayi', '', '10068', '1453267096');
INSERT INTO `go_data_statistics` VALUES ('3082', '30', '开心看', 'zlkxk', '', '10069', '1453267652');
INSERT INTO `go_data_statistics` VALUES ('3083', '24', '魅族', 'meizu', '', '10070', '1453267695');
INSERT INTO `go_data_statistics` VALUES ('3084', '8', '一元众乐', 'yyzl', '', '10071', '1453267991');
INSERT INTO `go_data_statistics` VALUES ('3085', '8', '一元众乐', 'yyzl', '', '10072', '1453268037');
INSERT INTO `go_data_statistics` VALUES ('3086', '19', '木蚂蚁', 'mumayi', '', '10073', '1453268175');
INSERT INTO `go_data_statistics` VALUES ('3087', '8', '一元众乐', 'yyzl', '', '10074', '1453268456');
INSERT INTO `go_data_statistics` VALUES ('3088', '17', 'PP助手', 'pp', '', '10075', '1453269217');
INSERT INTO `go_data_statistics` VALUES ('3089', '8', '一元众乐', 'yyzl', '', '10076', '1453269648');
INSERT INTO `go_data_statistics` VALUES ('3090', '19', '木蚂蚁', 'mumayi', '', '10077', '1453269711');
INSERT INTO `go_data_statistics` VALUES ('3091', '6', '信号增强器', 'zlxhzqq', '', '10078', '1453270283');
INSERT INTO `go_data_statistics` VALUES ('3092', '5', '酷赚锁屏', 'zlkzsp', '', '10079', '1453270788');
INSERT INTO `go_data_statistics` VALUES ('3093', '17', 'PP助手', 'pp', '', '10080', '1453270795');
INSERT INTO `go_data_statistics` VALUES ('3094', '30', '开心看', 'zlkxk', '', '10081', '1453270852');
INSERT INTO `go_data_statistics` VALUES ('3095', '15', '百度', 'baidu', '', '10082', '1453270962');
INSERT INTO `go_data_statistics` VALUES ('3096', '29', '小米开发者平台', 'xiaomi', '', '10083', '1453271084');
INSERT INTO `go_data_statistics` VALUES ('3097', '29', '小米开发者平台', 'xiaomi', '', '10084', '1453271084');
INSERT INTO `go_data_statistics` VALUES ('3098', '29', '小米开发者平台', 'xiaomi', '', '10085', '1453271084');
INSERT INTO `go_data_statistics` VALUES ('3099', '19', '木蚂蚁', 'mumayi', '', '10086', '1453271305');
INSERT INTO `go_data_statistics` VALUES ('3100', '19', '木蚂蚁', 'mumayi', '', '10088', '1453271315');
INSERT INTO `go_data_statistics` VALUES ('3101', '8', '一元众乐', 'yyzl', '', '10089', '1453272186');
INSERT INTO `go_data_statistics` VALUES ('3102', '19', '木蚂蚁', 'mumayi', '', '10090', '1453272605');
INSERT INTO `go_data_statistics` VALUES ('3103', '6', '信号增强器', 'zlxhzqq', '', '10091', '1453272609');
INSERT INTO `go_data_statistics` VALUES ('3104', '6', '信号增强器', 'zlxhzqq', '', '10092', '1453272898');
INSERT INTO `go_data_statistics` VALUES ('3105', '8', '一元众乐', 'yyzl', '', '10093', '1453273158');
INSERT INTO `go_data_statistics` VALUES ('3106', '8', '一元众乐', 'yyzl', '', '10094', '1453274017');
INSERT INTO `go_data_statistics` VALUES ('3107', '8', '一元众乐', 'yyzl', '', '10095', '1453274291');
INSERT INTO `go_data_statistics` VALUES ('3108', '19', '木蚂蚁', 'mumayi', '', '10096', '1453274455');
INSERT INTO `go_data_statistics` VALUES ('3109', '19', '木蚂蚁', 'mumayi', '', '10097', '1453274553');
INSERT INTO `go_data_statistics` VALUES ('3110', '8', '一元众乐', 'yyzl', '', '10098', '1453275082');
INSERT INTO `go_data_statistics` VALUES ('3111', '8', '一元众乐', 'yyzl', '', '10099', '1453275835');
INSERT INTO `go_data_statistics` VALUES ('3112', '19', '木蚂蚁', 'mumayi', '', '10100', '1453276480');
INSERT INTO `go_data_statistics` VALUES ('3113', '8', '一元众乐', 'yyzl', '', '10101', '1453276580');
INSERT INTO `go_data_statistics` VALUES ('3114', '14', '360手机助手', 'dev360', '', '10102', '1453276897');
INSERT INTO `go_data_statistics` VALUES ('3115', '8', '一元众乐', 'yyzl', '', '10103', '1453276910');
INSERT INTO `go_data_statistics` VALUES ('3116', '29', '小米开发者平台', 'xiaomi', '', '10104', '1453277504');
INSERT INTO `go_data_statistics` VALUES ('3117', '8', '一元众乐', 'yyzl', '', '10105', '1453277565');
INSERT INTO `go_data_statistics` VALUES ('3118', '8', '一元众乐', 'yyzl', '', '10106', '1453277591');
INSERT INTO `go_data_statistics` VALUES ('3119', '6', '信号增强器', 'zlxhzqq', '', '10107', '1453277845');
INSERT INTO `go_data_statistics` VALUES ('3120', '8', '一元众乐', 'yyzl', '', '10108', '1453278878');
INSERT INTO `go_data_statistics` VALUES ('3121', '14', '360手机助手', 'dev360', '', '10109', '1453279091');
INSERT INTO `go_data_statistics` VALUES ('3122', '19', '木蚂蚁', 'mumayi', '', '10110', '1453279172');
INSERT INTO `go_data_statistics` VALUES ('3123', '8', '一元众乐', 'yyzl', '', '10111', '1453279412');
INSERT INTO `go_data_statistics` VALUES ('3124', '8', '一元众乐', 'yyzl', '', '10112', '1453279636');
INSERT INTO `go_data_statistics` VALUES ('3125', '6', '信号增强器', 'zlxhzqq', '', '10113', '1453279725');
INSERT INTO `go_data_statistics` VALUES ('3126', '8', '一元众乐', 'yyzl', '', '10114', '1453279953');
INSERT INTO `go_data_statistics` VALUES ('3127', '19', '木蚂蚁', 'mumayi', '', '10115', '1453280516');
INSERT INTO `go_data_statistics` VALUES ('3128', '8', '一元众乐', 'yyzl', '', '10116', '1453281177');
INSERT INTO `go_data_statistics` VALUES ('3129', '8', '一元众乐', 'yyzl', '', '10117', '1453281325');
INSERT INTO `go_data_statistics` VALUES ('3130', '24', '魅族', 'meizu', '', '10118', '1453281799');
INSERT INTO `go_data_statistics` VALUES ('3131', '19', '木蚂蚁', 'mumayi', '', '10119', '1453281997');
INSERT INTO `go_data_statistics` VALUES ('3132', '8', '一元众乐', 'yyzl', '', '10120', '1453282022');
INSERT INTO `go_data_statistics` VALUES ('3133', '29', '小米开发者平台', 'xiaomi', '', '10123', '1453282159');
INSERT INTO `go_data_statistics` VALUES ('3134', '16', '豌豆荚', 'wandoujia', '', '10124', '1453282280');
INSERT INTO `go_data_statistics` VALUES ('3135', '17', 'PP助手', 'pp', '', '10125', '1453282351');
INSERT INTO `go_data_statistics` VALUES ('3136', '8', '一元众乐', 'yyzl', '', '10126', '1453283317');
INSERT INTO `go_data_statistics` VALUES ('3137', '6', '信号增强器', 'zlxhzqq', '', '10127', '1453283376');
INSERT INTO `go_data_statistics` VALUES ('3138', '23', '搜狗', 'sogou', '', '10128', '1453283697');
INSERT INTO `go_data_statistics` VALUES ('3139', '8', '一元众乐', 'yyzl', '', '10129', '1453284512');
INSERT INTO `go_data_statistics` VALUES ('3140', '30', '开心看', 'zlkxk', '', '10130', '1453284555');
INSERT INTO `go_data_statistics` VALUES ('3141', '30', '开心看', 'zlkxk', '', '10131', '1453284715');
INSERT INTO `go_data_statistics` VALUES ('3142', '19', '木蚂蚁', 'mumayi', '', '10132', '1453285093');
INSERT INTO `go_data_statistics` VALUES ('3143', '15', '百度', 'baidu', '', '10133', '1453285989');
INSERT INTO `go_data_statistics` VALUES ('3144', '8', '一元众乐', 'yyzl', '', '10134', '1453286058');
INSERT INTO `go_data_statistics` VALUES ('3145', '17', 'PP助手', 'pp', '', '10135', '1453286155');
INSERT INTO `go_data_statistics` VALUES ('3146', '8', '一元众乐', 'yyzl', '', '10136', '1453286159');
INSERT INTO `go_data_statistics` VALUES ('3147', '8', '一元众乐', 'yyzl', '', '10137', '1453286284');
INSERT INTO `go_data_statistics` VALUES ('3148', '24', '魅族', 'meizu', '', '10138', '1453286323');
INSERT INTO `go_data_statistics` VALUES ('3149', '8', '一元众乐', 'yyzl', '', '10139', '1453286558');
INSERT INTO `go_data_statistics` VALUES ('3150', '24', '魅族', 'meizu', '', '10141', '1453286839');
INSERT INTO `go_data_statistics` VALUES ('3151', '17', 'PP助手', 'pp', '', '10142', '1453286913');
INSERT INTO `go_data_statistics` VALUES ('3152', '17', 'PP助手', 'pp', '', '10143', '1453287002');
INSERT INTO `go_data_statistics` VALUES ('3153', '16', '豌豆荚', 'wandoujia', '', '10144', '1453287293');
INSERT INTO `go_data_statistics` VALUES ('3154', '15', '百度', 'baidu', '', '10145', '1453287296');
INSERT INTO `go_data_statistics` VALUES ('3155', '8', '一元众乐', 'yyzl', '', '10146', '1453287377');
INSERT INTO `go_data_statistics` VALUES ('3156', '8', '一元众乐', 'yyzl', '', '10147', '1453287396');
INSERT INTO `go_data_statistics` VALUES ('3157', '14', '360手机助手', 'dev360', '', '10148', '1453287550');
INSERT INTO `go_data_statistics` VALUES ('3158', '24', '魅族', 'meizu', '', '10149', '1453287685');
INSERT INTO `go_data_statistics` VALUES ('3159', '8', '一元众乐', 'yyzl', '', '10150', '1453287811');
INSERT INTO `go_data_statistics` VALUES ('3160', '30', '开心看', 'zlkxk', '', '10151', '1453287892');
INSERT INTO `go_data_statistics` VALUES ('3161', '14', '360手机助手', 'dev360', '', '10152', '1453288221');
INSERT INTO `go_data_statistics` VALUES ('3162', '30', '开心看', 'zlkxk', '', '10153', '1453288270');
INSERT INTO `go_data_statistics` VALUES ('3163', '8', '一元众乐', 'yyzl', '', '10155', '1453288611');
INSERT INTO `go_data_statistics` VALUES ('3164', '8', '一元众乐', 'yyzl', '', '10156', '1453288712');
INSERT INTO `go_data_statistics` VALUES ('3165', '30', '开心看', 'zlkxk', '', '10157', '1453289544');
INSERT INTO `go_data_statistics` VALUES ('3166', '8', '一元众乐', 'yyzl', '', '10158', '1453289705');
INSERT INTO `go_data_statistics` VALUES ('3167', '8', '一元众乐', 'yyzl', '', '10159', '1453289819');
INSERT INTO `go_data_statistics` VALUES ('3168', '8', '一元众乐', 'yyzl', '', '10160', '1453290134');
INSERT INTO `go_data_statistics` VALUES ('3169', '8', '一元众乐', 'yyzl', '', '10161', '1453290306');
INSERT INTO `go_data_statistics` VALUES ('3170', '30', '开心看', 'zlkxk', '', '10162', '1453290414');
INSERT INTO `go_data_statistics` VALUES ('3171', '14', '360手机助手', 'dev360', '', '10163', '1453290571');
INSERT INTO `go_data_statistics` VALUES ('3172', '19', '木蚂蚁', 'mumayi', '', '10164', '1453290914');
INSERT INTO `go_data_statistics` VALUES ('3173', '16', '豌豆荚', 'wandoujia', '', '10165', '1453291035');
INSERT INTO `go_data_statistics` VALUES ('3174', '8', '一元众乐', 'yyzl', '', '10166', '1453291088');
INSERT INTO `go_data_statistics` VALUES ('3175', '17', 'PP助手', 'pp', '', '10167', '1453291248');
INSERT INTO `go_data_statistics` VALUES ('3176', '8', '一元众乐', 'yyzl', '', '10168', '1453291280');
INSERT INTO `go_data_statistics` VALUES ('3177', '8', '一元众乐', 'yyzl', '', '10169', '1453291320');
INSERT INTO `go_data_statistics` VALUES ('3178', '30', '开心看', 'zlkxk', '', '10170', '1453291953');
INSERT INTO `go_data_statistics` VALUES ('3179', '8', '一元众乐', 'yyzl', '', '10172', '1453292118');
INSERT INTO `go_data_statistics` VALUES ('3180', '8', '一元众乐', 'yyzl', '', '10173', '1453292235');
INSERT INTO `go_data_statistics` VALUES ('3181', '13', '安卓市场', 'androidshichang', '', '10174', '1453292919');
INSERT INTO `go_data_statistics` VALUES ('3182', '8', '一元众乐', 'yyzl', '', '10175', '1453293013');
INSERT INTO `go_data_statistics` VALUES ('3183', '8', '一元众乐', 'yyzl', '', '10176', '1453293408');
INSERT INTO `go_data_statistics` VALUES ('3184', '30', '开心看', 'zlkxk', '', '10177', '1453293897');
INSERT INTO `go_data_statistics` VALUES ('3185', '19', '木蚂蚁', 'mumayi', '', '10178', '1453294247');
INSERT INTO `go_data_statistics` VALUES ('3186', '30', '开心看', 'zlkxk', '', '10179', '1453295061');
INSERT INTO `go_data_statistics` VALUES ('3187', '8', '一元众乐', 'yyzl', '', '10180', '1453295133');
INSERT INTO `go_data_statistics` VALUES ('3188', '8', '一元众乐', 'yyzl', '', '10181', '1453295137');
INSERT INTO `go_data_statistics` VALUES ('3189', '8', '一元众乐', 'yyzl', '', '10182', '1453295428');
INSERT INTO `go_data_statistics` VALUES ('3190', '16', '豌豆荚', 'wandoujia', '', '10184', '1453295526');
INSERT INTO `go_data_statistics` VALUES ('3191', '8', '一元众乐', 'yyzl', '', '10185', '1453295550');
INSERT INTO `go_data_statistics` VALUES ('3192', '19', '木蚂蚁', 'mumayi', '', '10186', '1453295906');
INSERT INTO `go_data_statistics` VALUES ('3193', '8', '一元众乐', 'yyzl', '', '10187', '1453295918');
INSERT INTO `go_data_statistics` VALUES ('3194', '8', '一元众乐', 'yyzl', '', '10188', '1453295936');
INSERT INTO `go_data_statistics` VALUES ('3195', '8', '一元众乐', 'yyzl', '', '10189', '1453295981');
INSERT INTO `go_data_statistics` VALUES ('3196', '8', '一元众乐', 'yyzl', '', '10191', '1453296226');
INSERT INTO `go_data_statistics` VALUES ('3197', '8', '一元众乐', 'yyzl', '', '10192', '1453296264');
INSERT INTO `go_data_statistics` VALUES ('3198', '8', '一元众乐', 'yyzl', '', '10193', '1453296384');
INSERT INTO `go_data_statistics` VALUES ('3199', '30', '开心看', 'zlkxk', '', '10194', '1453296720');
INSERT INTO `go_data_statistics` VALUES ('3200', '29', '小米开发者平台', 'xiaomi', '', '10195', '1453297076');
INSERT INTO `go_data_statistics` VALUES ('3201', '14', '360手机助手', 'dev360', '', '10196', '1453297192');
INSERT INTO `go_data_statistics` VALUES ('3202', '8', '一元众乐', 'yyzl', '', '10197', '1453297343');
INSERT INTO `go_data_statistics` VALUES ('3203', '8', '一元众乐', 'yyzl', '', '10198', '1453297870');
INSERT INTO `go_data_statistics` VALUES ('3204', '16', '豌豆荚', 'wandoujia', '', '10199', '1453298225');
INSERT INTO `go_data_statistics` VALUES ('3205', '14', '360手机助手', 'dev360', '', '10200', '1453298230');
INSERT INTO `go_data_statistics` VALUES ('3206', '30', '开心看', 'zlkxk', '', '10201', '1453298231');
INSERT INTO `go_data_statistics` VALUES ('3207', '19', '木蚂蚁', 'mumayi', '', '10202', '1453298698');
INSERT INTO `go_data_statistics` VALUES ('3208', '8', '一元众乐', 'yyzl', '', '10203', '1453298900');
INSERT INTO `go_data_statistics` VALUES ('3209', '2', '儒豹浏览器', 'rbower', '', '10204', '1453299081');
INSERT INTO `go_data_statistics` VALUES ('3210', '19', '木蚂蚁', 'mumayi', '', '10205', '1453299521');
INSERT INTO `go_data_statistics` VALUES ('3211', '19', '木蚂蚁', 'mumayi', '', '10206', '1453299807');
INSERT INTO `go_data_statistics` VALUES ('3212', '30', '开心看', 'zlkxk', '', '10207', '1453300009');
INSERT INTO `go_data_statistics` VALUES ('3213', '8', '一元众乐', 'yyzl', '', '10208', '1453300549');
INSERT INTO `go_data_statistics` VALUES ('3214', '8', '一元众乐', 'yyzl', '', '10209', '1453300705');
INSERT INTO `go_data_statistics` VALUES ('3215', '8', '一元众乐', 'yyzl', '', '10210', '1453300805');
INSERT INTO `go_data_statistics` VALUES ('3216', '30', '开心看', 'zlkxk', '', '10211', '1453301020');
INSERT INTO `go_data_statistics` VALUES ('3217', '8', '一元众乐', 'yyzl', '', '10212', '1453301765');
INSERT INTO `go_data_statistics` VALUES ('3218', '30', '开心看', 'zlkxk', '', '10213', '1453301896');
INSERT INTO `go_data_statistics` VALUES ('3219', '16', '豌豆荚', 'wandoujia', '', '10214', '1453302136');
INSERT INTO `go_data_statistics` VALUES ('3220', '8', '一元众乐', 'yyzl', '', '10215', '1453302405');
INSERT INTO `go_data_statistics` VALUES ('3221', '8', '一元众乐', 'yyzl', '', '10216', '1453302726');
INSERT INTO `go_data_statistics` VALUES ('3222', '8', '一元众乐', 'yyzl', '', '10217', '1453302874');
INSERT INTO `go_data_statistics` VALUES ('3223', '30', '开心看', 'zlkxk', '', '10218', '1453302910');
INSERT INTO `go_data_statistics` VALUES ('3224', '6', '信号增强器', 'zlxhzqq', '', '10219', '1453303125');
INSERT INTO `go_data_statistics` VALUES ('3225', '8', '一元众乐', 'yyzl', '', '10220', '1453303274');
INSERT INTO `go_data_statistics` VALUES ('3226', '21', 'oppo', 'oppo', '', '10221', '1453303673');
INSERT INTO `go_data_statistics` VALUES ('3227', '8', '一元众乐', 'yyzl', '', '10222', '1453303734');
INSERT INTO `go_data_statistics` VALUES ('3228', '19', '木蚂蚁', 'mumayi', '', '10223', '1453304805');
INSERT INTO `go_data_statistics` VALUES ('3229', '6', '信号增强器', 'zlxhzqq', '', '10224', '1453304929');
INSERT INTO `go_data_statistics` VALUES ('3230', '8', '一元众乐', 'yyzl', '', '10225', '1453305502');
INSERT INTO `go_data_statistics` VALUES ('3231', '8', '一元众乐', 'yyzl', '', '10226', '1453305502');
INSERT INTO `go_data_statistics` VALUES ('3232', '8', '一元众乐', 'yyzl', '', '10227', '1453305668');
INSERT INTO `go_data_statistics` VALUES ('3233', '17', 'PP助手', 'pp', '', '10228', '1453305919');
INSERT INTO `go_data_statistics` VALUES ('3234', '30', '开心看', 'zlkxk', '', '10229', '1453306139');
INSERT INTO `go_data_statistics` VALUES ('3235', '30', '开心看', 'zlkxk', '', '10230', '1453306989');
INSERT INTO `go_data_statistics` VALUES ('3236', '30', '开心看', 'zlkxk', '', '10231', '1453307502');
INSERT INTO `go_data_statistics` VALUES ('3237', '14', '360手机助手', 'dev360', '', '10232', '1453307641');
INSERT INTO `go_data_statistics` VALUES ('3238', '8', '一元众乐', 'yyzl', '', '10233', '1453307882');
INSERT INTO `go_data_statistics` VALUES ('3239', '6', '信号增强器', 'zlxhzqq', '', '10234', '1453308675');
INSERT INTO `go_data_statistics` VALUES ('3240', '30', '开心看', 'zlkxk', '', '10235', '1453309019');
INSERT INTO `go_data_statistics` VALUES ('3241', '8', '一元众乐', 'yyzl', '', '10236', '1453312566');
INSERT INTO `go_data_statistics` VALUES ('3242', '30', '开心看', 'zlkxk', '', '10237', '1453314821');
INSERT INTO `go_data_statistics` VALUES ('3243', '30', '开心看', 'zlkxk', '', '10238', '1453314870');
INSERT INTO `go_data_statistics` VALUES ('3244', '8', '一元众乐', 'yyzl', '', '10239', '1453314987');
INSERT INTO `go_data_statistics` VALUES ('3245', '30', '开心看', 'zlkxk', '', '10240', '1453315341');
INSERT INTO `go_data_statistics` VALUES ('3246', '8', '一元众乐', 'yyzl', '', '10241', '1453317043');
INSERT INTO `go_data_statistics` VALUES ('3247', '30', '开心看', 'zlkxk', '', '10242', '1453317668');
INSERT INTO `go_data_statistics` VALUES ('3248', '8', '一元众乐', 'yyzl', '', '10243', '1453322241');
INSERT INTO `go_data_statistics` VALUES ('3249', '8', '一元众乐', 'yyzl', '', '10245', '1453326432');
INSERT INTO `go_data_statistics` VALUES ('3250', '19', '木蚂蚁', 'mumayi', '', '10246', '1453327764');
INSERT INTO `go_data_statistics` VALUES ('3251', '8', '一元众乐', 'yyzl', '', '10249', '1453328901');
INSERT INTO `go_data_statistics` VALUES ('3252', '8', '一元众乐', 'yyzl', '', '10250', '1453330333');
INSERT INTO `go_data_statistics` VALUES ('3253', '30', '开心看', 'zlkxk', '', '10251', '1453330795');
INSERT INTO `go_data_statistics` VALUES ('3254', '8', '一元众乐', 'yyzl', '', '10252', '1453331333');
INSERT INTO `go_data_statistics` VALUES ('3255', '8', '一元众乐', 'yyzl', '', '10253', '1453331765');
INSERT INTO `go_data_statistics` VALUES ('3256', '8', '一元众乐', 'yyzl', '', '10254', '1453332822');
INSERT INTO `go_data_statistics` VALUES ('3257', '15', '百度', 'baidu', '', '10255', '1453334584');
INSERT INTO `go_data_statistics` VALUES ('3258', '8', '一元众乐', 'yyzl', '', '10256', '1453335149');
INSERT INTO `go_data_statistics` VALUES ('3259', '30', '开心看', 'zlkxk', '', '10257', '1453335379');
INSERT INTO `go_data_statistics` VALUES ('3260', '8', '一元众乐', 'yyzl', '', '10258', '1453335484');
INSERT INTO `go_data_statistics` VALUES ('3261', '8', '一元众乐', 'yyzl', '', '10259', '1453335991');
INSERT INTO `go_data_statistics` VALUES ('3262', '30', '开心看', 'zlkxk', '', '10261', '1453336924');
INSERT INTO `go_data_statistics` VALUES ('3263', '17', 'PP助手', 'pp', '', '10262', '1453337521');
INSERT INTO `go_data_statistics` VALUES ('3264', '19', '木蚂蚁', 'mumayi', '', '10263', '1453337523');
INSERT INTO `go_data_statistics` VALUES ('3265', '24', '魅族', 'meizu', '', '10264', '1453337925');
INSERT INTO `go_data_statistics` VALUES ('3266', '19', '木蚂蚁', 'mumayi', '', '10265', '1453338244');
INSERT INTO `go_data_statistics` VALUES ('3267', '6', '信号增强器', 'zlxhzqq', '', '10266', '1453338310');
INSERT INTO `go_data_statistics` VALUES ('3268', '8', '一元众乐', 'yyzl', '', '10267', '1453339064');
INSERT INTO `go_data_statistics` VALUES ('3269', '8', '一元众乐', 'yyzl', '', '10268', '1453339305');
INSERT INTO `go_data_statistics` VALUES ('3270', '14', '360手机助手', 'dev360', '', '10269', '1453339354');
INSERT INTO `go_data_statistics` VALUES ('3271', '30', '开心看', 'zlkxk', '', '10270', '1453340351');
INSERT INTO `go_data_statistics` VALUES ('3272', '30', '开心看', 'zlkxk', '', '10271', '1453341870');
INSERT INTO `go_data_statistics` VALUES ('3273', '15', '百度', 'baidu', '', '10272', '1453342375');
INSERT INTO `go_data_statistics` VALUES ('3274', '15', '百度', 'baidu', '', '10273', '1453342900');
INSERT INTO `go_data_statistics` VALUES ('3275', '30', '开心看', 'zlkxk', '', '10274', '1453343183');
INSERT INTO `go_data_statistics` VALUES ('3276', '15', '百度', 'baidu', '', '10275', '1453343474');
INSERT INTO `go_data_statistics` VALUES ('3277', '19', '木蚂蚁', 'mumayi', '', '10276', '1453343918');
INSERT INTO `go_data_statistics` VALUES ('3278', '8', '一元众乐', 'yyzl', '', '10277', '1453344013');
INSERT INTO `go_data_statistics` VALUES ('3279', '8', '一元众乐', 'yyzl', '', '10278', '1453344305');
INSERT INTO `go_data_statistics` VALUES ('3280', '19', '木蚂蚁', 'mumayi', '', '10279', '1453344368');
INSERT INTO `go_data_statistics` VALUES ('3281', '24', '魅族', 'meizu', '', '10280', '1453344643');
INSERT INTO `go_data_statistics` VALUES ('3282', '6', '信号增强器', 'zlxhzqq', '', '10281', '1453344815');
INSERT INTO `go_data_statistics` VALUES ('3283', '19', '木蚂蚁', 'mumayi', '', '10282', '1453348497');
INSERT INTO `go_data_statistics` VALUES ('3284', '8', '一元众乐', 'yyzl', '', '10283', '1453349531');
INSERT INTO `go_data_statistics` VALUES ('3285', '6', '信号增强器', 'zlxhzqq', '', '10284', '1453349561');
INSERT INTO `go_data_statistics` VALUES ('3286', '8', '一元众乐', 'yyzl', '', '10285', '1453349802');
INSERT INTO `go_data_statistics` VALUES ('3287', '8', '一元众乐', 'yyzl', '', '10286', '1453349917');
INSERT INTO `go_data_statistics` VALUES ('3288', '29', '小米开发者平台', 'xiaomi', '', '10287', '1453350009');
INSERT INTO `go_data_statistics` VALUES ('3289', '30', '开心看', 'zlkxk', '', '10288', '1453350427');
INSERT INTO `go_data_statistics` VALUES ('3290', '6', '信号增强器', 'zlxhzqq', '', '10289', '1453350516');
INSERT INTO `go_data_statistics` VALUES ('3291', '17', 'PP助手', 'pp', '', '10290', '1453350647');
INSERT INTO `go_data_statistics` VALUES ('3292', '8', '一元众乐', 'yyzl', '', '10291', '1453351061');
INSERT INTO `go_data_statistics` VALUES ('3293', '6', '信号增强器', 'zlxhzqq', '', '10292', '1453351080');
INSERT INTO `go_data_statistics` VALUES ('3294', '8', '一元众乐', 'yyzl', '', '10293', '1453351517');
INSERT INTO `go_data_statistics` VALUES ('3295', '8', '一元众乐', 'yyzl', '', '10294', '1453352205');
INSERT INTO `go_data_statistics` VALUES ('3296', '6', '信号增强器', 'zlxhzqq', '', '10295', '1453352914');
INSERT INTO `go_data_statistics` VALUES ('3297', '17', 'PP助手', 'pp', '', '10296', '1453352982');
INSERT INTO `go_data_statistics` VALUES ('3298', '8', '一元众乐', 'yyzl', '', '10297', '1453353792');
INSERT INTO `go_data_statistics` VALUES ('3299', '30', '开心看', 'zlkxk', '', '10298', '1453353859');
INSERT INTO `go_data_statistics` VALUES ('3300', '30', '开心看', 'zlkxk', '', '10299', '1453353892');
INSERT INTO `go_data_statistics` VALUES ('3301', '8', '一元众乐', 'yyzl', '', '10300', '1453354255');
INSERT INTO `go_data_statistics` VALUES ('3302', '15', '百度', 'baidu', '', '10301', '1453354286');
INSERT INTO `go_data_statistics` VALUES ('3303', '8', '一元众乐', 'yyzl', '', '10302', '1453354330');
INSERT INTO `go_data_statistics` VALUES ('3304', '24', '魅族', 'meizu', '', '10303', '1453354621');
INSERT INTO `go_data_statistics` VALUES ('3305', '8', '一元众乐', 'yyzl', '', '10304', '1453354762');
INSERT INTO `go_data_statistics` VALUES ('3306', '8', '一元众乐', 'yyzl', '', '10306', '1453355375');
INSERT INTO `go_data_statistics` VALUES ('3307', '30', '开心看', 'zlkxk', '', '10307', '1453355396');
INSERT INTO `go_data_statistics` VALUES ('3308', '8', '一元众乐', 'yyzl', '', '10308', '1453355612');
INSERT INTO `go_data_statistics` VALUES ('3309', '30', '开心看', 'zlkxk', '', '10309', '1453357863');
INSERT INTO `go_data_statistics` VALUES ('3310', '6', '信号增强器', 'zlxhzqq', '', '10310', '1453358033');
INSERT INTO `go_data_statistics` VALUES ('3311', '8', '一元众乐', 'yyzl', '', '10311', '1453358274');
INSERT INTO `go_data_statistics` VALUES ('3312', '30', '开心看', 'zlkxk', '', '10312', '1453358526');
INSERT INTO `go_data_statistics` VALUES ('3313', '8', '一元众乐', 'yyzl', '', '10313', '1453358787');
INSERT INTO `go_data_statistics` VALUES ('3314', '30', '开心看', 'zlkxk', '', '10315', '1453358978');
INSERT INTO `go_data_statistics` VALUES ('3315', '8', '一元众乐', 'yyzl', '', '10316', '1453359113');
INSERT INTO `go_data_statistics` VALUES ('3316', '8', '一元众乐', 'yyzl', '', '10317', '1453359167');
INSERT INTO `go_data_statistics` VALUES ('3317', '8', '一元众乐', 'yyzl', '', '10318', '1453359214');
INSERT INTO `go_data_statistics` VALUES ('3318', '8', '一元众乐', 'yyzl', '', '10319', '1453359275');
INSERT INTO `go_data_statistics` VALUES ('3319', '30', '开心看', 'zlkxk', '', '10320', '1453359294');
INSERT INTO `go_data_statistics` VALUES ('3320', '30', '开心看', 'zlkxk', '', '10321', '1453359371');
INSERT INTO `go_data_statistics` VALUES ('3321', '8', '一元众乐', 'yyzl', '', '10322', '1453359527');
INSERT INTO `go_data_statistics` VALUES ('3322', '29', '小米开发者平台', 'xiaomi', '', '10323', '1453359717');
INSERT INTO `go_data_statistics` VALUES ('3323', '6', '信号增强器', 'zlxhzqq', '', '10324', '1453360122');
INSERT INTO `go_data_statistics` VALUES ('3324', '30', '开心看', 'zlkxk', '', '10325', '1453360701');
INSERT INTO `go_data_statistics` VALUES ('3325', '30', '开心看', 'zlkxk', '', '10326', '1453360805');
INSERT INTO `go_data_statistics` VALUES ('3326', '19', '木蚂蚁', 'mumayi', '', '10327', '1453362819');
INSERT INTO `go_data_statistics` VALUES ('3327', '6', '信号增强器', 'zlxhzqq', '', '10328', '1453363003');
INSERT INTO `go_data_statistics` VALUES ('3328', '6', '信号增强器', 'zlxhzqq', '', '10329', '1453363281');
INSERT INTO `go_data_statistics` VALUES ('3329', '8', '一元众乐', 'yyzl', '', '10330', '1453364085');
INSERT INTO `go_data_statistics` VALUES ('3330', '29', '小米开发者平台', 'xiaomi', '', '10331', '1453365111');
INSERT INTO `go_data_statistics` VALUES ('3331', '8', '一元众乐', 'yyzl', '', '10332', '1453365504');
INSERT INTO `go_data_statistics` VALUES ('3332', '8', '一元众乐', 'yyzl', '', '10333', '1453365598');
INSERT INTO `go_data_statistics` VALUES ('3333', '19', '木蚂蚁', 'mumayi', '', '10334', '1453365726');
INSERT INTO `go_data_statistics` VALUES ('3334', '8', '一元众乐', 'yyzl', '', '10335', '1453366557');
INSERT INTO `go_data_statistics` VALUES ('3335', '6', '信号增强器', 'zlxhzqq', '', '10336', '1453366580');
INSERT INTO `go_data_statistics` VALUES ('3336', '6', '信号增强器', 'zlxhzqq', '', '10338', '1453366950');
INSERT INTO `go_data_statistics` VALUES ('3337', '30', '开心看', 'zlkxk', '', '10339', '1453367756');
INSERT INTO `go_data_statistics` VALUES ('3338', '20', '联想', 'lenovo', '', '10340', '1453367817');
INSERT INTO `go_data_statistics` VALUES ('3339', '8', '一元众乐', 'yyzl', '', '10341', '1453368260');
INSERT INTO `go_data_statistics` VALUES ('3340', '6', '信号增强器', 'zlxhzqq', '', '10342', '1453368547');
INSERT INTO `go_data_statistics` VALUES ('3341', '8', '一元众乐', 'yyzl', '', '10343', '1453368794');
INSERT INTO `go_data_statistics` VALUES ('3342', '8', '一元众乐', 'yyzl', '', '10345', '1453370082');
INSERT INTO `go_data_statistics` VALUES ('3343', '8', '一元众乐', 'yyzl', '', '10346', '1453370779');
INSERT INTO `go_data_statistics` VALUES ('3344', '30', '开心看', 'zlkxk', '', '10347', '1453370979');
INSERT INTO `go_data_statistics` VALUES ('3345', '8', '一元众乐', 'yyzl', '', '10348', '1453371669');
INSERT INTO `go_data_statistics` VALUES ('3346', '8', '一元众乐', 'yyzl', '', '10350', '1453372243');
INSERT INTO `go_data_statistics` VALUES ('3347', '8', '一元众乐', 'yyzl', '', '10351', '1453372591');
INSERT INTO `go_data_statistics` VALUES ('3348', '15', '百度', 'baidu', '', '10352', '1453372807');
INSERT INTO `go_data_statistics` VALUES ('3349', '8', '一元众乐', 'yyzl', '', '10353', '1453373214');
INSERT INTO `go_data_statistics` VALUES ('3350', '8', '一元众乐', 'yyzl', '', '10354', '1453373267');
INSERT INTO `go_data_statistics` VALUES ('3351', '8', '一元众乐', 'yyzl', '', '10355', '1453373584');
INSERT INTO `go_data_statistics` VALUES ('3352', '8', '一元众乐', 'yyzl', '', '10356', '1453373661');
INSERT INTO `go_data_statistics` VALUES ('3353', '8', '一元众乐', 'yyzl', '', '10357', '1453373733');
INSERT INTO `go_data_statistics` VALUES ('3354', '24', '魅族', 'meizu', '', '10358', '1453373967');
INSERT INTO `go_data_statistics` VALUES ('3355', '8', '一元众乐', 'yyzl', '', '10359', '1453374199');
INSERT INTO `go_data_statistics` VALUES ('3356', '8', '一元众乐', 'yyzl', '', '10360', '1453374204');
INSERT INTO `go_data_statistics` VALUES ('3357', '8', '一元众乐', 'yyzl', '', '10361', '1453374258');
INSERT INTO `go_data_statistics` VALUES ('3358', '8', '一元众乐', 'yyzl', '', '10363', '1453374371');
INSERT INTO `go_data_statistics` VALUES ('3359', '8', '一元众乐', 'yyzl', '', '10364', '1453374402');
INSERT INTO `go_data_statistics` VALUES ('3360', '8', '一元众乐', 'yyzl', '', '10365', '1453374644');
INSERT INTO `go_data_statistics` VALUES ('3361', '30', '开心看', 'zlkxk', '', '10366', '1453374748');
INSERT INTO `go_data_statistics` VALUES ('3362', '8', '一元众乐', 'yyzl', '', '10367', '1453374767');
INSERT INTO `go_data_statistics` VALUES ('3363', '8', '一元众乐', 'yyzl', '', '10368', '1453374787');
INSERT INTO `go_data_statistics` VALUES ('3364', '8', '一元众乐', 'yyzl', '', '10369', '1453374813');
INSERT INTO `go_data_statistics` VALUES ('3365', '30', '开心看', 'zlkxk', '', '10370', '1453374834');
INSERT INTO `go_data_statistics` VALUES ('3366', '8', '一元众乐', 'yyzl', '', '10371', '1453374838');
INSERT INTO `go_data_statistics` VALUES ('3367', '8', '一元众乐', 'yyzl', '', '10372', '1453374844');
INSERT INTO `go_data_statistics` VALUES ('3368', '8', '一元众乐', 'yyzl', '', '10373', '1453374856');
INSERT INTO `go_data_statistics` VALUES ('3369', '8', '一元众乐', 'yyzl', '', '10374', '1453374902');
INSERT INTO `go_data_statistics` VALUES ('3370', '8', '一元众乐', 'yyzl', '', '10375', '1453374991');
INSERT INTO `go_data_statistics` VALUES ('3371', '8', '一元众乐', 'yyzl', '', '10376', '1453375013');
INSERT INTO `go_data_statistics` VALUES ('3372', '8', '一元众乐', 'yyzl', '', '10377', '1453375095');
INSERT INTO `go_data_statistics` VALUES ('3373', '8', '一元众乐', 'yyzl', '', '10378', '1453375129');
INSERT INTO `go_data_statistics` VALUES ('3374', '8', '一元众乐', 'yyzl', '', '10379', '1453375184');
INSERT INTO `go_data_statistics` VALUES ('3375', '8', '一元众乐', 'yyzl', '', '10380', '1453375253');
INSERT INTO `go_data_statistics` VALUES ('3376', '8', '一元众乐', 'yyzl', '', '10381', '1453375289');
INSERT INTO `go_data_statistics` VALUES ('3377', '8', '一元众乐', 'yyzl', '', '10382', '1453375304');
INSERT INTO `go_data_statistics` VALUES ('3378', '8', '一元众乐', 'yyzl', '', '10383', '1453375369');
INSERT INTO `go_data_statistics` VALUES ('3379', '8', '一元众乐', 'yyzl', '', '10384', '1453375597');
INSERT INTO `go_data_statistics` VALUES ('3380', '8', '一元众乐', 'yyzl', '', '10385', '1453375607');
INSERT INTO `go_data_statistics` VALUES ('3381', '8', '一元众乐', 'yyzl', '', '10386', '1453375638');
INSERT INTO `go_data_statistics` VALUES ('3382', '8', '一元众乐', 'yyzl', '', '10387', '1453375639');
INSERT INTO `go_data_statistics` VALUES ('3383', '8', '一元众乐', 'yyzl', '', '10388', '1453375671');
INSERT INTO `go_data_statistics` VALUES ('3384', '8', '一元众乐', 'yyzl', '', '10389', '1453376019');
INSERT INTO `go_data_statistics` VALUES ('3385', '8', '一元众乐', 'yyzl', '', '10390', '1453376141');
INSERT INTO `go_data_statistics` VALUES ('3386', '8', '一元众乐', 'yyzl', '', '10391', '1453376489');
INSERT INTO `go_data_statistics` VALUES ('3387', '6', '信号增强器', 'zlxhzqq', '', '10392', '1453376604');
INSERT INTO `go_data_statistics` VALUES ('3388', '19', '木蚂蚁', 'mumayi', '', '10393', '1453377501');
INSERT INTO `go_data_statistics` VALUES ('3389', '15', '百度', 'baidu', '', '10394', '1453377525');
INSERT INTO `go_data_statistics` VALUES ('3390', '8', '一元众乐', 'yyzl', '', '10395', '1453378232');
INSERT INTO `go_data_statistics` VALUES ('3391', '8', '一元众乐', 'yyzl', '', '10396', '1453378356');
INSERT INTO `go_data_statistics` VALUES ('3392', '8', '一元众乐', 'yyzl', '', '10397', '1453378866');
INSERT INTO `go_data_statistics` VALUES ('3393', '8', '一元众乐', 'yyzl', '', '10398', '1453378907');
INSERT INTO `go_data_statistics` VALUES ('3394', '8', '一元众乐', 'yyzl', '', '10399', '1453379573');
INSERT INTO `go_data_statistics` VALUES ('3395', '30', '开心看', 'zlkxk', '', '10400', '1453379778');
INSERT INTO `go_data_statistics` VALUES ('3396', '8', '一元众乐', 'yyzl', '', '10401', '1453379799');
INSERT INTO `go_data_statistics` VALUES ('3397', '8', '一元众乐', 'yyzl', '', '10402', '1453380719');
INSERT INTO `go_data_statistics` VALUES ('3398', '8', '一元众乐', 'yyzl', '', '10403', '1453381312');
INSERT INTO `go_data_statistics` VALUES ('3399', '15', '百度', 'baidu', '', '10404', '1453381549');
INSERT INTO `go_data_statistics` VALUES ('3400', '29', '小米开发者平台', 'xiaomi', '', '10405', '1453381564');
INSERT INTO `go_data_statistics` VALUES ('3401', '30', '开心看', 'zlkxk', '', '10406', '1453381849');
INSERT INTO `go_data_statistics` VALUES ('3402', '30', '开心看', 'zlkxk', '', '10407', '1453381876');
INSERT INTO `go_data_statistics` VALUES ('3403', '8', '一元众乐', 'yyzl', '', '10408', '1453382240');
INSERT INTO `go_data_statistics` VALUES ('3404', '30', '开心看', 'zlkxk', '', '10409', '1453382483');
INSERT INTO `go_data_statistics` VALUES ('3405', '16', '豌豆荚', 'wandoujia', '', '10410', '1453382698');
INSERT INTO `go_data_statistics` VALUES ('3406', '30', '开心看', 'zlkxk', '', '10411', '1453383063');
INSERT INTO `go_data_statistics` VALUES ('3407', '6', '信号增强器', 'zlxhzqq', '', '10412', '1453383099');
INSERT INTO `go_data_statistics` VALUES ('3408', '8', '一元众乐', 'yyzl', '', '10413', '1453383244');
INSERT INTO `go_data_statistics` VALUES ('3409', '17', 'PP助手', 'pp', '', '10414', '1453383639');
INSERT INTO `go_data_statistics` VALUES ('3410', '30', '开心看', 'zlkxk', '', '10415', '1453383749');
INSERT INTO `go_data_statistics` VALUES ('3411', '30', '开心看', 'zlkxk', '', '10416', '1453383842');
INSERT INTO `go_data_statistics` VALUES ('3412', '30', '开心看', 'zlkxk', '', '10417', '1453384171');
INSERT INTO `go_data_statistics` VALUES ('3413', '30', '开心看', 'zlkxk', '', '10418', '1453384227');
INSERT INTO `go_data_statistics` VALUES ('3414', '8', '一元众乐', 'yyzl', '', '10419', '1453384250');
INSERT INTO `go_data_statistics` VALUES ('3415', '8', '一元众乐', 'yyzl', '', '10420', '1453384640');
INSERT INTO `go_data_statistics` VALUES ('3416', '8', '一元众乐', 'yyzl', '', '10421', '1453384965');
INSERT INTO `go_data_statistics` VALUES ('3417', '30', '开心看', 'zlkxk', '', '10422', '1453385775');
INSERT INTO `go_data_statistics` VALUES ('3418', '8', '一元众乐', 'yyzl', '', '10423', '1453385951');
INSERT INTO `go_data_statistics` VALUES ('3419', '8', '一元众乐', 'yyzl', '', '10424', '1453386091');
INSERT INTO `go_data_statistics` VALUES ('3420', '6', '信号增强器', 'zlxhzqq', '', '10426', '1453386255');
INSERT INTO `go_data_statistics` VALUES ('3421', '19', '木蚂蚁', 'mumayi', '', '10427', '1453386778');
INSERT INTO `go_data_statistics` VALUES ('3422', '30', '开心看', 'zlkxk', '', '10428', '1453386842');
INSERT INTO `go_data_statistics` VALUES ('3423', '19', '木蚂蚁', 'mumayi', '', '10429', '1453386854');
INSERT INTO `go_data_statistics` VALUES ('3424', '8', '一元众乐', 'yyzl', '', '10430', '1453387174');
INSERT INTO `go_data_statistics` VALUES ('3425', '8', '一元众乐', 'yyzl', '', '10431', '1453387227');
INSERT INTO `go_data_statistics` VALUES ('3426', '8', '一元众乐', 'yyzl', '', '10432', '1453387435');
INSERT INTO `go_data_statistics` VALUES ('3427', '8', '一元众乐', 'yyzl', '', '10433', '1453387518');
INSERT INTO `go_data_statistics` VALUES ('3428', '8', '一元众乐', 'yyzl', '', '10434', '1453387652');
INSERT INTO `go_data_statistics` VALUES ('3429', '30', '开心看', 'zlkxk', '', '10435', '1453387776');
INSERT INTO `go_data_statistics` VALUES ('3430', '8', '一元众乐', 'yyzl', '', '10436', '1453387920');
INSERT INTO `go_data_statistics` VALUES ('3431', '30', '开心看', 'zlkxk', '', '10437', '1453387928');
INSERT INTO `go_data_statistics` VALUES ('3432', '30', '开心看', 'zlkxk', '', '10438', '1453387932');
INSERT INTO `go_data_statistics` VALUES ('3433', '30', '开心看', 'zlkxk', '', '10439', '1453387998');
INSERT INTO `go_data_statistics` VALUES ('3434', '8', '一元众乐', 'yyzl', '', '10440', '1453388129');
INSERT INTO `go_data_statistics` VALUES ('3435', '8', '一元众乐', 'yyzl', '', '10441', '1453388134');
INSERT INTO `go_data_statistics` VALUES ('3436', '8', '一元众乐', 'yyzl', '', '10442', '1453388402');
INSERT INTO `go_data_statistics` VALUES ('3437', '8', '一元众乐', 'yyzl', '', '10443', '1453388531');
INSERT INTO `go_data_statistics` VALUES ('3438', '19', '木蚂蚁', 'mumayi', '', '10444', '1453389698');
INSERT INTO `go_data_statistics` VALUES ('3439', '8', '一元众乐', 'yyzl', '', '10445', '1453389834');
INSERT INTO `go_data_statistics` VALUES ('3440', '8', '一元众乐', 'yyzl', '', '10446', '1453390344');
INSERT INTO `go_data_statistics` VALUES ('3441', '8', '一元众乐', 'yyzl', '', '10447', '1453390491');
INSERT INTO `go_data_statistics` VALUES ('3442', '30', '开心看', 'zlkxk', '', '10448', '1453390579');
INSERT INTO `go_data_statistics` VALUES ('3443', '30', '开心看', 'zlkxk', '', '10449', '1453390644');
INSERT INTO `go_data_statistics` VALUES ('3444', '8', '一元众乐', 'yyzl', '', '10450', '1453391116');
INSERT INTO `go_data_statistics` VALUES ('3445', '8', '一元众乐', 'yyzl', '', '10451', '1453391430');
INSERT INTO `go_data_statistics` VALUES ('3446', '8', '一元众乐', 'yyzl', '', '10452', '1453392208');
INSERT INTO `go_data_statistics` VALUES ('3447', '8', '一元众乐', 'yyzl', '', '10454', '1453394451');
INSERT INTO `go_data_statistics` VALUES ('3448', '8', '一元众乐', 'yyzl', '', '10455', '1453394985');
INSERT INTO `go_data_statistics` VALUES ('3449', '24', '魅族', 'meizu', '', '10457', '1453399387');
INSERT INTO `go_data_statistics` VALUES ('3450', '2', '儒豹浏览器', 'rbower', '', '10458', '1453399561');
INSERT INTO `go_data_statistics` VALUES ('3451', '8', '一元众乐', 'yyzl', '', '10459', '1453401165');
INSERT INTO `go_data_statistics` VALUES ('3452', '8', '一元众乐', 'yyzl', '', '10460', '1453413637');
INSERT INTO `go_data_statistics` VALUES ('3453', '30', '开心看', 'zlkxk', '', '10461', '1453413904');
INSERT INTO `go_data_statistics` VALUES ('3454', '6', '信号增强器', 'zlxhzqq', '', '10462', '1453415354');
INSERT INTO `go_data_statistics` VALUES ('3455', '30', '开心看', 'zlkxk', '', '10463', '1453415403');
INSERT INTO `go_data_statistics` VALUES ('3456', '6', '信号增强器', 'zlxhzqq', '', '10464', '1453416754');
INSERT INTO `go_data_statistics` VALUES ('3457', '19', '木蚂蚁', 'mumayi', '', '10465', '1453417058');
INSERT INTO `go_data_statistics` VALUES ('3458', '8', '一元众乐', 'yyzl', '', '10466', '1453417111');
INSERT INTO `go_data_statistics` VALUES ('3459', '8', '一元众乐', 'yyzl', '', '10467', '1453417337');
INSERT INTO `go_data_statistics` VALUES ('3460', '6', '信号增强器', 'zlxhzqq', '', '10468', '1453417548');
INSERT INTO `go_data_statistics` VALUES ('3461', '30', '开心看', 'zlkxk', '', '10469', '1453418223');
INSERT INTO `go_data_statistics` VALUES ('3462', '6', '信号增强器', 'zlxhzqq', '', '10470', '1453419407');
INSERT INTO `go_data_statistics` VALUES ('3463', '24', '魅族', 'meizu', '', '10471', '1453419742');
INSERT INTO `go_data_statistics` VALUES ('3464', '8', '一元众乐', 'yyzl', '', '10472', '1453419844');
INSERT INTO `go_data_statistics` VALUES ('3465', '30', '开心看', 'zlkxk', '', '10473', '1453420764');
INSERT INTO `go_data_statistics` VALUES ('3466', '6', '信号增强器', 'zlxhzqq', '', '10474', '1453421338');
INSERT INTO `go_data_statistics` VALUES ('3467', '30', '开心看', 'zlkxk', '', '10475', '1453421414');
INSERT INTO `go_data_statistics` VALUES ('3468', '8', '一元众乐', 'yyzl', '', '10476', '1453421497');
INSERT INTO `go_data_statistics` VALUES ('3469', '8', '一元众乐', 'yyzl', '', '10477', '1453422001');
INSERT INTO `go_data_statistics` VALUES ('3470', '6', '信号增强器', 'zlxhzqq', '', '10478', '1453422002');
INSERT INTO `go_data_statistics` VALUES ('3471', '30', '开心看', 'zlkxk', '', '10479', '1453422093');
INSERT INTO `go_data_statistics` VALUES ('3472', '8', '一元众乐', 'yyzl', '', '10481', '1453424356');
INSERT INTO `go_data_statistics` VALUES ('3473', '8', '一元众乐', 'yyzl', '', '10482', '1453424427');
INSERT INTO `go_data_statistics` VALUES ('3474', '8', '一元众乐', 'yyzl', '', '10483', '1453424615');
INSERT INTO `go_data_statistics` VALUES ('3475', '30', '开心看', 'zlkxk', '', '10484', '1453424682');
INSERT INTO `go_data_statistics` VALUES ('3476', '8', '一元众乐', 'yyzl', '', '10485', '1453424860');
INSERT INTO `go_data_statistics` VALUES ('3477', '19', '木蚂蚁', 'mumayi', '', '10486', '1453425202');
INSERT INTO `go_data_statistics` VALUES ('3478', '8', '一元众乐', 'yyzl', '', '10487', '1453426160');
INSERT INTO `go_data_statistics` VALUES ('3479', '6', '信号增强器', 'zlxhzqq', '', '10488', '1453426703');
INSERT INTO `go_data_statistics` VALUES ('3480', '8', '一元众乐', 'yyzl', '', '10489', '1453428193');
INSERT INTO `go_data_statistics` VALUES ('3481', '30', '开心看', 'zlkxk', '', '10492', '1453429019');
INSERT INTO `go_data_statistics` VALUES ('3482', '6', '信号增强器', 'zlxhzqq', '', '10493', '1453429639');
INSERT INTO `go_data_statistics` VALUES ('3483', '30', '开心看', 'zlkxk', '', '10494', '1453429842');
INSERT INTO `go_data_statistics` VALUES ('3484', '8', '一元众乐', 'yyzl', '', '10495', '1453430411');
INSERT INTO `go_data_statistics` VALUES ('3485', '8', '一元众乐', 'yyzl', '', '10496', '1453430603');
INSERT INTO `go_data_statistics` VALUES ('3486', '24', '魅族', 'meizu', '', '10497', '1453430615');
INSERT INTO `go_data_statistics` VALUES ('3487', '8', '一元众乐', 'yyzl', '', '10498', '1453430644');
INSERT INTO `go_data_statistics` VALUES ('3488', '14', '360手机助手', 'dev360', '', '10499', '1453430883');
INSERT INTO `go_data_statistics` VALUES ('3489', '30', '开心看', 'zlkxk', '', '10500', '1453430993');
INSERT INTO `go_data_statistics` VALUES ('3490', '8', '一元众乐', 'yyzl', '', '10501', '1453431492');
INSERT INTO `go_data_statistics` VALUES ('3491', '6', '信号增强器', 'zlxhzqq', '', '10502', '1453431565');
INSERT INTO `go_data_statistics` VALUES ('3492', '8', '一元众乐', 'yyzl', '', '10504', '1453433246');
INSERT INTO `go_data_statistics` VALUES ('3493', '8', '一元众乐', 'yyzl', '', '10505', '1453433337');
INSERT INTO `go_data_statistics` VALUES ('3494', '30', '开心看', 'zlkxk', '', '10506', '1453433356');
INSERT INTO `go_data_statistics` VALUES ('3495', '8', '一元众乐', 'yyzl', '', '10507', '1453433505');
INSERT INTO `go_data_statistics` VALUES ('3496', '8', '一元众乐', 'yyzl', '', '10508', '1453433507');
INSERT INTO `go_data_statistics` VALUES ('3497', '8', '一元众乐', 'yyzl', '', '10509', '1453433523');
INSERT INTO `go_data_statistics` VALUES ('3498', '8', '一元众乐', 'yyzl', '', '10510', '1453433589');
INSERT INTO `go_data_statistics` VALUES ('3499', '6', '信号增强器', 'zlxhzqq', '', '10512', '1453433645');
INSERT INTO `go_data_statistics` VALUES ('3500', '30', '开心看', 'zlkxk', '', '10513', '1453433670');
INSERT INTO `go_data_statistics` VALUES ('3501', '8', '一元众乐', 'yyzl', '', '10514', '1453433718');
INSERT INTO `go_data_statistics` VALUES ('3502', '8', '一元众乐', 'yyzl', '', '10515', '1453433771');
INSERT INTO `go_data_statistics` VALUES ('3503', '8', '一元众乐', 'yyzl', '', '10516', '1453433794');
INSERT INTO `go_data_statistics` VALUES ('3504', '8', '一元众乐', 'yyzl', '', '10517', '1453433816');
INSERT INTO `go_data_statistics` VALUES ('3505', '8', '一元众乐', 'yyzl', '', '10518', '1453433904');
INSERT INTO `go_data_statistics` VALUES ('3506', '8', '一元众乐', 'yyzl', '', '10519', '1453433919');
INSERT INTO `go_data_statistics` VALUES ('3507', '8', '一元众乐', 'yyzl', '', '10520', '1453433940');
INSERT INTO `go_data_statistics` VALUES ('3508', '8', '一元众乐', 'yyzl', '', '10521', '1453433966');
INSERT INTO `go_data_statistics` VALUES ('3509', '8', '一元众乐', 'yyzl', '', '10522', '1453434167');
INSERT INTO `go_data_statistics` VALUES ('3510', '8', '一元众乐', 'yyzl', '', '10523', '1453434201');
INSERT INTO `go_data_statistics` VALUES ('3511', '8', '一元众乐', 'yyzl', '', '10525', '1453434247');
INSERT INTO `go_data_statistics` VALUES ('3512', '8', '一元众乐', 'yyzl', '', '10526', '1453434338');
INSERT INTO `go_data_statistics` VALUES ('3513', '8', '一元众乐', 'yyzl', '', '10527', '1453434449');
INSERT INTO `go_data_statistics` VALUES ('3514', '8', '一元众乐', 'yyzl', '', '10528', '1453434449');
INSERT INTO `go_data_statistics` VALUES ('3515', '8', '一元众乐', 'yyzl', '', '10529', '1453434543');
INSERT INTO `go_data_statistics` VALUES ('3516', '30', '开心看', 'zlkxk', '', '10530', '1453434547');
INSERT INTO `go_data_statistics` VALUES ('3517', '8', '一元众乐', 'yyzl', '', '10531', '1453434572');
INSERT INTO `go_data_statistics` VALUES ('3518', '8', '一元众乐', 'yyzl', '', '10532', '1453434592');
INSERT INTO `go_data_statistics` VALUES ('3519', '8', '一元众乐', 'yyzl', '', '10533', '1453434649');
INSERT INTO `go_data_statistics` VALUES ('3520', '8', '一元众乐', 'yyzl', '', '10534', '1453434671');
INSERT INTO `go_data_statistics` VALUES ('3521', '8', '一元众乐', 'yyzl', '', '10535', '1453434680');
INSERT INTO `go_data_statistics` VALUES ('3522', '8', '一元众乐', 'yyzl', '', '10536', '1453434693');
INSERT INTO `go_data_statistics` VALUES ('3523', '8', '一元众乐', 'yyzl', '', '10537', '1453434700');
INSERT INTO `go_data_statistics` VALUES ('3524', '8', '一元众乐', 'yyzl', '', '10538', '1453434712');
INSERT INTO `go_data_statistics` VALUES ('3525', '8', '一元众乐', 'yyzl', '', '10539', '1453434718');
INSERT INTO `go_data_statistics` VALUES ('3526', '8', '一元众乐', 'yyzl', '', '10540', '1453434756');
INSERT INTO `go_data_statistics` VALUES ('3527', '8', '一元众乐', 'yyzl', '', '10541', '1453434770');
INSERT INTO `go_data_statistics` VALUES ('3528', '8', '一元众乐', 'yyzl', '', '10542', '1453434775');
INSERT INTO `go_data_statistics` VALUES ('3529', '8', '一元众乐', 'yyzl', '', '10543', '1453434775');
INSERT INTO `go_data_statistics` VALUES ('3530', '8', '一元众乐', 'yyzl', '', '10544', '1453434803');
INSERT INTO `go_data_statistics` VALUES ('3531', '8', '一元众乐', 'yyzl', '', '10545', '1453434847');
INSERT INTO `go_data_statistics` VALUES ('3532', '8', '一元众乐', 'yyzl', '', '10546', '1453434900');
INSERT INTO `go_data_statistics` VALUES ('3533', '8', '一元众乐', 'yyzl', '', '10547', '1453434908');
INSERT INTO `go_data_statistics` VALUES ('3534', '8', '一元众乐', 'yyzl', '', '10548', '1453434998');
INSERT INTO `go_data_statistics` VALUES ('3535', '8', '一元众乐', 'yyzl', '', '10549', '1453435034');
INSERT INTO `go_data_statistics` VALUES ('3536', '30', '开心看', 'zlkxk', '', '10550', '1453435050');
INSERT INTO `go_data_statistics` VALUES ('3537', '8', '一元众乐', 'yyzl', '', '10551', '1453435064');
INSERT INTO `go_data_statistics` VALUES ('3538', '8', '一元众乐', 'yyzl', '', '10552', '1453435085');
INSERT INTO `go_data_statistics` VALUES ('3539', '8', '一元众乐', 'yyzl', '', '10553', '1453435119');
INSERT INTO `go_data_statistics` VALUES ('3540', '8', '一元众乐', 'yyzl', '', '10554', '1453435140');
INSERT INTO `go_data_statistics` VALUES ('3541', '8', '一元众乐', 'yyzl', '', '10555', '1453435187');
INSERT INTO `go_data_statistics` VALUES ('3542', '8', '一元众乐', 'yyzl', '', '10556', '1453435200');
INSERT INTO `go_data_statistics` VALUES ('3543', '8', '一元众乐', 'yyzl', '', '10557', '1453435261');
INSERT INTO `go_data_statistics` VALUES ('3544', '8', '一元众乐', 'yyzl', '', '10558', '1453435294');
INSERT INTO `go_data_statistics` VALUES ('3545', '8', '一元众乐', 'yyzl', '', '10559', '1453435312');
INSERT INTO `go_data_statistics` VALUES ('3546', '8', '一元众乐', 'yyzl', '', '10560', '1453435320');
INSERT INTO `go_data_statistics` VALUES ('3547', '8', '一元众乐', 'yyzl', '', '10561', '1453435405');
INSERT INTO `go_data_statistics` VALUES ('3548', '8', '一元众乐', 'yyzl', '', '10562', '1453435444');
INSERT INTO `go_data_statistics` VALUES ('3549', '8', '一元众乐', 'yyzl', '', '10563', '1453435452');
INSERT INTO `go_data_statistics` VALUES ('3550', '8', '一元众乐', 'yyzl', '', '10564', '1453435454');
INSERT INTO `go_data_statistics` VALUES ('3551', '8', '一元众乐', 'yyzl', '', '10565', '1453435475');
INSERT INTO `go_data_statistics` VALUES ('3552', '8', '一元众乐', 'yyzl', '', '10566', '1453435487');
INSERT INTO `go_data_statistics` VALUES ('3553', '8', '一元众乐', 'yyzl', '', '10567', '1453435537');
INSERT INTO `go_data_statistics` VALUES ('3554', '8', '一元众乐', 'yyzl', '', '10568', '1453435538');
INSERT INTO `go_data_statistics` VALUES ('3555', '8', '一元众乐', 'yyzl', '', '10569', '1453435561');
INSERT INTO `go_data_statistics` VALUES ('3556', '8', '一元众乐', 'yyzl', '', '10570', '1453435585');
INSERT INTO `go_data_statistics` VALUES ('3557', '8', '一元众乐', 'yyzl', '', '10571', '1453435674');
INSERT INTO `go_data_statistics` VALUES ('3558', '8', '一元众乐', 'yyzl', '', '10572', '1453435699');
INSERT INTO `go_data_statistics` VALUES ('3559', '8', '一元众乐', 'yyzl', '', '10573', '1453435719');
INSERT INTO `go_data_statistics` VALUES ('3560', '8', '一元众乐', 'yyzl', '', '10574', '1453435756');
INSERT INTO `go_data_statistics` VALUES ('3561', '8', '一元众乐', 'yyzl', '', '10575', '1453435838');
INSERT INTO `go_data_statistics` VALUES ('3562', '8', '一元众乐', 'yyzl', '', '10576', '1453435865');
INSERT INTO `go_data_statistics` VALUES ('3563', '8', '一元众乐', 'yyzl', '', '10577', '1453435875');
INSERT INTO `go_data_statistics` VALUES ('3564', '8', '一元众乐', 'yyzl', '', '10578', '1453435940');
INSERT INTO `go_data_statistics` VALUES ('3565', '8', '一元众乐', 'yyzl', '', '10579', '1453435997');
INSERT INTO `go_data_statistics` VALUES ('3566', '8', '一元众乐', 'yyzl', '', '10580', '1453435999');
INSERT INTO `go_data_statistics` VALUES ('3567', '8', '一元众乐', 'yyzl', '', '10581', '1453436060');
INSERT INTO `go_data_statistics` VALUES ('3568', '8', '一元众乐', 'yyzl', '', '10582', '1453436134');
INSERT INTO `go_data_statistics` VALUES ('3569', '8', '一元众乐', 'yyzl', '', '10583', '1453436139');
INSERT INTO `go_data_statistics` VALUES ('3570', '8', '一元众乐', 'yyzl', '', '10584', '1453436172');
INSERT INTO `go_data_statistics` VALUES ('3571', '8', '一元众乐', 'yyzl', '', '10585', '1453436237');
INSERT INTO `go_data_statistics` VALUES ('3572', '8', '一元众乐', 'yyzl', '', '10586', '1453436253');
INSERT INTO `go_data_statistics` VALUES ('3573', '8', '一元众乐', 'yyzl', '', '10587', '1453436294');
INSERT INTO `go_data_statistics` VALUES ('3574', '8', '一元众乐', 'yyzl', '', '10588', '1453436311');
INSERT INTO `go_data_statistics` VALUES ('3575', '8', '一元众乐', 'yyzl', '', '10589', '1453436340');
INSERT INTO `go_data_statistics` VALUES ('3576', '8', '一元众乐', 'yyzl', '', '10590', '1453436400');
INSERT INTO `go_data_statistics` VALUES ('3577', '8', '一元众乐', 'yyzl', '', '10591', '1453436400');
INSERT INTO `go_data_statistics` VALUES ('3578', '8', '一元众乐', 'yyzl', '', '10592', '1453436409');
INSERT INTO `go_data_statistics` VALUES ('3579', '8', '一元众乐', 'yyzl', '', '10593', '1453436412');
INSERT INTO `go_data_statistics` VALUES ('3580', '8', '一元众乐', 'yyzl', '', '10594', '1453436422');
INSERT INTO `go_data_statistics` VALUES ('3581', '8', '一元众乐', 'yyzl', '', '10595', '1453436521');
INSERT INTO `go_data_statistics` VALUES ('3582', '8', '一元众乐', 'yyzl', '', '10596', '1453436534');
INSERT INTO `go_data_statistics` VALUES ('3583', '8', '一元众乐', 'yyzl', '', '10597', '1453436547');
INSERT INTO `go_data_statistics` VALUES ('3584', '8', '一元众乐', 'yyzl', '', '10598', '1453436593');
INSERT INTO `go_data_statistics` VALUES ('3585', '8', '一元众乐', 'yyzl', '', '10599', '1453436642');
INSERT INTO `go_data_statistics` VALUES ('3586', '8', '一元众乐', 'yyzl', '', '10600', '1453436676');
INSERT INTO `go_data_statistics` VALUES ('3587', '8', '一元众乐', 'yyzl', '', '10601', '1453436838');
INSERT INTO `go_data_statistics` VALUES ('3588', '8', '一元众乐', 'yyzl', '', '10602', '1453437234');
INSERT INTO `go_data_statistics` VALUES ('3589', '8', '一元众乐', 'yyzl', '', '10603', '1453437317');
INSERT INTO `go_data_statistics` VALUES ('3590', '8', '一元众乐', 'yyzl', '', '10604', '1453437344');
INSERT INTO `go_data_statistics` VALUES ('3591', '6', '信号增强器', 'zlxhzqq', '', '10605', '1453437497');
INSERT INTO `go_data_statistics` VALUES ('3592', '29', '小米开发者平台', 'xiaomi', '', '10606', '1453437622');
INSERT INTO `go_data_statistics` VALUES ('3593', '8', '一元众乐', 'yyzl', '', '10607', '1453437634');
INSERT INTO `go_data_statistics` VALUES ('3594', '8', '一元众乐', 'yyzl', '', '10608', '1453437668');
INSERT INTO `go_data_statistics` VALUES ('3595', '8', '一元众乐', 'yyzl', '', '10609', '1453437686');
INSERT INTO `go_data_statistics` VALUES ('3596', '8', '一元众乐', 'yyzl', '', '10610', '1453437911');
INSERT INTO `go_data_statistics` VALUES ('3597', '8', '一元众乐', 'yyzl', '', '10611', '1453438007');
INSERT INTO `go_data_statistics` VALUES ('3598', '8', '一元众乐', 'yyzl', '', '10612', '1453438118');
INSERT INTO `go_data_statistics` VALUES ('3599', '8', '一元众乐', 'yyzl', '', '10614', '1453438200');
INSERT INTO `go_data_statistics` VALUES ('3600', '8', '一元众乐', 'yyzl', '', '10615', '1453438244');
INSERT INTO `go_data_statistics` VALUES ('3601', '8', '一元众乐', 'yyzl', '', '10616', '1453438324');
INSERT INTO `go_data_statistics` VALUES ('3602', '8', '一元众乐', 'yyzl', '', '10617', '1453438633');
INSERT INTO `go_data_statistics` VALUES ('3603', '8', '一元众乐', 'yyzl', '', '10618', '1453438918');
INSERT INTO `go_data_statistics` VALUES ('3604', '8', '一元众乐', 'yyzl', '', '10619', '1453439601');
INSERT INTO `go_data_statistics` VALUES ('3605', '8', '一元众乐', 'yyzl', '', '10620', '1453439816');
INSERT INTO `go_data_statistics` VALUES ('3606', '30', '开心看', 'zlkxk', '', '10621', '1453439924');
INSERT INTO `go_data_statistics` VALUES ('3607', '8', '一元众乐', 'yyzl', '', '10622', '1453439933');
INSERT INTO `go_data_statistics` VALUES ('3608', '8', '一元众乐', 'yyzl', '', '10623', '1453440297');
INSERT INTO `go_data_statistics` VALUES ('3609', '8', '一元众乐', 'yyzl', '', '10624', '1453440389');
INSERT INTO `go_data_statistics` VALUES ('3610', '8', '一元众乐', 'yyzl', '', '10625', '1453440498');
INSERT INTO `go_data_statistics` VALUES ('3611', '30', '开心看', 'zlkxk', '', '10626', '1453440524');
INSERT INTO `go_data_statistics` VALUES ('3612', '8', '一元众乐', 'yyzl', '', '10627', '1453440670');
INSERT INTO `go_data_statistics` VALUES ('3613', '8', '一元众乐', 'yyzl', '', '10628', '1453441181');
INSERT INTO `go_data_statistics` VALUES ('3614', '8', '一元众乐', 'yyzl', '', '10629', '1453441772');
INSERT INTO `go_data_statistics` VALUES ('3615', '8', '一元众乐', 'yyzl', '', '10630', '1453441772');
INSERT INTO `go_data_statistics` VALUES ('3616', '8', '一元众乐', 'yyzl', '', '10631', '1453441772');
INSERT INTO `go_data_statistics` VALUES ('3617', '8', '一元众乐', 'yyzl', '', '10632', '1453441783');
INSERT INTO `go_data_statistics` VALUES ('3618', '8', '一元众乐', 'yyzl', '', '10633', '1453441807');
INSERT INTO `go_data_statistics` VALUES ('3619', '8', '一元众乐', 'yyzl', '', '10634', '1453444768');
INSERT INTO `go_data_statistics` VALUES ('3620', '6', '信号增强器', 'zlxhzqq', '', '10635', '1453444839');
INSERT INTO `go_data_statistics` VALUES ('3621', '6', '信号增强器', 'zlxhzqq', '', '10636', '1453445103');
INSERT INTO `go_data_statistics` VALUES ('3622', '8', '一元众乐', 'yyzl', '', '10637', '1453445468');
INSERT INTO `go_data_statistics` VALUES ('3623', '8', '一元众乐', 'yyzl', '', '10638', '1453445821');
INSERT INTO `go_data_statistics` VALUES ('3624', '30', '开心看', 'zlkxk', '', '10639', '1453447801');
INSERT INTO `go_data_statistics` VALUES ('3625', '8', '一元众乐', 'yyzl', '', '10640', '1453447824');
INSERT INTO `go_data_statistics` VALUES ('3626', '8', '一元众乐', 'yyzl', '', '10641', '1453448860');
INSERT INTO `go_data_statistics` VALUES ('3627', '8', '一元众乐', 'yyzl', '', '10642', '1453449130');
INSERT INTO `go_data_statistics` VALUES ('3628', '8', '一元众乐', 'yyzl', '', '10643', '1453449360');
INSERT INTO `go_data_statistics` VALUES ('3629', '30', '开心看', 'zlkxk', '', '10645', '1453450178');
INSERT INTO `go_data_statistics` VALUES ('3630', '8', '一元众乐', 'yyzl', '', '10646', '1453450378');
INSERT INTO `go_data_statistics` VALUES ('3631', '8', '一元众乐', 'yyzl', '', '10647', '1453450411');
INSERT INTO `go_data_statistics` VALUES ('3632', '8', '一元众乐', 'yyzl', '', '10648', '1453450681');
INSERT INTO `go_data_statistics` VALUES ('3633', '8', '一元众乐', 'yyzl', '', '10649', '1453450724');
INSERT INTO `go_data_statistics` VALUES ('3634', '19', '木蚂蚁', 'mumayi', '', '10651', '1453452633');
INSERT INTO `go_data_statistics` VALUES ('3635', '19', '木蚂蚁', 'mumayi', '', '10652', '1453452633');
INSERT INTO `go_data_statistics` VALUES ('3636', '19', '木蚂蚁', 'mumayi', '', '10653', '1453452633');
INSERT INTO `go_data_statistics` VALUES ('3637', '19', '木蚂蚁', 'mumayi', '', '10654', '1453452643');
INSERT INTO `go_data_statistics` VALUES ('3638', '19', '木蚂蚁', 'mumayi', '', '10655', '1453453861');
INSERT INTO `go_data_statistics` VALUES ('3639', '8', '一元众乐', 'yyzl', '', '10656', '1453453981');
INSERT INTO `go_data_statistics` VALUES ('3640', '30', '开心看', 'zlkxk', '', '10658', '1453454321');
INSERT INTO `go_data_statistics` VALUES ('3641', '8', '一元众乐', 'yyzl', '', '10661', '1453456229');
INSERT INTO `go_data_statistics` VALUES ('3642', '8', '一元众乐', 'yyzl', '', '10662', '1453456828');
INSERT INTO `go_data_statistics` VALUES ('3643', '8', '一元众乐', 'yyzl', '', '10663', '1453456926');
INSERT INTO `go_data_statistics` VALUES ('3644', '8', '一元众乐', 'yyzl', '', '10665', '1453457199');
INSERT INTO `go_data_statistics` VALUES ('3645', '8', '一元众乐', 'yyzl', '', '10666', '1453457411');
INSERT INTO `go_data_statistics` VALUES ('3646', '19', '木蚂蚁', 'mumayi', '', '10667', '1453457484');
INSERT INTO `go_data_statistics` VALUES ('3647', '30', '开心看', 'zlkxk', '', '10668', '1453457959');
INSERT INTO `go_data_statistics` VALUES ('3648', '19', '木蚂蚁', 'mumayi', '', '10670', '1453458502');
INSERT INTO `go_data_statistics` VALUES ('3649', '8', '一元众乐', 'yyzl', '', '10672', '1453460614');
INSERT INTO `go_data_statistics` VALUES ('3650', '8', '一元众乐', 'yyzl', '', '10673', '1453460843');
INSERT INTO `go_data_statistics` VALUES ('3651', '23', '搜狗', 'sogou', '', '10675', '1453461931');
INSERT INTO `go_data_statistics` VALUES ('3652', '8', '一元众乐', 'yyzl', '', '10677', '1453462010');
INSERT INTO `go_data_statistics` VALUES ('3653', '8', '一元众乐', 'yyzl', '', '10678', '1453462368');
INSERT INTO `go_data_statistics` VALUES ('3654', '8', '一元众乐', 'yyzl', '', '10680', '1453462915');
INSERT INTO `go_data_statistics` VALUES ('3655', '30', '开心看', 'zlkxk', '', '10681', '1453463280');
INSERT INTO `go_data_statistics` VALUES ('3656', '8', '一元众乐', 'yyzl', '', '10682', '1453463486');
INSERT INTO `go_data_statistics` VALUES ('3657', '29', '小米开发者平台', 'xiaomi', '', '10683', '1453464259');
INSERT INTO `go_data_statistics` VALUES ('3658', '21', 'oppo', 'oppo', '', '10684', '1453464963');
INSERT INTO `go_data_statistics` VALUES ('3659', '19', '木蚂蚁', 'mumayi', '', '10685', '1453465124');
INSERT INTO `go_data_statistics` VALUES ('3660', '8', '一元众乐', 'yyzl', '', '10687', '1453465398');
INSERT INTO `go_data_statistics` VALUES ('3661', '19', '木蚂蚁', 'mumayi', '', '10688', '1453465751');
INSERT INTO `go_data_statistics` VALUES ('3662', '8', '一元众乐', 'yyzl', '', '10689', '1453465807');
INSERT INTO `go_data_statistics` VALUES ('3663', '30', '开心看', 'zlkxk', '', '10691', '1453466896');
INSERT INTO `go_data_statistics` VALUES ('3664', '30', '开心看', 'zlkxk', '', '10692', '1453467332');
INSERT INTO `go_data_statistics` VALUES ('3665', '30', '开心看', 'zlkxk', '', '10693', '1453467832');
INSERT INTO `go_data_statistics` VALUES ('3666', '8', '一元众乐', 'yyzl', '', '10694', '1453468096');
INSERT INTO `go_data_statistics` VALUES ('3667', '8', '一元众乐', 'yyzl', '', '10695', '1453469231');
INSERT INTO `go_data_statistics` VALUES ('3668', '8', '一元众乐', 'yyzl', '', '10696', '1453469534');
INSERT INTO `go_data_statistics` VALUES ('3669', '8', '一元众乐', 'yyzl', '', '10697', '1453470663');
INSERT INTO `go_data_statistics` VALUES ('3670', '8', '一元众乐', 'yyzl', '', '10698', '1453471577');
INSERT INTO `go_data_statistics` VALUES ('3671', '30', '开心看', 'zlkxk', '', '10699', '1453471876');
INSERT INTO `go_data_statistics` VALUES ('3672', '30', '开心看', 'zlkxk', '', '10700', '1453472107');
INSERT INTO `go_data_statistics` VALUES ('3673', '8', '一元众乐', 'yyzl', '', '10701', '1453472846');
INSERT INTO `go_data_statistics` VALUES ('3674', '30', '开心看', 'zlkxk', '', '10702', '1453473581');
INSERT INTO `go_data_statistics` VALUES ('3675', '8', '一元众乐', 'yyzl', '', '10703', '1453474691');
INSERT INTO `go_data_statistics` VALUES ('3676', '8', '一元众乐', 'yyzl', '', '10704', '1453474927');
INSERT INTO `go_data_statistics` VALUES ('3677', '8', '一元众乐', 'yyzl', '', '10705', '1453475673');
INSERT INTO `go_data_statistics` VALUES ('3678', '19', '木蚂蚁', 'mumayi', '', '10706', '1453476952');
INSERT INTO `go_data_statistics` VALUES ('3679', '8', '一元众乐', 'yyzl', '', '10708', '1453478269');
INSERT INTO `go_data_statistics` VALUES ('3680', '19', '木蚂蚁', 'mumayi', '', '10709', '1453478559');
INSERT INTO `go_data_statistics` VALUES ('3681', '8', '一元众乐', 'yyzl', '', '10710', '1453478733');
INSERT INTO `go_data_statistics` VALUES ('3682', '8', '一元众乐', 'yyzl', '', '10711', '1453478741');
INSERT INTO `go_data_statistics` VALUES ('3683', '8', '一元众乐', 'yyzl', '', '10712', '1453478773');
INSERT INTO `go_data_statistics` VALUES ('3684', '8', '一元众乐', 'yyzl', '', '10713', '1453478832');
INSERT INTO `go_data_statistics` VALUES ('3685', '21', 'oppo', 'oppo', '', '10714', '1453478890');
INSERT INTO `go_data_statistics` VALUES ('3686', '21', 'oppo', 'oppo', '', '10715', '1453478890');
INSERT INTO `go_data_statistics` VALUES ('3687', '8', '一元众乐', 'yyzl', '', '10716', '1453479045');
INSERT INTO `go_data_statistics` VALUES ('3688', '8', '一元众乐', 'yyzl', '', '10717', '1453479121');
INSERT INTO `go_data_statistics` VALUES ('3689', '8', '一元众乐', 'yyzl', '', '10718', '1453479258');
INSERT INTO `go_data_statistics` VALUES ('3690', '8', '一元众乐', 'yyzl', '', '10719', '1453479384');
INSERT INTO `go_data_statistics` VALUES ('3691', '8', '一元众乐', 'yyzl', '', '10720', '1453479429');
INSERT INTO `go_data_statistics` VALUES ('3692', '8', '一元众乐', 'yyzl', '', '10721', '1453479567');
INSERT INTO `go_data_statistics` VALUES ('3693', '8', '一元众乐', 'yyzl', '', '10722', '1453479577');
INSERT INTO `go_data_statistics` VALUES ('3694', '8', '一元众乐', 'yyzl', '', '10723', '1453479643');
INSERT INTO `go_data_statistics` VALUES ('3695', '8', '一元众乐', 'yyzl', '', '10724', '1453479722');
INSERT INTO `go_data_statistics` VALUES ('3696', '8', '一元众乐', 'yyzl', '', '10725', '1453479798');
INSERT INTO `go_data_statistics` VALUES ('3697', '8', '一元众乐', 'yyzl', '', '10726', '1453479854');
INSERT INTO `go_data_statistics` VALUES ('3698', '8', '一元众乐', 'yyzl', '', '10727', '1453479858');
INSERT INTO `go_data_statistics` VALUES ('3699', '8', '一元众乐', 'yyzl', '', '10728', '1453479975');
INSERT INTO `go_data_statistics` VALUES ('3700', '8', '一元众乐', 'yyzl', '', '10729', '1453480004');
INSERT INTO `go_data_statistics` VALUES ('3701', '8', '一元众乐', 'yyzl', '', '10730', '1453480022');
INSERT INTO `go_data_statistics` VALUES ('3702', '8', '一元众乐', 'yyzl', '', '10731', '1453480090');
INSERT INTO `go_data_statistics` VALUES ('3703', '8', '一元众乐', 'yyzl', '', '10732', '1453480218');
INSERT INTO `go_data_statistics` VALUES ('3704', '8', '一元众乐', 'yyzl', '', '10733', '1453480252');
INSERT INTO `go_data_statistics` VALUES ('3705', '8', '一元众乐', 'yyzl', '', '10734', '1453480259');
INSERT INTO `go_data_statistics` VALUES ('3706', '8', '一元众乐', 'yyzl', '', '10735', '1453480263');
INSERT INTO `go_data_statistics` VALUES ('3707', '8', '一元众乐', 'yyzl', '', '10736', '1453480380');
INSERT INTO `go_data_statistics` VALUES ('3708', '8', '一元众乐', 'yyzl', '', '10737', '1453480435');
INSERT INTO `go_data_statistics` VALUES ('3709', '8', '一元众乐', 'yyzl', '', '10738', '1453480528');
INSERT INTO `go_data_statistics` VALUES ('3710', '8', '一元众乐', 'yyzl', '', '10739', '1453480569');
INSERT INTO `go_data_statistics` VALUES ('3711', '8', '一元众乐', 'yyzl', '', '10740', '1453480600');
INSERT INTO `go_data_statistics` VALUES ('3712', '8', '一元众乐', 'yyzl', '', '10741', '1453480725');
INSERT INTO `go_data_statistics` VALUES ('3713', '8', '一元众乐', 'yyzl', '', '10742', '1453480780');
INSERT INTO `go_data_statistics` VALUES ('3714', '30', '开心看', 'zlkxk', '', '10743', '1453480837');
INSERT INTO `go_data_statistics` VALUES ('3715', '8', '一元众乐', 'yyzl', '', '10744', '1453480886');
INSERT INTO `go_data_statistics` VALUES ('3716', '8', '一元众乐', 'yyzl', '', '10745', '1453481067');
INSERT INTO `go_data_statistics` VALUES ('3717', '8', '一元众乐', 'yyzl', '', '10746', '1453481190');
INSERT INTO `go_data_statistics` VALUES ('3718', '8', '一元众乐', 'yyzl', '', '10747', '1453481332');
INSERT INTO `go_data_statistics` VALUES ('3719', '29', '小米开发者平台', 'xiaomi', '', '10748', '1453481371');
INSERT INTO `go_data_statistics` VALUES ('3720', '8', '一元众乐', 'yyzl', '', '10749', '1453482051');
INSERT INTO `go_data_statistics` VALUES ('3721', '8', '一元众乐', 'yyzl', '', '10750', '1453482083');
INSERT INTO `go_data_statistics` VALUES ('3722', '8', '一元众乐', 'yyzl', '', '10751', '1453482138');
INSERT INTO `go_data_statistics` VALUES ('3723', '8', '一元众乐', 'yyzl', '', '10752', '1453482215');
INSERT INTO `go_data_statistics` VALUES ('3724', '8', '一元众乐', 'yyzl', '', '10753', '1453482681');
INSERT INTO `go_data_statistics` VALUES ('3725', '8', '一元众乐', 'yyzl', '', '10754', '1453482801');
INSERT INTO `go_data_statistics` VALUES ('3726', '8', '一元众乐', 'yyzl', '', '10755', '1453482843');
INSERT INTO `go_data_statistics` VALUES ('3727', '8', '一元众乐', 'yyzl', '', '10756', '1453483124');
INSERT INTO `go_data_statistics` VALUES ('3728', '8', '一元众乐', 'yyzl', '', '10757', '1453483209');
INSERT INTO `go_data_statistics` VALUES ('3729', '8', '一元众乐', 'yyzl', '', '10758', '1453483394');
INSERT INTO `go_data_statistics` VALUES ('3730', '8', '一元众乐', 'yyzl', '', '10759', '1453483456');
INSERT INTO `go_data_statistics` VALUES ('3731', '8', '一元众乐', 'yyzl', '', '10760', '1453483570');
INSERT INTO `go_data_statistics` VALUES ('3732', '8', '一元众乐', 'yyzl', '', '10761', '1453483939');
INSERT INTO `go_data_statistics` VALUES ('3733', '8', '一元众乐', 'yyzl', '', '10762', '1453483980');
INSERT INTO `go_data_statistics` VALUES ('3734', '8', '一元众乐', 'yyzl', '', '10763', '1453484099');
INSERT INTO `go_data_statistics` VALUES ('3735', '8', '一元众乐', 'yyzl', '', '10764', '1453484772');
INSERT INTO `go_data_statistics` VALUES ('3736', '8', '一元众乐', 'yyzl', '', '10765', '1453485199');
INSERT INTO `go_data_statistics` VALUES ('3737', '8', '一元众乐', 'yyzl', '', '10766', '1453485228');
INSERT INTO `go_data_statistics` VALUES ('3738', '8', '一元众乐', 'yyzl', '', '10767', '1453485309');
INSERT INTO `go_data_statistics` VALUES ('3739', '8', '一元众乐', 'yyzl', '', '10768', '1453485615');
INSERT INTO `go_data_statistics` VALUES ('3740', '8', '一元众乐', 'yyzl', '', '10769', '1453485849');
INSERT INTO `go_data_statistics` VALUES ('3741', '8', '一元众乐', 'yyzl', '', '10770', '1453485870');
INSERT INTO `go_data_statistics` VALUES ('3742', '8', '一元众乐', 'yyzl', '', '10771', '1453486003');
INSERT INTO `go_data_statistics` VALUES ('3743', '8', '一元众乐', 'yyzl', '', '10772', '1453486621');
INSERT INTO `go_data_statistics` VALUES ('3744', '8', '一元众乐', 'yyzl', '', '10773', '1453486935');
INSERT INTO `go_data_statistics` VALUES ('3745', '8', '一元众乐', 'yyzl', '', '10774', '1453487000');
INSERT INTO `go_data_statistics` VALUES ('3746', '8', '一元众乐', 'yyzl', '', '10775', '1453487039');
INSERT INTO `go_data_statistics` VALUES ('3747', '8', '一元众乐', 'yyzl', '', '10776', '1453487192');
INSERT INTO `go_data_statistics` VALUES ('3748', '8', '一元众乐', 'yyzl', '', '10777', '1453487206');
INSERT INTO `go_data_statistics` VALUES ('3749', '8', '一元众乐', 'yyzl', '', '10778', '1453487576');
INSERT INTO `go_data_statistics` VALUES ('3750', '8', '一元众乐', 'yyzl', '', '10779', '1453487625');
INSERT INTO `go_data_statistics` VALUES ('3751', '8', '一元众乐', 'yyzl', '', '10780', '1453487876');
INSERT INTO `go_data_statistics` VALUES ('3752', '8', '一元众乐', 'yyzl', '', '10781', '1453489238');
INSERT INTO `go_data_statistics` VALUES ('3753', '8', '一元众乐', 'yyzl', '', '10782', '1453489250');
INSERT INTO `go_data_statistics` VALUES ('3754', '8', '一元众乐', 'yyzl', '', '10783', '1453489630');
INSERT INTO `go_data_statistics` VALUES ('3755', '8', '一元众乐', 'yyzl', '', '10784', '1453490076');
INSERT INTO `go_data_statistics` VALUES ('3756', '17', 'PP助手', 'pp', '', '10785', '1453494983');
INSERT INTO `go_data_statistics` VALUES ('3757', '8', '一元众乐', 'yyzl', '', '10786', '1453495073');
INSERT INTO `go_data_statistics` VALUES ('3758', '30', '开心看', 'zlkxk', '', '10787', '1453499299');
INSERT INTO `go_data_statistics` VALUES ('3759', '8', '一元众乐', 'yyzl', '', '10788', '1453499312');
INSERT INTO `go_data_statistics` VALUES ('3760', '8', '一元众乐', 'yyzl', '', '10789', '1453501828');
INSERT INTO `go_data_statistics` VALUES ('3761', '8', '一元众乐', 'yyzl', '', '10790', '1453503551');
INSERT INTO `go_data_statistics` VALUES ('3762', '30', '开心看', 'zlkxk', '', '10791', '1453504383');
INSERT INTO `go_data_statistics` VALUES ('3763', '30', '开心看', 'zlkxk', '', '10792', '1453506218');
INSERT INTO `go_data_statistics` VALUES ('3764', '23', '搜狗', 'sogou', '', '10793', '1453507136');
INSERT INTO `go_data_statistics` VALUES ('3765', '30', '开心看', 'zlkxk', '', '10794', '1453507180');
INSERT INTO `go_data_statistics` VALUES ('3766', '8', '一元众乐', 'yyzl', '', '10795', '1453507218');
INSERT INTO `go_data_statistics` VALUES ('3767', '30', '开心看', 'zlkxk', '', '10796', '1453508588');
INSERT INTO `go_data_statistics` VALUES ('3768', '19', '木蚂蚁', 'mumayi', '', '10797', '1453509438');
INSERT INTO `go_data_statistics` VALUES ('3769', '8', '一元众乐', 'yyzl', '', '10798', '1453509502');
INSERT INTO `go_data_statistics` VALUES ('3770', '8', '一元众乐', 'yyzl', '', '10799', '1453510930');
INSERT INTO `go_data_statistics` VALUES ('3771', '8', '一元众乐', 'yyzl', '', '10800', '1453513245');
INSERT INTO `go_data_statistics` VALUES ('3772', '8', '一元众乐', 'yyzl', '', '10801', '1453513578');
INSERT INTO `go_data_statistics` VALUES ('3773', '8', '一元众乐', 'yyzl', '', '10802', '1453514667');
INSERT INTO `go_data_statistics` VALUES ('3774', '30', '开心看', 'zlkxk', '', '10803', '1453517025');
INSERT INTO `go_data_statistics` VALUES ('3775', '30', '开心看', 'zlkxk', '', '10804', '1453519537');
INSERT INTO `go_data_statistics` VALUES ('3776', '16', '豌豆荚', 'wandoujia', '', '10805', '1453519761');
INSERT INTO `go_data_statistics` VALUES ('3777', '8', '一元众乐', 'yyzl', '', '10806', '1453520226');
INSERT INTO `go_data_statistics` VALUES ('3778', '8', '一元众乐', 'yyzl', '', '10807', '1453524197');
INSERT INTO `go_data_statistics` VALUES ('3779', '19', '木蚂蚁', 'mumayi', '', '10808', '1453524361');
INSERT INTO `go_data_statistics` VALUES ('3780', '30', '开心看', 'zlkxk', '', '10809', '1453524433');
INSERT INTO `go_data_statistics` VALUES ('3781', '8', '一元众乐', 'yyzl', '', '10810', '1453524965');
INSERT INTO `go_data_statistics` VALUES ('3782', '30', '开心看', 'zlkxk', '', '10811', '1453525619');
INSERT INTO `go_data_statistics` VALUES ('3783', '30', '开心看', 'zlkxk', '', '10816', '1453527597');
INSERT INTO `go_data_statistics` VALUES ('3784', '8', '一元众乐', 'yyzl', '', '10817', '1453527978');
INSERT INTO `go_data_statistics` VALUES ('3785', '8', '一元众乐', 'yyzl', '', '10818', '1453531050');
INSERT INTO `go_data_statistics` VALUES ('3786', '8', '一元众乐', 'yyzl', '', '10819', '1453531643');
INSERT INTO `go_data_statistics` VALUES ('3787', '17', 'PP助手', 'pp', '', '10820', '1453535360');
INSERT INTO `go_data_statistics` VALUES ('3788', '8', '一元众乐', 'yyzl', '', '10821', '1453535897');
INSERT INTO `go_data_statistics` VALUES ('3789', '19', '木蚂蚁', 'mumayi', '', '10822', '1453537896');
INSERT INTO `go_data_statistics` VALUES ('3790', '8', '一元众乐', 'yyzl', '', '10823', '1453538558');
INSERT INTO `go_data_statistics` VALUES ('3791', '19', '木蚂蚁', 'mumayi', '', '10824', '1453543995');
INSERT INTO `go_data_statistics` VALUES ('3792', '8', '一元众乐', 'yyzl', '', '10825', '1453544832');
INSERT INTO `go_data_statistics` VALUES ('3793', '8', '一元众乐', 'yyzl', '', '10827', '1453551369');
INSERT INTO `go_data_statistics` VALUES ('3794', '8', '一元众乐', 'yyzl', '', '10828', '1453551503');
INSERT INTO `go_data_statistics` VALUES ('3795', '8', '一元众乐', 'yyzl', '', '10829', '1453555678');
INSERT INTO `go_data_statistics` VALUES ('3796', '29', '小米开发者平台', 'xiaomi', '', '10830', '1453558358');
INSERT INTO `go_data_statistics` VALUES ('3797', '8', '一元众乐', 'yyzl', '', '10831', '1453558414');
INSERT INTO `go_data_statistics` VALUES ('3798', '24', '魅族', 'meizu', '', '10832', '1453561503');
INSERT INTO `go_data_statistics` VALUES ('3799', '8', '一元众乐', 'yyzl', '', '10833', '1453561900');
INSERT INTO `go_data_statistics` VALUES ('3800', '8', '一元众乐', 'yyzl', '', '10834', '1453564965');
INSERT INTO `go_data_statistics` VALUES ('3801', '8', '一元众乐', 'yyzl', '', '10835', '1453565014');
INSERT INTO `go_data_statistics` VALUES ('3802', '8', '一元众乐', 'yyzl', '', '10836', '1453565429');
INSERT INTO `go_data_statistics` VALUES ('3803', '8', '一元众乐', 'yyzl', '', '10837', '1453565577');
INSERT INTO `go_data_statistics` VALUES ('3804', '8', '一元众乐', 'yyzl', '', '10838', '1453565681');
INSERT INTO `go_data_statistics` VALUES ('3805', '8', '一元众乐', 'yyzl', '', '10839', '1453565822');
INSERT INTO `go_data_statistics` VALUES ('3806', '8', '一元众乐', 'yyzl', '', '10840', '1453565841');
INSERT INTO `go_data_statistics` VALUES ('3807', '8', '一元众乐', 'yyzl', '', '10841', '1453565894');
INSERT INTO `go_data_statistics` VALUES ('3808', '8', '一元众乐', 'yyzl', '', '10842', '1453565934');
INSERT INTO `go_data_statistics` VALUES ('3809', '8', '一元众乐', 'yyzl', '', '10843', '1453566069');
INSERT INTO `go_data_statistics` VALUES ('3810', '8', '一元众乐', 'yyzl', '', '10844', '1453566144');
INSERT INTO `go_data_statistics` VALUES ('3811', '8', '一元众乐', 'yyzl', '', '10845', '1453566156');
INSERT INTO `go_data_statistics` VALUES ('3812', '8', '一元众乐', 'yyzl', '', '10846', '1453566415');
INSERT INTO `go_data_statistics` VALUES ('3813', '8', '一元众乐', 'yyzl', '', '10847', '1453566458');
INSERT INTO `go_data_statistics` VALUES ('3814', '8', '一元众乐', 'yyzl', '', '10848', '1453566469');
INSERT INTO `go_data_statistics` VALUES ('3815', '8', '一元众乐', 'yyzl', '', '10849', '1453566684');
INSERT INTO `go_data_statistics` VALUES ('3816', '8', '一元众乐', 'yyzl', '', '10850', '1453566713');
INSERT INTO `go_data_statistics` VALUES ('3817', '8', '一元众乐', 'yyzl', '', '10851', '1453566868');
INSERT INTO `go_data_statistics` VALUES ('3818', '8', '一元众乐', 'yyzl', '', '10852', '1453567318');
INSERT INTO `go_data_statistics` VALUES ('3819', '8', '一元众乐', 'yyzl', '', '10853', '1453567523');
INSERT INTO `go_data_statistics` VALUES ('3820', '8', '一元众乐', 'yyzl', '', '10854', '1453568432');
INSERT INTO `go_data_statistics` VALUES ('3821', '8', '一元众乐', 'yyzl', '', '10855', '1453568484');
INSERT INTO `go_data_statistics` VALUES ('3822', '8', '一元众乐', 'yyzl', '', '10856', '1453568485');
INSERT INTO `go_data_statistics` VALUES ('3823', '8', '一元众乐', 'yyzl', '', '10857', '1453568672');
INSERT INTO `go_data_statistics` VALUES ('3824', '8', '一元众乐', 'yyzl', '', '10858', '1453568743');
INSERT INTO `go_data_statistics` VALUES ('3825', '8', '一元众乐', 'yyzl', '', '10859', '1453568775');
INSERT INTO `go_data_statistics` VALUES ('3826', '8', '一元众乐', 'yyzl', '', '10860', '1453568850');
INSERT INTO `go_data_statistics` VALUES ('3827', '8', '一元众乐', 'yyzl', '', '10861', '1453569152');
INSERT INTO `go_data_statistics` VALUES ('3828', '8', '一元众乐', 'yyzl', '', '10862', '1453569456');
INSERT INTO `go_data_statistics` VALUES ('3829', '8', '一元众乐', 'yyzl', '', '10863', '1453569472');
INSERT INTO `go_data_statistics` VALUES ('3830', '8', '一元众乐', 'yyzl', '', '10864', '1453569475');
INSERT INTO `go_data_statistics` VALUES ('3831', '8', '一元众乐', 'yyzl', '', '10865', '1453569885');
INSERT INTO `go_data_statistics` VALUES ('3832', '8', '一元众乐', 'yyzl', '', '10866', '1453569893');
INSERT INTO `go_data_statistics` VALUES ('3833', '8', '一元众乐', 'yyzl', '', '10867', '1453570150');
INSERT INTO `go_data_statistics` VALUES ('3834', '8', '一元众乐', 'yyzl', '', '10868', '1453570352');
INSERT INTO `go_data_statistics` VALUES ('3835', '8', '一元众乐', 'yyzl', '', '10869', '1453570498');
INSERT INTO `go_data_statistics` VALUES ('3836', '8', '一元众乐', 'yyzl', '', '10870', '1453570706');
INSERT INTO `go_data_statistics` VALUES ('3837', '8', '一元众乐', 'yyzl', '', '10871', '1453570734');
INSERT INTO `go_data_statistics` VALUES ('3838', '8', '一元众乐', 'yyzl', '', '10872', '1453571054');
INSERT INTO `go_data_statistics` VALUES ('3839', '8', '一元众乐', 'yyzl', '', '10873', '1453571277');
INSERT INTO `go_data_statistics` VALUES ('3840', '8', '一元众乐', 'yyzl', '', '10874', '1453571305');
INSERT INTO `go_data_statistics` VALUES ('3841', '8', '一元众乐', 'yyzl', '', '10875', '1453571582');
INSERT INTO `go_data_statistics` VALUES ('3842', '8', '一元众乐', 'yyzl', '', '10876', '1453571636');
INSERT INTO `go_data_statistics` VALUES ('3843', '8', '一元众乐', 'yyzl', '', '10877', '1453572111');
INSERT INTO `go_data_statistics` VALUES ('3844', '8', '一元众乐', 'yyzl', '', '10878', '1453572214');
INSERT INTO `go_data_statistics` VALUES ('3845', '8', '一元众乐', 'yyzl', '', '10879', '1453572284');
INSERT INTO `go_data_statistics` VALUES ('3846', '8', '一元众乐', 'yyzl', '', '10880', '1453572469');
INSERT INTO `go_data_statistics` VALUES ('3847', '8', '一元众乐', 'yyzl', '', '10881', '1453573423');
INSERT INTO `go_data_statistics` VALUES ('3848', '8', '一元众乐', 'yyzl', '', '10882', '1453573495');
INSERT INTO `go_data_statistics` VALUES ('3849', '8', '一元众乐', 'yyzl', '', '10883', '1453573574');
INSERT INTO `go_data_statistics` VALUES ('3850', '8', '一元众乐', 'yyzl', '', '10884', '1453573977');
INSERT INTO `go_data_statistics` VALUES ('3851', '8', '一元众乐', 'yyzl', '', '10885', '1453573984');
INSERT INTO `go_data_statistics` VALUES ('3852', '8', '一元众乐', 'yyzl', '', '10886', '1453574328');
INSERT INTO `go_data_statistics` VALUES ('3853', '8', '一元众乐', 'yyzl', '', '10887', '1453574739');
INSERT INTO `go_data_statistics` VALUES ('3854', '8', '一元众乐', 'yyzl', '', '10888', '1453574957');
INSERT INTO `go_data_statistics` VALUES ('3855', '8', '一元众乐', 'yyzl', '', '10889', '1453593014');
INSERT INTO `go_data_statistics` VALUES ('3856', '19', '木蚂蚁', 'mumayi', '', '10890', '1453595313');
INSERT INTO `go_data_statistics` VALUES ('3857', '8', '一元众乐', 'yyzl', '', '10891', '1453595968');
INSERT INTO `go_data_statistics` VALUES ('3858', '8', '一元众乐', 'yyzl', '', '10892', '1453597814');
INSERT INTO `go_data_statistics` VALUES ('3859', '8', '一元众乐', 'yyzl', '', '10893', '1453599192');
INSERT INTO `go_data_statistics` VALUES ('3860', '2', '儒豹浏览器', 'rbower', '', '10894', '1453601716');
INSERT INTO `go_data_statistics` VALUES ('3861', '8', '一元众乐', 'yyzl', '', '10895', '1453602262');
INSERT INTO `go_data_statistics` VALUES ('3862', '8', '一元众乐', 'yyzl', '', '10896', '1453612500');
INSERT INTO `go_data_statistics` VALUES ('3863', '14', '360手机助手', 'dev360', '', '10897', '1453612744');
INSERT INTO `go_data_statistics` VALUES ('3864', '8', '一元众乐', 'yyzl', '', '10900', '1453632980');
INSERT INTO `go_data_statistics` VALUES ('3865', '8', '一元众乐', 'yyzl', '', '10902', '1453633986');
INSERT INTO `go_data_statistics` VALUES ('3866', '8', '一元众乐', 'yyzl', '', '10906', '1453637006');
INSERT INTO `go_data_statistics` VALUES ('3867', '8', '一元众乐', 'yyzl', '', '10909', '1453639405');
INSERT INTO `go_data_statistics` VALUES ('3868', '8', '一元众乐', 'yyzl', '', '10912', '1453639949');
INSERT INTO `go_data_statistics` VALUES ('3869', '8', '一元众乐', 'yyzl', '', '10916', '1453640772');
INSERT INTO `go_data_statistics` VALUES ('3870', '14', '360手机助手', 'dev360', '', '10918', '1453641677');
INSERT INTO `go_data_statistics` VALUES ('3871', '8', '一元众乐', 'yyzl', '', '10919', '1453642325');
INSERT INTO `go_data_statistics` VALUES ('3872', '19', '木蚂蚁', 'mumayi', '', '10921', '1453644460');
INSERT INTO `go_data_statistics` VALUES ('3873', '29', '小米开发者平台', 'xiaomi', '', '10924', '1453648880');
INSERT INTO `go_data_statistics` VALUES ('3874', '8', '一元众乐', 'yyzl', '', '10925', '1453649320');
INSERT INTO `go_data_statistics` VALUES ('3875', '8', '一元众乐', 'yyzl', '', '10926', '1453650619');
INSERT INTO `go_data_statistics` VALUES ('3876', '8', '一元众乐', 'yyzl', '', '10927', '1453652034');
INSERT INTO `go_data_statistics` VALUES ('3877', '8', '一元众乐', 'yyzl', '', '10928', '1453652318');
INSERT INTO `go_data_statistics` VALUES ('3878', '8', '一元众乐', 'yyzl', '', '10929', '1453654475');
INSERT INTO `go_data_statistics` VALUES ('3879', '8', '一元众乐', 'yyzl', '', '10930', '1453659347');
INSERT INTO `go_data_statistics` VALUES ('3880', '8', '一元众乐', 'yyzl', '', '10932', '1453676634');
INSERT INTO `go_data_statistics` VALUES ('3881', '8', '一元众乐', 'yyzl', '', '10933', '1453687342');
INSERT INTO `go_data_statistics` VALUES ('3882', '8', '一元众乐', 'yyzl', '', '10934', '1453688286');
INSERT INTO `go_data_statistics` VALUES ('3883', '29', '小米开发者平台', 'xiaomi', '', '10935', '1453690089');
INSERT INTO `go_data_statistics` VALUES ('3884', '14', '360手机助手', 'dev360', '', '10938', '1453698569');
INSERT INTO `go_data_statistics` VALUES ('3885', '8', '一元众乐', 'yyzl', '', '10939', '1453700227');
INSERT INTO `go_data_statistics` VALUES ('3886', '8', '一元众乐', 'yyzl', '', '10940', '1453700439');
INSERT INTO `go_data_statistics` VALUES ('3887', '19', '木蚂蚁', 'mumayi', '', '10941', '1453705603');
INSERT INTO `go_data_statistics` VALUES ('3888', '8', '一元众乐', 'yyzl', '', '10942', '1453712722');
INSERT INTO `go_data_statistics` VALUES ('3889', '8', '一元众乐', 'yyzl', '', '10943', '1453713017');
INSERT INTO `go_data_statistics` VALUES ('3890', '8', '一元众乐', 'yyzl', '', '10944', '1453717074');
INSERT INTO `go_data_statistics` VALUES ('3891', '19', '木蚂蚁', 'mumayi', '', '10945', '1453718787');
INSERT INTO `go_data_statistics` VALUES ('3892', '14', '360手机助手', 'dev360', '', '10946', '1453721118');
INSERT INTO `go_data_statistics` VALUES ('3893', '14', '360手机助手', 'dev360', '', '10948', '1453725320');
INSERT INTO `go_data_statistics` VALUES ('3894', '19', '木蚂蚁', 'mumayi', '', '10951', '1453731120');
INSERT INTO `go_data_statistics` VALUES ('3895', '19', '木蚂蚁', 'mumayi', '', '10952', '1453731120');
INSERT INTO `go_data_statistics` VALUES ('3896', '19', '木蚂蚁', 'mumayi', '', '10954', '1453732611');
INSERT INTO `go_data_statistics` VALUES ('3897', '8', '一元众乐', 'yyzl', '', '10955', '1453733836');
INSERT INTO `go_data_statistics` VALUES ('3898', '8', '一元众乐', 'yyzl', '', '10956', '1453738794');
INSERT INTO `go_data_statistics` VALUES ('3899', '8', '一元众乐', 'yyzl', '', '10957', '1453744265');
INSERT INTO `go_data_statistics` VALUES ('3900', '8', '一元众乐', 'yyzl', '', '10958', '1453751706');
INSERT INTO `go_data_statistics` VALUES ('3901', '19', '木蚂蚁', 'mumayi', '', '10959', '1453763416');
INSERT INTO `go_data_statistics` VALUES ('3902', '21', 'oppo', 'oppo', '', '10960', '1453764489');
INSERT INTO `go_data_statistics` VALUES ('3903', '8', '一元众乐', 'yyzl', '', '10961', '1453767432');
INSERT INTO `go_data_statistics` VALUES ('3904', '29', '小米开发者平台', 'xiaomi', '', '10962', '1453767914');
INSERT INTO `go_data_statistics` VALUES ('3905', '8', '一元众乐', 'yyzl', '', '10963', '1453770862');
INSERT INTO `go_data_statistics` VALUES ('3906', '8', '一元众乐', 'yyzl', '', '10964', '1453772075');
INSERT INTO `go_data_statistics` VALUES ('3907', '6', '信号增强器', 'zlxhzqq', '', '10965', '1453776693');
INSERT INTO `go_data_statistics` VALUES ('3908', '6', '信号增强器', 'zlxhzqq', '', '10966', '1453779185');
INSERT INTO `go_data_statistics` VALUES ('3909', '8', '一元众乐', 'yyzl', '', '10967', '1453779544');
INSERT INTO `go_data_statistics` VALUES ('3910', '6', '信号增强器', 'zlxhzqq', '', '10971', '1453796049');
INSERT INTO `go_data_statistics` VALUES ('3911', '6', '信号增强器', 'zlxhzqq', '', '10972', '1453798714');
INSERT INTO `go_data_statistics` VALUES ('3912', '29', '小米开发者平台', 'xiaomi', '', '10973', '1453802319');
INSERT INTO `go_data_statistics` VALUES ('3913', '6', '信号增强器', 'zlxhzqq', '', '10974', '1453804681');
INSERT INTO `go_data_statistics` VALUES ('3914', '6', '信号增强器', 'zlxhzqq', '', '10975', '1453805154');
INSERT INTO `go_data_statistics` VALUES ('3915', '8', '一元众乐', 'yyzl', '', '10976', '1453808789');
INSERT INTO `go_data_statistics` VALUES ('3916', '8', '一元众乐', 'yyzl', '', '10977', '1453809073');
INSERT INTO `go_data_statistics` VALUES ('3917', '6', '信号增强器', 'zlxhzqq', '', '10978', '1453810922');
INSERT INTO `go_data_statistics` VALUES ('3918', '6', '信号增强器', 'zlxhzqq', '', '10979', '1453813170');
INSERT INTO `go_data_statistics` VALUES ('3919', '6', '信号增强器', 'zlxhzqq', '', '10980', '1453816282');
INSERT INTO `go_data_statistics` VALUES ('3920', '29', '小米开发者平台', 'xiaomi', '', '10981', '1453816363');
INSERT INTO `go_data_statistics` VALUES ('3921', '6', '信号增强器', 'zlxhzqq', '', '10982', '1453816762');
INSERT INTO `go_data_statistics` VALUES ('3922', '6', '信号增强器', 'zlxhzqq', '', '10983', '1453817046');
INSERT INTO `go_data_statistics` VALUES ('3923', '8', '一元众乐', 'yyzl', '', '10984', '1453819717');
INSERT INTO `go_data_statistics` VALUES ('3924', '8', '一元众乐', 'yyzl', '', '10986', '1453827139');
INSERT INTO `go_data_statistics` VALUES ('3925', '6', '信号增强器', 'zlxhzqq', '', '10987', '1453827559');
INSERT INTO `go_data_statistics` VALUES ('3926', '21', 'oppo', 'oppo', '', '10988', '1453828052');
INSERT INTO `go_data_statistics` VALUES ('3927', '6', '信号增强器', 'zlxhzqq', '', '10989', '1453832720');
INSERT INTO `go_data_statistics` VALUES ('3928', '8', '一元众乐', 'yyzl', '', '10990', '1453833791');
INSERT INTO `go_data_statistics` VALUES ('3929', '6', '信号增强器', 'zlxhzqq', '', '10991', '1453835101');
INSERT INTO `go_data_statistics` VALUES ('3930', '24', '魅族', 'meizu', '', '10992', '1453839873');
INSERT INTO `go_data_statistics` VALUES ('3931', '6', '信号增强器', 'zlxhzqq', '', '10993', '1453845882');
INSERT INTO `go_data_statistics` VALUES ('3932', '6', '信号增强器', 'zlxhzqq', '', '10994', '1453850483');
INSERT INTO `go_data_statistics` VALUES ('3933', '8', '一元众乐', 'yyzl', '', '10995', '1453850592');
INSERT INTO `go_data_statistics` VALUES ('3934', '21', 'oppo', 'oppo', '', '10996', '1453851043');
INSERT INTO `go_data_statistics` VALUES ('3935', '8', '一元众乐', 'yyzl', '', '10997', '1453851281');
INSERT INTO `go_data_statistics` VALUES ('3936', '8', '一元众乐', 'yyzl', '', '10998', '1453852025');
INSERT INTO `go_data_statistics` VALUES ('3937', '6', '信号增强器', 'zlxhzqq', '', '10999', '1453853340');
INSERT INTO `go_data_statistics` VALUES ('3938', '6', '信号增强器', 'zlxhzqq', '', '11000', '1453858313');
INSERT INTO `go_data_statistics` VALUES ('3939', '6', '信号增强器', 'zlxhzqq', '', '11001', '1453869542');
INSERT INTO `go_data_statistics` VALUES ('3940', '6', '信号增强器', 'zlxhzqq', '', '11002', '1453873755');
INSERT INTO `go_data_statistics` VALUES ('3941', '2', '儒豹浏览器', 'rbower', '', '11003', '1453875903');
INSERT INTO `go_data_statistics` VALUES ('3942', '24', '魅族', 'meizu', '', '11004', '1453876941');
INSERT INTO `go_data_statistics` VALUES ('3943', '21', 'oppo', 'oppo', '', '11005', '1453880490');
INSERT INTO `go_data_statistics` VALUES ('3944', '8', '一元众乐', 'yyzl', '', '11009', '1453885794');
INSERT INTO `go_data_statistics` VALUES ('3945', '6', '信号增强器', 'zlxhzqq', '', '11010', '1453885838');
INSERT INTO `go_data_statistics` VALUES ('3946', '6', '信号增强器', 'zlxhzqq', '', '11011', '1453896264');
INSERT INTO `go_data_statistics` VALUES ('3947', '8', '一元众乐', 'yyzl', '', '11012', '1453902509');
INSERT INTO `go_data_statistics` VALUES ('3948', '8', '一元众乐', 'yyzl', '', '11013', '1453902650');
INSERT INTO `go_data_statistics` VALUES ('3949', '6', '信号增强器', 'zlxhzqq', '', '11014', '1453903676');
INSERT INTO `go_data_statistics` VALUES ('3950', '8', '一元众乐', 'yyzl', '', '11015', '1453904019');
INSERT INTO `go_data_statistics` VALUES ('3951', '6', '信号增强器', 'zlxhzqq', '', '11016', '1453904437');
INSERT INTO `go_data_statistics` VALUES ('3952', '6', '信号增强器', 'zlxhzqq', '', '11017', '1453904493');
INSERT INTO `go_data_statistics` VALUES ('3953', '8', '一元众乐', 'yyzl', '', '11018', '1453905973');
INSERT INTO `go_data_statistics` VALUES ('3954', '19', '木蚂蚁', 'mumayi', '', '11019', '1453906254');
INSERT INTO `go_data_statistics` VALUES ('3955', '6', '信号增强器', 'zlxhzqq', '', '11020', '1453906327');
INSERT INTO `go_data_statistics` VALUES ('3956', '8', '一元众乐', 'yyzl', '', '11021', '1453910741');
INSERT INTO `go_data_statistics` VALUES ('3957', '28', '安智开发者联盟', 'anzhi', '', '11023', '1453912870');
INSERT INTO `go_data_statistics` VALUES ('3958', '8', '一元众乐', 'yyzl', '', '11024', '1453912912');
INSERT INTO `go_data_statistics` VALUES ('3959', '6', '信号增强器', 'zlxhzqq', '', '11025', '1453922724');
INSERT INTO `go_data_statistics` VALUES ('3960', '21', 'oppo', 'oppo', '', '11026', '1453929064');
INSERT INTO `go_data_statistics` VALUES ('3961', '8', '一元众乐', 'yyzl', '', '11027', '1453929975');
INSERT INTO `go_data_statistics` VALUES ('3962', '6', '信号增强器', 'zlxhzqq', '', '11028', '1453933166');
INSERT INTO `go_data_statistics` VALUES ('3963', '6', '信号增强器', 'zlxhzqq', '', '11029', '1453934910');
INSERT INTO `go_data_statistics` VALUES ('3964', '6', '信号增强器', 'zlxhzqq', '', '11030', '1453937656');
INSERT INTO `go_data_statistics` VALUES ('3965', '6', '信号增强器', 'zlxhzqq', '', '11031', '1453938616');
INSERT INTO `go_data_statistics` VALUES ('3966', '6', '信号增强器', 'zlxhzqq', '', '11032', '1453939009');
INSERT INTO `go_data_statistics` VALUES ('3967', '6', '信号增强器', 'zlxhzqq', '', '11033', '1453939744');
INSERT INTO `go_data_statistics` VALUES ('3968', '6', '信号增强器', 'zlxhzqq', '', '11034', '1453941588');
INSERT INTO `go_data_statistics` VALUES ('3969', '6', '信号增强器', 'zlxhzqq', '', '11035', '1453941591');
INSERT INTO `go_data_statistics` VALUES ('3970', '6', '信号增强器', 'zlxhzqq', '', '11036', '1453942664');
INSERT INTO `go_data_statistics` VALUES ('3971', '8', '一元众乐', 'yyzl', '', '11037', '1453943910');
INSERT INTO `go_data_statistics` VALUES ('3972', '6', '信号增强器', 'zlxhzqq', '', '11038', '1453945929');
INSERT INTO `go_data_statistics` VALUES ('3973', '8', '一元众乐', 'yyzl', '', '11039', '1453946191');
INSERT INTO `go_data_statistics` VALUES ('3974', '8', '一元众乐', 'yyzl', '', '11040', '1453951476');
INSERT INTO `go_data_statistics` VALUES ('3975', '6', '信号增强器', 'zlxhzqq', '', '11041', '1453951591');
INSERT INTO `go_data_statistics` VALUES ('3976', '8', '一元众乐', 'yyzl', '', '11042', '1453951604');
INSERT INTO `go_data_statistics` VALUES ('3977', '14', '360手机助手', 'dev360', '', '11043', '1453953043');
INSERT INTO `go_data_statistics` VALUES ('3978', '24', '魅族', 'meizu', '', '11044', '1453955129');
INSERT INTO `go_data_statistics` VALUES ('3979', '6', '信号增强器', 'zlxhzqq', '', '11045', '1453958616');
INSERT INTO `go_data_statistics` VALUES ('3980', '8', '一元众乐', 'yyzl', '', '11046', '1453961579');
INSERT INTO `go_data_statistics` VALUES ('3981', '29', '小米开发者平台', 'xiaomi', '', '11048', '1453977971');
INSERT INTO `go_data_statistics` VALUES ('3982', '6', '信号增强器', 'zlxhzqq', '', '11049', '1453981329');
INSERT INTO `go_data_statistics` VALUES ('3983', '6', '信号增强器', 'zlxhzqq', '', '11050', '1453981701');
INSERT INTO `go_data_statistics` VALUES ('3984', '8', '一元众乐', 'yyzl', '', '11051', '1453982135');
INSERT INTO `go_data_statistics` VALUES ('3985', '8', '一元众乐', 'yyzl', '', '11052', '1453987790');
INSERT INTO `go_data_statistics` VALUES ('3986', '8', '一元众乐', 'yyzl', '', '11053', '1453987800');
INSERT INTO `go_data_statistics` VALUES ('3987', '29', '小米开发者平台', 'xiaomi', '', '11054', '1453987974');
INSERT INTO `go_data_statistics` VALUES ('3988', '6', '信号增强器', 'zlxhzqq', '', '11055', '1453994394');
INSERT INTO `go_data_statistics` VALUES ('3989', '6', '信号增强器', 'zlxhzqq', '', '11057', '1453999947');
INSERT INTO `go_data_statistics` VALUES ('3990', '8', '一元众乐', 'yyzl', '', '11058', '1454006990');
INSERT INTO `go_data_statistics` VALUES ('3991', '9', '午夜艳视', 'wuyeyanshi', '', '11059', '1454009574');
INSERT INTO `go_data_statistics` VALUES ('3992', '17', 'PP助手', 'pp', '', '11060', '1454018342');
INSERT INTO `go_data_statistics` VALUES ('3993', '29', '小米开发者平台', 'xiaomi', '', '11061', '1454021762');
INSERT INTO `go_data_statistics` VALUES ('3994', '8', '一元众乐', 'yyzl', '', '11062', '1454022934');
INSERT INTO `go_data_statistics` VALUES ('3995', '2', '儒豹浏览器', 'rbower', '', '11063', '1454023851');
INSERT INTO `go_data_statistics` VALUES ('3996', '2', '儒豹浏览器', 'rbower', '', '11064', '1454023870');
INSERT INTO `go_data_statistics` VALUES ('3997', '8', '一元众乐', 'yyzl', '', '11065', '1454032202');
INSERT INTO `go_data_statistics` VALUES ('3998', '17', 'PP助手', 'pp', '', '11066', '1454034041');
INSERT INTO `go_data_statistics` VALUES ('3999', '8', '一元众乐', 'yyzl', '', '11067', '1454037871');
INSERT INTO `go_data_statistics` VALUES ('4000', '8', '一元众乐', 'yyzl', '', '11068', '1454042244');
INSERT INTO `go_data_statistics` VALUES ('4001', '8', '一元众乐', 'yyzl', '', '11069', '1454043171');
INSERT INTO `go_data_statistics` VALUES ('4002', '19', '木蚂蚁', 'mumayi', '', '11070', '1454049906');
INSERT INTO `go_data_statistics` VALUES ('4003', '8', '一元众乐', 'yyzl', '', '11071', '1454051022');
INSERT INTO `go_data_statistics` VALUES ('4004', '2', '儒豹浏览器', 'rbower', '', '11072', '1454052142');
INSERT INTO `go_data_statistics` VALUES ('4005', '8', '一元众乐', 'yyzl', '', '11073', '1454076743');
INSERT INTO `go_data_statistics` VALUES ('4006', '19', '木蚂蚁', 'mumayi', '', '11074', '1454082373');
INSERT INTO `go_data_statistics` VALUES ('4007', '8', '一元众乐', 'yyzl', '', '11075', '1454083949');
INSERT INTO `go_data_statistics` VALUES ('4008', '8', '一元众乐', 'yyzl', '', '11076', '1454089263');
INSERT INTO `go_data_statistics` VALUES ('4009', '6', '信号增强器', 'zlxhzqq', '', '11077', '1454118045');
INSERT INTO `go_data_statistics` VALUES ('4010', '16', '豌豆荚', 'wandoujia', '', '11078', '1454121865');
INSERT INTO `go_data_statistics` VALUES ('4011', '8', '一元众乐', 'yyzl', '', '11079', '1454128870');
INSERT INTO `go_data_statistics` VALUES ('4012', '24', '魅族', 'meizu', '', '11080', '1454142362');
INSERT INTO `go_data_statistics` VALUES ('4013', '29', '小米开发者平台', 'xiaomi', '', '11081', '1454144795');
INSERT INTO `go_data_statistics` VALUES ('4014', '8', '一元众乐', 'yyzl', '', '11082', '1454150070');
INSERT INTO `go_data_statistics` VALUES ('4015', '15', '百度', 'baidu', '', '11083', '1454163823');
INSERT INTO `go_data_statistics` VALUES ('4016', '29', '小米开发者平台', 'xiaomi', '', '11085', '1454171484');
INSERT INTO `go_data_statistics` VALUES ('4017', '8', '一元众乐', 'yyzl', '', '11087', '1454205657');
INSERT INTO `go_data_statistics` VALUES ('4018', '16', '豌豆荚', 'wandoujia', '', '11089', '1454234139');
INSERT INTO `go_data_statistics` VALUES ('4019', '8', '一元众乐', 'yyzl', '', '11090', '1454237551');
INSERT INTO `go_data_statistics` VALUES ('4020', '16', '豌豆荚', 'wandoujia', '', '11091', '1454247869');
INSERT INTO `go_data_statistics` VALUES ('4021', '8', '一元众乐', 'yyzl', '', '11092', '1454251280');
INSERT INTO `go_data_statistics` VALUES ('4022', '17', 'PP助手', 'pp', '', '11094', '1454299682');
INSERT INTO `go_data_statistics` VALUES ('4023', '8', '一元众乐', 'yyzl', '', '11095', '1454329177');
INSERT INTO `go_data_statistics` VALUES ('4024', '17', 'PP助手', 'pp', '', '11096', '1454332291');
INSERT INTO `go_data_statistics` VALUES ('4025', '21', 'oppo', 'oppo', '', '11097', '1454356215');
INSERT INTO `go_data_statistics` VALUES ('4026', '8', '一元众乐', 'yyzl', '', '11098', '1454387193');
INSERT INTO `go_data_statistics` VALUES ('4027', '16', '豌豆荚', 'wandoujia', '', '11099', '1454387987');
INSERT INTO `go_data_statistics` VALUES ('4028', '19', '木蚂蚁', 'mumayi', '', '11100', '1454393687');
INSERT INTO `go_data_statistics` VALUES ('4029', '21', 'oppo', 'oppo', '', '11101', '1454397281');
INSERT INTO `go_data_statistics` VALUES ('4030', '8', '一元众乐', 'yyzl', '', '11102', '1454397821');
INSERT INTO `go_data_statistics` VALUES ('4031', '8', '一元众乐', 'yyzl', '', '11103', '1454400810');
INSERT INTO `go_data_statistics` VALUES ('4032', '29', '小米开发者平台', 'xiaomi', '', '11104', '1454407133');
INSERT INTO `go_data_statistics` VALUES ('4033', '16', '豌豆荚', 'wandoujia', '', '11105', '1454409052');
INSERT INTO `go_data_statistics` VALUES ('4034', '8', '一元众乐', 'yyzl', '', '11106', '1454412131');
INSERT INTO `go_data_statistics` VALUES ('4035', '8', '一元众乐', 'yyzl', '', '11107', '1454417791');
INSERT INTO `go_data_statistics` VALUES ('4036', '20', '联想', 'lenovo', '', '11108', '1454422235');
INSERT INTO `go_data_statistics` VALUES ('4037', '8', '一元众乐', 'yyzl', '', '11109', '1454425790');
INSERT INTO `go_data_statistics` VALUES ('4038', '16', '豌豆荚', 'wandoujia', '', '11111', '1454430690');
INSERT INTO `go_data_statistics` VALUES ('4039', '8', '一元众乐', 'yyzl', '', '11112', '1454434207');
INSERT INTO `go_data_statistics` VALUES ('4040', '8', '一元众乐', 'yyzl', '', '11115', '1454469118');
INSERT INTO `go_data_statistics` VALUES ('4041', '16', '豌豆荚', 'wandoujia', '', '11116', '1454478399');
INSERT INTO `go_data_statistics` VALUES ('4042', '8', '一元众乐', 'yyzl', '', '11117', '1454480815');
INSERT INTO `go_data_statistics` VALUES ('4043', '29', '小米开发者平台', 'xiaomi', '', '11119', '1454502615');
INSERT INTO `go_data_statistics` VALUES ('4044', '21', 'oppo', 'oppo', '', '11120', '1454505306');
INSERT INTO `go_data_statistics` VALUES ('4045', '15', '百度', 'baidu', '', '11121', '1454512876');
INSERT INTO `go_data_statistics` VALUES ('4046', '8', '一元众乐', 'yyzl', '', '11122', '1454542565');
INSERT INTO `go_data_statistics` VALUES ('4047', '29', '小米开发者平台', 'xiaomi', '', '11123', '1454549455');
INSERT INTO `go_data_statistics` VALUES ('4048', '24', '魅族', 'meizu', '', '11124', '1454556374');
INSERT INTO `go_data_statistics` VALUES ('4049', '8', '一元众乐', 'yyzl', '', '11125', '1454576541');
INSERT INTO `go_data_statistics` VALUES ('4050', '8', '一元众乐', 'yyzl', '', '11126', '1454581201');
INSERT INTO `go_data_statistics` VALUES ('4051', '29', '小米开发者平台', 'xiaomi', '', '11127', '1454594543');
INSERT INTO `go_data_statistics` VALUES ('4052', '19', '木蚂蚁', 'mumayi', '', '11128', '1454595761');
INSERT INTO `go_data_statistics` VALUES ('4053', '16', '豌豆荚', 'wandoujia', '', '11129', '1454602823');
INSERT INTO `go_data_statistics` VALUES ('4054', '8', '一元众乐', 'yyzl', '', '11130', '1454605435');
INSERT INTO `go_data_statistics` VALUES ('4055', '8', '一元众乐', 'yyzl', '', '11131', '1454618269');
INSERT INTO `go_data_statistics` VALUES ('4056', '8', '一元众乐', 'yyzl', '', '11132', '1454639220');
INSERT INTO `go_data_statistics` VALUES ('4057', '8', '一元众乐', 'yyzl', '', '11133', '1454657513');
INSERT INTO `go_data_statistics` VALUES ('4058', '8', '一元众乐', 'yyzl', '', '11134', '1454664734');
INSERT INTO `go_data_statistics` VALUES ('4059', '8', '一元众乐', 'yyzl', '', '11135', '1454676469');
INSERT INTO `go_data_statistics` VALUES ('4060', '29', '小米开发者平台', 'xiaomi', '', '11136', '1454678052');
INSERT INTO `go_data_statistics` VALUES ('4061', '8', '一元众乐', 'yyzl', '', '11137', '1454682279');
INSERT INTO `go_data_statistics` VALUES ('4062', '16', '豌豆荚', 'wandoujia', '', '11138', '1454683000');
INSERT INTO `go_data_statistics` VALUES ('4063', '16', '豌豆荚', 'wandoujia', '', '11139', '1454726769');
INSERT INTO `go_data_statistics` VALUES ('4064', '8', '一元众乐', 'yyzl', '', '11140', '1454738749');
INSERT INTO `go_data_statistics` VALUES ('4065', '21', 'oppo', 'oppo', '', '11141', '1454749924');
INSERT INTO `go_data_statistics` VALUES ('4066', '21', 'oppo', 'oppo', '', '11142', '1454769890');
INSERT INTO `go_data_statistics` VALUES ('4067', '10', '腾讯应用宝', 'qq', '', '11143', '1454772439');
INSERT INTO `go_data_statistics` VALUES ('4068', '8', '一元众乐', 'yyzl', '', '11144', '1454858316');
INSERT INTO `go_data_statistics` VALUES ('4069', '19', '木蚂蚁', 'mumayi', '', '11145', '1454858971');
INSERT INTO `go_data_statistics` VALUES ('4070', '19', '木蚂蚁', 'mumayi', '', '11146', '1454859032');
INSERT INTO `go_data_statistics` VALUES ('4071', '19', '木蚂蚁', 'mumayi', '', '11147', '1454859168');
INSERT INTO `go_data_statistics` VALUES ('4072', '8', '一元众乐', 'yyzl', '', '11148', '1454861847');
INSERT INTO `go_data_statistics` VALUES ('4073', '8', '一元众乐', 'yyzl', '', '11149', '1454863099');
INSERT INTO `go_data_statistics` VALUES ('4074', '8', '一元众乐', 'yyzl', '', '11150', '1454884716');
INSERT INTO `go_data_statistics` VALUES ('4075', '14', '360手机助手', 'dev360', '', '11151', '1454887920');
INSERT INTO `go_data_statistics` VALUES ('4076', '8', '一元众乐', 'yyzl', '', '11152', '1454894683');
INSERT INTO `go_data_statistics` VALUES ('4077', '24', '魅族', 'meizu', '', '11153', '1454985890');
INSERT INTO `go_data_statistics` VALUES ('4078', '8', '一元众乐', 'yyzl', '', '11154', '1454987533');
INSERT INTO `go_data_statistics` VALUES ('4079', '16', '豌豆荚', 'wandoujia', '', '11155', '1454995443');
INSERT INTO `go_data_statistics` VALUES ('4080', '8', '一元众乐', 'yyzl', '', '11156', '1454995666');
INSERT INTO `go_data_statistics` VALUES ('4081', '2', '儒豹浏览器', 'rbower', '', '11157', '1455070685');
INSERT INTO `go_data_statistics` VALUES ('4082', '8', '一元众乐', 'yyzl', '', '11158', '1455114005');
INSERT INTO `go_data_statistics` VALUES ('4083', '8', '一元众乐', 'yyzl', '', '11165', '1455242033');
INSERT INTO `go_data_statistics` VALUES ('4084', '8', '一元众乐', 'yyzl', '', '11166', '1455246397');
INSERT INTO `go_data_statistics` VALUES ('4085', '16', '豌豆荚', 'wandoujia', '', '11171', '1455351277');
INSERT INTO `go_data_statistics` VALUES ('4086', '29', '小米开发者平台', 'xiaomi', '', '11172', '1455428914');
INSERT INTO `go_data_statistics` VALUES ('4087', '8', '一元众乐', 'yyzl', '', '11173', '1455457697');
INSERT INTO `go_data_statistics` VALUES ('4088', '8', '一元众乐', 'yyzl', '', '11174', '1455461843');
INSERT INTO `go_data_statistics` VALUES ('4089', '21', 'oppo', 'oppo', '', '11175', '1455464047');
INSERT INTO `go_data_statistics` VALUES ('4090', '16', '豌豆荚', 'wandoujia', '', '11176', '1455476327');
INSERT INTO `go_data_statistics` VALUES ('4091', '29', '小米开发者平台', 'xiaomi', '', '11177', '1455534119');
INSERT INTO `go_data_statistics` VALUES ('4092', '8', '一元众乐', 'yyzl', '', '11178', '1455536014');
INSERT INTO `go_data_statistics` VALUES ('4093', '8', '一元众乐', 'yyzl', '', '11179', '1455536187');
INSERT INTO `go_data_statistics` VALUES ('4094', '8', '一元众乐', 'yyzl', '', '11180', '1455551218');
INSERT INTO `go_data_statistics` VALUES ('4095', '29', '小米开发者平台', 'xiaomi', '', '11181', '1455602212');
INSERT INTO `go_data_statistics` VALUES ('4096', '16', '豌豆荚', 'wandoujia', '', '11183', '1455694423');
INSERT INTO `go_data_statistics` VALUES ('4097', '16', '豌豆荚', 'wandoujia', '', '11184', '1455697064');
INSERT INTO `go_data_statistics` VALUES ('4098', '16', '豌豆荚', 'wandoujia', '', '11185', '1455702465');
INSERT INTO `go_data_statistics` VALUES ('4099', '16', '豌豆荚', 'wandoujia', '', '11188', '1455755116');
INSERT INTO `go_data_statistics` VALUES ('4100', '16', '豌豆荚', 'wandoujia', '', '11189', '1455756948');
INSERT INTO `go_data_statistics` VALUES ('4101', '8', '一元众乐', 'yyzl', '', '11192', '1455872431');
INSERT INTO `go_data_statistics` VALUES ('4102', '21', 'oppo', 'oppo', '', '11193', '1455926840');
INSERT INTO `go_data_statistics` VALUES ('4103', '15', '百度', 'baidu', '', '11194', '1455948693');
INSERT INTO `go_data_statistics` VALUES ('4104', '8', '一元众乐', 'yyzl', '', '11196', '1455987023');
INSERT INTO `go_data_statistics` VALUES ('4105', '29', '小米开发者平台', 'xiaomi', '', '11200', '1456019018');
INSERT INTO `go_data_statistics` VALUES ('4106', '21', 'oppo', 'oppo', '', '11201', '1456020392');
INSERT INTO `go_data_statistics` VALUES ('4107', '21', 'oppo', 'oppo', '', '11203', '1456068893');
INSERT INTO `go_data_statistics` VALUES ('4108', '16', '豌豆荚', 'wandoujia', '', '11204', '1456085028');
INSERT INTO `go_data_statistics` VALUES ('4109', '16', '豌豆荚', 'wandoujia', '', '11205', '1456126370');
INSERT INTO `go_data_statistics` VALUES ('4110', '16', '豌豆荚', 'wandoujia', '', '11206', '1456128293');
INSERT INTO `go_data_statistics` VALUES ('4111', '21', 'oppo', 'oppo', '', '11207', '1456145673');
INSERT INTO `go_data_statistics` VALUES ('4112', '8', '一元众乐', 'yyzl', '', '11208', '1456278998');
INSERT INTO `go_data_statistics` VALUES ('4113', '8', '一元众乐', 'yyzl', '', '11209', '1456295525');
INSERT INTO `go_data_statistics` VALUES ('4114', '8', '一元众乐', 'yyzl', '', '11213', '1456355983');
INSERT INTO `go_data_statistics` VALUES ('4115', '21', 'oppo', 'oppo', '', '11214', '1456405769');
INSERT INTO `go_data_statistics` VALUES ('4116', '8', '一元众乐', 'yyzl', '', '11215', '1456413632');
INSERT INTO `go_data_statistics` VALUES ('4117', '8', '一元众乐', 'yyzl', '', '11216', '1456415349');
INSERT INTO `go_data_statistics` VALUES ('4118', '21', 'oppo', 'oppo', '', '11217', '1456418665');
INSERT INTO `go_data_statistics` VALUES ('4119', '8', '一元众乐', 'yyzl', '', '11218', '1456458980');
INSERT INTO `go_data_statistics` VALUES ('4120', '8', '一元众乐', 'yyzl', '', '11219', '1456459488');
INSERT INTO `go_data_statistics` VALUES ('4121', '8', '一元众乐', 'yyzl', '', '11220', '1456469139');
INSERT INTO `go_data_statistics` VALUES ('4122', '16', '豌豆荚', 'wandoujia', '', '11222', '1456529488');
INSERT INTO `go_data_statistics` VALUES ('4123', '21', 'oppo', 'oppo', '', '11223', '1456533865');
INSERT INTO `go_data_statistics` VALUES ('4124', '8', '一元众乐', 'yyzl', '', '11224', '1456536662');
INSERT INTO `go_data_statistics` VALUES ('4125', '21', 'oppo', 'oppo', '', '11226', '1456556791');
INSERT INTO `go_data_statistics` VALUES ('4126', '16', '豌豆荚', 'wandoujia', '', '11228', '1456587867');
INSERT INTO `go_data_statistics` VALUES ('4127', '8', '一元众乐', 'yyzl', '', '11230', '1456650973');
INSERT INTO `go_data_statistics` VALUES ('4128', '21', 'oppo', 'oppo', '', '11232', '1456679002');
INSERT INTO `go_data_statistics` VALUES ('4129', '21', 'oppo', 'oppo', '', '11234', '1456711082');
INSERT INTO `go_data_statistics` VALUES ('4130', '8', '一元众乐', 'yyzl', '', '11235', '1456718208');
INSERT INTO `go_data_statistics` VALUES ('4131', '8', '一元众乐', 'yyzl', '', '11237', '1456735667');
INSERT INTO `go_data_statistics` VALUES ('4132', '16', '豌豆荚', 'wandoujia', '', '11238', '1456750800');
INSERT INTO `go_data_statistics` VALUES ('4133', '8', '一元众乐', 'yyzl', '', '11239', '1456758785');
INSERT INTO `go_data_statistics` VALUES ('4134', '8', '一元众乐', 'yyzl', '', '11240', '1456803033');
INSERT INTO `go_data_statistics` VALUES ('4135', '8', '一元众乐', 'yyzl', '', '11241', '1456810301');
INSERT INTO `go_data_statistics` VALUES ('4136', '8', '一元众乐', 'yyzl', '', '11242', '1456815682');
INSERT INTO `go_data_statistics` VALUES ('4137', '29', '小米开发者平台', 'xiaomi', '', '11244', '1456826132');
INSERT INTO `go_data_statistics` VALUES ('4138', '16', '豌豆荚', 'wandoujia', '', '11245', '1456826336');
INSERT INTO `go_data_statistics` VALUES ('4139', '8', '一元众乐', 'yyzl', '', '11246', '1456833417');
INSERT INTO `go_data_statistics` VALUES ('4140', '8', '一元众乐', 'yyzl', '', '11247', '1456856847');
INSERT INTO `go_data_statistics` VALUES ('4141', '8', '一元众乐', 'yyzl', '', '11249', '1456904384');
INSERT INTO `go_data_statistics` VALUES ('4142', '16', '豌豆荚', 'wandoujia', '', '11250', '1456908734');
INSERT INTO `go_data_statistics` VALUES ('4143', '24', '魅族', 'meizu', '', '11251', '1456911620');
INSERT INTO `go_data_statistics` VALUES ('4144', '8', '一元众乐', 'yyzl', '', '11252', '1456912779');
INSERT INTO `go_data_statistics` VALUES ('4145', '8', '一元众乐', 'yyzl', '', '11253', '1456920331');
INSERT INTO `go_data_statistics` VALUES ('4146', '8', '一元众乐', 'yyzl', '', '11254', '1456986370');
INSERT INTO `go_data_statistics` VALUES ('4147', '8', '一元众乐', 'yyzl', '', '11255', '1456988589');
INSERT INTO `go_data_statistics` VALUES ('4148', '15', '百度', 'baidu', '', '11260', '1456989511');
INSERT INTO `go_data_statistics` VALUES ('4149', '8', '一元众乐', 'yyzl', '', '11261', '1457057684');
INSERT INTO `go_data_statistics` VALUES ('4150', '8', '一元众乐', 'yyzl', '', '11262', '1457070490');
INSERT INTO `go_data_statistics` VALUES ('4151', '24', '魅族', 'meizu', '', '11263', '1457070662');
INSERT INTO `go_data_statistics` VALUES ('4152', '16', '豌豆荚', 'wandoujia', '', '11265', '1457080583');
INSERT INTO `go_data_statistics` VALUES ('4153', '15', '百度', 'baidu', '', '11267', '1457109883');
INSERT INTO `go_data_statistics` VALUES ('4154', '8', '一元众乐', 'yyzl', '', '11268', '1457131059');
INSERT INTO `go_data_statistics` VALUES ('4155', '8', '一元众乐', 'yyzl', '', '11271', '1457140006');
INSERT INTO `go_data_statistics` VALUES ('4156', '8', '一元众乐', 'yyzl', '', '11273', '1457146921');
INSERT INTO `go_data_statistics` VALUES ('4157', '8', '一元众乐', 'yyzl', '', '11274', '1457146961');
INSERT INTO `go_data_statistics` VALUES ('4158', '16', '豌豆荚', 'wandoujia', '', '11276', '1457152700');
INSERT INTO `go_data_statistics` VALUES ('4159', '16', '豌豆荚', 'wandoujia', '', '11277', '1457158765');
INSERT INTO `go_data_statistics` VALUES ('4160', '8', '一元众乐', 'yyzl', '', '11278', '1457175709');
INSERT INTO `go_data_statistics` VALUES ('4161', '8', '一元众乐', 'yyzl', '', '11279', '1457181054');
INSERT INTO `go_data_statistics` VALUES ('4162', '8', '一元众乐', 'yyzl', '', '11280', '1457195978');
INSERT INTO `go_data_statistics` VALUES ('4163', '8', '一元众乐', 'yyzl', '', '11281', '1457217893');
INSERT INTO `go_data_statistics` VALUES ('4164', '29', '小米开发者平台', 'xiaomi', '', '11282', '1457239735');
INSERT INTO `go_data_statistics` VALUES ('4165', '8', '一元众乐', 'yyzl', '', '11284', '1457242931');
INSERT INTO `go_data_statistics` VALUES ('4166', '8', '一元众乐', 'yyzl', '', '11285', '1457245319');
INSERT INTO `go_data_statistics` VALUES ('4167', '21', 'oppo', 'oppo', '', '11286', '1457253961');
INSERT INTO `go_data_statistics` VALUES ('4168', '8', '一元众乐', 'yyzl', '', '11287', '1457256712');
INSERT INTO `go_data_statistics` VALUES ('4169', '8', '一元众乐', 'yyzl', '', '11289', '1457266397');
INSERT INTO `go_data_statistics` VALUES ('4170', '8', '一元众乐', 'yyzl', '', '11290', '1457266943');
INSERT INTO `go_data_statistics` VALUES ('4171', '17', 'PP助手', 'pp', '', '11291', '1457274584');
INSERT INTO `go_data_statistics` VALUES ('4172', '8', '一元众乐', 'yyzl', '', '11292', '1457274737');
INSERT INTO `go_data_statistics` VALUES ('4173', '19', '木蚂蚁', 'mumayi', '', '11293', '1457275975');
INSERT INTO `go_data_statistics` VALUES ('4174', '8', '一元众乐', 'yyzl', '', '11294', '1457278174');
INSERT INTO `go_data_statistics` VALUES ('4175', '16', '豌豆荚', 'wandoujia', '', '11295', '1457280251');
INSERT INTO `go_data_statistics` VALUES ('4176', '8', '一元众乐', 'yyzl', '', '11296', '1457280651');
INSERT INTO `go_data_statistics` VALUES ('4177', '15', '百度', 'baidu', '', '11299', '1457317641');
INSERT INTO `go_data_statistics` VALUES ('4178', '8', '一元众乐', 'yyzl', '', '11301', '1457320636');
INSERT INTO `go_data_statistics` VALUES ('4179', '16', '豌豆荚', 'wandoujia', '', '11302', '1457332038');
INSERT INTO `go_data_statistics` VALUES ('4180', '8', '一元众乐', 'yyzl', '', '11305', '1457364975');
INSERT INTO `go_data_statistics` VALUES ('4181', '21', 'oppo', 'oppo', '', '11306', '1457371795');
INSERT INTO `go_data_statistics` VALUES ('4182', '16', '豌豆荚', 'wandoujia', '', '11307', '1457399439');
INSERT INTO `go_data_statistics` VALUES ('4183', '8', '一元众乐', 'yyzl', '', '11310', '1457411029');
INSERT INTO `go_data_statistics` VALUES ('4184', '16', '豌豆荚', 'wandoujia', '', '11313', '1457418325');
INSERT INTO `go_data_statistics` VALUES ('4185', '29', '小米开发者平台', 'xiaomi', '', '11314', '1457428933');
INSERT INTO `go_data_statistics` VALUES ('4186', '8', '一元众乐', 'yyzl', '', '11315', '1457443445');
INSERT INTO `go_data_statistics` VALUES ('4187', '16', '豌豆荚', 'wandoujia', '', '11318', '1457500294');
INSERT INTO `go_data_statistics` VALUES ('4188', '4', '百度推广', 'zlbdtg', 'SEM00000225', '11319', '1457506889');
INSERT INTO `go_data_statistics` VALUES ('4189', '8', '一元众乐', 'yyzl', '', '11320', '1457507820');
INSERT INTO `go_data_statistics` VALUES ('4190', '8', '一元众乐', 'yyzl', '', '11327', '1457538044');
INSERT INTO `go_data_statistics` VALUES ('4191', '29', '小米开发者平台', 'xiaomi', '', '11331', '1457602390');
INSERT INTO `go_data_statistics` VALUES ('4192', '16', '豌豆荚', 'wandoujia', '', '11332', '1457615518');
INSERT INTO `go_data_statistics` VALUES ('4193', '24', '魅族', 'meizu', '', '11333', '1457633596');
INSERT INTO `go_data_statistics` VALUES ('4194', '23', '搜狗', 'sogou', '', '11335', '1457665145');
INSERT INTO `go_data_statistics` VALUES ('4195', '8', '一元众乐', 'yyzl', '', '11340', '1457690771');
INSERT INTO `go_data_statistics` VALUES ('4196', '8', '一元众乐', 'yyzl', '', '11341', '1457720570');
INSERT INTO `go_data_statistics` VALUES ('4197', '8', '一元众乐', 'yyzl', '', '11342', '1457749490');
INSERT INTO `go_data_statistics` VALUES ('4198', '16', '豌豆荚', 'wandoujia', '', '11345', '1457757630');
INSERT INTO `go_data_statistics` VALUES ('4199', '8', '一元众乐', 'yyzl', '', '11351', '1457865378');
INSERT INTO `go_data_statistics` VALUES ('4200', '8', '一元众乐', 'yyzl', '', '11352', '1457875509');
INSERT INTO `go_data_statistics` VALUES ('4201', '29', '小米开发者平台', 'xiaomi', '', '11354', '1457913643');
INSERT INTO `go_data_statistics` VALUES ('4202', '8', '一元众乐', 'yyzl', '', '11362', '1457953168');
INSERT INTO `go_data_statistics` VALUES ('4203', '15', '百度', 'baidu', '', '11363', '1457953314');
INSERT INTO `go_data_statistics` VALUES ('4204', '16', '豌豆荚', 'wandoujia', '', '11364', '1457957264');
INSERT INTO `go_data_statistics` VALUES ('4205', '16', '豌豆荚', 'wandoujia', '', '11365', '1457974965');
INSERT INTO `go_data_statistics` VALUES ('4206', '29', '小米开发者平台', 'xiaomi', '', '11367', '1458009426');
INSERT INTO `go_data_statistics` VALUES ('4207', '8', '一元众乐', 'yyzl', '', '11368', '1458013715');
INSERT INTO `go_data_statistics` VALUES ('4208', '8', '一元众乐', 'yyzl', '', '11370', '1458024649');
INSERT INTO `go_data_statistics` VALUES ('4209', '8', '一元众乐', 'yyzl', '', '11374', '1458040353');
INSERT INTO `go_data_statistics` VALUES ('4210', '8', '一元众乐', 'yyzl', '', '11375', '1458040753');
INSERT INTO `go_data_statistics` VALUES ('4211', '8', '一元众乐', 'yyzl', '', '11376', '1458053272');
INSERT INTO `go_data_statistics` VALUES ('4212', '15', '百度', 'baidu', '', '11379', '1458090987');
INSERT INTO `go_data_statistics` VALUES ('4213', '8', '一元众乐', 'yyzl', '', '11383', '1458095574');
INSERT INTO `go_data_statistics` VALUES ('4214', '8', '一元众乐', 'yyzl', '', '11384', '1458101455');
INSERT INTO `go_data_statistics` VALUES ('4215', '8', '一元众乐', 'yyzl', '', '11385', '1458102060');
INSERT INTO `go_data_statistics` VALUES ('4216', '8', '一元众乐', 'yyzl', '', '11387', '1458104093');
INSERT INTO `go_data_statistics` VALUES ('4217', '8', '一元众乐', 'yyzl', '', '11388', '1458105370');
INSERT INTO `go_data_statistics` VALUES ('4218', '8', '一元众乐', 'yyzl', '', '11389', '1458110383');
INSERT INTO `go_data_statistics` VALUES ('4219', '8', '一元众乐', 'yyzl', '', '11395', '1458185895');
INSERT INTO `go_data_statistics` VALUES ('4220', '8', '一元众乐', 'yyzl', '', '11397', '1458193873');
INSERT INTO `go_data_statistics` VALUES ('4221', '8', '一元众乐', 'yyzl', '', '11399', '1458218476');
INSERT INTO `go_data_statistics` VALUES ('4222', '16', '豌豆荚', 'wandoujia', '', '11400', '1458222716');
INSERT INTO `go_data_statistics` VALUES ('4223', '8', '一元众乐', 'yyzl', '', '11401', '1458268280');
INSERT INTO `go_data_statistics` VALUES ('4224', '16', '豌豆荚', 'wandoujia', '', '11402', '1458281915');
INSERT INTO `go_data_statistics` VALUES ('4225', '8', '一元众乐', 'yyzl', '', '11403', '1458285654');
INSERT INTO `go_data_statistics` VALUES ('4226', '16', '豌豆荚', 'wandoujia', '', '11409', '1458364904');
INSERT INTO `go_data_statistics` VALUES ('4227', '8', '一元众乐', 'yyzl', '', '11410', '1458366611');
INSERT INTO `go_data_statistics` VALUES ('4228', '13', '安卓市场', 'androidshichang', '', '11413', '1458379091');
INSERT INTO `go_data_statistics` VALUES ('4229', '29', '小米开发者平台', 'xiaomi', '', '11416', '1458385010');
INSERT INTO `go_data_statistics` VALUES ('4230', '16', '豌豆荚', 'wandoujia', '', '11417', '1458404349');
INSERT INTO `go_data_statistics` VALUES ('4231', '21', 'oppo', 'oppo', '', '11418', '1458406297');
INSERT INTO `go_data_statistics` VALUES ('4232', '21', 'oppo', 'oppo', '', '11420', '1458451936');
INSERT INTO `go_data_statistics` VALUES ('4233', '21', 'oppo', 'oppo', '', '11422', '1458461947');
INSERT INTO `go_data_statistics` VALUES ('4234', '8', '一元众乐', 'yyzl', '', '11423', '1458465228');
INSERT INTO `go_data_statistics` VALUES ('4235', '8', '一元众乐', 'yyzl', '', '11424', '1458469570');
INSERT INTO `go_data_statistics` VALUES ('4236', '8', '一元众乐', 'yyzl', '', '11426', '1458522304');
INSERT INTO `go_data_statistics` VALUES ('4237', '16', '豌豆荚', 'wandoujia', '', '11427', '1458525871');
INSERT INTO `go_data_statistics` VALUES ('4238', '29', '小米开发者平台', 'xiaomi', '', '11428', '1458529610');
INSERT INTO `go_data_statistics` VALUES ('4239', '8', '一元众乐', 'yyzl', '', '11429', '1458532558');
INSERT INTO `go_data_statistics` VALUES ('4240', '8', '一元众乐', 'yyzl', '', '11433', '1458546281');
INSERT INTO `go_data_statistics` VALUES ('4241', '8', '一元众乐', 'yyzl', '', '11434', '1458565054');
INSERT INTO `go_data_statistics` VALUES ('4242', '8', '一元众乐', 'yyzl', '', '11438', '1458578459');
INSERT INTO `go_data_statistics` VALUES ('4243', '17', 'PP助手', 'pp', '', '11440', '1458614026');
INSERT INTO `go_data_statistics` VALUES ('4244', '16', '豌豆荚', 'wandoujia', '', '11441', '1458617939');
INSERT INTO `go_data_statistics` VALUES ('4245', '8', '一元众乐', 'yyzl', '', '11442', '1458618327');
INSERT INTO `go_data_statistics` VALUES ('4246', '8', '一元众乐', 'yyzl', '', '11443', '1458619202');
INSERT INTO `go_data_statistics` VALUES ('4247', '8', '一元众乐', 'yyzl', '', '11450', '1461561476');
INSERT INTO `go_data_statistics` VALUES ('4248', '8', '一元众乐', 'yyzl', '', '11457', '1461669475');
INSERT INTO `go_data_statistics` VALUES ('4249', '8', '一元众乐', 'yyzl', '', '11458', '1461669747');
INSERT INTO `go_data_statistics` VALUES ('4250', '8', '一元众乐', 'yyzl', '', '11467', '1461810158');
INSERT INTO `go_data_statistics` VALUES ('4251', '8', '一元众乐', 'yyzl', '', '11480', '1461926129');
INSERT INTO `go_data_statistics` VALUES ('4252', '8', '一元众乐', 'yyzl', '', '11481', '1461941024');
INSERT INTO `go_data_statistics` VALUES ('4253', '8', '一元众乐', 'yyzl', '', '11484', '1461946651');

-- ----------------------------
-- Table structure for `go_egglotter_award`
-- ----------------------------
DROP TABLE IF EXISTS `go_egglotter_award`;
CREATE TABLE `go_egglotter_award` (
  `award_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `user_id` int(11) DEFAULT NULL COMMENT '用户ID',
  `user_name` varchar(11) DEFAULT NULL COMMENT '用户名字',
  `rule_id` int(11) DEFAULT NULL COMMENT '活动ID',
  `subtime` int(11) DEFAULT NULL COMMENT '中奖时间',
  `spoil_id` int(11) DEFAULT NULL COMMENT '奖品等级',
  PRIMARY KEY (`award_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of go_egglotter_award
-- ----------------------------

-- ----------------------------
-- Table structure for `go_egglotter_rule`
-- ----------------------------
DROP TABLE IF EXISTS `go_egglotter_rule`;
CREATE TABLE `go_egglotter_rule` (
  `rule_id` int(11) NOT NULL AUTO_INCREMENT,
  `rule_name` varchar(200) DEFAULT NULL,
  `starttime` int(11) DEFAULT NULL COMMENT '活动开始时间',
  `endtime` int(11) DEFAULT NULL COMMENT '活动结束时间',
  `subtime` int(11) DEFAULT NULL COMMENT '活动编辑时间',
  `lotterytype` int(11) DEFAULT NULL COMMENT '抽奖按币分类',
  `lotterjb` int(11) DEFAULT NULL COMMENT '每一次抽奖使用的金币',
  `ruledesc` text COMMENT '规则介绍',
  `startusing` tinyint(4) DEFAULT NULL COMMENT '启用',
  PRIMARY KEY (`rule_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of go_egglotter_rule
-- ----------------------------

-- ----------------------------
-- Table structure for `go_egglotter_spoil`
-- ----------------------------
DROP TABLE IF EXISTS `go_egglotter_spoil`;
CREATE TABLE `go_egglotter_spoil` (
  `spoil_id` int(11) NOT NULL AUTO_INCREMENT,
  `rule_id` int(11) DEFAULT NULL,
  `spoil_name` text COMMENT '名称',
  `spoil_jl` int(11) DEFAULT NULL COMMENT '机率',
  `spoil_dj` int(11) DEFAULT NULL,
  `urlimg` varchar(200) DEFAULT NULL,
  `subtime` int(11) DEFAULT NULL COMMENT '提交时间',
  PRIMARY KEY (`spoil_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of go_egglotter_spoil
-- ----------------------------

-- ----------------------------
-- Table structure for `go_fund`
-- ----------------------------
DROP TABLE IF EXISTS `go_fund`;
CREATE TABLE `go_fund` (
  `id` int(10) unsigned NOT NULL,
  `fund_off` tinyint(4) unsigned NOT NULL DEFAULT '1',
  `fund_money` decimal(10,2) unsigned NOT NULL,
  `fund_count_money` decimal(12,2) DEFAULT NULL COMMENT '云购基金',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of go_fund
-- ----------------------------
INSERT INTO `go_fund` VALUES ('1', '0', '0.10', '5282094.60');

-- ----------------------------
-- Table structure for `go_link`
-- ----------------------------
DROP TABLE IF EXISTS `go_link`;
CREATE TABLE `go_link` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '友情链接ID',
  `type` tinyint(1) unsigned NOT NULL COMMENT '链接类型',
  `name` char(20) NOT NULL COMMENT '名称',
  `logo` varchar(250) NOT NULL COMMENT '图片',
  `url` varchar(50) NOT NULL COMMENT '地址',
  PRIMARY KEY (`id`),
  KEY `type` (`type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of go_link
-- ----------------------------

-- ----------------------------
-- Table structure for `go_member`
-- ----------------------------
DROP TABLE IF EXISTS `go_member`;
CREATE TABLE `go_member` (
  `uid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` char(20) NOT NULL COMMENT '用户名',
  `email` varchar(50) DEFAULT NULL COMMENT '用户邮箱',
  `mobile` char(11) DEFAULT NULL COMMENT '用户手机',
  `password` char(32) DEFAULT NULL COMMENT '密码',
  `user_ip` varchar(255) DEFAULT NULL,
  `img` varchar(255) DEFAULT NULL COMMENT '用户头像',
  `qianming` varchar(255) DEFAULT NULL COMMENT '用户签名',
  `groupid` tinyint(4) unsigned DEFAULT '0' COMMENT '用户权限组',
  `addgroup` varchar(255) DEFAULT NULL COMMENT '用户加入的圈子组1|2|3',
  `money` decimal(10,2) unsigned DEFAULT '0.00' COMMENT '账户金额',
  `emailcode` char(21) DEFAULT '-1' COMMENT '邮箱认证码',
  `mobilecode` char(21) DEFAULT '-1' COMMENT '手机认证码',
  `passcode` char(21) NOT NULL DEFAULT '-1' COMMENT '找会密码认证码-1,1,码',
  `reg_key` varchar(100) DEFAULT NULL COMMENT '注册参数',
  `score` int(10) unsigned NOT NULL DEFAULT '0',
  `jingyan` int(10) unsigned DEFAULT '0',
  `yaoqing` int(10) unsigned DEFAULT NULL COMMENT '邀请人',
  `band` varchar(255) DEFAULT NULL,
  `time` int(10) unsigned DEFAULT '0',
  `login_time` int(10) unsigned DEFAULT '0',
  `reg_plat` varchar(100) DEFAULT NULL COMMENT '注册平台  如:android/ios/web/wap',
  `auto_user` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB AUTO_INCREMENT=13497 DEFAULT CHARSET=utf8 COMMENT='会员表';

-- ----------------------------
-- Records of go_member
-- ----------------------------

-- ----------------------------
-- Table structure for `go_member_account`
-- ----------------------------
DROP TABLE IF EXISTS `go_member_account`;
CREATE TABLE `go_member_account` (
  `uid` int(10) unsigned NOT NULL COMMENT '用户id',
  `type` tinyint(1) DEFAULT NULL COMMENT '充值1/消费-1',
  `pay` char(20) DEFAULT NULL,
  `content` varchar(255) DEFAULT NULL COMMENT '详情',
  `money` mediumint(8) NOT NULL DEFAULT '0' COMMENT '金额',
  `time` char(20) NOT NULL,
  KEY `uid` (`uid`),
  KEY `type` (`type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='会员账户明细';

-- ----------------------------
-- Records of go_member_account
-- ----------------------------

-- ----------------------------
-- Table structure for `go_member_addmoney_record`
-- ----------------------------
DROP TABLE IF EXISTS `go_member_addmoney_record`;
CREATE TABLE `go_member_addmoney_record` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uid` int(10) unsigned NOT NULL,
  `code` char(20) NOT NULL,
  `money` decimal(10,2) unsigned NOT NULL,
  `pay_type` char(10) NOT NULL,
  `status` char(20) NOT NULL,
  `time` int(10) NOT NULL,
  `score` int(10) unsigned DEFAULT NULL,
  `scookies` text COMMENT '购物车cookie',
  `transid` varchar(100) DEFAULT NULL COMMENT '交易流水号(爱贝对账用)',
  `transtime` varchar(100) DEFAULT NULL COMMENT '交易时间(爱贝对账用)',
  `paytype` int(4) DEFAULT NULL COMMENT '(爱贝对账用)',
  `result` int(1) DEFAULT NULL COMMENT '(爱贝对账用)',
  `plat_type` int(1) DEFAULT '0' COMMENT '支付平台  0:网站  1:爱贝',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8581 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of go_member_addmoney_record
-- ----------------------------

-- ----------------------------
-- Table structure for `go_member_band`
-- ----------------------------
DROP TABLE IF EXISTS `go_member_band`;
CREATE TABLE `go_member_band` (
  `b_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `b_uid` int(10) DEFAULT NULL COMMENT '用户ID',
  `b_type` char(10) DEFAULT NULL COMMENT '绑定登陆类型',
  `b_code` varchar(100) DEFAULT NULL COMMENT '返回数据1',
  `b_data` varchar(100) DEFAULT NULL COMMENT '返回数据2',
  `b_time` int(10) DEFAULT NULL,
  PRIMARY KEY (`b_id`),
  KEY `b_uid` (`b_uid`)
) ENGINE=InnoDB AUTO_INCREMENT=969 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of go_member_band
-- ----------------------------
INSERT INTO `go_member_band` VALUES ('953', '11471', 'weixin', 'obxIFwcEu_JQS_ueuFhuJYi72Ro4', null, '1461815193');
INSERT INTO `go_member_band` VALUES ('954', '11451', 'weixin', 'obxIFwfvvcfTIRUs0ddSuia91k1Q', null, '1461815598');
INSERT INTO `go_member_band` VALUES ('956', '11450', 'weixin', 'obxIFwVYrGAOiAhmLt7S9tsiRH5Y', null, '1461823050');
INSERT INTO `go_member_band` VALUES ('957', '11453', 'weixin', 'obxIFwVIXAGpyg-vrbuZhstv9jW8', null, '1461823620');
INSERT INTO `go_member_band` VALUES ('958', '11454', 'weixin', 'obxIFwWxFOd3CO-vRyZ7LQ0xbAVY', null, '1461824429');
INSERT INTO `go_member_band` VALUES ('960', '11478', 'weixin', 'obxIFwdfGqInr-fH8ebgZA0f7a00', null, '1461837709');
INSERT INTO `go_member_band` VALUES ('961', '11462', 'weixin', 'obxIFwQRbZgCTBxpRx0Yj3aeJLe8', null, '1461908117');
INSERT INTO `go_member_band` VALUES ('962', '11482', 'weixin', 'obxIFwZBCRrJB0BbR_V7jqcXCh_c', null, '1461946540');
INSERT INTO `go_member_band` VALUES ('963', '11483', 'weixin', 'obxIFwZBCRrJB0BbR_V7jqcXCh_c', null, '1461946556');
INSERT INTO `go_member_band` VALUES ('964', '11485', 'weixin', 'obxIFwSoEW-Kce4ojaeWXa4-jzZA', null, '1461946751');
INSERT INTO `go_member_band` VALUES ('965', '11481', 'weixin', 'obxIFwcLkwy_aQdjLz6xJVeAucx8', null, '1461976632');
INSERT INTO `go_member_band` VALUES ('966', '11487', 'weixin', 'obxIFwVFWvy-pEe9EZAMy5vQ7JtM', null, '1461993445');

-- ----------------------------
-- Table structure for `go_member_cashout`
-- ----------------------------
DROP TABLE IF EXISTS `go_member_cashout`;
CREATE TABLE `go_member_cashout` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `uid` int(10) unsigned NOT NULL COMMENT '用户id',
  `username` varchar(20) NOT NULL COMMENT '开户人',
  `bankname` varchar(255) NOT NULL COMMENT '银行名称',
  `branch` varchar(255) NOT NULL COMMENT '支行',
  `money` decimal(8,0) NOT NULL DEFAULT '0' COMMENT '申请提现金额',
  `time` char(20) NOT NULL COMMENT '申请时间',
  `banknumber` varchar(50) NOT NULL COMMENT '银行帐号',
  `linkphone` varchar(100) NOT NULL COMMENT '联系电话',
  `auditstatus` tinyint(4) NOT NULL COMMENT '1审核通过',
  `procefees` decimal(8,2) NOT NULL COMMENT '手续费',
  `reviewtime` char(20) NOT NULL COMMENT '审核通过时间',
  PRIMARY KEY (`id`),
  KEY `uid` (`uid`),
  KEY `type` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='会员账户明细';

-- ----------------------------
-- Records of go_member_cashout
-- ----------------------------

-- ----------------------------
-- Table structure for `go_member_del`
-- ----------------------------
DROP TABLE IF EXISTS `go_member_del`;
CREATE TABLE `go_member_del` (
  `uid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` char(20) NOT NULL COMMENT '用户名',
  `email` varchar(50) DEFAULT NULL COMMENT '用户邮箱',
  `mobile` char(11) DEFAULT NULL COMMENT '用户手机',
  `password` char(32) DEFAULT NULL COMMENT '密码',
  `user_ip` varchar(255) DEFAULT NULL,
  `img` varchar(255) DEFAULT NULL COMMENT '用户头像',
  `qianming` varchar(255) DEFAULT NULL COMMENT '用户签名',
  `groupid` tinyint(4) unsigned DEFAULT '0' COMMENT '用户权限组',
  `addgroup` varchar(255) DEFAULT NULL COMMENT '用户加入的圈子组1|2|3',
  `money` decimal(10,2) unsigned DEFAULT '0.00' COMMENT '账户金额',
  `emailcode` char(21) DEFAULT '-1' COMMENT '邮箱认证码',
  `mobilecode` char(21) DEFAULT '-1' COMMENT '手机认证码',
  `passcode` char(21) DEFAULT '-1' COMMENT '找会密码认证码-1,1,码',
  `reg_key` varchar(100) DEFAULT NULL COMMENT '注册参数',
  `score` int(10) unsigned NOT NULL DEFAULT '0',
  `jingyan` int(10) unsigned DEFAULT '0',
  `yaoqing` int(10) unsigned DEFAULT NULL,
  `band` varchar(255) DEFAULT NULL,
  `time` int(10) unsigned DEFAULT '0',
  `login_time` int(10) unsigned DEFAULT '0',
  `reg_plat` varchar(100) DEFAULT NULL,
  `auto_user` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB AUTO_INCREMENT=13348 DEFAULT CHARSET=utf8 COMMENT='会员表';

-- ----------------------------
-- Records of go_member_del
-- ----------------------------
INSERT INTO `go_member_del` VALUES ('11461', '????&nbsp;W.m&nbsp;?', null, null, 'e10adc3949ba59abbe56e057f20f883e', '北京市北京市,218.241.196.226', 'photo/member.jpg', null, '0', null, '0.00', '-1', '-1', '-1', null, '0', '0', null, 'weixin', '1461730731', '1461731768', null, '0');
INSERT INTO `go_member_del` VALUES ('11463', '???? W.m ?', '', '18701400070', 'e10adc3949ba59abbe56e057f20f883e', '北京市北京市,218.241.196.226', 'photo/member.jpg', '', '1', null, '479.00', '-1', '-1', '-1', null, '0', '0', null, 'weixin', '1461734486', '1461743680', null, '0');
INSERT INTO `go_member_del` VALUES ('11464', '????&nbsp;W.m&nbsp;?', null, null, 'e10adc3949ba59abbe56e057f20f883e', '北京市北京市,223.104.38.72', 'photo/member.jpg', null, '0', null, '0.00', '-1', '-1', '-1', null, '0', '0', null, 'weixin', '1461734504', '1461734523', null, '0');
INSERT INTO `go_member_del` VALUES ('11488', '..&nbsp;&nbsp;&nbsp;', null, null, 'e10adc3949ba59abbe56e057f20f883e', '陕西省西安市,111.20.241.231', 'photo/member.jpg', null, '0', null, '0.00', '-1', '-1', '-1', null, '0', '0', null, 'weixin', '1462120625', '1462120640', null, '0');
INSERT INTO `go_member_del` VALUES ('11489', '', '1048841928@qq.com', null, '96e79218965eb72c92a549dd5a330112', '山西省临汾市,61.237.157.142', 'photo/member.jpg', null, '0', null, '5469.00', '1', '-1', '-1', null, '0', '0', null, null, '1462245218', '0', null, '1');
INSERT INTO `go_member_del` VALUES ('11490', '', '2276397701@qq.com', null, '96e79218965eb72c92a549dd5a330112', '内蒙古自治区,61.233.61.163', 'photo/member.jpg', null, '0', null, '5055.00', '1', '-1', '-1', null, '0', '0', null, null, '1462245218', '0', null, '1');
INSERT INTO `go_member_del` VALUES ('11491', '', '2276397702@qq.com', null, '96e79218965eb72c92a549dd5a330112', '宁夏回族自治区吴忠市,222.75.72.54', 'photo/member.jpg', null, '0', null, '5445.00', '1', '-1', '-1', null, '0', '0', null, null, '1462245218', '0', null, '1');
INSERT INTO `go_member_del` VALUES ('13346', '茹茹', null, null, 'e10adc3949ba59abbe56e057f20f883e', '北京市北京市,223.255.1.20', 'photo/member.jpg', null, '0', null, '0.00', '-1', '-1', '-1', null, '0', '0', null, 'weixin', '1462327164', '1462327183', null, '0');
INSERT INTO `go_member_del` VALUES ('13347', '高桂金高桂金', null, '13812345678', '96e79218965eb72c92a549dd5a330112', '北京市北京市,61.232.248.146', 'photo/member.jpg', null, '0', null, '5493.00', '-1', '1', '-1', null, '0', '0', null, null, '1462418578', '0', null, '1');

-- ----------------------------
-- Table structure for `go_member_dizhi`
-- ----------------------------
DROP TABLE IF EXISTS `go_member_dizhi`;
CREATE TABLE `go_member_dizhi` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id',
  `uid` int(10) NOT NULL COMMENT '用户id',
  `sheng` varchar(15) DEFAULT NULL COMMENT '省',
  `shi` varchar(15) DEFAULT NULL COMMENT '市',
  `xian` varchar(15) DEFAULT NULL COMMENT '县',
  `jiedao` varchar(255) DEFAULT NULL COMMENT '街道地址',
  `youbian` mediumint(8) DEFAULT NULL COMMENT '邮编',
  `shouhuoren` varchar(15) DEFAULT NULL COMMENT '收货人',
  `mobile` char(11) DEFAULT NULL COMMENT '手机',
  `qq` char(11) DEFAULT NULL COMMENT 'QQ',
  `tell` varchar(15) DEFAULT NULL COMMENT '座机号',
  `default` char(1) DEFAULT 'N' COMMENT '是否默认',
  `time` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `uid` (`uid`)
) ENGINE=InnoDB AUTO_INCREMENT=1145 DEFAULT CHARSET=utf8 COMMENT='会员地址表';

-- ----------------------------
-- Records of go_member_dizhi
-- ----------------------------
INSERT INTO `go_member_dizhi` VALUES ('1140', '11453', '广东省', '深圳市', '福田区', '福田大厦', '0', '谢生', '13631688169', '', '', 'Y', '1461563566');
INSERT INTO `go_member_dizhi` VALUES ('1141', '11450', '广东省', '深圳市', '龙岗区', '中心城', null, '谢生', '13991984088', null, null, 'Y', '1461681976');
INSERT INTO `go_member_dizhi` VALUES ('1142', '11459', '北京市', '北京市', '朝阳区', '百子湾11号', '0', '周先生', '18674802623', '', '', 'Y', '1461730372');
INSERT INTO `go_member_dizhi` VALUES ('1143', '11462', '北京市', '北京市', '朝阳区', '农展馆南路12', '0', '王先生', '18701400070', '', '', 'Y', '1461732766');
INSERT INTO `go_member_dizhi` VALUES ('1144', '11451', '广东省', '深圳市', '龙岗区', '城投商务中心1003号', null, '陈远鹏', '13682681143', null, null, 'Y', '1461748756');

-- ----------------------------
-- Table structure for `go_member_friendship`
-- ----------------------------
DROP TABLE IF EXISTS `go_member_friendship`;
CREATE TABLE `go_member_friendship` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `uid` int(10) NOT NULL COMMENT '被邀请者',
  `yaoqing` int(10) DEFAULT NULL COMMENT '邀请者',
  `lev` tinyint(1) NOT NULL DEFAULT '1' COMMENT '默认是第一级好友  总共3级',
  `addtime` int(10) DEFAULT NULL COMMENT '添加时间',
  PRIMARY KEY (`id`),
  KEY `yaoqing` (`yaoqing`),
  KEY `lev` (`lev`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of go_member_friendship
-- ----------------------------
INSERT INTO `go_member_friendship` VALUES ('1', '133', '69', '1', '1451741175');

-- ----------------------------
-- Table structure for `go_member_go_record`
-- ----------------------------
DROP TABLE IF EXISTS `go_member_go_record`;
CREATE TABLE `go_member_go_record` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `code` char(20) DEFAULT NULL COMMENT '订单号',
  `code_tmp` tinyint(3) unsigned DEFAULT NULL COMMENT '相同订单',
  `username` varchar(30) NOT NULL,
  `uphoto` varchar(255) DEFAULT NULL,
  `uid` int(10) unsigned NOT NULL COMMENT '会员id',
  `shopid` int(6) unsigned NOT NULL COMMENT '商品id',
  `shopname` varchar(255) NOT NULL COMMENT '商品名',
  `shopqishu` smallint(6) NOT NULL DEFAULT '0' COMMENT '期数',
  `gonumber` smallint(5) unsigned DEFAULT NULL COMMENT '购买次数',
  `goucode` longtext NOT NULL COMMENT '云购码',
  `moneycount` decimal(10,2) NOT NULL,
  `huode` char(50) NOT NULL DEFAULT '0' COMMENT '中奖码',
  `pay_type` char(10) DEFAULT NULL COMMENT '付款方式',
  `ip` varchar(255) DEFAULT NULL,
  `status` char(30) DEFAULT NULL COMMENT '订单状态',
  `company_money` decimal(10,2) unsigned NOT NULL DEFAULT '0.00',
  `company_code` char(20) DEFAULT NULL,
  `company` char(10) DEFAULT NULL,
  `company_time` int(10) DEFAULT NULL COMMENT '发货时间',
  `confirm_time` int(10) DEFAULT NULL COMMENT '确认收货时间',
  `confrim_addr` varchar(255) DEFAULT NULL COMMENT '确认后的收货信息（收货人、电话、地址）',
  `confrim_addr_time` int(10) DEFAULT NULL COMMENT '确认收货地址时间',
  `confrim_addr_uinfo` varchar(100) DEFAULT NULL COMMENT '收货人姓名和电话 格式: 张三|13788888888',
  `time` char(21) NOT NULL COMMENT '购买时间',
  `cz_code` char(20) DEFAULT NULL COMMENT '充值订单号',
  PRIMARY KEY (`id`),
  KEY `uid` (`uid`),
  KEY `shopid` (`shopid`),
  KEY `time` (`time`)
) ENGINE=InnoDB AUTO_INCREMENT=28726 DEFAULT CHARSET=utf8 COMMENT='云购记录表';

-- ----------------------------
-- Records of go_member_go_record
-- ----------------------------

-- ----------------------------
-- Table structure for `go_member_group`
-- ----------------------------
DROP TABLE IF EXISTS `go_member_group`;
CREATE TABLE `go_member_group` (
  `groupid` tinyint(4) unsigned NOT NULL AUTO_INCREMENT,
  `name` char(15) NOT NULL COMMENT '会员组名',
  `jingyan_start` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '需要的经验值',
  `jingyan_end` int(10) NOT NULL,
  `icon` varchar(255) DEFAULT NULL COMMENT '图标',
  `type` char(1) NOT NULL DEFAULT 'N' COMMENT '是否是系统组',
  PRIMARY KEY (`groupid`),
  KEY `jingyan` (`jingyan_start`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COMMENT='会员权限组';

-- ----------------------------
-- Records of go_member_group
-- ----------------------------
INSERT INTO `go_member_group` VALUES ('1', '云购新手', '0', '500', null, 'N');
INSERT INTO `go_member_group` VALUES ('2', '云购小将', '501', '1000', null, 'N');
INSERT INTO `go_member_group` VALUES ('3', '云购中将', '1001', '3000', null, 'N');
INSERT INTO `go_member_group` VALUES ('4', '云购上将', '3001', '6000', null, 'N');
INSERT INTO `go_member_group` VALUES ('5', '云购大将', '6001', '20000', null, 'N');
INSERT INTO `go_member_group` VALUES ('6', '云购将军', '20001', '40000', null, 'N');

-- ----------------------------
-- Table structure for `go_member_jifen`
-- ----------------------------
DROP TABLE IF EXISTS `go_member_jifen`;
CREATE TABLE `go_member_jifen` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `uid` int(10) NOT NULL COMMENT '充值者',
  `yaoqing` int(10) NOT NULL COMMENT '获得积分的人',
  `lev` tinyint(1) NOT NULL COMMENT '级数',
  `jifen` decimal(10,2) DEFAULT NULL COMMENT '获得的积分',
  `record_id` int(10) NOT NULL COMMENT '充值记录的ID',
  `addtime` int(10) DEFAULT NULL COMMENT '获得积分的时间',
  PRIMARY KEY (`id`),
  KEY `yaoqing` (`yaoqing`),
  KEY `lev` (`lev`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of go_member_jifen
-- ----------------------------

-- ----------------------------
-- Table structure for `go_member_message`
-- ----------------------------
DROP TABLE IF EXISTS `go_member_message`;
CREATE TABLE `go_member_message` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uid` int(10) unsigned NOT NULL COMMENT '用户id',
  `type` tinyint(1) DEFAULT '0' COMMENT '消息来源,0系统,1私信',
  `sendid` int(10) unsigned DEFAULT '0' COMMENT '发送人ID',
  `sendname` char(20) DEFAULT NULL COMMENT '发送人名',
  `content` varchar(255) DEFAULT NULL COMMENT '发送内容',
  `time` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `uid` (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='会员消息表';

-- ----------------------------
-- Records of go_member_message
-- ----------------------------

-- ----------------------------
-- Table structure for `go_member_recodes`
-- ----------------------------
DROP TABLE IF EXISTS `go_member_recodes`;
CREATE TABLE `go_member_recodes` (
  `uid` int(10) unsigned NOT NULL COMMENT '用户id',
  `type` tinyint(1) NOT NULL COMMENT '收取1//充值-2/提现-3',
  `content` varchar(255) NOT NULL COMMENT '详情',
  `shopid` int(11) DEFAULT NULL COMMENT '商品id',
  `money` decimal(8,2) NOT NULL DEFAULT '0.00' COMMENT '佣金',
  `time` char(20) NOT NULL,
  `ygmoney` decimal(8,2) NOT NULL COMMENT '云购金额',
  `cashoutid` int(11) DEFAULT NULL COMMENT '申请提现记录表id',
  KEY `uid` (`uid`),
  KEY `type` (`type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='会员账户明细';

-- ----------------------------
-- Records of go_member_recodes
-- ----------------------------

-- ----------------------------
-- Table structure for `go_mobile_code`
-- ----------------------------
DROP TABLE IF EXISTS `go_mobile_code`;
CREATE TABLE `go_mobile_code` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `mobile` char(11) NOT NULL COMMENT '手机号码',
  `mobilecode` char(6) NOT NULL COMMENT '验证码',
  `time` int(10) NOT NULL,
  `is_reg` tinyint(1) NOT NULL DEFAULT '0' COMMENT '如果使用了该验证码注册，就为1',
  `is_found` tinyint(1) NOT NULL DEFAULT '0' COMMENT '如果使用了该验证码找回密码，就为1',
  `is_mod` tinyint(1) NOT NULL DEFAULT '0' COMMENT '如果使用了该验证码更换手机号，就为1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17735 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of go_mobile_code
-- ----------------------------

-- ----------------------------
-- Table structure for `go_model`
-- ----------------------------
DROP TABLE IF EXISTS `go_model`;
CREATE TABLE `go_model` (
  `modelid` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `name` char(10) NOT NULL,
  `table` char(20) NOT NULL,
  PRIMARY KEY (`modelid`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='模型表';

-- ----------------------------
-- Records of go_model
-- ----------------------------
INSERT INTO `go_model` VALUES ('1', '云购模型', 'shoplist');
INSERT INTO `go_model` VALUES ('2', '文章模型', 'article');

-- ----------------------------
-- Table structure for `go_navigation`
-- ----------------------------
DROP TABLE IF EXISTS `go_navigation`;
CREATE TABLE `go_navigation` (
  `cid` smallint(6) unsigned NOT NULL AUTO_INCREMENT,
  `parentid` smallint(6) unsigned DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `type` char(10) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `status` char(1) DEFAULT 'Y' COMMENT '显示/隐藏',
  `order` smallint(6) unsigned DEFAULT '1',
  PRIMARY KEY (`cid`),
  KEY `status` (`status`),
  KEY `order` (`order`),
  KEY `type` (`type`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of go_navigation
-- ----------------------------
INSERT INTO `go_navigation` VALUES ('1', '0', '所有商品', 'index', '/goods_list', 'Y', '6');
INSERT INTO `go_navigation` VALUES ('2', '0', '新手指南', 'index', '/single/newbie', 'Y', '2');
INSERT INTO `go_navigation` VALUES ('3', '0', '神马购圈', 'foot', '/group', 'N', '2');
INSERT INTO `go_navigation` VALUES ('4', '0', '关于神马购', 'foot', '/help/1', 'Y', '1');
INSERT INTO `go_navigation` VALUES ('5', '0', '隐私声明', 'foot', '/help/12', 'Y', '1');
INSERT INTO `go_navigation` VALUES ('6', '0', '合作专区', 'foot', '/single/business', 'Y', '1');
INSERT INTO `go_navigation` VALUES ('7', '0', '友情链接', 'foot', '/link', 'Y', '1');
INSERT INTO `go_navigation` VALUES ('8', '0', '联系我们', 'foot', '/help/24', 'Y', '1');
INSERT INTO `go_navigation` VALUES ('10', '0', '晒单分享', 'index', '/shaidan/', 'Y', '1');
INSERT INTO `go_navigation` VALUES ('12', '0', '最新揭晓', 'index', '/goods_lottery', 'N', '1');
INSERT INTO `go_navigation` VALUES ('14', '0', '限时揭晓', 'index', '/go/autolottery', 'N', '1');
INSERT INTO `go_navigation` VALUES ('16', '0', '最新揭晓', 'index', '/goods_lottery', 'Y', '1');
INSERT INTO `go_navigation` VALUES ('17', null, '十元专区', 'index', '/ten', 'Y', '5');

-- ----------------------------
-- Table structure for `go_pay`
-- ----------------------------
DROP TABLE IF EXISTS `go_pay`;
CREATE TABLE `go_pay` (
  `pay_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `pay_name` char(20) NOT NULL,
  `pay_class` char(20) NOT NULL,
  `pay_type` tinyint(3) NOT NULL,
  `pay_thumb` varchar(255) DEFAULT NULL,
  `pay_des` text,
  `pay_start` tinyint(4) NOT NULL,
  `pay_key` text,
  PRIMARY KEY (`pay_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of go_pay
-- ----------------------------
INSERT INTO `go_pay` VALUES ('1', '财付通', 'tenpay', '1', 'photo/cft.gif', '腾讯财付通	', '0', 'a:2:{s:2:\"id\";a:2:{s:4:\"name\";s:19:\"财付通商户号:\";s:3:\"val\";s:0:\"\";}s:3:\"key\";a:2:{s:4:\"name\";s:16:\"财付通密钥:\";s:3:\"val\";s:0:\"\";}}');
INSERT INTO `go_pay` VALUES ('2', '支付宝', 'alipay', '1', 'photo/20151127/59038215594697.png', '支付宝支付', '1', 'a:3:{s:2:\"id\";a:2:{s:4:\"name\";s:19:\"支付宝商户号:\";s:3:\"val\";s:16:\"2088901039824985\";}s:3:\"key\";a:2:{s:4:\"name\";s:16:\"支付宝密钥:\";s:3:\"val\";s:32:\"vudx4el82wvdvmteu84320h1231vxo5n\";}s:4:\"user\";a:2:{s:4:\"name\";s:16:\"支付宝账号:\";s:3:\"val\";s:16:\"329174302@qq.com\";}}');
INSERT INTO `go_pay` VALUES ('3', '易宝支付', 'yeepay', '1', 'photo/20151127/60682151594709.png', '易宝支付', '0', 'a:2:{s:2:\"id\";a:2:{s:4:\"name\";s:16:\"易宝商户号:\";s:3:\"val\";s:0:\"\";}s:3:\"key\";a:2:{s:4:\"name\";s:13:\"易宝密钥:\";s:3:\"val\";s:0:\"\";}}');
INSERT INTO `go_pay` VALUES ('4', '微信支付', 'wxpay', '1', 'photo/20151220/23224763605264.png', '微信支付 ', '1', 'a:2:{s:2:\"id\";a:2:{s:4:\"name\";s:9:\"商户号\";s:3:\"val\";s:10:\"1248277301\";}s:3:\"key\";a:2:{s:4:\"name\";s:6:\"密匙\";s:3:\"val\";s:32:\"beijinghainaboyi4008869187400886\";}}');
INSERT INTO `go_pay` VALUES ('5', '爱贝支付', 'iapppay', '1', null, '爱贝支付', '1', null);

-- ----------------------------
-- Table structure for `go_position`
-- ----------------------------
DROP TABLE IF EXISTS `go_position`;
CREATE TABLE `go_position` (
  `pos_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `pos_model` tinyint(3) unsigned NOT NULL,
  `pos_name` varchar(30) NOT NULL,
  `pos_num` tinyint(3) unsigned NOT NULL,
  `pos_maxnum` tinyint(3) unsigned NOT NULL,
  `pos_this_num` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `pos_time` int(10) unsigned NOT NULL,
  PRIMARY KEY (`pos_id`),
  KEY `pos_id` (`pos_id`),
  KEY `pos_model` (`pos_model`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of go_position
-- ----------------------------

-- ----------------------------
-- Table structure for `go_position_data`
-- ----------------------------
DROP TABLE IF EXISTS `go_position_data`;
CREATE TABLE `go_position_data` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `con_id` int(10) unsigned NOT NULL,
  `mod_id` tinyint(3) unsigned NOT NULL,
  `mod_name` char(20) NOT NULL,
  `pos_id` int(10) unsigned NOT NULL,
  `pos_data` mediumtext NOT NULL,
  `pos_order` int(10) unsigned NOT NULL DEFAULT '1',
  `pos_time` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of go_position_data
-- ----------------------------

-- ----------------------------
-- Table structure for `go_pv`
-- ----------------------------
DROP TABLE IF EXISTS `go_pv`;
CREATE TABLE `go_pv` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `page` varchar(255) DEFAULT NULL COMMENT '访问的页面',
  `source` varchar(255) DEFAULT NULL COMMENT '上一链接(有则写入，无则空）',
  `ip` varchar(30) DEFAULT NULL COMMENT 'ip地址',
  `su` varchar(100) DEFAULT NULL COMMENT '渠道名称',
  `sk` varchar(100) DEFAULT NULL COMMENT '渠道关键词',
  `type` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0:来自web网站    1：来自手机网站',
  `time` int(10) NOT NULL COMMENT '访问时间',
  PRIMARY KEY (`id`),
  KEY `su` (`su`),
  KEY `time` (`time`)
) ENGINE=InnoDB AUTO_INCREMENT=451177 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of go_pv
-- ----------------------------
INSERT INTO `go_pv` VALUES ('451140', 'http://121.40.198.103/index.php', '', '218.241.196.226', '', '', '0', '1462439207');
INSERT INTO `go_pv` VALUES ('451141', 'http://121.40.198.103/index.php', '', '218.241.196.226', '', '', '0', '1462439210');
INSERT INTO `go_pv` VALUES ('451142', 'http://121.40.198.103/index.php', '', '218.241.196.226', '', '', '0', '1462439229');
INSERT INTO `go_pv` VALUES ('451143', 'http://121.40.198.103/index.php/goods_list', 'http://121.40.198.103/', '218.241.196.226', '', '', '0', '1462439232');
INSERT INTO `go_pv` VALUES ('451144', 'http://121.40.198.103/index.php/ten', 'http://121.40.198.103/index.php/goods_list', '218.241.196.226', '', '', '0', '1462439234');
INSERT INTO `go_pv` VALUES ('451145', 'http://121.40.198.103/index.php/shaidan/', 'http://121.40.198.103/index.php/single/newbie', '218.241.196.226', '', '', '0', '1462439236');
INSERT INTO `go_pv` VALUES ('451146', 'http://121.40.198.103/index.php/shaidan/images/pa', 'http://121.40.198.103/index.php/shaidan/', '218.241.196.226', '', '', '0', '1462439236');
INSERT INTO `go_pv` VALUES ('451147', 'http://121.40.198.103/index.php/goods_lottery', 'http://121.40.198.103/index.php/shaidan/', '218.241.196.226', '', '', '0', '1462439237');
INSERT INTO `go_pv` VALUES ('451148', 'http://121.40.198.103/index.php', 'http://121.40.198.103/index.php/goods_lottery', '218.241.196.226', '', '', '0', '1462439238');
INSERT INTO `go_pv` VALUES ('451149', 'http://www.baidu.com:443/index.php', '', '107.151.226.203', '', '', '0', '1462439313');
INSERT INTO `go_pv` VALUES ('451150', 'http://www.baidu.com:443/index.php', '', '23.251.40.103', '', '', '0', '1462439316');
INSERT INTO `go_pv` VALUES ('451151', 'http://wetowin.com/index.php', 'http://uptime.com/wetowin.com', '45.79.81.142', '', '', '0', '1462439383');
INSERT INTO `go_pv` VALUES ('451152', 'http://www.baidu.com:443/index.php', '', '107.151.226.242', '', '', '0', '1462440870');
INSERT INTO `go_pv` VALUES ('451153', 'http://www.baidu.com:443/index.php', '', '23.251.40.103', '', '', '0', '1462440907');
INSERT INTO `go_pv` VALUES ('451154', 'http://www.baidu.com:443/index.php', '', '107.151.226.242', '', '', '0', '1462441791');
INSERT INTO `go_pv` VALUES ('451155', 'http://www.baidu.com:443/index.php', '', '107.151.226.242', '', '', '0', '1462441906');
INSERT INTO `go_pv` VALUES ('451156', 'http://121.40.198.103/index.php', '', '218.241.196.226', '', '', '0', '1462511086');
INSERT INTO `go_pv` VALUES ('451157', 'http://121.40.198.103/index.php', '', '218.241.196.226', '', '', '0', '1462511087');
INSERT INTO `go_pv` VALUES ('451158', 'http://121.40.198.103/index.php', '', '218.241.196.226', '', '', '0', '1462511185');
INSERT INTO `go_pv` VALUES ('451159', 'http://121.40.198.103/index.php', '', '218.241.196.226', '', '', '0', '1462511187');
INSERT INTO `go_pv` VALUES ('451160', 'http://121.40.198.103/index.php', '', '218.241.196.226', '', '', '0', '1462511240');
INSERT INTO `go_pv` VALUES ('451161', 'http://121.40.198.103/index.php', '', '218.241.196.226', '', '', '0', '1462511350');
INSERT INTO `go_pv` VALUES ('451162', 'http://121.40.198.103/index.php/goods_list', 'http://121.40.198.103/', '218.241.196.226', '', '', '0', '1462511357');
INSERT INTO `go_pv` VALUES ('451163', 'http://121.40.198.103/index.php/ten', 'http://121.40.198.103/index.php/goods_list', '218.241.196.226', '', '', '0', '1462511359');
INSERT INTO `go_pv` VALUES ('451164', 'http://121.40.198.103/index.php/shaidan/', 'http://121.40.198.103/index.php/single/newbie', '218.241.196.226', '', '', '0', '1462511361');
INSERT INTO `go_pv` VALUES ('451165', 'http://121.40.198.103/index.php/shaidan/images/pa', 'http://121.40.198.103/index.php/shaidan/', '218.241.196.226', '', '', '0', '1462511362');
INSERT INTO `go_pv` VALUES ('451166', 'http://121.40.198.103/index.php/goods_lottery', 'http://121.40.198.103/index.php/shaidan/', '218.241.196.226', '', '', '0', '1462511362');
INSERT INTO `go_pv` VALUES ('451167', 'http://121.40.198.103/index.php/login/aHR0cDovLzEyMS40MC4xOTguMTAzL2luZGV4LnBocC9nb29kc19sb3R0ZXJ5', 'http://121.40.198.103/index.php/goods_lottery', '218.241.196.226', '', '', '0', '1462511363');
INSERT INTO `go_pv` VALUES ('451168', 'http://121.40.198.103/index.php/goods_lottery', 'http://121.40.198.103/index.php/shaidan/', '218.241.196.226', '', '', '0', '1462511434');
INSERT INTO `go_pv` VALUES ('451169', 'http://121.40.198.103/index.php/goods_lottery', 'http://121.40.198.103/index.php/shaidan/', '218.241.196.226', '', '', '0', '1462511436');
INSERT INTO `go_pv` VALUES ('451170', 'http://121.40.198.103/index.php/login/aHR0cDovLzEyMS40MC4xOTguMTAzL2luZGV4LnBocC9nb29kc19sb3R0ZXJ5', 'http://121.40.198.103/index.php/goods_lottery', '218.241.196.226', '', '', '0', '1462511439');
INSERT INTO `go_pv` VALUES ('451171', 'http://121.40.198.103/index.php/login/aHR0cDovLzEyMS40MC4xOTguMTAzL2luZGV4LnBocC9nb29kc19sb3R0ZXJ5', 'http://121.40.198.103/index.php/goods_lottery', '218.241.196.226', '', '', '0', '1462511440');
INSERT INTO `go_pv` VALUES ('451172', 'http://121.40.198.103/index.php/login/aHR0cDovLzEyMS40MC4xOTguMTAzL2luZGV4LnBocC9nb29kc19sb3R0ZXJ5', 'http://121.40.198.103/index.php/goods_lottery', '218.241.196.226', '', '', '0', '1462511441');
INSERT INTO `go_pv` VALUES ('451173', 'http://121.40.198.103/index.php/login/aHR0cDovLzEyMS40MC4xOTguMTAzL2luZGV4LnBocC9nb29kc19sb3R0ZXJ5', 'http://121.40.198.103/index.php/goods_lottery', '218.241.196.226', '', '', '0', '1462511821');
INSERT INTO `go_pv` VALUES ('451174', 'http://121.40.198.103/index.php', '', '218.241.196.226', '', '', '0', '1462512187');
INSERT INTO `go_pv` VALUES ('451175', 'http://121.40.198.103/index.php', '', '218.241.196.226', '', '', '0', '1462512188');
INSERT INTO `go_pv` VALUES ('451176', 'http://121.40.198.103/index.php', '', '222.209.140.8', '', '', '0', '1462512698');

-- ----------------------------
-- Table structure for `go_qqset`
-- ----------------------------
DROP TABLE IF EXISTS `go_qqset`;
CREATE TABLE `go_qqset` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `qq` varchar(11) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `type` varchar(20) DEFAULT NULL,
  `province` varchar(50) DEFAULT NULL,
  `city` varchar(50) DEFAULT NULL,
  `county` varchar(50) DEFAULT NULL,
  `qqurl` varchar(250) DEFAULT NULL,
  `full` varchar(6) DEFAULT NULL COMMENT '是否已满',
  `subtime` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of go_qqset
-- ----------------------------

-- ----------------------------
-- Table structure for `go_quanzi`
-- ----------------------------
DROP TABLE IF EXISTS `go_quanzi`;
CREATE TABLE `go_quanzi` (
  `id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `title` char(15) NOT NULL COMMENT '标题',
  `img` varchar(255) DEFAULT NULL COMMENT '图片地址',
  `chengyuan` mediumint(8) unsigned DEFAULT '0' COMMENT '成员数',
  `tiezi` mediumint(8) unsigned DEFAULT '0' COMMENT '帖子数',
  `guanli` mediumint(8) unsigned NOT NULL COMMENT '管理员',
  `jinhua` smallint(5) unsigned DEFAULT NULL COMMENT '精华帖',
  `jianjie` varchar(255) DEFAULT '暂无介绍' COMMENT '简介',
  `gongao` varchar(255) DEFAULT '暂无' COMMENT '公告',
  `jiaru` char(1) DEFAULT 'Y' COMMENT '申请加入',
  `glfatie` char(1) DEFAULT 'N' COMMENT '发帖权限',
  `shenhe` char(1) NOT NULL DEFAULT 'N',
  `huifu` char(1) NOT NULL DEFAULT 'Y',
  `time` int(11) NOT NULL COMMENT '时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of go_quanzi
-- ----------------------------

-- ----------------------------
-- Table structure for `go_quanzi_hueifu`
-- ----------------------------
DROP TABLE IF EXISTS `go_quanzi_hueifu`;
CREATE TABLE `go_quanzi_hueifu` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `tzid` int(11) DEFAULT NULL COMMENT '帖子ID匹配',
  `hueifu` text COMMENT '回复内容',
  `hueiyuan` varchar(255) DEFAULT NULL COMMENT '会员',
  `hftime` int(11) DEFAULT NULL COMMENT '时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of go_quanzi_hueifu
-- ----------------------------

-- ----------------------------
-- Table structure for `go_quanzi_tiezi`
-- ----------------------------
DROP TABLE IF EXISTS `go_quanzi_tiezi`;
CREATE TABLE `go_quanzi_tiezi` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `qzid` int(10) unsigned DEFAULT NULL COMMENT '圈子ID匹配',
  `tiezi` int(10) unsigned NOT NULL DEFAULT '0',
  `hueiyuan` varchar(255) DEFAULT NULL COMMENT '会员信息',
  `title` varchar(255) DEFAULT NULL COMMENT '标题',
  `neirong` text COMMENT '内容',
  `hueifu` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT '回复',
  `dianji` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '点击量',
  `zhiding` char(1) DEFAULT 'N' COMMENT '置顶',
  `jinghua` char(1) DEFAULT 'N' COMMENT '精华',
  `zuihou` varchar(255) DEFAULT NULL COMMENT '最后回复',
  `shenhe` char(1) NOT NULL DEFAULT 'Y' COMMENT '是否通过',
  `time` int(10) unsigned DEFAULT NULL COMMENT '时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of go_quanzi_tiezi
-- ----------------------------

-- ----------------------------
-- Table structure for `go_recharge_percentage`
-- ----------------------------
DROP TABLE IF EXISTS `go_recharge_percentage`;
CREATE TABLE `go_recharge_percentage` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `charge` int(50) DEFAULT NULL,
  `percentage` int(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of go_recharge_percentage
-- ----------------------------
INSERT INTO `go_recharge_percentage` VALUES ('1', '100', '1');
INSERT INTO `go_recharge_percentage` VALUES ('2', '200', '3');
INSERT INTO `go_recharge_percentage` VALUES ('3', '300', '6');
INSERT INTO `go_recharge_percentage` VALUES ('4', '400', '8');
INSERT INTO `go_recharge_percentage` VALUES ('5', '500', '10');

-- ----------------------------
-- Table structure for `go_recom`
-- ----------------------------
DROP TABLE IF EXISTS `go_recom`;
CREATE TABLE `go_recom` (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT '推荐位id',
  `img` varchar(50) DEFAULT NULL COMMENT '推荐位图片',
  `title` varchar(30) DEFAULT NULL COMMENT '推荐位标题',
  `link` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of go_recom
-- ----------------------------

-- ----------------------------
-- Table structure for `go_send`
-- ----------------------------
DROP TABLE IF EXISTS `go_send`;
CREATE TABLE `go_send` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uid` int(10) unsigned NOT NULL,
  `gid` int(10) unsigned NOT NULL,
  `username` varchar(30) NOT NULL,
  `shoptitle` varchar(200) NOT NULL,
  `send_type` tinyint(4) NOT NULL,
  `send_time` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `uid` (`uid`),
  KEY `gid` (`gid`),
  KEY `send_type` (`send_type`)
) ENGINE=InnoDB AUTO_INCREMENT=2893 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of go_send
-- ----------------------------
INSERT INTO `go_send` VALUES ('2877', '11453', '3506', '136****8169', '三只松鼠 手剥巴旦木 手剥薄壳杏仁 235g', '2', '1461562890');
INSERT INTO `go_send` VALUES ('2878', '11453', '3503', '136****8169', '三只松鼠 夏威夷果 澳洲坚果特产 奶油味', '2', '1461659520');
INSERT INTO `go_send` VALUES ('2879', '11459', '3511', '18674802623', '百草味 2袋组合装 | 香辣/五香牛肉粒100g/袋', '2', '1461730290');
INSERT INTO `go_send` VALUES ('2880', '11462', '3509', '2342', '百草味 2包装 | 白芝麻猪肉脯180g 靖江特产零食小吃 特色肉干熟食', '2', '1461732689');
INSERT INTO `go_send` VALUES ('2881', '11462', '3468', '2342', '全新Kindle Paperwhite 电子书阅读器', '2', '1461735690');
INSERT INTO `go_send` VALUES ('2882', '11453', '3516', '我是大东哥', '三只松鼠 手剥巴旦木 手剥薄壳杏仁 235g', '2', '1461739891');
INSERT INTO `go_send` VALUES ('2883', '11462', '3558', '2342', '三只松鼠 夏威夷果 澳洲坚果特产 奶油味', '2', '1461745890');
INSERT INTO `go_send` VALUES ('2884', '11451', '3512', '哈哈88', '百草味 2包装 | 东北松子200g 坚果零食特产 干果炒货原味开口松子', '2', '1461748316');
INSERT INTO `go_send` VALUES ('2885', '11454', '3562', '东哥绝逼中相机', '三只松鼠 手剥巴旦木 手剥薄壳杏仁 235g', '2', '1461748896');
INSERT INTO `go_send` VALUES ('2886', '15', '3565', '2222', '三只松鼠 手剥巴旦木 手剥薄壳杏仁 235g', '2', '1461986517');
INSERT INTO `go_send` VALUES ('2887', '11497', '3575', '我怎么就不中奖呢', '三只松鼠 手剥巴旦木 手剥薄壳杏仁 235g', '2', '1462249319');
INSERT INTO `go_send` VALUES ('2888', '15', '3549', '2222', '罗尔思 (ROSS) C22(18) 4位总控插座 1.8米', '2', '1462253511');
INSERT INTO `go_send` VALUES ('2889', '15', '3449', '2222', 'Apple iPhone6s 64G 颜色随机', '2', '1462254090');
INSERT INTO `go_send` VALUES ('2890', '15', '3591', '2222', 'Apple iPhone6s 64G 颜色随机', '2', '1462254716');
INSERT INTO `go_send` VALUES ('2891', '15', '3560', '2222', '百草味 2包装 | 白芝麻猪肉脯180g 靖江特产零食小吃 特色肉干熟食', '2', '1462267319');
INSERT INTO `go_send` VALUES ('2892', '15', '3584', '2222', '亿觅 emie 能量刀锋 超薄移动电源 8000毫安', '2', '1462268492');

-- ----------------------------
-- Table structure for `go_shaidan`
-- ----------------------------
DROP TABLE IF EXISTS `go_shaidan`;
CREATE TABLE `go_shaidan` (
  `sd_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '晒单id',
  `sd_userid` int(10) unsigned DEFAULT NULL COMMENT '用户ID',
  `sd_shopid` int(10) unsigned DEFAULT NULL COMMENT '商品ID',
  `sd_shopsid` int(10) unsigned DEFAULT NULL,
  `sd_qishu` int(10) unsigned DEFAULT NULL,
  `sd_ip` varchar(255) DEFAULT NULL,
  `sd_title` varchar(255) DEFAULT NULL COMMENT '晒单标题',
  `sd_thumbs` varchar(255) DEFAULT NULL COMMENT '缩略图',
  `sd_content` text COMMENT '晒单内容',
  `sd_photolist` text COMMENT '晒单图片',
  `sd_zhan` int(10) unsigned DEFAULT '0' COMMENT '点赞',
  `sd_ping` int(10) unsigned DEFAULT '0' COMMENT '评论',
  `sd_time` int(10) unsigned DEFAULT NULL COMMENT '晒单时间',
  `is_audit` tinyint(1) NOT NULL DEFAULT '2' COMMENT '是否审核通过，  0待审核，  1审核失败， 2审核通过；  默认审核通过',
  `audit_info` varchar(255) NOT NULL COMMENT '审核信息',
  PRIMARY KEY (`sd_id`),
  KEY `sd_userid` (`sd_userid`),
  KEY `sd_shopid` (`sd_shopid`),
  KEY `sd_zhan` (`sd_zhan`),
  KEY `sd_ping` (`sd_ping`),
  KEY `sd_time` (`sd_time`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='晒单';

-- ----------------------------
-- Records of go_shaidan
-- ----------------------------

-- ----------------------------
-- Table structure for `go_shaidan_hueifu`
-- ----------------------------
DROP TABLE IF EXISTS `go_shaidan_hueifu`;
CREATE TABLE `go_shaidan_hueifu` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sdhf_id` int(11) NOT NULL COMMENT '晒单ID',
  `sdhf_userid` int(11) DEFAULT NULL COMMENT '晒单回复会员ID',
  `sdhf_content` text COMMENT '晒单回复内容',
  `sdhf_time` int(11) DEFAULT NULL,
  `sdhf_username` char(20) DEFAULT NULL,
  `sdhf_img` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of go_shaidan_hueifu
-- ----------------------------

-- ----------------------------
-- Table structure for `go_shopcodes_1`
-- ----------------------------
DROP TABLE IF EXISTS `go_shopcodes_1`;
CREATE TABLE `go_shopcodes_1` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `s_id` int(10) unsigned NOT NULL,
  `s_cid` smallint(5) unsigned NOT NULL,
  `s_len` smallint(5) DEFAULT NULL,
  `s_codes` text,
  `s_codes_tmp` text,
  PRIMARY KEY (`id`),
  KEY `s_id` (`s_id`),
  KEY `s_cid` (`s_cid`),
  KEY `s_len` (`s_len`)
) ENGINE=InnoDB AUTO_INCREMENT=7747 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of go_shopcodes_1
-- ----------------------------

-- ----------------------------
-- Table structure for `go_shoplist`
-- ----------------------------
DROP TABLE IF EXISTS `go_shoplist`;
CREATE TABLE `go_shoplist` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '商品id',
  `sid` int(10) unsigned NOT NULL COMMENT '同一个商品',
  `cateid` smallint(6) unsigned DEFAULT NULL COMMENT '所属栏目ID',
  `brandid` smallint(6) unsigned DEFAULT NULL COMMENT '所属品牌ID',
  `title` varchar(100) DEFAULT NULL COMMENT '商品标题',
  `title_style` varchar(100) DEFAULT NULL,
  `title2` varchar(100) DEFAULT NULL COMMENT '副标题',
  `keywords` varchar(100) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `money` decimal(10,2) DEFAULT '0.00' COMMENT '金额',
  `yunjiage` decimal(4,2) unsigned DEFAULT '1.00' COMMENT '云购人次价格',
  `zongrenshu` int(10) unsigned DEFAULT '0' COMMENT '总需人数',
  `canyurenshu` int(10) unsigned DEFAULT '0' COMMENT '已参与人数',
  `shenyurenshu` int(10) unsigned DEFAULT NULL,
  `def_renshu` int(10) unsigned DEFAULT '0',
  `qishu` smallint(6) unsigned DEFAULT '0' COMMENT '期数',
  `maxqishu` smallint(5) unsigned DEFAULT '1' COMMENT ' 最大期数',
  `thumb` varchar(255) DEFAULT NULL,
  `picarr` text COMMENT '商品图片',
  `content` mediumtext COMMENT '商品内容详情',
  `codes_table` char(20) DEFAULT NULL,
  `xsjx_time` int(10) unsigned DEFAULT NULL,
  `pos` tinyint(4) unsigned DEFAULT NULL COMMENT '是否推荐',
  `renqi` tinyint(4) unsigned DEFAULT '0' COMMENT '是否人气商品0否1是',
  `default_renci` tinyint(4) NOT NULL DEFAULT '1' COMMENT '默认购买人次',
  `time` int(10) unsigned DEFAULT NULL COMMENT '时间',
  `order` int(10) unsigned DEFAULT '1',
  `q_uid` int(10) unsigned DEFAULT NULL COMMENT '中奖人ID',
  `q_user` text COMMENT '中奖人信息',
  `q_user_code` char(20) DEFAULT NULL COMMENT '中奖码',
  `q_content` mediumtext COMMENT '揭晓内容',
  `q_counttime` char(20) DEFAULT NULL COMMENT '总时间相加',
  `q_end_time` char(20) DEFAULT NULL COMMENT '揭晓时间',
  `q_showtime` char(1) DEFAULT 'N' COMMENT 'Y/N揭晓动画',
  `q_ssccode` char(20) DEFAULT NULL,
  `q_sscopen` varchar(100) DEFAULT NULL,
  `q_sscphase` varchar(100) DEFAULT NULL,
  `q_djstime` char(20) DEFAULT NULL,
  `is_shi` tinyint(1) NOT NULL DEFAULT '0',
  `is_new` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否是最新  1是 0否',
  `is_apple` int(2) NOT NULL DEFAULT '0' COMMENT '是否是苹果相关产品',
  `zdrange` varchar(100) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `renqi` (`renqi`),
  KEY `order` (`yunjiage`),
  KEY `q_uid` (`q_uid`),
  KEY `sid` (`sid`),
  KEY `shenyurenshu` (`shenyurenshu`),
  KEY `q_showtime` (`q_showtime`)
) ENGINE=InnoDB AUTO_INCREMENT=3612 DEFAULT CHARSET=utf8 COMMENT='商品表';

-- ----------------------------
-- Records of go_shoplist
-- ----------------------------

-- ----------------------------
-- Table structure for `go_shoplist_copy`
-- ----------------------------
DROP TABLE IF EXISTS `go_shoplist_copy`;
CREATE TABLE `go_shoplist_copy` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '商品id',
  `sid` int(10) unsigned NOT NULL COMMENT '同一个商品',
  `cateid` smallint(6) unsigned DEFAULT NULL COMMENT '所属栏目ID',
  `brandid` smallint(6) unsigned DEFAULT NULL COMMENT '所属品牌ID',
  `title` varchar(100) DEFAULT NULL COMMENT '商品标题',
  `title_style` varchar(100) DEFAULT NULL,
  `title2` varchar(100) DEFAULT NULL COMMENT '副标题',
  `keywords` varchar(100) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `money` decimal(10,2) DEFAULT '0.00' COMMENT '金额',
  `yunjiage` decimal(4,2) unsigned DEFAULT '1.00' COMMENT '云购人次价格',
  `zongrenshu` int(10) unsigned DEFAULT '0' COMMENT '总需人数',
  `canyurenshu` int(10) unsigned DEFAULT '0' COMMENT '已参与人数',
  `shenyurenshu` int(10) unsigned DEFAULT NULL,
  `def_renshu` int(10) unsigned DEFAULT '0',
  `qishu` smallint(6) unsigned DEFAULT '0' COMMENT '期数',
  `maxqishu` smallint(5) unsigned DEFAULT '1' COMMENT ' 最大期数',
  `thumb` varchar(255) DEFAULT NULL,
  `picarr` text COMMENT '商品图片',
  `content` mediumtext COMMENT '商品内容详情',
  `codes_table` char(20) DEFAULT NULL,
  `xsjx_time` int(10) unsigned DEFAULT NULL,
  `pos` tinyint(4) unsigned DEFAULT NULL COMMENT '是否推荐',
  `renqi` tinyint(4) unsigned DEFAULT '0' COMMENT '是否人气商品0否1是',
  `default_renci` tinyint(4) NOT NULL DEFAULT '1' COMMENT '默认购买人次',
  `time` int(10) unsigned DEFAULT NULL COMMENT '时间',
  `order` int(10) unsigned DEFAULT '1',
  `q_uid` int(10) unsigned DEFAULT NULL COMMENT '中奖人ID',
  `q_user` text COMMENT '中奖人信息',
  `q_user_code` char(20) DEFAULT NULL COMMENT '中奖码',
  `q_content` mediumtext COMMENT '揭晓内容',
  `q_counttime` char(20) DEFAULT NULL COMMENT '总时间相加',
  `q_end_time` char(20) DEFAULT NULL COMMENT '揭晓时间',
  `q_showtime` char(1) DEFAULT 'N' COMMENT 'Y/N揭晓动画',
  `q_ssccode` char(20) DEFAULT NULL,
  `q_sscopen` varchar(100) DEFAULT NULL,
  `q_sscphase` varchar(100) DEFAULT NULL,
  `q_djstime` char(20) DEFAULT NULL,
  `is_shi` tinyint(1) NOT NULL DEFAULT '0',
  `is_new` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否是最新  1是 0否',
  PRIMARY KEY (`id`),
  KEY `renqi` (`renqi`),
  KEY `order` (`yunjiage`),
  KEY `q_uid` (`q_uid`),
  KEY `sid` (`sid`),
  KEY `shenyurenshu` (`shenyurenshu`),
  KEY `q_showtime` (`q_showtime`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='商品表';

-- ----------------------------
-- Records of go_shoplist_copy
-- ----------------------------

-- ----------------------------
-- Table structure for `go_shoplist_del`
-- ----------------------------
DROP TABLE IF EXISTS `go_shoplist_del`;
CREATE TABLE `go_shoplist_del` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sid` int(10) NOT NULL COMMENT '同一个商品',
  `cateid` smallint(6) unsigned DEFAULT NULL,
  `brandid` smallint(6) unsigned DEFAULT NULL,
  `title` varchar(100) DEFAULT NULL,
  `title_style` varchar(100) DEFAULT NULL,
  `title2` varchar(100) DEFAULT NULL,
  `keywords` varchar(100) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `money` decimal(10,2) DEFAULT '0.00',
  `yunjiage` decimal(4,2) unsigned DEFAULT '1.00',
  `zongrenshu` int(10) unsigned DEFAULT '0',
  `canyurenshu` int(10) unsigned DEFAULT '0',
  `shenyurenshu` int(10) unsigned DEFAULT NULL,
  `def_renshu` int(10) unsigned DEFAULT '0',
  `qishu` smallint(6) unsigned DEFAULT '0',
  `maxqishu` smallint(5) unsigned DEFAULT '1',
  `thumb` varchar(255) DEFAULT NULL,
  `picarr` text,
  `content` mediumtext,
  `codes_table` char(20) DEFAULT NULL,
  `xsjx_time` int(10) unsigned DEFAULT NULL,
  `pos` tinyint(4) unsigned DEFAULT NULL,
  `renqi` tinyint(4) unsigned DEFAULT '0',
  `time` int(10) unsigned DEFAULT NULL,
  `order` int(10) unsigned DEFAULT '1',
  `q_uid` int(10) unsigned DEFAULT NULL,
  `q_user` text COMMENT '中奖人信息',
  `q_user_code` char(20) DEFAULT NULL,
  `q_content` mediumtext,
  `q_counttime` char(20) DEFAULT NULL,
  `q_end_time` char(20) DEFAULT NULL,
  `q_showtime` char(1) DEFAULT 'N' COMMENT 'Y/N揭晓动画',
  PRIMARY KEY (`id`),
  KEY `renqi` (`renqi`),
  KEY `order` (`yunjiage`),
  KEY `q_uid` (`q_uid`),
  KEY `sid` (`sid`),
  KEY `shenyurenshu` (`shenyurenshu`),
  KEY `q_showtime` (`q_showtime`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of go_shoplist_del
-- ----------------------------

-- ----------------------------
-- Table structure for `go_slide`
-- ----------------------------
DROP TABLE IF EXISTS `go_slide`;
CREATE TABLE `go_slide` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `img` varchar(50) DEFAULT NULL COMMENT '幻灯片',
  `title` varchar(30) DEFAULT NULL,
  `link` varchar(255) DEFAULT NULL,
  `slide_type` tinyint(2) NOT NULL DEFAULT '0' COMMENT '幻灯片的类型：0无，1为商品分类，2为商品详情',
  `slide_val` varchar(100) DEFAULT '' COMMENT '取值  ',
  `is_apple` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否是苹果相关产品  1是 0否',
  `sort_order` int(2) NOT NULL DEFAULT '10',
  PRIMARY KEY (`id`),
  KEY `img` (`img`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8 COMMENT='幻灯片表';

-- ----------------------------
-- Records of go_slide
-- ----------------------------
INSERT INTO `go_slide` VALUES ('17', 'banner/20160506/40524070511332.jpg', '苹果', 'http://121.40.198.103/', '0', '', '0', '1');

-- ----------------------------
-- Table structure for `go_source_list`
-- ----------------------------
DROP TABLE IF EXISTS `go_source_list`;
CREATE TABLE `go_source_list` (
  `s_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `s_name` varchar(100) DEFAULT NULL,
  `s_ename` varchar(200) DEFAULT NULL,
  `s_words` text,
  `s_reglink` varchar(255) DEFAULT NULL,
  `addtime` int(10) DEFAULT NULL,
  PRIMARY KEY (`s_id`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of go_source_list
-- ----------------------------
INSERT INTO `go_source_list` VALUES ('2', '儒豹浏览器', 'rbower', '关键字添加|,', '', null);
INSERT INTO `go_source_list` VALUES ('3', '亿智蘑菇', 'zlyzmg', '关键字添加|,', '', null);
INSERT INTO `go_source_list` VALUES ('4', '百度推广', 'zlbdtg', '价格|SEM00000001,一元|SEM00000117 ,', '', null);
INSERT INTO `go_source_list` VALUES ('5', '酷赚锁屏', 'zlkzsp', '关键字添加|,', '', null);
INSERT INTO `go_source_list` VALUES ('6', '信号增强器', 'zlxhzqq', '关键字添加|,', '', null);
INSERT INTO `go_source_list` VALUES ('7', '陌陌', 'zlmomo', '关键字添加|,', '', null);
INSERT INTO `go_source_list` VALUES ('8', '一元众乐', 'yyzl', '关键字添加|,', '', null);
INSERT INTO `go_source_list` VALUES ('9', '午夜艳视', 'wuyeyanshi', '关键字添加|,', '', null);
INSERT INTO `go_source_list` VALUES ('10', '腾讯应用宝', 'qq', '关键字添加|,', '', null);
INSERT INTO `go_source_list` VALUES ('11', '手机微信', 'mweixin', '关键字添加|,', '', null);
INSERT INTO `go_source_list` VALUES ('12', '91助手', 'baidu91', '关键字添加|,', '', null);
INSERT INTO `go_source_list` VALUES ('13', '安卓市场', 'androidshichang', '关键字添加|,', '', null);
INSERT INTO `go_source_list` VALUES ('14', '360手机助手', 'dev360', '关键字添加|,', '', null);
INSERT INTO `go_source_list` VALUES ('15', '百度', 'baidu', '关键字添加|,', '', null);
INSERT INTO `go_source_list` VALUES ('16', '豌豆荚', 'wandoujia', '关键字添加|,', '', null);
INSERT INTO `go_source_list` VALUES ('17', 'PP助手', 'pp', '关键字添加|,', '', null);
INSERT INTO `go_source_list` VALUES ('18', '机锋', 'gfan', '关键字添加|,', '', null);
INSERT INTO `go_source_list` VALUES ('19', '木蚂蚁', 'mumayi', '关键字添加|,', '', null);
INSERT INTO `go_source_list` VALUES ('20', '联想', 'lenovo', '关键字添加|,', '', null);
INSERT INTO `go_source_list` VALUES ('21', 'oppo', 'oppo', '关键字添加|,', '', null);
INSERT INTO `go_source_list` VALUES ('22', '应用汇', 'appchina', '关键字添加|,', '', null);
INSERT INTO `go_source_list` VALUES ('23', '搜狗', 'sogou', '关键字添加|,', '', null);
INSERT INTO `go_source_list` VALUES ('24', '魅族', 'meizu', '关键字添加|,', '', null);
INSERT INTO `go_source_list` VALUES ('25', '优亿市场', 'eoemarket', '关键字添加|,', '', null);
INSERT INTO `go_source_list` VALUES ('26', 'N多网', 'nduoa', '关键字添加|,', '', null);
INSERT INTO `go_source_list` VALUES ('27', '3G安卓市场', '3g', '关键字添加|,', '', null);
INSERT INTO `go_source_list` VALUES ('28', '安智开发者联盟', 'anzhi', '关键字添加|,', '', null);
INSERT INTO `go_source_list` VALUES ('29', '小米开发者平台', 'xiaomi', '关键字添加|,', '', null);
INSERT INTO `go_source_list` VALUES ('30', '开心看', 'zlkxk', '关键字添加|,', '', null);
INSERT INTO `go_source_list` VALUES ('31', '暴雨', 'baoyu', '关键字添加|,', '', null);
INSERT INTO `go_source_list` VALUES ('32', '康佳', 'konka', '关键字添加|,', '', null);

-- ----------------------------
-- Table structure for `go_template`
-- ----------------------------
DROP TABLE IF EXISTS `go_template`;
CREATE TABLE `go_template` (
  `template_name` char(25) NOT NULL,
  `template` char(25) NOT NULL,
  `des` varchar(100) DEFAULT NULL,
  KEY `template` (`template`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of go_template
-- ----------------------------

-- ----------------------------
-- Table structure for `go_uv`
-- ----------------------------
DROP TABLE IF EXISTS `go_uv`;
CREATE TABLE `go_uv` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `page` varchar(255) DEFAULT NULL COMMENT '当前页面',
  `source` varchar(255) DEFAULT NULL COMMENT '上一页面',
  `ip` varchar(30) DEFAULT NULL,
  `su` varchar(100) DEFAULT NULL COMMENT '渠道名称',
  `sk` varchar(100) DEFAULT NULL COMMENT '渠道关键词',
  `type` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0电脑    1手机',
  `time` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `su` (`su`),
  KEY `time` (`time`)
) ENGINE=InnoDB AUTO_INCREMENT=160666 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of go_uv
-- ----------------------------
INSERT INTO `go_uv` VALUES ('160648', 'http://121.40.198.103/index.php', '', '218.241.196.226', '', '', '0', '1462439207');
INSERT INTO `go_uv` VALUES ('160649', 'http://121.40.198.103/index.php/goods_list', 'http://121.40.198.103/', '218.241.196.226', '', '', '0', '1462439232');
INSERT INTO `go_uv` VALUES ('160650', 'http://121.40.198.103/index.php/ten', 'http://121.40.198.103/index.php/goods_list', '218.241.196.226', '', '', '0', '1462439234');
INSERT INTO `go_uv` VALUES ('160651', 'http://121.40.198.103/index.php/shaidan/', 'http://121.40.198.103/index.php/single/newbie', '218.241.196.226', '', '', '0', '1462439236');
INSERT INTO `go_uv` VALUES ('160652', 'http://121.40.198.103/index.php/shaidan/images/pa', 'http://121.40.198.103/index.php/shaidan/', '218.241.196.226', '', '', '0', '1462439236');
INSERT INTO `go_uv` VALUES ('160653', 'http://121.40.198.103/index.php/goods_lottery', 'http://121.40.198.103/index.php/shaidan/', '218.241.196.226', '', '', '0', '1462439237');
INSERT INTO `go_uv` VALUES ('160654', 'http://www.baidu.com:443/index.php', '', '107.151.226.203', '', '', '0', '1462439313');
INSERT INTO `go_uv` VALUES ('160655', 'http://www.baidu.com:443/index.php', '', '23.251.40.103', '', '', '0', '1462439316');
INSERT INTO `go_uv` VALUES ('160656', 'http://wetowin.com/index.php', 'http://uptime.com/wetowin.com', '45.79.81.142', '', '', '0', '1462439383');
INSERT INTO `go_uv` VALUES ('160657', 'http://www.baidu.com:443/index.php', '', '107.151.226.242', '', '', '0', '1462440870');
INSERT INTO `go_uv` VALUES ('160658', 'http://121.40.198.103/index.php', '', '218.241.196.226', '', '', '0', '1462511086');
INSERT INTO `go_uv` VALUES ('160659', 'http://121.40.198.103/index.php/goods_list', 'http://121.40.198.103/', '218.241.196.226', '', '', '0', '1462511357');
INSERT INTO `go_uv` VALUES ('160660', 'http://121.40.198.103/index.php/ten', 'http://121.40.198.103/index.php/goods_list', '218.241.196.226', '', '', '0', '1462511359');
INSERT INTO `go_uv` VALUES ('160661', 'http://121.40.198.103/index.php/shaidan/', 'http://121.40.198.103/index.php/single/newbie', '218.241.196.226', '', '', '0', '1462511361');
INSERT INTO `go_uv` VALUES ('160662', 'http://121.40.198.103/index.php/shaidan/images/pa', 'http://121.40.198.103/index.php/shaidan/', '218.241.196.226', '', '', '0', '1462511362');
INSERT INTO `go_uv` VALUES ('160663', 'http://121.40.198.103/index.php/goods_lottery', 'http://121.40.198.103/index.php/shaidan/', '218.241.196.226', '', '', '0', '1462511362');
INSERT INTO `go_uv` VALUES ('160664', 'http://121.40.198.103/index.php/login/aHR0cDovLzEyMS40MC4xOTguMTAzL2luZGV4LnBocC9nb29kc19sb3R0ZXJ5', 'http://121.40.198.103/index.php/goods_lottery', '218.241.196.226', '', '', '0', '1462511363');
INSERT INTO `go_uv` VALUES ('160665', 'http://121.40.198.103/index.php', '', '222.209.140.8', '', '', '0', '1462512698');

-- ----------------------------
-- Table structure for `go_vote_activer`
-- ----------------------------
DROP TABLE IF EXISTS `go_vote_activer`;
CREATE TABLE `go_vote_activer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `option_id` int(11) NOT NULL,
  `vote_id` int(11) DEFAULT NULL,
  `userid` int(11) DEFAULT NULL,
  `ip` char(20) DEFAULT NULL,
  `subtime` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of go_vote_activer
-- ----------------------------

-- ----------------------------
-- Table structure for `go_vote_option`
-- ----------------------------
DROP TABLE IF EXISTS `go_vote_option`;
CREATE TABLE `go_vote_option` (
  `option_id` int(11) NOT NULL AUTO_INCREMENT,
  `vote_id` int(11) DEFAULT NULL,
  `option_title` varchar(100) DEFAULT NULL,
  `option_number` int(11) unsigned DEFAULT '0',
  PRIMARY KEY (`option_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of go_vote_option
-- ----------------------------

-- ----------------------------
-- Table structure for `go_vote_subject`
-- ----------------------------
DROP TABLE IF EXISTS `go_vote_subject`;
CREATE TABLE `go_vote_subject` (
  `vote_id` int(11) NOT NULL AUTO_INCREMENT,
  `vote_title` varchar(100) DEFAULT NULL,
  `vote_starttime` int(11) DEFAULT NULL,
  `vote_endtime` int(11) DEFAULT NULL,
  `vote_sendtime` int(11) DEFAULT NULL,
  `vote_description` text,
  `vote_allowview` tinyint(1) DEFAULT NULL,
  `vote_allowguest` tinyint(1) DEFAULT NULL,
  `vote_interval` int(11) DEFAULT '0',
  `vote_enabled` tinyint(1) DEFAULT NULL,
  `vote_number` int(11) DEFAULT NULL,
  PRIMARY KEY (`vote_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of go_vote_subject
-- ----------------------------

-- ----------------------------
-- Table structure for `go_wap`
-- ----------------------------
DROP TABLE IF EXISTS `go_wap`;
CREATE TABLE `go_wap` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `img` varchar(50) DEFAULT NULL COMMENT '幻灯片',
  `title` varchar(30) DEFAULT NULL,
  `color` varchar(30) DEFAULT NULL,
  `link` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `img` (`img`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='幻灯片表';

-- ----------------------------
-- Records of go_wap
-- ----------------------------

-- ----------------------------
-- Table structure for `go_zyy_log`
-- ----------------------------
DROP TABLE IF EXISTS `go_zyy_log`;
CREATE TABLE `go_zyy_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `log` text,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=517 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of go_zyy_log
-- ----------------------------
INSERT INTO `go_zyy_log` VALUES ('1', '{\"appid\":\"wxfd1c82ecf58bb9bf\",\"bank_type\":\"CFT\",\"cash_fee\":\"1\",\"fee_type\":\"CNY\",\"is_subscribe\":\"N\",\"mch_id\":\"1323555901\",\"nonce_str\":\"b21trrho3lve40ozxpm7wimub98ujazk\",\"openid\":\"ootf7slsTuQNRwWwMoawPVKQx1nA\",\"out_trade_no\":\"C14618273774980299\",\"result_code\":\"SUCCESS\",\"return_code\":\"SUCCESS\",\"sign\":\"93E0347A210103923F9A851123DD01B0\",\"time_end\":\"20160428150950\",\"total_fee\":\"1\",\"trade_type\":\"JSAPI\",\"transaction_id\":\"4005502001201604285305651944\"} zyy');
INSERT INTO `go_zyy_log` VALUES ('2', '{\"appid\":\"wxfd1c82ecf58bb9bf\",\"bank_type\":\"CFT\",\"cash_fee\":\"100\",\"fee_type\":\"CNY\",\"is_subscribe\":\"Y\",\"mch_id\":\"1323555901\",\"nonce_str\":\"1drwc2qqqnqan9em3n71jeo9j9dex15o\",\"openid\":\"ootf7slcvo-OZ2d7skGTs_I5KnQs\",\"out_trade_no\":\"C14618277528957597\",\"result_code\":\"SUCCESS\",\"return_code\":\"SUCCESS\",\"sign\":\"9B40124B334E4B367E07BE77C38A2E62\",\"time_end\":\"20160428151558\",\"total_fee\":\"100\",\"trade_type\":\"JSAPI\",\"transaction_id\":\"4003722001201604285305733532\"} zyy');
INSERT INTO `go_zyy_log` VALUES ('3', '{\"appid\":\"wxfd1c82ecf58bb9bf\",\"bank_type\":\"ICBC_DEBIT\",\"cash_fee\":\"100\",\"fee_type\":\"CNY\",\"is_subscribe\":\"Y\",\"mch_id\":\"1323555901\",\"nonce_str\":\"k0m6iuwaq5uuu751dqmq52uqs8y6f7pl\",\"openid\":\"ootf7sqCf6laucAfD8orWthqpZZ8\",\"out_trade_no\":\"C14618279563995649\",\"result_code\":\"SUCCESS\",\"return_code\":\"SUCCESS\",\"sign\":\"27CED255637718CBF1EC6869F4865F88\",\"time_end\":\"20160428151927\",\"total_fee\":\"100\",\"trade_type\":\"JSAPI\",\"transaction_id\":\"4008992001201604285305781366\"} zyy');
INSERT INTO `go_zyy_log` VALUES ('4', '{\"appid\":\"wxfd1c82ecf58bb9bf\",\"bank_type\":\"ICBC_DEBIT\",\"cash_fee\":\"100\",\"fee_type\":\"CNY\",\"is_subscribe\":\"Y\",\"mch_id\":\"1323555901\",\"nonce_str\":\"fvn1vnca7nqxdjvaiehjcwyo90t3tjph\",\"openid\":\"ootf7snVZoSf46uN03s30Cl1tgW4\",\"out_trade_no\":\"C14618279908689765\",\"result_code\":\"SUCCESS\",\"return_code\":\"SUCCESS\",\"sign\":\"DB874E77FD859B5AD095AA141CE33480\",\"time_end\":\"20160428152007\",\"total_fee\":\"100\",\"trade_type\":\"JSAPI\",\"transaction_id\":\"4008792001201604285305789182\"} zyy');
INSERT INTO `go_zyy_log` VALUES ('5', '{\"appid\":\"wxfd1c82ecf58bb9bf\",\"bank_type\":\"CMB_DEBIT\",\"cash_fee\":\"100\",\"fee_type\":\"CNY\",\"is_subscribe\":\"Y\",\"mch_id\":\"1323555901\",\"nonce_str\":\"2v75gngjsi3bjvu80kn4n6pyymih2f5t\",\"openid\":\"ootf7smqFa1V2mM4uzOQ_IEkFiJI\",\"out_trade_no\":\"C14618280887302309\",\"result_code\":\"SUCCESS\",\"return_code\":\"SUCCESS\",\"sign\":\"578907FE057E850105DC72CAE82AD7EB\",\"time_end\":\"20160428152135\",\"total_fee\":\"100\",\"trade_type\":\"JSAPI\",\"transaction_id\":\"4005312001201604285305814638\"} zyy');
INSERT INTO `go_zyy_log` VALUES ('6', '{\"appid\":\"wxfd1c82ecf58bb9bf\",\"bank_type\":\"CMB_DEBIT\",\"cash_fee\":\"100\",\"fee_type\":\"CNY\",\"is_subscribe\":\"Y\",\"mch_id\":\"1323555901\",\"nonce_str\":\"4fimpfhjgf2huen5f9fzi2k8ki426rve\",\"openid\":\"ootf7smqFa1V2mM4uzOQ_IEkFiJI\",\"out_trade_no\":\"C14618281388560900\",\"result_code\":\"SUCCESS\",\"return_code\":\"SUCCESS\",\"sign\":\"5261B2436523A45C6BBD54598628EC51\",\"time_end\":\"20160428152226\",\"total_fee\":\"100\",\"trade_type\":\"JSAPI\",\"transaction_id\":\"4005312001201604285305825849\"} zyy');
INSERT INTO `go_zyy_log` VALUES ('7', '{\"appid\":\"wxfd1c82ecf58bb9bf\",\"bank_type\":\"CFT\",\"cash_fee\":\"100\",\"fee_type\":\"CNY\",\"is_subscribe\":\"Y\",\"mch_id\":\"1323555901\",\"nonce_str\":\"8fxpezn0ugue9gmhzmul38qdlxmvjsja\",\"openid\":\"ootf7slcvo-OZ2d7skGTs_I5KnQs\",\"out_trade_no\":\"C14618284942534367\",\"result_code\":\"SUCCESS\",\"return_code\":\"SUCCESS\",\"sign\":\"90D7007CBBFFB24767F0782E8A80F234\",\"time_end\":\"20160428152821\",\"total_fee\":\"100\",\"trade_type\":\"JSAPI\",\"transaction_id\":\"4003722001201604285307334532\"} zyy');
INSERT INTO `go_zyy_log` VALUES ('8', '{\"appid\":\"wxfd1c82ecf58bb9bf\",\"bank_type\":\"ICBC_DEBIT\",\"cash_fee\":\"100\",\"fee_type\":\"CNY\",\"is_subscribe\":\"Y\",\"mch_id\":\"1323555901\",\"nonce_str\":\"b8kh3rs2ebdis0rccg50vr2hs94f0ea5\",\"openid\":\"ootf7sqCf6laucAfD8orWthqpZZ8\",\"out_trade_no\":\"C14618291265604282\",\"result_code\":\"SUCCESS\",\"return_code\":\"SUCCESS\",\"sign\":\"0CF6551256E93C0EB24801B22F02596A\",\"time_end\":\"20160428153858\",\"total_fee\":\"100\",\"trade_type\":\"JSAPI\",\"transaction_id\":\"4008992001201604285306055954\"} zyy');
INSERT INTO `go_zyy_log` VALUES ('9', '{\"appid\":\"wxfd1c82ecf58bb9bf\",\"bank_type\":\"ICBC_DEBIT\",\"cash_fee\":\"100\",\"fee_type\":\"CNY\",\"is_subscribe\":\"Y\",\"mch_id\":\"1323555901\",\"nonce_str\":\"j4ovykbssioeojeue1ldx0hrf744cvrc\",\"openid\":\"ootf7sqCf6laucAfD8orWthqpZZ8\",\"out_trade_no\":\"C14618291674049639\",\"result_code\":\"SUCCESS\",\"return_code\":\"SUCCESS\",\"sign\":\"4064FE3461478675C55A4163AFBFE5AA\",\"time_end\":\"20160428153933\",\"total_fee\":\"100\",\"trade_type\":\"JSAPI\",\"transaction_id\":\"4008992001201604285307492156\"} zyy');
INSERT INTO `go_zyy_log` VALUES ('10', '{\"appid\":\"wxfd1c82ecf58bb9bf\",\"bank_type\":\"ICBC_DEBIT\",\"cash_fee\":\"100\",\"fee_type\":\"CNY\",\"is_subscribe\":\"Y\",\"mch_id\":\"1323555901\",\"nonce_str\":\"j4ovykbssioeojeue1ldx0hrf744cvrc\",\"openid\":\"ootf7sqCf6laucAfD8orWthqpZZ8\",\"out_trade_no\":\"C14618291674049639\",\"result_code\":\"SUCCESS\",\"return_code\":\"SUCCESS\",\"sign\":\"4064FE3461478675C55A4163AFBFE5AA\",\"time_end\":\"20160428153933\",\"total_fee\":\"100\",\"trade_type\":\"JSAPI\",\"transaction_id\":\"4008992001201604285307492156\"} zyy');
INSERT INTO `go_zyy_log` VALUES ('11', '{\"appid\":\"wxfd1c82ecf58bb9bf\",\"bank_type\":\"ICBC_DEBIT\",\"cash_fee\":\"100\",\"fee_type\":\"CNY\",\"is_subscribe\":\"Y\",\"mch_id\":\"1323555901\",\"nonce_str\":\"j4ovykbssioeojeue1ldx0hrf744cvrc\",\"openid\":\"ootf7sqCf6laucAfD8orWthqpZZ8\",\"out_trade_no\":\"C14618291674049639\",\"result_code\":\"SUCCESS\",\"return_code\":\"SUCCESS\",\"sign\":\"4064FE3461478675C55A4163AFBFE5AA\",\"time_end\":\"20160428153933\",\"total_fee\":\"100\",\"trade_type\":\"JSAPI\",\"transaction_id\":\"4008992001201604285307492156\"} zyy');
INSERT INTO `go_zyy_log` VALUES ('12', '{\"appid\":\"wxfd1c82ecf58bb9bf\",\"bank_type\":\"ICBC_DEBIT\",\"cash_fee\":\"100\",\"fee_type\":\"CNY\",\"is_subscribe\":\"Y\",\"mch_id\":\"1323555901\",\"nonce_str\":\"j4ovykbssioeojeue1ldx0hrf744cvrc\",\"openid\":\"ootf7sqCf6laucAfD8orWthqpZZ8\",\"out_trade_no\":\"C14618291674049639\",\"result_code\":\"SUCCESS\",\"return_code\":\"SUCCESS\",\"sign\":\"4064FE3461478675C55A4163AFBFE5AA\",\"time_end\":\"20160428153933\",\"total_fee\":\"100\",\"trade_type\":\"JSAPI\",\"transaction_id\":\"4008992001201604285307492156\"} zyy');
INSERT INTO `go_zyy_log` VALUES ('13', '{\"appid\":\"wxfd1c82ecf58bb9bf\",\"bank_type\":\"ICBC_DEBIT\",\"cash_fee\":\"100\",\"fee_type\":\"CNY\",\"is_subscribe\":\"Y\",\"mch_id\":\"1323555901\",\"nonce_str\":\"j4ovykbssioeojeue1ldx0hrf744cvrc\",\"openid\":\"ootf7sqCf6laucAfD8orWthqpZZ8\",\"out_trade_no\":\"C14618291674049639\",\"result_code\":\"SUCCESS\",\"return_code\":\"SUCCESS\",\"sign\":\"4064FE3461478675C55A4163AFBFE5AA\",\"time_end\":\"20160428153933\",\"total_fee\":\"100\",\"trade_type\":\"JSAPI\",\"transaction_id\":\"4008992001201604285307492156\"} zyy');
INSERT INTO `go_zyy_log` VALUES ('14', '{\"appid\":\"wxfd1c82ecf58bb9bf\",\"bank_type\":\"CFT\",\"cash_fee\":\"100\",\"fee_type\":\"CNY\",\"is_subscribe\":\"Y\",\"mch_id\":\"1323555901\",\"nonce_str\":\"s3aglhae95jtne3gwbqu3w65i83trlrz\",\"openid\":\"ootf7slcvo-OZ2d7skGTs_I5KnQs\",\"out_trade_no\":\"C14618295115989695\",\"result_code\":\"SUCCESS\",\"return_code\":\"SUCCESS\",\"sign\":\"F39E4718F803BD04A466417C6DB9C6CD\",\"time_end\":\"20160428154517\",\"total_fee\":\"100\",\"trade_type\":\"JSAPI\",\"transaction_id\":\"4003722001201604285307573635\"} zyy');
INSERT INTO `go_zyy_log` VALUES ('15', '{\"appid\":\"wxfd1c82ecf58bb9bf\",\"bank_type\":\"CFT\",\"cash_fee\":\"100\",\"fee_type\":\"CNY\",\"is_subscribe\":\"Y\",\"mch_id\":\"1323555901\",\"nonce_str\":\"njzaex2aupq6edavvrwn9j6wpxo2163o\",\"openid\":\"ootf7slcvo-OZ2d7skGTs_I5KnQs\",\"out_trade_no\":\"C14618298113077464\",\"result_code\":\"SUCCESS\",\"return_code\":\"SUCCESS\",\"sign\":\"B1FA6E0036A99BDA4386696D944B4214\",\"time_end\":\"20160428155017\",\"total_fee\":\"100\",\"trade_type\":\"JSAPI\",\"transaction_id\":\"4003722001201604285307642293\"} zyy');
INSERT INTO `go_zyy_log` VALUES ('16', '{\"appid\":\"wxfd1c82ecf58bb9bf\",\"bank_type\":\"ICBC_DEBIT\",\"cash_fee\":\"100\",\"fee_type\":\"CNY\",\"is_subscribe\":\"Y\",\"mch_id\":\"1323555901\",\"nonce_str\":\"j4ovykbssioeojeue1ldx0hrf744cvrc\",\"openid\":\"ootf7sqCf6laucAfD8orWthqpZZ8\",\"out_trade_no\":\"C14618291674049639\",\"result_code\":\"SUCCESS\",\"return_code\":\"SUCCESS\",\"sign\":\"4064FE3461478675C55A4163AFBFE5AA\",\"time_end\":\"20160428153933\",\"total_fee\":\"100\",\"trade_type\":\"JSAPI\",\"transaction_id\":\"4008992001201604285307492156\"} zyy');
INSERT INTO `go_zyy_log` VALUES ('17', '{\"appid\":\"wxfd1c82ecf58bb9bf\",\"bank_type\":\"ICBC_DEBIT\",\"cash_fee\":\"100\",\"fee_type\":\"CNY\",\"is_subscribe\":\"Y\",\"mch_id\":\"1323555901\",\"nonce_str\":\"j4ovykbssioeojeue1ldx0hrf744cvrc\",\"openid\":\"ootf7sqCf6laucAfD8orWthqpZZ8\",\"out_trade_no\":\"C14618291674049639\",\"result_code\":\"SUCCESS\",\"return_code\":\"SUCCESS\",\"sign\":\"4064FE3461478675C55A4163AFBFE5AA\",\"time_end\":\"20160428153933\",\"total_fee\":\"100\",\"trade_type\":\"JSAPI\",\"transaction_id\":\"4008992001201604285307492156\"} zyy');
INSERT INTO `go_zyy_log` VALUES ('18', '{\"appid\":\"wxfd1c82ecf58bb9bf\",\"bank_type\":\"ICBC_DEBIT\",\"cash_fee\":\"100\",\"fee_type\":\"CNY\",\"is_subscribe\":\"Y\",\"mch_id\":\"1323555901\",\"nonce_str\":\"j4ovykbssioeojeue1ldx0hrf744cvrc\",\"openid\":\"ootf7sqCf6laucAfD8orWthqpZZ8\",\"out_trade_no\":\"C14618291674049639\",\"result_code\":\"SUCCESS\",\"return_code\":\"SUCCESS\",\"sign\":\"4064FE3461478675C55A4163AFBFE5AA\",\"time_end\":\"20160428153933\",\"total_fee\":\"100\",\"trade_type\":\"JSAPI\",\"transaction_id\":\"4008992001201604285307492156\"} zyy');
INSERT INTO `go_zyy_log` VALUES ('19', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('20', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('21', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('22', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('23', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('24', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('25', '{\"appid\":\"wxfd1c82ecf58bb9bf\",\"bank_type\":\"CMB_DEBIT\",\"cash_fee\":\"300\",\"fee_type\":\"CNY\",\"is_subscribe\":\"Y\",\"mch_id\":\"1323555901\",\"nonce_str\":\"ukftm39u9bb2yf60lu3tqa9ulozvl4ye\",\"openid\":\"ootf7smqFa1V2mM4uzOQ_IEkFiJI\",\"out_trade_no\":\"C14618361426128367\",\"result_code\":\"SUCCESS\",\"return_code\":\"SUCCESS\",\"sign\":\"B8068329C74EECE9378EFCB6D1CE1273\",\"time_end\":\"20160428173604\",\"total_fee\":\"300\",\"trade_type\":\"NATIVE\",\"transaction_id\":\"4005312001201604285309826650\"} zyy');
INSERT INTO `go_zyy_log` VALUES ('26', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('27', '{\"appid\":\"wxfd1c82ecf58bb9bf\",\"bank_type\":\"CMB_DEBIT\",\"cash_fee\":\"300\",\"fee_type\":\"CNY\",\"is_subscribe\":\"Y\",\"mch_id\":\"1323555901\",\"nonce_str\":\"ukftm39u9bb2yf60lu3tqa9ulozvl4ye\",\"openid\":\"ootf7smqFa1V2mM4uzOQ_IEkFiJI\",\"out_trade_no\":\"C14618361426128367\",\"result_code\":\"SUCCESS\",\"return_code\":\"SUCCESS\",\"sign\":\"B8068329C74EECE9378EFCB6D1CE1273\",\"time_end\":\"20160428173604\",\"total_fee\":\"300\",\"trade_type\":\"NATIVE\",\"transaction_id\":\"4005312001201604285309826650\"} zyy');
INSERT INTO `go_zyy_log` VALUES ('28', '{\"appid\":\"wxfd1c82ecf58bb9bf\",\"bank_type\":\"CMB_DEBIT\",\"cash_fee\":\"300\",\"fee_type\":\"CNY\",\"is_subscribe\":\"Y\",\"mch_id\":\"1323555901\",\"nonce_str\":\"ukftm39u9bb2yf60lu3tqa9ulozvl4ye\",\"openid\":\"ootf7smqFa1V2mM4uzOQ_IEkFiJI\",\"out_trade_no\":\"C14618361426128367\",\"result_code\":\"SUCCESS\",\"return_code\":\"SUCCESS\",\"sign\":\"B8068329C74EECE9378EFCB6D1CE1273\",\"time_end\":\"20160428173604\",\"total_fee\":\"300\",\"trade_type\":\"NATIVE\",\"transaction_id\":\"4005312001201604285309826650\"} zyy');
INSERT INTO `go_zyy_log` VALUES ('29', '{\"appid\":\"wxfd1c82ecf58bb9bf\",\"bank_type\":\"CMB_DEBIT\",\"cash_fee\":\"300\",\"fee_type\":\"CNY\",\"is_subscribe\":\"Y\",\"mch_id\":\"1323555901\",\"nonce_str\":\"ukftm39u9bb2yf60lu3tqa9ulozvl4ye\",\"openid\":\"ootf7smqFa1V2mM4uzOQ_IEkFiJI\",\"out_trade_no\":\"C14618361426128367\",\"result_code\":\"SUCCESS\",\"return_code\":\"SUCCESS\",\"sign\":\"B8068329C74EECE9378EFCB6D1CE1273\",\"time_end\":\"20160428173604\",\"total_fee\":\"300\",\"trade_type\":\"NATIVE\",\"transaction_id\":\"4005312001201604285309826650\"} zyy');
INSERT INTO `go_zyy_log` VALUES ('30', '{\"appid\":\"wxfd1c82ecf58bb9bf\",\"bank_type\":\"CMB_DEBIT\",\"cash_fee\":\"300\",\"fee_type\":\"CNY\",\"is_subscribe\":\"Y\",\"mch_id\":\"1323555901\",\"nonce_str\":\"ukftm39u9bb2yf60lu3tqa9ulozvl4ye\",\"openid\":\"ootf7smqFa1V2mM4uzOQ_IEkFiJI\",\"out_trade_no\":\"C14618361426128367\",\"result_code\":\"SUCCESS\",\"return_code\":\"SUCCESS\",\"sign\":\"B8068329C74EECE9378EFCB6D1CE1273\",\"time_end\":\"20160428173604\",\"total_fee\":\"300\",\"trade_type\":\"NATIVE\",\"transaction_id\":\"4005312001201604285309826650\"} zyy');
INSERT INTO `go_zyy_log` VALUES ('31', '{\"appid\":\"wxfd1c82ecf58bb9bf\",\"bank_type\":\"ICBC_DEBIT\",\"cash_fee\":\"100\",\"fee_type\":\"CNY\",\"is_subscribe\":\"Y\",\"mch_id\":\"1323555901\",\"nonce_str\":\"j4ovykbssioeojeue1ldx0hrf744cvrc\",\"openid\":\"ootf7sqCf6laucAfD8orWthqpZZ8\",\"out_trade_no\":\"C14618291674049639\",\"result_code\":\"SUCCESS\",\"return_code\":\"SUCCESS\",\"sign\":\"4064FE3461478675C55A4163AFBFE5AA\",\"time_end\":\"20160428153933\",\"total_fee\":\"100\",\"trade_type\":\"JSAPI\",\"transaction_id\":\"4008992001201604285307492156\"} zyy');
INSERT INTO `go_zyy_log` VALUES ('32', '{\"appid\":\"wxfd1c82ecf58bb9bf\",\"bank_type\":\"CMB_DEBIT\",\"cash_fee\":\"300\",\"fee_type\":\"CNY\",\"is_subscribe\":\"Y\",\"mch_id\":\"1323555901\",\"nonce_str\":\"ukftm39u9bb2yf60lu3tqa9ulozvl4ye\",\"openid\":\"ootf7smqFa1V2mM4uzOQ_IEkFiJI\",\"out_trade_no\":\"C14618361426128367\",\"result_code\":\"SUCCESS\",\"return_code\":\"SUCCESS\",\"sign\":\"B8068329C74EECE9378EFCB6D1CE1273\",\"time_end\":\"20160428173604\",\"total_fee\":\"300\",\"trade_type\":\"NATIVE\",\"transaction_id\":\"4005312001201604285309826650\"} zyy');
INSERT INTO `go_zyy_log` VALUES ('33', '{\"appid\":\"wxfd1c82ecf58bb9bf\",\"bank_type\":\"CMB_DEBIT\",\"cash_fee\":\"300\",\"fee_type\":\"CNY\",\"is_subscribe\":\"Y\",\"mch_id\":\"1323555901\",\"nonce_str\":\"ukftm39u9bb2yf60lu3tqa9ulozvl4ye\",\"openid\":\"ootf7smqFa1V2mM4uzOQ_IEkFiJI\",\"out_trade_no\":\"C14618361426128367\",\"result_code\":\"SUCCESS\",\"return_code\":\"SUCCESS\",\"sign\":\"B8068329C74EECE9378EFCB6D1CE1273\",\"time_end\":\"20160428173604\",\"total_fee\":\"300\",\"trade_type\":\"NATIVE\",\"transaction_id\":\"4005312001201604285309826650\"} zyy');
INSERT INTO `go_zyy_log` VALUES ('34', '{\"appid\":\"wxfd1c82ecf58bb9bf\",\"bank_type\":\"ICBC_DEBIT\",\"cash_fee\":\"100\",\"fee_type\":\"CNY\",\"is_subscribe\":\"Y\",\"mch_id\":\"1323555901\",\"nonce_str\":\"j4ovykbssioeojeue1ldx0hrf744cvrc\",\"openid\":\"ootf7sqCf6laucAfD8orWthqpZZ8\",\"out_trade_no\":\"C14618291674049639\",\"result_code\":\"SUCCESS\",\"return_code\":\"SUCCESS\",\"sign\":\"4064FE3461478675C55A4163AFBFE5AA\",\"time_end\":\"20160428153933\",\"total_fee\":\"100\",\"trade_type\":\"JSAPI\",\"transaction_id\":\"4008992001201604285307492156\"} zyy');
INSERT INTO `go_zyy_log` VALUES ('35', '{\"appid\":\"wxfd1c82ecf58bb9bf\",\"bank_type\":\"CMB_DEBIT\",\"cash_fee\":\"300\",\"fee_type\":\"CNY\",\"is_subscribe\":\"Y\",\"mch_id\":\"1323555901\",\"nonce_str\":\"ukftm39u9bb2yf60lu3tqa9ulozvl4ye\",\"openid\":\"ootf7smqFa1V2mM4uzOQ_IEkFiJI\",\"out_trade_no\":\"C14618361426128367\",\"result_code\":\"SUCCESS\",\"return_code\":\"SUCCESS\",\"sign\":\"B8068329C74EECE9378EFCB6D1CE1273\",\"time_end\":\"20160428173604\",\"total_fee\":\"300\",\"trade_type\":\"NATIVE\",\"transaction_id\":\"4005312001201604285309826650\"} zyy');
INSERT INTO `go_zyy_log` VALUES ('36', '{\"appid\":\"wxfd1c82ecf58bb9bf\",\"bank_type\":\"CMB_DEBIT\",\"cash_fee\":\"300\",\"fee_type\":\"CNY\",\"is_subscribe\":\"Y\",\"mch_id\":\"1323555901\",\"nonce_str\":\"ukftm39u9bb2yf60lu3tqa9ulozvl4ye\",\"openid\":\"ootf7smqFa1V2mM4uzOQ_IEkFiJI\",\"out_trade_no\":\"C14618361426128367\",\"result_code\":\"SUCCESS\",\"return_code\":\"SUCCESS\",\"sign\":\"B8068329C74EECE9378EFCB6D1CE1273\",\"time_end\":\"20160428173604\",\"total_fee\":\"300\",\"trade_type\":\"NATIVE\",\"transaction_id\":\"4005312001201604285309826650\"} zyy');
INSERT INTO `go_zyy_log` VALUES ('37', '{\"appid\":\"wxfd1c82ecf58bb9bf\",\"bank_type\":\"CMB_DEBIT\",\"cash_fee\":\"300\",\"fee_type\":\"CNY\",\"is_subscribe\":\"Y\",\"mch_id\":\"1323555901\",\"nonce_str\":\"ukftm39u9bb2yf60lu3tqa9ulozvl4ye\",\"openid\":\"ootf7smqFa1V2mM4uzOQ_IEkFiJI\",\"out_trade_no\":\"C14618361426128367\",\"result_code\":\"SUCCESS\",\"return_code\":\"SUCCESS\",\"sign\":\"B8068329C74EECE9378EFCB6D1CE1273\",\"time_end\":\"20160428173604\",\"total_fee\":\"300\",\"trade_type\":\"NATIVE\",\"transaction_id\":\"4005312001201604285309826650\"} zyy');
INSERT INTO `go_zyy_log` VALUES ('38', '{\"appid\":\"wxfd1c82ecf58bb9bf\",\"bank_type\":\"CFT\",\"cash_fee\":\"1\",\"fee_type\":\"CNY\",\"is_subscribe\":\"N\",\"mch_id\":\"1323555901\",\"nonce_str\":\"g94vwtzy15jnu3yjm2d1n9uaial0yx4t\",\"openid\":\"ootf7slsTuQNRwWwMoawPVKQx1nA\",\"out_trade_no\":\"C14619128133996808\",\"result_code\":\"SUCCESS\",\"return_code\":\"SUCCESS\",\"sign\":\"7ED8D74E83DC81B89DAF12CEE74F13A6\",\"time_end\":\"20160429145345\",\"total_fee\":\"1\",\"trade_type\":\"JSAPI\",\"transaction_id\":\"4005502001201604295337930637\"} zyy');
INSERT INTO `go_zyy_log` VALUES ('39', '{\"appid\":\"wxfd1c82ecf58bb9bf\",\"bank_type\":\"CFT\",\"cash_fee\":\"1\",\"fee_type\":\"CNY\",\"is_subscribe\":\"N\",\"mch_id\":\"1323555901\",\"nonce_str\":\"n28qy7e3bxyn9sxmbfbiiw13ut8qyn97\",\"openid\":\"ootf7slsTuQNRwWwMoawPVKQx1nA\",\"out_trade_no\":\"C14619184995352112\",\"result_code\":\"SUCCESS\",\"return_code\":\"SUCCESS\",\"sign\":\"C1BE1BE57CEA4E0589391B040146FE88\",\"time_end\":\"20160429162827\",\"total_fee\":\"1\",\"trade_type\":\"JSAPI\",\"transaction_id\":\"4005502001201604295342645732\"} zyy');
INSERT INTO `go_zyy_log` VALUES ('40', '{\"appid\":\"wxfd1c82ecf58bb9bf\",\"bank_type\":\"CFT\",\"cash_fee\":\"1\",\"fee_type\":\"CNY\",\"is_subscribe\":\"N\",\"mch_id\":\"1323555901\",\"nonce_str\":\"ugksvq9erpnzlc0g4dt7t8gksx2dweks\",\"openid\":\"ootf7slsTuQNRwWwMoawPVKQx1nA\",\"out_trade_no\":\"C14619185768648210\",\"result_code\":\"SUCCESS\",\"return_code\":\"SUCCESS\",\"sign\":\"EF95C1D56449613755F748D0793053F4\",\"time_end\":\"20160429162945\",\"total_fee\":\"1\",\"trade_type\":\"JSAPI\",\"transaction_id\":\"4005502001201604295342666809\"} zyy');
INSERT INTO `go_zyy_log` VALUES ('41', '{\"appid\":\"wxfd1c82ecf58bb9bf\",\"bank_type\":\"CFT\",\"cash_fee\":\"1\",\"fee_type\":\"CNY\",\"is_subscribe\":\"N\",\"mch_id\":\"1323555901\",\"nonce_str\":\"zy1pnf99r89vybiakex5w8vm3bvqdii2\",\"openid\":\"ootf7slsTuQNRwWwMoawPVKQx1nA\",\"out_trade_no\":\"C14619186786167751\",\"result_code\":\"SUCCESS\",\"return_code\":\"SUCCESS\",\"sign\":\"F28A5AE20A47E1ABA1F495D835C79128\",\"time_end\":\"20160429163127\",\"total_fee\":\"1\",\"trade_type\":\"JSAPI\",\"transaction_id\":\"4005502001201604295342690026\"} zyy');
INSERT INTO `go_zyy_log` VALUES ('42', '{\"appid\":\"wxfd1c82ecf58bb9bf\",\"bank_type\":\"CFT\",\"cash_fee\":\"1\",\"fee_type\":\"CNY\",\"is_subscribe\":\"N\",\"mch_id\":\"1323555901\",\"nonce_str\":\"9rvn2j2fek8gvqlvz1fp1zz6iqcy1m4f\",\"openid\":\"ootf7slsTuQNRwWwMoawPVKQx1nA\",\"out_trade_no\":\"C14619188177443435\",\"result_code\":\"SUCCESS\",\"return_code\":\"SUCCESS\",\"sign\":\"D0C31423D8CC6FAF7ABAAEA97FAE1758\",\"time_end\":\"20160429163347\",\"total_fee\":\"1\",\"trade_type\":\"JSAPI\",\"transaction_id\":\"4005502001201604295342714580\"} zyy');
INSERT INTO `go_zyy_log` VALUES ('43', '{\"appid\":\"wxfd1c82ecf58bb9bf\",\"bank_type\":\"CFT\",\"cash_fee\":\"1\",\"fee_type\":\"CNY\",\"is_subscribe\":\"Y\",\"mch_id\":\"1323555901\",\"nonce_str\":\"37pyjflzzfzcwrpnosowxb7z10b0wvvz\",\"openid\":\"ootf7sgpX-vESbMv7Mmm2D3CCUWk\",\"out_trade_no\":\"C14619192546745053\",\"result_code\":\"SUCCESS\",\"return_code\":\"SUCCESS\",\"sign\":\"082979E7A81E1F71889A10DA003FA84A\",\"time_end\":\"20160429164159\",\"total_fee\":\"1\",\"trade_type\":\"JSAPI\",\"transaction_id\":\"4006192001201604295342826960\"} zyy');
INSERT INTO `go_zyy_log` VALUES ('44', '{\"appid\":\"wxfd1c82ecf58bb9bf\",\"bank_type\":\"CFT\",\"cash_fee\":\"1\",\"fee_type\":\"CNY\",\"is_subscribe\":\"N\",\"mch_id\":\"1323555901\",\"nonce_str\":\"26hreviktlj1p05a766j8yfjq3sezas6\",\"openid\":\"ootf7slsTuQNRwWwMoawPVKQx1nA\",\"out_trade_no\":\"C14619194228800964\",\"result_code\":\"SUCCESS\",\"return_code\":\"SUCCESS\",\"sign\":\"CED567AA792713165FACE6BF78DCD8F5\",\"time_end\":\"20160429164549\",\"total_fee\":\"1\",\"trade_type\":\"JSAPI\",\"transaction_id\":\"4005502001201604295342867724\"} zyy');
INSERT INTO `go_zyy_log` VALUES ('45', '{\"appid\":\"wxfd1c82ecf58bb9bf\",\"bank_type\":\"CFT\",\"cash_fee\":\"1\",\"fee_type\":\"CNY\",\"is_subscribe\":\"N\",\"mch_id\":\"1323555901\",\"nonce_str\":\"02kh3ozv9tzcto1l0t2y5d16980uqe8u\",\"openid\":\"ootf7slsTuQNRwWwMoawPVKQx1nA\",\"out_trade_no\":\"C14619196398830035\",\"result_code\":\"SUCCESS\",\"return_code\":\"SUCCESS\",\"sign\":\"BE51983B036313F2B04612725D0C85C5\",\"time_end\":\"20160429164742\",\"total_fee\":\"1\",\"trade_type\":\"JSAPI\",\"transaction_id\":\"4005502001201604295341597674\"} zyy');
INSERT INTO `go_zyy_log` VALUES ('46', '{\"appid\":\"wxfd1c82ecf58bb9bf\",\"bank_type\":\"CFT\",\"cash_fee\":\"1\",\"fee_type\":\"CNY\",\"is_subscribe\":\"N\",\"mch_id\":\"1323555901\",\"nonce_str\":\"jcaw899cyrf0lte4yv88ukt3n9v6bimp\",\"openid\":\"ootf7slsTuQNRwWwMoawPVKQx1nA\",\"out_trade_no\":\"C14619199972960133\",\"result_code\":\"SUCCESS\",\"return_code\":\"SUCCESS\",\"sign\":\"E4779A98006138B5BAC70F595CD763F0\",\"time_end\":\"20160429165325\",\"total_fee\":\"1\",\"trade_type\":\"JSAPI\",\"transaction_id\":\"4005502001201604295341686080\"} zyy');
INSERT INTO `go_zyy_log` VALUES ('47', '{\"appid\":\"wxfd1c82ecf58bb9bf\",\"bank_type\":\"CFT\",\"cash_fee\":\"1\",\"fee_type\":\"CNY\",\"is_subscribe\":\"Y\",\"mch_id\":\"1323555901\",\"nonce_str\":\"1e9qwuvs6skfs4xqprmhkxtyvsq4r9lb\",\"openid\":\"ootf7slcvo-OZ2d7skGTs_I5KnQs\",\"out_trade_no\":\"C14619201249703395\",\"result_code\":\"SUCCESS\",\"return_code\":\"SUCCESS\",\"sign\":\"FA8E315FB904CF32EAE0FB021BDABF02\",\"time_end\":\"20160429165532\",\"total_fee\":\"1\",\"trade_type\":\"JSAPI\",\"transaction_id\":\"4003722001201604295343048997\"} zyy');
INSERT INTO `go_zyy_log` VALUES ('48', '{\"appid\":\"wxfd1c82ecf58bb9bf\",\"bank_type\":\"CFT\",\"cash_fee\":\"1\",\"fee_type\":\"CNY\",\"is_subscribe\":\"Y\",\"mch_id\":\"1323555901\",\"nonce_str\":\"d0i1dwr41kzqwh1p96iensvo6lsaku6v\",\"openid\":\"ootf7slcvo-OZ2d7skGTs_I5KnQs\",\"out_trade_no\":\"C14619201774557226\",\"result_code\":\"SUCCESS\",\"return_code\":\"SUCCESS\",\"sign\":\"7E565632F1096DB3D196530DC4E0C94F\",\"time_end\":\"20160429165622\",\"total_fee\":\"1\",\"trade_type\":\"JSAPI\",\"transaction_id\":\"4003722001201604295341728759\"} zyy');
INSERT INTO `go_zyy_log` VALUES ('49', '{\"appid\":\"wxfd1c82ecf58bb9bf\",\"bank_type\":\"CFT\",\"cash_fee\":\"1\",\"fee_type\":\"CNY\",\"is_subscribe\":\"Y\",\"mch_id\":\"1323555901\",\"nonce_str\":\"lllrzwd40noy3g105q80rnlk28dxa5x9\",\"openid\":\"ootf7slcvo-OZ2d7skGTs_I5KnQs\",\"out_trade_no\":\"C14619202848014316\",\"result_code\":\"SUCCESS\",\"return_code\":\"SUCCESS\",\"sign\":\"0CFFF0EA89CE1CD6C2B6241B0B68E12A\",\"time_end\":\"20160429165810\",\"total_fee\":\"1\",\"trade_type\":\"JSAPI\",\"transaction_id\":\"4003722001201604295343098669\"} zyy');
INSERT INTO `go_zyy_log` VALUES ('50', '{\"appid\":\"wxfd1c82ecf58bb9bf\",\"bank_type\":\"CFT\",\"cash_fee\":\"1\",\"fee_type\":\"CNY\",\"is_subscribe\":\"N\",\"mch_id\":\"1323555901\",\"nonce_str\":\"2l960jer5335rybpezohpwavwdj76ki0\",\"openid\":\"ootf7slsTuQNRwWwMoawPVKQx1nA\",\"out_trade_no\":\"C14619211041294542\",\"result_code\":\"SUCCESS\",\"return_code\":\"SUCCESS\",\"sign\":\"89C4FDC853A2ADC19D3900A223350485\",\"time_end\":\"20160429171152\",\"total_fee\":\"1\",\"trade_type\":\"JSAPI\",\"transaction_id\":\"4005502001201604295343321488\"} zyy');
INSERT INTO `go_zyy_log` VALUES ('51', '{\"appid\":\"wxfd1c82ecf58bb9bf\",\"bank_type\":\"CFT\",\"cash_fee\":\"1\",\"fee_type\":\"CNY\",\"is_subscribe\":\"N\",\"mch_id\":\"1323555901\",\"nonce_str\":\"k4h45zscm9rt140xjiwafhctykjdm2pa\",\"openid\":\"ootf7slsTuQNRwWwMoawPVKQx1nA\",\"out_trade_no\":\"C14619213354462969\",\"result_code\":\"SUCCESS\",\"return_code\":\"SUCCESS\",\"sign\":\"FF7ABF0F3B5C1E5EAA40A70C1401E409\",\"time_end\":\"20160429171543\",\"total_fee\":\"1\",\"trade_type\":\"JSAPI\",\"transaction_id\":\"4005502001201604295343383277\"} zyy');
INSERT INTO `go_zyy_log` VALUES ('52', '{\"appid\":\"wxfd1c82ecf58bb9bf\",\"bank_type\":\"CFT\",\"cash_fee\":\"100\",\"fee_type\":\"CNY\",\"is_subscribe\":\"Y\",\"mch_id\":\"1323555901\",\"nonce_str\":\"ehakjiqxs2a3mrrx91g4uoya67hjveiu\",\"openid\":\"ootf7slcvo-OZ2d7skGTs_I5KnQs\",\"out_trade_no\":\"C14619215564348112\",\"result_code\":\"SUCCESS\",\"return_code\":\"SUCCESS\",\"sign\":\"4DFE661414C7DB0C059D00A07222DB82\",\"time_end\":\"20160429171921\",\"total_fee\":\"100\",\"trade_type\":\"JSAPI\",\"transaction_id\":\"4003722001201604295342107007\"} zyy');
INSERT INTO `go_zyy_log` VALUES ('53', '{\"appid\":\"wxfd1c82ecf58bb9bf\",\"bank_type\":\"CFT\",\"cash_fee\":\"100\",\"fee_type\":\"CNY\",\"is_subscribe\":\"Y\",\"mch_id\":\"1323555901\",\"nonce_str\":\"caetzl199jrjexq4urz0yx2mgi913sia\",\"openid\":\"ootf7sj46ZLLVX0owlOi0mgIX2GA\",\"out_trade_no\":\"C14619467272099710\",\"result_code\":\"SUCCESS\",\"return_code\":\"SUCCESS\",\"sign\":\"C80BA20C36E0A5584115B55719DAE0B7\",\"time_end\":\"20160430001905\",\"total_fee\":\"100\",\"trade_type\":\"JSAPI\",\"transaction_id\":\"4001262001201604305356168800\"} zyy');
INSERT INTO `go_zyy_log` VALUES ('54', '{\"appid\":\"wxfd1c82ecf58bb9bf\",\"bank_type\":\"CFT\",\"cash_fee\":\"100\",\"fee_type\":\"CNY\",\"is_subscribe\":\"Y\",\"mch_id\":\"1323555901\",\"nonce_str\":\"kd7xbfpo627hxo55snzvwi0o1nhfq943\",\"openid\":\"ootf7stc49z6SGvh_YJfh4mVKw9w\",\"out_trade_no\":\"C14619468894630868\",\"result_code\":\"SUCCESS\",\"return_code\":\"SUCCESS\",\"sign\":\"53A4E85B1C3527BE9483EA09D2475F46\",\"time_end\":\"20160430002140\",\"total_fee\":\"100\",\"trade_type\":\"JSAPI\",\"transaction_id\":\"4008922001201604305356192830\"} zyy');
INSERT INTO `go_zyy_log` VALUES ('55', '{\"appid\":\"wxfd1c82ecf58bb9bf\",\"bank_type\":\"CFT\",\"cash_fee\":\"1\",\"fee_type\":\"CNY\",\"is_subscribe\":\"N\",\"mch_id\":\"1323555901\",\"nonce_str\":\"7wyz88yg7j17uksjg6z5v9lkmyoby4rx\",\"openid\":\"ootf7slsTuQNRwWwMoawPVKQx1nA\",\"out_trade_no\":\"C14620024793458089\",\"result_code\":\"SUCCESS\",\"return_code\":\"SUCCESS\",\"sign\":\"0180983C644DDE6BA284AFD7EC26F8FC\",\"time_end\":\"20160430154812\",\"total_fee\":\"1\",\"trade_type\":\"JSAPI\",\"transaction_id\":\"4005502001201604305373623307\"} zyy');
INSERT INTO `go_zyy_log` VALUES ('56', '{\"appid\":\"wxfd1c82ecf58bb9bf\",\"bank_type\":\"CFT\",\"cash_fee\":\"100\",\"fee_type\":\"CNY\",\"is_subscribe\":\"Y\",\"mch_id\":\"1323555901\",\"nonce_str\":\"agc5vpa0vtee4096h067qukua4747hiy\",\"openid\":\"ootf7slcvo-OZ2d7skGTs_I5KnQs\",\"out_trade_no\":\"C14620046704496600\",\"result_code\":\"SUCCESS\",\"return_code\":\"SUCCESS\",\"sign\":\"E826AE7D791559D3C14C1B41F6F656C3\",\"time_end\":\"20160430162435\",\"total_fee\":\"100\",\"trade_type\":\"JSAPI\",\"transaction_id\":\"4003722001201604305374172311\"} zyy');
INSERT INTO `go_zyy_log` VALUES ('57', '{\"appid\":\"wxfd1c82ecf58bb9bf\",\"bank_type\":\"CFT\",\"cash_fee\":\"100\",\"fee_type\":\"CNY\",\"is_subscribe\":\"Y\",\"mch_id\":\"1323555901\",\"nonce_str\":\"jjwwxcjr6k737c056eatwol71cx75wnl\",\"openid\":\"ootf7slcvo-OZ2d7skGTs_I5KnQs\",\"out_trade_no\":\"C14620048884693682\",\"result_code\":\"SUCCESS\",\"return_code\":\"SUCCESS\",\"sign\":\"8F6085F91A92A53E4B413BFE9A4E71AC\",\"time_end\":\"20160430162813\",\"total_fee\":\"100\",\"trade_type\":\"JSAPI\",\"transaction_id\":\"4003722001201604305375483066\"} zyy');
INSERT INTO `go_zyy_log` VALUES ('58', '{\"appid\":\"wxfd1c82ecf58bb9bf\",\"bank_type\":\"CFT\",\"cash_fee\":\"1000\",\"fee_type\":\"CNY\",\"is_subscribe\":\"Y\",\"mch_id\":\"1323555901\",\"nonce_str\":\"bhxms02vg86i3d2zx1lydm417pxaf9rj\",\"openid\":\"ootf7siIzayHRuhUzLQwvKqpejW0\",\"out_trade_no\":\"C14620061891339145\",\"result_code\":\"SUCCESS\",\"return_code\":\"SUCCESS\",\"sign\":\"E1AA01C05B7A776A2C2EAB1962425C91\",\"time_end\":\"20160430164955\",\"total_fee\":\"1000\",\"trade_type\":\"JSAPI\",\"transaction_id\":\"4002842001201604305375833019\"} zyy');
INSERT INTO `go_zyy_log` VALUES ('59', '{\"appid\":\"wxfd1c82ecf58bb9bf\",\"bank_type\":\"CMB_DEBIT\",\"cash_fee\":\"100\",\"fee_type\":\"CNY\",\"is_subscribe\":\"Y\",\"mch_id\":\"1323555901\",\"nonce_str\":\"dmr6v20nqneuu8o89gok6hfqwxmnjh79\",\"openid\":\"ootf7smqFa1V2mM4uzOQ_IEkFiJI\",\"out_trade_no\":\"C14620181656043093\",\"result_code\":\"SUCCESS\",\"return_code\":\"SUCCESS\",\"sign\":\"FD3C105CF702C2E48BD3E9501DE96F7A\",\"time_end\":\"20160430200932\",\"total_fee\":\"100\",\"trade_type\":\"JSAPI\",\"transaction_id\":\"4005312001201604305381910377\"} zyy');
INSERT INTO `go_zyy_log` VALUES ('60', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('61', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('62', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('63', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('64', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('65', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('66', '{\"appid\":\"wxfd1c82ecf58bb9bf\",\"bank_type\":\"CMB_DEBIT\",\"cash_fee\":\"100\",\"fee_type\":\"CNY\",\"is_subscribe\":\"Y\",\"mch_id\":\"1323555901\",\"nonce_str\":\"3cof4dnpqft6w5z06c8q4yevnlgkcq9c\",\"openid\":\"ootf7smqFa1V2mM4uzOQ_IEkFiJI\",\"out_trade_no\":\"C14622432971936966\",\"result_code\":\"SUCCESS\",\"return_code\":\"SUCCESS\",\"sign\":\"877DA8AF1D13E7724C37E9EC6677D36D\",\"time_end\":\"20160503104159\",\"total_fee\":\"100\",\"trade_type\":\"NATIVE\",\"transaction_id\":\"4005312001201605035460658734\"} zyy');
INSERT INTO `go_zyy_log` VALUES ('67', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('68', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('69', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('70', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('71', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('72', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('73', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('74', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('75', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('76', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('77', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('78', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('79', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('80', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('81', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('82', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('83', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('84', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('85', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('86', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('87', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('88', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('89', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('90', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('91', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('92', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('93', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('94', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('95', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('96', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('97', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('98', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('99', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('100', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('101', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('102', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('103', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('104', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('105', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('106', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('107', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('108', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('109', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('110', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('111', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('112', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('113', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('114', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('115', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('116', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('117', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('118', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('119', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('120', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('121', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('122', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('123', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('124', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('125', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('126', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('127', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('128', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('129', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('130', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('131', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('132', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('133', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('134', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('135', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('136', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('137', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('138', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('139', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('140', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('141', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('142', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('143', '{\"appid\":\"wxfd1c82ecf58bb9bf\",\"bank_type\":\"CFT\",\"cash_fee\":\"1\",\"fee_type\":\"CNY\",\"is_subscribe\":\"N\",\"mch_id\":\"1323555901\",\"nonce_str\":\"8uqpvdpzz9w3wpaa1hf3zdftzp5bjo7u\",\"openid\":\"ootf7soJxPFGGQqdxx17d-d4jk6g\",\"out_trade_no\":\"C14622582316109150\",\"result_code\":\"SUCCESS\",\"return_code\":\"SUCCESS\",\"sign\":\"E885FE7E2D64A0E3B8CF5448F7D8DCD3\",\"time_end\":\"20160503145200\",\"total_fee\":\"1\",\"trade_type\":\"NATIVE\",\"transaction_id\":\"4009902001201605035468905316\"} zyy');
INSERT INTO `go_zyy_log` VALUES ('144', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('145', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('146', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('147', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('148', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('149', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('150', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('151', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('152', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('153', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('154', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('155', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('156', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('157', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('158', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('159', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('160', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('161', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('162', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('163', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('164', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('165', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('166', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('167', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('168', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('169', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('170', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('171', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('172', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('173', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('174', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('175', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('176', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('177', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('178', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('179', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('180', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('181', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('182', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('183', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('184', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('185', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('186', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('187', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('188', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('189', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('190', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('191', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('192', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('193', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('194', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('195', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('196', '{\"appid\":\"wxfd1c82ecf58bb9bf\",\"bank_type\":\"CFT\",\"cash_fee\":\"1\",\"fee_type\":\"CNY\",\"is_subscribe\":\"N\",\"mch_id\":\"1323555901\",\"nonce_str\":\"ti0wxpodeobxadpvnzhu2h492jptstrm\",\"openid\":\"ootf7soJxPFGGQqdxx17d-d4jk6g\",\"out_trade_no\":\"C14622583740510470\",\"result_code\":\"SUCCESS\",\"return_code\":\"SUCCESS\",\"sign\":\"B7335E0D8EE8F3F6284E6ECD548CBC8F\",\"time_end\":\"20160503145347\",\"total_fee\":\"1\",\"trade_type\":\"NATIVE\",\"transaction_id\":\"4009902001201605035468249206\"} zyy');
INSERT INTO `go_zyy_log` VALUES ('197', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('198', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('199', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('200', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('201', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('202', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('203', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('204', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('205', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('206', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('207', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('208', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('209', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('210', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('211', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('212', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('213', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('214', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('215', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('216', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('217', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('218', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('219', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('220', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('221', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('222', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('223', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('224', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('225', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('226', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('227', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('228', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('229', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('230', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('231', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('232', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('233', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('234', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('235', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('236', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('237', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('238', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('239', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('240', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('241', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('242', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('243', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('244', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('245', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('246', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('247', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('248', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('249', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('250', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('251', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('252', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('253', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('254', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('255', '{\"appid\":\"wxfd1c82ecf58bb9bf\",\"bank_type\":\"CFT\",\"cash_fee\":\"1\",\"fee_type\":\"CNY\",\"is_subscribe\":\"N\",\"mch_id\":\"1323555901\",\"nonce_str\":\"t0rkl9icj6i98or4tj2lsg6gg4nke8z3\",\"openid\":\"ootf7soJxPFGGQqdxx17d-d4jk6g\",\"out_trade_no\":\"C14622585423667792\",\"result_code\":\"SUCCESS\",\"return_code\":\"SUCCESS\",\"sign\":\"A6F7D35731975B1F88DF1B68AA665636\",\"time_end\":\"20160503145612\",\"total_fee\":\"1\",\"trade_type\":\"NATIVE\",\"transaction_id\":\"4009902001201605035468966169\"} zyy');
INSERT INTO `go_zyy_log` VALUES ('256', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('257', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('258', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('259', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('260', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('261', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('262', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('263', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('264', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('265', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('266', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('267', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('268', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('269', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('270', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('271', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('272', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('273', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('274', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('275', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('276', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('277', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('278', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('279', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('280', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('281', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('282', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('283', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('284', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('285', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('286', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('287', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('288', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('289', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('290', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('291', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('292', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('293', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('294', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('295', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('296', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('297', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('298', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('299', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('300', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('301', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('302', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('303', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('304', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('305', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('306', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('307', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('308', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('309', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('310', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('311', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('312', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('313', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('314', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('315', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('316', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('317', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('318', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('319', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('320', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('321', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('322', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('323', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('324', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('325', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('326', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('327', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('328', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('329', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('330', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('331', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('332', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('333', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('334', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('335', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('336', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('337', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('338', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('339', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('340', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('341', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('342', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('343', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('344', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('345', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('346', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('347', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('348', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('349', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('350', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('351', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('352', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('353', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('354', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('355', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('356', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('357', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('358', '{\"appid\":\"wxfd1c82ecf58bb9bf\",\"bank_type\":\"CFT\",\"cash_fee\":\"100\",\"fee_type\":\"CNY\",\"is_subscribe\":\"Y\",\"mch_id\":\"1323555901\",\"nonce_str\":\"98kvmwyfqtr82zqm91g2hpjsdux69r8p\",\"openid\":\"ootf7slcvo-OZ2d7skGTs_I5KnQs\",\"out_trade_no\":\"C14622692509556838\",\"result_code\":\"SUCCESS\",\"return_code\":\"SUCCESS\",\"sign\":\"2F0210690D570502EE68FA1ACF2919B3\",\"time_end\":\"20160503175417\",\"total_fee\":\"100\",\"trade_type\":\"JSAPI\",\"transaction_id\":\"4003722001201605035474952076\"} zyy');
INSERT INTO `go_zyy_log` VALUES ('359', '{\"appid\":\"wxfd1c82ecf58bb9bf\",\"bank_type\":\"CFT\",\"cash_fee\":\"100\",\"fee_type\":\"CNY\",\"is_subscribe\":\"Y\",\"mch_id\":\"1323555901\",\"nonce_str\":\"98kvmwyfqtr82zqm91g2hpjsdux69r8p\",\"openid\":\"ootf7slcvo-OZ2d7skGTs_I5KnQs\",\"out_trade_no\":\"C14622692509556838\",\"result_code\":\"SUCCESS\",\"return_code\":\"SUCCESS\",\"sign\":\"2F0210690D570502EE68FA1ACF2919B3\",\"time_end\":\"20160503175417\",\"total_fee\":\"100\",\"trade_type\":\"JSAPI\",\"transaction_id\":\"4003722001201605035474952076\"} zyy');
INSERT INTO `go_zyy_log` VALUES ('360', '{\"appid\":\"wxfd1c82ecf58bb9bf\",\"bank_type\":\"CFT\",\"cash_fee\":\"100\",\"fee_type\":\"CNY\",\"is_subscribe\":\"Y\",\"mch_id\":\"1323555901\",\"nonce_str\":\"98kvmwyfqtr82zqm91g2hpjsdux69r8p\",\"openid\":\"ootf7slcvo-OZ2d7skGTs_I5KnQs\",\"out_trade_no\":\"C14622692509556838\",\"result_code\":\"SUCCESS\",\"return_code\":\"SUCCESS\",\"sign\":\"2F0210690D570502EE68FA1ACF2919B3\",\"time_end\":\"20160503175417\",\"total_fee\":\"100\",\"trade_type\":\"JSAPI\",\"transaction_id\":\"4003722001201605035474952076\"} zyy');
INSERT INTO `go_zyy_log` VALUES ('361', '{\"appid\":\"wxfd1c82ecf58bb9bf\",\"bank_type\":\"CFT\",\"cash_fee\":\"100\",\"fee_type\":\"CNY\",\"is_subscribe\":\"Y\",\"mch_id\":\"1323555901\",\"nonce_str\":\"98kvmwyfqtr82zqm91g2hpjsdux69r8p\",\"openid\":\"ootf7slcvo-OZ2d7skGTs_I5KnQs\",\"out_trade_no\":\"C14622692509556838\",\"result_code\":\"SUCCESS\",\"return_code\":\"SUCCESS\",\"sign\":\"2F0210690D570502EE68FA1ACF2919B3\",\"time_end\":\"20160503175417\",\"total_fee\":\"100\",\"trade_type\":\"JSAPI\",\"transaction_id\":\"4003722001201605035474952076\"} zyy');
INSERT INTO `go_zyy_log` VALUES ('362', '{\"appid\":\"wxfd1c82ecf58bb9bf\",\"bank_type\":\"CFT\",\"cash_fee\":\"100\",\"fee_type\":\"CNY\",\"is_subscribe\":\"Y\",\"mch_id\":\"1323555901\",\"nonce_str\":\"jj25bsswcsalvmsf34eoag9fxzb59hqm\",\"openid\":\"ootf7slcvo-OZ2d7skGTs_I5KnQs\",\"out_trade_no\":\"C14622693308790934\",\"result_code\":\"SUCCESS\",\"return_code\":\"SUCCESS\",\"sign\":\"259F9AE166D8E725AA80BE7479163209\",\"time_end\":\"20160503175535\",\"total_fee\":\"100\",\"trade_type\":\"JSAPI\",\"transaction_id\":\"4003722001201605035474974344\"} zyy');
INSERT INTO `go_zyy_log` VALUES ('363', '{\"appid\":\"wxfd1c82ecf58bb9bf\",\"bank_type\":\"CFT\",\"cash_fee\":\"100\",\"fee_type\":\"CNY\",\"is_subscribe\":\"Y\",\"mch_id\":\"1323555901\",\"nonce_str\":\"jj25bsswcsalvmsf34eoag9fxzb59hqm\",\"openid\":\"ootf7slcvo-OZ2d7skGTs_I5KnQs\",\"out_trade_no\":\"C14622693308790934\",\"result_code\":\"SUCCESS\",\"return_code\":\"SUCCESS\",\"sign\":\"259F9AE166D8E725AA80BE7479163209\",\"time_end\":\"20160503175535\",\"total_fee\":\"100\",\"trade_type\":\"JSAPI\",\"transaction_id\":\"4003722001201605035474974344\"} zyy');
INSERT INTO `go_zyy_log` VALUES ('364', '{\"appid\":\"wxfd1c82ecf58bb9bf\",\"bank_type\":\"CFT\",\"cash_fee\":\"100\",\"fee_type\":\"CNY\",\"is_subscribe\":\"Y\",\"mch_id\":\"1323555901\",\"nonce_str\":\"jj25bsswcsalvmsf34eoag9fxzb59hqm\",\"openid\":\"ootf7slcvo-OZ2d7skGTs_I5KnQs\",\"out_trade_no\":\"C14622693308790934\",\"result_code\":\"SUCCESS\",\"return_code\":\"SUCCESS\",\"sign\":\"259F9AE166D8E725AA80BE7479163209\",\"time_end\":\"20160503175535\",\"total_fee\":\"100\",\"trade_type\":\"JSAPI\",\"transaction_id\":\"4003722001201605035474974344\"} zyy');
INSERT INTO `go_zyy_log` VALUES ('365', '{\"appid\":\"wxfd1c82ecf58bb9bf\",\"bank_type\":\"CFT\",\"cash_fee\":\"100\",\"fee_type\":\"CNY\",\"is_subscribe\":\"Y\",\"mch_id\":\"1323555901\",\"nonce_str\":\"jj25bsswcsalvmsf34eoag9fxzb59hqm\",\"openid\":\"ootf7slcvo-OZ2d7skGTs_I5KnQs\",\"out_trade_no\":\"C14622693308790934\",\"result_code\":\"SUCCESS\",\"return_code\":\"SUCCESS\",\"sign\":\"259F9AE166D8E725AA80BE7479163209\",\"time_end\":\"20160503175535\",\"total_fee\":\"100\",\"trade_type\":\"JSAPI\",\"transaction_id\":\"4003722001201605035474974344\"} zyy');
INSERT INTO `go_zyy_log` VALUES ('366', '{\"appid\":\"wxfd1c82ecf58bb9bf\",\"bank_type\":\"CFT\",\"cash_fee\":\"100\",\"fee_type\":\"CNY\",\"is_subscribe\":\"Y\",\"mch_id\":\"1323555901\",\"nonce_str\":\"98kvmwyfqtr82zqm91g2hpjsdux69r8p\",\"openid\":\"ootf7slcvo-OZ2d7skGTs_I5KnQs\",\"out_trade_no\":\"C14622692509556838\",\"result_code\":\"SUCCESS\",\"return_code\":\"SUCCESS\",\"sign\":\"2F0210690D570502EE68FA1ACF2919B3\",\"time_end\":\"20160503175417\",\"total_fee\":\"100\",\"trade_type\":\"JSAPI\",\"transaction_id\":\"4003722001201605035474952076\"} zyy');
INSERT INTO `go_zyy_log` VALUES ('367', '{\"appid\":\"wxfd1c82ecf58bb9bf\",\"bank_type\":\"CFT\",\"cash_fee\":\"100\",\"fee_type\":\"CNY\",\"is_subscribe\":\"Y\",\"mch_id\":\"1323555901\",\"nonce_str\":\"jj25bsswcsalvmsf34eoag9fxzb59hqm\",\"openid\":\"ootf7slcvo-OZ2d7skGTs_I5KnQs\",\"out_trade_no\":\"C14622693308790934\",\"result_code\":\"SUCCESS\",\"return_code\":\"SUCCESS\",\"sign\":\"259F9AE166D8E725AA80BE7479163209\",\"time_end\":\"20160503175535\",\"total_fee\":\"100\",\"trade_type\":\"JSAPI\",\"transaction_id\":\"4003722001201605035474974344\"} zyy');
INSERT INTO `go_zyy_log` VALUES ('368', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('369', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('370', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('371', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('372', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('373', '{\"appid\":\"wxfd1c82ecf58bb9bf\",\"bank_type\":\"CFT\",\"cash_fee\":\"100\",\"fee_type\":\"CNY\",\"is_subscribe\":\"Y\",\"mch_id\":\"1323555901\",\"nonce_str\":\"mj70n20d5y5wgf13o30bqxwdikjvontd\",\"openid\":\"ootf7sqCf6laucAfD8orWthqpZZ8\",\"out_trade_no\":\"C14622705515878030\",\"result_code\":\"SUCCESS\",\"return_code\":\"SUCCESS\",\"sign\":\"0E8156DF98C98A156064B5F6E76E32A8\",\"time_end\":\"20160503181607\",\"total_fee\":\"100\",\"trade_type\":\"NATIVE\",\"transaction_id\":\"4008992001201605035474036900\"} zyy');
INSERT INTO `go_zyy_log` VALUES ('374', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('375', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('376', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('377', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('378', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('379', '{\"appid\":\"wxfd1c82ecf58bb9bf\",\"bank_type\":\"CFT\",\"cash_fee\":\"100\",\"fee_type\":\"CNY\",\"is_subscribe\":\"Y\",\"mch_id\":\"1323555901\",\"nonce_str\":\"mj70n20d5y5wgf13o30bqxwdikjvontd\",\"openid\":\"ootf7sqCf6laucAfD8orWthqpZZ8\",\"out_trade_no\":\"C14622705515878030\",\"result_code\":\"SUCCESS\",\"return_code\":\"SUCCESS\",\"sign\":\"0E8156DF98C98A156064B5F6E76E32A8\",\"time_end\":\"20160503181607\",\"total_fee\":\"100\",\"trade_type\":\"NATIVE\",\"transaction_id\":\"4008992001201605035474036900\"} zyy');
INSERT INTO `go_zyy_log` VALUES ('380', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('381', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('382', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('383', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('384', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('385', '{\"appid\":\"wxfd1c82ecf58bb9bf\",\"bank_type\":\"CFT\",\"cash_fee\":\"100\",\"fee_type\":\"CNY\",\"is_subscribe\":\"Y\",\"mch_id\":\"1323555901\",\"nonce_str\":\"mj70n20d5y5wgf13o30bqxwdikjvontd\",\"openid\":\"ootf7sqCf6laucAfD8orWthqpZZ8\",\"out_trade_no\":\"C14622705515878030\",\"result_code\":\"SUCCESS\",\"return_code\":\"SUCCESS\",\"sign\":\"0E8156DF98C98A156064B5F6E76E32A8\",\"time_end\":\"20160503181607\",\"total_fee\":\"100\",\"trade_type\":\"NATIVE\",\"transaction_id\":\"4008992001201605035474036900\"} zyy');
INSERT INTO `go_zyy_log` VALUES ('386', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('387', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('388', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('389', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('390', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('391', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('392', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('393', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('394', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('395', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('396', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('397', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('398', '{\"appid\":\"wxfd1c82ecf58bb9bf\",\"bank_type\":\"CFT\",\"cash_fee\":\"100\",\"fee_type\":\"CNY\",\"is_subscribe\":\"Y\",\"mch_id\":\"1323555901\",\"nonce_str\":\"mj70n20d5y5wgf13o30bqxwdikjvontd\",\"openid\":\"ootf7sqCf6laucAfD8orWthqpZZ8\",\"out_trade_no\":\"C14622705515878030\",\"result_code\":\"SUCCESS\",\"return_code\":\"SUCCESS\",\"sign\":\"0E8156DF98C98A156064B5F6E76E32A8\",\"time_end\":\"20160503181607\",\"total_fee\":\"100\",\"trade_type\":\"NATIVE\",\"transaction_id\":\"4008992001201605035474036900\"} zyy');
INSERT INTO `go_zyy_log` VALUES ('399', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('400', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('401', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('402', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('403', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('404', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('405', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('406', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('407', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('408', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('409', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('410', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('411', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('412', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('413', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('414', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('415', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('416', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('417', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('418', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('419', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('420', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('421', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('422', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('423', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('424', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('425', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('426', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('427', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('428', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('429', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('430', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('431', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('432', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('433', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('434', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('435', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('436', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('437', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('438', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('439', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('440', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('441', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('442', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('443', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('444', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('445', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('446', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('447', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('448', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('449', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('450', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('451', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('452', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('453', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('454', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('455', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('456', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('457', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('458', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('459', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('460', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('461', '{\"appid\":\"wxfd1c82ecf58bb9bf\",\"bank_type\":\"CFT\",\"cash_fee\":\"100\",\"fee_type\":\"CNY\",\"is_subscribe\":\"Y\",\"mch_id\":\"1323555901\",\"nonce_str\":\"mj70n20d5y5wgf13o30bqxwdikjvontd\",\"openid\":\"ootf7sqCf6laucAfD8orWthqpZZ8\",\"out_trade_no\":\"C14622705515878030\",\"result_code\":\"SUCCESS\",\"return_code\":\"SUCCESS\",\"sign\":\"0E8156DF98C98A156064B5F6E76E32A8\",\"time_end\":\"20160503181607\",\"total_fee\":\"100\",\"trade_type\":\"NATIVE\",\"transaction_id\":\"4008992001201605035474036900\"} zyy');
INSERT INTO `go_zyy_log` VALUES ('462', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('463', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('464', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('465', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('466', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('467', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('468', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('469', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('470', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('471', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('472', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('473', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('474', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('475', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('476', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('477', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('478', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('479', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('480', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('481', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('482', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('483', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('484', '{\"appid\":\"wxfd1c82ecf58bb9bf\",\"bank_type\":\"CFT\",\"cash_fee\":\"100\",\"fee_type\":\"CNY\",\"is_subscribe\":\"Y\",\"mch_id\":\"1323555901\",\"nonce_str\":\"98kvmwyfqtr82zqm91g2hpjsdux69r8p\",\"openid\":\"ootf7slcvo-OZ2d7skGTs_I5KnQs\",\"out_trade_no\":\"C14622692509556838\",\"result_code\":\"SUCCESS\",\"return_code\":\"SUCCESS\",\"sign\":\"2F0210690D570502EE68FA1ACF2919B3\",\"time_end\":\"20160503175417\",\"total_fee\":\"100\",\"trade_type\":\"JSAPI\",\"transaction_id\":\"4003722001201605035474952076\"} zyy');
INSERT INTO `go_zyy_log` VALUES ('485', '{\"appid\":\"wxfd1c82ecf58bb9bf\",\"bank_type\":\"CFT\",\"cash_fee\":\"100\",\"fee_type\":\"CNY\",\"is_subscribe\":\"Y\",\"mch_id\":\"1323555901\",\"nonce_str\":\"jj25bsswcsalvmsf34eoag9fxzb59hqm\",\"openid\":\"ootf7slcvo-OZ2d7skGTs_I5KnQs\",\"out_trade_no\":\"C14622693308790934\",\"result_code\":\"SUCCESS\",\"return_code\":\"SUCCESS\",\"sign\":\"259F9AE166D8E725AA80BE7479163209\",\"time_end\":\"20160503175535\",\"total_fee\":\"100\",\"trade_type\":\"JSAPI\",\"transaction_id\":\"4003722001201605035474974344\"} zyy');
INSERT INTO `go_zyy_log` VALUES ('486', '{\"appid\":\"wxfd1c82ecf58bb9bf\",\"bank_type\":\"CFT\",\"cash_fee\":\"100\",\"fee_type\":\"CNY\",\"is_subscribe\":\"Y\",\"mch_id\":\"1323555901\",\"nonce_str\":\"mj70n20d5y5wgf13o30bqxwdikjvontd\",\"openid\":\"ootf7sqCf6laucAfD8orWthqpZZ8\",\"out_trade_no\":\"C14622705515878030\",\"result_code\":\"SUCCESS\",\"return_code\":\"SUCCESS\",\"sign\":\"0E8156DF98C98A156064B5F6E76E32A8\",\"time_end\":\"20160503181607\",\"total_fee\":\"100\",\"trade_type\":\"NATIVE\",\"transaction_id\":\"4008992001201605035474036900\"} zyy');
INSERT INTO `go_zyy_log` VALUES ('487', '{\"appid\":\"wxfd1c82ecf58bb9bf\",\"bank_type\":\"CFT\",\"cash_fee\":\"100\",\"fee_type\":\"CNY\",\"is_subscribe\":\"Y\",\"mch_id\":\"1323555901\",\"nonce_str\":\"98kvmwyfqtr82zqm91g2hpjsdux69r8p\",\"openid\":\"ootf7slcvo-OZ2d7skGTs_I5KnQs\",\"out_trade_no\":\"C14622692509556838\",\"result_code\":\"SUCCESS\",\"return_code\":\"SUCCESS\",\"sign\":\"2F0210690D570502EE68FA1ACF2919B3\",\"time_end\":\"20160503175417\",\"total_fee\":\"100\",\"trade_type\":\"JSAPI\",\"transaction_id\":\"4003722001201605035474952076\"} zyy');
INSERT INTO `go_zyy_log` VALUES ('488', '{\"appid\":\"wxfd1c82ecf58bb9bf\",\"bank_type\":\"CFT\",\"cash_fee\":\"100\",\"fee_type\":\"CNY\",\"is_subscribe\":\"Y\",\"mch_id\":\"1323555901\",\"nonce_str\":\"jj25bsswcsalvmsf34eoag9fxzb59hqm\",\"openid\":\"ootf7slcvo-OZ2d7skGTs_I5KnQs\",\"out_trade_no\":\"C14622693308790934\",\"result_code\":\"SUCCESS\",\"return_code\":\"SUCCESS\",\"sign\":\"259F9AE166D8E725AA80BE7479163209\",\"time_end\":\"20160503175535\",\"total_fee\":\"100\",\"trade_type\":\"JSAPI\",\"transaction_id\":\"4003722001201605035474974344\"} zyy');
INSERT INTO `go_zyy_log` VALUES ('489', '{\"appid\":\"wxfd1c82ecf58bb9bf\",\"bank_type\":\"CFT\",\"cash_fee\":\"100\",\"fee_type\":\"CNY\",\"is_subscribe\":\"Y\",\"mch_id\":\"1323555901\",\"nonce_str\":\"mj70n20d5y5wgf13o30bqxwdikjvontd\",\"openid\":\"ootf7sqCf6laucAfD8orWthqpZZ8\",\"out_trade_no\":\"C14622705515878030\",\"result_code\":\"SUCCESS\",\"return_code\":\"SUCCESS\",\"sign\":\"0E8156DF98C98A156064B5F6E76E32A8\",\"time_end\":\"20160503181607\",\"total_fee\":\"100\",\"trade_type\":\"NATIVE\",\"transaction_id\":\"4008992001201605035474036900\"} zyy');
INSERT INTO `go_zyy_log` VALUES ('490', '{\"appid\":\"wxfd1c82ecf58bb9bf\",\"bank_type\":\"CFT\",\"cash_fee\":\"100\",\"fee_type\":\"CNY\",\"is_subscribe\":\"Y\",\"mch_id\":\"1323555901\",\"nonce_str\":\"98kvmwyfqtr82zqm91g2hpjsdux69r8p\",\"openid\":\"ootf7slcvo-OZ2d7skGTs_I5KnQs\",\"out_trade_no\":\"C14622692509556838\",\"result_code\":\"SUCCESS\",\"return_code\":\"SUCCESS\",\"sign\":\"2F0210690D570502EE68FA1ACF2919B3\",\"time_end\":\"20160503175417\",\"total_fee\":\"100\",\"trade_type\":\"JSAPI\",\"transaction_id\":\"4003722001201605035474952076\"} zyy');
INSERT INTO `go_zyy_log` VALUES ('491', '{\"appid\":\"wxfd1c82ecf58bb9bf\",\"bank_type\":\"CFT\",\"cash_fee\":\"100\",\"fee_type\":\"CNY\",\"is_subscribe\":\"Y\",\"mch_id\":\"1323555901\",\"nonce_str\":\"jj25bsswcsalvmsf34eoag9fxzb59hqm\",\"openid\":\"ootf7slcvo-OZ2d7skGTs_I5KnQs\",\"out_trade_no\":\"C14622693308790934\",\"result_code\":\"SUCCESS\",\"return_code\":\"SUCCESS\",\"sign\":\"259F9AE166D8E725AA80BE7479163209\",\"time_end\":\"20160503175535\",\"total_fee\":\"100\",\"trade_type\":\"JSAPI\",\"transaction_id\":\"4003722001201605035474974344\"} zyy');
INSERT INTO `go_zyy_log` VALUES ('492', '{\"appid\":\"wxfd1c82ecf58bb9bf\",\"bank_type\":\"CFT\",\"cash_fee\":\"100\",\"fee_type\":\"CNY\",\"is_subscribe\":\"Y\",\"mch_id\":\"1323555901\",\"nonce_str\":\"mj70n20d5y5wgf13o30bqxwdikjvontd\",\"openid\":\"ootf7sqCf6laucAfD8orWthqpZZ8\",\"out_trade_no\":\"C14622705515878030\",\"result_code\":\"SUCCESS\",\"return_code\":\"SUCCESS\",\"sign\":\"0E8156DF98C98A156064B5F6E76E32A8\",\"time_end\":\"20160503181607\",\"total_fee\":\"100\",\"trade_type\":\"NATIVE\",\"transaction_id\":\"4008992001201605035474036900\"} zyy');
INSERT INTO `go_zyy_log` VALUES ('493', '{\"appid\":\"wxfd1c82ecf58bb9bf\",\"bank_type\":\"CFT\",\"cash_fee\":\"100\",\"fee_type\":\"CNY\",\"is_subscribe\":\"Y\",\"mch_id\":\"1323555901\",\"nonce_str\":\"98kvmwyfqtr82zqm91g2hpjsdux69r8p\",\"openid\":\"ootf7slcvo-OZ2d7skGTs_I5KnQs\",\"out_trade_no\":\"C14622692509556838\",\"result_code\":\"SUCCESS\",\"return_code\":\"SUCCESS\",\"sign\":\"2F0210690D570502EE68FA1ACF2919B3\",\"time_end\":\"20160503175417\",\"total_fee\":\"100\",\"trade_type\":\"JSAPI\",\"transaction_id\":\"4003722001201605035474952076\"} zyy');
INSERT INTO `go_zyy_log` VALUES ('494', '{\"appid\":\"wxfd1c82ecf58bb9bf\",\"bank_type\":\"CFT\",\"cash_fee\":\"100\",\"fee_type\":\"CNY\",\"is_subscribe\":\"Y\",\"mch_id\":\"1323555901\",\"nonce_str\":\"jj25bsswcsalvmsf34eoag9fxzb59hqm\",\"openid\":\"ootf7slcvo-OZ2d7skGTs_I5KnQs\",\"out_trade_no\":\"C14622693308790934\",\"result_code\":\"SUCCESS\",\"return_code\":\"SUCCESS\",\"sign\":\"259F9AE166D8E725AA80BE7479163209\",\"time_end\":\"20160503175535\",\"total_fee\":\"100\",\"trade_type\":\"JSAPI\",\"transaction_id\":\"4003722001201605035474974344\"} zyy');
INSERT INTO `go_zyy_log` VALUES ('495', '{\"appid\":\"wxfd1c82ecf58bb9bf\",\"bank_type\":\"CFT\",\"cash_fee\":\"100\",\"fee_type\":\"CNY\",\"is_subscribe\":\"Y\",\"mch_id\":\"1323555901\",\"nonce_str\":\"mj70n20d5y5wgf13o30bqxwdikjvontd\",\"openid\":\"ootf7sqCf6laucAfD8orWthqpZZ8\",\"out_trade_no\":\"C14622705515878030\",\"result_code\":\"SUCCESS\",\"return_code\":\"SUCCESS\",\"sign\":\"0E8156DF98C98A156064B5F6E76E32A8\",\"time_end\":\"20160503181607\",\"total_fee\":\"100\",\"trade_type\":\"NATIVE\",\"transaction_id\":\"4008992001201605035474036900\"} zyy');
INSERT INTO `go_zyy_log` VALUES ('496', '{\"appid\":\"wxfd1c82ecf58bb9bf\",\"bank_type\":\"CFT\",\"cash_fee\":\"100\",\"fee_type\":\"CNY\",\"is_subscribe\":\"Y\",\"mch_id\":\"1323555901\",\"nonce_str\":\"98kvmwyfqtr82zqm91g2hpjsdux69r8p\",\"openid\":\"ootf7slcvo-OZ2d7skGTs_I5KnQs\",\"out_trade_no\":\"C14622692509556838\",\"result_code\":\"SUCCESS\",\"return_code\":\"SUCCESS\",\"sign\":\"2F0210690D570502EE68FA1ACF2919B3\",\"time_end\":\"20160503175417\",\"total_fee\":\"100\",\"trade_type\":\"JSAPI\",\"transaction_id\":\"4003722001201605035474952076\"} zyy');
INSERT INTO `go_zyy_log` VALUES ('497', '{\"appid\":\"wxfd1c82ecf58bb9bf\",\"bank_type\":\"CFT\",\"cash_fee\":\"100\",\"fee_type\":\"CNY\",\"is_subscribe\":\"Y\",\"mch_id\":\"1323555901\",\"nonce_str\":\"jj25bsswcsalvmsf34eoag9fxzb59hqm\",\"openid\":\"ootf7slcvo-OZ2d7skGTs_I5KnQs\",\"out_trade_no\":\"C14622693308790934\",\"result_code\":\"SUCCESS\",\"return_code\":\"SUCCESS\",\"sign\":\"259F9AE166D8E725AA80BE7479163209\",\"time_end\":\"20160503175535\",\"total_fee\":\"100\",\"trade_type\":\"JSAPI\",\"transaction_id\":\"4003722001201605035474974344\"} zyy');
INSERT INTO `go_zyy_log` VALUES ('498', '{\"appid\":\"wxfd1c82ecf58bb9bf\",\"bank_type\":\"CMB_DEBIT\",\"cash_fee\":\"100\",\"fee_type\":\"CNY\",\"is_subscribe\":\"Y\",\"mch_id\":\"1323555901\",\"nonce_str\":\"00vrq01wrh3w2salylca65onosb7rsft\",\"openid\":\"ootf7smqFa1V2mM4uzOQ_IEkFiJI\",\"out_trade_no\":\"C14622812331449387\",\"result_code\":\"SUCCESS\",\"return_code\":\"SUCCESS\",\"sign\":\"E973F892E6CE505E8E64322CB379397D\",\"time_end\":\"20160503211359\",\"total_fee\":\"100\",\"trade_type\":\"JSAPI\",\"transaction_id\":\"4005312001201605035482347024\"} zyy');
INSERT INTO `go_zyy_log` VALUES ('499', '{\"appid\":\"wxfd1c82ecf58bb9bf\",\"bank_type\":\"CMB_DEBIT\",\"cash_fee\":\"100\",\"fee_type\":\"CNY\",\"is_subscribe\":\"Y\",\"mch_id\":\"1323555901\",\"nonce_str\":\"00vrq01wrh3w2salylca65onosb7rsft\",\"openid\":\"ootf7smqFa1V2mM4uzOQ_IEkFiJI\",\"out_trade_no\":\"C14622812331449387\",\"result_code\":\"SUCCESS\",\"return_code\":\"SUCCESS\",\"sign\":\"E973F892E6CE505E8E64322CB379397D\",\"time_end\":\"20160503211359\",\"total_fee\":\"100\",\"trade_type\":\"JSAPI\",\"transaction_id\":\"4005312001201605035482347024\"} zyy');
INSERT INTO `go_zyy_log` VALUES ('500', '{\"appid\":\"wxfd1c82ecf58bb9bf\",\"bank_type\":\"CMB_DEBIT\",\"cash_fee\":\"100\",\"fee_type\":\"CNY\",\"is_subscribe\":\"Y\",\"mch_id\":\"1323555901\",\"nonce_str\":\"00vrq01wrh3w2salylca65onosb7rsft\",\"openid\":\"ootf7smqFa1V2mM4uzOQ_IEkFiJI\",\"out_trade_no\":\"C14622812331449387\",\"result_code\":\"SUCCESS\",\"return_code\":\"SUCCESS\",\"sign\":\"E973F892E6CE505E8E64322CB379397D\",\"time_end\":\"20160503211359\",\"total_fee\":\"100\",\"trade_type\":\"JSAPI\",\"transaction_id\":\"4005312001201605035482347024\"} zyy');
INSERT INTO `go_zyy_log` VALUES ('501', '{\"appid\":\"wxfd1c82ecf58bb9bf\",\"bank_type\":\"CMB_DEBIT\",\"cash_fee\":\"100\",\"fee_type\":\"CNY\",\"is_subscribe\":\"Y\",\"mch_id\":\"1323555901\",\"nonce_str\":\"00vrq01wrh3w2salylca65onosb7rsft\",\"openid\":\"ootf7smqFa1V2mM4uzOQ_IEkFiJI\",\"out_trade_no\":\"C14622812331449387\",\"result_code\":\"SUCCESS\",\"return_code\":\"SUCCESS\",\"sign\":\"E973F892E6CE505E8E64322CB379397D\",\"time_end\":\"20160503211359\",\"total_fee\":\"100\",\"trade_type\":\"JSAPI\",\"transaction_id\":\"4005312001201605035482347024\"} zyy');
INSERT INTO `go_zyy_log` VALUES ('502', '{\"appid\":\"wxfd1c82ecf58bb9bf\",\"bank_type\":\"CMB_DEBIT\",\"cash_fee\":\"100\",\"fee_type\":\"CNY\",\"is_subscribe\":\"Y\",\"mch_id\":\"1323555901\",\"nonce_str\":\"00vrq01wrh3w2salylca65onosb7rsft\",\"openid\":\"ootf7smqFa1V2mM4uzOQ_IEkFiJI\",\"out_trade_no\":\"C14622812331449387\",\"result_code\":\"SUCCESS\",\"return_code\":\"SUCCESS\",\"sign\":\"E973F892E6CE505E8E64322CB379397D\",\"time_end\":\"20160503211359\",\"total_fee\":\"100\",\"trade_type\":\"JSAPI\",\"transaction_id\":\"4005312001201605035482347024\"} zyy');
INSERT INTO `go_zyy_log` VALUES ('503', '{\"appid\":\"wxfd1c82ecf58bb9bf\",\"bank_type\":\"CFT\",\"cash_fee\":\"100\",\"fee_type\":\"CNY\",\"is_subscribe\":\"Y\",\"mch_id\":\"1323555901\",\"nonce_str\":\"mj70n20d5y5wgf13o30bqxwdikjvontd\",\"openid\":\"ootf7sqCf6laucAfD8orWthqpZZ8\",\"out_trade_no\":\"C14622705515878030\",\"result_code\":\"SUCCESS\",\"return_code\":\"SUCCESS\",\"sign\":\"0E8156DF98C98A156064B5F6E76E32A8\",\"time_end\":\"20160503181607\",\"total_fee\":\"100\",\"trade_type\":\"NATIVE\",\"transaction_id\":\"4008992001201605035474036900\"} zyy');
INSERT INTO `go_zyy_log` VALUES ('504', '{\"appid\":\"wxfd1c82ecf58bb9bf\",\"bank_type\":\"CMB_DEBIT\",\"cash_fee\":\"100\",\"fee_type\":\"CNY\",\"is_subscribe\":\"Y\",\"mch_id\":\"1323555901\",\"nonce_str\":\"00vrq01wrh3w2salylca65onosb7rsft\",\"openid\":\"ootf7smqFa1V2mM4uzOQ_IEkFiJI\",\"out_trade_no\":\"C14622812331449387\",\"result_code\":\"SUCCESS\",\"return_code\":\"SUCCESS\",\"sign\":\"E973F892E6CE505E8E64322CB379397D\",\"time_end\":\"20160503211359\",\"total_fee\":\"100\",\"trade_type\":\"JSAPI\",\"transaction_id\":\"4005312001201605035482347024\"} zyy');
INSERT INTO `go_zyy_log` VALUES ('505', '{\"appid\":\"wxfd1c82ecf58bb9bf\",\"bank_type\":\"CMB_DEBIT\",\"cash_fee\":\"100\",\"fee_type\":\"CNY\",\"is_subscribe\":\"Y\",\"mch_id\":\"1323555901\",\"nonce_str\":\"00vrq01wrh3w2salylca65onosb7rsft\",\"openid\":\"ootf7smqFa1V2mM4uzOQ_IEkFiJI\",\"out_trade_no\":\"C14622812331449387\",\"result_code\":\"SUCCESS\",\"return_code\":\"SUCCESS\",\"sign\":\"E973F892E6CE505E8E64322CB379397D\",\"time_end\":\"20160503211359\",\"total_fee\":\"100\",\"trade_type\":\"JSAPI\",\"transaction_id\":\"4005312001201605035482347024\"} zyy');
INSERT INTO `go_zyy_log` VALUES ('506', '{\"appid\":\"wxfd1c82ecf58bb9bf\",\"bank_type\":\"CMB_DEBIT\",\"cash_fee\":\"100\",\"fee_type\":\"CNY\",\"is_subscribe\":\"Y\",\"mch_id\":\"1323555901\",\"nonce_str\":\"00vrq01wrh3w2salylca65onosb7rsft\",\"openid\":\"ootf7smqFa1V2mM4uzOQ_IEkFiJI\",\"out_trade_no\":\"C14622812331449387\",\"result_code\":\"SUCCESS\",\"return_code\":\"SUCCESS\",\"sign\":\"E973F892E6CE505E8E64322CB379397D\",\"time_end\":\"20160503211359\",\"total_fee\":\"100\",\"trade_type\":\"JSAPI\",\"transaction_id\":\"4005312001201605035482347024\"} zyy');
INSERT INTO `go_zyy_log` VALUES ('507', '{\"appid\":\"wxfd1c82ecf58bb9bf\",\"bank_type\":\"CMB_DEBIT\",\"cash_fee\":\"100\",\"fee_type\":\"CNY\",\"is_subscribe\":\"Y\",\"mch_id\":\"1323555901\",\"nonce_str\":\"00vrq01wrh3w2salylca65onosb7rsft\",\"openid\":\"ootf7smqFa1V2mM4uzOQ_IEkFiJI\",\"out_trade_no\":\"C14622812331449387\",\"result_code\":\"SUCCESS\",\"return_code\":\"SUCCESS\",\"sign\":\"E973F892E6CE505E8E64322CB379397D\",\"time_end\":\"20160503211359\",\"total_fee\":\"100\",\"trade_type\":\"JSAPI\",\"transaction_id\":\"4005312001201605035482347024\"} zyy');
INSERT INTO `go_zyy_log` VALUES ('508', '{\"appid\":\"wxfd1c82ecf58bb9bf\",\"bank_type\":\"CMB_DEBIT\",\"cash_fee\":\"100\",\"fee_type\":\"CNY\",\"is_subscribe\":\"Y\",\"mch_id\":\"1323555901\",\"nonce_str\":\"00vrq01wrh3w2salylca65onosb7rsft\",\"openid\":\"ootf7smqFa1V2mM4uzOQ_IEkFiJI\",\"out_trade_no\":\"C14622812331449387\",\"result_code\":\"SUCCESS\",\"return_code\":\"SUCCESS\",\"sign\":\"E973F892E6CE505E8E64322CB379397D\",\"time_end\":\"20160503211359\",\"total_fee\":\"100\",\"trade_type\":\"JSAPI\",\"transaction_id\":\"4005312001201605035482347024\"} zyy');
INSERT INTO `go_zyy_log` VALUES ('509', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('510', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('511', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('512', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('513', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('514', 'false zyy');
INSERT INTO `go_zyy_log` VALUES ('515', '{\"appid\":\"wxfd1c82ecf58bb9bf\",\"bank_type\":\"CMB_DEBIT\",\"cash_fee\":\"200\",\"fee_type\":\"CNY\",\"is_subscribe\":\"Y\",\"mch_id\":\"1323555901\",\"nonce_str\":\"8spqssc5qk7gptg6gniy2t9ly3wdbe8a\",\"openid\":\"ootf7sgU8XnuHPLWifBT4ZxDFS_4\",\"out_trade_no\":\"C14623743909146658\",\"result_code\":\"SUCCESS\",\"return_code\":\"SUCCESS\",\"sign\":\"CEB7B2BF4B0D88B221FC3C677AB65196\",\"time_end\":\"20160504230650\",\"total_fee\":\"200\",\"trade_type\":\"JSAPI\",\"transaction_id\":\"4009042001201605045516847697\"} zyy');
INSERT INTO `go_zyy_log` VALUES ('516', '{\"appid\":\"wxfd1c82ecf58bb9bf\",\"bank_type\":\"CMB_DEBIT\",\"cash_fee\":\"100\",\"fee_type\":\"CNY\",\"is_subscribe\":\"Y\",\"mch_id\":\"1323555901\",\"nonce_str\":\"dwfzvsneo9e8zh5fh99ynmp1828syk3d\",\"openid\":\"ootf7smqFa1V2mM4uzOQ_IEkFiJI\",\"out_trade_no\":\"C14623746724980944\",\"result_code\":\"SUCCESS\",\"return_code\":\"SUCCESS\",\"sign\":\"F2560A8BB4D960E3AAA4A8867F31B63B\",\"time_end\":\"20160504231118\",\"total_fee\":\"100\",\"trade_type\":\"JSAPI\",\"transaction_id\":\"4005312001201605045516095605\"} zyy');
