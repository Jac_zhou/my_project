﻿<?php
/* *
 * 类名：JhpaySubmit
 * 功能：聚合富各接口请求提交类
 * 详细：构造聚合富各接口表单HTML文本，获取远程HTTP数据
 * 版本：1.0
 * 日期：2016-03-01
 * 说明：
 * 以下代码只是为了方便商户测试而提供的样例代码，商户可以根据自己网站的需要，按照技术文档编写,并非一定要使用该代码。
 * 该代码仅供学习和研究聚合富接口使用，只是提供一个参考。
 */
 //header('Content-Type: text/html; charset=UTF-8');//如果乱码可以指定编码
 include dirname(__FILE__).DIRECTORY_SEPARATOR."jhpay.class.php";

	config_jhpay();

	function config_jhpay(){
		//以下字段参考接口文档
		$jhpay_version = '1.0';
		$jhpay_merid ='26100535';//商户ID
		$jhpay_mername= '商户测试';//商户名称
		$jhpay_policyid = '000001';
		$jhpay_merorderid = $_POST['merorderid'];//订单号
		$jhpay_paymoney = $_POST['money'];//金额
		$jhpay_productname = $_POST['productName'];//商品名称，尽量不要用云购，1元云购等
		$jhpay_productdesc = '聚合富商品';//商品描述
		$jhpay_userid = '10';
		$jhpay_username = '张三';
		$jhpay_email = 'zs@jhf.com';
		$jhpay_phone = '15800000001';
		$jhpay_extra = '自定义参数无不用填写';//添加自定义内容
		$jhpay_custom = 'zhou';
		$jhpay_redirect = '2';
		$jhpay_redirecturl = $_POST['redirecturl'];//自己回调地址
		$jhpay_md5 = '82y5d877sod1t87a0j4gspevz78xkg0j';//尽量不要明文赋值

		$jhpay_config_input_charset = strtolower('utf-8');
		
		//构造要请求的参数数组，无需改动
		$parameter = array(
				"version" => $jhpay_version,
				"merid" =>$jhpay_merid,
				"mername"	=> $jhpay_mername,
				"policyid"	=> $jhpay_policyid,
				"merorderid"	=> $jhpay_merorderid,
				"paymoney"	=> $jhpay_paymoney,
				"productname"	=> $jhpay_productname,
				"productdesc"	=> $jhpay_productdesc,
				"userid"	=> $jhpay_userid,
				"username"	=> $jhpay_username,
				"email"	=> $jhpay_email,
				"phone"	=> $jhpay_phone,
				"extra"	=> $jhpay_extra,
				"custom" => $jhpay_custom,
				"redirect"	=> $jhpay_redirect,
				"redirecturl"	=> $jhpay_redirecturl
				// "md5"	=> $jhpay_md5   

		);

		//签名方式 不需修改
		$jhpay_config_sign_type = strtoupper('MD5');		
		//字符编码格式 目前支持 gbk 或 utf-8
		$jhpay_config_input_charset = strtolower('utf-8');
	
		$jhpay_config_transport   = 'http';
		
		$jhpay_config=array(
			"partner"      =>$jhpay_merid,
			"key"          =>$jhpay_md5,
			"sign_type"    =>$jhpay_config_sign_type,
			"input_charset"=>$jhpay_config_input_charset, 
			"transport"    =>$jhpay_config_transport
		);
		$jhpaySubmit = new JhpaySubmit($jhpay_config);
		$url = $jhpaySubmit->buildRequestForm($parameter,'POST','submit');
		echo $url;
		exit;
	}

?>