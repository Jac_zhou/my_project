/**
 * configure RequireJS
 * prefer named modules to long paths, especially for version mgt
 * or 3rd party libraries
 */
require.config({
    paths: {
        'jquery':'../lib/jquery.min',
        'md5':'../lib/jQuery.md5',
        'angular': '../lib/angular/angular',
        'angular-route': '../lib/angular-route/angular-route',
        'domReady': '../lib/requirejs-domready/domReady',
        'iscroll':'../lib/iscroll',
        'cart':'../lib/cart',
        'location':'../lib/location',
        'common':'../lib/common',
        'bootstrapmin':'../lib/bootstrap.min',
    },

    /**
     * for libs that either do not support AMD out of the box, or
     * require some fine tuning to dependency mgt'
     */
    shim: {
        'angular': {
            exports: 'angular'
        },
        'angular-route': {
            deps: ['angular']
        }
    },

    deps: [
        // kick start application... see bootstrap.js
        './bootstrap'
    ],
    //urlArgs: "bust=" + (new Date()).getTime()  //防止读取缓存，调试用
});
