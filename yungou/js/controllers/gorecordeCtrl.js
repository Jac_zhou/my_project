/**
 * Created by Administrator on 2016/8/8.
 */
define(['./module','./indexCtrl'], function (controllers) {
    'use strict';
    //中奖用户所有的云购码
    controllers.controller('gorecordeCtrl', function ($scope,$http,$location, $rootScope) {
        document.title = "商品详情";
        $('body').scrollTop(0);
        $scope.backTo = function(){
            window.history.go(-1);
        };
        //console.log($location);
        $scope.gid = $location.search()['gid'];     //商品id
        $scope.uid = $location.search()['uid'];     //用户id
        $scope.qishu = $location.search()['qishu']; //期数
        $scope.code = $location.search()['user_code']; //期数
        $scope.recordList = [];
        //console.log($scope.gid);
        //return false;
        $scope.get_more_number = function (code) {
            showLoading();
            var partner = "ZLAPITOH5WAP";
            var timestamp = Date.parse(new Date())/1000;
            var sign = $.md5(partner + timestamp + key);
            $http({method: 'POST', data: {partner: partner, timestamp: timestamp, sign: sign, id: $scope.gid, uid: $scope.uid, qishu: $scope.qishu,code:$scope.code}, url: "../index.php/yunapi/goods/get_more_number"})
                .success(function (response, status, headers, config) {

                    if (response.status == "1") {
                        $scope.recordeList=response.data.goucode.split(",");

                    } else {
                        $rootScope.showAlert(response.message);
                    }
                    hideLoading();
                })
                .error(function (response, status, headers, config) {

                });
            hideLoading();
        };
        $scope.get_more_number();


    })

});