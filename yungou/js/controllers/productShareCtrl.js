/**
 * Created by Administrator on 2016/8/8.
 */
define(['./module','./indexCtrl'], function (controllers) {
    'use strict';
    //晒单分享
    controllers.controller('productShareCtrl', function ($scope, $http, $location, $rootScope) {
        document.title = "晒单分享";
        $('body').scrollTop(0);
        $("#headerId").hide();
        $("#body_top").css('height','0px');
        $("#shopList").hide();
        $scope.backTo = function(){
            window.history.go(-1);
        };

        setTimeout(function(){
            $("a").on("touchstart", function(e){
                $(e.target).addClass('box-shadow-style');
            });
            $("a").on("touchmove", function(e){
                $(e.target).removeClass('box-shadow-style');
            });
            $("a").on("touchend", function(e){
                $(e.target).removeClass('box-shadow-style');
            });
        }, 500);

        $scope.noData = false;

        $scope.page = 1;
        $scope.size = 10;
        $scope.mScroll = "";
        $scope.isPullUp = false;
        $scope.haveMore = false;
        $scope.initIScroll = function(){
            if($scope.mScroll instanceof iScroll){
                $scope.mScroll.destroy();
            }
            $("#scroller_product_share").height($(window).height() - 45);

            $scope.mScroll = new iScroll("scroller_product_share",{
                hScrollbar:false,
                vScrollbar:false,
                onScrollMove: function(){
                    if(this.y <= (this.maxScrollY - 60)){
                        $scope.isPullUp = true;
                    }
                },
                onScrollEnd: function(){
                    if($scope.isPullUp && $scope.haveMore){
                        $scope.page++;
                        $scope.isPullUp = false;
                        if(!isNull($scope.uid)){
                            $scope.get_member_shaidan($scope.page, $scope.size);
                        } else if(!isNull($scope.sid)){
                            $scope.get_shaidan_list($scope.page, $scope.size);
                        }
                    }
                    if(this.y < -800){
                        $('#proId').show();
                    }else{
                        $('#proId').hide();
                    }
                }
            });
        };
        $('#proId').click(function(){
            $scope.mScroll.scrollTo(0, 0, 0);
        });

        $scope.uid = $location.search()['uid'];
        $scope.sid = $location.search()['sid'];
        $scope.shareList = [];

        $scope.get_member_shaidan = function (page, size) {
            showLoading();
            var partner = "ZLAPITOH5WAP";
            var timestamp = Date.parse(new Date())/1000;
            var sign = $.md5(partner + timestamp + key);
            $http({method: 'POST', data: {partner: partner, timestamp: timestamp, sign: sign, uid: $scope.uid, page: page, size: size}, url: "../index.php/yunapi/member/get_member_shaidan"})
                .success(function (response, status, headers, config) {
                    /*var str = JSON.stringify(response);
                     $rootScope.showAlert(str);*/
                    if (response.status == "1") {
                        if($scope.page == 1){
                            if(response.data){
                                $scope.shareList = response.data;
                                if($scope.shareList.length == 0){
                                    $scope.noData = true;
                                }
                            }else{
                                $scope.noData = true;
                            }
                            setTimeout(function(){$scope.initIScroll();},500);
                        } else {
                            $scope.shareList = $scope.shareList.concat(response.data);
                            setTimeout(function(){$scope.mScroll.refresh();},500);
                        }
                        if(response.data.length == $scope.size){
                            $scope.haveMore = true;
                        } else {
                            $scope.haveMore = false;
                        }
                        setTimeout(function(){
                            $("a").on("touchstart", function(e){
                                $(e.target).addClass('box-shadow-style');
                            });
                            $("a").on("touchmove", function(e){
                                $(e.target).removeClass('box-shadow-style');
                            });
                            $("a").on("touchend", function(e){
                                $(e.target).removeClass('box-shadow-style');
                            });
                        }, 500);
                    } else {
                        $rootScope.showAlert(response.message);
                    }
                    hideLoading();
                })
                .error(function (response, status, headers, config) {
                    hideLoading();
                });
        };
        $scope.get_shaidan_list = function (page, size) {
            showLoading();
            var partner = "ZLAPITOH5WAP";
            var timestamp = Date.parse(new Date())/1000;
            var sign = $.md5(partner + timestamp + key);
            $http({method: 'POST', data: {partner: partner, timestamp: timestamp, sign: sign, sid: $scope.sid, page: page, size: size}, url: "../index.php/yunapi/goods/get_shaidan_list"})
                .success(function (response, status, headers, config) {
                    /*var str = JSON.stringify(response);
                     $rootScope.showAlert(str);*/
                    if (response.status == "1") {
                        if($scope.page == 1){
                            if(response.data.list){
                                $scope.shareList = response.data.list;
                                if($scope.shareList.length == 0){
                                    $scope.noData = true;
                                }
                            }else{
                                $scope.noData = true;
                            }
                            setTimeout(function(){$scope.initIScroll();},500);
                        } else {
                            $scope.shareList = $scope.shareList.concat(response.data.list);
                            setTimeout(function(){$scope.mScroll.refresh();},500);
                        }
                        if(response.data.length == $scope.size){
                            $scope.haveMore = true;
                        } else {
                            $scope.haveMore = false;
                        }
                        setTimeout(function(){
                            $("a").on("touchstart", function(e){
                                $(e.target).addClass('box-shadow-style');
                            });
                            $("a").on("touchmove", function(e){
                                $(e.target).removeClass('box-shadow-style');
                            });
                            $("a").on("touchend", function(e){
                                $(e.target).removeClass('box-shadow-style');
                            });
                        }, 500);
                    } else {
                        $rootScope.showAlert(response.message);
                    }
                    hideLoading();
                })
                .error(function (response, status, headers, config) {
                    hideLoading();
                });
        };
        if(!isNull($scope.uid)){
            $scope.get_member_shaidan(1, 10);
        } else if(!isNull($scope.sid)){
            $scope.get_shaidan_list(1, 10);
        }
    })

});