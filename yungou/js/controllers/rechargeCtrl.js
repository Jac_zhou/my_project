define(['./module','./indexCtrl'], function (controllers) {
    'use strict';
    //充值页面
    controllers.controller('rechargeCtrl', function ($scope, $http, $rootScope) {
        document.title = "充值";
        $('body').scrollTop(0);
        $("#headerId").hide();
        $("#body_top").css('height','0px');
        $("#shopList").hide();
        setTimeout(function(){
            $("a").on("touchstart", function(e){
                $(e.target).addClass('box-shadow-style');
            });
            $("a").on("touchmove", function(e){
                $(e.target).removeClass('box-shadow-style');
            });
            $("a").on("touchend", function(e){
                $(e.target).removeClass('box-shadow-style');
            });
        }, 500);
        $scope.isSelected1 = true;
        $scope.isSelected2 = false;
        $scope.isSelected3 = false;
        $scope.isSelected4 = false;
        $scope.isSelected5 = false;
        $scope.setSelectedItem = function(num){
            if(num == 1){
                $scope.isSelected1 = true;
                $scope.isSelected2 = false;
                $scope.isSelected3 = false;
                $scope.isSelected4 = false;
                $scope.isSelected5 = false;
            } else if(num == 2){
                $scope.isSelected1 = false;
                $scope.isSelected2 = true;
                $scope.isSelected3 = false;
                $scope.isSelected4 = false;
                $scope.isSelected5 = false;
            } else if(num == 3){
                $scope.isSelected1 = false;
                $scope.isSelected2 = false;
                $scope.isSelected3 = true;
                $scope.isSelected4 = false;
                $scope.isSelected5 = false;
            } else if(num == 4){
                $scope.isSelected1 = false;
                $scope.isSelected2 = false;
                $scope.isSelected3 = false;
                $scope.isSelected4 = true;
                $scope.isSelected5 = false;
            } else if(num == 5){
                $scope.isSelected1 = false;
                $scope.isSelected2 = false;
                $scope.isSelected3 = false;
                $scope.isSelected4 = false;
                $scope.isSelected5 = true;
            }
        };
        $scope.backTo = function(){
            $location.url('/tab/personCenter');
        };

        $scope.setNumber = function () {
            if(isNull($scope.money) || $scope.money <= 0){
                $scope.money = 1;
            }
        };

        $scope.payClass = [];

        $scope.do_get_pay_class = function () {
            showLoading();
            var partner = "ZLAPITOH5WAP";
            var timestamp = Date.parse(new Date())/1000;
            var sign = $.md5(partner + timestamp + key);
            $http({method: 'POST', data: {partner: partner, timestamp: timestamp, sign: sign}, url: "../index.php/yunapi/cart/do_get_pay_class"})
                .success(function (response, status, headers, config) {
                    /*var str = JSON.stringify(response);
                     $rootScope.showAlert(str);*/
                    if (response.status == "1") {
                        /*for (var i = 0; i <= response.data.length; i--) {


                         if (response.data[i].pay_class == 'alipay') {
                         response.data[i].pay_name = "爱贝支付";
                         break;
                         };
                         };*/
                        //console.log(response.data);
                        $scope.payClass = response.data;
                    } else {
                        $rootScope.showAlert(response.message);
                    }
                    hideLoading();
                })
                .error(function (response, status, headers, config) {
                    hideLoading();
                });
        };
        //充值
        $scope.wap_add_money = function (type) {
            if($scope.isSelected1){
                $scope.money = 20;
            }
            if($scope.isSelected2){
                $scope.money = 50;
            }
            if($scope.isSelected3){
                $scope.money = 100;
            }
            if($scope.isSelected4){
                $scope.money = 200;
            }
            if(isNull($scope.money)){
                $rootScope.showAlert("请选择或输入要充值的金额");
                return;
            }
            showLoading();
            var partner = "ZLAPITOH5WAP";
            var timestamp = Date.parse(new Date())/1000;
            var sign = $.md5(partner + timestamp + key);
            //console.log(type);return false;
            if(type == 'wxpay'){

                var url = "../index.php/yunapi/cart/wap_add_money?data=";

                var ss = '{"partner":"'+partner+'","timestamp":"'+timestamp+'","sign":"'+sign+'","uid":"'+$rootScope.holdUID+'","money":"'+$scope.money+'","pay_type":"'+type+'"}';
                //var s = '{"partner":"'+partner+'",'"timestamp":"'+timestamp+'","sign":"+sign+"','uid':'"+$rootScope.holdUID+"','money':'"+$scope.money+"','pay_type':'"+type+"'}";
                url = url + ss;
                window.location.href = url;
                return false;

            }

            if(type == 'iapppay'){

                alert('爱贝支付是合作平台，请用户放心使用');

            }

            $http({method: 'POST', data: {partner: partner, timestamp: timestamp, sign: sign, uid: $rootScope.holdUID, money: $scope.money, pay_type: type}, url: "../index.php/yunapi/cart/wap_add_money"})
                .success(function (response, status, headers, config) {

                    console.log(response);

                    if (response.status == "1") {
                        //支付宝调用
                        if (type == 'alipay') {
                            $("#resultDisplay").html(response.data);

                        }else if (type == 'iapppay') {
                            window.location.href = response.data;
                        }

                    } else {
                        $rootScope.showAlert(response.message);
                    }
                    hideLoading();
                })
                .error(function (response, status, headers, config) {
                    hideLoading();
                });
        };
        $scope.confirmPay = function($event) {
            /*$($event.target).css('box-shadow', '0 0 15px #000');
             setTimeout(function(){
             $($event.target).css('box-shadow', 'none');
             }, 160);*/
            var radios = document.getElementsByName("optionsRadios");
            var tag = 0;
            for (var i = 0; i < radios.length; i++) {
                if (radios[i].checked) {
                    tag = 1;
                    $scope.wap_add_money(radios[i].value);
                    break;
                }
            }
            if(tag == 0){
                $rootScope.showAlert("请选择一种支付方式");
            }
        };
        $scope.do_get_pay_class();
    })

});/**
 * Created by Administrator on 2016/8/8.
 */
