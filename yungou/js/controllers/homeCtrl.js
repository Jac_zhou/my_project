/**
 * Created by Administrator on 2016/8/8.
 */
define(['./module','./indexCtrl'], function (controllers) {
    'use strict';
    //首页
    controllers.controller('homeCtrl', function ($scope, $http, $rootScope,$location) {
        document.title = "易中夺宝-最容易中的夺宝平台！";
        $("#headerId").show();
        $("#shopList").show();
        $("#proId").hide();
        $('.carousel').carousel({
            interval: 3000
        });

        $scope.$emit("itemChange",1);

        $scope.goodsStatus = "倒计时";

        $scope.imgTouchMove = function(){
            var slidePage = {};
            $('#carousel').on('touchstart', function(e) {
                var touch = e.originalEvent.targetTouches[0];
                slidePage.x = touch.pageX;
            });
            $('#carousel').on('touchend', function(e) {
                var touch = e.originalEvent.changedTouches[0];
                var x = touch.pageX;
                if(x - parseInt(slidePage.x) > 20){
                    //从左往右
                    $('#carousel').carousel('prev');
                } else if(x - parseInt(slidePage.x) < -20){
                    //从右往左
                    $('#carousel').carousel('next');
                }
            });
        };
        $scope.get_win_member_info_copy = function(num){
            if($scope.newGoods[num].status_public==1){
                return false;
            }
            var partner = "ZLAPITOH5WAP";
            var timestamp = Date.parse(new Date())/1000;
            var sign = $.md5(partner + timestamp + key);
            $http({method: 'POST', data: {partner: partner, timestamp: timestamp, sign: sign, id: $scope.newGoods[num].id}, url: "../index.php/yunapi/goods/get_win_member_info"})
                .success(function (response, status, headers, config) {
                    console.log(response);

                    if (response.status == "1") {
                        $scope.newGoods[num].waiting = false;
                        $scope.newGoods[num].tag = 0;
                        $scope.newGoods[num].goodsStatus = "恭喜";
                        $scope.newGoods[num].username = response.data.username;
                        $scope.newGoods[num].status_public=1;
                    } else {
                        //$rootScope.showAlert(response.message);
                    }
                })
                .error(function (response, status, headers, config) {

                });
        };

        $scope.imgData = [];

        $scope.get_slide_list = function () {
            showLoading();
            var partner = "ZLAPITOH5WAP";
            var timestamp = Date.parse(new Date())/1000;
            var sign = $.md5(partner + timestamp + key);
            $http({method: 'POST', data: {partner: partner, timestamp: timestamp, sign: sign}, url: "../index.php/yunapi/index/get_slide_list"})
                .success(function (response, status, headers, config) {
                    if (response.status == "1") {
                        var str = JSON.stringify(response);
                        //console.log("----------->", str);
                        if(response.data){
                            $scope.imgData = response.data;
                            setTimeout(function(){
                                $("a").on("touchstart", function(e){
                                    $(e.target).addClass('box-shadow-style');
                                });
                                $("a").on("touchmove", function(e){
                                    $(e.target).removeClass('box-shadow-style');
                                });
                                $("a").on("touchend", function(e){
                                    $(e.target).removeClass('box-shadow-style');
                                });
                            }, 500);
                        }
                    } else {
                        $rootScope.showAlert(response.message);
                    }
                    hideLoading();
                })
                .error(function (response, status, headers, config) {
                    hideLoading();
                });
        };

        //$scope.endTime = "";
        $scope.newGoods = [];
        $scope.keepTime = false;
        $scope.isFirst = true;

        $scope.get_newest_lottery = function (page, size) {
            //showLoading();
            var partner = "ZLAPITOH5WAP";
            var timestamp = Date.parse(new Date())/1000;
            var sign = $.md5(partner + timestamp + key);
            $http({method: 'POST', data: {partner: partner, timestamp: timestamp, sign: sign, page: page, size: size}, url: "../index.php/yunapi/goods/get_newest_lottery"})
                .success(function (response, status, headers, config) {
                    if (response.status == "1") {
                        $scope.newGoods = response.data.list;
                        for(var i = 0; i < $scope.newGoods.length; i++){
                            $scope.newGoods[i].waiting = false;
                            if($scope.newGoods[i].tag == 0){
                                $scope.newGoods[i].goodsStatus = "恭喜";
                                $scope.newGoods[i].status_public=1;
                            } else {
                                $scope.newGoods[i].goodsStatus = "倒计时";
                                //$scope.endTime = newGoods[i].q_end_time;
                                if($scope.newGoods[i].q_end_time.indexOf("年")){
                                    $scope.newGoods[i].q_end_time = $scope.newGoods[i].q_end_time.replace("年", "/");
                                }
                                if($scope.newGoods[i].q_end_time.indexOf("月")){
                                    $scope.newGoods[i].q_end_time = $scope.newGoods[i].q_end_time.replace("月", "/");
                                }
                                if($scope.newGoods[i].q_end_time.indexOf("日")){
                                    $scope.newGoods[i].q_end_time = $scope.newGoods[i].q_end_time.replace("日", "");
                                }
                                $scope.newGoods[i].isTime = true;
                            }
                        }
                        if($scope.newGoods[0].isTime){

                            setTimeout(function(){
                                var test_time_one = $scope.newGoods[0].test_time;

                                test_time_one = (new Date().getTime())+(parseInt(test_time_one))*1000;

                                $("#fnTimeCountDown0").fnTimeCountDown(test_time_one,  function(){
                                    if($scope.newGoods[0].tag == 1){
                                        $scope.newGoods[0].waiting = true;
                                        $scope.newGoods[0].tag = -1;
                                        setTimeout(function(){



                                            var chaxun0 = setInterval(function(){
                                                var partner = "ZLAPITOH5WAP";
                                                var timestamp = Date.parse(new Date())/1000;
                                                var sign = $.md5(partner + timestamp + key);
                                                $http({method: 'POST', data: {partner: partner, timestamp: timestamp, sign: sign, id: $scope.newGoods[0].id}, url: "../index.php/yunapi/goods/get_win_member_info"})
                                                    .success(function (response, status, headers, config) {

                                                        var str = JSON.stringify(response);

                                                        if (response.status == "1") {
                                                            $scope.newGoods[0].waiting = false;
                                                            $scope.newGoods[0].tag = 0;
                                                            clearInterval(chaxun0);
                                                            $scope.newGoods[0].goodsStatus = "恭喜";
                                                            $scope.newGoods[0].username = response.data.username;
                                                        } else {
                                                            //$rootScope.showAlert(response.message);
                                                        }
                                                    })
                                                    .error(function (response, status, headers, config) {

                                                    });
                                            },3000);
                                        },3000);
                                    }
                                });
                            },500);



                        }
                        if($scope.newGoods[1].isTime){

                            setTimeout(function(){
                                var test_time_two = $scope.newGoods[1].test_time;
                                test_time_two = (new Date().getTime())+(parseInt(test_time_two))*1000;
                                $("#fnTimeCountDown1").fnTimeCountDown(test_time_two, function(){
                                    if($scope.newGoods[1].tag == 1){
                                        $scope.newGoods[1].waiting = true;
                                        $scope.newGoods[1].tag = -1;
                                        setTimeout(function(){
                                            var chaxun1 = setInterval(function(){
                                                var partner = "ZLAPITOH5WAP";
                                                var timestamp = Date.parse(new Date())/1000;
                                                var sign = $.md5(partner + timestamp + key);
                                                $http({method: 'POST', data: {partner: partner, timestamp: timestamp, sign: sign, id: $scope.newGoods[1].id}, url: "../index.php/yunapi/goods/get_win_member_info"})
                                                    .success(function (response, status, headers, config) {
                                                        //console.log(response);
                                                        var str = JSON.stringify(response);
                                                        //console.log("home:", str);
                                                        if (response.status == "1") {
                                                            $scope.newGoods[1].waiting = false;
                                                            $scope.newGoods[1].tag = 0;
                                                            clearInterval(chaxun1);
                                                            $scope.newGoods[1].goodsStatus = "恭喜";
                                                            $scope.newGoods[1].username = response.data.username;
                                                        } else {
                                                            //$rootScope.showAlert(response.message);
                                                        }
                                                    })
                                                    .error(function (response, status, headers, config) {

                                                    });
                                            },3000);
                                        },3000);
                                    }
                                });
                            },500);
                        }
                        if($scope.newGoods[2].isTime){

                            setTimeout(function(){
                                var test_time_three = $scope.newGoods[2].test_time;
                                test_time_three = (new Date().getTime())+(parseInt(test_time_three))*1000;
                                $("#fnTimeCountDown2").fnTimeCountDown(test_time_three, function(){
                                    if($scope.newGoods[2].tag == 1){
                                        $scope.newGoods[2].waiting = true;
                                        $scope.newGoods[2].tag = -1;
                                        setTimeout(function(){
                                            var chaxun2 = setInterval(function(){
                                                var partner = "ZLAPITOH5WAP";
                                                var timestamp = Date.parse(new Date())/1000;
                                                var sign = $.md5(partner + timestamp + key);
                                                $http({method: 'POST', data: {partner: partner, timestamp: timestamp, sign: sign, id: $scope.newGoods[2].id}, url: "../index.php/yunapi/goods/get_win_member_info"})
                                                    .success(function (response, status, headers, config) {
                                                        //console.log(response);
                                                        var str = JSON.stringify(response);
                                                        //console.log("home:", str);
                                                        if (response.status == "1") {
                                                            $scope.newGoods[2].waiting = false;
                                                            $scope.newGoods[2].tag = 0;
                                                            clearInterval(chaxun2);
                                                            $scope.newGoods[2].goodsStatus = "恭喜";
                                                            $scope.newGoods[2].username = response.data.username;
                                                        } else {
                                                            //$rootScope.showAlert(response.message);
                                                        }
                                                    })
                                                    .error(function (response, status, headers, config) {

                                                    });
                                            },3000);
                                        },3000);
                                    }
                                });
                            },500);
                        }

                        setTimeout(function(){
                            $("a").on("touchstart", function(e){
                                $(e.target).addClass('box-shadow-style');
                            });
                            $("a").on("touchmove", function(e){
                                $(e.target).removeClass('box-shadow-style');
                            });
                            $("a").on("touchend", function(e){
                                $(e.target).removeClass('box-shadow-style');
                            });
                        }, 500);
                    } else {
                        $rootScope.showAlert(response.message);
                    }
                    //hideLoading();
                })
                .error(function (response, status, headers, config) {
                    //hideLoading();
                });
        };

        $scope.hotGoods = [];
        $scope.nGoods = [];
        $scope.get_type_goods_list = function (page, size, type) {
            showLoading();
            var partner = "ZLAPITOH5WAP";
            var timestamp = Date.parse(new Date())/1000;
            var sign = $.md5(partner + timestamp + key);
            $http({method: 'POST', data: {partner: partner, timestamp: timestamp, sign: sign, page: page, size: size, type: type, sort: 'desc',uid:$rootScope.holdUID}, url: "../index.php/yunapi/index/get_type_goods_list"})
                .success(function (response, status, headers, config) {
                    if (response.status == "1") {
                        if(type == 'renqi'){
                            $scope.hotGoods = response.data.list;
                        }else if(type == 'zuixin'){
                            $scope.nGoods = response.data.list;
                        }
                        setTimeout(function(){
                            $("a").on("touchstart", function(e){
                                $(e.target).addClass('box-shadow-style');
                            });
                            $("a").on("touchmove", function(e){
                                $(e.target).removeClass('box-shadow-style');
                            });
                            $("a").on("touchend", function(e){
                                $(e.target).removeClass('box-shadow-style');
                            });
                        }, 500);
                    } else {
                        $rootScope.showAlert(response.message);
                    }
                    hideLoading();
                })
                .error(function (response, status, headers, config) {
                    hideLoading();
                });
        };
        $scope.get_win_member_info = function (id, i) {
            showLoading();
            var partner = "ZLAPITOH5WAP";
            var timestamp = Date.parse(new Date())/1000;
            var sign = $.md5(partner + timestamp + key);
            $http({method: 'POST', data: {partner: partner, timestamp: timestamp, sign: sign, id: id}, url: "../index.php/yunapi/goods/get_win_member_info"})
                .success(function (response, status, headers, config) {
                    if (response.status == "1") {
                        $scope.newGoods[i].tag = 0;
                        $scope.newGoods[i].goodsStatus = "恭喜";
                        $scope.newGoods[i].username = response.data.username;

                        setTimeout(function(){
                            $("a").on("touchstart", function(e){
                                $(e.target).addClass('box-shadow-style');
                            });
                            $("a").on("touchmove", function(e){
                                $(e.target).removeClass('box-shadow-style');
                            });
                            $("a").on("touchend", function(e){
                                $(e.target).removeClass('box-shadow-style');
                            });
                        }, 500);
                    } else {
                        $rootScope.showAlert(response.message);
                        return false;
                    }
                    hideLoading();
                })
                .error(function (response, status, headers, config) {
                    hideLoading();
                });
        };

        $scope.imgTouchMove();
        $scope.get_slide_list();
        $scope.get_newest_lottery(1, 3);
        $scope.get_type_goods_list(1, 10, 'renqi');
        $scope.get_type_goods_list(1, 3, 'zuixin');
    })

});
