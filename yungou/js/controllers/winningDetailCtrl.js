/**
 * Created by Administrator on 2016/8/8.
 */
define(['./module','./indexCtrl'], function (controllers) {
    'use strict';
    //中奖确认
    controllers.controller('winningDetailCtrl', function ($scope, $http, $location, $rootScope) {
        document.title = "中奖确认";
        $('body').scrollTop(0);
        $("#headerId").hide();
        $("#body_top").css('height','0px');
        $("#shopList").hide();
        $scope.backTo = function(){
            window.history.go(-1);
        };
        setTimeout(function(){
            $("a").on("touchstart", function(e){
                $(e.target).addClass('box-shadow-style');
            });
            $("a").on("touchmove", function(e){
                $(e.target).removeClass('box-shadow-style');
            });
            $("a").on("touchend", function(e){
                $(e.target).removeClass('box-shadow-style');
            });
        }, 500);

        $scope.id = $location.search()['id'];

        $scope.get_win_records_info = function() {
            showLoading();
            var partner = "ZLAPITOH5WAP";
            var timestamp = Date.parse(new Date())/1000;
            var sign = $.md5(partner + timestamp + key);
            $http({method: 'POST', data: {partner: partner, timestamp: timestamp, sign: sign, id: $scope.id, uid: $rootScope.holdUID}, url: "../index.php/yunapi/member/get_win_records_info"})
                .success(function (response, status, headers, config) {
                    if (response.status == "1") {
                        $scope.winningInfo = response.data;
                        setTimeout(function(){
                            $("a").on("touchstart", function(e){
                                $(e.target).addClass('box-shadow-style');
                            });
                            $("a").on("touchmove", function(e){
                                $(e.target).removeClass('box-shadow-style');
                            });
                            $("a").on("touchend", function(e){
                                $(e.target).removeClass('box-shadow-style');
                            });
                        }, 500);
                    } else {
                        $rootScope.showAlert(response.message);
                    }
                    hideLoading();
                })
                .error(function (response, status, headers, config) {
                    hideLoading();
                });
        };
        $scope.do_confirm_receiving = function() {
            showLoading();
            var partner = "ZLAPITOH5WAP";
            var timestamp = Date.parse(new Date())/1000;
            var sign = $.md5(partner + timestamp + key);
            $http({method: 'POST', data: {partner: partner, timestamp: timestamp, sign: sign, rid: $scope.id, uid: $rootScope.holdUID}, url: "../index.php/yunapi/member/do_confirm_receiving"})
                .success(function (response, status, headers, config) {
                    if (response.status == "1") {
                        $rootScope.showAlert(response.message);
                        window.location.reload();
                    } else {
                        $rootScope.showAlert(response.message);
                    }
                    hideLoading();
                })
                .error(function (response, status, headers, config) {
                    hideLoading();
                });
        };

        $scope.get_member_address = function () {
            showLoading();
            var partner = "ZLAPITOH5WAP";
            var timestamp = Date.parse(new Date())/1000;
            var sign = $.md5(partner + timestamp + key);
            $http({method: 'POST', data: {partner: partner, timestamp: timestamp, sign: sign, uid: $rootScope.holdUID}, url: "../index.php/yunapi/member/get_member_address"})
                .success(function (response, status, headers, config) {
                    if (response.status == "1") {
                        if(response.data){
                            $rootScope.showAlert(response.data, 1, $scope.id);
                        }else{
                            $rootScope.showAlert("还没有收货地址，请添加");
                            $location.url("/tab/addAddress?uid=" + $rootScope.holdUID);
                        }
                    } else {
                        $rootScope.showAlert(response.message);
                    }
                    hideLoading();
                })
                .error(function (response, status, headers, config) {
                    hideLoading();
                });
        };

        $scope.confirmReceive = function(){
            $rootScope.confirmMsg = "确定已收货了吗？";
            $('#confirmModal').modal('show');
        };

        $scope.confirmAddress = function(){
            $scope.get_member_address();
        };

        $('#confirmModal').on('hide.bs.modal', function () {
            if($rootScope.holdState == 1){
                $rootScope.holdState = 0;
                $scope.do_confirm_receiving();
            }
        });

        $scope.get_win_records_info();

    })

});
