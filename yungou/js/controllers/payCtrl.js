/**
 * Created by Administrator on 2016/8/8.
 */
define(['./module','./indexCtrl'], function (controllers) {
    'use strict';
    //支付订单列表
    controllers.controller('payCtrl', function ($scope, $http, $location, $rootScope) {
        document.title = "支付订单";
        $('body').scrollTop(0);
        $("#headerId").hide();
        $("#body_top").css('height','0px');
        $("#shopList").hide();
        $scope.backTo = function(){
            window.history.go(-1);
        };
        setTimeout(function(){
            $("a").on("touchstart", function(e){
                $(e.target).addClass('box-shadow-style');
            });
            $("a").on("touchmove", function(e){
                $(e.target).removeClass('box-shadow-style');
            });
            $("a").on("touchend", function(e){
                $(e.target).removeClass('box-shadow-style');
            });
        }, 500);

        $scope.mScroll = "";
        $scope.initIScroll = function(){
            if($scope.mScroll instanceof iScroll){
                $scope.mScroll.destroy();
            }
            $("#pay_content").height($(window).height()-90);

            $scope.mScroll = new iScroll("pay_content",{
                hScrollbar:false,
                vScrollbar:false,
                onScrollMove: function(){

                },
                onScrollEnd: function(){
                    if(this.y < -800){
                        $('#proId').show();
                    }else{
                        $('#proId').hide();
                    }
                }
            });
        };
        $('#proId').click(function(){
            $scope.mScroll.scrollTo(0, 0, 0);
        });

        setTimeout(function(){$scope.initIScroll();},100);

        $scope.price = 0;
        for(var i = 0; i < $rootScope.shopList.length; i++){
            $rootScope.shopList[i].num = Math.ceil($rootScope.shopList[i].num);
            $scope.price += $rootScope.shopList[i].num;
        }

        $scope.payClass = [];

        $scope.do_get_pay_class = function () {
            showLoading();
            var partner = "ZLAPITOH5WAP";
            var timestamp = Date.parse(new Date())/1000;
            var sign = $.md5(partner + timestamp + key);
            $http({method: 'POST', data: {partner: partner, timestamp: timestamp, sign: sign}, url: "../index.php/yunapi/cart/do_get_pay_class"})
                .success(function (response, status, headers, config) {
                    /*var str = JSON.stringify(response);
                     $rootScope.showAlert(str);*/
                    if (response.status == "1") {
                        $scope.payClass = response.data;
                    } else {
                        $rootScope.showAlert(response.message);
                    }
                    hideLoading();
                })
                .error(function (response, status, headers, config) {
                    hideLoading();
                });
        };

        $scope.get_member_info = function() {
            showLoading();
            var partner = "ZLAPITOH5WAP";
            var timestamp = Date.parse(new Date())/1000;
            var sign = $.md5(partner + timestamp + key);
            $http({method: 'POST', data: {partner: partner, timestamp: timestamp, sign: sign, uid: $rootScope.holdUID}, url: "../index.php/yunapi/member/get_member_info"})
                .success(function (response, status, headers, config) {
                    if (response.status == "1") {
                        $scope.money = response.data.money;
                        if($scope.money > 0){
                            $("#optionsCheck").attr("checked", true);
                            $scope.enough = true;
                            $scope.price_wait = $scope.price - $scope.money;
                            if($scope.price_wait < 0){
                                $scope.price_wait = 0;
                            }
                            if($scope.money >= $scope.price){
                                $scope.isEnough = true;
                            }else{
                                $scope.isEnough = false;
                                $scope.do_get_pay_class();
                            }
                        }else{
                            $scope.price_wait = $scope.price;
                            $("#optionsCheck").attr("disabled", true);
                            $scope.enough = false;
                            $scope.do_get_pay_class();
                        }
                    } else {
                        $rootScope.showAlert(response.message);
                    }
                    hideLoading();
                })
                .error(function (response, status, headers, config) {
                    hideLoading();
                });
        };

        $("#optionsCheck").click(function(){
            if(document.getElementById("optionsCheck").checked){
                $scope.price_wait = $scope.price - $scope.money;
                if($scope.price_wait < 0){
                    $scope.price_wait = 0;
                }
            }else{
                $scope.price_wait = $scope.price;
            }
            $scope.$apply();
        });

        $scope.confirmPay = function() {
            var checkbox = document.getElementById("optionsCheck");
            var radios = document.getElementsByName("optionsRadios");
            var tag = 0;
            for (var i = 0; i < radios.length; i++) {
                if (radios[i].checked) {
                    tag = 1;
                    console.log(radios[i].value);
                    if(checkbox.checked){
                        $location.url("/tab/payResult?type=" + radios[i].value + "&is_yue=1&money="+$scope.price);
                    }else{
                        $location.url("/tab/payResult?type=" + radios[i].value + "&is_yue=0&money="+$scope.price);
                    }
                    break;
                }
            }
            if(tag == 0){
                if(checkbox.checked){
                    if($scope.money < $scope.price){
                        $rootScope.showAlert("余额不足，请选择一种支付方式");
                    }else{
                        $location.url("/tab/payResult?type=" + "&is_yue=1");
                    }
                }else{
                    $rootScope.showAlert("请选择一种支付方式");
                }
            }
        };

        $scope.get_member_info();

    })

});