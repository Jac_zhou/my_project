define(['./module','./indexCtrl'], function (controllers) {
    'use strict';
    //修改昵称
    controllers.controller('updateNicknameCtrl', function ($scope, $http, $location, $rootScope) {
        document.title = "修改昵称";
        $('body').scrollTop(0);
        $("#headerId").hide();
        $("#body_top").css('height','0px');
        $("#shopList").hide();
        $scope.backTo = function(){
            window.history.go(-1);
        };
        setTimeout(function(){
            $("a").on("touchstart", function(e){
                $(e.target).addClass('box-shadow-style');
            });
            $("a").on("touchmove", function(e){
                $(e.target).removeClass('box-shadow-style');
            });
            $("a").on("touchend", function(e){
                $(e.target).removeClass('box-shadow-style');
            });
        }, 500);

        $scope.uid = $location.search()['uid'];

        $scope.do_mod_username = function ($event) {
            /*$($event.target).css('color', '#f81000');
             setTimeout(function(){
             $($event.target).css('color', '#0079fe');
             }, 160);*/
            if(isNull($scope.username)){
                $rootScope.showAlert("昵称不能为空");
                return;
            }
            showLoading();
            var partner = "ZLAPITOH5WAP";
            var timestamp = Date.parse(new Date())/1000;
            var sign = $.md5(partner + timestamp + key);
            $http({method: 'POST', data: {partner: partner, timestamp: timestamp, sign: sign, uid: $scope.uid, username: $scope.username}, url: "../index.php/yunapi/member/do_mod_username"})
                .success(function (response, status, headers, config) {
                    if (response.status == "1") {
                        $location.url("/tab/personalInfo?uid=" + $scope.uid);
                    } else {
                        $rootScope.showAlert(response.message);
                    }
                    hideLoading();
                })
                .error(function (response, status, headers, config) {
                    hideLoading();
                });
        };
    })

});