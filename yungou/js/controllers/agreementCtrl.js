/**
 * Created by Administrator on 2016/8/8.
 */
define(['./module.js','./indexCtrl'], function (controllers) {
    'use strict';
    //服务协议
    controllers.controller('agreementCtrl', function ($scope, $http, $location, $rootScope) {
        document.title = "服务协议";
        $('body').scrollTop(0);
        $("#headerId").hide();
        $("#body_top").css('height','0px');
        $("#shopList").hide();
        $scope.backTo = function(){
            window.history.go(-1);
        };
        $("#footbar").hide();

        setTimeout(function(){
            $("a").on("touchstart", function(e){
                $(e.target).addClass('box-shadow-style');
            });
            $("a").on("touchmove", function(e){
                $(e.target).removeClass('box-shadow-style');
            });
            $("a").on("touchend", function(e){
                $(e.target).removeClass('box-shadow-style');
            });
        }, 500);

    })

});