/**
 * Created by Administrator on 2016/8/8.
 */
define(['./module','./indexCtrl'], function (controllers) {
    'use strict';
    //期数列表页
    controllers.controller('qishuListCtrl',function($scope, $http, $location, $rootScope){
        //console.log($location);return false;
        $scope.sid = $location.search()['sid'];
        if($scope.sid>0){
            //console.log($http);return false;
        }else{
            $rootScope.showAlert('非法访问！');
            return false;
        }
        $scope.qishu_text = 1;
        $scope.gomodel = function(qishu_text){
            if(qishu_text > 0){
                var partner = "ZLAPITOH5WAP";
                var timestamp = Date.parse(new Date())/1000;
                var sign = $.md5(partner + timestamp + key);
                $http({method: 'POST', data: {partner: partner, timestamp: timestamp, sign: sign, sid: $scope.sid,qishu:qishu_text}, url: "../index.php/yunapi/goods/get_qishu_id"})
                    .success(function(response){

                        if(response.status == 1){

                            $scope.id = response.data['id'];
                            if(response.data['id'] >0){
                                $scope.godetail(response.data['id']);
                            }else{
                                $rootScope.showAlert("没有这期");
                            }
                        }else{
                            $rootScope.showAlert(response.message);
                        }
                    })
                    .error(function(){
                        $rootScope.showAlert('请检查网络');
                    });
            }else{
                $rootScope.showAlert("请输入合适的数字");
            }
        };
        $scope.godetail = function(id){
            $location.url("/tab/productDetail?id="+id);
        };
        document.title = "往期揭晓";
        $('body').scrollTop(0);
        $("#headerId").hide();
        $("#body_top").css('height','0px');
        $("#shopList").hide();
        $scope.backTo = function(){
            window.history.go(-1);
        };

        setTimeout(function(){
            $("a").on("touchstart", function(e){
                $(e.target).addClass('box-shadow-style');
            });
            $("a").on("touchmove", function(e){
                $(e.target).removeClass('box-shadow-style');
            });
            $("a").on("touchend", function(e){
                $(e.target).removeClass('box-shadow-style');
            });
        }, 500);

        $scope.noData = false;

        $scope.page = 1;
        $scope.size = 9;
        $scope.mScroll = "";
        $scope.isPullUp = false;
        $scope.haveMore = false;
        $scope.initIScroll = function(){
            if($scope.mScroll instanceof iScroll){
                $scope.mScroll.destroy();
            }
            $("#scroller_passed_publish").height($(window).height() - 45);

            $scope.mScroll = new iScroll("scroller_passed_publish",{
                hScrollbar:false,
                vScrollbar:false,
                onScrollMove: function(){
                    //console.log(this.maxScrollY);
                    //console.log(this.y );
                    if(this.y <= (this.maxScrollY - 60)){
                        $scope.isPullUp = true;
                    }
                },
                onScrollEnd: function(){
                    if($scope.isPullUp && $scope.haveMore){
                        $scope.page++;
                        $scope.isPullUp = false;
                        $scope.get_old_lottery($scope.page, $scope.size);
                    }
                    if(this.y < -800){
                        $('#proId').show();
                    }else{
                        $('#proId').hide();
                    }
                }
            });
        };
        $('#proId').click(function(){
            $scope.mScroll.scrollTo(0, 0, 0);
        });
        $scope.finishedList = [];
        $scope.progressList = [];
        $scope.newestList = [];
        $scope.sid = $location.search()['sid'];
        $scope.get_old_lottery = function (page, size) {
            showLoading();
            var partner = "ZLAPITOH5WAP";
            var timestamp = Date.parse(new Date())/1000;
            var sign = $.md5(partner + timestamp + key);
            $http({method: 'POST', data: {partner: partner, timestamp: timestamp, sign: sign, page: page, size: size, sid: $scope.sid}, url: "../index.php/yunapi/goods/get_old_lottery"})
                .success(function (response, status, headers, config) {
                    /*var str = JSON.stringify(response);
                     $rootScope.showAlert(str);*/
                    if (response.status == "1") {
                        var list = response.data.list;
                        //console.log(list);
                        for(var i = 0; i < list.length; i++){
                            if(list[i].tag == 0){
                                $scope.finishedList.push(list[i]);
                            } else if(list[i].tag == 2){
                                $scope.progressList.push(list[i]);
                            }
                            if(list[i].tag == 1){
                                $scope.newestList.push(list[i]);
                            }
                        }
                        if($scope.page == 1){
                            if($scope.finishedList.length == 0 && $scope.progressList.length == 0){
                                $scope.noData = true;
                            }
                            setTimeout(function(){$scope.initIScroll();},500);
                        } else {
                            setTimeout(function(){$scope.mScroll.refresh();},500);
                        }
                        if(list.length == $scope.size){
                            $scope.haveMore = true;
                        } else {
                            $scope.haveMore = false;
                        }
                        setTimeout(function(){
                            $("a").on("touchstart", function(e){
                                $(e.target).addClass('box-shadow-style');
                            });
                            $("a").on("touchmove", function(e){
                                $(e.target).removeClass('box-shadow-style');
                            });
                            $("a").on("touchend", function(e){
                                $(e.target).removeClass('box-shadow-style');
                            });
                        }, 500);
                    } else {
                        $rootScope.showAlert(response.message);
                    }
                    hideLoading();
                })
                .error(function (response, status, headers, config) {
                    hideLoading();
                });
        };
        $scope.get_old_lottery(1,9);
    })

});