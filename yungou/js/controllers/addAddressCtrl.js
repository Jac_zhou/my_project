/**
 * Created by Administrator on 2016/8/8.
 */
define(['./module','./indexCtrl'], function (controllers) {
    'use strict';
    //添加收货地址
    controllers.controller('addAddressCtrl', function ($scope, $http, $location, $rootScope) {
        document.title = "添加地址";
        $('body').scrollTop(0);
        $("#headerId").hide();
        $("#body_top").css('height','0px');
        $("#shopList").hide();
        $scope.isSelected = true;
        $scope.default = 1;
        $scope.setSelectedItem = function(){
            if($scope.isSelected){
                $scope.isSelected = false;
                $scope.default = 0;
            } else {
                $scope.isSelected = true;
                $scope.default = 1;
            }
        };
        $scope.backTo = function(){
            window.history.go(-1);
        };
        setTimeout(function(){
            $("a").on("touchstart", function(e){
                $(e.target).addClass('box-shadow-style');
            });
            $("a").on("touchmove", function(e){
                $(e.target).removeClass('box-shadow-style');
            });
            $("a").on("touchend", function(e){
                $(e.target).removeClass('box-shadow-style');
            });
        }, 500);

        $scope.uid = $location.search()['uid'];
        $scope.flag = $location.search()['flag'];

        $scope.showLocation = function(province , city , town) {
            var loc	= new Location();
            var title	= ['省份' , '地级市' , '市、县、区'];
            $.each(title , function(k , v) {
                title[k]	= '<option value="">'+v+'</option>';
            });

            $('#loc_province').append(title[0]);
            $('#loc_city').append(title[1]);
            $('#loc_town').append(title[2]);

            $('#loc_province').change(function() {
                $('#loc_city').empty();
                $('#loc_city').append(title[1]);
                loc.fillOption('loc_city' , '0,'+$('#loc_province').val());
                $('#loc_town').empty();
                $('#loc_town').append(title[2]);
                $scope.sheng = $("#loc_province option:selected").text();
            });

            $('#loc_city').change(function() {
                $('#loc_town').empty();
                $('#loc_town').append(title[2]);
                loc.fillOption('loc_town' , '0,' + $('#loc_province').val() + ',' + $('#loc_city').val());
                $scope.shi = $("#loc_city option:selected").text();
            });

            $('#loc_town').change(function() {
                $scope.xian = $("#loc_town option:selected").text();
            });

            if (province) {
                loc.fillOption('loc_province' , '0' , province);

                if (city) {
                    loc.fillOption('loc_city' , '0,'+province , city);

                    if (town) {
                        loc.fillOption('loc_town' , '0,'+province+','+city , town);
                    }
                }

            } else {
                loc.fillOption('loc_province' , '0');
            }
        };

        $scope.isSave = false;

        $scope.do_save_member_address = function ($event) {
            /*$($event.target).css('color', '#f81000');
             setTimeout(function(){
             $($event.target).css('color', '#0079fe');
             }, 160);*/
            if(isNull($scope.shouhuoren)){
                $rootScope.showAlert("收货人不能为空");
                return;
            }
            if(isNull($scope.mobile)){
                $rootScope.showAlert("收货人手机号码不能为空");
                return;
            } else if($scope.mobile.length != 11){
                $rootScope.showAlert("请输入有效的手机号码");
                return;
            } else {
                var reg = /^(((13[0-9]{1})|(15[0-9]{1})|(18[0-9]{1}))+\d{8})$/;
                if(!reg.test($scope.mobile)){
                    $rootScope.showAlert("请输入有效的手机号码");
                    return;
                }
            }
            if(isNull($scope.sheng)){
                $rootScope.showAlert("请选择省份");
                return;
            }
            if(isNull($scope.shi)){
                $rootScope.showAlert("请选择城市");
                return;
            }
            if(isNull($scope.xian)){
                $rootScope.showAlert("请选择地区");
                return;
            }
            if(isNull($scope.jiedao)){
                $rootScope.showAlert("请输入街道号");
                return;
            }
            if($scope.isSave){
                return;
            }
            $scope.isSave = true;
            showLoading();
            var partner = "ZLAPITOH5WAP";
            var timestamp = Date.parse(new Date())/1000;
            var sign = $.md5(partner + timestamp + key);
            $http({
                method: 'POST',
                data: {
                    partner: partner,
                    timestamp: timestamp,
                    sign: sign,
                    uid: $scope.uid,
                    shouhuoren: $scope.shouhuoren,
                    mobile: $scope.mobile,
                    sheng: $scope.sheng,
                    shi: $scope.shi,
                    xian: $scope.xian,
                    jiedao: $scope.jiedao,
                    default: $scope.default
                },
                url: "../index.php/yunapi/member/do_save_member_address"
            })
                .success(function (response, status, headers, config) {
                    if (response.status == "1") {
                        if($scope.flag == "address"){
                            $rootScope.showAlert("添加成功");
                            window.history.go(-1);
                        }else{
                            $location.url("/tab/address?uid=" + $scope.uid);
                        }

                    } else {
                        $scope.isSave = false;
                        $rootScope.showAlert(response.message);
                    }
                    hideLoading();
                })
                .error(function (response, status, headers, config) {
                    $scope.isSave = false;
                    hideLoading();
                });
        };

        $scope.showLocation();
    })

});