define(['./module','./indexCtrl'], function (controllers) {
    'use strict';
    //晒单详情
    controllers.controller('shareDetailCtrl', function ($scope, $http, $location, $rootScope) {
        document.title = "晒单详情";
        $('body').scrollTop(0);
        $("#headerId").hide();
        $("#body_top").css('height','0px');
        $("#shopList").hide();
        $scope.backTo = function(){
            window.history.go(-1);
        };

        setTimeout(function(){
            $("a").on("touchstart", function(e){
                $(e.target).addClass('box-shadow-style');
            });
            $("a").on("touchmove", function(e){
                $(e.target).removeClass('box-shadow-style');
            });
            $("a").on("touchend", function(e){
                $(e.target).removeClass('box-shadow-style');
            });
        }, 500);

        $scope.sd_id = $location.search()['sd_id'];

        if($location.search()['come'] == "Android" || $location.search()['come'] == "IOS"){
            $scope.notNative = false;
            $("#footbar").hide();

            setTimeout(function(){
                connectWebViewJavascriptBridge(function(bridge) {
                    /* Init your app here */
                    bridge.init(function(message, responseCallback) {
                        if (responseCallback) {
                            responseCallback("Right back atcha")
                        }
                    });

                    var button = document.getElementById('native_user');
                    button.onclick = function(){
                        //test.preventDefault();
                        bridge.callHandler('goBackPersonalCenterVC', {'': ''}, function(response) {

                        })
                    };

                    var button1 = document.getElementById('native_product');
                    button1.onclick = function(){
                        //test.preventDefault();
                        bridge.callHandler('goToShangPinXqVC', {'productId': $scope.shareData.sd_shopid}, function(response) {

                        })
                    }
                });
            }, 500);

        }else{
            $scope.notNative = true;
            $("#shareDetailContent").css("margin-top", "45px");
        }

        $scope.get_shaidan_info = function () {
            showLoading();
            var partner = "ZLAPITOH5WAP";
            var timestamp = Date.parse(new Date())/1000;
            var sign = $.md5(partner + timestamp + key);
            $http({method: 'POST', data: {partner: partner, timestamp: timestamp, sign: sign, sd_id: $scope.sd_id}, url: "../index.php/yunapi/goods/get_shaidan_info"})
                .success(function (response, status, headers, config) {
                    if (response.status == "1") {
                        $scope.shareData = response.data;
                        setTimeout(function(){
                            $("a").on("touchstart", function(e){
                                $(e.target).addClass('box-shadow-style');
                            });
                            $("a").on("touchmove", function(e){
                                $(e.target).removeClass('box-shadow-style');
                            });
                            $("a").on("touchend", function(e){
                                $(e.target).removeClass('box-shadow-style');
                            });
                        }, 500);
                    } else {
                        $rootScope.showAlert(response.message);
                    }
                    hideLoading();
                })
                .error(function (response, status, headers, config) {
                    hideLoading();
                });
        };
        $scope.get_shaidan_info();
    })


});