/**
 * Created by Administrator on 2016/8/8.
 */
define(['./module','./indexCtrl'], function (controllers) {
    'use strict';
    //收货地址页面
    controllers.controller('addressCtrl', function ($scope, $http, $location, $rootScope) {
        document.title = "地址管理";
        $('body').scrollTop(0);
        $("#headerId").hide();
        $("#body_top").css('height','0px');
        $("#shopList").hide();
        $scope.backTo = function(){
            $location.url("/tab/personalInfo?uid=" + $scope.uid);
        };
        setTimeout(function(){
            $("a").on("touchstart", function(e){
                $(e.target).addClass('box-shadow-style');
            });
            $("a").on("touchmove", function(e){
                $(e.target).removeClass('box-shadow-style');
            });
            $("a").on("touchend", function(e){
                $(e.target).removeClass('box-shadow-style');
            });
        }, 500);

        $scope.noData = false;

        $scope.addAddress = function(){
            if($scope.userAddress.length > 4){
                $rootScope.showAlert("最多只能添加5条地址");
                return;
            }
            $location.url("/tab/addAddress?uid=" + $scope.uid);
        };

        $scope.uid = $location.search()['uid'];
        $scope.userAddress = [];

        $scope.get_member_address = function () {
            showLoading();
            var partner = "ZLAPITOH5WAP";
            var timestamp = Date.parse(new Date())/1000;
            var sign = $.md5(partner + timestamp + key);
            $http({method: 'POST', data: {partner: partner, timestamp: timestamp, sign: sign, uid: $scope.uid}, url: "../index.php/yunapi/member/get_member_address"})
                .success(function (response, status, headers, config) {
                    if (response.status == "1") {
                        if(response.data){
                            $scope.userAddress = response.data;
                        }
                        if($scope.userAddress.length == 0){
                            $scope.noData = true;
                        }
                    } else {
                        $rootScope.showAlert(response.message);
                    }
                    hideLoading();
                })
                .error(function (response, status, headers, config) {
                    hideLoading();
                });
        };

        $scope.get_member_address();
    })

});