/**
 * Created by Administrator on 2016/8/8.
 */
define(['./module','./indexCtrl'], function (controllers) {
    'use strict';
    //个人中心页
    controllers.controller('personCenterCtrl', function ($scope, $http, $location, $rootScope) {

        document.title = "个人中心";
        $('body').scrollTop(0);
        $("#headerId").hide();
        $("#body_top").css('height','0px');
        $("#shopList").hide();
        $scope.backTo = function(){
            window.history.go(-1);
        };
        console.log($rootScope.holdUID);
        $scope.uid = $rootScope.holdUID;
        //$scope.uid = $location.search()['uid'];
        if($scope.uid > 0){
            console.log($scope.uid);
        }else{
            window.location.href = "http://www.yz-db.com/yungou/index.html#/tab/home";
        }




        var type = $location.search()['type'];
        $scope.isHost = false;
        $scope.exit = function(){
            delCookie("holdUID");
            $rootScope.holdUID = "";
            //$rootScope.showAlert("退出成功");
            window.location.href = "http://www.yz-db.com/yungou/index.html#/tab/home";
        };

        setTimeout(function(){
            $("a").on("touchstart", function(e){
                $(e.target).addClass('box-shadow-style');
            });
            $("a").on("touchmove", function(e){
                $(e.target).removeClass('box-shadow-style');
            });
            $("a").on("touchend", function(e){
                $(e.target).removeClass('box-shadow-style');
            });
        }, 500);

        $scope.get_member_info = function () {
            showLoading();
            var partner = "ZLAPITOH5WAP";
            var timestamp = Date.parse(new Date())/1000;
            var sign = $.md5(partner + timestamp + key);

            $http({method: 'POST', data: {partner: partner, timestamp: timestamp, sign: sign, uid: $scope.uid}, url: "../index.php/yunapi/member/get_member_info"})
                .success(function (response, status, headers, config) {
                    if (response.status == "1") {
                        $scope.userInfo = response.data;
                        console.log($scope.userInfo);
                        setTimeout(function(){
                            $("a").on("touchstart", function(e){
                                $(e.target).addClass('box-shadow-style');
                            });
                            $("a").on("touchmove", function(e){
                                $(e.target).removeClass('box-shadow-style');
                            });
                            $("a").on("touchend", function(e){
                                $(e.target).removeClass('box-shadow-style');
                            });
                        }, 500);
                    } else {
                        $rootScope.showAlert(response.message);
                    }
                    hideLoading();
                })
                .error(function (response, status, headers, config) {
                    hideLoading();
                });
        };
        $scope.get_member_center = function () {
            showLoading();
            var partner = "ZLAPITOH5WAP";
            var timestamp = Date.parse(new Date())/1000;
            var sign = $.md5(partner + timestamp + key);
            $http({method: 'POST', data: {partner: partner, timestamp: timestamp, sign: sign, uid: $scope.uid}, url: "../index.php/yunapi/member/get_member_center"})
                .success(function (response, status, headers, config) {
                    if (response.status == "1") {
                        $scope.userInfo = response.data;

                        setTimeout(function(){
                            $("a").on("touchstart", function(e){
                                $(e.target).addClass('box-shadow-style');
                            });
                            $("a").on("touchmove", function(e){
                                $(e.target).removeClass('box-shadow-style');
                            });
                            $("a").on("touchend", function(e){
                                $(e.target).removeClass('box-shadow-style');
                            });
                        }, 500);
                    } else {
                        $rootScope.showAlert(response.message);
                    }
                    hideLoading();
                })
                .error(function (response, status, headers, config) {
                    hideLoading();
                });
        };



        if($scope.uid != $rootScope.holdUID){
            $scope.isHost = false;
            $scope.get_member_center();
        }else{
            $scope.isHost = true;
            if (type == 'other') {
                $scope.isHost = false;
            };

            $scope.get_member_info();
        }
    })

});