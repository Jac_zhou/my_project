/**
 * Created by Administrator on 2016/8/8.
 */
define(['./module','./indexCtrl'], function (controllers) {
    'use strict';
    //App计算结果页
    controllers.controller('appCalculationCtrl', function ($scope, $http, $location, $rootScope) {  //APP端计算详情
        document.title = "计算结果";
        $('body').scrollTop(0);
        $("#headerId").hide();
        $("#body_top").css('height','0px');
        $("#shopList").hide();
        $("#footbar").hide();	//不显示底部广告
        $scope.backTo = function(){
            window.history.go(-1);
        };
        $scope.isUp = true;
        $scope.setUpDown = function(){
            if($scope.isUp){
                $scope.isUp = false;
            }else{
                $scope.isUp = true;
            }
        }
        setTimeout(function(){
            $("a").on("touchstart", function(e){
                $(e.target).addClass('box-shadow-style');
            });
            $("a").on("touchmove", function(e){
                $(e.target).removeClass('box-shadow-style');
            });
            $("a").on("touchend", function(e){
                $(e.target).removeClass('box-shadow-style');
            });
        }, 500);

        $scope.id = $location.search()['id'];

        $scope.get_calc_details = function() {
            showLoading();
            var partner = "ZLAPITOH5WAP";
            var timestamp = Date.parse(new Date())/1000;
            var sign = $.md5(partner + timestamp + key);
            $http({method: 'POST', data: {partner: partner, timestamp: timestamp, sign: sign, id: $scope.id}, url: "../index.php/yunapi/goods/get_calc_details"})
                .success(function (response, status, headers, config) {
                    if (response.status == "1") {
                        $scope.calData = response.data;
                        setTimeout(function(){
                            $("a").on("touchstart", function(e){
                                $(e.target).addClass('box-shadow-style');
                            });
                            $("a").on("touchmove", function(e){
                                $(e.target).removeClass('box-shadow-style');
                            });
                            $("a").on("touchend", function(e){
                                $(e.target).removeClass('box-shadow-style');
                            });
                        }, 500);
                    } else {
                        $rootScope.showAlert(response.message);
                    }
                    hideLoading();
                })
                .error(function (response, status, headers, config) {
                    hideLoading();
                });
        };
        $scope.get_calc_details();

    })

});
