/**
 * Created by Administrator on 2016/8/8.
 */
define(['./module','./indexCtrl'], function (controllers) {
    'use strict';
    //服务条款
    controllers.controller('termOfServiceCtrl', function ($scope, $http, $location, $rootScope) {		//ios 服务条款
        document.title = "服务条款";
        $('body').scrollTop(0);
        $("#headerId").hide();
        $("#body_top").css('height','0px');
        $("#shopList").hide();
        $scope.backTo = function(){
            window.history.go(-1);
        };
        $("#footbar").hide();
    });

});
