/**
 * Created by Administrator on 2016/8/8.
 */
define(['./module','./indexCtrl'], function (controllers) {
    'use strict';
    //支付结果
    controllers.controller('payResultCtrl', function ($scope, $http, $location, $rootScope) {
        document.title = "支付结果";
        $('body').scrollTop(0);
        $("#headerId").hide();
        $("#body_top").css('height','0px');
        $("#shopList").hide();
        $scope.backTo = function(){
            $location.url("/tab/home");
        };
        setTimeout(function(){
            $("a").on("touchstart", function(e){
                $(e.target).addClass('box-shadow-style');
            });
            $("a").on("touchmove", function(e){
                $(e.target).removeClass('box-shadow-style');
            });
            $("a").on("touchend", function(e){
                $(e.target).removeClass('box-shadow-style');
            });
        }, 500);

        $scope.isFaild = false;
        $scope.isSuccess = false;

        $scope.type = $location.search()['type'];
        $scope.is_yue = $location.search()['is_yue'];
        $scope.code = $location.search()['code'];

        $scope.wap_do_pay = function(type, isYue) {
            var partner = "ZLAPITOH5WAP";
            var timestamp = Date.parse(new Date())/1000;
            var sign = $.md5(partner + timestamp + key);
            var info = "";
            for(var i = 0; i < $rootScope.shopList.length; i++){
                info += $rootScope.shopList[i].id + "," + $rootScope.shopList[i].num + "|";
            }
            if(info != ""){
                info = info.substring(0, info.length - 1);
            }

            console.log(info);
            console.log($scope.is_yue);
            console.log($scope.code);


            showLoading();
            if(type == 'wxpay'){
                var url = "../index.php/yunapi/cart/wap_do_pay?data=";
                var ss = '{"partner":"'+partner+'","timestamp":"'+timestamp+'","sign":"'+sign+'","uid":"'+$rootScope.holdUID+'","info":"'+info+'","pay_type":"'+type+'","is_yue":"'+isYue+'"}';
                url = url + ss;
                window.location.href = url;
                delCookie("shopList");
                return false;

            }

            $http({method: 'POST', data: {partner: partner, timestamp: timestamp, sign: sign, uid: $rootScope.holdUID, info: info, pay_type: type, is_yue: isYue}, url: "../index.php/yunapi/cart/wap_do_pay"})
                .success(function (response, status, headers, config) {
                    /*var str = JSON.stringify(response);
                     console.log(str);
                     return false;*/
                    if (response.status == "1") {
                        if(response.status_type == "2"){
                            if (type == 'alipay') {
                                $("#resultPay").html(response.data);
                            }else{
                                window.location.href=response.data;
                            }
                            /*$("#resultPay").html(response.data);
                             setTimeout(function(){
                             document.forms['alipaysubmit'].submit();
                             }, 500);*/

                        }else if(response.status_type == "1"){
                            $rootScope.shopList = [];
                            delCookie("shopList");
                            $scope.isSuccess = true;
                            $scope.resultData = response.data;
                        }
                        setTimeout(function(){
                            $("a").on("touchstart", function(e){
                                $(e.target).addClass('box-shadow-style');
                            });
                            $("a").on("touchmove", function(e){
                                $(e.target).removeClass('box-shadow-style');
                            });
                            $("a").on("touchend", function(e){
                                $(e.target).removeClass('box-shadow-style');
                            });
                        }, 500);
                    } else if(response.status == "2") {
                        if (response.status_type == "1") {
                            $rootScope.shopList = [];
                            $scope.isChongSuccess = true;
                            $rootScope.showAlert(response.message);
                        }else{
                            $rootScope.shopList = [];
                            $scope.isChongFaild = true;
                            $rootScope.showAlert(response.message);
                        }
                    } else {
                        $rootScope.shopList = [];
                        delCookie("shopList");
                        $scope.isFaild = true;
                        $rootScope.showAlert(response.message);
                    }
                    hideLoading();
                })
                .error(function (response, status, headers, config) {
                    hideLoading();
                });
        };

        $scope.do_get_shoplist = function() {
            showLoading();
            var partner = "ZLAPITOH5WAP";
            var timestamp = Date.parse(new Date())/1000;
            var sign = $.md5(partner + timestamp + key);
            $http({method: 'POST', data: {partner: partner, timestamp: timestamp, sign: sign, code: $scope.code, uid: $rootScope.holdUID}, url: "../index.php/yunapi/cart/do_get_shoplist"})
                .success(function (response, status, headers, config) {
                    $rootScope.shopList = [];
                    delCookie("shopList");
                    if (response.status == "1") {
                        $scope.isSuccess = true;
                        $scope.resultData = response.data;
                        setTimeout(function(){
                            $("a").on("touchstart", function(e){
                                $(e.target).addClass('box-shadow-style');
                            });
                            $("a").on("touchmove", function(e){
                                $(e.target).removeClass('box-shadow-style');
                            });
                            $("a").on("touchend", function(e){
                                $(e.target).removeClass('box-shadow-style');
                            });
                        }, 500);
                    } else {
                        $rootScope.showAlert(response.message);
                    }
                    hideLoading();
                })
                .error(function (response, status, headers, config) {
                    hideLoading();
                });
        };

        if(isNull($scope.code)){
            $scope.wap_do_pay($scope.type, $scope.is_yue);
        }else if($scope.code == 0){
            $rootScope.shopList = [];
            delCookie("shopList");
            $scope.isFaild = true;
        }else{
            $scope.do_get_shoplist();
        }

    })

});
