/** attach controllers to this module 
 * if you get 'unknown {x}Provider' errors from angular, be sure they are
 * properly referenced in one of the module dependencies in the array.
 * below, you can see we bring in our services and constants modules 
 * which avails each controller of, for example, the `config` constants object.
 **/
//加载所有控制器
define([
    './indexCtrl',                      //公用控制器
    './my-ctrl-1',
    './my-ctrl-2',
    './xiangouCtrl',
    './homeCtrl',
    './listCtrl',
    './shareCtrl',
    './productDetailCtrl',
    './gorecordeCtrl',
    './productInfoCtrl',
    './qishuListCtrl',
    './passedPublishCtrl',
    './productShareCtrl',
    './loginCtrl',
    './updatePasswordCtrl',
    './rigisterCtrl',
    './bindwxCtrl',
    './searchCtrl',
    './searchResultCtrl',
    './personCenterCtrl',
    './rechargeRecordCtrl',
    './winningRecordCtrl',
    './snatchRecordCtrl',
    './rechargeCtrl',
    './personalInfoCtrl',
    './addressCtrl',
    './addAddressCtrl',
    './updateNicknameCtrl',
    './updatePhoneCtrl',
    './shareDetailCtrl',
    './shopListCtrl',
    './payCtrl',
    './calculationCtrl',
    './payResultStateCtrl',
    './payReCtrl',
    './payResultCtrl',
    './winningDetailCtrl',
    './updateAddressCtrl',
    './appProductInfoCtrl',
    './appCalculationCtrl',
    './rechargeResultCtrl',
    './agreementCtrl',
    './questionCtrl',
    './friendRebateCtrl',
    './inviteCtrl',
    './notificationCtrl',
    './discoverCtrl',
    './articleInfoCtrl',
    './aboutCtrl','./privacyCtrl',
    './protectCtrl',
    './termOfServiceCtrl',


], function () {});
