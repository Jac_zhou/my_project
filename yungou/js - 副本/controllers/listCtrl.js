/**
 * Created by Administrator on 2016/8/8.
 */
define(['./module','./indexCtrl'], function (controllers) {
    'use strict';
    //全部商品
    controllers.controller('listCtrl', function ($scope, $http, $rootScope, $location) {
        document.title = "易中夺宝-好运连连";
        $("#headerId").show();
        $("#shopList").show();

        $scope.$emit("itemChange", 2);

        $scope.noData = false;

        $scope.isSelected = -1;
        $scope.isSelected1 = true;
        $scope.isSelected2 = false;
        $scope.isSelected3 = false;
        $scope.isSelected4 = false;
        $scope.isShow = false;
        $scope.typeList = [];
        $scope.displayType = "商品分类";
        $scope.typeId = 0;
        // $scope.type = "renqi";
        if($location.search()['flag'] == "zuixin"){
            $scope.isSelected1 = false;
            $scope.isSelected2 = true;
            $scope.isSelected3 = false;
            $scope.isSelected4 = false;
            $scope.type = "zuixin";
        }
        if($location.search()['flag'] == "zuixin"){
            $scope.isSelected1 = false;
            $scope.isSelected2 = true;
            $scope.isSelected3 = false;
            $scope.isSelected4 = false;
            $scope.type = "zuixin";
        }

        $scope.page = 1;
        $scope.size = 10;

        $scope.mScroll = "";
        $scope.isPullUp = false;
        $scope.haveMore = false;
        $scope.initIScroll = function(){
            if($scope.mScroll instanceof iScroll){
                $scope.mScroll.destroy();
            }
            $("#scroller").height($(window).height() - 120);

            $scope.mScroll = new iScroll("scroller",{
                hScrollbar:false,
                vScrollbar:false,
                onScrollMove: function(){
                    if(this.y <= (this.maxScrollY - 60)){
                        $scope.isPullUp = true;
                    }
                },
                onScrollEnd: function(){
                    if($scope.isPullUp && $scope.haveMore){
                        $scope.page++;
                        $scope.isPullUp = false;
                        $scope.get_cate_goods_list($scope.page, $scope.size, $scope.type, $scope.typeId);
                    }
                    if(this.y < -800){
                        $('#proId').show();
                    }else{
                        $('#proId').hide();
                    }
                }
            });
        };
        $('#proId').click(function(){
            $scope.mScroll.scrollTo(0, 0, 0);
        });

        $scope.chooseType = function(num, name, typeId){
            $scope.isSelected = num;
            $scope.isShow = false;
            $scope.displayType = name;
            $scope.typeId = typeId;
            $scope.get_cate_goods_list(1, 10, $scope.type, $scope.typeId);
        };
        $scope.openMenuList = function(){
            if($scope.isShow){
                $scope.isShow = false;
            } else {
                $scope.isShow = true;
            }
        };
        $scope.setSelectedItem = function(num){
            if(num == 1){
                $scope.isSelected1 = true;
                $scope.isSelected2 = false;
                $scope.isSelected3 = false;
                $scope.isSelected4 = false;
                $scope.type = "renqi";
            } else if(num == 2){
                $scope.isSelected1 = false;
                $scope.isSelected2 = true;
                $scope.isSelected3 = false;
                $scope.isSelected4 = false;
                $scope.type = "zuixin";
            } else if(num == 3){
                $scope.isSelected1 = false;
                $scope.isSelected2 = false;
                $scope.isSelected3 = true;
                $scope.isSelected4 = false;
                $scope.type = "shenyurenshu";
            } else if(num == 4){
                $scope.isSelected1 = false;
                $scope.isSelected2 = false;
                $scope.isSelected3 = false;
                $scope.isSelected4 = true;
                $scope.type = "zongxurenci";
            }
            $scope.page = 1;
            $scope.get_cate_goods_list($scope.page, 10, $scope.type, $scope.typeId);
        };

        $scope.goodsList = [];

        $scope.get_cate_goods_list = function (page, size, type, cateid) {
            showLoading();
            var partner = "ZLAPITOH5WAP";
            var timestamp = Date.parse(new Date())/1000;
            var sign = $.md5(partner + timestamp + key);
            //$scope.goodsList = [];
            $http({method: 'POST', data: {partner: partner, timestamp: timestamp, sign: sign, page: page, size: size, type: type, cateid: cateid,uid:$rootScope.holdUID}, url: "../index.php/yunapi/index/get_cate_goods_list"})
                .success(function (response, status, headers, config) {
                    /*var str = JSON.stringify(response);
                     $rootScope.showAlert(str);*/
                    if (response.status == "1") {
                        if($scope.page == 1){
                            $scope.goodsList = response.data.list;
                            if($scope.goodsList.length == 0){
                                $scope.noData = true;
                            }
                            setTimeout(function(){$scope.initIScroll();},500);
                        } else {
                            $scope.goodsList = $scope.goodsList.concat(response.data.list);
                            setTimeout(function(){$scope.mScroll.refresh();},500);
                        }
                        if(response.data.list.length == $scope.size){
                            $scope.haveMore = true;
                        } else {
                            $scope.haveMore = false;
                        }
                        setTimeout(function(){
                            $("a").on("touchstart", function(e){
                                $(e.target).addClass('box-shadow-style');
                            });
                            $("a").on("touchmove", function(e){
                                $(e.target).removeClass('box-shadow-style');
                            });
                            $("a").on("touchend", function(e){
                                $(e.target).removeClass('box-shadow-style');
                            });
                        }, 500);
                    } else {
                        $rootScope.showAlert(response.message);
                    }
                    hideLoading();
                })
                .error(function (response, status, headers, config) {
                    hideLoading();
                });
        };
        $scope.cateid = $location.search()['cateid'];
        $scope.typeList = [];
        $scope.get_category_list = function () {
            showLoading();
            var partner = "ZLAPITOH5WAP";
            var timestamp = Date.parse(new Date())/1000;
            var sign = $.md5(partner + timestamp + key);
            $http({method: 'POST', data: {partner: partner, timestamp: timestamp, sign: sign}, url: "../index.php/yunapi/index/get_category_list"})
                .success(function (response, status, headers, config) {
                    if (response.status == "1") {
                        $scope.typeList = response.data;
                        if(!isNull($scope.cateid)){
                            for(var i = 0; i < $scope.typeList.length; i++){
                                if($scope.typeList[i].cateid == $scope.cateid){
                                    $scope.displayType = $scope.typeList[i].name;
                                    $scope.isSelected = i;
                                    break;
                                }
                            }
                            $scope.typeId = $scope.cateid;
                        }
                        $scope.get_cate_goods_list(1, 10, $scope.type, $scope.typeId);
                        setTimeout(function(){
                            $("a").on("touchstart", function(e){
                                $(e.target).addClass('box-shadow-style');
                            });
                            $("a").on("touchmove", function(e){
                                $(e.target).removeClass('box-shadow-style');
                            });
                            $("a").on("touchend", function(e){
                                $(e.target).removeClass('box-shadow-style');
                            });
                        }, 500);
                    } else {
                        $rootScope.showAlert(response.message);
                    }
                    hideLoading();
                })
                .error(function (response, status, headers, config) {
                    hideLoading();
                });
        };
        $scope.get_category_list();

    })

});