/**
 * Created by Administrator on 2016/8/8.
 */
define(['./module','./indexCtrl'], function (controllers) {
    'use strict';
    //商品细节页
    controllers.controller('productDetailCtrl', function ($scope, $http, $location, $rootScope) {
        document.title = "商品详情";
        $('body').scrollTop(0);
        $("#headerId").hide();
        $("#body_top").css('height','0px');
        $("#shopList").hide();
        $scope.notLogin = false;			//无法登陆
        $scope.notJoin = false;				//没有参加
        $scope.isJoin = false;				//已参加
        $scope.havaWinner = false;			//是否揭晓出获奖者
        $scope.canBuy = true;				//是否可以购买

        $scope.id = $location.search()['id'];				//本期产品的id
        /*$scope.get_time = $location.search()['endTime'];
         if(!isNull($scope.get_time)){
         $scope.endTime =$scope.date_time($scope.get_time)*1000;//本期产品的结束时间   格式:  2016/06/17 11:05:02  转换成精确到毫秒的时间戳
         }*/

        $scope.uid = 0;					//默认用户没有登陆
        $scope.notLogin = true;
        if(!isNull($rootScope.holdUID)){
            $scope.uid = $rootScope.holdUID;
            $scope.notLogin = false;
        }

        $scope.backTo = function(){
            window.history.go(-1);
        };
        //跳转到获取   云购码的页面
        $scope.gorecorde = function(gid,winUid,qishu,user_code){
            //console.log(gid,winUid,qishu,user_code);
            if(gid>0){
                var url='/tab/gorecorde?gid='+gid+'&uid='+winUid+'&qishu='+qishu+'&user_code='+user_code;
                $location.url(url);
                return false;
            }
        };
        $scope.showAll = function(code){
            $rootScope.showAlert($scope.allCode, 0, code);
        };

        $("#footbar").hide();
        $("#shopList").css("bottom", "10px");

        setTimeout(function(){
            $("a").on("touchstart", function(e){
                $(e.target).addClass('box-shadow-style');
            });
            $("a").on("touchmove", function(e){
                $(e.target).removeClass('box-shadow-style');
            });
            $("a").on("touchend", function(e){
                $(e.target).removeClass('box-shadow-style');
            });
        }, 500);

        $scope.noData = false;				//是否有参与记录

        $scope.page = 1;					//默认获取的参与人信息页码
        $scope.size = 10;					//默认获取的参与人信息条数
        $scope.mScroll = "";
        $scope.isPullUp = false;
        $scope.haveMore = false;
        $scope.isTime = false;				//是否倒计时
        $scope.initIScroll = function(){
            if($scope.mScroll instanceof iScroll){
                $scope.mScroll.destroy();
            }
            $("#scroller_product_detail").height($(window).height() - 90);

            $scope.mScroll = new iScroll("scroller_product_detail",{
                hScrollbar:false,
                vScrollbar:false,
                onScrollMove: function(){
                    if(this.y <= (this.maxScrollY - 60)){
                        $scope.isPullUp = true;
                    }
                },
                onScrollEnd: function(){
                    if($scope.isPullUp && $scope.haveMore){
                        $scope.page++;
                        $scope.isPullUp = false;
                        $scope.get_record_list($scope.page, $scope.size);
                    }
                    if(this.y < -800){
                        $('#proId').show();
                    }else{
                        $('#proId').hide();
                    }
                }
            });
        };
        $('#proId').click(function(){
            $scope.mScroll.scrollTo(0, 0, 0);
        });

        $scope.imgTouchMove = function(){
            var slidePage = {};
            $('#carousel').on('touchstart', function(e) {
                var touch = e.originalEvent.targetTouches[0];
                slidePage.x = touch.pageX;
            });
            $('#carousel').on('touchend', function(e) {
                var touch = e.originalEvent.changedTouches[0];
                var x = touch.pageX;
                if(x - parseInt(slidePage.x) > 0){
                    //从左往右
                    $('#carousel').carousel('prev');
                } else {
                    //从右往左
                    $('#carousel').carousel('next');
                }
            });
        };

        $scope.get_win_member_info = function(e){
            if($scope.status_public==1){
                return false;
            }
            var partner = "ZLAPITOH5WAP";
            var timestamp = Date.parse(new Date())/1000;
            var sign = $.md5(partner + timestamp + key);
            $http({method: 'POST', data: {partner: partner, timestamp: timestamp, sign: sign, id: $scope.id}, url: "../index.php/yunapi/goods/get_win_member_info"})
                .success(function (response, status, headers, config) {
                    console.log(response);
                    console.log('kk');
                    if (response.status == "1") {
                        $scope.isTime = false;
                        $scope.havaWinner = true;
                        $scope.waiting = false;
                        $scope.status_public=1;
                        $scope.goodsInfo.q_user_code = response.data.q_user_code;
                        $scope.goodsInfo.q_user = response.data;
                        $scope.winUid = response.data.uid;
                    } else {

                    }
                })
                .error(function (response, status, headers, config) {

                });

        };
        //$scope.isFirst = true;

        //获取商品信息

        $scope.someCode = [];
        $scope.allCode = "";			//所有的云购买
        $scope.waiting = true;

        $scope.status_public=0;			//当前模块的全局变量  用来停止由于循环查询导致的无限查询

        $scope.get_goods_info = function () {
            showLoading();
            var partner = "ZLAPITOH5WAP";
            var timestamp = Date.parse(new Date())/1000;
            var sign = $.md5(partner + timestamp + key);
            $http({method: 'POST', data: {partner: partner, timestamp: timestamp, sign: sign, id: $scope.id, uid: $scope.uid}, url: "../index.php/yunapi/goods/get_goods_info"})
                .success(function (response, status, headers, config) {

                    //console.log(response);
                    if (response.status == "1") {
                        //获取前后几期列表
                        $scope.qishuList = response.qishu;
                        $scope.nowqishu = function(qishu){
                            if(qishu == $scope.id){
                                return 'now';
                            }
                        };
                        //商品信息
                        $scope.goodsInfo = response.data;
                        //第一期商品ID
                        $scope.sid 	= response.data['sid'];
                        //默认当前商品没有中奖人
                        $scope.havaWinner = false;
                        $scope.canBuy = false;			//默认当前产品不可以可以购买
                        $scope.isTime = false;			//默认不显示倒计时
                        $scope.waiting= false;			//默认不需要等待
                        //$scope.goodsInfo.tag判断当前商品是否已经揭晓
                        if($scope.goodsInfo.tag == 0){
                            $scope.tag = "已揭晓";
                            $scope.havaWinner = true;
                            $scope.winUid = response.data.q_user.uid;
                            $scope.user_code = $scope.goodsInfo['q_user_code'];
                            $scope.status_public=1;
                        } else if($scope.goodsInfo.tag == 1){
                            $scope.tag = "进行中";
                            $scope.canBuy = true;
                            $("#shopList").show();
                            $scope.status_public=1;
                        } else if($scope.goodsInfo.tag == 2){
                            $scope.tag = "倒计时";
                            $scope.isTime = true;
                            $scope.waiting = true;
                            $scope.test_time_sec = response.data.test_time;					//倒计时的  【秒数】
                            var test_time_msec = (new Date().getTime())+(parseInt($scope.test_time_sec))*1000;//倒计时时间戳 毫秒
                            //console.log($scope.test_time_sec);
                            setTimeout(function(){
                                //每隔两秒查询一次，如果有结果就停止
                                var chaxun = setInterval(function(){
                                    if($scope.status_public==1){
                                        clearInterval(chaxun);
                                    }
                                    $scope.get_win_member_info();
                                },2000);
                            },$scope.test_time_sec*1000);
                            var set_time_out = setTimeout(function(){
                                //console.log('倒计时时间戳：'+test_time_msec);
                                $("#fnTimeCountDown").fnTimeCountDown(test_time_msec,function(){});
                            },500);

                        } else if($scope.goodsInfo.tag == 3){
                            $scope.tag = "期满";
                        }
                        if($scope.goodsInfo.curr_uinfo.is_join == 0){
                            if(!$scope.notLogin){
                                //参与本期
                                $scope.notJoin = true;
                            }
                        } else if($scope.goodsInfo.curr_uinfo.is_join == 1){
                            //参与本期
                            $scope.isJoin = true;
                            $scope.someCode = $scope.goodsInfo.curr_uinfo.goucode.split(",");
                            $scope.allCode = $scope.goodsInfo.curr_uinfo.goucode_all;
                        }
                        //当前期数
                        $scope.qishu = response.data.qishu;
                        //获取本期商品的所有参与记录
                        $scope.get_record_list(1, 10);
                        setTimeout(function(){
                            $("a").on("touchstart", function(e){
                                $(e.target).addClass('box-shadow-style');
                            });
                            $("a").on("touchmove", function(e){
                                $(e.target).removeClass('box-shadow-style');
                            });
                            $("a").on("touchend", function(e){
                                $(e.target).removeClass('box-shadow-style');
                            });
                        }, 500);
                        hideLoading();
                    } else {
                        hideLoading();
                        $rootScope.showAlert(response.message);
                    }
                })
                .error(function (response, status, headers, config) {
                    hideLoading();
                });

        };

        $scope.recordList = [];
        //获取所有的参与记录
        $scope.get_record_list = function (page, size) {
            showLoading();
            var partner = "ZLAPITOH5WAP";
            var timestamp = Date.parse(new Date())/1000;
            var sign = $.md5(partner + timestamp + key);
            $http({method: 'POST', data: {partner: partner, timestamp: timestamp, sign: sign, shopid: $scope.id, qishu: $scope.qishu, page: page, size: size}, url: "../index.php/yunapi/goods/get_record_list"})
                .success(function (response, status, headers, config) {
                    /*var str = JSON.stringify(response);
                     console.log("------------>", str);*/
                    if (response.status == "1") {
                        if($scope.page == 1){
                            if(response.data.list){
                                $scope.recordList = response.data.list;
                                if($scope.recordList.length == 0){
                                    $scope.noData = true;
                                }
                            }else{
                                $scope.noData = true;
                            }
                            setTimeout(function(){$scope.initIScroll();},500);
                        } else {
                            $scope.recordList = $scope.recordList.concat(response.data.list);
                            setTimeout(function(){$scope.mScroll.refresh();},500);
                        }
                        if(response.data.list && response.data.list.length == $scope.size){
                            $scope.haveMore = true;
                        } else {
                            $scope.haveMore = false;
                        }
                        setTimeout(function(){
                            $("a").on("touchstart", function(e){
                                $(e.target).addClass('box-shadow-style');
                            });
                            $("a").on("touchmove", function(e){
                                $(e.target).removeClass('box-shadow-style');
                            });
                            $("a").on("touchend", function(e){
                                $(e.target).removeClass('box-shadow-style');
                            });
                        }, 500);
                    } else {
                        $rootScope.showAlert(response.message);
                    }
                    hideLoading();
                })
                .error(function (response, status, headers, config) {
                    hideLoading();
                });
        };
        $scope.get_more_number = function (code) {
            showLoading();
            var partner = "ZLAPITOH5WAP";
            var timestamp = Date.parse(new Date())/1000;
            var sign = $.md5(partner + timestamp + key);
            $http({method: 'POST', data: {partner: partner, timestamp: timestamp, sign: sign, id: $scope.id, uid: $scope.winUid, qishu: $scope.qishu}, url: "../index.php/yunapi/goods/get_more_number"})
                .success(function (response, status, headers, config) {
                    /*var str = JSON.stringify(response);
                     $rootScope.showAlert(str);*/
                    if (response.status == "1") {
                        $rootScope.showAlert(response.data.goucode, 0, code);
                        setTimeout(function(){
                            $("a").on("touchstart", function(e){
                                $(e.target).addClass('box-shadow-style');
                            });
                            $("a").on("touchmove", function(e){
                                $(e.target).removeClass('box-shadow-style');
                            });
                            $("a").on("touchend", function(e){
                                $(e.target).removeClass('box-shadow-style');
                            });
                        }, 500);
                    } else {
                        $rootScope.showAlert(response.message);
                    }
                    hideLoading();
                })
                .error(function (response, status, headers, config) {
                    hideLoading();
                });
        };
        $scope.imgTouchMove();
        $scope.get_goods_info();

    })

});
