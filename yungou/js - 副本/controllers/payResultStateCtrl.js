/**
 * Created by Administrator on 2016/8/8.
 */
define(['./module','./indexCtrl'], function (controllers) {
    'use strict';
    // 支付结果
    controllers.controller("payResultStateCtrl",function ($scope, $http, $location, $rootScope) {
        document.title = "支付结果";
        $('body').scrollTop(0);
        $("#headerId").hide();
        $("#body_top").css('height','0px');
        $("#shopList").hide();
        $scope.backTo = function(){
            $location.url("/tab/home");
        };
        setTimeout(function(){
            $("a").on("touchstart", function(e){
                $(e.target).addClass('box-shadow-style');
            });
            $("a").on("touchmove", function(e){
                $(e.target).removeClass('box-shadow-style');
            });
            $("a").on("touchend", function(e){
                $(e.target).removeClass('box-shadow-style');
            });
        }, 500);

        $scope.isFaild = false;
        $scope.isSuccess = false;
        $scope.isChongSuccess = false;
        $scope.isChongFaild = false;

        $scope.type = $location.search()['type'];
        $scope.code = $location.search()['code'];
        uid = $rootScope.holdUID;
        $scope.get_pay_state = function() {
            $http({method: 'POST', data: {code: $scope.code, uid: $rootScope.holdUID}, url: "../index.php/yunapi/cart/get_pay_state2"})
                .success(function (response, status, headers, config) {
                    //$rootScope.showAlert(response.status_type);  1--支付   2---充值
                    if (response.status == 1) {
                        if (response.pay_state == 1) {
                            $rootScope.shopList = [];
                            delCookie("shopList");
                            $scope.isSuccess = true;
                            $rootScope.showAlert(response.message);
                            $scope.resultData = response.data;
                        }else {
                            $rootScope.shopList = [];
                            $scope.isFaild = true;
                            $rootScope.showAlert(response.message);
                        }
                    }else{
                        if (response.pay_state == 1) {
                            $rootScope.shopList = [];
                            $scope.isChongSuccess = true;
                            $rootScope.showAlert(response.message);
                        }else{
                            $rootScope.shopList = [];
                            $scope.isChongFaild = true;
                            $rootScope.showAlert(response.message);
                        }
                    }
                    hideLoading();
                })
                .error(function (response, status, headers, config) {
                    hideLoading();
                });
        }
        $scope.get_pay_state();
    })

});