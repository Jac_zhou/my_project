/**
 * Created by Administrator on 2016/8/8.
 */
define(['./module','./indexCtrl'], function (controllers) {
    'use strict';
    //APP商品详情页
    controllers.controller('appProductInfoCtrl', function ($scope, $http, $location, $rootScope) {		//APP端商品图文详情
        document.title = "商品详情";
        $('body').scrollTop(0);
        $("#headerId").hide();
        $("#body_top").css('height','0px');
        $("#shopList").hide();
        $("#footbar").hide();	//不显示底部广告
        $scope.backTo = function(){
            window.history.go(-1);
        };
        setTimeout(function(){
            $("a").on("touchstart", function(e){
                $(e.target).addClass('box-shadow-style');
            });
            $("a").on("touchmove", function(e){
                $(e.target).removeClass('box-shadow-style');
            });
            $("a").on("touchend", function(e){
                $(e.target).removeClass('box-shadow-style');
            });
        }, 500);

        $scope.id = $location.search()['id'];
        $scope.get_goods_pic_content = function () {
            showLoading();
            var partner = "ZLAPITOH5WAP";
            var timestamp = Date.parse(new Date())/1000;
            var sign = $.md5(partner + timestamp + key);
            $http({method: 'POST', data: {partner: partner, timestamp: timestamp, sign: sign, id: $scope.id}, url: "../index.php/yunapi/goods/get_goods_pic_content"})
                .success(function (response, status, headers, config) {
                    /*var str = JSON.stringify(response);
                     $rootScope.showAlert(str);*/
                    if (response.status == "1") {
                        $("#productInfoData").append(response.data.content);
                        setTimeout(function(){
                            $("a").on("touchstart", function(e){
                                $(e.target).addClass('box-shadow-style');
                            });
                            $("a").on("touchmove", function(e){
                                $(e.target).removeClass('box-shadow-style');
                            });
                            $("a").on("touchend", function(e){
                                $(e.target).removeClass('box-shadow-style');
                            });
                        }, 500);
                    } else {
                        $rootScope.showAlert(response.message);
                    }
                    hideLoading();
                })
                .error(function (response, status, headers, config) {
                    hideLoading();
                });
        };
        $scope.get_goods_pic_content();
    })

});
