/**
 * Created by Administrator on 2016/8/8.
 */
define(['./module','./indexCtrl'], function (controllers) {
    'use strict';
    //支付结果
    controllers.controller('rechargeResultCtrl', function ($scope, $http, $location, $rootScope) {
        document.title = "支付结果";
        $('body').scrollTop(0);
        $("#headerId").hide();
        $("#body_top").css('height','0px');
        $("#shopList").hide();
        $scope.backTo = function(){
            $location.url("/tab/home");
        };
        setTimeout(function(){
            $("a").on("touchstart", function(e){
                $(e.target).addClass('box-shadow-style');
            });
            $("a").on("touchmove", function(e){
                $(e.target).removeClass('box-shadow-style');
            });
            $("a").on("touchend", function(e){
                $(e.target).removeClass('box-shadow-style');
            });
        }, 500);

        $scope.isFaild = false;
        $scope.isSuccess = false;

        $scope.ledou = $location.search()['ledou'];
        if($scope.ledou == "-1"){
            $scope.isFaild = true;
        }else{
            $scope.isSuccess = true;
        }

    })

});