define(['./module','./indexCtrl'], function (controllers) {
    'use strict';
    //云购记录页面
    controllers.controller('snatchRecordCtrl', function ($scope, $http, $location, $rootScope) {
        document.title = "云购记录";
        $('body').scrollTop(0);
        $("#headerId").hide();
        $("#body_top").css('height','0px');
        $("#shopList").hide();

        $scope.noData = false;

        setTimeout(function(){
            $("a").on("touchstart", function(e){
                $(e.target).addClass('box-shadow-style');
            });
            $("a").on("touchmove", function(e){
                $(e.target).removeClass('box-shadow-style');
            });
            $("a").on("touchend", function(e){
                $(e.target).removeClass('box-shadow-style');
            });
        }, 500);

        $scope.page = 1;
        $scope.size = 10;
        $scope.mScroll = "";
        $scope.isPullUp = false;
        $scope.haveMore = false;
        $scope.initIScroll = function(){
            if($scope.mScroll instanceof iScroll){
                $scope.mScroll.destroy();
            }
            $("#scroller_snatch_record").height($(window).height());

            $scope.mScroll = new iScroll("scroller_snatch_record",{
                hScrollbar:false,
                vScrollbar:false,
                onScrollMove: function(){
                    if(this.y <= (this.maxScrollY - 60)){
                        $scope.isPullUp = true;
                    }
                },
                onScrollEnd: function(){
                    if($scope.isPullUp && $scope.haveMore){
                        $scope.page++;
                        $scope.isPullUp = false;
                        $scope.get_duobao_list($scope.page, $scope.size, $scope.type);
                    }
                    if(this.y < -800){
                        $('#proId').show();
                    }else{
                        $('#proId').hide();
                    }

                }
            });
        };

        $('#proId').click(function(){
            $scope.mScroll.scrollTo(0, 0, 0);
        });

        $scope.isSelected1 = true;
        $scope.isSelected2 = false;
        $scope.isSelected3 = false;
        $scope.type = 0;
        $scope.setSelectedItem = function(num){
            if(num == 1){
                $scope.isSelected1 = true;
                $scope.isSelected2 = false;
                $scope.isSelected3 = false;
                $scope.type = 0;
            } else if(num == 2){
                $scope.isSelected1 = false;
                $scope.isSelected2 = true;
                $scope.isSelected3 = false;
                $scope.type = 3;
            } else if(num == 3){
                $scope.isSelected1 = false;
                $scope.isSelected2 = false;
                $scope.isSelected3 = true;
                $scope.type = 2;
            }
            $scope.page = 1;
            $scope.get_duobao_list($scope.page, $scope.size, $scope.type);
        };
        $scope.backTo = function(){
            window.history.go(-1);
        };

        $scope.uid = $location.search()['uid'];
        $scope.recordListProgress = [];
        $scope.countDown = [];
        $scope.recordListFinished = [];
        $scope.status_public = 0;
        if($scope.uid != $rootScope.holdUID){
            $scope.buyState = "跟买";
        }else{
            $scope.buyState = "追加";
        }
        $scope.CDdata = {};
        $scope.get_duobao_list = function (page, size, type) {
            showLoading();
            var partner = "ZLAPITOH5WAP";
            var timestamp = Date.parse(new Date())/1000;
            var sign = $.md5(partner + timestamp + key);
            //console.log($scope.uid);
            $http({method: 'POST', data: {partner: partner, timestamp: timestamp, sign: sign, uid: $scope.uid, page: page, size: size, type: type}, url: "../index.php/yunapi/member/get_duobao_list"})
                .success(function (response, status, headers, config) {
                    /*var str = JSON.stringify(response);
                     $rootScope.showAlert(str);*/
                    console.log(response);
                    if (response.status == "1") {
                        if($scope.page == 1){					//进行中
                            $scope.recordListProgress = [];
                            $scope.countDown = [];
                            $scope.recordListFinished = [];
                        }
                        if(!response.data){
                            $scope.noData = true;
                            hideLoading();
                            return;
                        }else{
                            $scope.noData = false;
                        }
                        for(var i = 0; i < response.data.list.length; i++){
                            if(response.data.list[i].tag == 0){										//已揭晓
                                $scope.recordListFinished.push(response.data.list[i]);
                            } else if(response.data.list[i].tag == 1){								//进行中
                                response.data.list[i].title = response.data.list[i].shopname;
                                $scope.recordListProgress.push(response.data.list[i]);
                            } else if(response.data.list[i].tag == 2){								//倒计时
                                $scope.countDown.push(response.data.list[i]);
                            }
                        }

                        $scope.get_win_member_info = function(data,i){
                            //console.log('需要查询的用户信息',data);
                            //console.log($scope.countDown);
                            //console.log(i);
                            var shopid = data.id;
                            if($scope.status_public==1){
                                return false;
                            }
                            var partner = "ZLAPITOH5WAP";
                            var timestamp = Date.parse(new Date())/1000;
                            var sign = $.md5(partner + timestamp + key);
                            $http({
                                method: 'POST',
                                data: {partner: partner, timestamp: timestamp, sign: sign, id: shopid},
                                url: "../index.php/yunapi/goods/get_win_member_info"
                            })
                                .success(function (response, status, headers, config) {
                                    console.log(response);
                                    if (response.status == "1") {
                                        var response_data = response.data;
                                        $scope.countDown[i]['status_public']=1;
                                        console.log('状态是否更改',$scope.countDown);
                                        var str = '<div class="m-user-goods-owner m-user-box">'+
                                            '<div class="m-user-box-name">'+
                                            '获奖者：<a href="#/tab/personCenter?type=other&uid='+response_data.uid+'">'+response_data.username+'</a>'+
                                            '</div>'+
                                            '<div class="m-user-box-cont">'+
                                            '本期参与：<span class="txt-impt">'+response_data.renci+'</span>人次'+
                                            '</div>'+
                                            '</div>'+
                                            '<p class="m-user-goods-code">'+
                                            '幸运号码：<span class="txt-impt">'+response_data.q_user_code+'</span>'+
                                            '</p>'+
                                            '<p class="m-user-goods-time">揭晓时间：<span>'+response_data.q_end_time+'</span></p>';
                                        console.log(str);
                                        var div_check ='.countdown_nav_' + data.shopid;
                                        $(div_check).html();
                                        $(div_check).html(str);
                                        return
                                    } else {
                                        console.log('还未中奖，继续请求');
                                    }
                                })
                                .error(function (response, status, headers, config) {

                                });

                        };
                        var  setTimeCountDown = function(class_name,test_time_sec){
                            setTimeout(function(){
                                $(class_name).fnTimeCountDown(test_time_sec*1000,function(){});
                            },500);
                        }
                        var chaxun_fun = function(i,test_time_msec){
                            setTimeout(function(){
                                //每隔两秒查询一次，如果有结果就停止
                                var chaxun = setInterval(function(){        // i 变量到里面会自增1
                                    if($scope.countDown[i].status_public==1){//已揭晓状态
                                        clearInterval(chaxun);
                                    }
                                    $scope.get_win_member_info($scope.countDown[i],i);
                                },2000);
                            },test_time_msec);//测试
                        }
                        //针对正在揭晓
                        console.log('倒计时产品',$scope.countDown);
                        for(var i=0;i<$scope.countDown.length;i++){
                            console.log('序号',i);
                            var test_time_sec = $scope.countDown[i].q_end_time;
                            var test_time_msec = (parseInt(test_time_sec))*1000 - (new Date().getTime());//倒计时时间戳 毫秒
                            var class_name = ".fnTimeCountDown_"+i;
                            setTimeCountDown(class_name,test_time_sec);             //异步
                            chaxun_fun(i,test_time_msec);                    //异步
                        }




                        if(page == 1){
                            setTimeout(function(){$scope.initIScroll();},500);
                        } else {
                            setTimeout(function(){$scope.mScroll.refresh();},500);
                        }
                        if(response.data.list.length == $scope.size){
                            $scope.haveMore = true;
                        } else {
                            $scope.haveMore = false;
                        }
                        setTimeout(function(){
                            $("a").on("touchstart", function(e){
                                $(e.target).addClass('box-shadow-style');
                            });
                            $("a").on("touchmove", function(e){
                                $(e.target).removeClass('box-shadow-style');
                            });
                            $("a").on("touchend", function(e){
                                $(e.target).removeClass('box-shadow-style');
                            });
                        }, 500);
                    } else {
                        $rootScope.showAlert(response.message);
                    }
                    hideLoading();
                })
                .error(function (response, status, headers, config) {
                    hideLoading();
                });
        };
        $scope.get_duobao_list($scope.page, $scope.size, $scope.type);
    })

});
/**
 * Created by Administrator on 2016/8/8.
 */
