/**
 * Created by Administrator on 2016/8/8.
 */
define(['./module','./indexCtrl'], function (controllers) {
    'use strict';
    //绑定
    controllers.controller('bindwxCtrl',function($scope, $http, $location, $rootScope){

        $scope.openid 	= $location.search()['openid'];
        $scope.type 	= $location.search()['type'];

        if($scope.openid && $scope.type){
            var go_url = "../index.php/yunapi/SecLogin/Sec_login_userinfo";
        }else{
            $location.url('/tab/login');return false;
        }

        $scope.get_wx_userinfo = function(){
            showLoading();
            var partner = "ZLAPITOH5WAP";
            var timestamp = Date.parse(new Date())/1000;
            var sign = $.md5(partner + timestamp + key);
            $http({
                method:'POST',
                data:{
                    partner: partner,
                    timestamp: timestamp,
                    sign: sign,
                    openid:$scope.openid,
                    type:$scope.type,
                },
                url:go_url,
            })
                .success(function(response, status, headers, config){
                    console.log(response);
                    if (response.status == "1") {
                        var data = response.data;
                        $scope.headimgurl  = data.headimgurl;	//微信头像
                        $scope.nickname = data.nickname;		//微信名字
                        hideLoading();
                    } else {
                        hideLoading();
                        $rootScope.showAlert(response.message);
                    }

                })
                .error(function(response, status, headers, config){
                    hideLoading();
                    $rootScope.showAlert('检查网络！');
                });
        };
        $scope.get_wx_userinfo();

        $scope.do_bind = function(){
            if(isNull($scope.mobile)){
                $rootScope.showAlert('手机号不能为空！');
                return;
            }else if($scope.mobile.length != 11){
                $rootScope.showAlert('手机号长度不正确！');
                return;
            }else {
                var reg = /^(1[0-9]{10})$/;
                if(!reg.test($scope.mobile)){
                    $rootScope.showAlert("请输入有效的手机号码");
                    return;
                }
            }
            if(isNull($scope.password)){
                $rootScope.showAlert('密码不能为空！');
                return;
            }
            if($scope.password.length < 6 || $scope.password.length > 20){
                $rootScope.showAlert("密码长度不在规定范围内");
                return;
            }
            showLoading();
            var partner = "ZLAPITOH5WAP";
            var timestamp = Date.parse(new Date())/1000;
            var sign = $.md5(partner + timestamp + key);

            $http({
                method:'POST',
                data:{
                    partner: partner,
                    timestamp: timestamp,
                    sign: sign,
                    openid:$scope.openid,
                    username:$scope.mobile,
                    password:$scope.password,
                    type:$scope.type,
                },
                url:"../index.php/yunapi/SecLogin/do_bind",
            })
                .success(function(response, status, headers, config){
                    console.log(response);
                    if (response.status == "1") {
                        $rootScope.holdUID = response.message.uid;
                        //console.log("loginCtrl", response.message.uid);
                        var url = '/tab/personCenter?uid=' + response.message.uid;
                        $location.url(url);
                    } else {
                        $rootScope.showAlert(response.message);
                    }
                    hideLoading();
                })
                .error(function(response, status, headers, config){
                    hideLoading();
                    $rootScope.showAlert('检查网络！');

                });


        };








    })

});