app
    .controller('indexCtrl', function ($scope, $http, $location, $rootScope) {
    	//console.log($location.host()+"/index.php/api/wapwxlogin");
        document.title = "易中夺宝-好运连连";
        $rootScope.holdUID = getCookie("holdUID");
        $rootScope.holdState = 0;
        if(isNull(getCookie("shopList"))){
            $rootScope.shopList = [];
        }else{
            $rootScope.shopList = eval('('+getCookie("shopList")+')');
        }
        //日期格式  ->时间戳     精确到秒
		$scope.date_time = function transdate(endTime){
			var date=new Date();
			date.setFullYear(endTime.substring(0,4));
			date.setMonth(endTime.substring(5,7)-1);
			date.setDate(endTime.substring(8,10));
			date.setHours(endTime.substring(11,13));
			date.setMinutes(endTime.substring(14,16));
			date.setSeconds(endTime.substring(17,19));
			return Date.parse(date)/1000;
		}
		$scope.is_weixin = function(){
    	    var ua = window.navigator.userAgent.toLowerCase();
    	    if(ua.match(/MicroMessenger/i) == 'micromessenger'){
    	        return true;
    	    }else{
    	        return false;
    	    }
        };
		$scope.login = function(){
            if(!isNull($rootScope.holdUID)){
                $location.url('/tab/personCenter?uid=' + $rootScope.holdUID);
            }else{
            	/*if($scope.is_weixin()){
            		window.location.href="http://www.yz-db.com/index.php/api/wapwxlogin";
            	}else{*/
            		$location.url('/tab/login');
            	//}

            }
        };
        $scope.$on("itemChange", function (event, msg) {
            //console.log("indexCtrl", msg);
            $scope.$broadcast("itemChangeFromParrent", msg);
        });
        $scope.$on("uid", function (event, msg) {
            //console.log("indexCtrl", msg);
            $rootScope.holdUID = msg;
            addCookie("holdUID", msg, 0);
        });

        $scope.confirmExecute = function(){
            $rootScope.holdState = 1;
            $('#confirmModal').modal('hide');
        };

        $scope.dowloadApp = function(){
            var sUserAgent = navigator.userAgent.toLowerCase();
            var bIsIphoneOs = sUserAgent.match(/iphone os/i) == "iphone os";
            var bIsAndroid = sUserAgent.match(/android/i) == "android";
            var bIsWeiXin = sUserAgent.match(/MicroMessenger/i) == "micromessenger";
            if (bIsAndroid) {
            	if(bIsWeiXin){
                    $("#mcover").css("display","block");
                }else{
                	window.location.href = "http://www.yz-db.com/yyzl.apk";
                }
            } else if(bIsIphoneOs) {
            	if(bIsWeiXin){
                    $("#mcover").css("display","block");
                }else{
                //$rootScope.showAlert("IOS版即将上线，敬请期待");
                	window.location.href = "http://itunes.apple.com/cn/app/id1071871513?mt=8";
                }
            }
        };

        $scope.choosed = false;

        $scope.addNewAddress = function(){
            $('#addressModal').modal('hide');
            if($scope.userAddress.length > 4){
                $rootScope.showAlert("最多只能添加5条地址");
                return;
            }
            $location.url("/tab/addAddress?flag=address&uid=" + $rootScope.holdUID);
        };

        $scope.addressExecute = function(){
            var radios = document.getElementsByName("addressRadios");
            for (var i = 0; i < radios.length; i++) {
                if (radios[i].checked) {
                    if($scope.choosed){
                        $scope.choosed = false;
                        $('#addressModal').modal('hide');
                        $scope.do_confirm_address($scope.rid, radios[i].value);
                    }else{
                        $scope.address_c = $scope.userAddress[i];
                        $scope.choosed = true;
                    }
                    break;
                }
            }
        };

        $scope.hideAddress = function(){
            $('#addressModal').modal('hide');
            $scope.choosed = false;
        };
        //页面跳转
		$scope.click_href=function(url){

			$location.url(url);
		};
        $scope.do_confirm_address = function (rid, aid) {
            showLoading();
            var partner = "ZLAPITOH5WAP";
            var timestamp = Date.parse(new Date())/1000;
            var sign = $.md5(partner + timestamp + key);
            $http({method: 'POST', data: {partner: partner, timestamp: timestamp, sign: sign, uid: $rootScope.holdUID, rid: rid, aid: aid}, url: "../index.php/yunapi/member/do_confirm_address"})
                .success(function (response, status, headers, config) {
                    if (response.status == "1") {
                        /*var str = JSON.stringify(response);
                        console.log("----------->", str);*/
                        $rootScope.showAlert(response.message);
                        $scope.userAddress = [];
                        window.location.reload();
                    } else {
                        $rootScope.showAlert(response.message);
                    }
                    hideLoading();
                })
                .error(function (response, status, headers, config) {
                    hideLoading();
                });
        };

        $rootScope.addToCart = function(data, $event){

            if($rootScope.holdUID>0){
	            if(data.wap_ten == 0){
	            	$rootScope.showAlert("该产品的购买次数已达到上限");return false;
	            }

                var isAdded = false;
                var syrs = data.zongrenshu - data.canyurenshu;		//剩余人数
                for(var i = 0; i < $rootScope.shopList.length; i++){
                    if(data.id == $rootScope.shopList[i].id){
                        isAdded = true;
                        if(data.is_ten == 1){								//5元限购
                            if($rootScope.shopList[i].num >=data.wap_ten){
                                $rootScope.shopList[i].num = data.wap_ten;
                                $rootScope.showAlert("该产品的购买数量达到上限！");return false;
                            }
                        }
                        if(syrs > $rootScope.shopList[i].num){
                            $rootScope.shopList[i].num ++;
                        }
                        break;
                    }
                }
                if(!isAdded){
                    data.num = 1;
                    $rootScope.shopList.push(data);
                }



                var img = data.thumb;
	            var flyElm = $('<img style="z-index:999;width:40px;" class="u-flyer" src="'+img+'">');
	            $('body').append(flyElm);
	            flyElm.css({
	                'z-index': 9000,
	                'display': 'block',
	                'position': 'absolute',
	                'top': event.pageY-50 +'px',
	                'left': event.pageX-50 +'px',
	                'width': 60 +'px',
	                'height': 60 +'px'
	            });
	            flyElm.animate({
	                top: $("#shopList").offset().top,
	                left: $("#shopList").offset().left+20,
	                width: 5,
	                height: 10
	            }, 'slow', function() {
	                flyElm.remove();
	            });


	            addCookie("shopList", JSON.stringify($rootScope.shopList), 0);
            }else{

            	//xian登陆
            	$scope.login();
            }
        };

        $scope.showMsg = [];
        $rootScope.showAlert = function(msg, num, code){
            if(num == 0){
                $scope.winCode = code;
                $scope.showMsg = msg.split(",");
                $('#showModal').modal('show');
                $('#showModal').css('margin-top', $(window).height()/2 - 230);
            }else if(num == 1){
                $scope.userAddress = msg;
                $scope.rid = code;
                $('#addressModal').modal('show');
            }else{
                $scope.alertMsg = msg;
                $('#alertModal').modal('show');
            }
        };

        $rootScope.showPic = function(msg){
            $scope.picUrl = msg;
            $('#picModal').modal('show');
        };
		//浏览器跳转
        $scope.browserRedirect = function(){
            var sUserAgent = navigator.userAgent.toLowerCase();
            var bIsIpad = sUserAgent.match(/ipad/i) == "ipad";
            var bIsIphoneOs = sUserAgent.match(/iphone os/i) == "iphone os";
            var bIsMidp = sUserAgent.match(/midp/i) == "midp";
            var bIsUc7 = sUserAgent.match(/rv:1.2.3.4/i) == "rv:1.2.3.4";
            var bIsUc = sUserAgent.match(/ucweb/i) == "ucweb";
            var bIsAndroid = sUserAgent.match(/android/i) == "android";
            var bIsCE = sUserAgent.match(/windows ce/i) == "windows ce";
            var bIsWM = sUserAgent.match(/windows mobile/i) == "windows mobile";
            if (bIsIpad || bIsIphoneOs || bIsMidp || bIsUc7 || bIsUc || bIsAndroid || bIsCE || bIsWM) {
                $("#footbar").show();
            } else {
                $("#footbar").hide();
                window.location.href = "http://www.yz-db.com";
            }
        };
        $scope.browserRedirect();

        $scope.closeBar = function(){
            $("#footbar").hide();
            $("#shopList").css("bottom", "10px");
        };
    })
    //限购
    .controller('xiangouCtrl',function($scope, $http, $rootScope, $location){
        document.title = "易中夺宝-好运连连";
        $("#headerId").show();
        $("#shopList").show();

        $scope.$emit("itemChange", 4);

        $scope.noData = false;

        $scope.isSelected = -1;
        $scope.isSelected0 = true;
        $scope.isSelected1 = false;
        $scope.isSelected2 = false;
        $scope.isSelected3 = false;
        $scope.isSelected4 = false;
        $scope.isShow = false;
        $scope.typeList = [];
        $scope.displayType = "商品分类";
        $scope.typeId = 0;
        // $scope.type = "renqi";
        if($location.search()['flag'] == "zuixin"){
            $scope.isSelected1 = false;
            $scope.isSelected2 = true;
            $scope.isSelected3 = false;
            $scope.isSelected4 = false;
            $scope.type = "zuixin";
        }
        if($location.search()['flag'] == "zuixin"){
            $scope.isSelected1 = false;
            $scope.isSelected2 = true;
            $scope.isSelected3 = false;
            $scope.isSelected4 = false;
            $scope.type = "zuixin";
        }

        $scope.page = 1;
        $scope.size = 10;

        $scope.mScroll = "";
        $scope.isPullUp = false;
        $scope.haveMore = false;
        $scope.initIScroll = function(){
            if($scope.mScroll instanceof iScroll){     //instanceof判断是否是继承关系
                $scope.mScroll.destroy();
            }
            $("#scroller").height($(window).height() - 120);

            $scope.mScroll = new iScroll("scroller",{
                hScrollbar:false,
                vScrollbar:false,
                onScrollMove: function(){
                    if(this.y <= (this.maxScrollY - 60)){
                        $scope.isPullUp = true;
                    }
                },
                onScrollEnd: function(){
                    if($scope.isPullUp && $scope.haveMore){
                        $scope.page++;
                        $scope.isPullUp = false;
                        $scope.get_ten_yuan_list($scope.page, $scope.size, $scope.type, $scope.typeId);
                    }
                    if(this.y < -800){
                        $('#proId').show();
                    }else{
                        $('#proId').hide();
                    }
                }
            });
        };
        $('#proId').click(function(){
            $scope.mScroll.scrollTo(0, 0, 0);
        });

        $scope.chooseType = function(num, name, typeId){
            $scope.isSelected = num;
            $scope.isShow = false;
            $scope.displayType = name;
            $scope.typeId = typeId;
            $scope.get_ten_yuan_list(1, 10, $scope.type, $scope.typeId);
        };
        $scope.openMenuList = function(){
            if($scope.isShow){
                $scope.isShow = false;
            } else {
                $scope.isShow = true;
            }
        };
        $scope.setSelectedItem = function(num){
        	if(num == 0){
        		$scope.isSelected0 = true;
        		$scope.isSelected1 = false;
                $scope.isSelected2 = false;
                $scope.isSelected3 = false;
                $scope.isSelected4 = false;
                $scope.type = "0";				//默认排序
            }else if(num == 1){
            	$scope.isSelected0 = false;
                $scope.isSelected1 = true;
                $scope.isSelected2 = false;
                $scope.isSelected3 = false;
                $scope.isSelected4 = false;
                $scope.type = "1";				//推荐排序
            } else if(num == 2){
            	$scope.isSelected0 = false;
                $scope.isSelected1 = false;
                $scope.isSelected2 = true;
                $scope.isSelected3 = false;
                $scope.isSelected4 = false;
                $scope.type = "2";				//时间排序
            } else if(num == 3){
            	$scope.isSelected0 = false;
                $scope.isSelected1 = false;
                $scope.isSelected2 = false;
                $scope.isSelected3 = true;
                $scope.isSelected4 = false;
                $scope.type = "3";				//剩余人次排序
            } else if(num == 4){
            	$scope.isSelected0 = false;
                $scope.isSelected1 = false;
                $scope.isSelected2 = false;
                $scope.isSelected3 = false;
                $scope.isSelected4 = true;
                $scope.type = "4";				//所需总人次排序
            }
            $scope.page = 1;
            $scope.get_ten_yuan_list($scope.page, 10, $scope.type, $scope.typeId);
        };

        $scope.goodsList = [];

        $scope.get_ten_yuan_list = function (page, size, type, cateid) {
            showLoading();
            var partner = "ZLAPITOH5WAP";
            var timestamp = Date.parse(new Date())/1000;
            var sign = $.md5(partner + timestamp + key);
            //$scope.goodsList = [];
            $http({method: 'POST', data: {partner: partner, timestamp: timestamp, sign: sign, page: page, size: size, type: type, cateid: cateid,uid:$rootScope.holdUID}, url: "../index.php/yunapi/index/get_ten_yuan_list"})
                .success(function (response, status, headers, config) {
                    /*var str = JSON.stringify(response);
                    $rootScope.showAlert(str);*/
                    //console.log(response);
                    if (response.status == "1") {		//请求成功
                        if($scope.page == 1){
                            $scope.goodsList = response.data.list;		//获得的商品列表
                            if($scope.goodsList.length == 0){			//商品数目
                                $scope.noData = true;
                            }
                            setTimeout(function(){$scope.initIScroll();},500);
                        } else {
                            $scope.goodsList = $scope.goodsList.concat(response.data.list);
                            setTimeout(function(){$scope.mScroll.refresh();},500);
                        }
                        if(response.data.list.length == $scope.size){
                            $scope.haveMore = true;
                        } else {
                            $scope.haveMore = false;
                        }
                        setTimeout(function(){
                            $("a").on("touchstart", function(e){
                                $(e.target).addClass('box-shadow-style');
                            });
                            $("a").on("touchmove", function(e){
                                $(e.target).removeClass('box-shadow-style');
                            });
                            $("a").on("touchend", function(e){
                                $(e.target).removeClass('box-shadow-style');
                            });
                        }, 500);
                    } else {
                        $rootScope.showAlert(response.message);
                    }
                    hideLoading();
                })
                .error(function (response, status, headers, config) {
                    hideLoading();
                });

        };
        $scope.cateid = $location.search()['cateid'];
        $scope.typeList = [];
        $scope.get_category_list = function () {
            showLoading();
            var partner = "ZLAPITOH5WAP";
            var timestamp = Date.parse(new Date())/1000;
            var sign = $.md5(partner + timestamp + key);
            $http({method: 'POST', data: {partner: partner, timestamp: timestamp, sign: sign}, url: "../index.php/yunapi/index/get_category_list"})
                .success(function (response, status, headers, config) {
                    if (response.status == "1") {
                        $scope.typeList = response.data;
                        if(!isNull($scope.cateid)){
                            for(var i = 0; i < $scope.typeList.length; i++){
                                if($scope.typeList[i].cateid == $scope.cateid){
                                    $scope.displayType = $scope.typeList[i].name;
                                    $scope.isSelected = i;
                                    break;
                                }
                            }
                            $scope.typeId = $scope.cateid;
                        }
                        $scope.get_ten_yuan_list(1, 10, $scope.type, $scope.typeId);
                        setTimeout(function(){
                            $("a").on("touchstart", function(e){
                                $(e.target).addClass('box-shadow-style');
                            });
                            $("a").on("touchmove", function(e){
                                $(e.target).removeClass('box-shadow-style');
                            });
                            $("a").on("touchend", function(e){
                                $(e.target).removeClass('box-shadow-style');
                            });
                        }, 500);
                    } else {
                        $rootScope.showAlert(response.message);
                    }
                    hideLoading();
                })
                .error(function (response, status, headers, config) {
                    hideLoading();
                });
        };
        $scope.get_category_list();

    })
    //头部
    .controller('headerCtrl', function ($scope, $location, $rootScope) {
    	$('#body_top').css('height','0px');
        $scope.isSelected1 = true;
        $scope.isSelected2 = false;
        $scope.isSelected3 = false;
        $scope.isSelected4 = false;
        $scope.$on("itemChangeFromParrent", function (event, msg) {
            //console.log("headerCtrl", msg);
            if(msg ==1){
            	$scope.isSelected1 = true;
                $scope.isSelected2 = false;
                $scope.isSelected3 = false;
                $scope.isSelected4 = false;
                $('#body_top').css('height','84px');
            }else if(msg == 2){
                $scope.isSelected1 = false;
                $scope.isSelected2 = true;
                $scope.isSelected3 = false;
                $scope.isSelected4 = false;
                $('#body_top').css('height','84px');
            } else if(msg == 3){
                $scope.isSelected1 = false;
                $scope.isSelected2 = false;
                $scope.isSelected3 = true;
                $scope.isSelected4 = false;
                $('#body_top').css('height','84px');
            }else if(msg == 4){
            	$scope.isSelected1 = false;
                $scope.isSelected2 = false;
                $scope.isSelected3 = false;
                $scope.isSelected4 = true;
                $('#body_top').css('height','84px');
            }
            $('body').scrollTop(0);
        });

        $scope.is_weixin = function(){
    	    var ua = window.navigator.userAgent.toLowerCase();
    	    if(ua.match(/MicroMessenger/i) == 'micromessenger'){
    	        return true;
    	    }else{
    	        return false;
    	    }
        };

        $scope.toLogin = function(){
            if(!isNull($rootScope.holdUID)){
                $location.url('/tab/personCenter?uid=' + $rootScope.holdUID);
            }else{
            	//if($scope.is_weixin()){
            		//window.location.href="http://www.yz-db.com/index.php/api/wapwxlogin";
            	//}else{
            		$location.url('/tab/login');
            	//}

            }
        };

        $scope.setSelectedItem = function(num){
            if(num ==1){
            	$scope.isSelected1 = true;
                $scope.isSelected2 = false;
                $scope.isSelected3 = false;
                $scope.isSelected4 = false;
            }else if(num == 2){
                $scope.isSelected1 = false;
                $scope.isSelected2 = true;
                $scope.isSelected3 = false;
                $scope.isSelected4 = false;
            } else if(num == 3){
                $scope.isSelected1 = false;
                $scope.isSelected2 = false;
                $scope.isSelected3 = true;
                $scope.isSelected4 = false;
            }else if(num == 4){
            	$scope.isSelected1 = false;
                $scope.isSelected2 = false;
                $scope.isSelected3 = false;
                $scope.isSelected4 = true;
            }
        };
        $rootScope.holdUID = getCookie("holdUID");
        if($rootScope.holdUID > 0){
        	console.log($rootScope.holdUID );
        }
    })
    //首页
    .controller('homeCtrl', function ($scope, $http, $rootScope,$location) {
        document.title = "易中夺宝-好运连连";
        $("#headerId").show();
        $("#shopList").show();
        $("#proId").hide();
        $('.carousel').carousel({
            interval: 3000
        });
        
        $scope.close_box = function(){
        	$(".Advertisent").hide();
        }
		$scope.go_lemon = function(){
			$location.url('tab/lemon');
		}
        $scope.$emit("itemChange",1);

        $scope.goodsStatus = "倒计时";

        $scope.imgTouchMove = function(){
            var slidePage = {};
            $('#carousel').on('touchstart', function(e) {
                var touch = e.originalEvent.targetTouches[0];
                slidePage.x = touch.pageX;
            });
            $('#carousel').on('touchend', function(e) {
                var touch = e.originalEvent.changedTouches[0];
                var x = touch.pageX;
                if(x - parseInt(slidePage.x) > 20){
                    //从左往右
                    $('#carousel').carousel('prev');
                } else if(x - parseInt(slidePage.x) < -20){
                    //从右往左
                    $('#carousel').carousel('next');
                }
            });
        };
        $scope.get_win_member_info_copy = function(num){
        	if($scope.newGoods[num].status_public==1){
        		return false;
        	}
        	var partner = "ZLAPITOH5WAP";
            var timestamp = Date.parse(new Date())/1000;
            var sign = $.md5(partner + timestamp + key);
            $http({method: 'POST', data: {partner: partner, timestamp: timestamp, sign: sign, id: $scope.newGoods[num].id}, url: "../index.php/yunapi/goods/get_win_member_info"})
                .success(function (response, status, headers, config) {
                	console.log(response);

                    if (response.status == "1") {
                        $scope.newGoods[num].waiting = false;
                        $scope.newGoods[num].tag = 0;
                        $scope.newGoods[num].goodsStatus = "恭喜";
                        $scope.newGoods[num].username = response.data.username;
                        $scope.newGoods[num].status_public=1;
                    } else {
                        //$rootScope.showAlert(response.message);
                    }
                })
                .error(function (response, status, headers, config) {

                });
        };

        $scope.imgData = [];

        $scope.get_slide_list = function () {
            showLoading();
            var partner = "ZLAPITOH5WAP";
            var timestamp = Date.parse(new Date())/1000;
            var sign = $.md5(partner + timestamp + key);
            $http({method: 'POST', data: {partner: partner, timestamp: timestamp, sign: sign}, url: "../index.php/yunapi/index/get_slide_list"})
                .success(function (response, status, headers, config) {
                    if (response.status == "1") {
                        var str = JSON.stringify(response);
                        //console.log("----------->", str);
                        if(response.data){
                            $scope.imgData = response.data;
                            setTimeout(function(){
                                $("a").on("touchstart", function(e){
                                    $(e.target).addClass('box-shadow-style');
                                });
                                $("a").on("touchmove", function(e){
                                    $(e.target).removeClass('box-shadow-style');
                                });
                                $("a").on("touchend", function(e){
                                    $(e.target).removeClass('box-shadow-style');
                                });
                            }, 500);
                        }
                    } else {
                        $rootScope.showAlert(response.message);
                    }
                    hideLoading();
                })
                .error(function (response, status, headers, config) {
                    hideLoading();
                });
        };

        //$scope.endTime = "";
        $scope.newGoods = [];
        $scope.keepTime = false;
        $scope.isFirst = true;

        $scope.get_newest_lottery = function (page, size) {
            //showLoading();
            var partner = "ZLAPITOH5WAP";
            var timestamp = Date.parse(new Date())/1000;
            var sign = $.md5(partner + timestamp + key);
            $http({method: 'POST', data: {partner: partner, timestamp: timestamp, sign: sign, page: page, size: size}, url: "../index.php/yunapi/goods/get_newest_lottery"})
                .success(function (response, status, headers, config) {
                    if (response.status == "1") {
                        $scope.newGoods = response.data.list;
                        for(var i = 0; i < $scope.newGoods.length; i++){
                            $scope.newGoods[i].waiting = false;
                            if($scope.newGoods[i].tag == 0){
                                $scope.newGoods[i].goodsStatus = "恭喜";
                                $scope.newGoods[i].status_public=1;
                            } else {
                                $scope.newGoods[i].goodsStatus = "倒计时";
                                //$scope.endTime = newGoods[i].q_end_time;
                                if($scope.newGoods[i].q_end_time.indexOf("年")){
                                    $scope.newGoods[i].q_end_time = $scope.newGoods[i].q_end_time.replace("年", "/");
                                }
                                if($scope.newGoods[i].q_end_time.indexOf("月")){
                                    $scope.newGoods[i].q_end_time = $scope.newGoods[i].q_end_time.replace("月", "/");
                                }
                                if($scope.newGoods[i].q_end_time.indexOf("日")){
                                    $scope.newGoods[i].q_end_time = $scope.newGoods[i].q_end_time.replace("日", "");
                                }
                                $scope.newGoods[i].isTime = true;
                            }
                        }
                        if($scope.newGoods[0].isTime){

                            setTimeout(function(){
								var test_time_one = $scope.newGoods[0].test_time;

								test_time_one = (new Date().getTime())+(parseInt(test_time_one))*1000;

                                $("#fnTimeCountDown0").fnTimeCountDown(test_time_one,  function(){
                                    if($scope.newGoods[0].tag == 1){
                                        $scope.newGoods[0].waiting = true;
                                        $scope.newGoods[0].tag = -1;
                                        setTimeout(function(){



	                                        var chaxun0 = setInterval(function(){
	                                            var partner = "ZLAPITOH5WAP";
	                                            var timestamp = Date.parse(new Date())/1000;
	                                            var sign = $.md5(partner + timestamp + key);
	                                            $http({method: 'POST', data: {partner: partner, timestamp: timestamp, sign: sign, id: $scope.newGoods[0].id}, url: "../index.php/yunapi/goods/get_win_member_info"})
	                                                .success(function (response, status, headers, config) {

	                   									var str = JSON.stringify(response);

	                                                    if (response.status == "1") {
	                                                        $scope.newGoods[0].waiting = false;
	                                                        $scope.newGoods[0].tag = 0;
	                                                        clearInterval(chaxun0);
	                                                        $scope.newGoods[0].goodsStatus = "恭喜";
	                                                        $scope.newGoods[0].username = response.data.username;
	                                                    } else {
	                                                        //$rootScope.showAlert(response.message);
	                                                    }
	                                                })
	                                                .error(function (response, status, headers, config) {

	                                                });
	                                        },3000);
                                        },3000);
                                    }
                                });
                            },500);



                        }
                        if($scope.newGoods[1].isTime){

                            setTimeout(function(){
                                var test_time_two = $scope.newGoods[1].test_time;
								test_time_two = (new Date().getTime())+(parseInt(test_time_two))*1000;
                                $("#fnTimeCountDown1").fnTimeCountDown(test_time_two, function(){
                                    if($scope.newGoods[1].tag == 1){
                                        $scope.newGoods[1].waiting = true;
                                        $scope.newGoods[1].tag = -1;
                                        setTimeout(function(){
                                        	var chaxun1 = setInterval(function(){
	                                            var partner = "ZLAPITOH5WAP";
	                                            var timestamp = Date.parse(new Date())/1000;
	                                            var sign = $.md5(partner + timestamp + key);
	                                            $http({method: 'POST', data: {partner: partner, timestamp: timestamp, sign: sign, id: $scope.newGoods[1].id}, url: "../index.php/yunapi/goods/get_win_member_info"})
	                                                .success(function (response, status, headers, config) {
	                                                    //console.log(response);
	                    								var str = JSON.stringify(response);
	                    								//console.log("home:", str);
	                                                    if (response.status == "1") {
	                                                        $scope.newGoods[1].waiting = false;
	                                                        $scope.newGoods[1].tag = 0;
	                                                        clearInterval(chaxun1);
	                                                        $scope.newGoods[1].goodsStatus = "恭喜";
	                                                        $scope.newGoods[1].username = response.data.username;
	                                                    } else {
	                                                        //$rootScope.showAlert(response.message);
	                                                    }
	                                                })
	                                                .error(function (response, status, headers, config) {

	                                                });
	                                        },3000);
                                        },3000);
                                    }
                                });
                            },500);
                        }
                        if($scope.newGoods[2].isTime){

                            setTimeout(function(){
                                var test_time_three = $scope.newGoods[2].test_time;
								test_time_three = (new Date().getTime())+(parseInt(test_time_three))*1000;
                                $("#fnTimeCountDown2").fnTimeCountDown(test_time_three, function(){
                                    if($scope.newGoods[2].tag == 1){
                                        $scope.newGoods[2].waiting = true;
                                        $scope.newGoods[2].tag = -1;
                                        setTimeout(function(){
                                        	var chaxun2 = setInterval(function(){
	                                            var partner = "ZLAPITOH5WAP";
	                                            var timestamp = Date.parse(new Date())/1000;
	                                            var sign = $.md5(partner + timestamp + key);
	                                            $http({method: 'POST', data: {partner: partner, timestamp: timestamp, sign: sign, id: $scope.newGoods[2].id}, url: "../index.php/yunapi/goods/get_win_member_info"})
	                                                .success(function (response, status, headers, config) {
	                                                    //console.log(response);
	                    								var str = JSON.stringify(response);
	                    								//console.log("home:", str);
	                                                    if (response.status == "1") {
	                                                        $scope.newGoods[2].waiting = false;
	                                                        $scope.newGoods[2].tag = 0;
	                                                        clearInterval(chaxun2);
	                                                        $scope.newGoods[2].goodsStatus = "恭喜";
	                                                        $scope.newGoods[2].username = response.data.username;
	                                                    } else {
	                                                        //$rootScope.showAlert(response.message);
	                                                    }
	                                                })
	                                                .error(function (response, status, headers, config) {

	                                                });
	                                        },3000);
                                        },3000);
                                    }
                                });
                            },500);
                        }

                        setTimeout(function(){
                            $("a").on("touchstart", function(e){
                                $(e.target).addClass('box-shadow-style');
                            });
                            $("a").on("touchmove", function(e){
                                $(e.target).removeClass('box-shadow-style');
                            });
                            $("a").on("touchend", function(e){
                                $(e.target).removeClass('box-shadow-style');
                            });
                        }, 500);
                    } else {
                        $rootScope.showAlert(response.message);
                    }
                    //hideLoading();
                })
                .error(function (response, status, headers, config) {
                    //hideLoading();
                });
        };

        $scope.hotGoods = [];
        $scope.nGoods = [];
        $scope.get_type_goods_list = function (page, size, type) {
            showLoading();
            var partner = "ZLAPITOH5WAP";
            var timestamp = Date.parse(new Date())/1000;
            var sign = $.md5(partner + timestamp + key);
            $http({method: 'POST', data: {partner: partner, timestamp: timestamp, sign: sign, page: page, size: size, type: type, sort: 'desc',uid:$rootScope.holdUID}, url: "../index.php/yunapi/index/get_type_goods_list"})
                .success(function (response, status, headers, config) {
                    if (response.status == "1") {
                        if(type == 'jindu'){
                            $scope.hotGoods = response.data.list;
                            console.log('进度',$scope.hotGoods);
                        }else if(type == 'zuixin'){
                            $scope.nGoods = response.data.list;
                        }
                        setTimeout(function(){
                            $("a").on("touchstart", function(e){
                                $(e.target).addClass('box-shadow-style');
                            });
                            $("a").on("touchmove", function(e){
                                $(e.target).removeClass('box-shadow-style');
                            });
                            $("a").on("touchend", function(e){
                                $(e.target).removeClass('box-shadow-style');
                            });
                        }, 500);
                    } else {
                        $rootScope.showAlert(response.message);
                    }
                    hideLoading();
                })
                .error(function (response, status, headers, config) {
                    hideLoading();
                });
        };
        $scope.get_win_member_info = function (id, i) {
            showLoading();
            var partner = "ZLAPITOH5WAP";
            var timestamp = Date.parse(new Date())/1000;
            var sign = $.md5(partner + timestamp + key);
            $http({method: 'POST', data: {partner: partner, timestamp: timestamp, sign: sign, id: id}, url: "../index.php/yunapi/goods/get_win_member_info"})
                .success(function (response, status, headers, config) {
                    if (response.status == "1") {
                        $scope.newGoods[i].tag = 0;
                        $scope.newGoods[i].goodsStatus = "恭喜";
                        $scope.newGoods[i].username = response.data.username;

                        setTimeout(function(){
                            $("a").on("touchstart", function(e){
                                $(e.target).addClass('box-shadow-style');
                            });
                            $("a").on("touchmove", function(e){
                                $(e.target).removeClass('box-shadow-style');
                            });
                            $("a").on("touchend", function(e){
                                $(e.target).removeClass('box-shadow-style');
                            });
                        }, 500);
                    } else {
                        $rootScope.showAlert(response.message);
                        return false;
                    }
                    hideLoading();
                })
                .error(function (response, status, headers, config) {
                    hideLoading();
                });
        };

        $scope.imgTouchMove();
        $scope.get_slide_list();
        $scope.get_newest_lottery(1, 3);
        $scope.get_type_goods_list(1, 10, 'jindu');
        $scope.get_type_goods_list(1, 3, 'zuixin');
    })
    //全部商品
    .controller('listCtrl', function ($scope, $http, $rootScope, $location) {
        document.title = "易中夺宝-好运连连";
        $("#headerId").show();
        $("#shopList").show();

        $scope.$emit("itemChange", 2);

        $scope.noData = false;

        $scope.isSelected = -1;
        $scope.isSelected1 = true;
        $scope.isSelected2 = false;
        $scope.isSelected3 = false;
        $scope.isSelected4 = false;
        $scope.isShow = false;
        $scope.typeList = [];
        $scope.displayType = "商品分类";
        $scope.typeId = 0;
        $scope.type = "shenyurenshu";               //默认显示剩余人数（进度）
        if($location.search()['flag'] == "zuixin"){
            $scope.isSelected1 = false;
            $scope.isSelected2 = true;
            $scope.isSelected3 = false;
            $scope.isSelected4 = false;
            $scope.type = "zuixin";
        }
        $scope.page = 1;
        $scope.size = 10;

        $scope.mScroll = "";
        $scope.isPullUp = false;
        $scope.haveMore = false;
        $scope.initIScroll = function(){
            if($scope.mScroll instanceof iScroll){
                $scope.mScroll.destroy();
            }
            $("#scroller").height($(window).height() - 120);

            $scope.mScroll = new iScroll("scroller",{
                hScrollbar:false,
                vScrollbar:false,
                onScrollMove: function(){
                    if(this.y <= (this.maxScrollY - 60)){
                        $scope.isPullUp = true;
                    }
                },
                onScrollEnd: function(){
                    if($scope.isPullUp && $scope.haveMore){
                        $scope.page++;
                        $scope.isPullUp = false;
                        $scope.get_cate_goods_list($scope.page, $scope.size, $scope.type, $scope.typeId);
                    }
                    if(this.y < -800){
                        $('#proId').show();
                    }else{
                        $('#proId').hide();
                    }
                }
            });
        };
        $('#proId').click(function(){
            $scope.mScroll.scrollTo(0, 0, 0);
        });

        $scope.chooseType = function(num, name, typeId){
            $scope.isSelected = num;
            $scope.isShow = false;
            $scope.displayType = name;
            $scope.typeId = typeId;
            $scope.get_cate_goods_list(1, 10, $scope.type, $scope.typeId);
        };
        $scope.openMenuList = function(){
            if($scope.isShow){
                $scope.isShow = false;
            } else {
                $scope.isShow = true;
            }
        };
        $scope.setSelectedItem = function(num){
            if(num == 1){
                $scope.isSelected1 = true;
                $scope.isSelected2 = false;
                $scope.isSelected3 = false;
                $scope.isSelected4 = false;
                $scope.type = "shenyurenshu";
            } else if(num == 2){
                $scope.isSelected1 = false;
                $scope.isSelected2 = true;
                $scope.isSelected3 = false;
                $scope.isSelected4 = false;
                $scope.type = "zuixin";
            } else if(num == 3){
                $scope.isSelected1 = false;
                $scope.isSelected2 = false;
                $scope.isSelected3 = true;
                $scope.isSelected4 = false;
                $scope.type = "renqi";
            } else if(num == 4){
                $scope.isSelected1 = false;
                $scope.isSelected2 = false;
                $scope.isSelected3 = false;
                $scope.isSelected4 = true;
                $scope.type = "zongxurenci";
            }
            $scope.page = 1;
            $scope.get_cate_goods_list($scope.page, 10, $scope.type, $scope.typeId);
        };

        $scope.goodsList = [];

        $scope.get_cate_goods_list = function (page, size, type, cateid) {
            showLoading();
            var partner = "ZLAPITOH5WAP";
            var timestamp = Date.parse(new Date())/1000;
            var sign = $.md5(partner + timestamp + key);
            //$scope.goodsList = [];
            $http({method: 'POST', data: {partner: partner, timestamp: timestamp, sign: sign, page: page, size: size, type: type, cateid: cateid,uid:$rootScope.holdUID}, url: "../index.php/yunapi/index/get_cate_goods_list"})
                .success(function (response, status, headers, config) {
                    /*var str = JSON.stringify(response);
                    $rootScope.showAlert(str);*/
                    if (response.status == "1") {
                        if($scope.page == 1){
                            $scope.goodsList = response.data.list;
                            if($scope.goodsList.length == 0){
                                $scope.noData = true;
                            }
                            setTimeout(function(){$scope.initIScroll();},500);
                        } else {
                            $scope.goodsList = $scope.goodsList.concat(response.data.list);
                            setTimeout(function(){$scope.mScroll.refresh();},500);
                        }
                        if(response.data.list.length == $scope.size){
                            $scope.haveMore = true;
                        } else {
                            $scope.haveMore = false;
                        }
                        setTimeout(function(){
                            $("a").on("touchstart", function(e){
                                $(e.target).addClass('box-shadow-style');
                            });
                            $("a").on("touchmove", function(e){
                                $(e.target).removeClass('box-shadow-style');
                            });
                            $("a").on("touchend", function(e){
                                $(e.target).removeClass('box-shadow-style');
                            });
                        }, 500);
                    } else {
                        $rootScope.showAlert(response.message);
                    }
                    hideLoading();
                })
                .error(function (response, status, headers, config) {
                    hideLoading();
                });
        };
        $scope.cateid = $location.search()['cateid'];
        $scope.typeList = [];
        $scope.get_category_list = function () {
            showLoading();
            var partner = "ZLAPITOH5WAP";
            var timestamp = Date.parse(new Date())/1000;
            var sign = $.md5(partner + timestamp + key);
            $http({method: 'POST', data: {partner: partner, timestamp: timestamp, sign: sign}, url: "../index.php/yunapi/index/get_category_list"})
                .success(function (response, status, headers, config) {
                    if (response.status == "1") {
                        $scope.typeList = response.data;
                        if(!isNull($scope.cateid)){
                            for(var i = 0; i < $scope.typeList.length; i++){
                                if($scope.typeList[i].cateid == $scope.cateid){
                                    $scope.displayType = $scope.typeList[i].name;
                                    $scope.isSelected = i;
                                    break;
                                }
                            }
                            $scope.typeId = $scope.cateid;
                        }
                        $scope.get_cate_goods_list(1, 10, $scope.type, $scope.typeId);
                        setTimeout(function(){
                            $("a").on("touchstart", function(e){
                                $(e.target).addClass('box-shadow-style');
                            });
                            $("a").on("touchmove", function(e){
                                $(e.target).removeClass('box-shadow-style');
                            });
                            $("a").on("touchend", function(e){
                                $(e.target).removeClass('box-shadow-style');
                            });
                        }, 500);
                    } else {
                        $rootScope.showAlert(response.message);
                    }
                    hideLoading();
                })
                .error(function (response, status, headers, config) {
                    hideLoading();
                });
        };
        $scope.get_category_list();

    })
    //分享
    .controller('shareCtrl', function ($scope, $http, $rootScope) {
        document.title = "易中夺宝-好运连连";
        $("#headerId").show();
        $("#shopList").hide();

        $scope.$emit("itemChange", 3);

        $scope.noData = false;

        $scope.page = 1;
        $scope.size = 10;
        $scope.mScroll = "";
        $scope.isPullUp = false;
        $scope.haveMore = false;
        $scope.initIScroll = function(){
            if($scope.mScroll instanceof iScroll){
                $scope.mScroll.destroy();
            }
            $("#scroller_share").height($(window).height() - 90);

            $scope.mScroll = new iScroll("scroller_share",{
                hScrollbar:false,
                vScrollbar:false,
                onScrollMove: function(){
                    if(this.y <= (this.maxScrollY - 60)){
                        $scope.isPullUp = true;
                    }
                },
                onScrollEnd: function(){
                    if($scope.isPullUp && $scope.haveMore){
                        $scope.page++;
                        $scope.isPullUp = false;
                        $scope.get_shaidan_list($scope.page, $scope.size);
                    }
                    if(this.y < -800){
                        $('#proId').show();
                    }else{
                        $('#proId').hide();
                    }
                }
            });
        };
        $('#proId').click(function(){
            $scope.mScroll.scrollTo(0, 0, 0);
        });

        $scope.shareList = [];

        $scope.get_shaidan_list = function (page, size) {
            showLoading();
            var partner = "ZLAPITOH5WAP";
            var timestamp = Date.parse(new Date())/1000;
            var sign = $.md5(partner + timestamp + key);
            $http({method: 'POST', data: {partner: partner, timestamp: timestamp, sign: sign, page: page, size: size}, url: "../index.php/yunapi/goods/get_shaidan_list"})
                .success(function (response, status, headers, config) {
                    if (response.status == "1") {
                        if($scope.page == 1){
                            if(response.data.list){
                                $scope.shareList = response.data.list;
                                if($scope.shareList.length == 0){
                                    $scope.noData = true;
                                }
                            }else{
                                $scope.noData = true;
                            }
                            setTimeout(function(){$scope.initIScroll();},500);
                        } else {
                            $scope.shareList = $scope.shareList.concat(response.data.list);
                            setTimeout(function(){$scope.mScroll.refresh();},500);
                        }
                        if(response.data.list && response.data.list.length == $scope.size){
                            $scope.haveMore = true;
                        } else {
                            $scope.haveMore = false;
                        }
                        setTimeout(function(){
                            $("a").on("touchstart", function(e){
                                $(e.target).addClass('box-shadow-style');
                            });
                            $("a").on("touchmove", function(e){
                                $(e.target).removeClass('box-shadow-style');
                            });
                            $("a").on("touchend", function(e){
                                $(e.target).removeClass('box-shadow-style');
                            });
                        }, 500);
                    } else {
                        $rootScope.showAlert(response.message);
                    }
                    hideLoading();
                })
                .error(function (response, status, headers, config) {
                    hideLoading();
                });
        };
        $scope.get_shaidan_list(1, 10);
    })
    //商品细节页
    .controller('productDetailCtrl', function ($scope, $http, $location, $rootScope) {
        document.title = "商品详情";
        $('body').scrollTop(0);
        $("#headerId").hide();
        $("#body_top").css('height','0px');
        $("#shopList").hide();
        $scope.notLogin = false;			//无法登陆
        $scope.notJoin = false;				//没有参加
        $scope.isJoin = false;				//已参加
        $scope.havaWinner = false;			//是否揭晓出获奖者
        $scope.canBuy = true;				//是否可以购买

        $scope.id = $location.search()['id'];				//本期产品的id
        /*$scope.get_time = $location.search()['endTime'];
        if(!isNull($scope.get_time)){
        	$scope.endTime =$scope.date_time($scope.get_time)*1000;//本期产品的结束时间   格式:  2016/06/17 11:05:02  转换成精确到毫秒的时间戳
        }*/

        $scope.uid = 0;					//默认用户没有登陆
        $scope.notLogin = true;
        if(!isNull($rootScope.holdUID)){
            $scope.uid = $rootScope.holdUID;
            $scope.notLogin = false;
        }

        $scope.backTo = function(){
            window.history.go(-1);
        };
        //跳转到获取   云购码的页面
        $scope.gorecorde = function(gid,winUid,qishu,user_code){
        	//console.log(gid,winUid,qishu,user_code);
            if(gid>0){
            	var url='/tab/gorecorde?gid='+gid+'&uid='+winUid+'&qishu='+qishu+'&user_code='+user_code;
                $location.url(url);
                return false;
            }
        };
        $scope.showAll = function(code){
            $rootScope.showAlert($scope.allCode, 0, code);
        };

        $("#footbar").hide();
        $("#shopList").css("bottom", "10px");

        setTimeout(function(){
            $("a").on("touchstart", function(e){
                $(e.target).addClass('box-shadow-style');
            });
            $("a").on("touchmove", function(e){
                $(e.target).removeClass('box-shadow-style');
            });
            $("a").on("touchend", function(e){
                $(e.target).removeClass('box-shadow-style');
            });
        }, 500);

        $scope.noData = false;				//是否有参与记录

        $scope.page = 1;					//默认获取的参与人信息页码
        $scope.size = 10;					//默认获取的参与人信息条数
        $scope.mScroll = "";
        $scope.isPullUp = false;
        $scope.haveMore = false;
        $scope.isTime = false;				//是否倒计时
        $scope.initIScroll = function(){
            if($scope.mScroll instanceof iScroll){
                $scope.mScroll.destroy();
            }
            $("#scroller_product_detail").height($(window).height() - 90);

            $scope.mScroll = new iScroll("scroller_product_detail",{
                hScrollbar:false,
                vScrollbar:false,
                onScrollMove: function(){
                    if(this.y <= (this.maxScrollY - 60)){
                        $scope.isPullUp = true;
                    }
                },
                onScrollEnd: function(){
                    if($scope.isPullUp && $scope.haveMore){
                        $scope.page++;
                        $scope.isPullUp = false;
                        $scope.get_record_list($scope.page, $scope.size);
                    }
                    if(this.y < -800){
                        $('#proId').show();
                    }else{
                        $('#proId').hide();
                    }
                }
            });
        };
        $('#proId').click(function(){
            $scope.mScroll.scrollTo(0, 0, 0);
        });

        $scope.imgTouchMove = function(){
            var slidePage = {};
            $('#carousel').on('touchstart', function(e) {
                var touch = e.originalEvent.targetTouches[0];
                slidePage.x = touch.pageX;
            });
            $('#carousel').on('touchend', function(e) {
                var touch = e.originalEvent.changedTouches[0];
                var x = touch.pageX;
                if(x - parseInt(slidePage.x) > 0){
                    //从左往右
                    $('#carousel').carousel('prev');
                } else {
                    //从右往左
                    $('#carousel').carousel('next');
                }
            });
        };

		$scope.get_win_member_info = function(e){
			if($scope.status_public==1){
				return false;
			}
			var partner = "ZLAPITOH5WAP";
	        var timestamp = Date.parse(new Date())/1000;
	        var sign = $.md5(partner + timestamp + key);
	        $http({method: 'POST', data: {partner: partner, timestamp: timestamp, sign: sign, id: $scope.id}, url: "../index.php/yunapi/goods/get_win_member_info"})
	            .success(function (response, status, headers, config) {
	                console.log(response);
                    console.log('kk');
	                if (response.status == "1") {
	                    $scope.isTime = false;
	                    $scope.havaWinner = true;
	                    $scope.waiting = false;
	                    $scope.status_public=1;
	                    $scope.goodsInfo.q_user_code = response.data.q_user_code;
	                    $scope.goodsInfo.q_user = response.data;
	                    $scope.winUid = response.data.uid;
	                } else {

	                }
	            })
	            .error(function (response, status, headers, config) {

	            });

		};
        //$scope.isFirst = true;

		//获取商品信息

        $scope.someCode = [];
        $scope.allCode = "";			//所有的云购买
        $scope.waiting = true;

        $scope.status_public=0;			//当前模块的全局变量  用来停止由于循环查询导致的无限查询

        $scope.get_goods_info = function () {
            showLoading();
            var partner = "ZLAPITOH5WAP";
            var timestamp = Date.parse(new Date())/1000;
            var sign = $.md5(partner + timestamp + key);
            $http({method: 'POST', data: {partner: partner, timestamp: timestamp, sign: sign, id: $scope.id, uid: $scope.uid}, url: "../index.php/yunapi/goods/get_goods_info"})
                .success(function (response, status, headers, config) {

                   	//console.log(response);
                    if (response.status == "1") {
                    	//获取前后几期列表
						$scope.qishuList = response.qishu;
						$scope.nowqishu = function(qishu){
							if(qishu == $scope.id){
								return 'now';
							}
						};
						//商品信息
                        $scope.goodsInfo = response.data;
                        //第一期商品ID
                        $scope.sid 	= response.data['sid'];
                        //默认当前商品没有中奖人
                        $scope.havaWinner = false;
                        $scope.canBuy = false;			//默认当前产品不可以可以购买
                        $scope.isTime = false;			//默认不显示倒计时
                        $scope.waiting= false;			//默认不需要等待
						//$scope.goodsInfo.tag判断当前商品是否已经揭晓
                        if($scope.goodsInfo.tag == 0){
                            $scope.tag = "已揭晓";
                            $scope.havaWinner = true;
                            $scope.winUid = response.data.q_user.uid;
                            $scope.user_code = $scope.goodsInfo['q_user_code'];
                            $scope.status_public=1;
                        } else if($scope.goodsInfo.tag == 1){
                            $scope.tag = "进行中";
                            $scope.canBuy = true;
                            $("#shopList").show();
                            $scope.status_public=1;
                        } else if($scope.goodsInfo.tag == 2){
                            $scope.tag = "倒计时";
                            $scope.isTime = true;
                            $scope.waiting = true;
                            $scope.test_time_sec = response.data.test_time;					//倒计时的  【秒数】
                            var test_time_msec = (new Date().getTime())+(parseInt($scope.test_time_sec))*1000;//倒计时时间戳 毫秒
							//console.log($scope.test_time_sec);
							setTimeout(function(){
                            	//每隔两秒查询一次，如果有结果就停止
	                            var chaxun = setInterval(function(){
	                            	if($scope.status_public==1){
										clearInterval(chaxun);
									}
	                                $scope.get_win_member_info();
	                            },2000);
                            },$scope.test_time_sec*1000);
                            var set_time_out = setTimeout(function(){
								//console.log('倒计时时间戳：'+test_time_msec);
								$("#fnTimeCountDown").fnTimeCountDown(test_time_msec,function(){});
                            },500);

                        } else if($scope.goodsInfo.tag == 3){
                            $scope.tag = "期满";
                        }
                        if($scope.goodsInfo.curr_uinfo.is_join == 0){
                            if(!$scope.notLogin){
                            	//参与本期
                                $scope.notJoin = true;
                            }
                        } else if($scope.goodsInfo.curr_uinfo.is_join == 1){
                        	//参与本期
                            $scope.isJoin = true;
                            $scope.someCode = $scope.goodsInfo.curr_uinfo.goucode.split(",");
                            $scope.allCode = $scope.goodsInfo.curr_uinfo.goucode_all;
                        }
                        //当前期数
                        $scope.qishu = response.data.qishu;
                        $scope.isten = response.data.is_ten;
                        //获取本期商品的所有参与记录
                        $scope.get_record_list(1, 10);
                        setTimeout(function(){
                            $("a").on("touchstart", function(e){
                                $(e.target).addClass('box-shadow-style');
                            });
                            $("a").on("touchmove", function(e){
                                $(e.target).removeClass('box-shadow-style');
                            });
                            $("a").on("touchend", function(e){
                                $(e.target).removeClass('box-shadow-style');
                            });
                        }, 500);
                        hideLoading();
                    } else {
                        hideLoading();
                        $rootScope.showAlert(response.message);
                    }
                })
                .error(function (response, status, headers, config) {
                    hideLoading();
                });

        };

        $scope.recordList = [];
        //获取所有的参与记录
        $scope.get_record_list = function (page, size) {
            showLoading();
            var partner = "ZLAPITOH5WAP";
            var timestamp = Date.parse(new Date())/1000;
            var sign = $.md5(partner + timestamp + key);
            $http({method: 'POST', data: {partner: partner, timestamp: timestamp, sign: sign, shopid: $scope.id, qishu: $scope.qishu, page: page, size: size}, url: "../index.php/yunapi/goods/get_record_list"})
                .success(function (response, status, headers, config) {
                    /*var str = JSON.stringify(response);
                    console.log("------------>", str);*/
                    if (response.status == "1") {
                        if($scope.page == 1){
                            if(response.data.list){
                                $scope.recordList = response.data.list;
                                if($scope.recordList.length == 0){
                                    $scope.noData = true;
                                }
                            }else{
                                $scope.noData = true;
                            }
                            setTimeout(function(){$scope.initIScroll();},500);
                        } else {
                            $scope.recordList = $scope.recordList.concat(response.data.list);
                            setTimeout(function(){$scope.mScroll.refresh();},500);
                        }
                        if(response.data.list && response.data.list.length == $scope.size){
                            $scope.haveMore = true;
                        } else {
                            $scope.haveMore = false;
                        }
                        setTimeout(function(){
                            $("a").on("touchstart", function(e){
                                $(e.target).addClass('box-shadow-style');
                            });
                            $("a").on("touchmove", function(e){
                                $(e.target).removeClass('box-shadow-style');
                            });
                            $("a").on("touchend", function(e){
                                $(e.target).removeClass('box-shadow-style');
                            });
                        }, 500);
                    } else {
                        $rootScope.showAlert(response.message);
                    }
                    hideLoading();
                })
                .error(function (response, status, headers, config) {
                    hideLoading();
                });
        };
        $scope.get_more_number = function (code) {
            showLoading();
            var partner = "ZLAPITOH5WAP";
            var timestamp = Date.parse(new Date())/1000;
            var sign = $.md5(partner + timestamp + key);
            $http({method: 'POST', data: {partner: partner, timestamp: timestamp, sign: sign, id: $scope.id, uid: $scope.winUid, qishu: $scope.qishu}, url: "../index.php/yunapi/goods/get_more_number"})
                .success(function (response, status, headers, config) {
                    /*var str = JSON.stringify(response);
                     $rootScope.showAlert(str);*/
                    if (response.status == "1") {
                        $rootScope.showAlert(response.data.goucode, 0, code);
                        setTimeout(function(){
                            $("a").on("touchstart", function(e){
                                $(e.target).addClass('box-shadow-style');
                            });
                            $("a").on("touchmove", function(e){
                                $(e.target).removeClass('box-shadow-style');
                            });
                            $("a").on("touchend", function(e){
                                $(e.target).removeClass('box-shadow-style');
                            });
                        }, 500);
                    } else {
                        $rootScope.showAlert(response.message);
                    }
                    hideLoading();
                })
                .error(function (response, status, headers, config) {
                    hideLoading();
                });
        };
        $scope.imgTouchMove();
        $scope.get_goods_info();

    })
    //中奖用户所有的云购码
    .controller('gorecordeCtrl', function ($scope,$http,$location, $rootScope) {
        document.title = "商品详情";
        $('body').scrollTop(0);
        $scope.backTo = function(){
            window.history.go(-1);
        };
        //console.log($location);
        $scope.gid = $location.search()['gid'];     //商品id
        $scope.uid = $location.search()['uid'];     //用户id
        $scope.qishu = $location.search()['qishu']; //期数
        $scope.code = $location.search()['user_code']; //期数
        $scope.type = $location.search()['type']; //期数
        $scope.recordList = [];
        //console.log($scope.gid);
        //return false;
        $scope.get_more_number = function (code) {
            showLoading();
            var partner = "ZLAPITOH5WAP";
            var timestamp = Date.parse(new Date())/1000;
            var sign = $.md5(partner + timestamp + key);
            $http({method: 'POST', data: {partner: partner, timestamp: timestamp, sign: sign, id: $scope.gid, uid: $scope.uid, qishu: $scope.qishu,code:$scope.code}, url: "../index.php/yunapi/goods/get_more_number"})
                .success(function (response, status, headers, config) {

                    if (response.status == "1") {
                        $scope.recordeList=response.data.goucode.split(",");

                    } else {
                        $rootScope.showAlert(response.message);
                    }
                    hideLoading();
                })
                .error(function (response, status, headers, config) {

                });
            hideLoading();
        };

        $scope.get_gou_list = function (code) {
            showLoading();
            var partner = "ZLAPITOH5WAP";
            var timestamp = Date.parse(new Date())/1000;
            var sign = $.md5(partner + timestamp + key);
            $http({method: 'POST', data: {partner: partner, timestamp: timestamp, sign: sign, id: $scope.gid, uid: $scope.uid,code:$scope.code}, url: "../index.php/yunapi/goods/get_gou_list"})
                .success(function (response, status, headers, config) {

                    if (response.status == "1") {
                        $scope.recordeList=response.data.goucode.split(",");
                    } else {
                        $rootScope.showAlert(response.message);
                    }
                    hideLoading();
                })
                .error(function (response, status, headers, config) {

                });
            hideLoading();
        };
        if($scope.type=='self'){
            $scope.get_gou_list();
        }else{
            $scope.get_more_number();
        }



    })
    //商品详细介绍信息
    .controller('productInfoCtrl', function ($scope, $http, $location, $rootScope) {
        document.title = "商品详情";
        $('body').scrollTop(0);
        $("#headerId").hide();
        $("#body_top").css('height','0px');
        $("#shopList").hide();
        $scope.backTo = function(){
            window.history.go(-1);
        };

        setTimeout(function(){
            $("a").on("touchstart", function(e){
                $(e.target).addClass('box-shadow-style');
            });
            $("a").on("touchmove", function(e){
                $(e.target).removeClass('box-shadow-style');
            });
            $("a").on("touchend", function(e){
                $(e.target).removeClass('box-shadow-style');
            });
        }, 500);

        $scope.id = $location.search()['id'];
        $scope.get_goods_pic_content = function () {
            showLoading();
            var partner = "ZLAPITOH5WAP";
            var timestamp = Date.parse(new Date())/1000;
            var sign = $.md5(partner + timestamp + key);
            $http({method: 'POST', data: {partner: partner, timestamp: timestamp, sign: sign, id: $scope.id}, url: "../index.php/yunapi/goods/get_goods_pic_content"})
                .success(function (response, status, headers, config) {
                    /*var str = JSON.stringify(response);
                    $rootScope.showAlert(str);*/
                    if (response.status == "1") {
                        $("#productInfoData").append(response.data.content);
                        setTimeout(function(){
                            $("a").on("touchstart", function(e){
                                $(e.target).addClass('box-shadow-style');
                            });
                            $("a").on("touchmove", function(e){
                                $(e.target).removeClass('box-shadow-style');
                            });
                            $("a").on("touchend", function(e){
                                $(e.target).removeClass('box-shadow-style');
                            });
                        }, 500);
                    } else {
                        $rootScope.showAlert(response.message);
                    }
                    hideLoading();
                })
                .error(function (response, status, headers, config) {
                    hideLoading();
                });
        };
        $scope.get_goods_pic_content();
    })
    //期数列表页
    .controller('qishuListCtrl',function($scope, $http, $location, $rootScope){
    	//console.log($location);return false;
    	$scope.sid = $location.search()['sid'];
    	if($scope.sid>0){
    		//console.log($http);return false;
    	}else{
    		$rootScope.showAlert('非法访问！');
    		return false;
    	}
		$scope.qishu_text = 1;
		$scope.gomodel = function(qishu_text){
			if(qishu_text > 0){
				var partner = "ZLAPITOH5WAP";
            	var timestamp = Date.parse(new Date())/1000;
            	var sign = $.md5(partner + timestamp + key);
				$http({method: 'POST', data: {partner: partner, timestamp: timestamp, sign: sign, sid: $scope.sid,qishu:qishu_text}, url: "../index.php/yunapi/goods/get_qishu_id"})
                .success(function(response){

                	if(response.status == 1){

                		$scope.id = response.data['id'];
                		if(response.data['id'] >0){
                			$scope.godetail(response.data['id']);
                		}else{
                			$rootScope.showAlert("没有这期");
                		}
                	}else{
                		$rootScope.showAlert(response.message);
                	}
                })
                .error(function(){
                	$rootScope.showAlert('请检查网络');
                });
			}else{
				$rootScope.showAlert("请输入合适的数字");
			}
		};
		$scope.godetail = function(id){
			$location.url("/tab/productDetail?id="+id);
		};
        document.title = "往期揭晓";
        $('body').scrollTop(0);
        $("#headerId").hide();
        $("#body_top").css('height','0px');
        $("#shopList").hide();
        $scope.backTo = function(){
            window.history.go(-1);
        };

        setTimeout(function(){
            $("a").on("touchstart", function(e){
                $(e.target).addClass('box-shadow-style');
            });
            $("a").on("touchmove", function(e){
                $(e.target).removeClass('box-shadow-style');
            });
            $("a").on("touchend", function(e){
                $(e.target).removeClass('box-shadow-style');
            });
        }, 500);

        $scope.noData = false;

        $scope.page = 1;
        $scope.size = 9;
        $scope.mScroll = "";
        $scope.isPullUp = false;
        $scope.haveMore = false;
        $scope.initIScroll = function(){
            if($scope.mScroll instanceof iScroll){
                $scope.mScroll.destroy();
            }
            $("#scroller_passed_publish").height($(window).height() - 45);

            $scope.mScroll = new iScroll("scroller_passed_publish",{
                hScrollbar:false,
                vScrollbar:false,
                onScrollMove: function(){
                	//console.log(this.maxScrollY);
                	//console.log(this.y );
                    if(this.y <= (this.maxScrollY - 60)){
                        $scope.isPullUp = true;
                    }
                },
                onScrollEnd: function(){
                    if($scope.isPullUp && $scope.haveMore){
                        $scope.page++;
                        $scope.isPullUp = false;
                        $scope.get_old_lottery($scope.page, $scope.size);
                    }
                    if(this.y < -800){
                        $('#proId').show();
                    }else{
                        $('#proId').hide();
                    }
                }
            });
        };
        $('#proId').click(function(){
            $scope.mScroll.scrollTo(0, 0, 0);
        });
        $scope.finishedList = [];
        $scope.progressList = [];
        $scope.newestList = [];
        $scope.sid = $location.search()['sid'];
        $scope.get_old_lottery = function (page, size) {
            showLoading();
            var partner = "ZLAPITOH5WAP";
            var timestamp = Date.parse(new Date())/1000;
            var sign = $.md5(partner + timestamp + key);
            $http({method: 'POST', data: {partner: partner, timestamp: timestamp, sign: sign, page: page, size: size, sid: $scope.sid}, url: "../index.php/yunapi/goods/get_old_lottery"})
                .success(function (response, status, headers, config) {
                    /*var str = JSON.stringify(response);
                    $rootScope.showAlert(str);*/
                    if (response.status == "1") {
                        var list = response.data.list;
                        //console.log(list);
                        for(var i = 0; i < list.length; i++){
                            if(list[i].tag == 0){
                                $scope.finishedList.push(list[i]);
                            } else if(list[i].tag == 2){
                                $scope.progressList.push(list[i]);
                            }
                            if(list[i].tag == 1){
                            	$scope.newestList.push(list[i]);
                            }
                        }
                        if($scope.page == 1){
                            if($scope.finishedList.length == 0 && $scope.progressList.length == 0){
                                $scope.noData = true;
                            }
                            setTimeout(function(){$scope.initIScroll();},500);
                        } else {
                            setTimeout(function(){$scope.mScroll.refresh();},500);
                        }
                        if(list.length == $scope.size){
                            $scope.haveMore = true;
                        } else {
                            $scope.haveMore = false;
                        }
                        setTimeout(function(){
                            $("a").on("touchstart", function(e){
                                $(e.target).addClass('box-shadow-style');
                            });
                            $("a").on("touchmove", function(e){
                                $(e.target).removeClass('box-shadow-style');
                            });
                            $("a").on("touchend", function(e){
                                $(e.target).removeClass('box-shadow-style');
                            });
                        }, 500);
                    } else {
                        $rootScope.showAlert(response.message);
                    }
                    hideLoading();
                })
                .error(function (response, status, headers, config) {
                    hideLoading();
                });
        };
        $scope.get_old_lottery(1,9);
    })
	//往期揭晓
    .controller('passedPublishCtrl', function ($scope, $http, $location, $rootScope) {
        document.title = "往期揭晓";
        $('body').scrollTop(0);
        $("#headerId").hide();
        $("#body_top").css('height','0px');
        $("#shopList").hide();
        $scope.backTo = function(){
            window.history.go(-1);
        };

        setTimeout(function(){
            $("a").on("touchstart", function(e){
                $(e.target).addClass('box-shadow-style');
            });
            $("a").on("touchmove", function(e){
                $(e.target).removeClass('box-shadow-style');
            });
            $("a").on("touchend", function(e){
                $(e.target).removeClass('box-shadow-style');
            });
        }, 500);

        $scope.noData = false;

        $scope.page = 1;
        $scope.size = 10;
        $scope.mScroll = "";
        $scope.isPullUp = false;
        $scope.haveMore = false;
        $scope.initIScroll = function(){
            if($scope.mScroll instanceof iScroll){
                $scope.mScroll.destroy();
            }
            $("#scroller_passed_publish").height($(window).height() - 45);

            $scope.mScroll = new iScroll("scroller_passed_publish",{
                hScrollbar:false,
                vScrollbar:false,
                onScrollMove: function(){
                	//console.log(this.maxScrollY);
                	//console.log(this.y );
                    if(this.y <= (this.maxScrollY - 60)){
                        $scope.isPullUp = true;
                    }
                },
                onScrollEnd: function(){
                    if($scope.isPullUp && $scope.haveMore){
                        $scope.page++;
                        $scope.isPullUp = false;
                        $scope.get_old_lottery($scope.page, $scope.size);
                    }
                    if(this.y < -800){
                        $('#proId').show();
                    }else{
                        $('#proId').hide();
                    }
                }
            });
        };
        $('#proId').click(function(){
            $scope.mScroll.scrollTo(0, 0, 0);
        });
        $scope.finishedList = [];
        $scope.progressList = [];
        $scope.sid = $location.search()['sid'];
        $scope.get_old_lottery = function (page, size) {
            showLoading();
            var partner = "ZLAPITOH5WAP";
            var timestamp = Date.parse(new Date())/1000;
            var sign = $.md5(partner + timestamp + key);
            $http({method: 'POST', data: {partner: partner, timestamp: timestamp, sign: sign, page: page, size: size, sid: $scope.sid}, url: "../index.php/yunapi/goods/get_old_lottery"})
                .success(function (response, status, headers, config) {
                    /*var str = JSON.stringify(response);
                    $rootScope.showAlert(str);*/
                    if (response.status == "1") {
                        var list = response.data.list;
                        for(var i = 0; i < list.length; i++){
                            if(list[i].tag == 0){
                                $scope.finishedList.push(list[i]);
                            } else if(list[i].tag == 2){
                                $scope.progressList.push(list[i]);
                            }
                        }
                        if($scope.page == 1){
                            if($scope.finishedList.length == 0 && $scope.progressList.length == 0){
                                $scope.noData = true;
                            }
                            setTimeout(function(){$scope.initIScroll();},500);
                        } else {
                            setTimeout(function(){$scope.mScroll.refresh();},500);
                        }
                        if(list.length == $scope.size){
                            $scope.haveMore = true;
                        } else {
                            $scope.haveMore = false;
                        }
                        setTimeout(function(){
                            $("a").on("touchstart", function(e){
                                $(e.target).addClass('box-shadow-style');
                            });
                            $("a").on("touchmove", function(e){
                                $(e.target).removeClass('box-shadow-style');
                            });
                            $("a").on("touchend", function(e){
                                $(e.target).removeClass('box-shadow-style');
                            });
                        }, 500);
                    } else {
                        $rootScope.showAlert(response.message);
                    }
                    hideLoading();
                })
                .error(function (response, status, headers, config) {
                    hideLoading();
                });
        };
        $scope.get_old_lottery(1, 10);
    })
    //晒单分享
    .controller('productShareCtrl', function ($scope, $http, $location, $rootScope) {
        document.title = "晒单分享";
        $('body').scrollTop(0);
        $("#headerId").hide();
        $("#body_top").css('height','0px');
        $("#shopList").hide();
        $scope.backTo = function(){
            window.history.go(-1);
        };

        setTimeout(function(){
            $("a").on("touchstart", function(e){
                $(e.target).addClass('box-shadow-style');
            });
            $("a").on("touchmove", function(e){
                $(e.target).removeClass('box-shadow-style');
            });
            $("a").on("touchend", function(e){
                $(e.target).removeClass('box-shadow-style');
            });
        }, 500);

        $scope.noData = false;

        $scope.page = 1;
        $scope.size = 10;
        $scope.mScroll = "";
        $scope.isPullUp = false;
        $scope.haveMore = false;
        $scope.initIScroll = function(){
            if($scope.mScroll instanceof iScroll){
                $scope.mScroll.destroy();
            }
            $("#scroller_product_share").height($(window).height() - 45);

            $scope.mScroll = new iScroll("scroller_product_share",{
                hScrollbar:false,
                vScrollbar:false,
                onScrollMove: function(){
                    if(this.y <= (this.maxScrollY - 60)){
                        $scope.isPullUp = true;
                    }
                },
                onScrollEnd: function(){
                    if($scope.isPullUp && $scope.haveMore){
                        $scope.page++;
                        $scope.isPullUp = false;
                        if(!isNull($scope.uid)){
                            $scope.get_member_shaidan($scope.page, $scope.size);
                        } else if(!isNull($scope.sid)){
                            $scope.get_shaidan_list($scope.page, $scope.size);
                        }
                    }
                    if(this.y < -800){
                        $('#proId').show();
                    }else{
                        $('#proId').hide();
                    }
                }
            });
        };
        $('#proId').click(function(){
            $scope.mScroll.scrollTo(0, 0, 0);
        });

        $scope.uid = $location.search()['uid'];
        $scope.sid = $location.search()['sid'];
        $scope.shareList = [];

        $scope.get_member_shaidan = function (page, size) {
            showLoading();
            var partner = "ZLAPITOH5WAP";
            var timestamp = Date.parse(new Date())/1000;
            var sign = $.md5(partner + timestamp + key);
            $http({method: 'POST', data: {partner: partner, timestamp: timestamp, sign: sign, uid: $scope.uid, page: page, size: size}, url: "../index.php/yunapi/member/get_member_shaidan"})
                .success(function (response, status, headers, config) {
                    /*var str = JSON.stringify(response);
                     $rootScope.showAlert(str);*/
                    if (response.status == "1") {
                        if($scope.page == 1){
                            if(response.data){
                                $scope.shareList = response.data;
                                if($scope.shareList.length == 0){
                                    $scope.noData = true;
                                }
                            }else{
                                $scope.noData = true;
                            }
                            setTimeout(function(){$scope.initIScroll();},500);
                        } else {
                            $scope.shareList = $scope.shareList.concat(response.data);
                            setTimeout(function(){$scope.mScroll.refresh();},500);
                        }
                        if(response.data.length == $scope.size){
                            $scope.haveMore = true;
                        } else {
                            $scope.haveMore = false;
                        }
                        setTimeout(function(){
                            $("a").on("touchstart", function(e){
                                $(e.target).addClass('box-shadow-style');
                            });
                            $("a").on("touchmove", function(e){
                                $(e.target).removeClass('box-shadow-style');
                            });
                            $("a").on("touchend", function(e){
                                $(e.target).removeClass('box-shadow-style');
                            });
                        }, 500);
                    } else {
                        $rootScope.showAlert(response.message);
                    }
                    hideLoading();
                })
                .error(function (response, status, headers, config) {
                    hideLoading();
                });
        };
        $scope.get_shaidan_list = function (page, size) {
            showLoading();
            var partner = "ZLAPITOH5WAP";
            var timestamp = Date.parse(new Date())/1000;
            var sign = $.md5(partner + timestamp + key);
            $http({method: 'POST', data: {partner: partner, timestamp: timestamp, sign: sign, sid: $scope.sid, page: page, size: size}, url: "../index.php/yunapi/goods/get_shaidan_list"})
                .success(function (response, status, headers, config) {
                    /*var str = JSON.stringify(response);
                     $rootScope.showAlert(str);*/
                    if (response.status == "1") {
                        if($scope.page == 1){
                            if(response.data.list){
                                $scope.shareList = response.data.list;
                                if($scope.shareList.length == 0){
                                    $scope.noData = true;
                                }
                            }else{
                                $scope.noData = true;
                            }
                            setTimeout(function(){$scope.initIScroll();},500);
                        } else {
                            $scope.shareList = $scope.shareList.concat(response.data.list);
                            setTimeout(function(){$scope.mScroll.refresh();},500);
                        }
                        if(response.data.length == $scope.size){
                            $scope.haveMore = true;
                        } else {
                            $scope.haveMore = false;
                        }
                        setTimeout(function(){
                            $("a").on("touchstart", function(e){
                                $(e.target).addClass('box-shadow-style');
                            });
                            $("a").on("touchmove", function(e){
                                $(e.target).removeClass('box-shadow-style');
                            });
                            $("a").on("touchend", function(e){
                                $(e.target).removeClass('box-shadow-style');
                            });
                        }, 500);
                    } else {
                        $rootScope.showAlert(response.message);
                    }
                    hideLoading();
                })
                .error(function (response, status, headers, config) {
                    hideLoading();
                });
        };
        if(!isNull($scope.uid)){
            $scope.get_member_shaidan(1, 10);
        } else if(!isNull($scope.sid)){
            $scope.get_shaidan_list(1, 10);
        }
    })
    //登陆
    .controller('loginCtrl', function ($scope, $http, $location, $rootScope) {
        $rootScope.holdUID=getCookie("holdUID");
        if(!isNull($rootScope.holdUID)){
            $location.url('/tab/personCenter?uid=' + $rootScope.holdUID);
        }
        document.title = "登陆";
        $('body').scrollTop(0);
        $("#headerId").hide();
        $("#body_top").css('height','0px');
        $("#shopList").hide();
        $scope.backTo = function(){
            window.history.go(-1);
        };

        setTimeout(function(){
            $("a").on("touchstart", function(e){
                $(e.target).addClass('box-shadow-style');
            });
            $("a").on("touchmove", function(e){
                $(e.target).removeClass('box-shadow-style');
            });
            $("a").on("touchend", function(e){
                $(e.target).removeClass('box-shadow-style');
            });
        }, 500);

        $scope.is_weixin = function(){
            var ua = navigator.userAgent.toLowerCase();
            if(ua.match(/MicroMessenger/i)=="micromessenger") {
                return true;
            } else {
                return false;
            }
        };
        console.log($scope.is_weixin());
        $scope.do_login = function ($event) {
            /*$($event.target).css('box-shadow', '0 0 15px #000');
            setTimeout(function(){
                $($event.target).css('box-shadow', 'none');
            }, 160);*/
            if(isNull($scope.account)){
                $rootScope.showAlert("请输入用户名");
                return;
            }
            if(isNull($scope.password)){
                $rootScope.showAlert("请输入密码");
                return;
            }
            showLoading();
            var partner = "ZLAPITOH5WAP";
            var timestamp = Date.parse(new Date())/1000;
            var sign = $.md5(partner + timestamp + key);
            $http({method: 'POST', data: {partner: partner, timestamp: timestamp, sign: sign, username: $scope.account, password: $scope.password}, url: "../index.php/yunapi/member/do_login"})
                .success(function (response, status, headers, config) {
                    if (response.status == "1") {
                    	addCookie('holdUID',response.data.uid);
                        //$scope.$emit("uid", response.data.uid);
                        $rootScope.holdUID = response.data.uid;
                        //console.log("loginCtrl", response.data.uid);
                        var url = '/tab/personCenter?uid=' + response.data.uid;
                        $location.url(url);
                    } else {
                        $rootScope.showAlert(response.message);
                    }
                    hideLoading();
                })
                .error(function (response, status, headers, config) {
                    hideLoading();
                });
        };


        $scope.qqlogin = function(){
        	window.location.href="http://www.yz-db.com/Zapi/wapqqlogin/init";
        };



    })
    //修改密码
    .controller('updatePasswordCtrl', function ($scope, $http, $rootScope) {
        document.title = "忘记密码";
        $('body').scrollTop(0);
        $("#headerId").hide();
        $("#body_top").css('height','0px');
        $("#shopList").hide();
        $scope.backTo = function(){
            window.history.go(-1);
        };

        setTimeout(function(){
            $("a").on("touchstart", function(e){
                $(e.target).addClass('box-shadow-style');
            });
            $("a").on("touchmove", function(e){
                $(e.target).removeClass('box-shadow-style');
            });
            $("a").on("touchend", function(e){
                $(e.target).removeClass('box-shadow-style');
            });
        }, 500);

        $scope.checked = false;
        $scope.codeText = "获取验证码";
        $scope.nowtime = 60;
        $scope.setTime = function(){
            if($scope.nowtime == 0){
                clearInterval($scope.shut);
                $scope.checked = false;
                $scope.codeText = "重新获取";
                $scope.$apply();
            } else {
                $scope.nowtime --;
                $scope.codeText = "("+$scope.nowtime+"S)重新获取";
                $scope.$apply();
            }
        };

        $scope.do_send_found_code = function () {
            if(isNull($scope.mobile)){
                $rootScope.showAlert("请输入手机号码");
                return;
            } else if($scope.mobile.length != 11){
                $rootScope.showAlert("请输入有效的手机号码");
                return;
            } else {
                var reg = /^(1[0-9]{10})$/;
                if(!reg.test($scope.mobile)){
                    $rootScope.showAlert("请输入有效的手机号码");
                    return;
                }
            }
            showLoading();
            var partner = "ZLAPITOH5WAP";
            var timestamp = Date.parse(new Date())/1000;
            var sign = $.md5(partner + timestamp + key);
            $http({method: 'POST', data: {partner: partner, timestamp: timestamp, sign: sign, mobile: $scope.mobile}, url: "../index.php/yunapi/member/do_send_found_code"})
                .success(function (response, status, headers, config) {
                    if (response.status == "1") {
                        $scope.checked = true;
                        $scope.nowtime = 60;
                        $scope.shut = setInterval($scope.setTime,1000);
                        $rootScope.showAlert("验证码发送成功");
                    } else {
                        $rootScope.showAlert(response.message);
                    }
                    hideLoading();
                })
                .error(function (response, status, headers, config) {
                    hideLoading();
                });
        };
        $scope.do_save_new_password = function ($event) {
            /*$($event.target).css('color', '#f81000');
            setTimeout(function(){
                $($event.target).css('color', '#0079fe');
            }, 160);*/
            if(isNull($scope.mobile)){
                $rootScope.showAlert("请输入手机号码");
                return;
            } else if($scope.mobile.length != 11){
                $rootScope.showAlert("请输入有效的手机号码");
                return;
            } else {
                var reg = /^(1[0-9]{10})$/;
                if(!reg.test($scope.mobile)){
                    $rootScope.showAlert("请输入有效的手机号码");
                    return;
                }
            }
            if(isNull($scope.password)){
                $rootScope.showAlert("请输入密码");
                return;
            }
            if($scope.password.length < 6 || $scope.password.length > 16){
                $rootScope.showAlert("密码长度不在规定范围内(6~16)");
                return;
            }
            if(isNull($scope.code)){
                $rootScope.showAlert("请输入验证码");
                return;
            }
            showLoading();
            var partner = "ZLAPITOH5WAP";
            var timestamp = Date.parse(new Date())/1000;
            var sign = $.md5(partner + timestamp + key);
            $http({method: 'POST', data: {partner: partner, timestamp: timestamp, sign: sign, mobile: $scope.mobile, password: $scope.password, code: $scope.code}, url: "../index.php/yunapi/member/do_save_new_password"})
                .success(function (response, status, headers, config) {
                    if (response.status == "1") {
                        $('#updateModal').modal('show');
                    } else {
                        $rootScope.showAlert(response.message);
                    }
                    hideLoading();
                })
                .error(function (response, status, headers, config) {
                    hideLoading();
                });
        };
    })
    //注册
    .controller('rigisterCtrl', function ($scope, $http, $location, $rootScope) {
        document.title = "注册";
        $('body').scrollTop(0);
        $("#headerId").hide();
        $("#body_top").css('height','0px');
        $("#shopList").hide();
        $scope.checked = false;
        $scope.codeText = "获取验证码";
        $scope.backTo = function(){
            window.history.go(-1);
        };

        var su = getCookie('su');
        var sk = getCookie('sk');

        $scope.nowtime = 60;
        $scope.setTime = function(){
            if($scope.nowtime == 0){
                clearInterval($scope.shut);
                $scope.checked = false;
                $scope.codeText = "重新获取";
                $scope.$apply();
            } else {
                $scope.nowtime --;
                $scope.codeText = "("+$scope.nowtime+"S)重新获取";
                $scope.$apply();
            }
        };

        setTimeout(function(){
            $("a").on("touchstart", function(e){
                $(e.target).addClass('box-shadow-style');
            });
            $("a").on("touchmove", function(e){
                $(e.target).removeClass('box-shadow-style');
            });
            $("a").on("touchend", function(e){
                $(e.target).removeClass('box-shadow-style');
            });
        }, 500);

        $scope.do_send_mobile_code = function () {
            if(isNull($scope.mobile)){
                $rootScope.showAlert("请输入手机号码");
                return;
            } else if($scope.mobile.length != 11){
                $rootScope.showAlert("请输入有效的手机号码");
                return;
            } else {
                var reg = /^(1[0-9]{10})$/;
                if(!reg.test($scope.mobile)){
                    $rootScope.showAlert("请输入有效的手机号码");
                    return;
                }
            }
            showLoading();
            var partner = "ZLAPITOH5WAP";
            var timestamp = Date.parse(new Date())/1000;
            var sign = $.md5(partner + timestamp + key);
            $http({method: 'POST', data: {partner: partner, timestamp: timestamp, sign: sign, mobile: $scope.mobile}, url: "../index.php/yunapi/member/do_send_mobile_code"})
                .success(function (response, status, headers, config) {
                    /*var str = JSON.stringify(response);
                    $rootScope.showAlert(str);*/
                    if (response.status == "1") {
                        $scope.checked = true;
                        $scope.nowtime = 60;
                        $scope.shut = setInterval($scope.setTime,1000);
                        $rootScope.showAlert("验证码发送成功");
                    } else {
                        $rootScope.showAlert(response.message);
                    }
                    hideLoading();
                })
                .error(function (response, status, headers, config) {
                    hideLoading();
                });
        };

        $scope.yaoqing = $location.search()['yaoqing'];

        $scope.do_register = function ($event) {
            /*$($event.target).css('color', '#f81000');
            setTimeout(function(){
                $($event.target).css('color', '#0079fe');
            }, 160);*/
            var checkbox = document.getElementById("optionsRigister");
            if(!checkbox.checked){
                $rootScope.showAlert("您还没有同意服务协议");
                return;
            }
            if(isNull($scope.mobile)){
                $rootScope.showAlert("请输入手机号码");
                return;
            } else if($scope.mobile.length != 11){
                $rootScope.showAlert("请输入有效的手机号码");
                return;
            } else {
                var reg = /^(1[0-9]{10})$/;
                if(!reg.test($scope.mobile)){
                    $rootScope.showAlert("请输入有效的手机号码");
                    return;
                }
            }
            if(isNull($scope.password)){
                $rootScope.showAlert("请输入密码");
                return;
            }
            if($scope.password.length < 6 || $scope.password.length > 16){
                $rootScope.showAlert("密码长度不在规定范围内(6~16)");
                return;
            }
            if(isNull($scope.inputCode)){
                $rootScope.showAlert("请输入验证码");
                return;
            }
            if(isNull($scope.yaoqing)){
                $scope.yaoqing = "";
            }
            showLoading();
            var partner = "ZLAPITOH5WAP";
            var timestamp = Date.parse(new Date())/1000;
            var sign = $.md5(partner + timestamp + key);
            $http({method: 'POST', data: {partner: partner, timestamp: timestamp, sign: sign, mobile: $scope.mobile, password: $scope.password, code: $scope.inputCode, yaoqing: $scope.yaoqing,su:su,sk:sk}, url: "../index.php/yunapi/member/do_register"})
                .success(function (response, status, headers, config) {
                    if (response.status == "1") {
                        $("#rigisterModal").modal();
                    } else {
                        $rootScope.showAlert(response.message);
                    }
                    hideLoading();
                })
                .error(function (response, status, headers, config) {
                    hideLoading();
                });
        };
    })

    .controller('bindwxCtrl',function($scope, $http, $location, $rootScope){
        $('body').scrollTop(0);
        $("#headerId").hide();
        $("#body_top").css('height','0px');
        $("#shopList").hide();
    	$scope.openid 	= $location.search()['openid'];
    	$scope.type 	= $location.search()['type'];

    	if($scope.openid && $scope.type){
    		var go_url = "../index.php/yunapi/SecLogin/Sec_login_userinfo";
    	}else{
    		$location.url('/tab/login');return false;
    	}

    	$scope.get_wx_userinfo = function(){
    	    showLoading();
    		var partner = "ZLAPITOH5WAP";
            var timestamp = Date.parse(new Date())/1000;
            var sign = $.md5(partner + timestamp + key);
    		$http({
    			method:'POST',
    			data:{
    				partner: partner,
    				timestamp: timestamp,
    				sign: sign,
    				openid:$scope.openid,
    				type:$scope.type,
    			},
    			url:go_url,
    		})
    		.success(function(response, status, headers, config){
    			console.log(response);
    			if (response.status == "1") {
                   var data = response.data;
                   $scope.headimgurl  = data.headimgurl;	//微信头像
                   $scope.nickname = data.nickname;		//微信名字
                   hideLoading();
                } else {
                	hideLoading();
                    $rootScope.showAlert(response.message);
                }

    		})
    		.error(function(response, status, headers, config){
    			hideLoading();
    			$rootScope.showAlert('检查网络！');
    		});
    	};
		$scope.get_wx_userinfo();

    	$scope.do_bind = function(){
    		if(isNull($scope.mobile)){
    			$rootScope.showAlert('手机号不能为空！');
    			return;
    		}else if($scope.mobile.length != 11){
    			$rootScope.showAlert('手机号长度不正确！');
    			return;
    		}else {
                var reg = /^(1[0-9]{10})$/;
                if(!reg.test($scope.mobile)){
                    $rootScope.showAlert("请输入有效的手机号码");
                    return;
                }
            }
    		if(isNull($scope.password)){
    			$rootScope.showAlert('密码不能为空！');
    			return;
    		}
            if($scope.password.length < 6 || $scope.password.length > 20){
                $rootScope.showAlert("密码长度不在规定范围内");
                return;
            }
            showLoading();
    		var partner = "ZLAPITOH5WAP";
            var timestamp = Date.parse(new Date())/1000;
            var sign = $.md5(partner + timestamp + key);

    		$http({
    			method:'POST',
    			data:{
    				partner: partner,
    				timestamp: timestamp,
    				sign: sign,
    				openid:$scope.openid,
    				username:$scope.mobile,
    				password:$scope.password,
    				type:$scope.type,
    			},
    			url:"../index.php/yunapi/SecLogin/do_bind",
    		})
    		.success(function(response, status, headers, config){
    			console.log(response);
    			if (response.status == "1") {
    				$rootScope.holdUID = response.message.uid;
                    var url = '/tab/personCenter?uid=' + response.message.uid;
                    $location.url(url);
               } else {
                    $rootScope.showAlert(response.message);
                }
                hideLoading();
    		})
    		.error(function(response, status, headers, config){
    			hideLoading();
    			$rootScope.showAlert('检查网络！');

    		});


    	};
        $scope.go_home = function(){
            $location.url('/tab/home');
        }
        //双向数据绑定     倒计时
        $scope.checked  = false;
        $scope.codeText = "获取验证码";
        $scope.nowtime  = 120;
        $scope.setTime  = function(){

            if($scope.nowtime == 0){
                clearInterval($scope.shut);
                $scope.checked = false;
                $scope.codeText = "重新获取";
                $scope.$apply();
            } else {
                $scope.nowtime --;
                $scope.codeText = "("+$scope.nowtime+"S)重新获取";
                $scope.$apply();
            }
        };
        $scope.write_username = 1;
        $scope.write_password = 0;
        $scope.write_checkcode = 0;
        $scope.write_password_code = 0;
        $scope.check_user = function(){
            if(isNull($scope.mobile)){
                $rootScope.showAlert('手机号不能为空！');
                return;
            }else {
                var reg = /^(1[0-9]{10})$/;
                if(!reg.test($scope.mobile)){
                    $rootScope.showAlert("请输入有效的手机号码");
                    return;
                }
            }
            showLoading();
            var partner = "ZLAPITOH5WAP";
            var timestamp = Date.parse(new Date())/1000;
            var sign = $.md5(partner + timestamp + key);

            $http({
                method:'POST',
                data:{
                    partner: partner,
                    timestamp: timestamp,
                    sign: sign,
                    mobile:$scope.mobile,
                    type:$scope.type,
                },
                url:"../index.php/yunapi/SecLogin/check_user",
            })
                .success(function(response, status, headers, config){
                    console.log(response);
                    //手机号码已经注册，通过密码进行绑定登录操作
                    if (response.status == "1") {
                        $scope.write_username = 0;
                        $scope.write_password = 1;
                        $scope.uid = response.data.uid;
                    }
                    if(response.status == "2"){
                        $scope.write_username = 0;
                        $scope.write_checkcode = 1;
                        //发送验证码
                        $scope.send_code();
                    }
                    if(response.status == "0"){
                        $scope.write_username = 1;
                        $scope.write_password = 0;
                        $scope.write_checkcode = 0;
                        $rootScope.showAlert(response.message);
                    }
                    hideLoading();
                })
                .error(function(response, status, headers, config){
                    hideLoading();
                    $rootScope.showAlert('检查网络！');

                });


        }
        $scope.can_send_code = 1;
        $scope.send_code = function(){
            if( $scope.can_send_code == 0){
                return false;
            }
            if(isNull($scope.mobile)){
                $rootScope.showAlert('手机号不能为空！');
                return;
            }else {
                var reg = /^(1[0-9]{10})$/;
                if(!reg.test($scope.mobile)){
                    $rootScope.showAlert("请输入有效的手机号码");
                    return;
                }
            }
            var partner = "ZLAPITOH5WAP";
            var timestamp = Date.parse(new Date())/1000;
            var sign = $.md5(partner + timestamp + key);
            $http({
                method:'POST',
                data:{
                    partner: partner,
                    timestamp: timestamp,
                    sign: sign,
                    mobile:$scope.mobile,
                },
                url:"../index.php/yunapi/member/do_send_mobile_code",
            })
                .success(function(response){
                    if(response.status==1){
                        $scope.can_send_code = 0;
                        $rootScope.showAlert('发送成功');
                        $scope.checked = true;
                        $scope.nowtime =120;
                        setTimeout(function(){
                            $scope.can_send_code = 1;
                        },118 * 1000);
                        $scope.shut = setInterval($scope.setTime,1000);
                        hideLoading();
                    }else{
                        $rootScope.showAlert(response.message);
                        hideLoading();
                    }


                })
                .error(function(){
                    hideLoading();
                });


        }
        $scope.login_bind = function(){
            if(isNull($scope.password)){
                $rootScope.showAlert('密码不能为空！');
                return;
            }
            if($scope.password.length < 6 || $scope.password.length > 20){
                $rootScope.showAlert("密码长度不在规定范围内");
                return;
            }
            showLoading();
            var partner = "ZLAPITOH5WAP";
            var timestamp = Date.parse(new Date())/1000;
            var sign = $.md5(partner + timestamp + key);
            $http({
                method:'POST',
                data:{
                    partner: partner,
                    timestamp: timestamp,
                    sign: sign,

                    password:$scope.password,
                    type:$scope.type,
                    uid: $scope.uid,
                    openid:$scope.openid,
                },
                url:"../index.php/yunapi/SecLogin/check_password",
            })
                .success(function(response){
                    console.log(response);
                    if(response.status==1){
                        $rootScope.holdUID = response.message.uid;
                        var url = '/tab/personCenter?uid=' + response.message.uid;
                        $location.url(url);
                    }else{
                        $rootScope.showAlert(response.message);
                    }
                    hideLoading();
                })
                .error(function(){
                    hideLoading();
                    $rootScope.showAlert('检查网络！');
                });
        }

        $scope.login_bind_code = function(){
            showLoading();
            var partner = "ZLAPITOH5WAP";
            var timestamp = Date.parse(new Date())/1000;
            var sign = $.md5(partner + timestamp + key);
            $http({
                method:'POST',
                data:{
                    partner: partner,
                    timestamp: timestamp,
                    sign: sign,
                    mobile:$scope.mobile_code,
                    code:$scope.checkcode,
                    password:$scope.password,
                    type:$scope.type,
                    openid:$scope.openid,
                },
                url:"../index.php/yunapi/SecLogin/sec_do_register",
            })
                .success(function(response){
                    console.log(response);
                    if(response.status==1){
                        $rootScope.holdUID = response.message.uid;
                        var url = '/tab/personCenter?uid=' + response.message.uid;
                        $location.url(url);
                    }else{
                        $rootScope.showAlert(response.message);
                    }
                    hideLoading();
                })
                .error(function(){
                    hideLoading();
                    $rootScope.showAlert('检查网络！');
                });
        }

        $scope.check_mobile_code = function(){
            var reg = /^([0-9]{6})$/;
            if(!reg.test($scope.checkcode)){
                $rootScope.showAlert("请输入有效的验证码");
                return;
            }
            if(isNull($scope.mobile)){
                $rootScope.showAlert('手机号不能为空！');
                return;
            }else {
                var reg = /^(1[0-9]{10})$/;
                if(!reg.test($scope.mobile)){
                    $rootScope.showAlert("请输入有效的手机号码");
                    return;
                }
            }

            showLoading();
            var partner = "ZLAPITOH5WAP";
            var timestamp = Date.parse(new Date())/1000;
            var sign = $.md5(partner + timestamp + key);
            $http({
                method:'POST',
                data:{
                    partner: partner,
                    timestamp: timestamp,
                    sign: sign,
                    mobile:$scope.mobile,
                    code:$scope.checkcode,
                },
                url:"../index.php/yunapi/SecLogin/check_mobile_code",
            })
                .success(function(response){
                    console.log(response);
                    if(response.status==1){
                        //加密后的手机号码
                        $scope.mobile_code = response.data.mobile;
                        //显示验证码注册终极页面 !^_^
                        $scope.write_password_code = 1;
                        $scope.write_checkcode = 0;
                    }else{
                        $rootScope.showAlert(response.message);
                    }
                    hideLoading();
                })
                .error(function(){
                    hideLoading();
                    $rootScope.showAlert('检查网络！');
                });
        }




    })

    //搜索商品页
    .controller('searchCtrl', function ($scope, $http, $location, $rootScope) {
        document.title = "搜索商品";
        $('body').scrollTop(0);
        $("#headerId").hide();
        $("#body_top").css('height','0px');
        $("#shopList").hide();
        $scope.backTo = function(){
            window.history.go(-1);
        };

        setTimeout(function(){
            $("a").on("touchstart", function(e){
                $(e.target).addClass('box-shadow-style');
            });
            $("a").on("touchmove", function(e){
                $(e.target).removeClass('box-shadow-style');
            });
            $("a").on("touchend", function(e){
                $(e.target).removeClass('box-shadow-style');
            });
        }, 500);

        $scope.goSearch = function($event){
            /*$($event.target).css('box-shadow', '0 0 15px #000');
            setTimeout(function(){
                $($event.target).css('box-shadow', 'none');
            }, 160);*/
            if(isNull($scope.keywords)){
                $rootScope.showAlert("请输入要搜索的关键字");
            } else {
                $location.url("/tab/searchResult?keywords=" + $scope.keywords);
            }
        };

        $scope.wordList = [];

        $scope.get_search_hot_words = function () {
            showLoading();
            var partner = "ZLAPITOH5WAP";
            var timestamp = Date.parse(new Date())/1000;
            var sign = $.md5(partner + timestamp + key);
            $http({method: 'POST', data: {partner: partner, timestamp: timestamp, sign: sign}, url: "../index.php/yunapi/index/get_search_hot_words"})
                .success(function (response, status, headers, config) {
                    if (response.status == "1") {
                        $scope.wordList = response.data;
                        setTimeout(function(){
                            $("a").on("touchstart", function(e){
                                $(e.target).addClass('box-shadow-style');
                            });
                            $("a").on("touchmove", function(e){
                                $(e.target).removeClass('box-shadow-style');
                            });
                            $("a").on("touchend", function(e){
                                $(e.target).removeClass('box-shadow-style');
                            });
                        }, 500);
                    } else {
                        $rootScope.showAlert(response.message);
                    }
                    hideLoading();
                })
                .error(function (response, status, headers, config) {
                    hideLoading();
                });
        };
        $scope.get_search_hot_words();
    })
    //搜索结果页
    .controller('searchResultCtrl', function ($scope, $http, $location, $rootScope) {
        document.title = "搜索结果";
        $('body').scrollTop(0);
        $("#headerId").hide();
        $("#body_top").css('height','0px');
        $("#shopList").show();
        $scope.backTo = function(){
            window.history.go(-1);
        };

        setTimeout(function(){
            $("a").on("touchstart", function(e){
                $(e.target).addClass('box-shadow-style');
            });
            $("a").on("touchmove", function(e){
                $(e.target).removeClass('box-shadow-style');
            });
            $("a").on("touchend", function(e){
                $(e.target).removeClass('box-shadow-style');
            });
        }, 500);

        $scope.page = 1;
        $scope.size = 10;
        $scope.mScroll = "";
        $scope.isPullUp = false;
        $scope.haveMore = false;
        $scope.initIScroll = function(){
            if($scope.mScroll instanceof iScroll){
                $scope.mScroll.destroy();
            }
            $("#scroller_search_result").height($(window).height() - 80);

            $scope.mScroll = new iScroll("scroller_search_result",{
                hScrollbar:false,
                vScrollbar:false,
                onScrollMove: function(){
                    if(this.y <= (this.maxScrollY - 60)){
                        $scope.isPullUp = true;
                    }
                },
                onScrollEnd: function(){
                    if($scope.isPullUp && $scope.haveMore){
                        $scope.page++;
                        $scope.isPullUp = false;
                        $scope.do_search($scope.page, $scope.size);
                    }
                    if(this.y < -800){
                        $('#proId').show();
                    }else{
                        $('#proId').hide();
                    }
                }
            });
        };
        $('#proId').click(function(){
            $scope.mScroll.scrollTo(0, 0, 0);
        });

        $scope.searchList = [];

        $scope.keywords = $location.search()['keywords'];

        $scope.do_search = function (page, size) {
            showLoading();
            var partner = "ZLAPITOH5WAP";
            var timestamp = Date.parse(new Date())/1000;
            var sign = $.md5(partner + timestamp + key);
            $http({method: 'POST', data: {partner: partner, timestamp: timestamp, sign: sign, keywords: $scope.keywords, page: page, size: size}, url: "../index.php/yunapi/goods/do_search"})
                .success(function (response, status, headers, config) {
                    /*var str = JSON.stringify(response);
                    $rootScope.showAlert(str);*/
                    if (response.status == "1") {
                        if($scope.page == 1){
                            $scope.searchList = response.data;
                            setTimeout(function(){$scope.initIScroll();},500);
                        } else {
                            $scope.searchList = $scope.searchList.concat(response.data);
                            setTimeout(function(){$scope.mScroll.refresh();},500);
                        }
                        if(response.data.length == $scope.size){
                            $scope.haveMore = true;
                        } else {
                            $scope.haveMore = false;
                        }
                        setTimeout(function(){
                            $("a").on("touchstart", function(e){
                                $(e.target).addClass('box-shadow-style');
                            });
                            $("a").on("touchmove", function(e){
                                $(e.target).removeClass('box-shadow-style');
                            });
                            $("a").on("touchend", function(e){
                                $(e.target).removeClass('box-shadow-style');
                            });
                        }, 500);
                    } else {
                        $rootScope.showAlert(response.message);
                        $scope.searchList.total = 0;
                    }
                    hideLoading();
                })
                .error(function (response, status, headers, config) {
                    hideLoading();
                });
        };
        $scope.do_search(1, 10);
    })
    //个人中心页
    .controller('personCenterCtrl', function ($scope, $http, $location, $rootScope) {

        document.title = "个人中心";
        $('body').scrollTop(0);
        $("#headerId").hide();
        $("#body_top").css('height','0px');
        $("#shopList").hide();
        $scope.backTo = function(){
            window.history.go(-1);
        };
        console.log($rootScope.holdUID);
       	$scope.uid = $rootScope.holdUID;
        //$scope.uid = $location.search()['uid'];
        if($scope.uid > 0){
        	console.log($scope.uid);
        }else{
        	window.location.href = "http://www.yz-db.com/yungou/index.html#/tab/home";
        }




        var type = $location.search()['type'];
        $scope.isHost = false;
        $scope.exit = function(){
            delCookie("holdUID");
            $rootScope.holdUID = "";
            //$rootScope.showAlert("退出成功");
            window.location.href = "http://www.yz-db.com/yungou/index.html#/tab/home";
        };

        setTimeout(function(){
            $("a").on("touchstart", function(e){
                $(e.target).addClass('box-shadow-style');
            });
            $("a").on("touchmove", function(e){
                $(e.target).removeClass('box-shadow-style');
            });
            $("a").on("touchend", function(e){
                $(e.target).removeClass('box-shadow-style');
            });
        }, 500);

        $scope.get_member_info = function () {
            showLoading();
            var partner = "ZLAPITOH5WAP";
            var timestamp = Date.parse(new Date())/1000;
            var sign = $.md5(partner + timestamp + key);

            $http({method: 'POST', data: {partner: partner, timestamp: timestamp, sign: sign, uid: $scope.uid}, url: "../index.php/yunapi/member/get_member_info"})
                .success(function (response, status, headers, config) {
                    if (response.status == "1") {
                        $scope.userInfo = response.data;
                        console.log($scope.userInfo);
                        addCookie('codeimg',$scope.userInfo.codeImg);
                        setTimeout(function(){
                            $("a").on("touchstart", function(e){
                                $(e.target).addClass('box-shadow-style');
                            });
                            $("a").on("touchmove", function(e){
                                $(e.target).removeClass('box-shadow-style');
                            });
                            $("a").on("touchend", function(e){
                                $(e.target).removeClass('box-shadow-style');
                            });
                        }, 500);
                    } else {
                        $rootScope.showAlert(response.message);
                        setTimeout($scope.exit,1000);
                    }
                    hideLoading();
                })
                .error(function (response, status, headers, config) {
                    hideLoading();
                });
        };
        $scope.get_member_center = function () {
            showLoading();
            var partner = "ZLAPITOH5WAP";
            var timestamp = Date.parse(new Date())/1000;
            var sign = $.md5(partner + timestamp + key);
            $http({method: 'POST', data: {partner: partner, timestamp: timestamp, sign: sign, uid: $scope.uid}, url: "../index.php/yunapi/member/get_member_center"})
                .success(function (response, status, headers, config) {
                    if (response.status == "1") {
                        $scope.userInfo = response.data;

                        setTimeout(function(){
                            $("a").on("touchstart", function(e){
                                $(e.target).addClass('box-shadow-style');
                            });
                            $("a").on("touchmove", function(e){
                                $(e.target).removeClass('box-shadow-style');
                            });
                            $("a").on("touchend", function(e){
                                $(e.target).removeClass('box-shadow-style');
                            });
                        }, 500);
                    } else {
                        $rootScope.showAlert(response.message);
                    }
                    hideLoading();
                })
                .error(function (response, status, headers, config) {
                    hideLoading();
                });
        };

		

        if($scope.uid != $rootScope.holdUID){
            $scope.isHost = false;
            $scope.get_member_center();
        }else{
            $scope.isHost = true;
            if (type == 'other') {
                $scope.isHost = false;
            };

            $scope.get_member_info();
        }
    })
    //充值记录页面
    .controller('rechargeRecordCtrl', function ($scope, $http, $location, $rootScope) {
        document.title = "充值记录";
        $('body').scrollTop(0);
        $("#headerId").hide();
        $("#body_top").css('height','0px');
        $("#shopList").hide();
        $scope.backTo = function(){
            window.history.go(-1);
        };

        setTimeout(function(){
            $("a").on("touchstart", function(e){
                $(e.target).addClass('box-shadow-style');
            });
            $("a").on("touchmove", function(e){
                $(e.target).removeClass('box-shadow-style');
            });
            $("a").on("touchend", function(e){
                $(e.target).removeClass('box-shadow-style');
            });
        }, 500);

        $scope.noData = false;

        $scope.page = 1;
        $scope.size = 10;
        $scope.mScroll = "";
        $scope.isPullUp = false;
        $scope.haveMore = false;
        $scope.initIScroll = function(){
            if($scope.mScroll instanceof iScroll){
                $scope.mScroll.destroy();
            }
            $("#scroller_recharge_record").height($(window).height() - 85);

            $scope.mScroll = new iScroll("scroller_recharge_record",{
                hScrollbar:false,
                vScrollbar:false,
                onScrollMove: function(){
                    if(this.y <= (this.maxScrollY - 60)){
                        $scope.isPullUp = true;
                    }
                },
                onScrollEnd: function(){
                    if($scope.isPullUp && $scope.haveMore){
                        $scope.page++;
                        $scope.isPullUp = false;
                        $scope.get_member_recharge_records($scope.page, $scope.size);
                    }
                    if(this.y < -800){
                        $('#proId').show();
                    }else{
                        $('#proId').hide();
                    }
                }
            });
        };
        $('#proId').click(function(){
            $scope.mScroll.scrollTo(0, 0, 0);
        });

        $scope.uid = $location.search()['uid'];
        $scope.recordList = [];

        $scope.get_member_recharge_records = function (page, size) {
            showLoading();
            var partner = "ZLAPITOH5WAP";
            var timestamp = Date.parse(new Date())/1000;
            var sign = $.md5(partner + timestamp + key);
            $http({method: 'POST', data: {partner: partner, timestamp: timestamp, sign: sign, uid: $scope.uid, page: page, size: size}, url: "../index.php/yunapi/member/get_member_recharge_records"})
                .success(function (response, status, headers, config) {
                    /*var str = JSON.stringify(response);
                     $rootScope.showAlert(str);*/
                    if (response.status == "1") {
                        if($scope.page == 1){
                            if(response.data){
                                $scope.recordList = response.data;
                                if($scope.recordList.length == 0){
                                    $scope.noData = true;
                                }
                            }else{
                                $scope.noData = true;
                            }
                            setTimeout(function(){$scope.initIScroll();},500);
                        } else {
                            $scope.recordList = $scope.recordList.concat(response.data);
                            setTimeout(function(){$scope.mScroll.refresh();},500);
                        }
                        if(response.data.length == $scope.size){
                            $scope.haveMore = true;
                        } else {
                            $scope.haveMore = false;
                        }
                        setTimeout(function(){
                            $("a").on("touchstart", function(e){
                                $(e.target).addClass('box-shadow-style');
                            });
                            $("a").on("touchmove", function(e){
                                $(e.target).removeClass('box-shadow-style');
                            });
                            $("a").on("touchend", function(e){
                                $(e.target).removeClass('box-shadow-style');
                            });
                        }, 500);
                    } else {
                        $rootScope.showAlert(response.message);
                    }
                    hideLoading();
                })
                .error(function (response, status, headers, config) {
                    hideLoading();
                });
        };
        $scope.get_member_recharge_records(1, 10);
    })
    //中奖记录页面
    .controller('winningRecordCtrl', function ($scope, $http, $location, $rootScope) {
        document.title = "中奖记录";
        $('body').scrollTop(0);
        $("#headerId").hide();
        $("#body_top").css('height','0px');
        $("#shopList").hide();
        $scope.backTo = function(){
            window.history.go(-1);
        };

        setTimeout(function(){
            $("a").on("touchstart", function(e){
                $(e.target).addClass('box-shadow-style');
            });
            $("a").on("touchmove", function(e){
                $(e.target).removeClass('box-shadow-style');
            });
            $("a").on("touchend", function(e){
                $(e.target).removeClass('box-shadow-style');
            });
        }, 500);

        $scope.noData = false;

        $scope.page = 1;
        $scope.size = 10;
        $scope.mScroll = "";
        $scope.isPullUp = false;
        $scope.haveMore = false;
        $scope.initIScroll = function(){
            if($scope.mScroll instanceof iScroll){
                $scope.mScroll.destroy();
            }
            $("#scroller_winning_record").height($(window).height() - 45);

            $scope.mScroll = new iScroll("scroller_winning_record",{
                hScrollbar:false,
                vScrollbar:false,
                onScrollMove: function(){
                    if(this.y <= (this.maxScrollY - 60)){
                        $scope.isPullUp = true;
                    }
                },
                onScrollEnd: function(){
                    if($scope.isPullUp && $scope.haveMore){
                        $scope.page++;
                        $scope.isPullUp = false;
                        $scope.get_member_win_records($scope.page, $scope.size);
                    }
                    if(this.y < -800){
                        $('#proId').show();
                    }else{
                        $('#proId').hide();
                    }
                }
            });
        };
        $('#proId').click(function(){
            $scope.mScroll.scrollTo(0, 0, 0);
        });

        $scope.uid = $location.search()['uid'];
        if(!isNull($scope.uid)){
            $scope.uid = $rootScope.holdUID;
        }

        $scope.winningRecordList = [];

        $scope.get_member_win_records = function (page, size) {
        	console.log();
            showLoading();
            var partner = "ZLAPITOH5WAP";
            var timestamp = Date.parse(new Date())/1000;
            var sign = $.md5(partner + timestamp + key);
            $http({method: 'POST', data: {partner: partner, timestamp: timestamp, sign: sign, uid: $scope.uid, page: page, size: size}, url: "../index.php/yunapi/member/get_member_win_records"})
                .success(function (response, status, headers, config) {
                	console.log(response);
                    if (response.status == "1") {
                        if($scope.page == 1){
                            if(response.data){
                                $scope.winningRecordList = response.data;
                                if($scope.winningRecordList.length == 0){
                                    $scope.noData = true;
                                }
                            }else{
                                $scope.noData = true;
                            }
                            setTimeout(function(){$scope.initIScroll();},500);
                        } else {
                            $scope.winningRecordList = $scope.winningRecordList.concat(response.data);
                            setTimeout(function(){$scope.mScroll.refresh();},500);
                        }
                        if(response.data.length == $scope.size){
                            $scope.haveMore = true;
                        } else {
                            $scope.haveMore = false;
                        }
                        setTimeout(function(){
                            $("a").on("touchstart", function(e){
                                $(e.target).addClass('box-shadow-style');
                            });
                            $("a").on("touchmove", function(e){
                                $(e.target).removeClass('box-shadow-style');
                            });
                            $("a").on("touchend", function(e){
                                $(e.target).removeClass('box-shadow-style');
                            });
                        }, 500);
                    } else {
                        $rootScope.showAlert(response.message);
                    }
                    hideLoading();
                })
                .error(function (response, status, headers, config) {
                    hideLoading();
                });
        };
        $scope.get_member_win_records(1, 10);
    })
    //云购记录页面
    .controller('snatchRecordCtrl', function ($scope, $http, $location, $rootScope) {
        document.title = "云购记录";
        $('body').scrollTop(0);
        $("#headerId").hide();
        $("#body_top").css('height','0px');
        $("#shopList").hide();

        $scope.noData = false;

        setTimeout(function(){
            $("a").on("touchstart", function(e){
                $(e.target).addClass('box-shadow-style');
            });
            $("a").on("touchmove", function(e){
                $(e.target).removeClass('box-shadow-style');
            });
            $("a").on("touchend", function(e){
                $(e.target).removeClass('box-shadow-style');
            });
        }, 500);

        $scope.page = 1;
        $scope.size = 10;
        $scope.mScroll = "";
        $scope.isPullUp = false;
        $scope.haveMore = false;
        $scope.initIScroll = function(){
            if($scope.mScroll instanceof iScroll){
                $scope.mScroll.destroy();
            }
            $("#scroller_snatch_record").height($(window).height());

            $scope.mScroll = new iScroll("scroller_snatch_record",{
                hScrollbar:false,
                vScrollbar:false,
                onScrollMove: function(){
                    if(this.y <= (this.maxScrollY - 60)){
                        $scope.isPullUp = true;
                    }
                },
                onScrollEnd: function(){
                    if($scope.isPullUp && $scope.haveMore){
                        $scope.page++;
                        $scope.isPullUp = false;
                        $scope.get_duobao_list($scope.page, $scope.size, $scope.type);
                    }
                    if(this.y < -800){
                        $('#proId').show();
                    }else{
                        $('#proId').hide();
                    }

                }
            });
        };

        $('#proId').click(function(){
            $scope.mScroll.scrollTo(0, 0, 0);
        });

        $scope.isSelected1 = true;
        $scope.isSelected2 = false;
        $scope.isSelected3 = false;
        $scope.type = 0;
        $scope.setSelectedItem = function(num){
            if(num == 1){
                $scope.isSelected1 = true;
                $scope.isSelected2 = false;
                $scope.isSelected3 = false;
                $scope.type = 0;
            } else if(num == 2){
                $scope.isSelected1 = false;
                $scope.isSelected2 = true;
                $scope.isSelected3 = false;
                $scope.type = 3;
            } else if(num == 3){
                $scope.isSelected1 = false;
                $scope.isSelected2 = false;
                $scope.isSelected3 = true;
                $scope.type = 2;
            }
            $scope.page = 1;
            $scope.get_duobao_list($scope.page, $scope.size, $scope.type);
        };
        $scope.backTo = function(){
            window.history.go(-1);
        };

        $scope.uid = $location.search()['uid'];
        $scope.recordListProgress = [];
        $scope.countDown = [];
        $scope.recordListFinished = [];
        $scope.status_public = 0;
        if($scope.uid != $rootScope.holdUID){
            $scope.buyState = "跟买";
        }else{
            $scope.buyState = "追加";
        }
        $scope.CDdata = {};
        $scope.get_duobao_list = function (page, size, type) {
            showLoading();
            var partner = "ZLAPITOH5WAP";
            var timestamp = Date.parse(new Date())/1000;
            var sign = $.md5(partner + timestamp + key);
            //console.log($scope.uid);
            $http({method: 'POST', data: {partner: partner, timestamp: timestamp, sign: sign, uid: $scope.uid, page: page, size: size, type: type}, url: "../index.php/yunapi/member/get_duobao_list"})
                .success(function (response, status, headers, config) {
                    /*var str = JSON.stringify(response);
                     $rootScope.showAlert(str);*/
                    console.log(response);
                    if (response.status == "1") {
                        if($scope.page == 1){					//进行中
                            $scope.recordListProgress = [];
                            $scope.countDown = [];
                            $scope.recordListFinished = [];
                        }
                        if(!response.data){
                            $scope.noData = true;
                            hideLoading();
                            return;
                        }else{
                            $scope.noData = false;
                        }
                        for(var i = 0; i < response.data.list.length; i++){
                            if(response.data.list[i].tag == 0){										//已揭晓
                                $scope.recordListFinished.push(response.data.list[i]);
                            } else if(response.data.list[i].tag == 1){								//进行中
                                response.data.list[i].title = response.data.list[i].shopname;
                                $scope.recordListProgress.push(response.data.list[i]);
                            } else if(response.data.list[i].tag == 2){								//倒计时
                                $scope.countDown.push(response.data.list[i]);
                            }
                        }

                        $scope.get_win_member_info = function(data,i){
                            //console.log('需要查询的用户信息',data);
                            //console.log($scope.countDown);
                            //console.log(i);
                            var shopid = data.id;
                            if($scope.status_public==1){
                                return false;
                            }
                            var partner = "ZLAPITOH5WAP";
                            var timestamp = Date.parse(new Date())/1000;
                            var sign = $.md5(partner + timestamp + key);
                            $http({
                                method: 'POST',
                                data: {partner: partner, timestamp: timestamp, sign: sign, id: shopid},
                                url: "../index.php/yunapi/goods/get_win_member_info"
                            })
                            .success(function (response, status, headers, config) {
                                console.log(response);
                                if (response.status == "1") {
                                    var response_data = response.data;
                                    $scope.countDown[i]['status_public']=1;
                                    console.log('状态是否更改',$scope.countDown);
                                    var str = '<div class="m-user-goods-owner m-user-box">'+
                                                '<div class="m-user-box-name">'+
                                                    '获奖者：<a href="#/tab/personCenter?type=other&uid='+response_data.uid+'">'+response_data.username+'</a>'+
                                                '</div>'+
                                                '<div class="m-user-box-cont">'+
                                                    '本期参与：<span class="txt-impt">'+response_data.renci+'</span>人次'+
                                                '</div>'+
                                            '</div>'+
                                            '<p class="m-user-goods-code">'+
                                            '幸运号码：<span class="txt-impt">'+response_data.q_user_code+'</span>'+
                                            '</p>'+
                                            '<p class="m-user-goods-time">揭晓时间：<span>'+response_data.q_end_time+'</span></p>';
                                    console.log(str);
                                    var div_check ='.countdown_nav_' + data.shopid;
                                    $(div_check).html();
                                    $(div_check).html(str);
                                    return
                                } else {
                                    console.log('还未中奖，继续请求');
                                }
                            })
                            .error(function (response, status, headers, config) {

                            });

                        };
                        //针对正在揭晓
                        console.log('倒计时产品',$scope.countDown);
                        for(var i=0;i<$scope.countDown.length;i++){
                            console.log('序号',i);
                            var test_time_sec = $scope.countDown[i].q_end_time;
                            var test_time_msec = (parseInt(test_time_sec))*1000 - (new Date().getTime());//倒计时时间戳 毫秒
                            var class_name = ".fnTimeCountDown_"+i;
                            setTimeCountDown(class_name,test_time_sec);             //异步
                            chaxun_fun(i,test_time_msec);                    //异步
                        }
                        function setTimeCountDown(class_name,test_time_sec){
                            setTimeout(function(){
                                $(class_name).fnTimeCountDown(test_time_sec*1000,function(){});
                            },500);
                        }
                        function chaxun_fun(i,test_time_msec){
                            setTimeout(function(){
                                //每隔两秒查询一次，如果有结果就停止
                                var chaxun = setInterval(function(){        // i 变量到里面会自增1
                                    if($scope.countDown[i].status_public==1){//已揭晓状态
                                        clearInterval(chaxun);
                                    }
                                    $scope.get_win_member_info($scope.countDown[i],i);
                                },2000);
                            },test_time_msec);//测试
                        }



                        if(page == 1){
                            setTimeout(function(){$scope.initIScroll();},500);
                        } else {
                            setTimeout(function(){$scope.mScroll.refresh();},500);
                        }
                        if(response.data.list.length == $scope.size){
                            $scope.haveMore = true;
                        } else {
                            $scope.haveMore = false;
                        }
                        setTimeout(function(){
                            $("a").on("touchstart", function(e){
                                $(e.target).addClass('box-shadow-style');
                            });
                            $("a").on("touchmove", function(e){
                                $(e.target).removeClass('box-shadow-style');
                            });
                            $("a").on("touchend", function(e){
                                $(e.target).removeClass('box-shadow-style');
                            });
                        }, 500);
                    } else {
                        $rootScope.showAlert(response.message);
                    }
                    hideLoading();
                })
                .error(function (response, status, headers, config) {
                    hideLoading();
                });
        };
        $scope.get_duobao_list($scope.page, $scope.size, $scope.type);
    })
    //充值页面
    .controller('rechargeCtrl', function ($scope, $http, $rootScope,$location) {
    	
        document.title = "充值";
        $('body').scrollTop(0);
        $("#headerId").hide();
        $("#body_top").css('height','0px');
        $("#shopList").hide();
        setTimeout(function(){
            $("a").on("touchstart", function(e){
                $(e.target).addClass('box-shadow-style');
            });
            $("a").on("touchmove", function(e){
                $(e.target).removeClass('box-shadow-style');
            });
            $("a").on("touchend", function(e){
                $(e.target).removeClass('box-shadow-style');
            });
        }, 500);
        
        $scope.isSelected1 = true;
        $scope.isSelected2 = false;
        $scope.isSelected3 = false;
        $scope.isSelected4 = false;
        $scope.isSelected5 = false;
        $scope.setSelectedItem = function(num){
            if(num == 1){
                $scope.isSelected1 = true;
                $scope.isSelected2 = false;
                $scope.isSelected3 = false;
                $scope.isSelected4 = false;
                $scope.isSelected5 = false;
            } else if(num == 2){
                $scope.isSelected1 = false;
                $scope.isSelected2 = true;
                $scope.isSelected3 = false;
                $scope.isSelected4 = false;
                $scope.isSelected5 = false;
            } else if(num == 3){
                $scope.isSelected1 = false;
                $scope.isSelected2 = false;
                $scope.isSelected3 = true;
                $scope.isSelected4 = false;
                $scope.isSelected5 = false;
            } else if(num == 4){
                $scope.isSelected1 = false;
                $scope.isSelected2 = false;
                $scope.isSelected3 = false;
                $scope.isSelected4 = true;
                $scope.isSelected5 = false;
            } else if(num == 5){
                $scope.isSelected1 = false;
                $scope.isSelected2 = false;
                $scope.isSelected3 = false;
                $scope.isSelected4 = false;
                $scope.isSelected5 = true;
            }
        };
        if($rootScope.holdUID > 0){
    		
    	}else{
    		$location.url('tab/personCenter');
    	}
        $scope.backTo = function(){
            $location.url('/tab/personCenter');
        };

        $scope.setNumber = function () {
            if(isNull($scope.money) || $scope.money <= 0){
                $scope.money = 1;
            }
        };

        $scope.payClass = [];

        $scope.do_get_pay_class = function () {
            showLoading();
            var partner = "ZLAPITOH5WAP";
            var timestamp = Date.parse(new Date())/1000;
            var sign = $.md5(partner + timestamp + key);
            $http({method: 'POST', data: {partner: partner, timestamp: timestamp, sign: sign}, url: "../index.php/yunapi/cart/do_get_pay_class"})
                .success(function (response, status, headers, config) {
                    /*var str = JSON.stringify(response);
                     $rootScope.showAlert(str);*/
                    if (response.status == "1") {
                        /*for (var i = 0; i <= response.data.length; i--) {


                            if (response.data[i].pay_class == 'alipay') {
                                response.data[i].pay_name = "爱贝支付";
                                break;
                            };
                        };*/
                        //console.log(response.data);
                        $scope.payClass = response.data;
                    } else {
                        $rootScope.showAlert(response.message);
                    }
                    hideLoading();
                })
                .error(function (response, status, headers, config) {
                    hideLoading();
                });
        };
        $scope.wap_add_money = function (type) {
            if($scope.isSelected1){
                $scope.money = 20;
            }
            if($scope.isSelected2){
                $scope.money = 50;
            }
            if($scope.isSelected3){
                $scope.money = 100;
            }
            if($scope.isSelected4){
                $scope.money = 200;
            }
            if(isNull($scope.money)){
                $rootScope.showAlert("请选择或输入要充值的金额");
                return;
            }
            showLoading();
            var partner = "ZLAPITOH5WAP";
            var timestamp = Date.parse(new Date())/1000;
            var sign = $.md5(partner + timestamp + key);
			      console.log(type);
            if(type == 'wxpay'){

                var url = "../index.php/yunapi/cart/wap_add_money?data=";

                var ss = '{"partner":"'+partner+'","timestamp":"'+timestamp+'","sign":"'+sign+'","uid":"'+$rootScope.holdUID+'","money":"'+$scope.money+'","pay_type":"'+type+'"}';
                //var s = '{"partner":"'+partner+'",'"timestamp":"'+timestamp+'","sign":"+sign+"','uid':'"+$rootScope.holdUID+"','money':'"+$scope.money+"','pay_type':'"+type+"'}";
                url = url + ss;
                window.location.href = url;
                return false;

            }

			if(type == 'iapppay'){
				
				alert('爱贝支付是合作平台，请用户放心使用');
				
			}
			
            $http({method: 'POST', data: {partner: partner, timestamp: timestamp, sign: sign, uid: $rootScope.holdUID, money: $scope.money, pay_type: type}, url: "../index.php/yunapi/cart/wap_add_money"})
                .success(function (response, status, headers, config) {

                    console.log(response);

                    if (response.status == "1") {
                    	//支付宝调用
                        if (type == 'alipay') {
                            $("#resultDisplay").html(response.data);

                        }else if (type == 'iapppay') {
                            window.location.href = response.data;
                        }

                    } else {
                        $rootScope.showAlert(response.message);
                    }
                    hideLoading();
                })
                .error(function (response, status, headers, config) {
                    hideLoading();
                });
        };
        $scope.confirmPay = function($event) {
            /*$($event.target).css('box-shadow', '0 0 15px #000');
            setTimeout(function(){
                $($event.target).css('box-shadow', 'none');
            }, 160);*/
            var radios = document.getElementsByName("optionsRadios");
            var tag = 0;
            for (var i = 0; i < radios.length; i++) {
                if (radios[i].checked) {
                    tag = 1;
                    $scope.wap_add_money(radios[i].value);
                    break;
                }
            }
            if(tag == 0){
                $rootScope.showAlert("请选择一种支付方式");
            }
        };
        $scope.do_get_pay_class();
    })
    //个人资料页面
    .controller('personalInfoCtrl', function ($scope, $http, $location, $rootScope) {
        document.title = "个人资料";
        $('body').scrollTop(0);
        $("#headerId").hide();
        $("#body_top").css('height','0px');
        $("#shopList").hide();
        $scope.backTo = function(){
            $location.url("/tab/personCenter?uid=" + $scope.uid);
        };
        setTimeout(function(){
            $("a").on("touchstart", function(e){
                $(e.target).addClass('box-shadow-style');
            });
            $("a").on("touchmove", function(e){
                $(e.target).removeClass('box-shadow-style');
            });
            $("a").on("touchend", function(e){
                $(e.target).removeClass('box-shadow-style');
            });
        }, 500);

        $scope.uid = $location.search()['uid'];

        $scope.get_member_center = function () {
            showLoading();
            var partner = "ZLAPITOH5WAP";
            var timestamp = Date.parse(new Date())/1000;
            var sign = $.md5(partner + timestamp + key);
            $http({method: 'POST', data: {partner: partner, timestamp: timestamp, sign: sign, uid: $scope.uid}, url: "../index.php/yunapi/member/get_member_center"})
                .success(function (response, status, headers, config) {
                    if (response.status == "1") {
                        $scope.userInfo = response.data;
                    } else {
                        $rootScope.showAlert(response.message);
                    }
                    hideLoading();
                })
                .error(function (response, status, headers, config) {
                    hideLoading();
                });
        };
        $scope.get_member_center();
    })
    //收货地址页面
    .controller('addressCtrl', function ($scope, $http, $location, $rootScope) {
        document.title = "地址管理";
        $('body').scrollTop(0);
        $("#headerId").hide();
        $("#body_top").css('height','0px');
        $("#shopList").hide();
        $scope.backTo = function(){
            $location.url("/tab/personalInfo?uid=" + $scope.uid);
        };
        setTimeout(function(){
            $("a").on("touchstart", function(e){
                $(e.target).addClass('box-shadow-style');
            });
            $("a").on("touchmove", function(e){
                $(e.target).removeClass('box-shadow-style');
            });
            $("a").on("touchend", function(e){
                $(e.target).removeClass('box-shadow-style');
            });
        }, 500);

        $scope.noData = false;

        $scope.addAddress = function(){
            if($scope.userAddress.length > 4){
                $rootScope.showAlert("最多只能添加5条地址");
                return;
            }
            $location.url("/tab/addAddress?uid=" + $scope.uid);
        };

        $scope.uid = $location.search()['uid'];
        $scope.userAddress = [];

        $scope.get_member_address = function () {
            showLoading();
            var partner = "ZLAPITOH5WAP";
            var timestamp = Date.parse(new Date())/1000;
            var sign = $.md5(partner + timestamp + key);
            $http({method: 'POST', data: {partner: partner, timestamp: timestamp, sign: sign, uid: $scope.uid}, url: "../index.php/yunapi/member/get_member_address"})
                .success(function (response, status, headers, config) {
                    if (response.status == "1") {
                        if(response.data){
                            $scope.userAddress = response.data;
                        }
                        if($scope.userAddress.length == 0){
                            $scope.noData = true;
                        }
                    } else {
                        $rootScope.showAlert(response.message);
                    }
                    hideLoading();
                })
                .error(function (response, status, headers, config) {
                    hideLoading();
                });
        };

        $scope.get_member_address();
    })
    //添加收货地址
    .controller('addAddressCtrl', function ($scope, $http, $location, $rootScope) {
        document.title = "添加地址";
        $('body').scrollTop(0);
        $("#headerId").hide();
        $("#body_top").css('height','0px');
        $("#shopList").hide();
        $scope.isSelected = true;
        $scope.default = 1;
        $scope.setSelectedItem = function(){
          if($scope.isSelected){
              $scope.isSelected = false;
              $scope.default = 0;
          } else {
              $scope.isSelected = true;
              $scope.default = 1;
          }
        };
        $scope.backTo = function(){
            window.history.go(-1);
        };
        setTimeout(function(){
            $("a").on("touchstart", function(e){
                $(e.target).addClass('box-shadow-style');
            });
            $("a").on("touchmove", function(e){
                $(e.target).removeClass('box-shadow-style');
            });
            $("a").on("touchend", function(e){
                $(e.target).removeClass('box-shadow-style');
            });
        }, 500);

        $scope.uid = $location.search()['uid'];
        $scope.flag = $location.search()['flag'];

        $scope.showLocation = function(province , city , town) {
            var loc	= new Location();
            var title	= ['省份' , '地级市' , '市、县、区'];
            $.each(title , function(k , v) {
                title[k]	= '<option value="">'+v+'</option>';
            });

            $('#loc_province').append(title[0]);
            $('#loc_city').append(title[1]);
            $('#loc_town').append(title[2]);

            $('#loc_province').change(function() {
                $('#loc_city').empty();
                $('#loc_city').append(title[1]);
                loc.fillOption('loc_city' , '0,'+$('#loc_province').val());
                $('#loc_town').empty();
                $('#loc_town').append(title[2]);
                $scope.sheng = $("#loc_province option:selected").text();
            });

            $('#loc_city').change(function() {
                $('#loc_town').empty();
                $('#loc_town').append(title[2]);
                loc.fillOption('loc_town' , '0,' + $('#loc_province').val() + ',' + $('#loc_city').val());
                $scope.shi = $("#loc_city option:selected").text();
            });

            $('#loc_town').change(function() {
                $scope.xian = $("#loc_town option:selected").text();
            });

            if (province) {
                loc.fillOption('loc_province' , '0' , province);

                if (city) {
                    loc.fillOption('loc_city' , '0,'+province , city);

                    if (town) {
                        loc.fillOption('loc_town' , '0,'+province+','+city , town);
                    }
                }

            } else {
                loc.fillOption('loc_province' , '0');
            }
        };

        $scope.isSave = false;

        $scope.do_save_member_address = function ($event) {
            /*$($event.target).css('color', '#f81000');
            setTimeout(function(){
                $($event.target).css('color', '#0079fe');
            }, 160);*/
            if(isNull($scope.shouhuoren)){
                $rootScope.showAlert("收货人不能为空");
                return;
            }
            if(isNull($scope.mobile)){
                $rootScope.showAlert("收货人手机号码不能为空");
                return;
            } else if($scope.mobile.length != 11){
                $rootScope.showAlert("请输入有效的手机号码");
                return;
            } else {
                var reg = /^(((13[0-9]{1})|(15[0-9]{1})|(18[0-9]{1}))+\d{8})$/;
                if(!reg.test($scope.mobile)){
                    $rootScope.showAlert("请输入有效的手机号码");
                    return;
                }
            }
            if(isNull($scope.sheng)){
                $rootScope.showAlert("请选择省份");
                return;
            }
            if(isNull($scope.shi)){
                $rootScope.showAlert("请选择城市");
                return;
            }
            if(isNull($scope.xian)){
                $rootScope.showAlert("请选择地区");
                return;
            }
            if(isNull($scope.jiedao)){
                $rootScope.showAlert("请输入街道号");
                return;
            }
            if($scope.isSave){
                return;
            }
            $scope.isSave = true;
            showLoading();
            var partner = "ZLAPITOH5WAP";
            var timestamp = Date.parse(new Date())/1000;
            var sign = $.md5(partner + timestamp + key);
            $http({
            	method: 'POST',
            	data: {
            		partner: partner,
            		timestamp: timestamp,
            		sign: sign,
            		uid: $scope.uid,
            		shouhuoren: $scope.shouhuoren,
            		mobile: $scope.mobile,
            		sheng: $scope.sheng,
            		shi: $scope.shi,
            		xian: $scope.xian,
            		jiedao: $scope.jiedao,
            		default: $scope.default
            	},
            	url: "../index.php/yunapi/member/do_save_member_address"
            	})
                .success(function (response, status, headers, config) {
                    if (response.status == "1") {
                        if($scope.flag == "address"){
                            $rootScope.showAlert("添加成功");
                            window.history.go(-1);
                        }else{
                            $location.url("/tab/address?uid=" + $scope.uid);
                        }

                    } else {
                        $scope.isSave = false;
                        $rootScope.showAlert(response.message);
                    }
                    hideLoading();
                })
                .error(function (response, status, headers, config) {
                    $scope.isSave = false;
                    hideLoading();
                });
        };

        $scope.showLocation();
    })
    //修改昵称
    .controller('updateNicknameCtrl', function ($scope, $http, $location, $rootScope) {
        document.title = "修改昵称";
        $('body').scrollTop(0);
        $("#headerId").hide();
        $("#body_top").css('height','0px');
        $("#shopList").hide();
        $scope.backTo = function(){
            window.history.go(-1);
        };
        setTimeout(function(){
            $("a").on("touchstart", function(e){
                $(e.target).addClass('box-shadow-style');
            });
            $("a").on("touchmove", function(e){
                $(e.target).removeClass('box-shadow-style');
            });
            $("a").on("touchend", function(e){
                $(e.target).removeClass('box-shadow-style');
            });
        }, 500);

        $scope.uid = $location.search()['uid'];

        $scope.do_mod_username = function ($event) {
            /*$($event.target).css('color', '#f81000');
            setTimeout(function(){
                $($event.target).css('color', '#0079fe');
            }, 160);*/
            if(isNull($scope.username)){
                $rootScope.showAlert("昵称不能为空");
                return;
            }
            showLoading();
            var partner = "ZLAPITOH5WAP";
            var timestamp = Date.parse(new Date())/1000;
            var sign = $.md5(partner + timestamp + key);
            $http({method: 'POST', data: {partner: partner, timestamp: timestamp, sign: sign, uid: $scope.uid, username: $scope.username}, url: "../index.php/yunapi/member/do_mod_username"})
                .success(function (response, status, headers, config) {
                    if (response.status == "1") {
                        $location.url("/tab/personalInfo?uid=" + $scope.uid);
                    } else {
                        $rootScope.showAlert(response.message);
                    }
                    hideLoading();
                })
                .error(function (response, status, headers, config) {
                    hideLoading();
                });
        };
    })
    //修改手机号码
    .controller('updatePhoneCtrl', function ($scope, $http, $location, $rootScope) {
        document.title = "修改手机号码";
        $('body').scrollTop(0);
        $("#headerId").hide();
        $("#body_top").css('height','0px');
        $("#shopList").hide();
        $scope.backTo = function(){
            window.history.go(-1);
        };
        setTimeout(function(){
            $("a").on("touchstart", function(e){
                $(e.target).addClass('box-shadow-style');
            });
            $("a").on("touchmove", function(e){
                $(e.target).removeClass('box-shadow-style');
            });
            $("a").on("touchend", function(e){
                $(e.target).removeClass('box-shadow-style');
            });
        }, 500);
        $scope.checked = false;
        $scope.codeText = "获取验证码";
        $scope.nowtime = 60;
        $scope.setTime = function(){
            if($scope.nowtime == 0){
                clearInterval($scope.shut);
                $scope.checked = false;
                $scope.codeText = "重新获取";
                $scope.$apply();
            } else {
                $scope.nowtime --;
                $scope.codeText = "("+$scope.nowtime+"S)重新获取";
                $scope.$apply();
            }
        };

        $scope.uid = $location.search()['uid'];

        $scope.send_mod_mobile_code = function () {
            if(isNull($scope.mobile)){
                $rootScope.showAlert("请输入新手机号码");
                return;
            } else if($scope.mobile.length != 11){
                $rootScope.showAlert("请输入有效的手机号码");
                return;
            } else {
                var reg = /^(((13[0-9]{1})|(15[0-9]{1})|(18[0-9]{1}))+\d{8})$/;
                if(!reg.test($scope.mobile)){
                    $rootScope.showAlert("请输入有效的手机号码");
                    return;
                }
            }
            showLoading();
            var partner = "ZLAPITOH5WAP";
            var timestamp = Date.parse(new Date())/1000;
            var sign = $.md5(partner + timestamp + key);
            $http({method: 'POST', data: {partner: partner, timestamp: timestamp, sign: sign, uid: $scope.uid, mobile: $scope.mobile}, url: "../index.php/yunapi/member/send_mod_mobile_code"})
                .success(function (response, status, headers, config) {
                    if (response.status == "1") {
                        $scope.checked = true;
                        $scope.nowtime = 60;
                        $scope.shut = setInterval($scope.setTime,1000);
                        $rootScope.showAlert("验证码发送成功");
                    } else {
                        $rootScope.showAlert(response.message);
                    }
                    hideLoading();
                })
                .error(function (response, status, headers, config) {
                    hideLoading();
                });
        };
        $scope.do_save_mobile = function ($event) {
            /*$($event.target).css('color', '#f81000');
            setTimeout(function(){
                $($event.target).css('color', '#0079fe');
            }, 160);*/
            if(isNull($scope.mobile)){
                $rootScope.showAlert("请输入新手机号码");
                return;
            } else if($scope.mobile.length != 11){
                $rootScope.showAlert("请输入有效的手机号码");
                return;
            } else {
                var reg = /^(((13[0-9]{1})|(15[0-9]{1})|(18[0-9]{1}))+\d{8})$/;
                if(!reg.test($scope.mobile)){
                    $rootScope.showAlert("请输入有效的手机号码");
                    return;
                }
            }
            if(isNull($scope.code)){
                $rootScope.showAlert("请输入验证码");
                return;
            }
            showLoading();
            var partner = "ZLAPITOH5WAP";
            var timestamp = Date.parse(new Date())/1000;
            var sign = $.md5(partner + timestamp + key);
            $http({method: 'POST', data: {partner: partner, timestamp: timestamp, sign: sign, uid: $scope.uid, mobile: $scope.mobile, code: $scope.code}, url: "../index.php/yunapi/member/do_save_mobile"})
                .success(function (response, status, headers, config) {
                    if (response.status == "1") {
                        $location.url("/tab/personalInfo?uid=" + $scope.uid);
                    } else {
                        $rootScope.showAlert(response.message);
                    }
                    hideLoading();
                })
                .error(function (response, status, headers, config) {
                    hideLoading();
                });
        };
    })
    //晒单详情
    .controller('shareDetailCtrl', function ($scope, $http, $location, $rootScope) {
        document.title = "晒单详情";
        $('body').scrollTop(0);
        $("#headerId").hide();
        $("#body_top").css('height','0px');
        $("#shopList").hide();
        $scope.backTo = function(){
            window.history.go(-1);
        };

        setTimeout(function(){
            $("a").on("touchstart", function(e){
                $(e.target).addClass('box-shadow-style');
            });
            $("a").on("touchmove", function(e){
                $(e.target).removeClass('box-shadow-style');
            });
            $("a").on("touchend", function(e){
                $(e.target).removeClass('box-shadow-style');
            });
        }, 500);

        $scope.sd_id = $location.search()['sd_id'];

        if($location.search()['come'] == "Android" || $location.search()['come'] == "IOS"){
            $scope.notNative = false;
            $("#footbar").hide();

            setTimeout(function(){
                connectWebViewJavascriptBridge(function(bridge) {
                    /* Init your app here */
                    bridge.init(function(message, responseCallback) {
                        if (responseCallback) {
                            responseCallback("Right back atcha")
                        }
                    });

                    var button = document.getElementById('native_user');
                    button.onclick = function(){
                        //test.preventDefault();
                        bridge.callHandler('goBackPersonalCenterVC', {'': ''}, function(response) {

                        })
                    };

                    var button1 = document.getElementById('native_product');
                    button1.onclick = function(){
                        //test.preventDefault();
                        bridge.callHandler('goToShangPinXqVC', {'productId': $scope.shareData.sd_shopid}, function(response) {

                        })
                    }
                });
            }, 500);

        }else{
            $scope.notNative = true;
            $("#shareDetailContent").css("margin-top", "45px");
        }

        $scope.get_shaidan_info = function () {
            showLoading();
            var partner = "ZLAPITOH5WAP";
            var timestamp = Date.parse(new Date())/1000;
            var sign = $.md5(partner + timestamp + key);
            $http({method: 'POST', data: {partner: partner, timestamp: timestamp, sign: sign, sd_id: $scope.sd_id}, url: "../index.php/yunapi/goods/get_shaidan_info"})
                .success(function (response, status, headers, config) {
                    if (response.status == "1") {
                        $scope.shareData = response.data;
                        setTimeout(function(){
                            $("a").on("touchstart", function(e){
                                $(e.target).addClass('box-shadow-style');
                            });
                            $("a").on("touchmove", function(e){
                                $(e.target).removeClass('box-shadow-style');
                            });
                            $("a").on("touchend", function(e){
                                $(e.target).removeClass('box-shadow-style');
                            });
                        }, 500);
                    } else {
                        $rootScope.showAlert(response.message);
                    }
                    hideLoading();
                })
                .error(function (response, status, headers, config) {
                    hideLoading();
                });
        };
        $scope.get_shaidan_info();
    })

    //购单列表
    .controller('shopListCtrl', function ($scope, $http, $location, $rootScope) {
        document.title = "云购单";
        $('body').scrollTop(0);
        $("#headerId").hide();
        $("#body_top").css('height','0px');
        $("#shopList").hide();

        if($rootScope.shopList.length == 0){
            $scope.isEmpty = true;
        }else{
            $scope.isEmpty = false;
        }
        setTimeout(function(){
            $("a").on("touchstart", function(e){
                $(e.target).addClass('box-shadow-style');
            });
            $("a").on("touchmove", function(e){
                $(e.target).removeClass('box-shadow-style');
            });
            $("a").on("touchend", function(e){
                $(e.target).removeClass('box-shadow-style');
            });
        }, 500);

        $scope.mScroll = "";
        $scope.initIScroll = function(){
            if($scope.mScroll instanceof iScroll){
                $scope.mScroll.destroy();
            }
            $("#shop_content").height($(window).height()-90);

            $scope.mScroll = new iScroll("shop_content",{
                hScrollbar:false,
                vScrollbar:false,
                onScrollMove: function(){

                },
                onScrollEnd: function(){
                    if(this.y < -800){
                        $('#proId').show();
                    }else{
                        $('#proId').hide();
                    }
                }
            });
        };
        $('#proId').click(function(){
            $scope.mScroll.scrollTo(0, 0, 0);
        });

        setTimeout(function(){$scope.initIScroll();},100);

        $scope.price = 0;
        for(var i = 0; i < $rootScope.shopList.length; i++){
            $scope.price += $rootScope.shopList[i].num;
            if(isNaN($scope.price)){
        		$scope.price = 0;
        	}
        }

        $scope.setNumber = function (index) {
        	var xiangou_number = $rootScope.shopList[index].wap_ten;			//限购数量

            //console.log(parseInt($rootScope.shopList[index].num));
            if($rootScope.shopList[index].num && $rootScope.shopList[index].num < 1){
                $rootScope.shopList[index].num = 1;
            }else if($rootScope.shopList[index].num > xiangou_number){
                $rootScope.shopList[index].num = xiangou_number;
            }
            $scope.price = 0;
            for(var i = 0; i < $rootScope.shopList.length; i++){
                $scope.price += parseInt($rootScope.shopList[i].num);
                if(isNaN($scope.price)){
            		$scope.price = 0;
            	}
            }
            addCookie("shopList", JSON.stringify($rootScope.shopList), 0);
        };

        $scope.backTo = function(){
            window.history.go(-1);
        };

        $scope.plus = function(index){
        	var xiangou_number = $rootScope.shopList[index].wap_ten;//限购数量
        	//var syrs = $rootScope.shopList[index].zongrenshu - $rootScope.shopList[index].canyurenshu;
        	//console.log(parseInt($rootScope.shopList[index].num));
        	if(($rootScope.shopList[index].num + 1) <= xiangou_number){
            	$rootScope.shopList[index].num++;
           	 	$scope.price ++;
            }else{
            	$rootScope.shopList[index].num = xiangou_number;
            }

            addCookie("shopList", JSON.stringify($rootScope.shopList), 0);
        };
        $scope.subtract = function(index){

            if($rootScope.shopList[index].num - 1 > 0){
                $rootScope.shopList[index].num--;
                $scope.price--;
            }
            addCookie("shopList", JSON.stringify($rootScope.shopList), 0);
        };
        $scope.del = function(index){
            $scope.price = $scope.price - $rootScope.shopList[index].num;
            $rootScope.shopList.splice(index, 1);
            if($rootScope.shopList.length == 0){
                $scope.isEmpty = true;
            }else{
                $scope.isEmpty = false;
            }
            addCookie("shopList", JSON.stringify($rootScope.shopList), 0);
        };

        $scope.addToCart = function () {
        	if($rootScope.holdUID>0){
	            if($scope.isEmpty){
	                return;
	            }
	            if(isNull($rootScope.holdUID)){
	                $location.url("/tab/login");
	            } else {
	            	var shoplist = eval('('+getCookie('shopList')+')');
	            	for(var i=0;i<shoplist.length;i++){
		        		console.log(shoplist[i].wap_ten);//return false;
		        		if(shoplist[i].num>0){

		        		}else{
		        			$rootScope.showAlert("请确认您填写的商品数量是否合法！");return false;
		        		}
		        		//console.log($rootScope.shopList);
		        		if(shoplist[i].num > shoplist[i].wap_ten){
		        			$rootScope.shopList[i].num = shoplist[i].wap_ten;
		        			addCookie("shopList", JSON.stringify(shopList));
		        			$rootScope.showAlert('您购物车中有商品达到购买上限，系统已将其纠正！');
		        			$location.url("/tab/shoplist");
		        			return false;
		        		}
		        	}
	                $location.url("/tab/pay");
	            }
	        }else{
        		 $rootScope.showAlert("请到右上角先登录!");return false;
	        }
        };

    })
    //支付订单列表
    .controller('payCtrl', function ($scope, $http, $location, $rootScope) {
        document.title = "支付订单";
        $('body').scrollTop(0);
        $("#headerId").hide();
        $("#body_top").css('height','0px');
        $("#shopList").hide();
        $scope.backTo = function(){
            window.history.go(-1);
        };
        setTimeout(function(){
            $("a").on("touchstart", function(e){
                $(e.target).addClass('box-shadow-style');
            });
            $("a").on("touchmove", function(e){
                $(e.target).removeClass('box-shadow-style');
            });
            $("a").on("touchend", function(e){
                $(e.target).removeClass('box-shadow-style');
            });
        }, 500);

        $scope.mScroll = "";
        $scope.initIScroll = function(){
            if($scope.mScroll instanceof iScroll){
                $scope.mScroll.destroy();
            }
            $("#pay_content").height($(window).height()-90);

            $scope.mScroll = new iScroll("pay_content",{
                hScrollbar:false,
                vScrollbar:false,
                onScrollMove: function(){

                },
                onScrollEnd: function(){
                    if(this.y < -800){
                        $('#proId').show();
                    }else{
                        $('#proId').hide();
                    }
                }
            });
        };
        $('#proId').click(function(){
            $scope.mScroll.scrollTo(0, 0, 0);
        });

        setTimeout(function(){$scope.initIScroll();},100);

        $scope.price = 0;
        for(var i = 0; i < $rootScope.shopList.length; i++){
            $rootScope.shopList[i].num = Math.ceil($rootScope.shopList[i].num);
            $scope.price += $rootScope.shopList[i].num;
        }

        $scope.payClass = [];

        $scope.do_get_pay_class = function () {
            showLoading();
            var partner = "ZLAPITOH5WAP";
            var timestamp = Date.parse(new Date())/1000;
            var sign = $.md5(partner + timestamp + key);
            $http({method: 'POST', data: {partner: partner, timestamp: timestamp, sign: sign}, url: "../index.php/yunapi/cart/do_get_pay_class"})
                .success(function (response, status, headers, config) {
                    /*var str = JSON.stringify(response);
                     $rootScope.showAlert(str);*/
                    if (response.status == "1") {
                        $scope.payClass = response.data;
                    } else {
                        $rootScope.showAlert(response.message);
                    }
                    hideLoading();
                })
                .error(function (response, status, headers, config) {
                    hideLoading();
                });
        };

        $scope.get_member_info = function() {
            showLoading();
            var partner = "ZLAPITOH5WAP";
            var timestamp = Date.parse(new Date())/1000;
            var sign = $.md5(partner + timestamp + key);
            $http({method: 'POST', data: {partner: partner, timestamp: timestamp, sign: sign, uid: $rootScope.holdUID}, url: "../index.php/yunapi/member/get_member_info"})
                .success(function (response, status, headers, config) {
                    if (response.status == "1") {
                        $scope.money = response.data.money;
                        if($scope.money > 0){
                            $("#optionsCheck").attr("checked", true);
                            $scope.enough = true;
                            $scope.price_wait = $scope.price - $scope.money;
                            if($scope.price_wait < 0){
                                $scope.price_wait = 0;
                            }
                            if($scope.money >= $scope.price){
                                $scope.isEnough = true;
                            }else{
                                $scope.isEnough = false;
                                $scope.do_get_pay_class();
                            }
                        }else{
                            $scope.price_wait = $scope.price;
                            $("#optionsCheck").attr("disabled", true);
                            $scope.enough = false;
                            $scope.do_get_pay_class();
                        }
                    } else {
                        $rootScope.showAlert(response.message);
                    }
                    hideLoading();
                })
                .error(function (response, status, headers, config) {
                    hideLoading();
                });
        };

        $("#optionsCheck").click(function(){
            if(document.getElementById("optionsCheck").checked){
                $scope.price_wait = $scope.price - $scope.money;
                if($scope.price_wait < 0){
                    $scope.price_wait = 0;
                }
            }else{
                $scope.price_wait = $scope.price;
            }
            $scope.$apply();
        });

        $scope.confirmPay = function() {
            var checkbox = document.getElementById("optionsCheck");
            var radios = document.getElementsByName("optionsRadios");
            var tag = 0;
            for (var i = 0; i < radios.length; i++) {
                if (radios[i].checked) {
                    tag = 1;
                    console.log(radios[i].value);
                    if(checkbox.checked){
                        $location.url("/tab/payResult?type=" + radios[i].value + "&is_yue=1&money="+$scope.price);
                    }else{
                        $location.url("/tab/payResult?type=" + radios[i].value + "&is_yue=0&money="+$scope.price);
                    }
                    break;
                }
            }
            if(tag == 0){
                if(checkbox.checked){
                    if($scope.money < $scope.price){
                        $rootScope.showAlert("余额不足，请选择一种支付方式");
                    }else{
                        $location.url("/tab/payResult?type=" + "&is_yue=1");
                    }
                }else{
                    $rootScope.showAlert("请选择一种支付方式");
                }
            }
        };

        $scope.get_member_info();

    })
    //计算结果
    .controller('calculationCtrl', function ($scope, $http, $location, $rootScope) {
        document.title = "计算结果";
        $('body').scrollTop(0);
        $("#headerId").hide();
        $("#body_top").css('height','0px');
        $("#shopList").hide();
        $scope.backTo = function(){
            window.history.go(-1);
        };
        $scope.isUp = true;
        $scope.setUpDown = function(){
            if($scope.isUp){
                $scope.isUp = false;
            }else{
                $scope.isUp = true;
            }
        };
        setTimeout(function(){
            $("a").on("touchstart", function(e){
                $(e.target).addClass('box-shadow-style');
            });
            $("a").on("touchmove", function(e){
                $(e.target).removeClass('box-shadow-style');
            });
            $("a").on("touchend", function(e){
                $(e.target).removeClass('box-shadow-style');
            });
        }, 500);

        $scope.id = $location.search()['id'];

        $scope.get_calc_details = function() {
            showLoading();
            var partner = "ZLAPITOH5WAP";
            var timestamp = Date.parse(new Date())/1000;
            var sign = $.md5(partner + timestamp + key);
            $http({method: 'POST', data: {partner: partner, timestamp: timestamp, sign: sign, id: $scope.id}, url: "../index.php/yunapi/goods/get_calc_details"})
                .success(function (response, status, headers, config) {
                    if (response.status == "1") {
                        $scope.calData = response.data;
                        setTimeout(function(){
                            $("a").on("touchstart", function(e){
                                $(e.target).addClass('box-shadow-style');
                            });
                            $("a").on("touchmove", function(e){
                                $(e.target).removeClass('box-shadow-style');
                            });
                            $("a").on("touchend", function(e){
                                $(e.target).removeClass('box-shadow-style');
                            });
                        }, 500);
                    } else {
                        $rootScope.showAlert(response.message);
                    }
                    hideLoading();
                })
                .error(function (response, status, headers, config) {
                    hideLoading();
                });
        };
        $scope.get_calc_details();

    })
   	// 支付结果
    .controller("payResultStateCtrl",function ($scope, $http, $location, $rootScope) {
        document.title = "支付结果";
        $('body').scrollTop(0);
        $("#headerId").hide();
        $("#body_top").css('height','0px');
        $("#shopList").hide();
        $scope.backTo = function(){
            $location.url("/tab/home");
        };
        setTimeout(function(){
            $("a").on("touchstart", function(e){
                $(e.target).addClass('box-shadow-style');
            });
            $("a").on("touchmove", function(e){
                $(e.target).removeClass('box-shadow-style');
            });
            $("a").on("touchend", function(e){
                $(e.target).removeClass('box-shadow-style');
            });
        }, 500);

        $scope.isFaild = false;
        $scope.isSuccess = false;
        $scope.isChongSuccess = false;
        $scope.isChongFaild = false;

        $scope.type = $location.search()['type'];
        $scope.code = $location.search()['code'];
        $uid = $rootScope.holdUID;
        $scope.get_pay_state = function() {
            $http({method: 'POST', data: {code: $scope.code, uid: $rootScope.holdUID}, url: "../index.php/yunapi/cart/get_pay_state2"})
            .success(function (response, status, headers, config) {
                //$rootScope.showAlert(response.status_type);  1--支付   2---充值
                if (response.status == 1) {
                    if (response.pay_state == 1) {
                        $rootScope.shopList = [];
                        delCookie("shopList");
                        $scope.isSuccess = true;
                        $rootScope.showAlert(response.message);
                        $scope.resultData = response.data;
                    }else {
                        $rootScope.shopList = [];
                        $scope.isFaild = true;
                        $rootScope.showAlert(response.message);
                    }
                }else{
                    if (response.pay_state == 1) {
                        $rootScope.shopList = [];
                        $scope.isChongSuccess = true;
                        $rootScope.showAlert(response.message);
                    }else{
                        $rootScope.shopList = [];
                        $scope.isChongFaild = true;
                        $rootScope.showAlert(response.message);
                    }
                }
                hideLoading();
            })
            .error(function (response, status, headers, config) {
                    hideLoading();
                });
        }
        $scope.get_pay_state();
    })
    //支付结果
    .controller('payReCtrl', function ($scope, $http, $location, $rootScope) {
        document.title = "支付结果";
        $('body').scrollTop(0);
        $("#headerId").hide();
        $("#body_top").css('height','0px');
        $("#shopList").hide();
        $scope.backTo = function(){
            $location.url("/tab/home");
        };
        setTimeout(function(){
            $("a").on("touchstart", function(e){
                $(e.target).addClass('box-shadow-style');
            });
            $("a").on("touchmove", function(e){
                $(e.target).removeClass('box-shadow-style');
            });
            $("a").on("touchend", function(e){
                $(e.target).removeClass('box-shadow-style');
            });
        }, 500);

        $scope.isFaild = false;
        $scope.isSuccess = false;
        $scope.isChongSuccess = false;
        $scope.isChongFail = false;

        $scope.type = $location.search()['type'];
        $scope.is_yue = $location.search()['is_yue'];
        $scope.code = $location.search()['code'];
        $uid = $rootScope.holdUID;
        $scope.get_pay_state = function() {
            $http({method: 'POST', data: {code: $scope.code, uid: $rootScope.holdUID}, url: "../index.php/yunapi/cart/get_pay_state"})
            .success(function (response, status, headers, config) {
                //$rootScope.showAlert(response.status_type);
                if (response.status == 1) {
                    if (response.status_type == 2) {
                        $scope.isFaild = true;
                    }else if(response.status_type == 3) {
                        $scope.isChongSuccess = true;
                    }else{
                        $scope.isSuccess = true;
                    }
                    $rootScope.shopList = [];
                    delCookie("shopList");

                    $scope.resultData = response.data;
                }else{
                    $rootScope.shopList = [];
                    delCookie("shopList");
                    $scope.isFaild = true;
                    $rootScope.showAlert(response.message);
                }
                hideLoading();
            })
            .error(function (response, status, headers, config) {
                    hideLoading();
                });
        }
        $scope.get_pay_state();
    })
    //支付结果
    .controller('payResultCtrl', function ($scope, $http, $location, $rootScope) {
        document.title = "支付结果";
        $('body').scrollTop(0);
        $("#headerId").hide();
        $("#body_top").css('height','0px');
        $("#shopList").hide();
        $scope.backTo = function(){
            $location.url("/tab/home");
        };
        setTimeout(function(){
            $("a").on("touchstart", function(e){
                $(e.target).addClass('box-shadow-style');
            });
            $("a").on("touchmove", function(e){
                $(e.target).removeClass('box-shadow-style');
            });
            $("a").on("touchend", function(e){
                $(e.target).removeClass('box-shadow-style');
            });
        }, 500);

        $scope.isFaild = false;
        $scope.isSuccess = false;

        $scope.type = $location.search()['type'];
        $scope.is_yue = $location.search()['is_yue'];
        $scope.code = $location.search()['code'];
		
        $scope.wap_do_pay = function(type, isYue) {
            var partner = "ZLAPITOH5WAP";
            var timestamp = Date.parse(new Date())/1000;
            var sign = $.md5(partner + timestamp + key);
            var info = "";
            for(var i = 0; i < $rootScope.shopList.length; i++){
                info += $rootScope.shopList[i].id + "," + $rootScope.shopList[i].num + "|";
            }
            if(info != ""){
                info = info.substring(0, info.length - 1);
            }
            
            console.log(info);
			console.log($scope.is_yue);
			console.log($scope.code);
			
            
            showLoading();
            if(type == 'wxpay'){
                var url = "../index.php/yunapi/cart/wap_do_pay?data=";
                var ss = '{"partner":"'+partner+'","timestamp":"'+timestamp+'","sign":"'+sign+'","uid":"'+$rootScope.holdUID+'","info":"'+info+'","pay_type":"'+type+'","is_yue":"'+isYue+'"}';
                url = url + ss;
                window.location.href = url;
                delCookie("shopList");
                return false;

            }

            $http({method: 'POST', data: {partner: partner, timestamp: timestamp, sign: sign, uid: $rootScope.holdUID, info: info, pay_type: type, is_yue: isYue}, url: "../index.php/yunapi/cart/wap_do_pay"})
                .success(function (response, status, headers, config) {
                    /*var str = JSON.stringify(response);
                     console.log(str);
                     return false;*/
                    if (response.status == "1") {
                        if(response.status_type == "2"){
                            if (type == 'alipay') {
                                $("#resultPay").html(response.data);
                            }else{
                                window.location.href=response.data;
                            }
                            /*$("#resultPay").html(response.data);
                            setTimeout(function(){
                                document.forms['alipaysubmit'].submit();
                            }, 500);*/

                        }else if(response.status_type == "1"){
                            $rootScope.shopList = [];
                            delCookie("shopList");
                            $scope.isSuccess = true;
                            $scope.resultData = response.data;
                        }
                        setTimeout(function(){
                            $("a").on("touchstart", function(e){
                                $(e.target).addClass('box-shadow-style');
                            });
                            $("a").on("touchmove", function(e){
                                $(e.target).removeClass('box-shadow-style');
                            });
                            $("a").on("touchend", function(e){
                                $(e.target).removeClass('box-shadow-style');
                            });
                        }, 500);
                    } else if(response.status == "2") {
                        if (response.status_type == "1") {
                            $rootScope.shopList = [];
                            $scope.isChongSuccess = true;
                            $rootScope.showAlert(response.message);
                        }else{
                            $rootScope.shopList = [];
                            $scope.isChongFaild = true;
                            $rootScope.showAlert(response.message);
                        }
                    } else {
                        $rootScope.shopList = [];
                        delCookie("shopList");
                        $scope.isFaild = true;
                        $rootScope.showAlert(response.message);
                    }
                    hideLoading();
                })
                .error(function (response, status, headers, config) {
                    hideLoading();
                });
        };

        $scope.do_get_shoplist = function() {
            showLoading();
            var partner = "ZLAPITOH5WAP";
            var timestamp = Date.parse(new Date())/1000;
            var sign = $.md5(partner + timestamp + key);
            $http({method: 'POST', data: {partner: partner, timestamp: timestamp, sign: sign, code: $scope.code, uid: $rootScope.holdUID}, url: "../index.php/yunapi/cart/do_get_shoplist"})
                .success(function (response, status, headers, config) {
                    $rootScope.shopList = [];
                    delCookie("shopList");
                    if (response.status == "1") {
                        $scope.isSuccess = true;
                        $scope.resultData = response.data;
                        setTimeout(function(){
                            $("a").on("touchstart", function(e){
                                $(e.target).addClass('box-shadow-style');
                            });
                            $("a").on("touchmove", function(e){
                                $(e.target).removeClass('box-shadow-style');
                            });
                            $("a").on("touchend", function(e){
                                $(e.target).removeClass('box-shadow-style');
                            });
                        }, 500);
                    } else {
                        $rootScope.showAlert(response.message);
                    }
                    hideLoading();
                })
                .error(function (response, status, headers, config) {
                    hideLoading();
                });
        };

        if(isNull($scope.code)){
            $scope.wap_do_pay($scope.type, $scope.is_yue);
        }else if($scope.code == 0){
            $rootScope.shopList = [];
            delCookie("shopList");
            $scope.isFaild = true;
        }else{
            $scope.do_get_shoplist();
        }

    })
    //中奖确认
    .controller('winningDetailCtrl', function ($scope, $http, $location, $rootScope) {
        document.title = "中奖确认";
        $('body').scrollTop(0);
        $("#headerId").hide();
        $("#body_top").css('height','0px');
        $("#shopList").hide();
        $scope.backTo = function(){
            window.history.go(-1);
        };
        setTimeout(function(){
            $("a").on("touchstart", function(e){
                $(e.target).addClass('box-shadow-style');
            });
            $("a").on("touchmove", function(e){
                $(e.target).removeClass('box-shadow-style');
            });
            $("a").on("touchend", function(e){
                $(e.target).removeClass('box-shadow-style');
            });
        }, 500);

        $scope.id = $location.search()['id'];

        $scope.get_win_records_info = function() {
            showLoading();
            var partner = "ZLAPITOH5WAP";
            var timestamp = Date.parse(new Date())/1000;
            var sign = $.md5(partner + timestamp + key);
            $http({method: 'POST', data: {partner: partner, timestamp: timestamp, sign: sign, id: $scope.id, uid: $rootScope.holdUID}, url: "../index.php/yunapi/member/get_win_records_info"})
                .success(function (response, status, headers, config) {
                    if (response.status == "1") {
                        $scope.winningInfo = response.data;
                        setTimeout(function(){
                            $("a").on("touchstart", function(e){
                                $(e.target).addClass('box-shadow-style');
                            });
                            $("a").on("touchmove", function(e){
                                $(e.target).removeClass('box-shadow-style');
                            });
                            $("a").on("touchend", function(e){
                                $(e.target).removeClass('box-shadow-style');
                            });
                        }, 500);
                    } else {
                        $rootScope.showAlert(response.message);
                    }
                    hideLoading();
                })
                .error(function (response, status, headers, config) {
                    hideLoading();
                });
        };
        $scope.do_confirm_receiving = function() {
            showLoading();
            var partner = "ZLAPITOH5WAP";
            var timestamp = Date.parse(new Date())/1000;
            var sign = $.md5(partner + timestamp + key);
            $http({method: 'POST', data: {partner: partner, timestamp: timestamp, sign: sign, rid: $scope.id, uid: $rootScope.holdUID}, url: "../index.php/yunapi/member/do_confirm_receiving"})
                .success(function (response, status, headers, config) {
                    if (response.status == "1") {
                        $rootScope.showAlert(response.message);
                        window.location.reload();
                    } else {
                        $rootScope.showAlert(response.message);
                    }
                    hideLoading();
                })
                .error(function (response, status, headers, config) {
                    hideLoading();
                });
        };

        $scope.get_member_address = function () {
            showLoading();
            var partner = "ZLAPITOH5WAP";
            var timestamp = Date.parse(new Date())/1000;
            var sign = $.md5(partner + timestamp + key);
            $http({method: 'POST', data: {partner: partner, timestamp: timestamp, sign: sign, uid: $rootScope.holdUID}, url: "../index.php/yunapi/member/get_member_address"})
                .success(function (response, status, headers, config) {
                    if (response.status == "1") {
                        if(response.data){
                            $rootScope.showAlert(response.data, 1, $scope.id);
                        }else{
                            $rootScope.showAlert("还没有收货地址，请添加");
                            $location.url("/tab/addAddress?uid=" + $rootScope.holdUID);
                        }
                    } else {
                        $rootScope.showAlert(response.message);
                    }
                    hideLoading();
                })
                .error(function (response, status, headers, config) {
                    hideLoading();
                });
        };

        $scope.confirmReceive = function(){
            $rootScope.confirmMsg = "确定已收货了吗？";
            $('#confirmModal').modal('show');
        };

        $scope.confirmAddress = function(){
            $scope.get_member_address();
        };

        $('#confirmModal').on('hide.bs.modal', function () {
            if($rootScope.holdState == 1){
                $rootScope.holdState = 0;
                $scope.do_confirm_receiving();
            }
        });

        $scope.get_win_records_info();

    })
    //修改地址
    .controller('updateAddressCtrl', function ($scope, $http, $location, $rootScope) {
        document.title = "修改地址";
        $('body').scrollTop(0);
        $("#headerId").hide();
        $("#body_top").css('height','0px');
        $("#shopList").hide();
        $scope.isSelected = true;
        $scope.default = 1;
        $scope.setSelectedItem = function(){
            if($scope.isSelected){
                $scope.isSelected = false;
                $scope.default = 0;
            } else {
                $scope.isSelected = true;
                $scope.default = 1;
            }
        };
        setTimeout(function(){
            $("a").on("touchstart", function(e){
                $(e.target).addClass('box-shadow-style');
            });
            $("a").on("touchmove", function(e){
                $(e.target).removeClass('box-shadow-style');
            });
            $("a").on("touchend", function(e){
                $(e.target).removeClass('box-shadow-style');
            });
        }, 500);
        $scope.backTo = function(){
            window.history.go(-1);
        };

        $scope.id = $location.search()['id'];

        $scope.showLocation = function(province , city , town) {
            var loc	= new Location();
            var title	= ['省份' , '地级市' , '市、县、区'];
            $.each(title , function(k , v) {
                title[k]	= '<option value="">'+v+'</option>';
            });

            $('#loc_province').append(title[0]);
            $('#loc_city').append(title[1]);
            $('#loc_town').append(title[2]);

            $('#loc_province').change(function() {
                $('#loc_city').empty();
                $('#loc_city').append(title[1]);
                loc.fillOption('loc_city' , '0,'+$('#loc_province').val());
                $('#loc_town').empty();
                $('#loc_town').append(title[2]);
                $scope.sheng = $("#loc_province option:selected").text();
            });

            $('#loc_city').change(function() {
                $('#loc_town').empty();
                $('#loc_town').append(title[2]);
                loc.fillOption('loc_town' , '0,' + $('#loc_province').val() + ',' + $('#loc_city').val());
                $scope.shi = $("#loc_city option:selected").text();
            });

            $('#loc_town').change(function() {
                $scope.xian = $("#loc_town option:selected").text();
            });

            if (province) {
                loc.fillOption('loc_province' , '0' , province);

                if (city) {
                    loc.fillOption('loc_city' , '0,'+province , city);

                    if (town) {
                        loc.fillOption('loc_town' , '0,'+province+','+city , town);
                    }
                }

            } else {
                loc.fillOption('loc_province' , '0');
            }
        };

        $scope.isSave = false;

        $scope.do_save_member_address = function ($event) {
            /*$($event.target).css('color', '#f81000');
            setTimeout(function(){
                $($event.target).css('color', '#0079fe');
            }, 160);*/
            if(isNull($scope.shouhuoren)){
                $rootScope.showAlert("收货人不能为空");
                return;
            }
            if(isNull($scope.mobile)){
                $rootScope.showAlert("收货人手机号码不能为空");
                return;
            } else if($scope.mobile.length != 11){
                $rootScope.showAlert("请输入有效的手机号码");
                return;
            } else {
                var reg = /^(((13[0-9]{1})|(15[0-9]{1})|(18[0-9]{1}))+\d{8})$/;
                if(!reg.test($scope.mobile)){
                    $rootScope.showAlert("请输入有效的手机号码");
                    return;
                }
            }
            if(isNull($scope.sheng)){
                $rootScope.showAlert("请选择省份");
                return;
            }
            if(isNull($scope.shi)){
                $rootScope.showAlert("请选择城市");
                return;
            }
            if(isNull($scope.xian)){
                $rootScope.showAlert("请选择地区");
                return;
            }
            if(isNull($scope.jiedao)){
                $rootScope.showAlert("请输入街道号");
                return;
            }
            if($scope.isSave){
                return;
            }
            $scope.isSave = true;
            showLoading();
            var partner = "ZLAPITOH5WAP";
            var timestamp = Date.parse(new Date())/1000;
            var sign = $.md5(partner + timestamp + key);
            $http({
            	method: 'POST',
            	data: {
            		partner: partner,
            		timestamp: timestamp,
            		sign: sign,
            		uid: $rootScope.holdUID,
            		shouhuoren: $scope.shouhuoren,
            		mobile: $scope.mobile,
            		sheng: $scope.sheng,
            		shi: $scope.shi,
            		xian: $scope.xian,
            		jiedao: $scope.jiedao,
            		default: $scope.default,
            		id: $scope.id
            	},
            	url: "../index.php/yunapi/member/do_save_member_address"
            	})
                .success(function (response, status, headers, config) {
                    if (response.status == "1") {
                        $location.url("/tab/address?uid=" + $rootScope.holdUID);
                    } else {
                        $scope.isSave = false;
                        $rootScope.showAlert(response.message);
                    }
                    hideLoading();
                })
                .error(function (response, status, headers, config) {
                    $scope.isSave = false;
                    hideLoading();
                });
        };

        $scope.get_member_address = function () {
            showLoading();
            var partner = "ZLAPITOH5WAP";
            var timestamp = Date.parse(new Date())/1000;
            var sign = $.md5(partner + timestamp + key);
            $http({method: 'POST', data: {partner: partner, timestamp: timestamp, sign: sign, uid: $rootScope.holdUID, id: $scope.id}, url: "../index.php/yunapi/member/get_member_address"})
                .success(function (response, status, headers, config) {
                    /*var str = JSON.stringify(response);
                     $rootScope.showAlert(str);*/
                    if (response.status == "1") {
                        $scope.shouhuoren = response.data.shouhuoren;
                        $scope.mobile = response.data.mobile;
                        $scope.sheng = response.data.sheng;
                        $scope.shi = response.data.shi;
                        $scope.xian = response.data.xian;
                        $scope.jiedao = response.data.jiedao;
                        if(response.data.is_default == "Y"){
                            $scope.default = 1;
                            $scope.isSelected = true;
                        }else{
                            $scope.default = 0;
                            $scope.isSelected = false;
                        }
                        $scope.setAddress($scope.sheng, $scope.shi, $scope.xian);
                    } else {
                        $rootScope.showAlert(response.message);
                    }
                    hideLoading();
                })
                .error(function (response, status, headers, config) {
                    hideLoading();
                });
        };

        $scope.setAddress = function(province , city , town){
            var loc	= new Location();
            var selectList = document.getElementById("loc_province");
            for(var i = 0;i < selectList.options.length; i++){
                if(selectList.options[i].text == province){
                    selectList.options[i].selected = true;
                    $('#loc_city').empty();
                    loc.fillOption('loc_city' , '0,'+$('#loc_province').val());
                    break;
                }
            }

            var selectList1 = document.getElementById("loc_city");
            for(var j = 0;j < selectList1.options.length; j++){
                if(selectList1.options[j].text == city){
                    selectList1.options[j].selected = true;
                    $('#loc_town').empty();
                    loc.fillOption('loc_town' , '0,' + $('#loc_province').val() + ',' + $('#loc_city').val());
                    break;
                }
            }

            var selectList2 = document.getElementById("loc_town");
            for(var k = 0;k < selectList2.options.length; k++){
                if(selectList2.options[k].text == town){
                    selectList2.options[k].selected = true;
                    break;
                }
            }
        };

        $scope.do_del_member_address = function () {
            showLoading();
            var partner = "ZLAPITOH5WAP";
            var timestamp = Date.parse(new Date())/1000;
            var sign = $.md5(partner + timestamp + key);
            $http({method: 'POST', data: {partner: partner, timestamp: timestamp, sign: sign, uid: $rootScope.holdUID, id: $scope.id}, url: "../index.php/yunapi/member/do_del_member_address"})
                .success(function (response, status, headers, config) {
                    if (response.status == "1") {
                        //$rootScope.showAlert(response.message);
                        $location.url("/tab/address?uid=" + $rootScope.holdUID);
                    } else {
                        $rootScope.showAlert(response.message);
                    }
                    hideLoading();
                })
                .error(function (response, status, headers, config) {
                    hideLoading();
                });
        };

        $scope.delAddress = function($event){
            /*$($event.target).css('color', '#656565');
            setTimeout(function(){
                $($event.target).css('color', '#DA3752');
            }, 160);*/
            $rootScope.confirmMsg = "确定删除本条收货地址吗？";
            $('#confirmModal').modal('show');
        };

        $('#confirmModal').on('hide.bs.modal', function () {
            if($rootScope.holdState == 1){
                $rootScope.holdState = 0;
                $scope.do_del_member_address();
            }
        });

        $scope.showLocation();
        $scope.get_member_address();
    })
    //APP商品详情页
	.controller('appProductInfoCtrl', function ($scope, $http, $location, $rootScope) {		//APP端商品图文详情
        document.title = "商品详情";
        $('body').scrollTop(0);
        $("#headerId").hide();
        $("#body_top").css('height','0px');
        $("#shopList").hide();
        $("#footbar").hide();	//不显示底部广告
        $scope.backTo = function(){
            window.history.go(-1);
        };
        setTimeout(function(){
            $("a").on("touchstart", function(e){
                $(e.target).addClass('box-shadow-style');
            });
            $("a").on("touchmove", function(e){
                $(e.target).removeClass('box-shadow-style');
            });
            $("a").on("touchend", function(e){
                $(e.target).removeClass('box-shadow-style');
            });
        }, 500);

        $scope.id = $location.search()['id'];
        $scope.get_goods_pic_content = function () {
            showLoading();
            var partner = "ZLAPITOH5WAP";
            var timestamp = Date.parse(new Date())/1000;
            var sign = $.md5(partner + timestamp + key);
            $http({method: 'POST', data: {partner: partner, timestamp: timestamp, sign: sign, id: $scope.id}, url: "../index.php/yunapi/goods/get_goods_pic_content"})
                .success(function (response, status, headers, config) {
                    /*var str = JSON.stringify(response);
                    $rootScope.showAlert(str);*/
                    if (response.status == "1") {
                        $("#productInfoData").append(response.data.content);
                        setTimeout(function(){
                            $("a").on("touchstart", function(e){
                                $(e.target).addClass('box-shadow-style');
                            });
                            $("a").on("touchmove", function(e){
                                $(e.target).removeClass('box-shadow-style');
                            });
                            $("a").on("touchend", function(e){
                                $(e.target).removeClass('box-shadow-style');
                            });
                        }, 500);
                    } else {
                        $rootScope.showAlert(response.message);
                    }
                    hideLoading();
                })
                .error(function (response, status, headers, config) {
                    hideLoading();
                });
        };
        $scope.get_goods_pic_content();
    })
	//App计算结果页
	.controller('appCalculationCtrl', function ($scope, $http, $location, $rootScope) {  //APP端计算详情
        document.title = "计算结果";
        $('body').scrollTop(0);
        $("#headerId").hide();
        $("#body_top").css('height','0px');
        $("#shopList").hide();
        $("#footbar").hide();	//不显示底部广告
        $scope.backTo = function(){
            window.history.go(-1);
        };
        $scope.isUp = true;
        $scope.setUpDown = function(){
            if($scope.isUp){
                $scope.isUp = false;
            }else{
                $scope.isUp = true;
            }
        }
        setTimeout(function(){
            $("a").on("touchstart", function(e){
                $(e.target).addClass('box-shadow-style');
            });
            $("a").on("touchmove", function(e){
                $(e.target).removeClass('box-shadow-style');
            });
            $("a").on("touchend", function(e){
                $(e.target).removeClass('box-shadow-style');
            });
        }, 500);

        $scope.id = $location.search()['id'];

        $scope.get_calc_details = function() {
            showLoading();
            var partner = "ZLAPITOH5WAP";
            var timestamp = Date.parse(new Date())/1000;
            var sign = $.md5(partner + timestamp + key);
            $http({method: 'POST', data: {partner: partner, timestamp: timestamp, sign: sign, id: $scope.id}, url: "../index.php/yunapi/goods/get_calc_details"})
                .success(function (response, status, headers, config) {
                    if (response.status == "1") {
                        $scope.calData = response.data;
                        setTimeout(function(){
                            $("a").on("touchstart", function(e){
                                $(e.target).addClass('box-shadow-style');
                            });
                            $("a").on("touchmove", function(e){
                                $(e.target).removeClass('box-shadow-style');
                            });
                            $("a").on("touchend", function(e){
                                $(e.target).removeClass('box-shadow-style');
                            });
                        }, 500);
                    } else {
                        $rootScope.showAlert(response.message);
                    }
                    hideLoading();
                })
                .error(function (response, status, headers, config) {
                    hideLoading();
                });
        };
        $scope.get_calc_details();

    })
    //支付结果
    .controller('rechargeResultCtrl', function ($scope, $http, $location, $rootScope) {
        document.title = "支付结果";
        $('body').scrollTop(0);
        $("#headerId").hide();
        $("#body_top").css('height','0px');
        $("#shopList").hide();
        $scope.backTo = function(){
            $location.url("/tab/home");
        };
        setTimeout(function(){
            $("a").on("touchstart", function(e){
                $(e.target).addClass('box-shadow-style');
            });
            $("a").on("touchmove", function(e){
                $(e.target).removeClass('box-shadow-style');
            });
            $("a").on("touchend", function(e){
                $(e.target).removeClass('box-shadow-style');
            });
        }, 500);

        $scope.isFaild = false;
        $scope.isSuccess = false;

        $scope.ledou = $location.search()['ledou'];
        if($scope.ledou == "-1"){
            $scope.isFaild = true;
        }else{
            $scope.isSuccess = true;
        }

    })
    //服务协议
    .controller('agreementCtrl', function ($scope, $http, $location, $rootScope) {
        document.title = "服务协议";
        $('body').scrollTop(0);
        $("#headerId").hide();
        $("#body_top").css('height','0px');
        $("#shopList").hide();
        $scope.backTo = function(){
            window.history.go(-1);
        };
		$("#footbar").hide();

        setTimeout(function(){
            $("a").on("touchstart", function(e){
                $(e.target).addClass('box-shadow-style');
            });
            $("a").on("touchmove", function(e){
                $(e.target).removeClass('box-shadow-style');
            });
            $("a").on("touchend", function(e){
                $(e.target).removeClass('box-shadow-style');
            });
        }, 500);

    })
    //常见问题
    .controller('questionCtrl', function ($scope, $http, $location, $rootScope) {
        document.title = "常见问题";
        $('body').scrollTop(0);
        $("#headerId").hide();
        $("#body_top").css('height','0px');
        $("#shopList").hide();
        $scope.backTo = function(){
            window.history.go(-1);
        };
		$("#footbar").hide();

        setTimeout(function(){
            $("a").on("touchstart", function(e){
                $(e.target).addClass('box-shadow-style');
            });
            $("a").on("touchmove", function(e){
                $(e.target).removeClass('box-shadow-style');
            });
            $("a").on("touchend", function(e){
                $(e.target).removeClass('box-shadow-style');
            });
        }, 500);

    })
   	//好友返利
   	.controller('friendRebateCtrl', function ($scope, $http, $location, $rootScope) {
        document.title = "好友返利";
        $('body').scrollTop(0);
        $("#headerId").hide();
        $("#body_top").css('height','0px');
        $("#shopList").hide();
        $scope.backTo = function(){
            window.history.go(-1);
        };
		$("#footbar").hide();

        setTimeout(function(){
            $("a").on("touchstart", function(e){
                $(e.target).addClass('box-shadow-style');
            });
            $("a").on("touchmove", function(e){
                $(e.target).removeClass('box-shadow-style');
            });
            $("a").on("touchend", function(e){
                $(e.target).removeClass('box-shadow-style');
            });
        }, 500);

    })
    //邀请攻略
    .controller('inviteCtrl', function ($scope, $http, $location, $rootScope) {
        document.title = "邀请攻略";
        $('body').scrollTop(0);
        $("#headerId").hide();
        $("#body_top").css('height','0px');
        $("#shopList").hide();
        $scope.backTo = function(){
            window.history.go(-1);
        };
		$("#footbar").hide();

        setTimeout(function(){
            $("a").on("touchstart", function(e){
                $(e.target).addClass('box-shadow-style');
            });
            $("a").on("touchmove", function(e){
                $(e.target).removeClass('box-shadow-style');
            });
            $("a").on("touchend", function(e){
                $(e.target).removeClass('box-shadow-style');
            });
        }, 500);

    })
    //通知
    .controller('notificationCtrl', function ($scope, $http, $location, $rootScope) {
        document.title = "通知";
        $('body').scrollTop(0);
        $("#headerId").hide();
        $("#shopList").hide();
        $("#body_top").css('height','0px');
        $scope.backTo = function(){
            window.history.go(-1);
        };
		$("#footbar").hide();

        setTimeout(function(){
            $("a").on("touchstart", function(e){
                $(e.target).addClass('box-shadow-style');
            });
            $("a").on("touchmove", function(e){
                $(e.target).removeClass('box-shadow-style');
            });
            $("a").on("touchend", function(e){
                $(e.target).removeClass('box-shadow-style');
            });
        }, 500);

        $scope.get_article_list = function(page, size, type) {
            showLoading();
            var partner = "ZLAPITOH5WAP";
            var timestamp = Date.parse(new Date())/1000;
            var sign = $.md5(partner + timestamp + key);
            $http({method: 'POST', data: {partner: partner, timestamp: timestamp, sign: sign, page: page, size: size, type: type}, url: "../index.php/yunapi/index/get_article_list"})
                .success(function (response, status, headers, config) {
                    if (response.status == "1") {
                        $scope.listData = response.data;
                        setTimeout(function(){
                            $("a").on("touchstart", function(e){
                                $(e.target).addClass('box-shadow-style');
                            });
                            $("a").on("touchmove", function(e){
                                $(e.target).removeClass('box-shadow-style');
                            });
                            $("a").on("touchend", function(e){
                                $(e.target).removeClass('box-shadow-style');
                            });
                        }, 500);
                    } else {
                        $rootScope.showAlert(response.message);
                    }
                    hideLoading();
                })
                .error(function (response, status, headers, config) {
                    hideLoading();
                });
        };
        $scope.get_article_list(1, 20, 3);

    })
    //发现页面
    .controller('discoverCtrl', function ($scope, $http, $location, $rootScope) {
        document.title = "发现";
        $('body').scrollTop(0);
        $("#headerId").hide();
        $("#body_top").css('height','0px');
        $("#shopList").hide();
        $scope.backTo = function(){
            window.history.go(-1);
        };
		$("#footbar").hide();

        setTimeout(function(){
            $("a").on("touchstart", function(e){
                $(e.target).addClass('box-shadow-style');
            });
            $("a").on("touchmove", function(e){
                $(e.target).removeClass('box-shadow-style');
            });
            $("a").on("touchend", function(e){
                $(e.target).removeClass('box-shadow-style');
            });
        }, 500);

        $scope.get_article_list = function(page, size, type) {
            showLoading();
            var partner = "ZLAPITOH5WAP";
            var timestamp = Date.parse(new Date())/1000;
            var sign = $.md5(partner + timestamp + key);
            $http({method: 'POST', data: {partner: partner, timestamp: timestamp, sign: sign, page: page, size: size, type: type}, url: "../index.php/yunapi/index/get_article_list"})
                .success(function (response, status, headers, config) {
                    if (response.status == "1") {
                        $scope.listData = response.data;
                        setTimeout(function(){
                            $("a").on("touchstart", function(e){
                                $(e.target).addClass('box-shadow-style');
                            });
                            $("a").on("touchmove", function(e){
                                $(e.target).removeClass('box-shadow-style');
                            });
                            $("a").on("touchend", function(e){
                                $(e.target).removeClass('box-shadow-style');
                            });
                        }, 500);
                    } else {
                        $rootScope.showAlert(response.message);
                    }
                    hideLoading();
                })
                .error(function (response, status, headers, config) {
                    hideLoading();
                });
        };
        $scope.get_article_list(1, 20, 1);

    })
    //标题信息
    .controller('articleInfoCtrl', function ($scope, $http, $location, $rootScope) {
        $('body').scrollTop(0);
        $("#headerId").hide();
        $("#body_top").css('height','0px');
        $("#shopList").hide();
        $scope.backTo = function(){
            window.history.go(-1);
        };
		$("#footbar").hide();

        setTimeout(function(){
            $("a").on("touchstart", function(e){
                $(e.target).addClass('box-shadow-style');
            });
            $("a").on("touchmove", function(e){
                $(e.target).removeClass('box-shadow-style');
            });
            $("a").on("touchend", function(e){
                $(e.target).removeClass('box-shadow-style');
            });
        }, 500);

        $scope.id = $location.search()['id'];

        $scope.get_article_info = function() {
            showLoading();
            var partner = "ZLAPITOH5WAP";
            var timestamp = Date.parse(new Date())/1000;
            var sign = $.md5(partner + timestamp + key);
            $http({method: 'POST', data: {partner: partner, timestamp: timestamp, sign: sign, id: $scope.id}, url: "../index.php/yunapi/index/get_article_info"})
                .success(function (response, status, headers, config) {
                    if (response.status == "1") {
                        document.title = response.data.title;
                        $("#articleInfoData").append(response.data.content);
                        setTimeout(function(){
                            $("a").on("touchstart", function(e){
                                $(e.target).addClass('box-shadow-style');
                            });
                            $("a").on("touchmove", function(e){
                                $(e.target).removeClass('box-shadow-style');
                            });
                            $("a").on("touchend", function(e){
                                $(e.target).removeClass('box-shadow-style');
                            });
                        }, 500);
                    } else {
                        $rootScope.showAlert(response.message);
                    }
                    hideLoading();
                })
                .error(function (response, status, headers, config) {
                    hideLoading();
                });
        };
        $scope.get_article_info();

    })
    //关于易中夺宝
    .controller('aboutCtrl', function ($scope, $http, $location, $rootScope) {
        document.title = "什么是易中夺宝";
        $('body').scrollTop(0);
        $("#headerId").hide();
        $("#body_top").css('height','0px');
        $("#shopList").hide();
        $scope.backTo = function(){
            window.history.go(-1);
        };
		$("#footbar").hide();

        setTimeout(function(){
            $("a").on("touchstart", function(e){
                $(e.target).addClass('box-shadow-style');
            });
            $("a").on("touchmove", function(e){
                $(e.target).removeClass('box-shadow-style');
            });
            $("a").on("touchend", function(e){
                $(e.target).removeClass('box-shadow-style');
            });
        }, 500);

    })
	//隐私申明
	.controller('privacyCtrl', function ($scope, $http, $location, $rootScope) {		//ios需要的隐私声明
        document.title = "隐私声明";
        $('body').scrollTop(0);
        $("#headerId").hide();
        $("#body_top").css('height','0px');
        $("#shopList").hide();
        $scope.backTo = function(){
            window.history.go(-1);
        };
		$("#footbar").hide();
    })
	//网络游戏和个人信息保护
	.controller('protectCtrl', function ($scope, $http, $location, $rootScope) {		//ios需要的网络游戏和个人信息保护
        document.title = "信息保护";
        $('body').scrollTop(0);
        $("#headerId").hide();
        $("#body_top").css('height','0px');
        $("#shopList").hide();
        $scope.backTo = function(){
            window.history.go(-1);
        };
		$("#footbar").hide();
    })
	//服务条款
	.controller('termOfServiceCtrl', function ($scope, $http, $location, $rootScope) {		//ios 服务条款
        document.title = "服务条款";
        $('body').scrollTop(0);
        $("#headerId").hide();
        $("#body_top").css('height','0px');
        $("#shopList").hide();
        $scope.backTo = function(){
            window.history.go(-1);
        };
		$("#footbar").hide();
    })
	
	.controller('lemonCtrl', function ($scope, $http, $location, $rootScope) {		//ios 服务条款
		
		document.title = "活动赠奖金";
        $('body').scrollTop(0);
        $("#headerId").hide();
        $("#body_top").css('height','0px');
        $("#shopList").hide();
        $scope.backTo = function(){
            window.history.go(-1);
        };
		$("#footbar").hide();
		
		$scope.go_login = function(){
			$location.url('tab/login');
		}
		
		$scope.show_codeimg = function(){
			if($rootScope.holdUID > 0 ){
				var codeimg = getCookie('codeimg');
				$rootScope.showPic(codeimg);
			}else{
				$rootScope.showAlert('您还没有登录！');
			}
		}
		
		$scope.go_recharge = function(){
			if($rootScope.holdUID > 0 ){
				$location.url('tab/recharge');
			}else{
				$rootScope.showAlert('您还没有登录！');
			}
		}
		
		
	});
