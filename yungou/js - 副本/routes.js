/**
 * Defines the main routes in the application.
 * The routes you see here will be anchors '#/' unless specifically configured otherwise.
 * 路由控制中心
 *
 */

define(['./app'], function (app) {
    'use strict';
    return app.config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/view1', {
            templateUrl: 'partials/partial1.html',
            controller: 'MyCtrl1'
        });

        $routeProvider.when('/view2', {
            templateUrl: 'partials/partial2.html',
            controller: 'MyCtrl2'
        });

        $routeProvider.otherwise({
            redirectTo: '/tab/home'
        });

        $routeProvider
            .when('/tab/home', {
                templateUrl: 'template/home.html',
                controller: 'homeCtrl'
            })
            .when('/tab/list', {
                templateUrl: 'template/list.html',
                controller: 'listCtrl'
            })
            .when('/tab/share', {
                templateUrl: 'template/share.html',
                controller: 'shareCtrl'
            })
            .when('/tab/xiangou', {
                templateUrl: 'template/index/xiangou.html',
                controller: 'xiangouCtrl'
            })
            .when('/tab/productDetail', {
                templateUrl: 'template/productDetail.html',
                controller: 'productDetailCtrl'
            })
            .when('/tab/gorecorde', {
                templateUrl: 'template/index/gorecorde.html',
                controller: 'gorecordeCtrl'
            })
            .when('/tab/productInfo', {
                templateUrl: 'template/productInfo.html',
                controller: 'productInfoCtrl'
            })
            .when('/tab/passedPublish', {
                templateUrl: 'template/passedPublish.html',
                controller: 'passedPublishCtrl'
            })
            .when('/tab/qishuList', {
                templateUrl: 'template/index/qishuList.html',
                controller: 'qishuListCtrl'
            })
            .when('/tab/productShare', {
                templateUrl: 'template/productShare.html',
                controller: 'productShareCtrl'
            })
            .when('/tab/login', {
                templateUrl: 'template/login.html',
                controller: 'loginCtrl'
            })
            .when('/tab/callback',{
                templateUrl: 'template/login/callback.html',
                controller: 'callbackCtrl'
            })
            .when('/tab/bindwx', {
                templateUrl: 'template/login/bindwx.html',
                controller: 'bindwxCtrl'
            })
            .when('/tab/updatePassword', {
                templateUrl: 'template/updatePassword.html',
                controller: 'updatePasswordCtrl'
            })
            .when('/tab/rigister', {
                templateUrl: 'template/rigister.html',
                controller: 'rigisterCtrl'
            })
            .when('/tab/search', {
                templateUrl: 'template/search.html',
                controller: 'searchCtrl'
            })
            .when('/tab/searchResult', {
                templateUrl: 'template/searchResult.html',
                controller: 'searchResultCtrl'
            })
            .when('/tab/personCenter', {
                templateUrl: 'template/personCenter.html',
                controller: 'personCenterCtrl'
            })
            .when('/tab/rechargeRecord', {
                templateUrl: 'template/rechargeRecord.html',
                controller: 'rechargeRecordCtrl'
            })
            .when('/tab/winningRecord', {
                templateUrl: 'template/winningRecord.html',
                controller: 'winningRecordCtrl'
            })
            .when('/tab/snatchRecord', {
                templateUrl: 'template/snatchRecord.html',
                controller: 'snatchRecordCtrl'
            })
            .when('/tab/recharge', {
                templateUrl: 'template/recharge.html',
                controller: 'rechargeCtrl'
            })
            .when('/tab/personalInfo', {
                templateUrl: 'template/personalInfo.html',
                controller: 'personalInfoCtrl'
            })
            .when('/tab/address', {
                templateUrl: 'template/address.html',
                controller: 'addressCtrl'
            })
            .when('/tab/addAddress', {
                templateUrl: 'template/addAddress.html',
                controller: 'addAddressCtrl'
            })
            .when('/tab/updateAddress', {
                templateUrl: 'template/updateAddress.html',
                controller: 'updateAddressCtrl'
            })
            .when('/tab/updateNickname', {
                templateUrl: 'template/updateNickname.html',
                controller: 'updateNicknameCtrl'
            })
            .when('/tab/updatePhone', {
                templateUrl: 'template/updatePhone.html',
                controller: 'updatePhoneCtrl'
            })
            .when('/tab/shareDetail', {
                templateUrl: 'template/shareDetail.html',
                controller: 'shareDetailCtrl'
            })
            .when('/tab/shopList', {
                templateUrl: 'template/shopList.html',
                controller: 'shopListCtrl'
            })
            .when('/tab/pay', {
                templateUrl: 'template/pay.html',
                controller: 'payCtrl'
            })
            .when('/tab/payResult', {
                templateUrl: 'template/payResult.html',
                controller: 'payResultCtrl'
            })
            .when('/tab/payRe', {
                templateUrl: 'template/payRe.html',
                controller: 'payReCtrl'
            })
            .when('/tab/payResultState', {
                templateUrl: 'template/payResultState.html',
                controller: 'payResultStateCtrl'
            })
            .when('/tab/rechargeResult', {
                templateUrl: 'template/rechargeResult.html',
                controller: 'rechargeResultCtrl'
            })
            .when('/tab/winningDetail', {
                templateUrl: 'template/winningDetail.html',
                controller: 'winningDetailCtrl'
            })
            .when('/tab/calculation', {
                templateUrl: 'template/calculation.html',
                controller: 'calculationCtrl'
            })
            .when('/tab/agreement', {
                templateUrl: 'template/agreement.html',
                controller: 'agreementCtrl'
            })
            .when('/tab/question', {
                templateUrl: 'template/question.html',
                controller: 'questionCtrl'
            })
            .when('/tab/friendRebate', {
                templateUrl: 'template/friendRebate.html',
                controller: 'friendRebateCtrl'
            })
            .when('/tab/invite', {
                templateUrl: 'template/invite.html',
                controller: 'inviteCtrl'
            })
            .when('/tab/notification', {
                templateUrl: 'template/notification.html',
                controller: 'notificationCtrl'
            })
            .when('/tab/articleInfo', {
                templateUrl: 'template/articleInfo.html',
                controller: 'articleInfoCtrl'
            })
            .when('/tab/discover', {
                templateUrl: 'template/discover.html',
                controller: 'discoverCtrl'
            })
            .when('/tab/about', {
                templateUrl: 'template/about.html',
                controller: 'aboutCtrl'
            })
            .when('/tab/privacy', {		//隐私声明  ios
                templateUrl: 'template/privacy.html',
                controller: 'privacyCtrl'
            })
            .when('/tab/protect', {		//网络游戏xx 个人信息保护 ios
                templateUrl: 'template/protect.html',
                controller: 'protectCtrl'
            })
            .when('/tab/termOfService', {		//服务条款
                templateUrl: 'template/termOfService.html',
                controller: 'termOfServiceCtrl'
            })
            .when('/tab/appProductInfo', {		//APP端商品图文详情
                templateUrl: 'template/appProductInfo.html',
                controller: 'appProductInfoCtrl'
            })
            .when('/tab/appCalculation', {		//APP端计算详情
                templateUrl: 'template/appCalculation.html',
                controller: 'appCalculationCtrl'
            });

    }]);
});
