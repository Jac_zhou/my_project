<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="UTF-8">
    <meta name="viewport" content="width = device-width, initial-scale = 1.0, minimum-scale = 1.0, maximum-scale = 1.0, user-scalable = n"/>
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <link rel="stylesheet" href="css/page_style.css" type="text/css" media="all">
    <link rel="stylesheet" href="css/bootstrap.min.css"/>
    <script src="js/jquery.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/page_action.js"></script>
    <title></title>
    <style>
        body,div,span,p{margin: 0;padding: 0;font-family: Microsoft Yahei;}
        .page2_lev1{
            width: 100%;
            float: left;
            position: absolute;
        }
        .page2_lev1 img{
            width: 100%;

        }

        .page2_lev2,.page2_lev3,.page2_lev4{
            width: 100%;
            float: left;
            position: relative;
        }
        .page2_lev2 .pic_1{
            text-align: center;margin-bottom: 14px;
        }
        .action_top_p{
            padding-right: 20px;padding-left: 20px;
        }
        .page2_lev2 p{
            text-align: center;font-size:9px;
        }
        .page2_lev2 img{
            width: 74%;
        }
        .page2_lev3 .zhuce{
            width: 100%;
            float: left;
            margin-top: 53px;
        }
        .page2_lev3 .zhuce .zhuche_1{
            width: 50%;float: left;
        }
       .page2_lev3 .zhuce .zhuche_1 img{
            width: 60%;margin-top: -10px;
       }
        .page2_lev3 .zhuce .zhuche_2{
            width: 50%;
            float: left;
        }
        .page2_lev3 .zhuce .zhuche_2 img{
            width: 30%;
        }
        .page2_lev3 .zhuce .zhuche_2 div{
            width: 100%;font-size: 9px;
        }
        .title_1{
            padding-right: 10px;
        }
        .title_2{
            margin-top: 12px;
        }
        .button_1{
            width: 100%;text-align: center;float: left;margin-top: 24px;
        }
        .button_1 div{
            width: 36.55%;margin: 0 auto;
        }
        .button_1 div:hover{
            opacity: 0.8;
        }
        .button_1 div img{
            width:100%;
        }
        /*底部*/
        .page2_lev4{
            background:url("images/background_03.png") no-repeat;
            -webkit-background-size: 100%;
            background-size: 100%;
            margin-top: 52px;
        }
        .show_lev1{
            font-size: smaller;
            color: #ffffff;
            width: 100%;float: left;

        }
        .show_lev2{

            font-size: smaller;
            width: 100%;float: left;
            text-indent: 18px;
        }

        .show_right{
            padding-left: 5px;
            padding-right: 5px;
            color: #DA4453;
            float: left;
        }
        .show_right div:nth-child(2){
           padding-left: 38px;margin-top: 5px;
        }
        .show_right .name_1{
           font-size: 16px;
        }
        .show_right .number_1{
           text-indent: 0;
            text-align: center;
            width: 100%;
        }
        .show_right .number_1 div{
          font-size: 16px;
            color: #6D553A;
            border-bottom: 1px solid #6D553A;
            width: 20px;
            display: inline-block;
            margin-top: 2px;
        }
        .show_right .number_1 span{
            color: #6D553A;
        }
    </style>
</head>
<body>
<div class="bg_page2">
    <div class="page2_lev1">
        <img src="images/action_12.png">
    </div>
    <div class="page2_lev2">
        <div class="pic_1"><img src="images/zi.png"></div>
        <div class="action_top_p"><p>凡在指定促销日期内，注册成为“易中夺宝”会员并登陆者，便可获赠由“易中夺宝”提供的222元红包（每个ID只有一次获赠机会）</p></div>
    </div>
    <!--中间内容-->
    <div class="page2_lev3">
        <!--注册-->
        <div class="zhuce">
            <div class="zhuche_1">
                <img src="images/5hongbao.png">
            </div>
            <div class="zhuche_2">
                <div class="title_1">
                    <div><img src="images/wenzi.png"></div>
                    <div>注册送5元现金券</div>
                </div>
                <div class="title_1 title_2">
                    <div><img src="images/wenzi1.png"></div>
                    <div>注册时送的5元现金券，需要充值15元才能激活</div>
                </div>
            </div>
            <div class="button_1">
                <div><a href="/yungou/#/tab/rigister"><img src="images/zhuce.png"></a></div>
            </div>
        </div>
        <!--充值-->
        <div class="zhuce">
            <div class="zhuche_1">
                <img src="images/215.png">
            </div>
            <div class="zhuche_2">
                <div class="title_1">
                    <div><img src="images/wenzi2.png"></div>
                    <div>充值送215现金大礼包，多充多得 </div>
                </div>
                <div class="title_1 title_2">
                    <div><img src="images/wenzi1.png"></div>
                    <div>充值金额时以整数为计算单位，具体充值送现金红包比例为：<br>
                        100元----送8%<br>
                        200元----送9%<br>
                        300元----送11<br>
                        400元----送14%<br>
                        500元----送20%<br>
                        充值送红包的金额不包括激活现金券的15元</div>
                </div>
            </div>
            <div class="button_1">
                <div><a href="/yungou/#/tab/recharge"><img src="images/congzhi.png"></a></div>
            </div>
        </div>
        <!--分享-->
        <div class="zhuce">
            <div class="zhuche_1">
                <img src="images/2shuzi.png">
            </div>
            <div class="zhuche_2">
                <div class="title_1">
                    <div><img src="images/wenzi3.png"></div>
                    <div>凡是在“易中夺宝”众筹中奖的用户将产品晒出来，将中奖心语写出来就追加奖励红包2元</div>
                </div>

            </div>
            <div class="button_1">
                <div><a href="/yungou/#/tab/login"><img src="images/fenxiang.png"></a></div>
            </div>
        </div>
    </div>
    <!--底部展示-->
    <div class="page2_lev4">
        <div class="show_lev1">
           总需：<span>200</span><span>人次</span>
        </div>
        <div class="show_lev2">
            <!-- <div class="left_img"><img></div>-->
            <div class="show_right">
                <div>(第79期)Apple MacBook Air 13.3英寸128G 纤薄 轻巧 强劲</div>
                <div>恭喜：<span  class="name_1">李二柱</span></div>
                <div class="number_1">TA只购买了：<div><strong>10</strong></div>人次<span>中奖啦！</span></div>
            </div>
        </div>

    </div>
	
</div>
</body>
</html>