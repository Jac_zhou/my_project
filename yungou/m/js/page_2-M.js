/**
 * Created by Administrator on 2016/1/8.
 */
$(function(){
    var $Width=$(window).width();
    /*lev1*/
    var Height_1=312*$Width/512+"px";
    /*lev2*/
    var Height_2=250*$Width/512+"px";
    var Height_2_1=-1*$Width/512+"px";
    var Height_2_2=195*$Width/512+"px";
    var Height_2_3=23*$Width/512+"px";
    var Height_2_4=26*$Width/512+"px";
    /*lev3*/
    var Height_3=300*$Width/512+"px";
    var Height_3_1=-4*$Width/512+"px";
    var Height_3_2=200*$Width/512+"px";
    var Height_3_3=55*$Width/512+"px";
    var Height_3_4=30*$Width/512+"px";
    var Height_3_5=0.85*$Width+"px";
    var Height_3_6=15*$Width/512+"px";
    /*lev4*/
    var Height_4=433*$Width/512+"px";
    var Height_4_2=10*$Width/512+"px";
    var Height_4_3=0.9*$Width+"px";
    /*lev5*/
    var Height_5=419*$Width/512+"px";
    var Height_5_1=76*$Width/512+"px";
    var Height_5_2=146*$Width/512+"px";
    var Height_5_3=96*$Width/512+"px";
    var Height_5_4=20*$Width/512+"px";
    var Height_5_5=150*$Width/512+"px";
    var Height_5_6=10*$Width/512+"px";
    var Height_5_7=5*$Width/512+"px";
    var Height_5_8=3*$Width/512+"px";
    var Height_5_9=256*$Width/512+"px";
    var Height_5_10=25*$Width/512+"px";
    var Height_5_11=1*$Width/512+"px";
    /*lev1*/
    $(".P_lev_1").css("height",Height_1);
    /*lev2*/
    $(".P_lev_2").css(
        {
            "height":Height_2,
            "margin-top":Height_2_1,
        });
    $(".P_lev_2 .img_1").css("margin-top",Height_2_2);
    $(".P_lev_2 .img_1 img").css("margin-left",Height_2_3);
    $(".P_lev_2 .img_2").css("margin-top",Height_2_2);
    $(".P_lev_2 .img_2 img").css("margin-left",Height_2_4);
    $(".P_lev_2 .img_3").css("margin-top",Height_2_2);
    $(".P_lev_2 .img_3 img").css("margin-left",Height_2_4);
    /*lev3*/
    $(".P_lev_3").css(
        {
            "height":Height_3,
            "margin-top":Height_3_1,
        });
    $(".lev3_text").css({
        "width":Height_3_5,
        "height":Height_3_2,
        "padding-top":Height_3_3,
        "padding-left":Height_3_4,
    });
    $(".lev3_text div:nth-child(2),.lev3_text div:nth-child(3)").css("margin-top",Height_3_6);
    /*lev4*/
    $(".P_lev_4").css(
        {
            "height":Height_4,
            "margin-top":Height_3_1,
        });

    $(".P_lev_4 .rule_text").css({
        "width":Height_4_3,
        "height":Height_3_2,
        "padding-top":Height_3_3,
        "padding-left":Height_3_4,
    });
    $(".P_lev_4 .a123").css("padding-left",Height_3_4);

    /*lev5*/
    $(".P_lev_5").css(
        {
            "height":Height_5,
            "margin-top":Height_3_1,
        });

    $(".container_lev_1").css("margin-top",Height_5_8).css("margin-top",Height_5_2);
/*    $(".swiper-wrapper").css("margin-top",Height_5_8);
    $(".swiper-container").css("margin-left",Height_5_1);*/
    $(".img_left").css("width",Height_5_3);
    $(".img_left img").css("width",Height_5_3).css("margin-bottom",Height_5_4);
    $(".content_right").css("width",Height_5_5).css("padding-left",Height_5_6);
    $(".content_right div:nth-child(2)").css("margin-top",Height_5_7);
    $(".content_right div:nth-child(3)").css("margin-top",Height_5_11);
    $("#demo").css("width",Height_5_9).css("margin-left",Height_5_1);
    $(".slide_content").css("width",Height_5_9).css("margin-right",Height_5_10);
})
