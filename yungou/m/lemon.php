<!DOCTYPE html>
<html lang="en">

    <meta charset="UTF-8">
    <meta name="viewport" content="width = device-width, initial-scale = 1.0, minimum-scale = 1.0, maximum-scale = 1.0, user-scalable = n"/>
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <title></title>
    <link href="css/swiper.min.css" type="text/css" rel="stylesheet">
    <link href="css/page_2-M.css" type="text/css" rel="stylesheet">
    <script src="js/jquery.min.js" type="text/javascript"></script>
    <script src="js/swiper.min.js" type="text/javascript"></script>
    <script src="js/page_2-M.js" type="text/javascript"></script>
</head>
<body>
    <div class="Display_Box">
        <div class="P_lev_1"></div>
        <div class="P_lev_2">
            <div class="img_1">
                <a href="/yungou/index.html#/tab/rigister"><img src="images/zhuc.png"></a>
            </div>
            <div class="img_2">
               <a href="/yungou/index.html#/tab/rigister"><img src="images/chongzhi.png"></a>
            </div>
            <div class="img_3">
               <a href="/yungou/index.html#/tab/rigister"><img src="images/shaidan.png"></a>
            </div>
        </div>
        <div class="P_lev_3">
            <div class="lev3_text">
                <div >活动1：注册送5元现金券；</div>
                <div>活动2：充值送215现金大礼包，多充多得；</div>
                <div>活动3：分享奖有奖：凡是在“易中夺宝”众筹中奖的用户将产品晒出来，将中奖心语写出来就追加奖励红包2元。通过这些方式活跃会员气氛，并增加网站的公开、透明。</div>
            </div>
        </div>
        <div class="P_lev_4">
            <div class="rule_text" style="font-family: 'Microsoft YaHei'">
                <div class="rule_1">
                    <div><img style="margin-right: 5px; " src="images/dian.png">注册时送的5元现金券，需要充值15元才能激活。</div>
                </div>
                <div class="rule_1">
                    <div><img style="margin-right: 5px; " src="images/dian.png">活动期间，不同金额首笔充值均能领取现金红包。</div>
                </div>
                <div class="rule_1">
                    <div><img style="margin-right: 5px; " src="images/dian.png">充值金额时以整数为计算单位，具体充值送现金红包比例为：</div>
                </div>
                <div class="rule_list">
                    <div class="left">
                        <div class="a123" >100元----送8%</div>
                        <div class="a123" >300元----送11%</div>
                        <div class="a123" >500元----送20%</div>
                    </div>
                    <div class="right">
                        <div class="a123" > 200元----送9%</div>
                        <div class="a123" > 400元----送14%</div>
                    </div>
                </div>
                <div class="rule_1">
                    <div><img style="margin-right: 5px; " src="images/dian.png">充值送红包的金额不包括激活现金券的15元。</div>
                </div>
                <div class="rule_1">
                    <div><img style="margin-right: 5px;" src="images/dian.png">分享奖中奖送现金红包2元。</div>
                </div>
                <div class="rule_1">
                    <div><img style="margin-right: 5px; " src="images/dian.png">现金券需要激活才能使用；现金红包可以直接使用。</div>
                </div>
                <div class="b123" style="float: left" >凡在指定促销日期内，注册成为“易中夺宝”会员并登录者，便可获赠由“易中夺宝”提供的222元红包（每个ID只有一次获赠机会）</div>
            </div>
        </div>
        <div class="gap"  >

        </div>

       <div class="P_lev_5 LunBo">
           <div id="demo" style="overflow: hidden;">
               <table border="0" align="center" cellpadding="1" cellspacing="1" cellspace="0">
                   <tr>
                       <td id="demo1" valign="top">
                           <!-- 特别注意，下面的图片总宽度必须大于上面定义的demo的宽度，如上面demo的宽度为500px,则下面图片总宽度必须大于500,否则会出现些问题！ -->
                           <table border="0" cellspacing="0" cellpadding="0">
                               <tr id="lottery_list" align="center">
                                   
                                   <!--<td>
                                       <div class="container_lev_1">
                                           <div class=" CurrentPic slide_content">
                                               <div class="img_left">
                                                   <img src="images/shouji.jpg">
                                               </div>
                                               <div class="content_right">
                                                   <div> (第6期)佳能 EOS 70D 套机 (第6期)佳能 EOS 70D 套机.</div>
                                                   <div>恭喜</div>
                                                   <div>王大锤</div>
                                               </div>
                                               <div style="clear: both"> </div>
                                           </div>
                                       </div>
                                   </td>-->
                                   
                               </tr>
                           </table>
                       </td>
                       <td id="demo2" valign="top">
                       </td>
                   </tr>
               </table>
           </div>
        </div>
        <div style="clear: both"> </div>
    </div>
    <script>
			var _html = '';
			var url = "http://www.yz-db.com/index.php/go/lemon/get_new_lottery";
			$.ajaxSetup({async:false});
			$.get(url,{},function(_data){
				if(_data.status == '1'){
					$.each(_data.data,function(n,v){
						_html += '<td>'+
                        '<div class="container_lev_1">'+
                            '<div class=" CurrentPic slide_content">'+
                                '<div class="img_left">'+
                                '<img src="'+ v.thumb +'">'+
                                '</div>'+
                                '<div class="content_right">'+
                                '<div> (第'+v.qishu+'期)'+v.shopname+'</div>'+
                                    '<div>恭喜</div>'+
                                    '<div>'+v.username+'</div>'+
                                '</div>'+
                                '<div style="clear: both"> </div>'+
                            '</div>'+
                        '</div>'+
                    '</td>';
					});
					
					$('#lottery_list').html(_html);
					init_swiper();
				}else{
					$('.LunBo').hide();
				}
			},'json');
			
			function init_swiper()
			{
				var speed=50;
		        var demo = $("#demo");
		        var demo1 = $("#demo1");
		        var demo2 = $("#demo2");
		        demo2.html(demo1.html());
		        function Marquee(){
		            if(demo.scrollLeft()>=demo1.width())
		                demo.scrollLeft(0);
		            else{
		                demo.scrollLeft(demo.scrollLeft()+1);
		            }
		        }
		        var MyMar=setInterval(Marquee,speed)
		        demo.mouseover(function() {
		            clearInterval(MyMar);
		        } )
		        demo.mouseout(function() {
		            MyMar=setInterval(Marquee,speed);
		        } )
			}
			
    </script>

</body>
</html>