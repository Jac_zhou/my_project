<?php
/*
* 数据统计需要的数据
*/
$su = isset($_GET['su']) ? trim($_GET['su']) : '';	//渠道名称
$sk = isset($_GET['sk']) ? trim($_GET['sk']) : '';    //推广关键词
if(!empty($su)){
	setcookie('su',$su);
	setcookie('sk',$sk);
}
 ?>
<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="UTF-8">
    <meta name="viewport" content="width = device-width, initial-scale = 1.0, minimum-scale = 1.0, maximum-scale = 1.0, user-scalable = n"/>
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <link rel="stylesheet" href="css/page_style.css" type="text/css" media="all">
    <link rel="stylesheet" href="css/bootstrap.min.css"/>
    <script src="js/jquery.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/page_action.js"></script>
    <title></title>
    <style>
        body,div,span,p{margin: 0;padding: 0;font-family: Microsoft Yahei;}
        .bg_page1{
            background: url("images/bg123.png") no-repeat;
            -webkit-background-size: 100%;
            background-size: 100%;
        }

        .action_button1{
            background: url("images/222.png") no-repeat;
            -webkit-background-size: 100%;
            background-size: 100%;
            position: absolute;
        }
        .action_button2{
            background: url("images/linghongbao.png") no-repeat;
            -webkit-background-size: 100%;
            background-size: 100%;
            position: absolute;
        }
        .action_button3{
            background: url("images/kaiqiang.png") no-repeat;
            -webkit-background-size: 100%;
            background-size: 100%;
            position: absolute;
        }
        .action_button1:hover,.action_button2:hover,.action_button3:hover{
            opacity: 0.8;
        }

        .action_content{
            width: 100%;
            position: absolute;
            padding: 0 15px;
        }
        .action_content .action_people1,
        .action_content .action_people2,
        .action_content .action_people3,
        .action_content .action_people1 img,
        .action_content .action_people2 img,
        .action_content .action_people3 img{
            width: 100%;margin-bottom: 10px;
        }
        .action_content .action_people2 img{
            margin-bottom: 0;
        }

    </style>
</head>
<body>
    <div class="bg_page1">
        <a href="/yungou/#/tab/rigister"><div class="action_button1"></div></a>
        <a href="lemon.php"><div class="action_button2"></div></a>
        <a href="/yungou/#/tab/rigister"><div class="action_button3"></div></a>
        <div class="action_content">
            <div class="action_people1">
                <img src="images/baozupo_background_04.png">
            </div>
            <div class="action_people2">
                <img src="images/xiaomei_background_04.png">
            </div>
            <div class="action_people3">
                <img src="images/xiaowang_background_04.png">
            </div>
        </div>
    </div>
</body>
</html>