/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50547
Source Host           : localhost:3306
Source Database       : duobao

Target Server Type    : MYSQL
Target Server Version : 50547
File Encoding         : 65001

Date: 2016-08-31 09:48:06
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `go_active`
-- ----------------------------
DROP TABLE IF EXISTS `go_active`;
CREATE TABLE `go_active` (
`id`  int(11) NOT NULL AUTO_INCREMENT ,
`uid`  int(11) NOT NULL DEFAULT 0 COMMENT '用户名' ,
`type`  tinyint(4) NOT NULL DEFAULT 1 COMMENT '1为现金券，2位红包' ,
`cash_value`  decimal(10,2) NOT NULL DEFAULT 0.00 COMMENT '红包或者券的值' ,
`is_active`  tinyint(4) NOT NULL DEFAULT 0 COMMENT '激活，1为激活的' ,
`r_time`  int(11) NOT NULL DEFAULT 0 ,
`mobile`  varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '0' COMMENT '手机号码' ,
`is_use`  tinyint(4) NOT NULL DEFAULT 0 COMMENT '1为使用完了' ,
`money`  decimal(8,2) NOT NULL DEFAULT 0.00 COMMENT '充值的金额' ,
PRIMARY KEY (`id`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
AUTO_INCREMENT=148

;

-- ----------------------------
-- Table structure for `go_ad_area`
-- ----------------------------
DROP TABLE IF EXISTS `go_ad_area`;
CREATE TABLE `go_ad_area` (
`id`  int(10) UNSIGNED NOT NULL AUTO_INCREMENT ,
`title`  varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL ,
`width`  smallint(6) UNSIGNED NULL DEFAULT NULL ,
`height`  smallint(6) UNSIGNED NULL DEFAULT NULL ,
`des`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`checked`  tinyint(1) NULL DEFAULT 0 COMMENT '1表示通过' ,
PRIMARY KEY (`id`),
INDEX `checked` (`checked`) USING BTREE 
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
COMMENT='广告位'
AUTO_INCREMENT=5

;

-- ----------------------------
-- Table structure for `go_ad_data`
-- ----------------------------
DROP TABLE IF EXISTS `go_ad_data`;
CREATE TABLE `go_ad_data` (
`id`  int(10) UNSIGNED NOT NULL AUTO_INCREMENT ,
`aid`  int(10) UNSIGNED NOT NULL ,
`title`  varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL ,
`type`  char(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'code,text,img' ,
`content`  text CHARACTER SET utf8 COLLATE utf8_general_ci NULL ,
`checked`  tinyint(1) NULL DEFAULT 0 COMMENT '1表示通过' ,
`addtime`  int(10) UNSIGNED NOT NULL ,
`endtime`  int(10) UNSIGNED NOT NULL ,
PRIMARY KEY (`id`),
INDEX `type` (`type`) USING BTREE 
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
COMMENT='广告'
AUTO_INCREMENT=1

;

-- ----------------------------
-- Table structure for `go_admin`
-- ----------------------------
DROP TABLE IF EXISTS `go_admin`;
CREATE TABLE `go_admin` (
`uid`  tinyint(3) UNSIGNED NOT NULL AUTO_INCREMENT ,
`mid`  tinyint(3) UNSIGNED NOT NULL ,
`username`  char(15) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL ,
`userpass`  char(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL ,
`useremail`  varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`addtime`  int(10) UNSIGNED NULL DEFAULT NULL ,
`logintime`  int(10) UNSIGNED NULL DEFAULT NULL ,
`loginip`  varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
PRIMARY KEY (`uid`),
INDEX `username` (`username`) USING BTREE 
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
COMMENT='管理员表'
AUTO_INCREMENT=29

;

-- ----------------------------
-- Table structure for `go_admin_group`
-- ----------------------------
DROP TABLE IF EXISTS `go_admin_group`;
CREATE TABLE `go_admin_group` (
`mid`  int(3) UNSIGNED NOT NULL AUTO_INCREMENT ,
`name`  varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '组名称' ,
`auth`  varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '权限' ,
`status`  tinyint(1) NOT NULL DEFAULT 1 COMMENT '状态' ,
PRIMARY KEY (`mid`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
COMMENT='管理员列表'
AUTO_INCREMENT=14

;

-- ----------------------------
-- Table structure for `go_app_article`
-- ----------------------------
DROP TABLE IF EXISTS `go_app_article`;
CREATE TABLE `go_app_article` (
`id`  int(10) UNSIGNED NOT NULL AUTO_INCREMENT ,
`title`  varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '标题' ,
`img_path`  varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '图片' ,
`content`  text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '内容描写' ,
`order_num`  mediumint(8) UNSIGNED NOT NULL DEFAULT 1 COMMENT '排序' ,
`is_show`  tinyint(4) NOT NULL DEFAULT 1 COMMENT '是否显示，1为是，0为否' ,
`type`  tinyint(2) NOT NULL DEFAULT 1 ,
`is_new`  tinyint(1) NOT NULL DEFAULT 0 COMMENT '是否最新' ,
`time`  int(10) NOT NULL ,
`description`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '文章摘要' ,
PRIMARY KEY (`id`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
AUTO_INCREMENT=1

;

-- ----------------------------
-- Table structure for `go_app_cart`
-- ----------------------------
DROP TABLE IF EXISTS `go_app_cart`;
CREATE TABLE `go_app_cart` (
`id`  int(11) UNSIGNED NOT NULL AUTO_INCREMENT ,
`shopid`  int(10) NULL DEFAULT NULL COMMENT '商品ID' ,
`qishu`  smallint(6) NULL DEFAULT NULL ,
`title`  varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '商品标题' ,
`thumb`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '图片地址' ,
`zongrenshu`  int(10) NULL DEFAULT NULL COMMENT '总需人数' ,
`canyurenshu`  int(10) NULL DEFAULT NULL ,
`shenyurenshu`  int(10) NULL DEFAULT NULL COMMENT '剩余人数' ,
`yunjiage`  decimal(4,2) NULL DEFAULT NULL ,
`uid`  int(10) NULL DEFAULT NULL COMMENT '用户ID' ,
`cache_id`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '客户端唯一标识(uid和cache_id必填一个)' ,
`gonumber`  int(10) NULL DEFAULT NULL COMMENT '购买人次' ,
PRIMARY KEY (`id`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
AUTO_INCREMENT=835

;

-- ----------------------------
-- Table structure for `go_appimg`
-- ----------------------------
DROP TABLE IF EXISTS `go_appimg`;
CREATE TABLE `go_appimg` (
`id`  mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT ,
`name`  varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '图标名称' ,
`url`  varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '链接地址' ,
`img_path`  varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '图片的路径' ,
`is_show`  tinyint(1) NOT NULL DEFAULT 0 COMMENT '是否启用，1为启用，0为不启用' ,
`order_num`  int(11) NOT NULL DEFAULT 1 COMMENT '排序' ,
`handel`  varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '操作' ,
PRIMARY KEY (`id`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
AUTO_INCREMENT=21

;

-- ----------------------------
-- Table structure for `go_article`
-- ----------------------------
DROP TABLE IF EXISTS `go_article`;
CREATE TABLE `go_article` (
`id`  int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '文章id' ,
`cateid`  char(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '文章父ID' ,
`author`  char(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`title`  char(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '标题' ,
`title_style`  varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`thumb`  varchar(3) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`picarr`  text CHARACTER SET utf8 COLLATE utf8_general_ci NULL ,
`keywords`  varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`description`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`content`  mediumtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '内容' ,
`hit`  int(10) UNSIGNED NULL DEFAULT 0 ,
`order`  tinyint(3) UNSIGNED NULL DEFAULT NULL ,
`posttime`  int(10) UNSIGNED NULL DEFAULT NULL COMMENT '添加时间' ,
`is_my`  tinyint(1) NOT NULL DEFAULT 0 COMMENT '是不是其它推广用文章' ,
PRIMARY KEY (`id`),
INDEX `cateid` (`cateid`) USING BTREE 
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
AUTO_INCREMENT=41

;

-- ----------------------------
-- Table structure for `go_auth_list`
-- ----------------------------
DROP TABLE IF EXISTS `go_auth_list`;
CREATE TABLE `go_auth_list` (
`id`  int(10) UNSIGNED NOT NULL AUTO_INCREMENT ,
`pid`  int(10) NOT NULL COMMENT '父级id' ,
`name`  varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '名称' ,
`code_one`  varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`code_two`  varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
PRIMARY KEY (`id`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
COMMENT='后台的菜单栏目'
AUTO_INCREMENT=11

;

-- ----------------------------
-- Table structure for `go_brand`
-- ----------------------------
DROP TABLE IF EXISTS `go_brand`;
CREATE TABLE `go_brand` (
`id`  int(10) UNSIGNED NOT NULL AUTO_INCREMENT ,
`cateid`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '所属栏目ID' ,
`status`  char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'Y' COMMENT '显示隐藏' ,
`name`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`thumb`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`url`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`order`  int(11) NULL DEFAULT 1 ,
PRIMARY KEY (`id`),
INDEX `status` (`status`) USING BTREE ,
INDEX `order` (`order`) USING BTREE 
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
COMMENT='品牌表'
AUTO_INCREMENT=162

;

-- ----------------------------
-- Table structure for `go_caches`
-- ----------------------------
DROP TABLE IF EXISTS `go_caches`;
CREATE TABLE `go_caches` (
`id`  int(11) UNSIGNED NOT NULL AUTO_INCREMENT ,
`key`  varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL ,
`value`  text CHARACTER SET utf8 COLLATE utf8_general_ci NULL ,
PRIMARY KEY (`id`),
INDEX `key` (`key`) USING BTREE 
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
COMMENT='缓存的消息发送模板'
AUTO_INCREMENT=14

;

-- ----------------------------
-- Table structure for `go_category`
-- ----------------------------
DROP TABLE IF EXISTS `go_category`;
CREATE TABLE `go_category` (
`cateid`  smallint(6) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '栏目id' ,
`parentid`  smallint(6) NULL DEFAULT NULL COMMENT '父ID' ,
`channel`  tinyint(4) NOT NULL DEFAULT 0 ,
`model`  tinyint(1) NULL DEFAULT NULL COMMENT '栏目模型' ,
`name`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '栏目名称' ,
`catdir`  char(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '英文名' ,
`url`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`info`  text CHARACTER SET utf8 COLLATE utf8_general_ci NULL ,
`order`  smallint(6) UNSIGNED NULL DEFAULT 1 COMMENT '排序' ,
PRIMARY KEY (`cateid`),
INDEX `name` (`name`) USING BTREE ,
INDEX `order` (`order`) USING BTREE 
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
COMMENT='栏目表'
AUTO_INCREMENT=21

;

-- ----------------------------
-- Table structure for `go_ceshi_message`
-- ----------------------------
DROP TABLE IF EXISTS `go_ceshi_message`;
CREATE TABLE `go_ceshi_message` (
`id`  int(10) UNSIGNED NOT NULL AUTO_INCREMENT ,
`message`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL ,
`type`  tinyint(1) NOT NULL ,
`time`  date NOT NULL COMMENT '日期' ,
PRIMARY KEY (`id`)
)
ENGINE=MyISAM
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
AUTO_INCREMENT=16

;

-- ----------------------------
-- Table structure for `go_config`
-- ----------------------------
DROP TABLE IF EXISTS `go_config`;
CREATE TABLE `go_config` (
`id`  int(10) UNSIGNED NOT NULL AUTO_INCREMENT ,
`name`  varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL ,
`value`  mediumtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL ,
`zhushi`  text CHARACTER SET utf8 COLLATE utf8_general_ci NULL ,
PRIMARY KEY (`id`),
INDEX `name` (`name`) USING BTREE 
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
AUTO_INCREMENT=26

;

-- ----------------------------
-- Table structure for `go_data_statistics`
-- ----------------------------
DROP TABLE IF EXISTS `go_data_statistics`;
CREATE TABLE `go_data_statistics` (
`id`  int(11) UNSIGNED NOT NULL AUTO_INCREMENT ,
`s_id`  int(10) NULL DEFAULT NULL ,
`s_name`  varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`s_ename`  varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`s_words`  varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`uid`  int(10) NULL DEFAULT NULL ,
`addtime`  int(10) NULL DEFAULT NULL ,
PRIMARY KEY (`id`),
INDEX `s_id` (`s_id`) USING BTREE ,
INDEX `s_words` (`s_words`) USING BTREE ,
INDEX `addtime` (`addtime`) USING BTREE ,
INDEX `s_ename` (`s_ename`) USING BTREE ,
INDEX `uid` (`uid`) USING BTREE 
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
AUTO_INCREMENT=4337

;

-- ----------------------------
-- Table structure for `go_egglotter_award`
-- ----------------------------
DROP TABLE IF EXISTS `go_egglotter_award`;
CREATE TABLE `go_egglotter_award` (
`award_id`  int(11) NOT NULL AUTO_INCREMENT COMMENT 'id' ,
`user_id`  int(11) NULL DEFAULT NULL COMMENT '用户ID' ,
`user_name`  varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户名字' ,
`rule_id`  int(11) NULL DEFAULT NULL COMMENT '活动ID' ,
`subtime`  int(11) NULL DEFAULT NULL COMMENT '中奖时间' ,
`spoil_id`  int(11) NULL DEFAULT NULL COMMENT '奖品等级' ,
PRIMARY KEY (`award_id`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
AUTO_INCREMENT=1

;

-- ----------------------------
-- Table structure for `go_egglotter_rule`
-- ----------------------------
DROP TABLE IF EXISTS `go_egglotter_rule`;
CREATE TABLE `go_egglotter_rule` (
`rule_id`  int(11) NOT NULL AUTO_INCREMENT ,
`rule_name`  varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`starttime`  int(11) NULL DEFAULT NULL COMMENT '活动开始时间' ,
`endtime`  int(11) NULL DEFAULT NULL COMMENT '活动结束时间' ,
`subtime`  int(11) NULL DEFAULT NULL COMMENT '活动编辑时间' ,
`lotterytype`  int(11) NULL DEFAULT NULL COMMENT '抽奖按币分类' ,
`lotterjb`  int(11) NULL DEFAULT NULL COMMENT '每一次抽奖使用的金币' ,
`ruledesc`  text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '规则介绍' ,
`startusing`  tinyint(4) NULL DEFAULT NULL COMMENT '启用' ,
PRIMARY KEY (`rule_id`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
AUTO_INCREMENT=1

;

-- ----------------------------
-- Table structure for `go_egglotter_spoil`
-- ----------------------------
DROP TABLE IF EXISTS `go_egglotter_spoil`;
CREATE TABLE `go_egglotter_spoil` (
`spoil_id`  int(11) NOT NULL AUTO_INCREMENT ,
`rule_id`  int(11) NULL DEFAULT NULL ,
`spoil_name`  text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '名称' ,
`spoil_jl`  int(11) NULL DEFAULT NULL COMMENT '机率' ,
`spoil_dj`  int(11) NULL DEFAULT NULL ,
`urlimg`  varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`subtime`  int(11) NULL DEFAULT NULL COMMENT '提交时间' ,
PRIMARY KEY (`spoil_id`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
AUTO_INCREMENT=1

;

-- ----------------------------
-- Table structure for `go_fund`
-- ----------------------------
DROP TABLE IF EXISTS `go_fund`;
CREATE TABLE `go_fund` (
`id`  int(10) UNSIGNED NOT NULL ,
`fund_off`  tinyint(4) UNSIGNED NOT NULL DEFAULT 1 ,
`fund_money`  decimal(10,2) UNSIGNED NOT NULL ,
`fund_count_money`  decimal(12,2) NULL DEFAULT NULL COMMENT '云购基金' ,
PRIMARY KEY (`id`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci

;

-- ----------------------------
-- Table structure for `go_goodlist`
-- ----------------------------
DROP TABLE IF EXISTS `go_goodlist`;
CREATE TABLE `go_goodlist` (
`id`  int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '商品id' ,
`fid`  int(10) UNSIGNED NOT NULL COMMENT '同一个商品' ,
`cateid`  smallint(6) UNSIGNED NOT NULL COMMENT '所属栏目ID' ,
`brandid`  smallint(6) UNSIGNED NOT NULL COMMENT '所属品牌ID' ,
`title`  varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '商品标题' ,
`title_style`  varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL ,
`title2`  varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '副标题' ,
`keywords`  varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL ,
`description`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL ,
`money`  decimal(10,2) NOT NULL DEFAULT 0.00 COMMENT '金额' ,
`yunjiage`  decimal(4,2) UNSIGNED NOT NULL DEFAULT 1.00 COMMENT '云购人次价格' ,
`zongrenshu`  int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '总需人数' ,
`canyurenshu`  int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '已参与人数' ,
`shenyurenshu`  int(10) UNSIGNED NOT NULL ,
`def_renshu`  int(10) UNSIGNED NOT NULL DEFAULT 0 ,
`new_qishu`  smallint(6) UNSIGNED NOT NULL DEFAULT 0 COMMENT '最新期数' ,
`maxqishu`  smallint(5) UNSIGNED NOT NULL DEFAULT 1 COMMENT ' 最大期数' ,
`thumb`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL ,
`picarr`  text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '商品图片' ,
`content`  mediumtext CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '商品内容详情' ,
`codes_table`  char(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL ,
`xsjx_time`  int(10) UNSIGNED NOT NULL ,
`pos`  tinyint(4) UNSIGNED NOT NULL COMMENT '是否推荐' ,
`renqi`  tinyint(4) UNSIGNED NOT NULL DEFAULT 0 COMMENT '是否人气商品0否1是' ,
`default_renci`  tinyint(4) NOT NULL DEFAULT 1 COMMENT '默认购买人次' ,
`first_time`  int(10) UNSIGNED NOT NULL COMMENT '时间' ,
`order`  int(5) UNSIGNED NOT NULL DEFAULT 1 ,
`is_shi`  tinyint(1) NOT NULL DEFAULT 0 ,
`is_new`  tinyint(1) NOT NULL DEFAULT 0 COMMENT '是否是最新  1是 0否' ,
`is_apple`  tinyint(1) NOT NULL DEFAULT 0 COMMENT '是否是苹果相关产品' ,
`new_version`  int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '同一产品的最新一版本信息' ,
PRIMARY KEY (`id`)
)
ENGINE=MyISAM
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
AUTO_INCREMENT=8

;

-- ----------------------------
-- Table structure for `go_goodlist_li`
-- ----------------------------
DROP TABLE IF EXISTS `go_goodlist_li`;
CREATE TABLE `go_goodlist_li` (
`id`  int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '商品id' ,
`sid`  int(10) UNSIGNED NOT NULL COMMENT '同一个商品' ,
`canyurenshu`  int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '已参与人数' ,
`zongrenshu`  int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '所需总参与人数' ,
`qishu`  smallint(6) UNSIGNED NULL DEFAULT 0 COMMENT '期数' ,
`time`  int(10) UNSIGNED NULL DEFAULT NULL COMMENT '时间' ,
`q_uid`  int(10) UNSIGNED NULL DEFAULT NULL COMMENT '中奖人ID' ,
`q_user_code`  char(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '中奖码' ,
`q_ssccode`  char(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '时时彩中奖码' ,
`q_sscopen`  varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`q_sscphase`  varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`q_showtime`  char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'N' COMMENT 'Y/N揭晓动画' ,
`q_counttime`  char(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '总时间相加' ,
`q_content`  mediumtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '揭晓内容' ,
`q_end_time`  char(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '揭晓时间' ,
`q_djstime`  char(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '倒计时' ,
`zdrange`  varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT 's指定range' ,
PRIMARY KEY (`id`)
)
ENGINE=MyISAM
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
AUTO_INCREMENT=3

;

-- ----------------------------
-- Table structure for `go_goods`
-- ----------------------------
DROP TABLE IF EXISTS `go_goods`;
CREATE TABLE `go_goods` (
`id`  int(10) UNSIGNED NOT NULL AUTO_INCREMENT ,
`fid`  int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '父id' ,
`title`  varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主标题' ,
`title2`  varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '副标题' ,
`title_style`  varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '副标题颜色' ,
`brandid`  smallint(6) UNSIGNED NOT NULL COMMENT '品牌ID' ,
`cateid`  smallint(6) UNSIGNED NOT NULL COMMENT '栏目ID' ,
`keywords`  varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '关键词' ,
`description`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '商品详情描述' ,
`money`  decimal(10,2) UNSIGNED NOT NULL ,
`yunjiage`  decimal(4,2) UNSIGNED NOT NULL DEFAULT 1.00 ,
`zongrenshu`  int(10) UNSIGNED NOT NULL COMMENT '总人数' ,
`def_renshu`  int(10) NOT NULL DEFAULT 0 ,
`maxqishu`  smallint(5) UNSIGNED NOT NULL DEFAULT 1 COMMENT '最大期数' ,
`thumb`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`picarr`  text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '商品图片' ,
`content`  mediumtext CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '商品内容详情' ,
`code_table`  char(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`xsjx_time`  int(10) UNSIGNED NULL DEFAULT NULL ,
`pos`  tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '是否为推荐产品' ,
`renqi`  tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '是否为推荐产品' ,
`default_renci`  tinyint(4) UNSIGNED NOT NULL DEFAULT 0 COMMENT '默认购买人次' ,
`first_time`  int(10) UNSIGNED NOT NULL COMMENT '添加首件商品的时间' ,
`is_shi`  tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '是否是限购产品' ,
`is_apple`  tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '是否为苹果产品' ,
`is_new`  tinyint(1) UNSIGNED NOT NULL DEFAULT 1 COMMENT '是否为最新产品' ,
`order`  tinyint(5) UNSIGNED NOT NULL DEFAULT 100 COMMENT '值越大，排序越靠前' ,
PRIMARY KEY (`id`, `fid`)
)
ENGINE=MyISAM
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
AUTO_INCREMENT=1

;

-- ----------------------------
-- Table structure for `go_link`
-- ----------------------------
DROP TABLE IF EXISTS `go_link`;
CREATE TABLE `go_link` (
`id`  int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '友情链接ID' ,
`type`  tinyint(1) UNSIGNED NOT NULL COMMENT '链接类型' ,
`name`  char(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '名称' ,
`logo`  varchar(250) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '图片' ,
`url`  varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '地址' ,
PRIMARY KEY (`id`),
INDEX `type` (`type`) USING BTREE 
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
AUTO_INCREMENT=1

;

-- ----------------------------
-- Table structure for `go_member`
-- ----------------------------
DROP TABLE IF EXISTS `go_member`;
CREATE TABLE `go_member` (
`uid`  int(10) UNSIGNED NOT NULL AUTO_INCREMENT ,
`username`  char(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户名' ,
`email`  varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户邮箱' ,
`mobile`  char(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户手机' ,
`password`  char(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '密码' ,
`user_ip`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`img`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户头像' ,
`qianming`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户签名' ,
`groupid`  tinyint(4) UNSIGNED NULL DEFAULT 0 COMMENT '用户权限组' ,
`addgroup`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户加入的圈子组1|2|3' ,
`money`  decimal(10,2) UNSIGNED NULL DEFAULT 0.00 COMMENT '账户金额' ,
`emailcode`  char(21) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '-1' COMMENT '邮箱认证码' ,
`mobilecode`  char(21) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '-1' COMMENT '手机认证码' ,
`passcode`  char(21) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '-1' COMMENT '找会密码认证码-1,1,码' ,
`reg_key`  varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '注册参数' ,
`score`  int(10) UNSIGNED NOT NULL DEFAULT 0 ,
`jingyan`  int(10) UNSIGNED NULL DEFAULT 0 ,
`yaoqing`  int(10) UNSIGNED NULL DEFAULT NULL COMMENT '邀请人' ,
`qq_id`  int(10) NOT NULL DEFAULT 0 COMMENT 'QQ绑定ID' ,
`wx_id`  int(10) NOT NULL DEFAULT 0 COMMENT '绑定的微信ID' ,
`band`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '准备弃用' ,
`band_id`  int(10) NOT NULL DEFAULT 0 COMMENT 'QQ登录的id   弃用' ,
`time`  int(10) UNSIGNED NULL DEFAULT 0 ,
`login_time`  int(10) UNSIGNED NULL DEFAULT 0 ,
`reg_plat`  varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '注册平台  如:android/ios/web/wap' ,
`auto_user`  tinyint(1) NULL DEFAULT 0 ,
`get_yaoqing_money`  tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '是否得到邀请好友资金  0  1 ' ,
PRIMARY KEY (`uid`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
COMMENT='会员表'
AUTO_INCREMENT=14380

;

-- ----------------------------
-- Table structure for `go_member_account`
-- ----------------------------
DROP TABLE IF EXISTS `go_member_account`;
CREATE TABLE `go_member_account` (
`uid`  int(10) UNSIGNED NOT NULL COMMENT '用户id' ,
`type`  tinyint(1) NULL DEFAULT NULL COMMENT '充值1/消费-1' ,
`pay`  char(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`content`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '详情' ,
`money`  mediumint(8) NOT NULL DEFAULT 0 COMMENT '金额' ,
`time`  char(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL ,
INDEX `uid` (`uid`) USING BTREE ,
INDEX `type` (`type`) USING BTREE 
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
COMMENT='会员账户明细'

;

-- ----------------------------
-- Table structure for `go_member_addmoney_record`
-- ----------------------------
DROP TABLE IF EXISTS `go_member_addmoney_record`;
CREATE TABLE `go_member_addmoney_record` (
`id`  int(10) UNSIGNED NOT NULL AUTO_INCREMENT ,
`uid`  int(10) UNSIGNED NOT NULL ,
`code`  char(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL ,
`money`  decimal(10,2) UNSIGNED NOT NULL ,
`pay_type`  char(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL ,
`status`  char(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL ,
`time`  int(10) NOT NULL ,
`score`  int(10) UNSIGNED NULL DEFAULT NULL ,
`scookies`  text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '购物车cookie' ,
`transid`  varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '交易流水号(爱贝对账用)' ,
`transtime`  varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '交易时间(爱贝对账用)' ,
`paytype`  int(4) NULL DEFAULT NULL COMMENT '(爱贝对账用)' ,
`result`  int(1) NULL DEFAULT NULL COMMENT '(爱贝对账用)' ,
`plat_type`  int(1) NULL DEFAULT 0 COMMENT '支付平台  0:网站  1:爱贝' ,
PRIMARY KEY (`id`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
AUTO_INCREMENT=1902

;

-- ----------------------------
-- Table structure for `go_member_band`
-- ----------------------------
DROP TABLE IF EXISTS `go_member_band`;
CREATE TABLE `go_member_band` (
`b_id`  int(10) UNSIGNED NOT NULL AUTO_INCREMENT ,
`b_uid`  int(10) NOT NULL DEFAULT 0 COMMENT '用户ID' ,
`b_type`  char(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '绑定登陆类型' ,
`b_code`  varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '返回数据1    qq openid' ,
`b_data`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '该账号的信息数据' ,
`b_time`  int(10) NULL DEFAULT NULL ,
PRIMARY KEY (`b_id`),
INDEX `b_uid` (`b_uid`) USING BTREE 
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
AUTO_INCREMENT=1260

;

-- ----------------------------
-- Table structure for `go_member_cashout`
-- ----------------------------
DROP TABLE IF EXISTS `go_member_cashout`;
CREATE TABLE `go_member_cashout` (
`id`  int(11) UNSIGNED NOT NULL AUTO_INCREMENT ,
`uid`  int(10) UNSIGNED NOT NULL COMMENT '用户id' ,
`username`  varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '开户人' ,
`bankname`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '银行名称' ,
`branch`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '支行' ,
`money`  decimal(8,0) NOT NULL DEFAULT 0 COMMENT '申请提现金额' ,
`time`  char(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '申请时间' ,
`banknumber`  varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '银行帐号' ,
`linkphone`  varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '联系电话' ,
`auditstatus`  tinyint(4) NOT NULL COMMENT '1审核通过' ,
`procefees`  decimal(8,2) NOT NULL COMMENT '手续费' ,
`reviewtime`  char(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '审核通过时间' ,
PRIMARY KEY (`id`),
INDEX `uid` (`uid`) USING BTREE ,
INDEX `type` (`username`) USING BTREE 
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
COMMENT='会员账户明细'
AUTO_INCREMENT=1

;

-- ----------------------------
-- Table structure for `go_member_del`
-- ----------------------------
DROP TABLE IF EXISTS `go_member_del`;
CREATE TABLE `go_member_del` (
`uid`  int(10) UNSIGNED NOT NULL AUTO_INCREMENT ,
`username`  char(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户名' ,
`email`  varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户邮箱' ,
`mobile`  char(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户手机' ,
`password`  char(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '密码' ,
`user_ip`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`img`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户头像' ,
`qianming`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户签名' ,
`groupid`  tinyint(4) UNSIGNED NULL DEFAULT 0 COMMENT '用户权限组' ,
`addgroup`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户加入的圈子组1|2|3' ,
`money`  decimal(10,2) UNSIGNED NULL DEFAULT 0.00 COMMENT '账户金额' ,
`emailcode`  char(21) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '-1' COMMENT '邮箱认证码' ,
`mobilecode`  char(21) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '-1' COMMENT '手机认证码' ,
`passcode`  char(21) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '-1' COMMENT '找会密码认证码-1,1,码' ,
`reg_key`  varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '注册参数' ,
`score`  int(10) UNSIGNED NOT NULL DEFAULT 0 ,
`jingyan`  int(10) UNSIGNED NULL DEFAULT 0 ,
`yaoqing`  int(10) UNSIGNED NULL DEFAULT NULL COMMENT '邀请人' ,
`qq_id`  int(10) NOT NULL DEFAULT 0 COMMENT 'QQ绑定ID' ,
`wx_id`  int(10) NOT NULL DEFAULT 0 COMMENT '绑定的微信ID' ,
`band`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '准备弃用' ,
`band_id`  int(10) NOT NULL DEFAULT 0 COMMENT 'QQ登录的id   弃用' ,
`time`  int(10) UNSIGNED NULL DEFAULT 0 ,
`login_time`  int(10) UNSIGNED NULL DEFAULT 0 ,
`reg_plat`  varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '注册平台  如:android/ios/web/wap' ,
`auto_user`  tinyint(1) NULL DEFAULT 0 ,
`get_yaoqing_money`  tinyint(1) NOT NULL DEFAULT 0 ,
PRIMARY KEY (`uid`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
COMMENT='会员表'
AUTO_INCREMENT=14342

;

-- ----------------------------
-- Table structure for `go_member_dizhi`
-- ----------------------------
DROP TABLE IF EXISTS `go_member_dizhi`;
CREATE TABLE `go_member_dizhi` (
`id`  int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'id' ,
`uid`  int(10) NOT NULL COMMENT '用户id' ,
`sheng`  varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '省' ,
`shi`  varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '市' ,
`xian`  varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '县' ,
`jiedao`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '街道地址' ,
`youbian`  mediumint(8) NULL DEFAULT NULL COMMENT '邮编' ,
`shouhuoren`  varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '收货人' ,
`mobile`  char(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '手机' ,
`qq`  char(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'QQ' ,
`tell`  varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '座机号' ,
`default`  char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'N' COMMENT '是否默认' ,
`time`  int(10) UNSIGNED NOT NULL ,
PRIMARY KEY (`id`),
INDEX `uid` (`uid`) USING BTREE 
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
COMMENT='会员地址表(收货地址)'
AUTO_INCREMENT=1221

;

-- ----------------------------
-- Table structure for `go_member_friendship`
-- ----------------------------
DROP TABLE IF EXISTS `go_member_friendship`;
CREATE TABLE `go_member_friendship` (
`id`  int(11) UNSIGNED NOT NULL AUTO_INCREMENT ,
`uid`  int(10) NOT NULL COMMENT '被邀请者' ,
`yaoqing`  int(10) NULL DEFAULT NULL COMMENT '邀请者' ,
`lev`  tinyint(1) NOT NULL DEFAULT 1 COMMENT '默认是第一级好友  总共3级' ,
`addtime`  int(10) NULL DEFAULT NULL COMMENT '添加时间' ,
PRIMARY KEY (`id`),
INDEX `yaoqing` (`yaoqing`) USING BTREE ,
INDEX `lev` (`lev`) USING BTREE 
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
AUTO_INCREMENT=1

;

-- ----------------------------
-- Table structure for `go_member_go_record`
-- ----------------------------
DROP TABLE IF EXISTS `go_member_go_record`;
CREATE TABLE `go_member_go_record` (
`id`  int(10) UNSIGNED NOT NULL AUTO_INCREMENT ,
`code`  char(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '订单号' ,
`code_tmp`  tinyint(3) UNSIGNED NULL DEFAULT NULL COMMENT '相同订单' ,
`username`  varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL ,
`uphoto`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`uid`  int(10) UNSIGNED NOT NULL COMMENT '会员id' ,
`shopid`  int(6) UNSIGNED NOT NULL COMMENT '商品id' ,
`shopname`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '商品名' ,
`shopqishu`  smallint(6) NOT NULL DEFAULT 0 COMMENT '期数' ,
`gonumber`  smallint(5) UNSIGNED NULL DEFAULT NULL COMMENT '购买次数' ,
`goucode`  longtext CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '云购码' ,
`moneycount`  decimal(10,2) NOT NULL ,
`huode`  char(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '0' COMMENT '中奖码' ,
`pay_type`  char(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '付款方式' ,
`ip`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`status`  char(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '订单状态' ,
`company_money`  decimal(10,2) UNSIGNED NOT NULL DEFAULT 0.00 ,
`company_code`  char(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`company`  char(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`company_time`  int(10) NULL DEFAULT NULL COMMENT '发货时间' ,
`confirm_time`  int(10) NULL DEFAULT NULL COMMENT '确认收货时间' ,
`confrim_addr`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '确认后的收货信息（收货人、电话、地址）' ,
`confrim_addr_time`  int(10) NULL DEFAULT NULL COMMENT '确认收货地址时间' ,
`confrim_addr_uinfo`  varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '收货人姓名和电话 格式: 张三|13788888888' ,
`time`  char(21) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '购买时间' ,
`cz_code`  char(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '充值订单号' ,
`change_user`  tinyint(1) NOT NULL DEFAULT 0 COMMENT '是否是发货商改动   0否  1是' ,
PRIMARY KEY (`id`),
INDEX `uid` (`uid`) USING BTREE ,
INDEX `shopid` (`shopid`) USING BTREE ,
INDEX `time` (`time`) USING BTREE 
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
COMMENT='云购记录表'
AUTO_INCREMENT=68978

;

-- ----------------------------
-- Table structure for `go_member_group`
-- ----------------------------
DROP TABLE IF EXISTS `go_member_group`;
CREATE TABLE `go_member_group` (
`groupid`  tinyint(4) UNSIGNED NOT NULL AUTO_INCREMENT ,
`name`  char(15) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '会员组名' ,
`jingyan_start`  int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '需要的经验值' ,
`jingyan_end`  int(10) NOT NULL ,
`icon`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '图标' ,
`type`  char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'N' COMMENT '是否是系统组' ,
PRIMARY KEY (`groupid`),
INDEX `jingyan` (`jingyan_start`) USING BTREE 
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
COMMENT='会员权限组'
AUTO_INCREMENT=7

;

-- ----------------------------
-- Table structure for `go_member_jifen`
-- ----------------------------
DROP TABLE IF EXISTS `go_member_jifen`;
CREATE TABLE `go_member_jifen` (
`id`  int(11) UNSIGNED NOT NULL AUTO_INCREMENT ,
`uid`  int(10) NOT NULL COMMENT '充值者' ,
`yaoqing`  int(10) NOT NULL COMMENT '获得积分的人' ,
`lev`  tinyint(1) NOT NULL COMMENT '级数' ,
`jifen`  decimal(10,2) NULL DEFAULT NULL COMMENT '获得的积分' ,
`record_id`  int(10) NOT NULL COMMENT '充值记录的ID' ,
`addtime`  int(10) NULL DEFAULT NULL COMMENT '获得积分的时间' ,
PRIMARY KEY (`id`),
INDEX `yaoqing` (`yaoqing`) USING BTREE ,
INDEX `lev` (`lev`) USING BTREE 
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
AUTO_INCREMENT=1

;

-- ----------------------------
-- Table structure for `go_member_message`
-- ----------------------------
DROP TABLE IF EXISTS `go_member_message`;
CREATE TABLE `go_member_message` (
`id`  int(10) UNSIGNED NOT NULL AUTO_INCREMENT ,
`uid`  int(10) UNSIGNED NOT NULL COMMENT '用户id' ,
`type`  tinyint(1) NULL DEFAULT 0 COMMENT '消息来源,0系统,1私信' ,
`sendid`  int(10) UNSIGNED NULL DEFAULT 0 COMMENT '发送人ID' ,
`sendname`  char(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '发送人名' ,
`content`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '发送内容' ,
`time`  int(10) NULL DEFAULT NULL ,
PRIMARY KEY (`id`),
INDEX `uid` (`uid`) USING BTREE 
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
COMMENT='会员消息表'
AUTO_INCREMENT=1

;

-- ----------------------------
-- Table structure for `go_member_recodes`
-- ----------------------------
DROP TABLE IF EXISTS `go_member_recodes`;
CREATE TABLE `go_member_recodes` (
`id`  int(10) UNSIGNED NOT NULL AUTO_INCREMENT ,
`uid`  int(10) UNSIGNED NOT NULL COMMENT '用户id' ,
`type`  tinyint(1) NOT NULL COMMENT '收取1//充值-2/提现-3' ,
`content`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '详情' ,
`shopid`  int(11) NULL DEFAULT NULL COMMENT '商品id' ,
`money`  decimal(8,2) NOT NULL DEFAULT 0.00 COMMENT '佣金' ,
`time`  char(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL ,
`ygmoney`  decimal(8,2) NOT NULL COMMENT '云购金额' ,
`cashoutid`  int(11) NULL DEFAULT NULL COMMENT '申请提现记录表id' ,
PRIMARY KEY (`id`),
INDEX `uid` (`uid`) USING BTREE ,
INDEX `type` (`type`) USING BTREE 
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
COMMENT='会员账户明细'
AUTO_INCREMENT=2

;

-- ----------------------------
-- Table structure for `go_mobile_code`
-- ----------------------------
DROP TABLE IF EXISTS `go_mobile_code`;
CREATE TABLE `go_mobile_code` (
`id`  int(11) UNSIGNED NOT NULL AUTO_INCREMENT ,
`mobile`  char(11) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '手机号码' ,
`mobilecode`  char(6) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '验证码' ,
`type`  tinyint(1) NOT NULL DEFAULT 0 COMMENT '验证码的类型  2是验证   3是注册' ,
`time`  int(10) NOT NULL ,
`is_reg`  tinyint(1) NOT NULL DEFAULT 0 COMMENT '如果使用了该验证码注册，就为1' ,
`is_found`  tinyint(1) NOT NULL DEFAULT 0 COMMENT '如果使用了该验证码找回密码，就为1' ,
`is_mod`  tinyint(1) NOT NULL DEFAULT 0 COMMENT '如果使用了该验证码更换手机号，就为1' ,
PRIMARY KEY (`id`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
COMMENT='用户存储发送给用户的验证码,可以添加一个失效时间,初始状态是3个0,,有任何一个发生改变,'
AUTO_INCREMENT=18766

;

-- ----------------------------
-- Table structure for `go_model`
-- ----------------------------
DROP TABLE IF EXISTS `go_model`;
CREATE TABLE `go_model` (
`modelid`  smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT ,
`name`  char(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL ,
`table`  char(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL ,
PRIMARY KEY (`modelid`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
COMMENT='模型表'
AUTO_INCREMENT=3

;

-- ----------------------------
-- Table structure for `go_navigation`
-- ----------------------------
DROP TABLE IF EXISTS `go_navigation`;
CREATE TABLE `go_navigation` (
`cid`  smallint(6) UNSIGNED NOT NULL AUTO_INCREMENT ,
`parentid`  smallint(6) UNSIGNED NULL DEFAULT NULL ,
`name`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`type`  char(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`url`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`status`  char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'Y' COMMENT '显示/隐藏' ,
`order`  smallint(6) UNSIGNED NULL DEFAULT 1 ,
PRIMARY KEY (`cid`),
INDEX `status` (`status`) USING BTREE ,
INDEX `order` (`order`) USING BTREE ,
INDEX `type` (`type`) USING BTREE 
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
AUTO_INCREMENT=18

;

-- ----------------------------
-- Table structure for `go_newest_jackpot`
-- ----------------------------
DROP TABLE IF EXISTS `go_newest_jackpot`;
CREATE TABLE `go_newest_jackpot` (
`id`  int(10) UNSIGNED NOT NULL AUTO_INCREMENT ,
`uid`  int(10) NOT NULL COMMENT '用户ID' ,
`sid`  int(10) NOT NULL COMMENT '中奖商品' ,
`time`  datetime NOT NULL COMMENT '时间' ,
PRIMARY KEY (`id`)
)
ENGINE=MyISAM
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
AUTO_INCREMENT=549

;

-- ----------------------------
-- Table structure for `go_pay`
-- ----------------------------
DROP TABLE IF EXISTS `go_pay`;
CREATE TABLE `go_pay` (
`pay_id`  int(11) UNSIGNED NOT NULL AUTO_INCREMENT ,
`pay_name`  char(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL ,
`pay_class`  char(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL ,
`pay_type`  tinyint(3) NOT NULL ,
`pay_thumb`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`pay_des`  text CHARACTER SET utf8 COLLATE utf8_general_ci NULL ,
`pay_start`  tinyint(4) NOT NULL ,
`pay_key`  text CHARACTER SET utf8 COLLATE utf8_general_ci NULL ,
`wap`  tinyint(1) NOT NULL DEFAULT 1 COMMENT '是否在H5开启本支付' ,
`app`  tinyint(1) NOT NULL DEFAULT 1 COMMENT '是否在APP开启本支付' ,
`web`  tinyint(1) NOT NULL DEFAULT 1 COMMENT '是否在网站开启本支付' ,
PRIMARY KEY (`pay_id`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
AUTO_INCREMENT=7

;

-- ----------------------------
-- Table structure for `go_position`
-- ----------------------------
DROP TABLE IF EXISTS `go_position`;
CREATE TABLE `go_position` (
`pos_id`  int(11) UNSIGNED NOT NULL AUTO_INCREMENT ,
`pos_model`  tinyint(3) UNSIGNED NOT NULL ,
`pos_name`  varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL ,
`pos_num`  tinyint(3) UNSIGNED NOT NULL ,
`pos_maxnum`  tinyint(3) UNSIGNED NOT NULL ,
`pos_this_num`  tinyint(3) UNSIGNED NOT NULL DEFAULT 0 ,
`pos_time`  int(10) UNSIGNED NOT NULL ,
PRIMARY KEY (`pos_id`),
INDEX `pos_id` (`pos_id`) USING BTREE ,
INDEX `pos_model` (`pos_model`) USING BTREE 
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
AUTO_INCREMENT=1

;

-- ----------------------------
-- Table structure for `go_position_data`
-- ----------------------------
DROP TABLE IF EXISTS `go_position_data`;
CREATE TABLE `go_position_data` (
`id`  int(10) UNSIGNED NOT NULL AUTO_INCREMENT ,
`con_id`  int(10) UNSIGNED NOT NULL ,
`mod_id`  tinyint(3) UNSIGNED NOT NULL ,
`mod_name`  char(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL ,
`pos_id`  int(10) UNSIGNED NOT NULL ,
`pos_data`  mediumtext CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL ,
`pos_order`  int(10) UNSIGNED NOT NULL DEFAULT 1 ,
`pos_time`  int(10) UNSIGNED NOT NULL ,
PRIMARY KEY (`id`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
AUTO_INCREMENT=1

;

-- ----------------------------
-- Table structure for `go_pv`
-- ----------------------------
DROP TABLE IF EXISTS `go_pv`;
CREATE TABLE `go_pv` (
`id`  int(10) UNSIGNED NOT NULL AUTO_INCREMENT ,
`page`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '访问的页面' ,
`source`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '上一链接(有则写入，无则空）' ,
`ip`  varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'ip地址' ,
`su`  varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '渠道名称' ,
`sk`  varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '渠道关键词' ,
`type`  tinyint(1) NOT NULL DEFAULT 0 COMMENT '0:来自web网站    1：来自手机网站' ,
`time`  int(10) NOT NULL COMMENT '访问时间' ,
PRIMARY KEY (`id`),
INDEX `su` (`su`) USING BTREE ,
INDEX `time` (`time`) USING BTREE 
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
AUTO_INCREMENT=626581

;

-- ----------------------------
-- Table structure for `go_qqset`
-- ----------------------------
DROP TABLE IF EXISTS `go_qqset`;
CREATE TABLE `go_qqset` (
`id`  int(11) NOT NULL AUTO_INCREMENT ,
`qq`  varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`name`  varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`type`  varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`province`  varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`city`  varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`county`  varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`qqurl`  varchar(250) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`full`  varchar(6) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '是否已满' ,
`subtime`  int(11) NULL DEFAULT NULL ,
PRIMARY KEY (`id`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
AUTO_INCREMENT=1

;

-- ----------------------------
-- Table structure for `go_quanzi`
-- ----------------------------
DROP TABLE IF EXISTS `go_quanzi`;
CREATE TABLE `go_quanzi` (
`id`  tinyint(3) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID' ,
`title`  char(15) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '标题' ,
`img`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '图片地址' ,
`chengyuan`  mediumint(8) UNSIGNED NULL DEFAULT 0 COMMENT '成员数' ,
`tiezi`  mediumint(8) UNSIGNED NULL DEFAULT 0 COMMENT '帖子数' ,
`guanli`  mediumint(8) UNSIGNED NOT NULL COMMENT '管理员' ,
`jinhua`  smallint(5) UNSIGNED NULL DEFAULT NULL COMMENT '精华帖' ,
`jianjie`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '暂无介绍' COMMENT '简介' ,
`gongao`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '暂无' COMMENT '公告' ,
`jiaru`  char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'Y' COMMENT '申请加入' ,
`glfatie`  char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'N' COMMENT '发帖权限' ,
`shenhe`  char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'N' ,
`huifu`  char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'Y' ,
`time`  int(11) NOT NULL COMMENT '时间' ,
PRIMARY KEY (`id`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
AUTO_INCREMENT=1

;

-- ----------------------------
-- Table structure for `go_quanzi_hueifu`
-- ----------------------------
DROP TABLE IF EXISTS `go_quanzi_hueifu`;
CREATE TABLE `go_quanzi_hueifu` (
`id`  int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID' ,
`tzid`  int(11) NULL DEFAULT NULL COMMENT '帖子ID匹配' ,
`hueifu`  text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '回复内容' ,
`hueiyuan`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '会员' ,
`hftime`  int(11) NULL DEFAULT NULL COMMENT '时间' ,
PRIMARY KEY (`id`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
AUTO_INCREMENT=1

;

-- ----------------------------
-- Table structure for `go_quanzi_tiezi`
-- ----------------------------
DROP TABLE IF EXISTS `go_quanzi_tiezi`;
CREATE TABLE `go_quanzi_tiezi` (
`id`  int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID' ,
`qzid`  int(10) UNSIGNED NULL DEFAULT NULL COMMENT '圈子ID匹配' ,
`tiezi`  int(10) UNSIGNED NOT NULL DEFAULT 0 ,
`hueiyuan`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '会员信息' ,
`title`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '标题' ,
`neirong`  text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '内容' ,
`hueifu`  smallint(5) UNSIGNED NOT NULL DEFAULT 0 COMMENT '回复' ,
`dianji`  int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '点击量' ,
`zhiding`  char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'N' COMMENT '置顶' ,
`jinghua`  char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'N' COMMENT '精华' ,
`zuihou`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '最后回复' ,
`shenhe`  char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'Y' COMMENT '是否通过' ,
`time`  int(10) UNSIGNED NULL DEFAULT NULL COMMENT '时间' ,
PRIMARY KEY (`id`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
AUTO_INCREMENT=1

;

-- ----------------------------
-- Table structure for `go_recharge_percentage`
-- ----------------------------
DROP TABLE IF EXISTS `go_recharge_percentage`;
CREATE TABLE `go_recharge_percentage` (
`id`  int(10) NOT NULL AUTO_INCREMENT ,
`charge`  int(50) NULL DEFAULT NULL ,
`percentage`  int(50) NULL DEFAULT NULL ,
PRIMARY KEY (`id`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
AUTO_INCREMENT=6

;

-- ----------------------------
-- Table structure for `go_recom`
-- ----------------------------
DROP TABLE IF EXISTS `go_recom`;
CREATE TABLE `go_recom` (
`id`  int(10) NOT NULL AUTO_INCREMENT COMMENT '推荐位id' ,
`img`  varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '推荐位图片' ,
`title`  varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '推荐位标题' ,
`link`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
PRIMARY KEY (`id`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
AUTO_INCREMENT=1

;

-- ----------------------------
-- Table structure for `go_send`
-- ----------------------------
DROP TABLE IF EXISTS `go_send`;
CREATE TABLE `go_send` (
`id`  int(10) UNSIGNED NOT NULL AUTO_INCREMENT ,
`uid`  int(10) UNSIGNED NOT NULL ,
`gid`  int(10) UNSIGNED NOT NULL ,
`username`  varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL ,
`shoptitle`  varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL ,
`send_type`  tinyint(4) NOT NULL ,
`send_time`  int(10) UNSIGNED NOT NULL ,
PRIMARY KEY (`id`),
INDEX `uid` (`uid`) USING BTREE ,
INDEX `gid` (`gid`) USING BTREE ,
INDEX `send_type` (`send_type`) USING BTREE 
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
AUTO_INCREMENT=168

;

-- ----------------------------
-- Table structure for `go_shaidan`
-- ----------------------------
DROP TABLE IF EXISTS `go_shaidan`;
CREATE TABLE `go_shaidan` (
`sd_id`  int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '晒单id' ,
`sd_userid`  int(10) UNSIGNED NULL DEFAULT NULL COMMENT '用户ID' ,
`sd_shopid`  int(10) UNSIGNED NULL DEFAULT NULL COMMENT '商品ID' ,
`sd_shopsid`  int(10) UNSIGNED NULL DEFAULT NULL ,
`sd_qishu`  int(10) UNSIGNED NULL DEFAULT NULL ,
`sd_ip`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`sd_title`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '晒单标题' ,
`sd_thumbs`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '缩略图' ,
`sd_content`  text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '晒单内容' ,
`sd_photolist`  text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '晒单图片' ,
`sd_zhan`  int(10) UNSIGNED NULL DEFAULT 0 COMMENT '点赞' ,
`sd_ping`  int(10) UNSIGNED NULL DEFAULT 0 COMMENT '评论' ,
`sd_time`  int(10) UNSIGNED NULL DEFAULT NULL COMMENT '晒单时间' ,
`is_audit`  tinyint(1) NOT NULL DEFAULT 0 COMMENT '是否审核通过，  0待审核，  1审核失败， 2审核通过；  默认审核通过' ,
`audit_info`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '审核信息' ,
PRIMARY KEY (`sd_id`),
INDEX `sd_userid` (`sd_userid`) USING BTREE ,
INDEX `sd_shopid` (`sd_shopid`) USING BTREE ,
INDEX `sd_zhan` (`sd_zhan`) USING BTREE ,
INDEX `sd_ping` (`sd_ping`) USING BTREE ,
INDEX `sd_time` (`sd_time`) USING BTREE 
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
COMMENT='晒单'
AUTO_INCREMENT=27

;

-- ----------------------------
-- Table structure for `go_shaidan_hueifu`
-- ----------------------------
DROP TABLE IF EXISTS `go_shaidan_hueifu`;
CREATE TABLE `go_shaidan_hueifu` (
`id`  int(10) UNSIGNED NOT NULL AUTO_INCREMENT ,
`sdhf_id`  int(11) NOT NULL COMMENT '晒单ID' ,
`sdhf_userid`  int(11) NULL DEFAULT NULL COMMENT '晒单回复会员ID' ,
`sdhf_content`  text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '晒单回复内容' ,
`sdhf_time`  int(11) NULL DEFAULT NULL ,
`sdhf_username`  char(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`sdhf_img`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
PRIMARY KEY (`id`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
AUTO_INCREMENT=1

;

-- ----------------------------
-- Table structure for `go_shopcodes_1`
-- ----------------------------
DROP TABLE IF EXISTS `go_shopcodes_1`;
CREATE TABLE `go_shopcodes_1` (
`id`  int(10) UNSIGNED NOT NULL AUTO_INCREMENT ,
`s_id`  int(10) UNSIGNED NOT NULL ,
`s_cid`  smallint(5) UNSIGNED NOT NULL ,
`s_len`  smallint(5) NULL DEFAULT NULL ,
`s_codes`  text CHARACTER SET utf8 COLLATE utf8_general_ci NULL ,
`s_codes_tmp`  text CHARACTER SET utf8 COLLATE utf8_general_ci NULL ,
PRIMARY KEY (`id`),
INDEX `s_id` (`s_id`) USING BTREE ,
INDEX `s_cid` (`s_cid`) USING BTREE ,
INDEX `s_len` (`s_len`) USING BTREE 
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
AUTO_INCREMENT=12203

;

-- ----------------------------
-- Table structure for `go_shoplist`
-- ----------------------------
DROP TABLE IF EXISTS `go_shoplist`;
CREATE TABLE `go_shoplist` (
`id`  int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '商品id' ,
`sid`  int(10) UNSIGNED NOT NULL COMMENT '同一个商品' ,
`cateid`  smallint(6) UNSIGNED NULL DEFAULT NULL COMMENT '所属栏目ID' ,
`brandid`  smallint(6) UNSIGNED NULL DEFAULT NULL COMMENT '所属品牌ID' ,
`title`  varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '商品标题' ,
`title_style`  varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`title2`  varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '副标题' ,
`keywords`  varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`description`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`money`  decimal(10,2) NULL DEFAULT 0.00 COMMENT '金额' ,
`yunjiage`  decimal(4,2) UNSIGNED NULL DEFAULT 1.00 COMMENT '云购人次价格' ,
`zongrenshu`  int(10) UNSIGNED NULL DEFAULT 0 COMMENT '总需人数' ,
`canyurenshu`  int(10) UNSIGNED NULL DEFAULT 0 COMMENT '已参与人数' ,
`shenyurenshu`  int(10) UNSIGNED NULL DEFAULT NULL ,
`def_renshu`  int(10) UNSIGNED NULL DEFAULT 0 ,
`qishu`  smallint(6) UNSIGNED NULL DEFAULT 0 COMMENT '期数' ,
`maxqishu`  smallint(5) UNSIGNED NULL DEFAULT 1 COMMENT ' 最大期数' ,
`thumb`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`picarr`  text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '商品图片' ,
`content`  mediumtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '商品内容详情' ,
`codes_table`  char(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`xsjx_time`  int(10) UNSIGNED NULL DEFAULT NULL ,
`pos`  tinyint(4) UNSIGNED NULL DEFAULT NULL COMMENT '是否推荐' ,
`renqi`  tinyint(4) UNSIGNED NULL DEFAULT 0 COMMENT '是否人气商品0否1是' ,
`default_renci`  tinyint(4) NOT NULL DEFAULT 1 COMMENT '默认购买人次' ,
`time`  int(10) UNSIGNED NULL DEFAULT NULL COMMENT '时间' ,
`order`  int(10) UNSIGNED NULL DEFAULT 1 ,
`q_uid`  int(10) UNSIGNED NULL DEFAULT NULL COMMENT '中奖人ID' ,
`q_user`  text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '中奖人信息' ,
`q_user_code`  char(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '中奖码' ,
`q_content`  mediumtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '揭晓内容' ,
`q_counttime`  char(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '总时间相加' ,
`q_end_time`  char(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '揭晓时间' ,
`q_showtime`  char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'N' COMMENT 'Y/N揭晓动画' ,
`q_ssccode`  char(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`q_sscopen`  varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`q_sscphase`  varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`q_djstime`  char(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`is_shi`  tinyint(1) NOT NULL DEFAULT 0 ,
`is_new`  tinyint(1) NOT NULL DEFAULT 0 COMMENT '是否是最新  1是 0否' ,
`is_apple`  int(2) NOT NULL DEFAULT 0 COMMENT '是否是苹果相关产品' ,
`zdrange`  varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' ,
PRIMARY KEY (`id`),
INDEX `renqi` (`renqi`) USING BTREE ,
INDEX `order` (`yunjiage`) USING BTREE ,
INDEX `q_uid` (`q_uid`) USING BTREE ,
INDEX `sid` (`sid`) USING BTREE ,
INDEX `shenyurenshu` (`shenyurenshu`) USING BTREE ,
INDEX `q_showtime` (`q_showtime`) USING BTREE 
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
COMMENT='商品表'
AUTO_INCREMENT=4787

;

-- ----------------------------
-- Table structure for `go_shoplist_copy`
-- ----------------------------
DROP TABLE IF EXISTS `go_shoplist_copy`;
CREATE TABLE `go_shoplist_copy` (
`id`  int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '商品id' ,
`sid`  int(10) UNSIGNED NOT NULL COMMENT '同一个商品' ,
`cateid`  smallint(6) UNSIGNED NULL DEFAULT NULL COMMENT '所属栏目ID' ,
`brandid`  smallint(6) UNSIGNED NULL DEFAULT NULL COMMENT '所属品牌ID' ,
`title`  varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '商品标题' ,
`title_style`  varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`title2`  varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '副标题' ,
`keywords`  varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`description`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`money`  decimal(10,2) NULL DEFAULT 0.00 COMMENT '金额' ,
`yunjiage`  decimal(4,2) UNSIGNED NULL DEFAULT 1.00 COMMENT '云购人次价格' ,
`zongrenshu`  int(10) UNSIGNED NULL DEFAULT 0 COMMENT '总需人数' ,
`canyurenshu`  int(10) UNSIGNED NULL DEFAULT 0 COMMENT '已参与人数' ,
`shenyurenshu`  int(10) UNSIGNED NULL DEFAULT NULL ,
`def_renshu`  int(10) UNSIGNED NULL DEFAULT 0 ,
`qishu`  smallint(6) UNSIGNED NULL DEFAULT 0 COMMENT '期数' ,
`maxqishu`  smallint(5) UNSIGNED NULL DEFAULT 1 COMMENT ' 最大期数' ,
`thumb`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`picarr`  text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '商品图片' ,
`content`  mediumtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '商品内容详情' ,
`codes_table`  char(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`xsjx_time`  int(10) UNSIGNED NULL DEFAULT NULL ,
`pos`  tinyint(4) UNSIGNED NULL DEFAULT NULL COMMENT '是否推荐' ,
`renqi`  tinyint(4) UNSIGNED NULL DEFAULT 0 COMMENT '是否人气商品0否1是' ,
`default_renci`  tinyint(4) NOT NULL DEFAULT 1 COMMENT '默认购买人次' ,
`time`  int(10) UNSIGNED NULL DEFAULT NULL COMMENT '时间' ,
`order`  int(10) UNSIGNED NULL DEFAULT 1 ,
`q_uid`  int(10) UNSIGNED NULL DEFAULT NULL COMMENT '中奖人ID' ,
`q_user`  text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '中奖人信息' ,
`q_user_code`  char(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '中奖码' ,
`q_content`  mediumtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '揭晓内容' ,
`q_counttime`  char(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '总时间相加' ,
`q_end_time`  char(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '揭晓时间' ,
`q_showtime`  char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'N' COMMENT 'Y/N揭晓动画' ,
`q_ssccode`  char(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`q_sscopen`  varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`q_sscphase`  varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`q_djstime`  char(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`is_shi`  tinyint(1) NOT NULL DEFAULT 0 ,
`is_new`  tinyint(1) NOT NULL DEFAULT 0 COMMENT '是否是最新  1是 0否' ,
`is_apple`  int(2) NOT NULL DEFAULT 0 COMMENT '是否是苹果相关产品' ,
`zdrange`  varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' ,
PRIMARY KEY (`id`),
INDEX `renqi` (`renqi`) USING BTREE ,
INDEX `order` (`yunjiage`) USING BTREE ,
INDEX `q_uid` (`q_uid`) USING BTREE ,
INDEX `sid` (`sid`) USING BTREE ,
INDEX `shenyurenshu` (`shenyurenshu`) USING BTREE ,
INDEX `q_showtime` (`q_showtime`) USING BTREE 
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
COMMENT='商品表'
AUTO_INCREMENT=4255

;

-- ----------------------------
-- Table structure for `go_shoplist_del`
-- ----------------------------
DROP TABLE IF EXISTS `go_shoplist_del`;
CREATE TABLE `go_shoplist_del` (
`id`  int(10) UNSIGNED NOT NULL AUTO_INCREMENT ,
`sid`  int(10) NOT NULL COMMENT '同一个商品' ,
`cateid`  smallint(6) UNSIGNED NULL DEFAULT NULL ,
`brandid`  smallint(6) UNSIGNED NULL DEFAULT NULL ,
`title`  varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`title_style`  varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`title2`  varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`keywords`  varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`description`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`money`  decimal(10,2) NULL DEFAULT 0.00 ,
`yunjiage`  decimal(4,2) UNSIGNED NULL DEFAULT 1.00 ,
`zongrenshu`  int(10) UNSIGNED NULL DEFAULT 0 ,
`canyurenshu`  int(10) UNSIGNED NULL DEFAULT 0 ,
`shenyurenshu`  int(10) UNSIGNED NULL DEFAULT NULL ,
`def_renshu`  int(10) UNSIGNED NULL DEFAULT 0 ,
`qishu`  smallint(6) UNSIGNED NULL DEFAULT 0 ,
`maxqishu`  smallint(5) UNSIGNED NULL DEFAULT 1 ,
`thumb`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`picarr`  text CHARACTER SET utf8 COLLATE utf8_general_ci NULL ,
`content`  mediumtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL ,
`codes_table`  char(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`xsjx_time`  int(10) UNSIGNED NULL DEFAULT NULL ,
`pos`  tinyint(4) UNSIGNED NULL DEFAULT NULL ,
`renqi`  tinyint(4) UNSIGNED NULL DEFAULT 0 ,
`time`  int(10) UNSIGNED NULL DEFAULT NULL ,
`order`  int(10) UNSIGNED NULL DEFAULT 1 ,
`q_uid`  int(10) UNSIGNED NULL DEFAULT NULL ,
`q_user`  text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '中奖人信息' ,
`q_user_code`  char(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`q_content`  mediumtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL ,
`q_counttime`  char(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`q_end_time`  char(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`q_showtime`  char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'N' COMMENT 'Y/N揭晓动画' ,
PRIMARY KEY (`id`),
INDEX `renqi` (`renqi`) USING BTREE ,
INDEX `order` (`yunjiage`) USING BTREE ,
INDEX `q_uid` (`q_uid`) USING BTREE ,
INDEX `sid` (`sid`) USING BTREE ,
INDEX `shenyurenshu` (`shenyurenshu`) USING BTREE ,
INDEX `q_showtime` (`q_showtime`) USING BTREE 
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
AUTO_INCREMENT=1

;

-- ----------------------------
-- Table structure for `go_slide`
-- ----------------------------
DROP TABLE IF EXISTS `go_slide`;
CREATE TABLE `go_slide` (
`id`  int(10) UNSIGNED NOT NULL AUTO_INCREMENT ,
`img`  varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '幻灯片' ,
`title`  varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`link`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`slide_type`  tinyint(2) NOT NULL DEFAULT 0 COMMENT '幻灯片的类型：0无，1为商品分类，2为商品详情' ,
`slide_val`  varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '取值  ' ,
`is_apple`  tinyint(1) NOT NULL DEFAULT 0 COMMENT '是否是苹果相关产品  1是 0否' ,
`sort_order`  int(2) NOT NULL DEFAULT 10 ,
PRIMARY KEY (`id`),
INDEX `img` (`img`) USING BTREE 
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
COMMENT='幻灯片表'
AUTO_INCREMENT=27

;

-- ----------------------------
-- Table structure for `go_source_list`
-- ----------------------------
DROP TABLE IF EXISTS `go_source_list`;
CREATE TABLE `go_source_list` (
`s_id`  int(10) UNSIGNED NOT NULL AUTO_INCREMENT ,
`s_name`  varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`s_ename`  varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`s_words`  text CHARACTER SET utf8 COLLATE utf8_general_ci NULL ,
`s_reglink`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`addtime`  int(10) NULL DEFAULT NULL ,
PRIMARY KEY (`s_id`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
AUTO_INCREMENT=33

;

-- ----------------------------
-- Table structure for `go_template`
-- ----------------------------
DROP TABLE IF EXISTS `go_template`;
CREATE TABLE `go_template` (
`id`  int(10) UNSIGNED NOT NULL AUTO_INCREMENT ,
`template_name`  char(25) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL ,
`template`  char(25) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL ,
`des`  varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
PRIMARY KEY (`id`),
INDEX `template` (`template`) USING BTREE 
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
AUTO_INCREMENT=1

;

-- ----------------------------
-- Table structure for `go_tongji`
-- ----------------------------
DROP TABLE IF EXISTS `go_tongji`;
CREATE TABLE `go_tongji` (
`id`  int(10) UNSIGNED NOT NULL AUTO_INCREMENT ,
`ip`  varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户ip' ,
`from`  varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '推广来源' ,
`type`  varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'pc 或者 mobile' ,
`date_time`  varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '时间' ,
PRIMARY KEY (`id`)
)
ENGINE=MyISAM
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
AUTO_INCREMENT=44

;

-- ----------------------------
-- Table structure for `go_uv`
-- ----------------------------
DROP TABLE IF EXISTS `go_uv`;
CREATE TABLE `go_uv` (
`id`  int(10) UNSIGNED NOT NULL AUTO_INCREMENT ,
`page`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '当前页面' ,
`source`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '上一页面' ,
`ip`  varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`su`  varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '渠道名称' ,
`sk`  varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '渠道关键词' ,
`type`  tinyint(1) NOT NULL DEFAULT 0 COMMENT '0电脑    1手机' ,
`time`  int(10) NULL DEFAULT NULL ,
PRIMARY KEY (`id`),
INDEX `su` (`su`) USING BTREE ,
INDEX `time` (`time`) USING BTREE 
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
AUTO_INCREMENT=191983

;

-- ----------------------------
-- Table structure for `go_vote_activer`
-- ----------------------------
DROP TABLE IF EXISTS `go_vote_activer`;
CREATE TABLE `go_vote_activer` (
`id`  int(11) NOT NULL AUTO_INCREMENT ,
`option_id`  int(11) NOT NULL ,
`vote_id`  int(11) NULL DEFAULT NULL ,
`userid`  int(11) NULL DEFAULT NULL ,
`ip`  char(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`subtime`  int(11) NULL DEFAULT NULL ,
PRIMARY KEY (`id`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
AUTO_INCREMENT=1

;

-- ----------------------------
-- Table structure for `go_vote_option`
-- ----------------------------
DROP TABLE IF EXISTS `go_vote_option`;
CREATE TABLE `go_vote_option` (
`option_id`  int(11) NOT NULL AUTO_INCREMENT ,
`vote_id`  int(11) NULL DEFAULT NULL ,
`option_title`  varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`option_number`  int(11) UNSIGNED NULL DEFAULT 0 ,
PRIMARY KEY (`option_id`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
AUTO_INCREMENT=1

;

-- ----------------------------
-- Table structure for `go_vote_subject`
-- ----------------------------
DROP TABLE IF EXISTS `go_vote_subject`;
CREATE TABLE `go_vote_subject` (
`vote_id`  int(11) NOT NULL AUTO_INCREMENT ,
`vote_title`  varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`vote_starttime`  int(11) NULL DEFAULT NULL ,
`vote_endtime`  int(11) NULL DEFAULT NULL ,
`vote_sendtime`  int(11) NULL DEFAULT NULL ,
`vote_description`  text CHARACTER SET utf8 COLLATE utf8_general_ci NULL ,
`vote_allowview`  tinyint(1) NULL DEFAULT NULL ,
`vote_allowguest`  tinyint(1) NULL DEFAULT NULL ,
`vote_interval`  int(11) NULL DEFAULT 0 ,
`vote_enabled`  tinyint(1) NULL DEFAULT NULL ,
`vote_number`  int(11) NULL DEFAULT NULL ,
PRIMARY KEY (`vote_id`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
AUTO_INCREMENT=1

;

-- ----------------------------
-- Table structure for `go_wap`
-- ----------------------------
DROP TABLE IF EXISTS `go_wap`;
CREATE TABLE `go_wap` (
`id`  int(10) UNSIGNED NOT NULL AUTO_INCREMENT ,
`img`  varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '幻灯片' ,
`title`  varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`color`  varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`link`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
PRIMARY KEY (`id`),
INDEX `img` (`img`) USING BTREE 
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
COMMENT='幻灯片表'
AUTO_INCREMENT=1

;

-- ----------------------------
-- Table structure for `go_zyy_log`
-- ----------------------------
DROP TABLE IF EXISTS `go_zyy_log`;
CREATE TABLE `go_zyy_log` (
`id`  int(11) NOT NULL AUTO_INCREMENT ,
`log`  text CHARACTER SET utf8 COLLATE utf8_general_ci NULL ,
PRIMARY KEY (`id`)
)
ENGINE=MyISAM
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
AUTO_INCREMENT=24374

;

-- ----------------------------
-- Auto increment value for `go_active`
-- ----------------------------
ALTER TABLE `go_active` AUTO_INCREMENT=148;

-- ----------------------------
-- Auto increment value for `go_ad_area`
-- ----------------------------
ALTER TABLE `go_ad_area` AUTO_INCREMENT=5;

-- ----------------------------
-- Auto increment value for `go_ad_data`
-- ----------------------------
ALTER TABLE `go_ad_data` AUTO_INCREMENT=1;

-- ----------------------------
-- Auto increment value for `go_admin`
-- ----------------------------
ALTER TABLE `go_admin` AUTO_INCREMENT=29;

-- ----------------------------
-- Auto increment value for `go_admin_group`
-- ----------------------------
ALTER TABLE `go_admin_group` AUTO_INCREMENT=14;

-- ----------------------------
-- Auto increment value for `go_app_article`
-- ----------------------------
ALTER TABLE `go_app_article` AUTO_INCREMENT=1;

-- ----------------------------
-- Auto increment value for `go_app_cart`
-- ----------------------------
ALTER TABLE `go_app_cart` AUTO_INCREMENT=835;

-- ----------------------------
-- Auto increment value for `go_appimg`
-- ----------------------------
ALTER TABLE `go_appimg` AUTO_INCREMENT=21;

-- ----------------------------
-- Auto increment value for `go_article`
-- ----------------------------
ALTER TABLE `go_article` AUTO_INCREMENT=41;

-- ----------------------------
-- Auto increment value for `go_auth_list`
-- ----------------------------
ALTER TABLE `go_auth_list` AUTO_INCREMENT=11;

-- ----------------------------
-- Auto increment value for `go_brand`
-- ----------------------------
ALTER TABLE `go_brand` AUTO_INCREMENT=162;

-- ----------------------------
-- Auto increment value for `go_caches`
-- ----------------------------
ALTER TABLE `go_caches` AUTO_INCREMENT=14;

-- ----------------------------
-- Auto increment value for `go_category`
-- ----------------------------
ALTER TABLE `go_category` AUTO_INCREMENT=21;

-- ----------------------------
-- Auto increment value for `go_ceshi_message`
-- ----------------------------
ALTER TABLE `go_ceshi_message` AUTO_INCREMENT=16;

-- ----------------------------
-- Auto increment value for `go_config`
-- ----------------------------
ALTER TABLE `go_config` AUTO_INCREMENT=26;

-- ----------------------------
-- Auto increment value for `go_data_statistics`
-- ----------------------------
ALTER TABLE `go_data_statistics` AUTO_INCREMENT=4337;

-- ----------------------------
-- Auto increment value for `go_egglotter_award`
-- ----------------------------
ALTER TABLE `go_egglotter_award` AUTO_INCREMENT=1;

-- ----------------------------
-- Auto increment value for `go_egglotter_rule`
-- ----------------------------
ALTER TABLE `go_egglotter_rule` AUTO_INCREMENT=1;

-- ----------------------------
-- Auto increment value for `go_egglotter_spoil`
-- ----------------------------
ALTER TABLE `go_egglotter_spoil` AUTO_INCREMENT=1;

-- ----------------------------
-- Auto increment value for `go_goodlist`
-- ----------------------------
ALTER TABLE `go_goodlist` AUTO_INCREMENT=8;

-- ----------------------------
-- Auto increment value for `go_goodlist_li`
-- ----------------------------
ALTER TABLE `go_goodlist_li` AUTO_INCREMENT=3;

-- ----------------------------
-- Auto increment value for `go_goods`
-- ----------------------------
ALTER TABLE `go_goods` AUTO_INCREMENT=1;

-- ----------------------------
-- Auto increment value for `go_link`
-- ----------------------------
ALTER TABLE `go_link` AUTO_INCREMENT=1;

-- ----------------------------
-- Auto increment value for `go_member`
-- ----------------------------
ALTER TABLE `go_member` AUTO_INCREMENT=14380;

-- ----------------------------
-- Auto increment value for `go_member_addmoney_record`
-- ----------------------------
ALTER TABLE `go_member_addmoney_record` AUTO_INCREMENT=1902;

-- ----------------------------
-- Auto increment value for `go_member_band`
-- ----------------------------
ALTER TABLE `go_member_band` AUTO_INCREMENT=1260;

-- ----------------------------
-- Auto increment value for `go_member_cashout`
-- ----------------------------
ALTER TABLE `go_member_cashout` AUTO_INCREMENT=1;

-- ----------------------------
-- Auto increment value for `go_member_del`
-- ----------------------------
ALTER TABLE `go_member_del` AUTO_INCREMENT=14342;

-- ----------------------------
-- Auto increment value for `go_member_dizhi`
-- ----------------------------
ALTER TABLE `go_member_dizhi` AUTO_INCREMENT=1221;

-- ----------------------------
-- Auto increment value for `go_member_friendship`
-- ----------------------------
ALTER TABLE `go_member_friendship` AUTO_INCREMENT=1;

-- ----------------------------
-- Auto increment value for `go_member_go_record`
-- ----------------------------
ALTER TABLE `go_member_go_record` AUTO_INCREMENT=68978;

-- ----------------------------
-- Auto increment value for `go_member_group`
-- ----------------------------
ALTER TABLE `go_member_group` AUTO_INCREMENT=7;

-- ----------------------------
-- Auto increment value for `go_member_jifen`
-- ----------------------------
ALTER TABLE `go_member_jifen` AUTO_INCREMENT=1;

-- ----------------------------
-- Auto increment value for `go_member_message`
-- ----------------------------
ALTER TABLE `go_member_message` AUTO_INCREMENT=1;

-- ----------------------------
-- Auto increment value for `go_member_recodes`
-- ----------------------------
ALTER TABLE `go_member_recodes` AUTO_INCREMENT=2;

-- ----------------------------
-- Auto increment value for `go_mobile_code`
-- ----------------------------
ALTER TABLE `go_mobile_code` AUTO_INCREMENT=18766;

-- ----------------------------
-- Auto increment value for `go_model`
-- ----------------------------
ALTER TABLE `go_model` AUTO_INCREMENT=3;

-- ----------------------------
-- Auto increment value for `go_navigation`
-- ----------------------------
ALTER TABLE `go_navigation` AUTO_INCREMENT=18;

-- ----------------------------
-- Auto increment value for `go_newest_jackpot`
-- ----------------------------
ALTER TABLE `go_newest_jackpot` AUTO_INCREMENT=549;

-- ----------------------------
-- Auto increment value for `go_pay`
-- ----------------------------
ALTER TABLE `go_pay` AUTO_INCREMENT=7;

-- ----------------------------
-- Auto increment value for `go_position`
-- ----------------------------
ALTER TABLE `go_position` AUTO_INCREMENT=1;

-- ----------------------------
-- Auto increment value for `go_position_data`
-- ----------------------------
ALTER TABLE `go_position_data` AUTO_INCREMENT=1;

-- ----------------------------
-- Auto increment value for `go_pv`
-- ----------------------------
ALTER TABLE `go_pv` AUTO_INCREMENT=626581;

-- ----------------------------
-- Auto increment value for `go_qqset`
-- ----------------------------
ALTER TABLE `go_qqset` AUTO_INCREMENT=1;

-- ----------------------------
-- Auto increment value for `go_quanzi`
-- ----------------------------
ALTER TABLE `go_quanzi` AUTO_INCREMENT=1;

-- ----------------------------
-- Auto increment value for `go_quanzi_hueifu`
-- ----------------------------
ALTER TABLE `go_quanzi_hueifu` AUTO_INCREMENT=1;

-- ----------------------------
-- Auto increment value for `go_quanzi_tiezi`
-- ----------------------------
ALTER TABLE `go_quanzi_tiezi` AUTO_INCREMENT=1;

-- ----------------------------
-- Auto increment value for `go_recharge_percentage`
-- ----------------------------
ALTER TABLE `go_recharge_percentage` AUTO_INCREMENT=6;

-- ----------------------------
-- Auto increment value for `go_recom`
-- ----------------------------
ALTER TABLE `go_recom` AUTO_INCREMENT=1;

-- ----------------------------
-- Auto increment value for `go_send`
-- ----------------------------
ALTER TABLE `go_send` AUTO_INCREMENT=168;

-- ----------------------------
-- Auto increment value for `go_shaidan`
-- ----------------------------
ALTER TABLE `go_shaidan` AUTO_INCREMENT=27;

-- ----------------------------
-- Auto increment value for `go_shaidan_hueifu`
-- ----------------------------
ALTER TABLE `go_shaidan_hueifu` AUTO_INCREMENT=1;

-- ----------------------------
-- Auto increment value for `go_shopcodes_1`
-- ----------------------------
ALTER TABLE `go_shopcodes_1` AUTO_INCREMENT=12203;

-- ----------------------------
-- Auto increment value for `go_shoplist`
-- ----------------------------
ALTER TABLE `go_shoplist` AUTO_INCREMENT=4787;

-- ----------------------------
-- Auto increment value for `go_shoplist_copy`
-- ----------------------------
ALTER TABLE `go_shoplist_copy` AUTO_INCREMENT=4255;

-- ----------------------------
-- Auto increment value for `go_shoplist_del`
-- ----------------------------
ALTER TABLE `go_shoplist_del` AUTO_INCREMENT=1;

-- ----------------------------
-- Auto increment value for `go_slide`
-- ----------------------------
ALTER TABLE `go_slide` AUTO_INCREMENT=27;

-- ----------------------------
-- Auto increment value for `go_source_list`
-- ----------------------------
ALTER TABLE `go_source_list` AUTO_INCREMENT=33;

-- ----------------------------
-- Auto increment value for `go_template`
-- ----------------------------
ALTER TABLE `go_template` AUTO_INCREMENT=1;

-- ----------------------------
-- Auto increment value for `go_tongji`
-- ----------------------------
ALTER TABLE `go_tongji` AUTO_INCREMENT=44;

-- ----------------------------
-- Auto increment value for `go_uv`
-- ----------------------------
ALTER TABLE `go_uv` AUTO_INCREMENT=191983;

-- ----------------------------
-- Auto increment value for `go_vote_activer`
-- ----------------------------
ALTER TABLE `go_vote_activer` AUTO_INCREMENT=1;

-- ----------------------------
-- Auto increment value for `go_vote_option`
-- ----------------------------
ALTER TABLE `go_vote_option` AUTO_INCREMENT=1;

-- ----------------------------
-- Auto increment value for `go_vote_subject`
-- ----------------------------
ALTER TABLE `go_vote_subject` AUTO_INCREMENT=1;

-- ----------------------------
-- Auto increment value for `go_wap`
-- ----------------------------
ALTER TABLE `go_wap` AUTO_INCREMENT=1;

-- ----------------------------
-- Auto increment value for `go_zyy_log`
-- ----------------------------
ALTER TABLE `go_zyy_log` AUTO_INCREMENT=24374;
