//弹窗timer对象
var msg_timer = 0;
//弹窗对象函数
var msg_show = {
	// 您无此操作权限
	info : function(msg, _callback) {
		var layerHtml = '<div class="layer"></div>';
		var erHtml = '<div class="msgBox"><div class="msg_content"><div class="msg_pic qx_msg_pic"></div><p class="msg_text">'
				+ msg + '</p><i class="cancel"></i></div></div>';
		$("body").append(layerHtml + erHtml);
		$(".cancel").click(function() {
			msg_show.closeAll();
		});
		if (typeof (_callback) == "function") {
			_callback();
		}
	},
	//弹出警告
	waring : function(msg, _callback) {
		var layerHtml = '<div class="layer2"></div>';
		var erHtml = '<div class="msgBox"><div class="msg_content"><div class="msg_pic qx_msg_pic"></div><p class="msg_text">'
				+ msg + '</p><i class="cancel"></i></div></div>';
		$("body").append(layerHtml + erHtml);
		$(".cancel").click(function() {
			msg_show.closeAll();
		});
		msg_timer=setTimeout(function() {
			if (typeof (_callback) == "function") {
				_callback();
			}
			msg_show.closeAll();
		}, 2000);
	},
	//弹出致命错误
	error:function(msg,type,url){
		var layerHtml = '<div class="layer"></div>';
		if(type==1){
			var erHtml = '<div class="msgBox"><div class="msg_content"><div class="msg_pic cz_msg_pic"></div><p class="msg_text">'
				+ msg
				+ '</p><i class="cancel"></i></div><div class="msg_btn"><div class="triangle"></div><button class="sub_btn bj_sub_btn" href="#">取消</button></div>  </div>';
		}else{
			var erHtml = '<div class="msgBox"><div class="msg_content"><div class="msg_pic cz_msg_pic"></div><p class="msg_text">'
				+ msg
				+ '</p></div><div class="msg_btn"><div class="triangle"></div></div>  </div>';
		}
		
		$("body").append(layerHtml + erHtml);
		$(".cancel").click(function() {
			msg_show.closeAll();
		});
		msg_timer=setTimeout(function(){
			if(type==1){
				if (typeof (url) !== "undefined" && url !== "") {
					location.href = url;
				}
			}
			msg_show.closeAll();
		},2000);
	},
	// 是否确认此操作
	promptmsg : function(msg, _callbacks) {
		var layerHtml = '<div class="layer"></div>';
		var erHtml = '<div class="msgBox"><div class="msg_content"><div class="msg_pic"></div><p class="msg_text">'
				+ msg
				+ '</p><i class="cancel"></i></div><div class="msg_btn"><div class="triangle"></div><button class="sub_btn" href="#">确认</button><button class="colse_btn">取消</button></div></div>';
		$("body").append(layerHtml + erHtml);
		$(".cancel,.colse_btn").click(function() {
			msg_show.closeAll();
		});
		$(".sub_btn").off("click").on("click",function(){
			if(typeof(_callbacks.yes)=="function"){
				_callbacks.yes();
			}
			msg_show.closeAll();
		});
		$(".colse_btn").off("click").on("click",function(){
			if(typeof(_callbacks.no)=="function"){
				_callbacks.no();
			}
			msg_show.closeAll();
		});
	},
	// 您已完成此项操作
	editormsg : function(msg, _callback) {
		var layerHtml = '<div class="layer"></div>';
		var erHtml = '<div class="msgBox"><div class="msg_content"><div class="msg_pic cz_msg_pic"></div><p class="msg_text">'
				+ msg
				+ '</p><i class="cancel"></i></div><div class="msg_btn"><div class="triangle"></div><button class="sub_btn bj_sub_btn" href="#">完成</button></div>  </div>';
		$("body").append(layerHtml + erHtml);
		$(".cancel,.bj_sub_btn").click(function() {
			msg_show.closeAll();
		});
		if (typeof (_callback) == "function") {
			_callback();
		}
	},
	// 自动关闭兼跳转
	success : function(msg, url) {
		var layerHtml = '<div class="layer"></div>';
		var erHtml = '<div class="msgBox"><div class="msg_content"><div class="msg_pic cz_msg_pic"></div><p class="msg_text">'
				+ msg
				+ '</p></div><div class="msg_btn"><div class="triangle"></div></div>  </div>';
		$("body").append(layerHtml + erHtml);
		$(".cancel,.bj_sub_btn").click(function() {
			msg_show.closeAll();
		});
		msg_timer=setTimeout(function() {
			if(url==1){
				location=location
			}else if (typeof (url) !== "undefined" && url !== "") {
				location.href = url;
			}
			msg_show.closeAll();
		}, 2000);
	},
	//关闭层
	closeAll : function() {
		if(msg_timer!==0){
			clearTimeout(msg_timer);
		}
		$(".layer").remove();
		$(".msgBox").remove();
		$(".layer2").remove();
		$(".msg_bg").remove();
	},
	
	//显示加载动画
	show_loading:function()
	{
		var str='<div class="msg_bg" style="background:#000;opacity:0.5;filter:alpha(opacity=50);z-index:99998;width:100%;position:absolute;left:0;top:0"></div>';
		str+='<div class="msg_bg" style="z-index:99999;width:100%;position:absolute;left:0;top:0;text-align:center;"><img src="/Public/images/loading.gif" alt="" class="loading"></div>'
		$('body').append(str);
		var scroll_height=$(document).scrollTop(); 
		$('.msg_bg').height($(document).height());
		$('.loading').css('margin-top',scroll_height+240);
	}
}
