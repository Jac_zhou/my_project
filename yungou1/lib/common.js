function isNull(str) {
    if (str == null || str == "" || str == "undefined") {
        return true;
    }
    return false;
}

function addCookie(objName, objValue, objHours){//添加cookie
    var str = objName + "=" + escape(objValue)+"; path=/";
    if (objHours > 0) {//为0时不设定过期时间，浏览器关闭时cookie自动消失
        var date = new Date();
        var ms = objHours * 3600 * 1000;
        date.setTime(date.getTime() + ms);
        str += "; expires=" + date.toGMTString();
    }
    document.cookie = str;
}

function getCookie(objName){//获取指定名称的cookie的值
    var arrStr = document.cookie.split("; ");
    for (var i = 0; i < arrStr.length; i++) {
        var temp = arrStr[i].split("=");
        if (temp[0] == objName)
            return unescape(temp[1]);
    }
}

function delCookie(name){//为了删除指定名称的cookie，可以将其过期时间设定为一个过去的时间
    var date = new Date();
    date.setTime(date.getTime() - 10000);
    document.cookie = name + "=a; expires=" + date.toGMTString()+"; path=/";
}


var key = "90782AC08A87C5E750369836C34DB46B";

/*var on_off = false;

function get_on_off(){
    console.log("on_off", on_off);
    return on_off;
}

function set_on_off(data){
    on_off = data;
    console.log("on_off", on_off);
}*/

function showLoading(){
    $('#loadingModal').modal({backdrop: 'static', keyboard: false});
    //$('#loadingModal').modal('show');
    $('#loadingModal').css('margin-top', $(window).height()/2 - 30);
}

function hideLoading(){
    $('#loadingModal').modal('hide');
}

function connectWebViewJavascriptBridge(callback) {
    if (window.WebViewJavascriptBridge) {
        callback(WebViewJavascriptBridge);
    } else {
        document.addEventListener('WebViewJavascriptBridgeReady', function() {
            callback(WebViewJavascriptBridge);
        }, false)
    }
}
/*
//设置cookie
function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    var expires = "expires="+d.toUTCString();
    document.cookie = cname + "=" + cvalue + "; " + expires+"; path=/";
}
/*
//获取cookie
function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i=0; i<ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1);
        if (c.indexOf(name) != -1) return c.substring(name.length, c.length);
    }
    return "";
}*/
//清除cookie  
/*
function clearCookie(name) {  
    setCookie(name, "", -1);  
}  
function checkCookie() {
    var user = getCookie("username");
    if (user != "") {
        alert("Welcome again " + user);
    } else {
        user = prompt("Please enter your name:", "");
        if (user != "" && user != null) {
            setCookie("username", user, 365);
        }
    }
}
*/
$.extend($.fn,{
    fnTimeCountDown:function(d,callback){
        this.each(function(){
            var $this = $(this);
            var o = {
                hm: $this.find(".hm"),
                sec: $this.find(".sec"),
                mini: $this.find(".mini"),
                hour: $this.find(".hour"),
                day: $this.find(".day"),
                month:$this.find(".month"),
                year: $this.find(".year")
            };
            var f = {
                haomiao: function(n){
                    if(n < 10)return "00" + n.toString();
                    if(n < 100)return "0" + n.toString();
                    return n.toString();
                },
                zero: function(n){
                    var _n = parseInt(n, 10);//解析字符串,返回整数
                    if(_n > 0){
                        if(_n <= 9){
                            _n = "0" + _n
                        }
                        return String(_n);
                    }else{
                        return "00";
                    }
                },
                dv: function(){
                    //d = d || Date.UTC(2050, 0, 1); //如果未定义时间，则我们设定倒计时日期是2050年1月1日
                    var _d = d;
                    timeold = _d-(new Date().getTime());
                    //现在将来秒差值
                    //alert(future.getTimezoneOffset());
                    var pms = {
                        hm:"000",
                        sec: "00",
                        mini: "00",
                        hour: "00",
                        day: "00",
                        month: "00",
                        year: "0"
                    };
                    if(timeold > 0){
                        sectimeold=timeold/1000
                        secondsold=Math.floor(sectimeold);
                        msPerDay=24*60*60*1000
                        e_daysold=timeold/msPerDay
                        daysold=Math.floor(e_daysold); 				//天
                        e_hrsold=(e_daysold-daysold)*24;
                        hrsold=Math.floor(e_hrsold); 				//时
                        e_minsold=(e_hrsold-hrsold)*60;
                        //分
                        minsold=Math.floor((e_hrsold-hrsold)*60);
                        minsold = (minsold<10?'0'+minsold:minsold)
                        minsold = new String(minsold);
                        minsold_1 = minsold.substr(0,1);
                        minsold_2 = minsold.substr(1,1);

                        //秒
                        e_seconds = (e_minsold-minsold)*60;
                        seconds=Math.floor((e_minsold-minsold)*60);
                        seconds = (seconds<10?'0'+seconds:seconds)
                        seconds = new String(seconds);
                        seconds_1 = seconds.substr(0,1);
                        seconds_2 = seconds.substr(1,1);
                        //毫秒
                        ms = e_seconds-seconds;
                        ms = new String(ms)
                        ms_1 = ms.substr(2,1);
                        ms_2 = ms.substr(3,1);


                        pms.hm = ms_1+''+ms_2;
                        pms.sec = seconds_1+''+seconds_2;
                        pms.mini = minsold_1+''+minsold_2;
                        pms.hour = hrsold>10?hrsold:'0'+hrsold;
                        pms.day = daysold;
                    }else{
                        pms.year=pms.month=pms.day=pms.hour=pms.mini=pms.sec="00";
                        pms.hm = "000";
                        callback();
                        //alert('结束了');
                        //return;
                    }
                    return pms;
                },
                ui: function(){
                    if(o.hm){
                        o.hm.html(f.dv().hm);
                    }
                    if(o.sec){
                        o.sec.html(f.dv().sec);
                    }
                    if(o.mini){
                        o.mini.html(f.dv().mini);
                    }
                    if(o.hour){
                        o.hour.html(f.dv().hour);
                    }
                    if(o.day){
                        o.day.html(f.dv().day);
                    }
                    if(o.month){
                        o.month.html(f.dv().month);
                    }
                    if(o.year){
                        o.year.html(f.dv().year);
                    }
                    setTimeout(f.ui, 1);
                }
            };
            f.ui();
        });
    }
});

var weChat = function(){
    $("#mcover").css("display","none");
};