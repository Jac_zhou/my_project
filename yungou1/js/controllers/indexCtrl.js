/**
 * Created by Administrator on 2016/8/5.
 */
define(['./module','location','iscroll','common','cart','md5','bootstrapmin'], function (controllers) {
    'use strict';
    //console.log('控制器Common',controllers);
    //控制器共同调用函数
    controllers.common = {
        state:'ceshi',
        index:function(){

        },
    };

    controllers.controller('CommonCtrl',function($scope,$rootScope,$location,$http){
        document.title = "易中夺宝-好运连连";
        $rootScope.holdUID = getCookie("holdUID");
        $rootScope.holdState = 0;
        if(isNull(getCookie("shopList"))){
            $rootScope.shopList = [];
        }else{
            $rootScope.shopList = eval('('+getCookie("shopList")+')');
        }
        //日期格式  ->时间戳     精确到秒
        $scope.date_time = function transdate(endTime){
            var date=new Date();
            date.setFullYear(endTime.substring(0,4));
            date.setMonth(endTime.substring(5,7)-1);
            date.setDate(endTime.substring(8,10));
            date.setHours(endTime.substring(11,13));
            date.setMinutes(endTime.substring(14,16));
            date.setSeconds(endTime.substring(17,19));
            return Date.parse(date)/1000;
        }
        $scope.is_weixin = function(){
            var ua = window.navigator.userAgent.toLowerCase();
            if(ua.match(/MicroMessenger/i) == 'micromessenger'){
                return true;
            }else{
                return false;
            }
        };
        $scope.login = function(){
            if(!isNull($rootScope.holdUID)){
                $location.url('/tab/personCenter?uid=' + $rootScope.holdUID);
            }else{
                /*if($scope.is_weixin()){
                 window.location.href="http://www.yz-db.com/index.php/api/wapwxlogin";
                 }else{*/
                $location.url('/tab/login');
                //}

            }
        };
        $scope.$on("itemChange", function (event, msg) {
            //console.log("indexCtrl", msg);
            $scope.$broadcast("itemChangeFromParrent", msg);
        });
        $scope.$on("uid", function (event, msg) {
            //console.log("indexCtrl", msg);
            $rootScope.holdUID = msg;
            addCookie("holdUID", msg, 0);
        });

        $scope.confirmExecute = function(){
            $rootScope.holdState = 1;
            $('#confirmModal').modal('hide');
        };

        $scope.dowloadApp = function(){
            var sUserAgent = navigator.userAgent.toLowerCase();
            var bIsIphoneOs = sUserAgent.match(/iphone os/i) == "iphone os";
            var bIsAndroid = sUserAgent.match(/android/i) == "android";
            var bIsWeiXin = sUserAgent.match(/MicroMessenger/i) == "micromessenger";
            if (bIsAndroid) {
                if(bIsWeiXin){
                    $("#mcover").css("display","block");
                }else{
                    window.location.href = "http://www.yz-db.com/yyzl.apk";
                }
            } else if(bIsIphoneOs) {
                if(bIsWeiXin){
                    $("#mcover").css("display","block");
                }else{
                    //$rootScope.showAlert("IOS版即将上线，敬请期待");
                    window.location.href = "http://itunes.apple.com/cn/app/id1071871513?mt=8";
                }
            }
        };

        $scope.choosed = false;

        $scope.addNewAddress = function(){
            $('#addressModal').modal('hide');
            if($scope.userAddress.length > 4){
                $rootScope.showAlert("最多只能添加5条地址");
                return;
            }
            $location.url("/tab/addAddress?flag=address&uid=" + $rootScope.holdUID);
        };

        $scope.addressExecute = function(){
            var radios = document.getElementsByName("addressRadios");
            for (var i = 0; i < radios.length; i++) {
                if (radios[i].checked) {
                    if($scope.choosed){
                        $scope.choosed = false;
                        $('#addressModal').modal('hide');
                        $scope.do_confirm_address($scope.rid, radios[i].value);
                    }else{
                        $scope.address_c = $scope.userAddress[i];
                        $scope.choosed = true;
                    }
                    break;
                }
            }
        };

        $scope.hideAddress = function(){
            $('#addressModal').modal('hide');
            $scope.choosed = false;
        };
        //页面跳转
        $scope.click_href=function(url){

            $location.url(url);
        };
        $scope.do_confirm_address = function (rid, aid) {
            showLoading();
            var partner = "ZLAPITOH5WAP";
            var timestamp = Date.parse(new Date())/1000;
            var sign = $.md5(partner + timestamp + key);
            $http({method: 'POST', data: {partner: partner, timestamp: timestamp, sign: sign, uid: $rootScope.holdUID, rid: rid, aid: aid}, url: "../index.php/yunapi/member/do_confirm_address"})
                .success(function (response, status, headers, config) {
                    if (response.status == "1") {
                        /*var str = JSON.stringify(response);
                         console.log("----------->", str);*/
                        $rootScope.showAlert(response.message);
                        $scope.userAddress = [];
                        window.location.reload();
                    } else {
                        $rootScope.showAlert(response.message);
                    }
                    hideLoading();
                })
                .error(function (response, status, headers, config) {
                    hideLoading();
                });
        };

        $rootScope.addToCart = function(data, $event){

            if($rootScope.holdUID>0){
                if(data.wap_ten == 0){
                    $rootScope.showAlert("该产品的购买次数已达到上限");return false;
                }
                var img = data.thumb;
                var flyElm = $('<img style="z-index:999;width:40px;" class="u-flyer" src="'+img+'">');
                $('body').append(flyElm);
                flyElm.css({
                    'z-index': 9000,
                    'display': 'block',
                    'position': 'absolute',
                    'top': event.pageY-50 +'px',
                    'left': event.pageX-50 +'px',
                    'width': 60 +'px',
                    'height': 60 +'px'
                });
                flyElm.animate({
                    top: $("#shopList").offset().top,
                    left: $("#shopList").offset().left+20,
                    width: 5,
                    height: 10
                }, 'slow', function() {
                    flyElm.remove();
                });

                var isAdded = false;
                var syrs = data.zongrenshu - data.canyurenshu;		//剩余人数
                for(var i = 0; i < $rootScope.shopList.length; i++){
                    if(data.id == $rootScope.shopList[i].id){
                        isAdded = true;
                        if(data.is_ten == 1){								//5元限购
                            if($rootScope.shopList[i].num >=data.wap_ten){
                                $rootScope.shopList[i].num = data.wap_ten;
                                $rootScope.showAlert("该产品的购买数量达到上限！");return false;
                            }
                        }
                        if(syrs > $rootScope.shopList[i].num){
                            $rootScope.shopList[i].num ++;
                        }
                        break;
                    }
                }
                if(!isAdded){
                    data.num = 1;
                    $rootScope.shopList.push(data);
                }
                addCookie("shopList", JSON.stringify($rootScope.shopList), 0);
            }else{

                //xian登陆
                $scope.login();
            }
        };

        $scope.showMsg = [];
        $rootScope.showAlert = function(msg, num, code){
            if(num == 0){
                $scope.winCode = code;
                $scope.showMsg = msg.split(",");
                $('#showModal').modal('show');
                $('#showModal').css('margin-top', $(window).height()/2 - 230);
            }else if(num == 1){
                $scope.userAddress = msg;
                $scope.rid = code;
                $('#addressModal').modal('show');
            }else{
                $scope.alertMsg = msg;
                $('#alertModal').modal('show');
            }
        };

        $rootScope.showPic = function(msg){
            $scope.picUrl = msg;
            $('#picModal').modal('show');
        };
        //浏览器跳转
        $scope.browserRedirect = function(){
            var sUserAgent = navigator.userAgent.toLowerCase();
            var bIsIpad = sUserAgent.match(/ipad/i) == "ipad";
            var bIsIphoneOs = sUserAgent.match(/iphone os/i) == "iphone os";
            var bIsMidp = sUserAgent.match(/midp/i) == "midp";
            var bIsUc7 = sUserAgent.match(/rv:1.2.3.4/i) == "rv:1.2.3.4";
            var bIsUc = sUserAgent.match(/ucweb/i) == "ucweb";
            var bIsAndroid = sUserAgent.match(/android/i) == "android";
            var bIsCE = sUserAgent.match(/windows ce/i) == "windows ce";
            var bIsWM = sUserAgent.match(/windows mobile/i) == "windows mobile";
            if (bIsIpad || bIsIphoneOs || bIsMidp || bIsUc7 || bIsUc || bIsAndroid || bIsCE || bIsWM) {
                $("#footbar").show();
            } else {
                $("#footbar").hide();
                // window.location.href = "http://www.yz-db.com";
                window.location.href = "http://120.76.53.156/";
            }
        };
        $scope.browserRedirect();

        $scope.closeBar = function(){
            $("#footbar").hide();
            $("#shopList").css("bottom", "10px");
        };
    });


    controllers.controller('headerCtrl',function($scope,$rootScope,$location,$http){
        $('#body_top').css('height','0px');
        $scope.isSelected1 = true;
        $scope.isSelected2 = false;
        $scope.isSelected3 = false;
        $scope.isSelected4 = false;
        $scope.$on("itemChangeFromParrent", function (event, msg) {
            //console.log("headerCtrl", msg);
            if(msg ==1){
                $scope.isSelected1 = true;
                $scope.isSelected2 = false;
                $scope.isSelected3 = false;
                $scope.isSelected4 = false;
                $('#body_top').css('height','84px');
            }else if(msg == 2){
                $scope.isSelected1 = false;
                $scope.isSelected2 = true;
                $scope.isSelected3 = false;
                $scope.isSelected4 = false;
                $('#body_top').css('height','84px');
            } else if(msg == 3){
                $scope.isSelected1 = false;
                $scope.isSelected2 = false;
                $scope.isSelected3 = true;
                $scope.isSelected4 = false;
                $('#body_top').css('height','84px');
            }else if(msg == 4){
                $scope.isSelected1 = false;
                $scope.isSelected2 = false;
                $scope.isSelected3 = false;
                $scope.isSelected4 = true;
                $('#body_top').css('height','84px');
            }
            $('body').scrollTop(0);
        });

        $scope.is_weixin = function(){
            var ua = window.navigator.userAgent.toLowerCase();
            if(ua.match(/MicroMessenger/i) == 'micromessenger'){
                return true;
            }else{
                return false;
            }
        };

        $scope.toLogin = function(){
            if(!isNull($rootScope.holdUID)){
                $location.url('/tab/personCenter?uid=' + $rootScope.holdUID);
            }else{
                //if($scope.is_weixin()){
                //window.location.href="http://www.yz-db.com/index.php/api/wapwxlogin";
                //}else{
                $location.url('/tab/login');
                //}

            }
        };

        $scope.setSelectedItem = function(num){
            if(num ==1){
                $scope.isSelected1 = true;
                $scope.isSelected2 = false;
                $scope.isSelected3 = false;
                $scope.isSelected4 = false;
            }else if(num == 2){
                $scope.isSelected1 = false;
                $scope.isSelected2 = true;
                $scope.isSelected3 = false;
                $scope.isSelected4 = false;
            } else if(num == 3){
                $scope.isSelected1 = false;
                $scope.isSelected2 = false;
                $scope.isSelected3 = true;
                $scope.isSelected4 = false;
            }else if(num == 4){
                $scope.isSelected1 = false;
                $scope.isSelected2 = false;
                $scope.isSelected3 = false;
                $scope.isSelected4 = true;
            }
        };
        $rootScope.holdUID = getCookie("holdUID");
        if($rootScope.holdUID > 0){
            console.log($rootScope.holdUID );
        }
    });




});