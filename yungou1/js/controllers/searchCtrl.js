define(['./module','./indexCtrl'], function (controllers) {
    'use strict';
    //搜索商品页
    controllers.controller('searchCtrl', function ($scope, $http, $location, $rootScope) {
        document.title = "搜索商品";
        $('body').scrollTop(0);
        $("#headerId").hide();
        $("#body_top").css('height','0px');
        $("#shopList").hide();
        $scope.backTo = function(){
            window.history.go(-1);
        };

        setTimeout(function(){
            $("a").on("touchstart", function(e){
                $(e.target).addClass('box-shadow-style');
            });
            $("a").on("touchmove", function(e){
                $(e.target).removeClass('box-shadow-style');
            });
            $("a").on("touchend", function(e){
                $(e.target).removeClass('box-shadow-style');
            });
        }, 500);

        $scope.goSearch = function($event){
            /*$($event.target).css('box-shadow', '0 0 15px #000');
             setTimeout(function(){
             $($event.target).css('box-shadow', 'none');
             }, 160);*/
            if(isNull($scope.keywords)){
                $rootScope.showAlert("请输入要搜索的关键字");
            } else {
                $location.url("/tab/searchResult?keywords=" + $scope.keywords);
            }
        };

        $scope.wordList = [];

        $scope.get_search_hot_words = function () {
            showLoading();
            var partner = "ZLAPITOH5WAP";
            var timestamp = Date.parse(new Date())/1000;
            var sign = $.md5(partner + timestamp + key);
            $http({method: 'POST', data: {partner: partner, timestamp: timestamp, sign: sign}, url: "../index.php/yunapi/index/get_search_hot_words"})
                .success(function (response, status, headers, config) {
                    if (response.status == "1") {
                        $scope.wordList = response.data;
                        setTimeout(function(){
                            $("a").on("touchstart", function(e){
                                $(e.target).addClass('box-shadow-style');
                            });
                            $("a").on("touchmove", function(e){
                                $(e.target).removeClass('box-shadow-style');
                            });
                            $("a").on("touchend", function(e){
                                $(e.target).removeClass('box-shadow-style');
                            });
                        }, 500);
                    } else {
                        $rootScope.showAlert(response.message);
                    }
                    hideLoading();
                })
                .error(function (response, status, headers, config) {
                    hideLoading();
                });
        };
        $scope.get_search_hot_words();
    })

});