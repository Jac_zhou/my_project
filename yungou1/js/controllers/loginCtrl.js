/**
 * Created by Administrator on 2016/8/8.
 */
define(['./module','./indexCtrl'], function (controllers) {
    'use strict';
    //登陆
    controllers.controller('loginCtrl', function ($scope, $http, $location, $rootScope) {
        $rootScope.holdUID=getCookie("holdUID");
        if(!isNull($rootScope.holdUID)){
            $location.url('/tab/personCenter?uid=' + $rootScope.holdUID);
        }
        document.title = "登陆";
        $('body').scrollTop(0);
        $("#headerId").hide();
        $("#body_top").css('height','0px');
        $("#shopList").hide();
        $scope.backTo = function(){
            window.history.go(-1);
        };

        setTimeout(function(){
            $("a").on("touchstart", function(e){
                $(e.target).addClass('box-shadow-style');
            });
            $("a").on("touchmove", function(e){
                $(e.target).removeClass('box-shadow-style');
            });
            $("a").on("touchend", function(e){
                $(e.target).removeClass('box-shadow-style');
            });
        }, 500);

        $scope.is_weixin = function(){
            var ua = navigator.userAgent.toLowerCase();
            if(ua.match(/MicroMessenger/i)=="micromessenger") {
                return true;
            } else {
                return false;
            }
        };
        console.log($scope.is_weixin());
        $scope.do_login = function ($event) {
            /*$($event.target).css('box-shadow', '0 0 15px #000');
             setTimeout(function(){
             $($event.target).css('box-shadow', 'none');
             }, 160);*/
            if(isNull($scope.account)){
                $rootScope.showAlert("请输入用户名");
                return;
            }
            if(isNull($scope.password)){
                $rootScope.showAlert("请输入密码");
                return;
            }
            showLoading();
            var partner = "ZLAPITOH5WAP";
            var timestamp = Date.parse(new Date())/1000;
            var sign = $.md5(partner + timestamp + key);
            $http({method: 'POST', data: {partner: partner, timestamp: timestamp, sign: sign, username: $scope.account, password: $scope.password}, url: "../index.php/yunapi/member/do_login"})
                .success(function (response, status, headers, config) {
                    if (response.status == "1") {
                        addCookie('holdUID',response.data.uid);
                        //$scope.$emit("uid", response.data.uid);
                        $rootScope.holdUID = response.data.uid;
                        //console.log("loginCtrl", response.data.uid);
                        var url = '/tab/personCenter?uid=' + response.data.uid;
                        $location.url(url);
                    } else {
                        $rootScope.showAlert(response.message);
                    }
                    hideLoading();
                })
                .error(function (response, status, headers, config) {
                    hideLoading();
                });
        };


        $scope.qqlogin = function(){
            window.location.href="http://www.yz-db.com/Zapi/wapqqlogin/init";
        };



    })

});