/**
 * Created by Administrator on 2016/8/8.
 */
define(['./module','./indexCtrl'], function (controllers) {
    'use strict';
    //通知
    controllers.controller('notificationCtrl', function ($scope, $http, $location, $rootScope) {
        document.title = "通知";
        $('body').scrollTop(0);
        $("#headerId").hide();
        $("#shopList").hide();
        $("#body_top").css('height','0px');
        $scope.backTo = function(){
            window.history.go(-1);
        };
        $("#footbar").hide();

        setTimeout(function(){
            $("a").on("touchstart", function(e){
                $(e.target).addClass('box-shadow-style');
            });
            $("a").on("touchmove", function(e){
                $(e.target).removeClass('box-shadow-style');
            });
            $("a").on("touchend", function(e){
                $(e.target).removeClass('box-shadow-style');
            });
        }, 500);

        $scope.get_article_list = function(page, size, type) {
            showLoading();
            var partner = "ZLAPITOH5WAP";
            var timestamp = Date.parse(new Date())/1000;
            var sign = $.md5(partner + timestamp + key);
            $http({method: 'POST', data: {partner: partner, timestamp: timestamp, sign: sign, page: page, size: size, type: type}, url: "../index.php/yunapi/index/get_article_list"})
                .success(function (response, status, headers, config) {
                    if (response.status == "1") {
                        $scope.listData = response.data;
                        setTimeout(function(){
                            $("a").on("touchstart", function(e){
                                $(e.target).addClass('box-shadow-style');
                            });
                            $("a").on("touchmove", function(e){
                                $(e.target).removeClass('box-shadow-style');
                            });
                            $("a").on("touchend", function(e){
                                $(e.target).removeClass('box-shadow-style');
                            });
                        }, 500);
                    } else {
                        $rootScope.showAlert(response.message);
                    }
                    hideLoading();
                })
                .error(function (response, status, headers, config) {
                    hideLoading();
                });
        };
        $scope.get_article_list(1, 20, 3);

    })

});