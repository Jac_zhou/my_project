/**
 * Created by Administrator on 2016/8/8.
 */
define(['./module','./indexCtrl'], function (controllers) {
    'use strict';
    //注册
    controllers.controller('rigisterCtrl', function ($scope, $http, $location, $rootScope) {
        document.title = "注册";
        $('body').scrollTop(0);
        $("#headerId").hide();
        $("#body_top").css('height','0px');
        $("#shopList").hide();
        $scope.checked = false;
        $scope.codeText = "获取验证码";
        $scope.backTo = function(){
            window.history.go(-1);
        };

        var su = getCookie('su');
        var sk = getCookie('sk');

        $scope.nowtime = 60;
        $scope.setTime = function(){
            if($scope.nowtime == 0){
                clearInterval($scope.shut);
                $scope.checked = false;
                $scope.codeText = "重新获取";
                $scope.$apply();
            } else {
                $scope.nowtime --;
                $scope.codeText = "("+$scope.nowtime+"S)重新获取";
                $scope.$apply();
            }
        };

        setTimeout(function(){
            $("a").on("touchstart", function(e){
                $(e.target).addClass('box-shadow-style');
            });
            $("a").on("touchmove", function(e){
                $(e.target).removeClass('box-shadow-style');
            });
            $("a").on("touchend", function(e){
                $(e.target).removeClass('box-shadow-style');
            });
        }, 500);

        $scope.do_send_mobile_code = function () {
            if(isNull($scope.mobile)){
                $rootScope.showAlert("请输入手机号码");
                return;
            } else if($scope.mobile.length != 11){
                $rootScope.showAlert("请输入有效的手机号码");
                return;
            } else {
                var reg = /^(1[0-9]{10})$/;
                if(!reg.test($scope.mobile)){
                    $rootScope.showAlert("请输入有效的手机号码");
                    return;
                }
            }
            showLoading();
            var partner = "ZLAPITOH5WAP";
            var timestamp = Date.parse(new Date())/1000;
            var sign = $.md5(partner + timestamp + key);
            $http({method: 'POST', data: {partner: partner, timestamp: timestamp, sign: sign, mobile: $scope.mobile}, url: "../index.php/yunapi/member/do_send_mobile_code"})
                .success(function (response, status, headers, config) {
                    /*var str = JSON.stringify(response);
                     $rootScope.showAlert(str);*/
                    if (response.status == "1") {
                        $scope.checked = true;
                        $scope.nowtime = 60;
                        $scope.shut = setInterval($scope.setTime,1000);
                        $rootScope.showAlert("验证码发送成功");
                    } else {
                        $rootScope.showAlert(response.message);
                    }
                    hideLoading();
                })
                .error(function (response, status, headers, config) {
                    hideLoading();
                });
        };

        $scope.yaoqing = $location.search()['yaoqing'];

        $scope.do_register = function ($event) {
            /*$($event.target).css('color', '#f81000');
             setTimeout(function(){
             $($event.target).css('color', '#0079fe');
             }, 160);*/
            var checkbox = document.getElementById("optionsRigister");
            if(!checkbox.checked){
                $rootScope.showAlert("您还没有同意服务协议");
                return;
            }
            if(isNull($scope.mobile)){
                $rootScope.showAlert("请输入手机号码");
                return;
            } else if($scope.mobile.length != 11){
                $rootScope.showAlert("请输入有效的手机号码");
                return;
            } else {
                var reg = /^(1[0-9]{10})$/;
                if(!reg.test($scope.mobile)){
                    $rootScope.showAlert("请输入有效的手机号码");
                    return;
                }
            }
            if(isNull($scope.password)){
                $rootScope.showAlert("请输入密码");
                return;
            }
            if($scope.password.length < 6 || $scope.password.length > 20){
                $rootScope.showAlert("密码长度不在规定范围内");
                return;
            }
            if(isNull($scope.inputCode)){
                $rootScope.showAlert("请输入验证码");
                return;
            }
            if(isNull($scope.yaoqing)){
                $scope.yaoqing = "";
            }
            showLoading();
            var partner = "ZLAPITOH5WAP";
            var timestamp = Date.parse(new Date())/1000;
            var sign = $.md5(partner + timestamp + key);
            $http({method: 'POST', data: {partner: partner, timestamp: timestamp, sign: sign, mobile: $scope.mobile, password: $scope.password, code: $scope.inputCode, yaoqing: $scope.yaoqing,su:su,sk:sk}, url: "../index.php/yunapi/member/do_register"})
                .success(function (response, status, headers, config) {
                    if (response.status == "1") {
                        $("#rigisterModal").modal();
                    } else {
                        $rootScope.showAlert(response.message);
                    }
                    hideLoading();
                })
                .error(function (response, status, headers, config) {
                    hideLoading();
                });
        };
    })

});