/**
 * Created by Administrator on 2016/8/8.
 */
define(['./module','./indexCtrl'], function (controllers) {
    'use strict';
    //购单列表
    controllers.controller('shopListCtrl', function ($scope, $http, $location, $rootScope) {
        document.title = "云购单";
        $('body').scrollTop(0);
        $("#headerId").hide();
        $("#body_top").css('height','0px');
        $("#shopList").hide();

        if($rootScope.shopList.length == 0){
            $scope.isEmpty = true;
        }else{
            $scope.isEmpty = false;
        }
        setTimeout(function(){
            $("a").on("touchstart", function(e){
                $(e.target).addClass('box-shadow-style');
            });
            $("a").on("touchmove", function(e){
                $(e.target).removeClass('box-shadow-style');
            });
            $("a").on("touchend", function(e){
                $(e.target).removeClass('box-shadow-style');
            });
        }, 500);

        $scope.mScroll = "";
        $scope.initIScroll = function(){
            if($scope.mScroll instanceof iScroll){
                $scope.mScroll.destroy();
            }
            $("#shop_content").height($(window).height()-90);

            $scope.mScroll = new iScroll("shop_content",{
                hScrollbar:false,
                vScrollbar:false,
                onScrollMove: function(){

                },
                onScrollEnd: function(){
                    if(this.y < -800){
                        $('#proId').show();
                    }else{
                        $('#proId').hide();
                    }
                }
            });
        };
        $('#proId').click(function(){
            $scope.mScroll.scrollTo(0, 0, 0);
        });

        setTimeout(function(){$scope.initIScroll();},100);

        $scope.price = 0;
        for(var i = 0; i < $rootScope.shopList.length; i++){
            $scope.price += $rootScope.shopList[i].num;
            if(isNaN($scope.price)){
                $scope.price = 0;
            }
        }

        $scope.setNumber = function (index) {
            var xiangou_number = $rootScope.shopList[index].wap_ten;			//限购数量

            //console.log(parseInt($rootScope.shopList[index].num));
            if($rootScope.shopList[index].num && $rootScope.shopList[index].num < 1){
                $rootScope.shopList[index].num = 1;
            }else if($rootScope.shopList[index].num > xiangou_number){
                $rootScope.shopList[index].num = xiangou_number;
            }
            $scope.price = 0;
            for(var i = 0; i < $rootScope.shopList.length; i++){
                $scope.price += parseInt($rootScope.shopList[i].num);
                if(isNaN($scope.price)){
                    $scope.price = 0;
                }
            }
            addCookie("shopList", JSON.stringify($rootScope.shopList), 0);
        };

        $scope.backTo = function(){
            window.history.go(-1);
        };

        $scope.plus = function(index){
            var xiangou_number = $rootScope.shopList[index].wap_ten;//限购数量
            //var syrs = $rootScope.shopList[index].zongrenshu - $rootScope.shopList[index].canyurenshu;
            //console.log(parseInt($rootScope.shopList[index].num));
            if(($rootScope.shopList[index].num + 1) <= xiangou_number){
                $rootScope.shopList[index].num++;
                $scope.price ++;
            }else{
                $rootScope.shopList[index].num = xiangou_number;
            }

            addCookie("shopList", JSON.stringify($rootScope.shopList), 0);
        };
        $scope.subtract = function(index){

            if($rootScope.shopList[index].num - 1 > 0){
                $rootScope.shopList[index].num--;
                $scope.price--;
            }
            addCookie("shopList", JSON.stringify($rootScope.shopList), 0);
        };
        $scope.del = function(index){
            $scope.price = $scope.price - $rootScope.shopList[index].num;
            $rootScope.shopList.splice(index, 1);
            if($rootScope.shopList.length == 0){
                $scope.isEmpty = true;
            }else{
                $scope.isEmpty = false;
            }
            addCookie("shopList", JSON.stringify($rootScope.shopList), 0);
        };

        $scope.addToCart = function () {
            if($rootScope.holdUID>0){
                if($scope.isEmpty){
                    return;
                }
                if(isNull($rootScope.holdUID)){
                    $location.url("/tab/login");
                } else {
                    var shoplist = eval('('+getCookie('shopList')+')');
                    for(var i=0;i<shoplist.length;i++){
                        console.log(shoplist[i].wap_ten);//return false;
                        if(shoplist[i].num>0){

                        }else{
                            $rootScope.showAlert("请确认您填写的商品数量是否合法！");return false;
                        }
                        //console.log($rootScope.shopList);
                        if(shoplist[i].num > shoplist[i].wap_ten){
                            $rootScope.shopList[i].num = shoplist[i].wap_ten;
                            addCookie("shopList", JSON.stringify(shopList));
                            $rootScope.showAlert('您购物车中有商品达到购买上限，系统已将其纠正！');
                            $location.url("/tab/shoplist");
                            return false;
                        }
                    }
                    $location.url("/tab/pay");
                }
            }else{
                $rootScope.showAlert("请到右上角先登录!");return false;
            }
        };

    })

});
