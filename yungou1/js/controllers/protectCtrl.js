/**
 * Created by Administrator on 2016/8/8.
 */
define(['./module','./indexCtrl'], function (controllers) {
    'use strict';
    //网络游戏和个人信息保护
    controllers.controller('protectCtrl', function ($scope, $http, $location, $rootScope) {		//ios需要的网络游戏和个人信息保护
        document.title = "信息保护";
        $('body').scrollTop(0);
        $("#headerId").hide();
        $("#body_top").css('height','0px');
        $("#shopList").hide();
        $scope.backTo = function(){
            window.history.go(-1);
        };
        $("#footbar").hide();
    })

});
