/**
 * Created by Administrator on 2016/8/8.
 */
define(['./module','./indexCtrl'], function (controllers) {
    'use strict';
    //隐私申明
    controllers.controller('privacyCtrl', function ($scope, $http, $location, $rootScope) {		//ios需要的隐私声明
        document.title = "隐私声明";
        $('body').scrollTop(0);
        $("#headerId").hide();
        $("#body_top").css('height','0px');
        $("#shopList").hide();
        $scope.backTo = function(){
            window.history.go(-1);
        };
        $("#footbar").hide();
    })

});
