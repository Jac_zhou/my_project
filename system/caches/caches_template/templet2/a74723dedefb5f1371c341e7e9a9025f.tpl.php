<?php defined('G_IN_SYSTEM')or exit('No permission resources.'); ?><?php include templates("index","header");?>
<?php include templates("member","member_top");?>
<link rel="stylesheet" type="text/css" href="<?php echo G_TEMPLATES_STYLE; ?>/css/new/Newmember_style.css"/>
<div class="memberxhcontains">
	<?php include templates("member","member_nav");?>
	<?php if(count($record)==0): ?>
		<div class="member_noticle_tishi">还没有中奖记录，赶快<a href="<?php echo WEB_PATH; ?>" target="_blank" class="membercolor_blur01">去参加</a>吧！</div>
	<?php  else: ?>
	<!--搜索-->
    <?php include templates("member","search_t");?>
	<script type="text/javascript">
	var _action = 'all';
	$(".memberxhcontains_serachl span").click(function(){
		var _index = $(this).index();
		if(_index == 1){
			 _action = 'today';
		}else if(_index == 2){
			 _action = 'weekend';
		}else if(_index == 3){
			 _action = 'month';
		}else{
			 _action = 'all';
		}
		$(".memberxhcontains_serachl span").removeClass("current");
		$(this).addClass("current");
		window.location.href = "<?php echo WEB_PATH; ?>/member/home/orderlist/"+_action;
	});
	</script>
    <!--中奖记录-->
	
    <div class="member_user_winner">
    	<ul>
		<?php $ln=1;if(is_array($record)) foreach($record AS $recd): ?>
        	<li style="height:420px">
            	<div class="member_user_winnerimg member_user_winnerimg-Modify"><a href="<?php echo WEB_PATH; ?>/dataserver/<?php echo $recd['shopid']; ?>" target="_blank"><img src="<?php echo G_UPLOAD_PATH; ?>/<?php echo yunjl($recd['shopid']); ?>" width="200px" height="150px"/></a></div>
            	<h4><a href="<?php echo WEB_PATH; ?>/dataserver/<?php echo $recd['shopid']; ?>" class="membercolor_blur01 xh_overflow_x" target="_blank"><?php echo $recd['shopname']; ?></a></h4>
                <div class="member_user_winnerinfo">
                	<p>总需：<?php echo $recd['zongrenshu']; ?>人次</p>
					<p>期号：第（<?php echo $recd['shopqishu']; ?>）期</p>
                    <p>幸运号码：<span class="membercolor_red01"><?php echo $recd['huode']; ?></span></p>
                    <p>总共参与：<?php echo get_user_goods_num($recd['uid'],$recd['shopid']); ?>人次</p>
                    <p>揭晓时间：<?php echo microt($recd['q_end_time']); ?></p>
					<p class="order_status">状态：
						<?php              
							$status=@explode(",",$recd['status']);               
							if($status[2]=='未完成' || $status[2]=='待收货'){       	
								if($status[1]=='未发货'){ 
                                    if(!$recd['confrim_addr'] || !$recd['confrim_addr_time']){
										echo "<span  class='membercolor_red01'>请确认收货地址</span>&nbsp;&nbsp;&nbsp;&nbsp;<a style='text-decoration:underline !important' class='membercolor_blur01' href='".WEB_PATH."/member/home/singledetail/".$recd["id"]."'>去确认</a>";
									}else{
										echo "<span  class='membercolor_red01'>等待发货</span>";
									}								
									
								} 
								if($status[1]=='已发货'){
									echo "<span  class='membercolor_red01'>已发货</span>";
									echo '<a  href="javascript:void(0)" class="member_personinfo_comfire" oid="'.$recd['id'].'" uid="'.$recd['uid'].'">确认收货</a>';                      
								}  
							} 
							if($status[2]=='已完成'){
								echo "<span  class='membercolor_red01'>已完成</span>";
							}   
							if($status[2]=='已作废'){
								echo "<span  class='membercolor_red01'>已作废</span>";
							}          
						 ?>
					</p>
					<div style='height:10px;'></div>
					<p>
						<a href="<?php echo WEB_PATH; ?>/member/home/singledetail/<?php echo $recd['id']; ?>" class="member_xiangqing">详情</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<?php if($status[2]=='已完成'): ?>
						<?php if(!is_shaidan($recd['uid'],$recd['shopid'])): ?>
						<a href="javascript:void(0)" class="member_xiangqing member_confirm_shaidan" oid="<?php echo $recd['id']; ?>">晒单</a>
						<?php endif; ?>
						<?php endif; ?>
						
					</p>
					
					<input type="hidden" name="record_id" value="<?php echo $recd['id']; ?>"/>
                
                </div>
            </li>
		<?php  endforeach; $ln++; unset($ln); ?>
        </ul>
    
    </div>
	<?php if($total>12): ?>
    <style>
		#divPageNav{width:96%; text-align:right;padding-top:10px;}
		#divPageNav ul{ text-align:center;float:right}
		#divPageNav li a{ padding:5px 8px;}
	</style>
    <div id="divPageNav" class="page_nav">
        <?php echo $page->show('two'); ?>
    </div>
	<?php endif; ?>
	<?php endif; ?>
</div>
<link rel="stylesheet" type="text/css" href="<?php echo G_TEMPLATES_CSS; ?>/new/alert.css"/>
<script type="text/javascript" src='<?php echo G_TEMPLATES_JS; ?>/new/alert.js'></script>
<script type="text/javascript">
/*2015/12/010确定收货*/
$(".member_personinfo_comfire").click(function(){
	var currr_obj = $(this);
	var _html = '';
	msg_show.promptmsg("您确定收到货了吗？",{"yes":function(){
		var oid = currr_obj.attr('oid');
		var uid = currr_obj.attr('uid');
		var rurl = '<?php echo WEB_PATH; ?>/api/dingdan/set';
		$.ajax({
			url:rurl,
			data:{oid:oid,uid:uid},
			type:"POST",
			success:function(_data){
				_data=$.parseJSON(_data);
				if(_data.error=="1"){
					msg_show.waring(_data.msg);
				}else if(_data.error=="0"){
					 window.location.href= _data.url;
				}
				return false;
			}
		});
	
	}});
});

//进入添加晒单
//添加晒单
$(".member_confirm_shaidan").click(function(){
	var recover_id = $(this).attr("oid");
	var rurl = '<?php echo WEB_PATH; ?>/member/home/addsinglestatus';
	$.ajax({
		url:rurl,
		data:{recover_id:recover_id,action:'page'},
		type:"POST",
		success:function(data){
			var _data=$.parseJSON(data);
			if(_data.error == "1"){
				msg_show.waring(_data.msg);
			}else if(_data.error == "0"){
				//msg_show.success(_data.msg,_data.url);
				window.location.href = _data.url;
			}
		}
	});
	return false;
});

//进入物流详情

</script>

<?php include templates("index","footer");?>