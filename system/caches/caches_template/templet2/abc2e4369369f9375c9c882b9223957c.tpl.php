<?php defined('G_IN_SYSTEM')or exit('No permission resources.'); ?><?php include templates("index","header");?>
<?php include templates("member","member_top_visit");?>
<link rel="stylesheet" type="text/css" href="<?php echo G_TEMPLATES_STYLE; ?>/css/layout-records.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo G_TEMPLATES_STYLE; ?>/css/layout-Frame.css"/>
<div class="memberxhcontains">
	<?php include templates("member","member_nav_visit");?>
    <div class="R-content" >
        <div class="member-t" style="width:96%; margin:30px auto;"><h2>夺宝记录</h2></div>
        <?php 
        $jiexiao = get_shop_if_jiexiao($shopinfo['id']);

         ?>
        <?php if($jiexiao['q_uid']): ?>
        <div class="yg_record_goods" style="width:96%; margin:0 auto;">
            <a href="<?php echo WEB_PATH; ?>/dataserver/<?php echo $shopinfo['id']; ?>" target="_blank" class="fl-img"><img src="<?php echo G_UPLOAD_PATH; ?>/<?php echo $shopinfo['thumb']; ?>"></a>
            <div class="yg_record_r">
                <li><span class="orange">(第<?php echo $record['shopqishu']; ?>期)</span> <a target="_blank" href="<?php echo WEB_PATH; ?>/dataserver/<?php echo $shopinfo['id']; ?>_<?php echo $shopinfo['qishu']; ?>" class="blue"><?php echo $jiexiao['title']; ?></a></li>
                <li class="gray02">本期商品夺宝已结束 【获得者：<a href="<?php echo WEB_PATH; ?>/uname/<?php echo idjia($member['uid']); ?>" target="_blank" class="blue">
                    <?php echo get_user_name($jiexiao['q_user']); ?></a>
                    幸运编号：<?php echo $jiexiao['q_user_code']; ?> 】</li>
                <li><a target="_blank" href="<?php echo WEB_PATH; ?>/dataserver/<?php echo $shopinfo['id']; ?>" class="bluebut">查看详情</a></li></div>
        </div>
        <?php  else: ?>
        <div class="yg_record_goods" style="width:96%; margin:0 auto;">
            <a href="<?php echo WEB_PATH; ?>/goods/<?php echo $shopinfo['id']; ?>" target="_blank" class="fl-img"><img src="<?php echo G_UPLOAD_PATH; ?>/<?php echo $shopinfo['thumb']; ?>"></a>
            <div class="yg_record_r">
                <li><span class="orange">(第<?php echo $shopinfo['qishu']; ?>期)</span> <a target="_blank" href="<?php echo WEB_PATH; ?>/goods/<?php echo $shopinfo['id']; ?>" class="blue"><?php echo $jiexiao['title']; ?></a></li>
                <li><a target="_blank" href="<?php echo WEB_PATH; ?>/goods/<?php echo $shopinfo['id']; ?>" class="bluebut">查看详情</a></li></div>
        </div>
        <?php endif; ?>
        <div class="goods-tit gray02" style="width:96%; margin:10px auto;">
            <p class="fl">本期商品您总共夺宝<span><?php echo get_user_goods_num($member['uid'],$shopinfo['id']); ?></span>人次 拥有<span><?php echo get_user_goods_num($member['uid'],$shopinfo['id']); ?></span>个幸运码</p>
        </div>


        <div class="list-tab goodsList" id="userbuylist">
            <ul class="listTitle">
                <li style="width:15%; text-align:left">夺宝时间</li>
                <li style="width:10%; text-align:center">夺宝人次</li>
                <li style="width:65%; text-align:left">夺宝号</li>
            </ul>
            <?php $ln=1;if(is_array($record)) foreach($record AS $rec): ?>
            <ul>
                <li style="width:15%; text-align:left"><?php echo microt($rec['time']); ?></li>
                <li style="width:10%; text-align:center"><?php echo $rec['gonumber']; ?>人次</li>
                <li class="Code" style="width:65%; text-align:left"><p style="overflow: auto"><?php echo $rec['goucode']; ?></p></li>
            </ul>
            <?php  endforeach; $ln++; unset($ln); ?>

        </div>

        <div class="goods-tit gray02" style="width:96%; margin:0 auto;">
            <a href="<?php echo WEB_PATH; ?>/member/home/userbuylist" class="blue fr" >&lt;&lt; 返回列表</a></div>
    </div>

</div>
<?php include templates("index","footer");?>