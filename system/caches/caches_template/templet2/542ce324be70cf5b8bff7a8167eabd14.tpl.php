<?php defined('G_IN_SYSTEM')or exit('No permission resources.'); ?><?php include templates("index","header");?>
<link rel="stylesheet" type="text/css" href="<?php echo G_TEMPLATES_STYLE; ?>/css/GoodsDetail.css"/>

<link rel="stylesheet" type="text/css" href="<?php echo G_TEMPLATES_STYLE; ?>/js/cloud-zoom.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo G_TEMPLATES_STYLE; ?>/css/qishu_list.css"/>
<style type="text/css">
	.zoom-section {
		clear: both;
		margin-top: 20px;
	}
	
	.zoom-small-image {
		border: 2px solid #dedede;
		float: left;
		margin-bottom: 10px;
		width: 400px;
		height: 400px;
	}
	
	.zoom-small-image img {
		width: 400px;
		height: 400px;
	}
	
	.zoom-desc {
		float: left;
		width: 404px;
		height: 63px;
		margin-bottom: 20px;
		overflow: hidden;
	}
	
	.zoom-desc p {
		width: 10000px;
		height: 63px;
		float: left;
		display: block;
		position: absolute;
		top: 0;
		z-index: 3;
		overflow: hidden;
	}
	
	.zoom-desc label {
		width: 50px;
		height: 63px;
		margin: 0 5px 0 0;
		margin-right: 4px;
		display: block;
		float: left;
		overflow: hidden;
	}
	
	.zoom-tiny-image {
		margin: 0px;
		margin-top: 10px;
		width: 48px;
		height: 50px;
		border: 1px solid #CCC;
	}
</style>
<div class="Current_nav"> <a href="<?php echo WEB_PATH; ?>">首页</a> <span>&gt;</span> <a href="<?php echo WEB_PATH; ?>/goods_list/<?php echo $item['cateid']; ?>"> <?php echo $category['name']; ?> </a><span>&gt;</span> <a href="<?php echo WEB_PATH; ?>/goods_list/<?php echo $item['cateid']; ?>e<?php echo $item['brandid']; ?>"> <?php echo $brand['name']; ?> </a> <span>&gt;</span>商品详情 </div>
<?php include templates("index","qishu_list");?>
<div class="show_content"> 
 
  <!-- 商品信息 -->
  <div class="Pro_Details">
    <h1><span>(第<?php echo $item['qishu']; ?>期)</span><span ><?php echo $item['title']; ?></span><span style="<?php echo $item['title_style']; ?>"><?php echo $item['title2']; ?></span></h1>
    <div class="Pro_Detleft">
      <div class="zoom-small-image"> 
      	
      	<span href="<?php echo G_UPLOAD_PATH; ?>/<?php echo $item['thumb']; ?>" class = 'cloud-zoom' id='zoom1' rel="adjustX:10, adjustY:-2"> 
      		<?php if($item['is_shi']==1): ?>
      		<div style="position:absolute;z-index: 10000;width:50px;height:50px;background: #000000;background: url(<?php echo G_TEMPLATES_IMAGE; ?>/shiyuan.png) no-repeat;background-size: 100%;"></div>
                <?php elseif ($item['pk_product']==1): ?>
                
                <div style="position:absolute;z-index: 10000;width:50px;height:50px;background: #000000;background: url(<?php echo G_TEMPLATES_IMAGE; ?>/PK6.png) no-repeat;background-size: 100%;"><span class="pk_numer" style="color: #fff;font-size: 20px;margin-left: 10px;"><?php echo $item['pk_number']; ?></span> </div>
      		<?php endif; ?>
      		<img width="80px" height="80px" src="<?php echo G_UPLOAD_PATH; ?>/<?php echo $frist_one_arr['0']; ?>" class="mousetrap"/>
      	</span> 
      </div>
      <div class="zoom-desc">
        <div class="jcarousel-prev jcarousel-prev-disabled"></div>
        <div class="jcarousel-clip" style="height:63px;width:384px;">
          <p> <?php $ln=1;if(is_array($item['picarr'])) foreach($item['picarr'] AS $imgtu): ?>
            <label href="<?php echo G_UPLOAD_PATH; ?>/<?php echo $imgtu; ?>" class='cloud-zoom-gallery'  rel="useZoom: 'zoom1', smallImage: '<?php echo G_UPLOAD_PATH; ?>/<?php echo $imgtu; ?>'"> <img class="zoom-tiny-image" src="<?php echo G_UPLOAD_PATH; ?>/<?php echo $imgtu; ?>" /></label>
            <?php  endforeach; $ln++; unset($ln); ?> </p>
        </div>
        <div class="jcarousel-next jcarousel-next-disabled"></div>
      </div>
      <script>$(function() {
	$(".jcarousel-clip").find("label").eq(0).addClass("xh_xiaotu");
	$(".zoom-tiny-image").eq(0).css({
		"border": "1px solid #db3652",
		// "border-bottom": "none"
	});

	$(".cloud-zoom-gallery").hover(function() {
		$(".jcarousel-clip").find(".cloud-zoom-gallery").removeClass("xh_xiaotu");
		$(".cloud-zoom-gallery").find("img").css({
			"border": "1px solid #CCC"
		});
		$(this).addClass("xh_xiaotu");
		$(this).find("img").css({
			"border": "none",
			"border": "1px solid #db3652",
			// "border-bottom": "none"
		});

		$("#zoom1 img").attr("src", $(this).find("img").attr("src"));
	});
});</script>
      <script>
				var si=$(".jcarousel-clip label").size();
				var label=si*55;
				$(".jcarousel-clip p").css({width:label,left:"0"});
				if(label>395){
					$(".jcarousel-prev,.jcarousel-next").show();
				}else{
					$(".jcarousel-prev,.jcarousel-next").hide();
				}
				$(".jcarousel-prev").click(function(){
					var le=$(".jcarousel-clip p").css("left");
					var le2=le.replace(/px/,"");
					if(le!='0px'){
						$(".jcarousel-clip p").css({left:le2*1+55});
					}						
				})
				$(".jcarousel-next").click(function(){
					var le=$(".jcarousel-clip p").css("left");
					var le2=le.replace(/px/,"");
					var max_next=-(si-7)*55+"px";
					if(le!=max_next){						
						$(".jcarousel-clip p").css({left:le2*1-55});
					}
				})
			</script> 
      </div>
    <div class="Pro_Detright">
      <p class="Det_money">价值：<span class="rmbgray"><?php echo $item['money']; ?></span></p>
      <!--显示揭晓动画 start--> 
      <?php if(($q_showtime=='Y')): ?>
      <?php include templates("index","item_animation");?>
      <?php  else: ?>
      <?php include templates("index","item_contents");?>
      <?php endif; ?> 
      <!--显示揭晓动画 end-->
      <div class="Security">
        <ul>
          <li><a href="<?php echo WEB_PATH; ?>/help/4" target="_blank"><i></i>100%公平公正</a></li>
          <li><a href="<?php echo WEB_PATH; ?>/help/5" target="_blank"><s></s>100%正品保证</a></li>
          <li><a href="<?php echo WEB_PATH; ?>/help/7" target="_blank"><b></b>全国免费配送</a></li>
        </ul>
      </div>
      <div class="Pro_Record">
		<ul id="ulRecordTab" class="Record_tit">
			<li class="NewestRec Record_titCur">最新云购记录</li>
			<li class="MytRec">我的云购记录</li>
		</ul>
		<div class="Newest_Con hide">
			<ul>
				<?php $ln=1;if(is_array($us)) foreach($us AS $user): ?>
				<li>
				<a href="<?php echo WEB_PATH; ?>/uname/<?php echo idjia($user['uid']); ?>" target="_blank">
				<?php if(!empty($user['uphoto'])): ?>
					<img src="<?php echo G_UPLOAD_PATH; ?>/<?php echo $user['uphoto']; ?>" border="0" alt="" width="20" height="20">
				<?php  else: ?>
					<img src="<?php echo G_UPLOAD_PATH; ?>/photo/member.jpg" border="0" alt="" width="20" height="20">
				<?php endif; ?>
				</a>					
				<a href="<?php echo WEB_PATH; ?>/uname/<?php echo idjia($user['uid']); ?>" target="_blank" class="blue"><?php echo $user['username']; ?></a>
				<?php if($user['ip']): ?>
				(<?php echo get_ip($user['id'],'ipcity'); ?>) 
				<?php endif; ?>				
				<?php echo _put_time($user['time']); ?> 云购了
				<em class="Fb gray01"><?php echo $user['gonumber']; ?></em>人次</li>
				<?php  endforeach; $ln++; unset($ln); ?>
			</ul>
			<p style=""><a id="btnUserBuyMore" href="javascript:;" class="gray01">查看更多</a></p>					
		</div>
		
		<?php 
			$is_uid = get_user_uid() ? : '';
		 ?>
		
		<!--我的云购记录-->
		<div class="My_Record hide" style="display:none;">
			<?php if(get_user_uid()): ?>				
			<ul>				
							<?php $mod_member_member = System::load_app_model('member','member');$datas = $mod_member_member->get_record(get_user_uid(),$item['id'],5); ?>	
				<?php $ln=1;if(is_array($datas)) foreach($datas AS $row): ?>									
				<li><?php echo _put_time($row['time']); ?>  云购了  <?php echo $row['gonumber']; ?>  个云购号</li>						
				<?php  endforeach; $ln++; unset($ln); ?> 
			</ul>
			<?php  else: ?>					
			<div class="My_RecordReg">
				<b class="gray01">看不到？是不是没登录或是没注册？ 登录后看看</b>
				<a href="<?php echo WEB_PATH; ?>/member/user/login" class="My_Signbut">登录</a><a href="<?php echo WEB_PATH; ?>/member/user/register" class="My_Regbut">注册</a>
			</div>
			<?php endif; ?>
		</div>
		<!--/我的云购记录-->

	</div>	
			
    </div>
    
	
    <?php include templates("index","first_qi");?>   
    
    
  </div>
</div>
<!-- 商品信息导航 -->
<div class="ProductTabNav">
  <div id="divProductNav" class="DetailsT_Tit">
    <div class="DetailsT_TitP">
      <ul>
        <li class="Product_DetT DetailsTCur"><span class="DetailsTCur">商品详情</span></li>
        <li id="liUserBuyAll" class="All_RecordT"><span class="">所有参与记录</span></li>
        <li class="Single_ConT"><span class="">晒单</span></li>
      </ul>
      <!-- <p><a id="btnAdd2Cart" href="javascript:;" class="white DetailsT_Cart"><s></s>加入购物车</a></p> --> 
    </div>
  </div>
</div>

<!--补丁3.1.6_b.0.1-->
<div id="divContent" class="Product_Content"> 
  <!-- 商品内容 -->
  <div class="Product_Con">
	<?php echo $item['content']; ?>
		<dl class="tishi">
			<dt class="tishi_tit">温馨提示：</dt>
			<dd class="tishi_content">
					1、以上详情页面展示信息仅供参考，商品以实物为准。<br/>
					2、非质量问题，不在三包范围内，不给予退换货。<br/>
					3、请尽量亲自签收并当面拆包验货，若发现物流途中造成了商品的损坏，请不要签收，可以拒签退回。<br/>
					4、如快递无法配送至获奖者提供的送货地址，将默认配送到距离最近的送货地，由获奖者本人自提。<br/>
			</dd>
		</dl>
  </div>
  <!-- 商品内容 --> 
  
  <!-- 购买记录20条 -->
  <div id="bitem" class="AllRecordCon" style="display:none;">
    <iframe id="iframea_bitem" g_src="<?php echo WEB_PATH; ?>/go/goods/go_record_ifram/<?php echo $itemid; ?>/20" style="width:978px; border:none;height:100%" frameborder="0" scrolling="no"></iframe>
  </div>
  <!-- /购买记录20条 --> 
  
  <!-- 晒单 -->
  <div id="divPost" class="Single_Content" style="display:none;">
    <iframe id="iframea_divPost" g_src="<?php echo WEB_PATH; ?>/go/shaidan/itmeifram/<?php echo $itemid; ?>" style="width:978px; border:none;height:100%" frameborder="0" scrolling="no"></iframe>
  </div>
  <!-- 晒单 --> 
</div>
<div id="fbar" style="background:#fff;">
	<dl>
		<dt><i class="zhengpin"></i><strong>正品行货</strong></dt>
		<dd>易中夺宝所有商品均来自官方/正规渠道，100%正品保障，保证所有商品均为正品行货。</dd>
		<dt><i class="lianbao"></i><strong>全国联保</strong></dt>
		<dd>发票、质保证书等相关材料由各品牌官方/正规渠道直接提供，中奖用户可享受品牌厂商提供的全国联保等售后服务。</dd>
		<dt><i class="anquan"></i><strong>物流安全</strong></dt>
		<dd>商品通过京东、顺丰等物流方式送达中奖用户，物流安全有保障；商品发货后，中奖用户可通过易中夺宝用户中心查询物流单号，并据此追踪物流进程。 </dd>
	</dl>
</div>
<!--补丁3.1.6_b.0.1--> 

<!--登录窗口开始-->
<?php include templates("user","login_ajax");?>  
<!--登录窗口结束-->
<!--2015/12/04-->

<script type="text/javascript">

function set_iframe_height(fid,did,height){	
	$("#"+fid).css("height",height);
}
var user_can_buy_number="<?php echo $user_can_buy_number; ?>";//当前产品用户能购买的次数
var is_shi = "<?php echo $item['is_shi']; ?>";
$(function(){
	$("#ulRecordTab li").click(function(){
		var add=$("#ulRecordTab li").index(this);
		$("#ulRecordTab li").removeClass("Record_titCur").eq(add).addClass("Record_titCur");
		$(".Pro_Record .hide").hide().eq(add).show();
	});
	
	var DetailsT_TitP = $(".DetailsT_TitP ul li");
	var divContent    = $("#divContent div");	
	DetailsT_TitP.click(function(){
		var index = $(this).index();
			DetailsT_TitP.removeClass("DetailsTCur").eq(index).addClass("DetailsTCur");
	
			var iframe = divContent.hide().eq(index).find("iframe");
			if (typeof(iframe.attr("g_src")) != "undefined") {
			  	 iframe.attr("src",iframe.attr("g_src"));
				 iframe.removeAttr("g_src");
			}
			divContent.hide().eq(index).show();
	});

	$("#btnUserBuyMore").click(function(){
		DetailsT_TitP.removeClass("DetailsTCur").eq(1).addClass("DetailsTCur");
			var iframe = divContent.hide().eq(1).find("iframe");
			if (typeof(iframe.attr("g_src")) != "undefined") {
			  	 iframe.attr("src",iframe.attr("g_src"));
				 iframe.removeAttr("g_src");
			}
			divContent.hide().eq(1).show();		
		$("html,body").animate({scrollTop:750},1500);
		//$("#divProductNav").addClass("nav-fixed");
	});
	$(window).scroll(function(){
		if($(window).scrollTop()>=941){
			$("#divProductNav").addClass("nav-fixed");
		}else if($(window).scrollTop()<941){
			$("#divProductNav").removeClass("nav-fixed");
		}
	});
})
var shopinfo={'shopid':"<?php echo $item['id']; ?>",'money':"<?php echo $item['yunjiage']; ?>",'shenyu':"<?php echo $syrs; ?>",};
var _xhtml = "参与人次越多，获奖机会越大哦！";

$(function(){
	function baifenshua(aa,n){
		n = n || 2;
		return ( Math.round( aa * Math.pow( 10, n + 2 ) ) / Math.pow( 10, n ) ).toFixed( n ) + '%';
	}
	var shopnum = $("#num_dig");
	shopnum.keyup(function(){
		var numshop=shopnum.val();
		if(numshop<1){
			shopnum.val(1);
		}z
		if(numshop>user_can_buy_number){
			shopnum.val(user_can_buy_number);
		}
		
		if(is_shi==1 && numshop>user_can_buy_number){
			shopnum.val(user_can_buy_number);
			_xhtml = "参与人次最多5人";
		}
		if(is_shi==0){
			if(numshop >= 1 && numshop<=4){
				_xhtml = "参与人次越多，获奖机会越大哦！";
			}else if(numshop > 4 && numshop<=10){
				_xhtml = "参与10人次，极有机会获奖!";
			}else if(parseInt(numshop) > 10){
				_xhtml = "加大参与人次，云购在望";
			}
		}
		$("#chance").html(_xhtml).css({"color":"red"});
	});	
	
	$("#shopadd").click(function(){
		var shopnum = $("#num_dig");
		var num = parseInt(shopnum.val());
		num++;
		
		if(num >user_can_buy_number){				
			shopnum.val(user_can_buy_number);
		}else{
			shopnum.val(num);
		}
		if(is_shi == 1){
			_xhtml = "本产品限购5人次";
		}else{
			if(num >= 1 && num<=4){
				_xhtml = "参与人次越多，获奖机会越大哦！";
			}else if(num > 4 && num<=10){
				_xhtml = "参与10人次，极有机会获奖!";
			}else if(num > 10){
				_xhtml = "加大参与人次，云购在望";
			}
		}	
		$("#chance").html(_xhtml).css({"color":"#e53c08"});
	});
	$("#shopsub").click(function(){
		var shopnum = $("#num_dig");
		var num = parseInt(shopnum.val());
		
		if(num-1<1){
			shopnum.val(1);
		}else{
			shopnum.val(num-1);
		}
		num  =	parseInt(shopnum.val());
		
		if(is_shi == 0){
			if(num >= 1 && num<=4){
				_xhtml = "参与人次越多，获奖机会越大哦！";
			}else if(num > 4 && num<=10){
				_xhtml = "参与10人次，极有机会获奖!";
			}else if(num > 10){
				_xhtml = "加大参与人次，云购在望";
			}
		}else{
			_xhtml = "本产品限购5人次";
		}
		/*2015/12/4结束*/
		$("#chance").html(_xhtml).css({"color":"#e53c08"});
	});

});

$(function(){
$(".xDet_Cart").click(function(){ 
	//添加到购物车动画
	
	//判断购物该商品剩余人数是否为0
	if($("#CodeLift").text() <= 0){
		return false;
	}

	var src=$("#zoom1 img").attr('src');  
	var $shadow = $('<img id="cart_dh" style="display: none; border:1px solid #aaa; z-index: 99999;" width="400" height="400" src="'+src+'" />').prependTo("body"); 
	var $img = $(".mousetrap");
	$shadow.css({ 
	   'width' : $img.css('width'), 
	   'height': $img.css('height'),
	   'position' : 'absolute',      
	   'top' : $img.offset().top,
	   'left' : $img.offset().left, 
	   'opacity' :1    
	}).show();
	var $cart =$("#btnMyCart");	
	$shadow.animate({   
		width: 1, 
		height: 1, 
		top: $cart.offset().top, 
		left: $cart.offset().left,
		opacity: 0
	},500,function(){
		Cartcookie(false);
	});		
});
     xgoods_number();
	 
	$(".xDet_Shopbut").click(function(){
		if( "<?php echo $arr_member_info['uid']; ?>" == ''){
			ajax_login();return false;
		}else{
			Cartcookie(true);
		}		
		
	});	
});

/*2015/12/06判断商品的数量是否正确*/
function xgoods_number(){
  $("#num_dig").blur(function(){
	var real_value = $.trim($(this).val());
	if(is_shi == 1){
		if(parseInt(real_value) > 5){
			$(this).val(5);
		}
	}
	if(real_value == '' || real_value == 0){
		$(this).val(1);
	}
	
	if($(this).val() > "<?php echo $syrs; ?>"){
		$(this).val("<?php echo $syrs; ?>");
	}
  });
}

function Cartcookie(cook){
	var shopid=shopinfo['shopid'];
	var number = parseInt($("#num_dig").val());
	
	if(number < 1){
		number=1;
	}
	
	var Cartlist = $.cookie('Cartlist');
	if(!Cartlist){
		var info = {};
	}else{
		var info = $.evalJSON(Cartlist);
		if((typeof info) !== 'object'){
			var info = {};
		}
	}		
	if(!info[shopid]){
		var CartTotal=$("#sCartTotal").text();
			$("#sCartTotal").text(parseInt(CartTotal)+1);
			$("#btnMyCart em").text(parseInt(CartTotal)+1);
	}
	if(typeof info[shopid] == 'undefined'){
		info[shopid]={};
	}
	info[shopid]['num'] = typeof(info[shopid]['num'])=='undefined' ? number : Number(info[shopid]['num'])+Number(number);
	if(info[shopid]['num']>="<?php echo $syrs; ?>"){
		info[shopid]['num'] = "<?php echo $syrs; ?>";
	}
	info[shopid]['shenyu'] = shopinfo['shenyu'];
	info[shopid]['money'] = typeof(info[shopid]['money'])=='undefined' ? shopinfo['money'] : Number(info[shopid]['money'])+Number(shopinfo['money']) ;
	info['MoenyCount']='0.00';	
	$.cookie('Cartlist',$.toJSON(info),{expires:7,path:'/'});
	
	if(cook){
		window.location.href="<?php echo WEB_PATH; ?>/member/cart/cartlist/"+new Date().getTime();//+new Date().getTime()
	}
}  
</script> 
<?php include templates("index","footer");?>