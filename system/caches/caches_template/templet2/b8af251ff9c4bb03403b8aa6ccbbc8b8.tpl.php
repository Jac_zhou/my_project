<?php defined('G_IN_SYSTEM')or exit('No permission resources.'); ?><div class="memberxh_title">
	<ul>
		<li <?php if($current_status=='userbuylist'): ?>class="current"<?php  else: ?><?php endif; ?>><a href="<?php echo WEB_PATH; ?>/member/home/userbuylist">夺宝记录</a></li>
		<li <?php if($current_status=='orderlist'): ?>class="current"<?php endif; ?>><a href="<?php echo WEB_PATH; ?>/member/home/orderlist">中奖记录</a></li>
		<li <?php if($current_status=='singlelist'): ?>class="current"<?php endif; ?> ><a href="<?php echo WEB_PATH; ?>/member/home/singlelist">晒单</a></li>
		<li <?php if($current_status=='userbalance'): ?>class="current"<?php endif; ?> ><a href="<?php echo WEB_PATH; ?>/member/home/userbalance">账户明细</a></li>
		<li <?php if($current_status=='address'): ?>class="current"<?php endif; ?> ><a href="<?php echo WEB_PATH; ?>/member/home/address">联系地址</a></li>
		<li <?php if($current_status=='modify'): ?>class="current"<?php endif; ?> ><a href="<?php echo WEB_PATH; ?>/member/home/modify">个人设置</a></li>
		<li <?php if($current_status=='activing'): ?>class="current"<?php endif; ?> ><a href="<?php echo WEB_PATH; ?>/member/home/activing">活动管理</a></li>
		<li <?php if($current_status=='friends'): ?>class="current current_box"<?php endif; ?> ><a href="<?php echo WEB_PATH; ?>/member/home/friends">邀请好友</a></li>
	</ul>
</div>
<script type="text/javascript" src="<?php echo G_TEMPLATES_STYLE; ?>/js/jquery.Validform.min.js"></script>
<link href="<?php echo G_TEMPLATES_STYLE; ?>/js/style.css" rel="stylesheet" type="text/css" />