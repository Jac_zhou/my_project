<?php defined('G_IN_SYSTEM')or exit('No permission resources.'); ?><link rel="stylesheet" type="text/css" href="<?php echo G_TEMPLATES_CSS; ?>/xiaohe_zhe.css"/>
<div class="xiaohe_zhe"></div>
<div class="xh_login" >
	<div class="xh_login_top">
    	<div class="xh_login_top_left">请登录帐号</div>
        <div class="xh_login_top_right">x</div>
    </div>
    <div class="xh_login_bottom">
    	<div class="xh_login_bottom_username xh_login_01"><input name="username" type="text" value="请输入手机号" onfocus="if(this.value==this.defaultValue)this.value=''" onblur="if(this.value=='')this.value=this.defaultValue"/></div>
        <div class="xh_tishi" style="display:none"></div>
        <div class="xh_login_bottom_username xh_login_02" style="margin-top:15px;"> <input name="password" type="password" value="请输入密码" onfocus="if(this.value==this.defaultValue)this.value=''"/></div>
        <div class="xh_tishi"></div>
        <div class="w-button w-button-main m-login-btn xh_login_bottom_login" style="margin-top:15px;">登&nbsp;&nbsp;录</div>
        <div class="xh_login_regist"><a href="<?php echo WEB_PATH; ?>/register">马上注册</a></div>
    </div>
	
	<?php 
		$conn_cfg = System::load_app_config("connect",'','api');
	 ?>
	<div style="display:none">
		<div class="xh_login_three"><span>第三方登录</span></div>
		<div class="xh_login_threeimg">
			<!--<?php if($conn_cfg['qq']['off']): ?>
			<a href="<?php echo WEB_PATH; ?>/api/qqlogin/" class="xh_login_threetubiao xh_login_threeqq" title="qq登录"></a>
			<?php endif; ?>-->
			<!--<?php if($conn_cfg['weixin']['off']): ?>
			<a href="<?php echo WEB_PATH; ?>/api/wxlogin/" class="xh_login_threetubiao xh_login_threewx" title="微信登录"></a>
			<?php endif; ?>-->
			<!--<?php if($conn_cfg['weibo']['off']): ?>
			<a href="<?php echo WEB_PATH; ?>/api/weibologin/" class="xh_login_threetubiao xh_login_threewb" title="新浪登录"></a>
			<?php endif; ?>-->
		</div>
	</div>
	
</div>
<script type="text/javascript">
$(window).keydown(function(event){
	if(event.keyCode == '13'){
		$(".xh_login_bottom_login").click();
	}
});
function ajax_login(){
	var page_w = $(window).width();
	var page_h = $(window).height();
	var ju_l = Math.ceil((page_w-530)/2);
	var ju_t = Math.ceil((page_h-400)/2);
	$(".xiaohe_zhe").show();
	$(".xh_login").show().css({"left":ju_l,"top":ju_t});
		
	$(".xh_login_bottom_login").click(function(){
		var username_obj = $(".xh_login_bottom_username").find("input[name=username]");
		var pass_obj = $(".xh_login_bottom_username").find("input[name=password]")
		var username = username_obj.val();
		username_obj.keydown(function(){
			$(".xh_tishi").eq(0).hide();
		});
		if(username == '' || username=='请输入手机号'){
			username_obj.focus();
			$(".xh_tishi").eq(0).show().text('帐号不能为空!')
			return false;
		}
		
		pass_obj.keydown(function(){
			$(".xh_tishi").eq(1).hide();
		});
		
		var password = pass_obj.val();
		if(password == '' || password=='请输入密码'){
			$(".xh_tishi").eq(1).show().text('密码不能为空!');
			pass_obj.focus();
			return false;
		}
		var rurl = "<?php echo WEB_PATH; ?>/member/user/ajax_login";
		$.post(
			rurl,
			{username:username,password:password},
			function(_data){
				if(_data.status == 1){
					$(".xh_tishi").eq(0).show().text(_data.message);
					return false;
				}else if(_data.status == 2){
					$(".xh_tishi").eq(1).show().text(_data.message);
					return false;
				}else{
					window.location.reload();
				
				}
			},"json"
		);
	});
	
	$(".xh_login_top_right").click(function(){
		$(".xiaohe_zhe").hide();
		$(".xh_login").hide();
	});	
	
}
</script>