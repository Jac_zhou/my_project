<?php defined('G_IN_SYSTEM')or exit('No permission resources.'); ?><?php include templates("index","header");?>
<?php include templates("member","member_top");?>
<div class="memberxhcontains">
	<?php include templates("member","member_nav");?>
    <?php if(count($account)==0): ?>
		<div class="member_noticle_tishi"><i></i>暂时没有充值记录</div>
	<?php  else: ?>
	<!--搜索-->
    <?php include templates("member","search_t");?>
	<script type="text/javascript">
	var _action = 'all';
	$(".memberxhcontains_serachl span").click(function(){
		var _index = $(this).index();
		if(_index == 1){
			 _action = 'today';
		}else if(_index == 2){
			 _action = 'weekend';
		}else if(_index == 3){
			 _action = 'month';
		}else{
			 _action = 'all';
		}
		$(".memberxhcontains_serachl span").removeClass("current");
		$(this).addClass("current");
		window.location.href = "<?php echo WEB_PATH; ?>/member/home/userbalance/"+_action;
	});
	</script>
    
    <!--账户明细-->
    <table cellpadding="0" cellspacing="0" class="xmember_acount">
    	<tr>
        	<td width="25%">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;交易号</td>
            <td width="25%" align="center">交易时间</td>	
            <td width="10%" align="center">金额（元）</td>
             <td width="15%" align="center">充值渠道</td>
            <td width="15%" align="center">充值状态</td>
        <tr>
    </table>
    <table cellpadding="0" cellspacing="0" class="xmember_acount_info">
       <?php $ln=1;if(is_array($account)) foreach($account AS $ac): ?>
       <?php 
       		$time = time() - 3600;	//超过一小时就算过期
        ?>
       <?php if($ac[time] < $time && $ac[status] == '未付款' ): ?>
       <tr>
        	<td width="25%">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<s><?php echo $ac['code']; ?></s></td>	
            <td width="25%" align="center"><s><?php echo now_time($ac['time']); ?></s></td>
            <td width="10%" align="center"><s><?php echo $ac['money']; ?></s></td>
            <td width="15%" align="center"><s><?php echo $ac['pay_type']; ?></s></td>
            <td width="15%" align="center"><s>已过期</s></td>
        <tr>
       <?php  else: ?>
    	<tr>
        	<td width="25%">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $ac['code']; ?></td>	
            <td width="25%" align="center"><?php echo now_time($ac['time']); ?></td>
            <td width="10%" align="center"><?php echo $ac['money']; ?></td>
            <td width="15%" align="center"><?php echo $ac['pay_type']; ?></td>
            <?php if($ac[status] == '未付款'): ?>
            	<td width="15%" align="center" style="color:red;">
	            	<?php echo $ac['status']; ?>&nbsp;
	            	
            	</td>
            <?php  else: ?>
            	<td width="15%" align="center"><?php echo $ac['status']; ?></td>
            <?php endif; ?>
        <tr>
        <?php endif; ?>
        <?php  endforeach; $ln++; unset($ln); ?>
    </table>
    <?php endif; ?>
	<?php if($total>20): ?>
     <style>
		#divPageNav{width:96%; text-align:right;padding-top:10px;}
		#divPageNav ul{ text-align:center;float:right}
		#divPageNav li a{ padding:5px 8px;}
	</style>
    <div id="divPageNav" class="page_nav">
        <?php echo $page->show('two'); ?>
    </div>
	<?php endif; ?>
</div>

<?php include templates("index","footer");?>