<?php defined('G_IN_SYSTEM')or exit('No permission resources.'); ?><?php include templates("index","header");?>
<?php include templates("member","member_top");?>
<div class="memberxhcontains">
	<?php include templates("member","member_nav");?>
	<?php if(count($record)==0): ?>
		<div class="member_noticle_tishi"><i></i>还没有购买记录，赶快<a href="<?php echo WEB_PATH; ?>" target="_blank" class="membercolor_blur01">去参加</a>吧！</div>
	<?php  else: ?>
	<!--搜索-->
    <?php include templates("member","search_t");?>
	<script type="text/javascript">
	var _action = 'all';
	$(".memberxhcontains_serachl span").click(function(){
		var _index = $(this).index();
		if(_index == 1){
			 _action = 'today';
		}else if(_index == 2){
			 _action = 'weekend';
		}else if(_index == 3){
			 _action = 'month';
		}else{
			 _action = 'all';
		}
		$(".memberxhcontains_serachl span").removeClass("current");
		$(this).addClass("current");
		window.location.href = "<?php echo WEB_PATH; ?>/member/home/userbuylist/"+_action;
	});
	</script>
    <!--夺宝信息标题-->
    <table cellpadding="0" cellspacing="0" class="memberxh_duobaotitle" >
        <tr>
            <td width="12%" align="center">商品图片</td>
            <td width="35%" align="left">商品名称</td>
            <td width="20%" align="center">商品期号</td>
            <td width="12%" align="center">夺宝状态</td>
            <td width="10%" align="center">参与人次</td>
            <td width="10%" align="center">操作</td>
        </tr>
    </table>
    <table cellpadding="0" cellspacing="0" class="memberxh_duobaocontent" >
    	<?php $ln=1;if(is_array($record)) foreach($record AS $rt): ?>
        <?php 
        	$jiexiao = get_shop_if_jiexiao($rt['shopid']);
         ?>
		<?php if($jiexiao['q_uid']): ?>
    	<tr><td height="15px"></td></tr>
    	<tr class="memberxh_duobaocontenttr">
        	<td width="12%" align="center" valign="top">
            	<a href="<?php echo WEB_PATH; ?>/dataserver/<?php echo $rt['shopid']; ?>_<?php echo $rt['shopqishu']; ?>" target="_blank">
                	<!--<img src="<?php echo G_UPLOAD_PATH; ?>/<?php echo $jiexiao['thumb']; ?>" width='64px' height="48px"/>-->
					<img src="<?php echo G_UPLOAD_PATH; ?>/<?php echo $jiexiao['thumb']; ?>" width='64px' height="64px"/>
                </a>
            </td>
	
			<td width="35%">
            	<table cellpadding="0" cellspacing="0" class="memberxh_duobaocontentinfo">
                	<tr><td><div class="name_control"><a class="membercolor_blur01 xh_overflow_x" href="<?php echo WEB_PATH; ?>/dataserver/<?php echo $rt['shopid']; ?>_<?php echo $rt['shopqishu']; ?>" target="_blanak"><?php echo $rt['shopname']; ?></a></div></td></tr>
                    <tr><td>总需：<?php echo $jiexiao['zongrenshu']; ?>人次</td></tr>
                    <tr><td>获得者：<span class="membercolor_blur01">
                    	<a href="<?php echo WEB_PATH; ?>/uname/<?php echo idjia($jiexiao['q_uid']); ?>" class="membercolor_blur01"><?php echo get_user_name($jiexiao['q_user']); ?></a></span>（本期共参与<?php echo get_user_goods_num($jiexiao['q_uid'],$rt['shopid']); ?>人次）</td></tr>
                    <tr><td>幸运号码：<span class="membercolor_red01"><?php echo $jiexiao['q_user_code']; ?></span></td></tr>
                    <tr><td>揭晓时间：<span><?php echo microt($jiexiao['q_end_time']); ?></span></td></tr>
                	<tr><td height="15px"></td></tr>
                </table>
            </td>
            <td width="20%" align="center" valign="top">第(<?php echo $rt['shopqishu']; ?>)期</td>
            <td width="12%" align="center" valign="top"  class="membercolor_red01">已揭晓</td>
            <td width="10%" align="center" valign="top"><?php echo $rt['count_gonumber']; ?>人次</td>
            <td width="10%" align="center" valign="top">
            	<a href="<?php echo WEB_PATH; ?>/member/home/userbuydetail/<?php echo $rt['shopid']; ?>" target="_blank" class="membercolor_blur01">详情</a>
            </td>
        </tr>
        <?php  else: ?>
        <tr><td height="15px"></td></tr>
    	<tr class="memberxh_duobaocontenttr">
        	
        	<td width="12%" align="center" valign="top">
            	<a href="<?php echo WEB_PATH; ?>/goods/<?php echo $rt['shopid']; ?>" target="_blank">
                	<img src="<?php echo G_UPLOAD_PATH; ?>/<?php echo yunjl($rt['shopid']); ?>" width='64px' height="64px"/>
                </a>
            </td>
            <td width="35%">
            	<table cellpadding="0" cellspacing="0" class="memberxh_duobaocontentinfo">
                	<tr><td><div class="name_control"><a class="membercolor_blur01 xh_overflow_x" href="<?php echo WEB_PATH; ?>/goods/<?php echo $rt['shopid']; ?>" target="_blanak"><?php echo $rt['shopname']; ?></a></div></td></tr>
                    <tr><td>总需：<?php echo get_shopx_info($rt['shopid'],'zongrenshu'); ?>人次</td></tr>
					<tr>
						<td>
							<p class="Progress-bar"><span style="width:<?php echo get_length($rt['shopid'],'373'); ?>px;height:8px;border: 1px solid #f80;background:e53c08;"></span></p>
						</td>
					</tr>
					<tr class="Pro-bar-li">
						<td>
							<ul>
								<li class="P-bar01" style="color:e53c08"><?php echo get_shopx_info($rt['shopid'],'canyurenshu'); ?></li>
								<li class="P-bar02" style="color:#777"><?php echo get_shopx_info($rt['shopid'],'zongrenshu'); ?></li>
								<li class="P-bar03" style="color:#14A8E4"><?php echo get_shopx_info($rt['shopid'],'shenyurenshu'); ?></li>
							</ul>
						</td>
					</tr>
					<tr class="Pro-bar-li">
						<td>
							<ul>
								<li class="P-bar01">已参与人次</li>
								<li class="P-bar02">总需人次</li>
								<li class="P-bar03">剩余人次</li>
							</ul>
						</td>
					</tr>
                	<tr><td height="15px"></td></tr>
                </table>
            </td>
            <td width="20%" align="center" valign="top">第(<?php echo $rt['shopqishu']; ?>)期</td>
            <td width="12%" align="center" valign="top"  class="membercolor_red01">
				<?php if($jiexiao['shenyurenshu']==0): ?>正在揭晓<?php  else: ?>正在进行<p><a href="<?php echo WEB_PATH; ?>/goods/<?php echo $rt['shopid']; ?>" target="_blank" class="membercolor_blur01">追加</a></p><?php endif; ?>
			</td>
            <td width="10%" align="center" valign="top"><?php echo $rt['count_gonumber']; ?>人次</td>
            <td width="10%" align="center" valign="top">
            	<a href="<?php echo WEB_PATH; ?>/member/home/userbuydetail/<?php echo $rt['shopid']; ?>" target="_blank" class="membercolor_blur01">详情</a>
            </td>
        </tr>
        
        <?php endif; ?>
		<?php  endforeach; $ln++; unset($ln); ?>
    
    </table>
	<?php if($total>10): ?>
    <style>
		#divPageNav{width:96%; text-align:right;padding-top:10px;}
		#divPageNav ul{ text-align:center;float:right}
		#divPageNav li a{ padding:5px 8px;}
	</style>
    <div id="divPageNav" class="page_nav">
        <?php echo $page->show('two'); ?>
    </div>
	<?php endif; ?>
	<?php endif; ?>
</div>

<?php include templates("index","footer");?>