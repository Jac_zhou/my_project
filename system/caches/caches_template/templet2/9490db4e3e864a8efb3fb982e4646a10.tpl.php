<?php defined('G_IN_SYSTEM')or exit('No permission resources.'); ?><?php include templates("index","header");?>
<link rel="stylesheet" type="text/css" href="<?php echo G_TEMPLATES_STYLE; ?>/css/Index.css"/>
<div class="bg">
    <div class="banner">
        <img src="<?php echo G_TEMPLATES_STYLE; ?>/images/B_zs-banner.png" width="882" height="259"></div>
    <div class="menu">
        <div class="menu_right">
        </div>
        <div class="menu_left">
        </div>
        <div class="menu_center">
            <ul>
                <li><a href="#01">易中夺宝网简介</a></li>
                <li class="menuline"></li>
                <li><a href="#02">易中夺宝的核心优势</a></li>
                <li class="menuline"></li>
                <li><a href="#03">合作对象</a></li>
                <li class="menuline"></li>
                <li><a href="#03">合作方式</a></li>
                <li class="menuline"></li>
                <li><a href="#04">合作流程</a></li>
                <li class="menuline"></li>
                <li><a href="#05">招商联系</a></li>
            </ul>
        </div>
    </div>
    <div class="con">
        <div class="fonttop">
            <!--&nbsp;&nbsp;易中夺宝（www.yz-db.com）是一种全新的互动购物体验方式，是时尚、潮流的风向标，能满足个性、年轻消费者的购物需求，由深圳市中利保投资管理有限公司注入巨资打造的新型购物网。-->
			&nbsp;&nbsp;   &nbsp;&nbsp;易中夺宝（www.1ysmg.com）是深圳市大唐讯士电子科技有限公司推出的集娱乐、购物于一身的活动平台，平台引入众筹理念，每个用户最低花费1元即可有机会获得心仪商品，凭借技术实力和算法公平，确保100%公平公正、100%正品保障，旨在提供全新的网购体验。
			</div>
        <div class="title1">
            <span>夺宝简介</span><a name="01" id="01"></a></div>
        <div class="text">
            易中夺宝是指一件被均分成若干“等份”，用户只需支付1元（即1个幸运豆），就能得到一个随机分配的幸运码，当该商品分配的所有“等份”被认购完毕，系统将按既定算法计算出一个幸运编号，拥有该幸运编号的用户即可获得该商品。<br>
          
              例如，一部5000元的iPhone6s手机，易中夺宝将其均分成5000“等份”，每一“等份”对应一个幸运码，用户支付1元获得1个号码，当5000个号码被认购完毕，由既定算法计算出一个中奖幸运码，拥有该幸运码的用户即可喜抱iPhone归！ <br>
            <p>
              <br> 易中夺宝（www.1ysmg.com）是一种全新的互动购物体验方式，是时尚、潮流的风向标，能满足个性、年轻消费者的购物需求，以“快乐夺宝，惊喜无限”为宗旨，力求打造一个100%公平公正、100%正品保障、集娱乐与购物一体化的新型购物网站。<br>
                <br>
                随着互联网的发展及网购消费模式的多样化，易中夺宝势必成为中国电子商务网站中最具活力的生力军及网民最爱的购物网，必将迅速崛起，成为互联网新时代的佼佼者！
                <br>
            </p>
        </div>
        <div class="title2">
            <span>易中夺宝核心优势</span><a name="02" id="02"></a></div>
        <div class="text">
            1. 易中夺宝是全球模式创新、国内最具潜质的综合型B2C电子商务平台，由深圳市乐迈互动有限公司投入巨资打造。<br>
            2. 易中夺宝拥有激扬的青春舞台，这里洋溢着激情与个性，这里充满着创意与灵感！我们立志成为一家快速成长、充满活力的创业公司，我们更
            <br>
            &nbsp;&nbsp;&nbsp;&nbsp;矢志成为一家伟大的、备受社会尊敬的基业长青的企业！<br>
            3. 易中夺宝具有高端信息技术支撑的网络购物平台，可支持1000万用户同时访问，每天可处理上万订单。<br>
            4. 强大的互联网营销能力，领先的精准营销手段，线上线下渠道的无缝整合，快速准确地向用户传递最新、最热商品信息。</div>
        <div class="title3">
            <span>合作对象与合作方式</span><a name="03" id="03"></a></div>
        <div class="text">
            <b>1、合作对象</b><br>
            <p style="margin: 0; padding: 0 0 0 20px;">
                1）国内外品牌商、经销商或有正规货源及授权的渠道商，有合法完整的企业营业执照、企业税务登记证、品牌授权书（或商标注册证）；</p>
            <b>2、合作方式</b><br>
            <p style="margin: 0; padding: 0 0 0 20px;">
                1）代销
                2）其他商业合作<br>
        </div>
        <div class="title4">
            <span>合作方式</span><a name="04" id="04"></a></div>
        <div class="text">
            <img src="<?php echo G_TEMPLATES_STYLE; ?>/images/B_lc_pic.png" width="833" height="195"></div>
        <div class="title5">
            <span>招商联系</span><a name="05" id="05"></a></div>
        <div class="text" style="background: none;">
            <div class="linktext">
                联系人：商品运营中心<br>
                <!--电话：400-601-8106<br>-->
                地址：深圳市龙岗区龙岗中心城城投商务中心<br>
                邮编：518000<br>
            </div>
        </div>
    </div>
</div>
<?php include templates("index","footer");?>