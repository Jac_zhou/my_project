<?php defined('G_IN_SYSTEM')or exit('No permission resources.'); ?><?php include templates("index","header");?>
<link rel="stylesheet" type="text/css" href="<?php echo G_TEMPLATES_STYLE; ?>/css/new/trio.css"/>
<div class="trio-container">
    <div class="trio-bg">
        <img src="<?php echo G_TEMPLATES_STYLE; ?>/images/new/background_trio_2.png">
    </div>
    <div class="suggest-one">

        <div class="suggestion-1 add-5">选择一款商品，点击“立即夺宝”</div>
        <div class="suggestion-1"></div>
        <div class="suggestion-1"></div>
    </div>
    <div class="suggest-two">
        <div class=" add-1 add-5" >支付一元，购买一人次，获得一个“幸运码”</div>
        <div class=" add-2"></div>
        <div class="add-2"><br>
			<br>
			</div>
    </div>
    <div class="suggest-three">
        <div class="suggestion-1 add-5 add-4">当一件商品达到总参与人次，抽出一名商品获得者</div>
        <div class="suggestion-1 add-4"></div>
        <div class="suggestion-1 add-4"><br>
			</div>
        <div class="suggestion-1 add-4"></div>
    </div>
    <!--发货验收-->
    <div class="send-check">
        <div>开奖信息会第一时间在平台公开，用户还可以查看他人交易/中奖/晒单记录。</div>
        <div>中奖用户在与官方确认信息后，即可等待发货、验收，并晒单分享。</div>
    </div>
    <!--规则-->
    <div class="trio-rule">
        <!--第一个列表-->

       
		<div class="list-1">每件商品参考市场价均分成若干“等份”，每份1元，对应1个幸运豆，支付一个幸运豆即可获得一个幸运码。</div>
        <div class="list-1">同一件商品可以购买多次，或者一次购买多份。</div>
        <div class="list-1">当一件商品所有“等份”全部售罄，按既定算法计算出中奖幸运码，拥有中奖幸运码者即可获得该商品。</div>
        <!--第二个列表-->
        <div class="list-top">
            <div class="list-1">1、取该商品最后购买时间前网站所有商品的最后100条购买时间记录 （限时购买商品取截止时间前网站所有商品100条购买时间记录）；</div>
            <div class="list-1">2、每个时间记录按时、分、秒、毫秒依次排列取数值并将这100个数值求和（A） ；</div>
            <div class="list-1">3、为保证公平公正公开，系统还会等待一小段时间，取最近一期中国乐彩彩票“时时彩”的开奖结果（一个五位数值B） ； </div>
			<div class="list-1">注：最后一个号码分配时间距离中国乐彩彩票“时时彩”最近下一期开奖大于15分钟，默认“时时彩”开奖结果为00000。  </div>
			<div class="list-1">4、将A与B之和除以该商品总参与人次后取余数，余数加上10000001 即为“幸运财神号”。 如果网站未满100条购买记录，则按照【 10000001 + (揭晓时间总和与时时彩中奖号码求和结果*100/参与人数) 的余数】即为“中奖幸运码”。  </div>
        </div>

    </div>
    <div class="button-color">
        <div class="trio-button">
            <a href="<?php echo WEB_PATH; ?>" target="_blank">立即参与易中夺宝</a>
        </div>
    </div>
</div>


<!--新手指南结束-->
<?php include templates("index","footer");?>