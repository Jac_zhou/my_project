<?php defined('G_IN_SYSTEM')or exit('No permission resources.'); ?><?php include templates("index","header");?>
<meta http-equiv = "X-UA-Compatible" content = "IE=edge,chrome=1">
<link rel="stylesheet" type="text/css" href="<?php echo G_TEMPLATES_STYLE; ?>/css/Home.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo G_TEMPLATES_STYLE; ?>/css/GoodsList.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo G_TEMPLATES_STYLE; ?>/css/Comm.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo G_TEMPLATES_STYLE; ?>/css/new/NewGoodsList.css"/>

<link rel="stylesheet" type="text/css" href="<?php echo G_TEMPLATES_STYLE; ?>/css/new/list-c.css"/>
<!--<link rel="stylesheet" type="text/css" href="<?php echo G_TEMPLATES_STYLE; ?>/css/header.css"/>-->

<script>
function showh(){
var height=$("#ddBrandList_brand").innerHeight();	
	if(height==78){
		$("#ddBrandList_brand").css("height","auto");
		$(".list_classMore").addClass("MoreClick");
		$(".list_classMore").html("收起<i></i>");
	}else{
		$("#ddBrandList_brand").css("height","78px");
		$(".list_classMore").removeClass("MoreClick");
		$(".list_classMore").html("展开<i></i>");
	};
}
$(function(){	
	$(".list_classMore").click(showh);
});
</script>
<div class="wrap" id="loadingPicBlock">
	<div class="Current_nav"><a  style="padding:15px;padding-right:15px;" href="<?php echo WEB_PATH; ?>">首页</a> &gt; <?php echo $daohang_title; ?></div>
	<div id="current" class="list_Curtit">
		<h1 class="fl"><?php echo $daohang_title; ?></h1>
		<span id="spTotalCount">(共<em class="orange"><?php echo $total; ?></em>件相关商品)</span>
	</div>	
	<?php 
		$two_cate_list = $this->db->GetList("select cateid,parentid,name from `@#_category` where `model` = '1' and `parentid` = '0' order by `order` DESC LIMIT 8 ");
	 ?>
	<?php if(!empty($two_cate_list)): ?>
	<div class="list_class">
	<!--商品分类-->
        <div id="ddBrandList-Modify">
            <ul >
				<?php $ln=1; if(is_array($two_cate_list)) foreach($two_cate_list AS $key => $two_cate): ?>
                <li class="l_content">
					<a href="<?php echo WEB_PATH; ?>/goods_list/<?php echo $two_cate['cateid']; ?>_0_0">
						<div class="l_a">
							<div class="ease_in_out l_img l_img<?php echo $key+1; ?>" ></div><span class="l_span <?php if($two_cate['cateid']==$cid): ?>l_span_curr<?php endif; ?>" ><?php echo $two_cate['name']; ?></span>
						</div>
					</a>
				</li>
				<?php  endforeach; $ln++; unset($ln); ?>
            </ul>
        </div>
    <div style="clear: both"> </div>

	
	</div>	
	<?php endif; ?>
	
	<div class="list_class">
	<dl>
		<dt>品牌</dt>
		<?php if(count($brand)>17): ?>
		<dd id="ddBrandList_brand" style="height:78px">
		<?php  else: ?>
		<dd id="ddBrandList_brand">
		<?php endif; ?>
			<ul>
            	<?php if(!$bid): ?>
				<li><a href="<?php echo WEB_PATH; ?>/goods_list/<?php echo $cid; ?>_0_<?php echo $order; ?>" class="ClassCur" >全部</a></li> 
                <?php  else: ?>
                <li><a href="<?php echo WEB_PATH; ?>/goods_list/<?php echo $cid; ?>_0_<?php echo $order; ?>">全部</a></li>
                <?php endif; ?>				
				<?php $ln=1;if(is_array($brand)) foreach($brand AS $brands): ?>             
				<?php if($brands['id'] == $bid): ?>
				<li><a class="ClassCur" href="<?php echo WEB_PATH; ?>/goods_list/<?php echo $cid; ?>_<?php echo $brands['id']; ?>_<?php echo $order; ?>"><?php echo $brands['name']; ?></a></li>   
				<?php  else: ?>
				<li><a href="<?php echo WEB_PATH; ?>/goods_list/<?php echo $cid; ?>_<?php echo $brands['id']; ?>_<?php echo $order; ?>"><?php echo $brands['name']; ?></a></li>   
				<?php endif; ?>
				<?php  endforeach; $ln++; unset($ln); ?>
			</ul>
		</dd>
	</dl>

	<?php if(count($brand)>17): ?>	
	<a class="list_classMore" href="javascript:;">展开<i></i></a>
	<?php endif; ?>	
	</div>
	    <div class="list_Sort">
		    <dl>
			    <dt>排序</dt>
			    <dd>
                <a href="<?php echo WEB_PATH; ?>/goods_list/<?php echo $cid; ?>_<?php echo $bid; ?>_1" <?php if($order=='1'): ?>class="SortCur"<?php endif; ?>>即将揭晓</a>
                <a href="<?php echo WEB_PATH; ?>/goods_list/<?php echo $cid; ?>_<?php echo $bid; ?>_2" <?php if($order=='2'): ?>class="SortCur"<?php endif; ?>>人气</a>
                <a href="<?php echo WEB_PATH; ?>/goods_list/<?php echo $cid; ?>_<?php echo $bid; ?>_3" <?php if($order=='3'): ?>class="SortCur"<?php endif; ?>>剩余人次</a>
                <a href="<?php echo WEB_PATH; ?>/goods_list/<?php echo $cid; ?>_<?php echo $bid; ?>_4" <?php if($order=='4'): ?>class="SortCur"<?php endif; ?>>最新</a>
                <?php if($order=='5'): ?>
                <a href="<?php echo WEB_PATH; ?>/goods_list/<?php echo $cid; ?>_<?php echo $bid; ?>_6" class="Price_Sort SortCur">价格 <i></i></a>
                <?php  else: ?>
                    <?php if($order=='6'): ?>
                   		<a href="<?php echo WEB_PATH; ?>/goods_list/<?php echo $cid; ?>_<?php echo $bid; ?>_5" class="Price_Sort SortCur">价格 <s></s></a>
                    <?php  else: ?>
                    	<a href="<?php echo WEB_PATH; ?>/goods_list/<?php echo $cid; ?>_<?php echo $bid; ?>_5" class="Price_Sort">价格 <s></s></a>
                    <?php endif; ?>
                <?php endif; ?>
                </dd>
		    </dl>
	    </div>
	
	<?php if($shoplist): ?>
	<!--商品列表-->
	<div class="listContent">
		<ul class="item" id="ulGoodsList">		
			<?php $ln=1;if(is_array($shoplist)) foreach($shoplist AS $shop): ?>
			<li class="goods-iten" >
				<div class="pic">
					<a href="<?php echo WEB_PATH; ?>/goods/<?php echo $shop['id']; ?>" target="_blank" title="<?php echo $shop['title']; ?> ">
                    	<!--新品的图标-->
						<!--<i class="goods_xp"></i>-->				
						<!--推荐的图标-->
                   	 	<!--<i class="goods_tj"></i>-->
                   	 	<!--人气商品的图标-->
                        <!--<i class="goods_rq"></i>-->	                  	 
						<?php if($shop['is_shi'] ==1): ?>						
                                                    <?php echo '<i class="goods_xp_shi"></i>'; ?>	
                                                <?php elseif ($shop['pk_product']==1): ?>
                                                    <?php echo '<i class="goods_xp_shis"></i>'; ?>
						<?php endif; ?>
                                         	<span class="pk_numer"><?php echo $shop['pk_number']; ?></span> 
						<img alt="<?php echo $shop['title']; ?>" src="<?php echo G_UPLOAD_PATH; ?>/<?php echo $shop['thumb']; ?>">
					</a>
					<p name="buyCount" style="display:none;"></p>
				</div>
				<p class="name">
					<a href="<?php echo WEB_PATH; ?>/goods/<?php echo $shop['id']; ?>" target="_blank" title="<?php echo $shop['title']; ?>" class="xh_overflow_x"><?php echo $shop['title']; ?></a>
				</p>
				<p class="money">价值：<span class="rmbgray"><?php echo $shop['money']; ?></span></p>
				<div class="Progress-bar">
					<p title="已完成<?php echo percent($shop['canyurenshu'],$shop['zongrenshu']); ?>"><span style="width:<?php echo width($shop['canyurenshu'],$shop['zongrenshu'],213); ?>px;"></span></p>
					<ul class="Pro-bar-li">
						<li class="P-bar01"><em><?php echo $shop['canyurenshu']; ?></em>已参与人次</li>
						<li class="P-bar02"><em><?php echo $shop['zongrenshu']; ?></em>总需人次</li>
						<li class="P-bar03"><em><?php echo $shop['zongrenshu']-$shop['canyurenshu']; ?></em>剩余人次</li>
					</ul>
				</div>
				<div class="gl_buybtn">
					<div class="go_buy">
						<div style="margin-left:18%;">
							<input type="hidden" value="<?php echo WEB_PATH; ?>/goods_list/<?php echo $shop['id']; ?>" />
							<a href="javascript:;" title="立即夺宝" class="go_Shopping fl" default-renci="<?php echo $shop['default_renci']; ?>">立即夺宝</a>
							<a href="javascript:;" title="加入购物车" class="go_cart xh_floatleft" default-renci="<?php echo $shop['default_renci']; ?>"></a>
						</div>
					</div>
				</div>
				
				<div class="Curbor_id" style="display:none;"><?php echo $shop['id']; ?></div>
				<div class="Curbor_yunjiage" style="display:none;"><?php echo $shop['yunjiage']; ?></div>
				<div class="Curbor_shenyu" style="display:none;"><?php echo $shop['shenyurenshu']; ?></div>
				<div class="Curbor_default_renci" style="display:none;"><?php echo $shop['default_renci']; ?></div>
			</li>
			<?php  endforeach; $ln++; unset($ln); ?>
		</ul>	
	<?php if($total>$num): ?>
	<div style="width:1200px;margin:0 auto; text-align:center">
		<div class="pagesx"><?php echo $page->show('two'); ?></div>
	</div>
	
	<?php endif; ?>
	</div>
	<!--商品列表完-->
	<?php  else: ?>
	<!--未找到商品-->
	<div class="NoConMsg"><span>无相关商品记录哦~！</span></div>
	<!--未找到商品-->
	<?php endif; ?>
	
</div>
<script type="text/javascript">

$(function(){
//商品购物车
	$("#ulGoodsList li a.go_cart").click(function(){ 
		var sw = $("#ulGoodsList li a.go_cart").index(this);		
		var src = $("#ulGoodsList li .pic a img").eq(sw).attr('src');				
		var $shadow = $('<img id="cart_dh" style="display:none; border:1px solid #aaa; z-index:99999;" width="200" height="200" src="'+src+'" />').prependTo("body");  			
		var $img = $("#ulGoodsList li .pic").eq(sw);
		$shadow.css({ 
			'width' : 200, 
			'height': 200, 
			'position' : 'absolute',      
			"left":$img.offset().left+16, 
			"top":$img.offset().top+9,
			'opacity' : 1    
		}).show();
		
		//顶部购物车显示数目
		var $cart = $("#btnMyCart");

		$shadow.animate({   
			width: 1, 
			height: 1, 
			top: $cart.offset().top,    
			left: $cart.offset().left, 
			opacity: 0
		},500,function(){
			Cartcookie(sw,false);
		});	
		
	});
	$("#ulGoodsList li a.go_Shopping").click(function(){	
		var sw = $("#ulGoodsList li a.go_Shopping").index(this);
		Cartcookie(sw,true); 
	});	
});
//存到COOKIE
function Cartcookie(sw,cook){

	var shopid = $(".Curbor_id").eq(sw).text(),
		shenyu = parseInt($(".Curbor_shenyu").eq(sw).text()),
		money = $(".Curbor_yunjiage").eq(sw).text(),
		default_renci = parseInt($(".Curbor_default_renci").eq(sw).text());
	//重新计算金额和默认人次   20151208 kangpengfei
	if(default_renci > shenyu){
		default_renci = shenyu;
	}
	money = money * default_renci;
	
	var Cartlist = $.cookie('Cartlist');
	if(!Cartlist){
		var info = {};
	}else{
		var info = $.evalJSON(Cartlist);
	}
	if(!info[shopid]){	
		var CartTotal=$("#sCartTotal").text();			
			$("#sCartTotal").text(parseInt(CartTotal)+1);			
			$("a#btnMyCart em").text(parseInt(CartTotal)+1);
	}	
	if(typeof info[shopid] == 'undefined'){
		info[shopid]={};
	}
	info[shopid]['num']=typeof(info[shopid]['num']) == 'undefined' ? default_renci : Number(info[shopid]['num'])+Number(default_renci);
	info[shopid]['shenyu']=shenyu;
	info[shopid]['money']=typeof(info[shopid]['money']) == 'undefined' ? money : Number(info[shopid]['money'])+Number(money);
	info['MoenyCount']='0.00';
	$.cookie('Cartlist',$.toJSON(info),{expires:30,path:'/'});
	if(cook){
		window.location.href="<?php echo WEB_PATH; ?>/member/cart/cartlist";
	}
} 

</script>
<?php include templates("index","footer");?>