<?php defined('G_IN_SYSTEM')or exit('No permission resources.'); ?><?php include templates("index","header");?>
<link rel="stylesheet" type="text/css" href="<?php echo G_TEMPLATES_STYLE; ?>/css/layout-Frame.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo G_TEMPLATES_STYLE; ?>/css/layout-setUp.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo G_TEMPLATES_CSS; ?>/new/alert.css"/>
<style>
.infos tr td{margin-top:8px;}
</style>
<script type="text/javascript" src='<?php echo G_TEMPLATES_JS; ?>/new/alert.js'></script>
<script type="text/javascript">
$(function(){		
	var demo=$(".registerform").Validform({
		tiptype:2,
		ignoreHidden:true,
		datatype:{
			"tel":/([a-z][A-Z][0-9])+/,
			"nic":function(gets,obj,curform,regxp){
				var len = gets.replace(/[^\x00-\xff]/g,'**');
				var reg1 = /^[A-Za-z0-9_\u4e00-\u9fa5]{1,20}$/;
				if( ! reg1.test(gets)) return false;
				if(len.length < 1 || len.length > 20){
					return false;
				}else{
					return true;
				}
			}
			//  /^[A-Za-z0-9_\u4e00-\u9fa5]{2,20}$/,
		},
		beforeSubmit:function(curform){
			//在验证成功后，表单提交前执行的函数，curform参数是当前表单对象。
			//这里明确return false的话表单将不会提交;			
			//$.Hidemsg();
			var x_data = $(curform).serialize();
			//alert(x_data);return;
			var rurl = '<?php echo WEB_PATH; ?>/member/home/usermodify';
			$.ajax({
				url:rurl,
				data:x_data,
				type:"POST",
				success:function(data){
					var _data=$.parseJSON(data);
					if(_data.error == "1"){
						msg_show.waring(_data.msg);
					}else if(_data.error == "0"){
						//msg_show.success(_data.msg,_data.url);
						window.location.href=_data.url;
						return false;
					}
				}
			});
			return false;
		}
	});	
	demo.tipmsg.w["tel"]="请正确输入电话号码(区号、号码必填，用“-”隔开)";
	demo.tipmsg.w["nic"]="昵称为1-20个字符(可由汉字、字母、数字、“_”等字符组成)";
	demo.addRule([
	{
		ele:"#nicxx",
		datatype:"nic",
	},
	{
		ele:"#txt_ship_tel",
		datatype:"tel",
	}]);
	
	//绑定手机
	$(".bangding_phone").click(function(){
		$.ajax({
			url:"<?php echo WEB_PATH; ?>/member/home/mobilechecking",
			data:{submit:"submit"},
			type:"POST",
			success:function(data){
				var _data=$.parseJSON(data);
				if(_data.error == "1"){
					msg_show.waring(_data.msg);
				}else if(_data.error == "0"){
					//msg_show.success(_data.msg,_data.url);
					window.location.href=_data.url;
					return false;
				}
			}
		});
	});
	//绑定邮箱
	$(".bangding_email").click(function(){
		$.ajax({
			url:"<?php echo WEB_PATH; ?>/member/home/mailchecking",
			data:{submit:"submit"},
			type:"POST",
			success:function(data){
				var _data=$.parseJSON(data);
				if(_data.error == "1"){
					msg_show.waring(_data.msg);
				}else if(_data.error == "0"){
					//msg_show.success(_data.msg,_data.url);
					window.location.href=_data.url;
					return false;
				}
			}
		});
	});
	
});
</script>
<?php include templates("member","member_top");?>
<div class="memberxhcontains">
	<?php include templates("member","member_nav");?>
	<div class="R-content">
		<?php include templates("member","shezhi");?>
		<div class="prompt orange">完善以下资料，<?php echo _cfg('web_name_two'); ?>不会以任何形式公开您的个人隐私，请放心填写！<s></s></div>
		<div class="info" style="width:74%; margin:20px auto">
		<form class="registerform" method="post" action="">
			<table class="infos" border="0" cellpadding="0" cellspacing="8">
				<tr>
					<?php if($member['mobile']!=null && $member['mobilecode']=='1'): ?>		
					<td align="right"><em><font>*</font><label>手机：</label></em></td>
					<td>
						<b><?php echo $member['mobile']; ?></b> 
						 已验证
					</td>
					
					<?php endif; ?>
				</tr>
				<!-- <tr>
					<?php if($member['email']!=null && $member['emailcode']=='1'): ?>		
					<td align="right"><em><font>*</font><label>邮箱：</label></em></td>
					<td>
						<b><?php echo $member['email']; ?></b> 
						 已验证
					</td>
					
					<?php endif; ?>
				</tr> -->
				
				<tr>
					<td align="right"><em><font>*</font><label>昵称：</label></em></td>
					<td>
						<input id="nicxx" datatype="nic" nullmsg="昵称不能为空" name="username" value="<?php echo $member['username']; ?>" type="text"  class="txt gray" maxlength="20" />					
					</td>					
					<td><div class="Validform_checktip">昵称为2-20个汉字、字母、数字、“_”字符组成</div></td>
				</tr>
				<!-- <tr>
					<td align="right"><em><font>&nbsp;</font><label>签名：</label></em></td>
					<?php if($member['qianming']==null): ?>
					<td><textarea name="qianming" class="info_txtarea gray03" >让别人看到不一样的你！</textarea></td>
					<?php  else: ?>
					<td><textarea name="qianming" class="info_txtarea gray03" ><?php echo $member['qianming']; ?></textarea></td>
					<?php endif; ?>
				</tr> -->
				<tr>
					<td><em>&nbsp;</em></td>
					<td><input name="submit" type="submit" class="bluebut" value="保存"></td>
				</tr>
			</table>
		</form>	
		</div>
	</div>
</div>
<?php include templates("index","footer");?>