<?php defined('G_IN_SYSTEM')or exit('No permission resources.'); ?><?php include templates("index","header");?>
<link rel="stylesheet" type="text/css" href="<?php echo G_TEMPLATES_STYLE; ?>/css/LotteryDetail.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo G_TEMPLATES_STYLE; ?>/css/new/NewLotteryDetail.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo G_TEMPLATES_STYLE; ?>/css/new/ten.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo G_TEMPLATES_STYLE; ?>/css/qishu_list.css"/>
<div class="Current_nav">
    <a href="<?php echo WEB_PATH; ?>">首页</a> <span>&gt;</span>
    <a href="<?php echo WEB_PATH; ?>/goods_list/<?php echo $item['cateid']; ?>">
        <?php echo $category['name']; ?>
    </a> <span>&gt;</span>
    <a href="<?php echo WEB_PATH; ?>/goods_list/<?php echo $item['cateid']; ?>e<?php echo $item['brandid']; ?>">
        <?php echo $brand['name']; ?>
    </a> <span>&gt;</span>揭晓详情
</div>
<!--本商品的期数导航start-->
<?php include templates("index","qishu_list");?>
<!--本商品的期数导航end-->
<div class="show_content">
    <!-- 商品信息 -->
    <div class="Pro_Details">
        <h1><span>(第<?php echo $item['qishu']; ?>期)</span><span><?php echo $item['title']; ?></span><span style="<?php echo $item['title_style']; ?>"><?php echo $item['title2']; ?></span>
        </h1>
        <div class="Pro_Detleft">
            <div class="Pro_Detimg">
                <div class="Pro_pic">
                    <a href="<?php echo WEB_PATH; ?>/goods/<?php echo $item['id']; ?>" title="<?php echo $item['title']; ?>">
                        <?php if($item['is_shi']==1): ?>
                        <div style="position:absolute;z-index: 10000;top:5px;width:50px;height:50px;background: #000000;background: url(<?php echo G_TEMPLATES_IMAGE; ?>/shiyuan.png) no-repeat;background-size: 100%;"></div>
                        <?php endif; ?>
                        <img width="398" height="398" src="<?php echo G_UPLOAD_PATH; ?>/<?php echo $item['thumb']; ?>">
                    </a>
                </div>
            </div>
            <!-- 2015/12/20这里要查看自己购买的夺宝号 -->
            <div class="Result_LConduct" style="overflow:hidden">
                <div class="itemsee_zhongle">
                    <?php if(empty($arr_member_info['uid'])): ?>
                    <div class="itemsee_zhonglelogin"><span>请登录</span>，查看你的夺宝号！</div>
                    <?php elseif ($item['q_uid'] == $arr_member_info['uid']): ?>
                    <div class="mycanyuno">恭喜您获得本期商品！</div>
                    <?php  else: ?>
                    <div class="itemsee_zhonglemy">
                        <?php if($my_zhongle_count==0): ?>
                        <div class="mycanyuno">您没有参与本次夺宝哦！</div>
                        <?php  else: ?>
                        <div class="mycanyu">
                            我参与了: <span class="itemsee_zhonglemyleft"><?php echo $my_zhongle_count; ?></span>人次&nbsp;|&nbsp;
                            <span class="itemsee_zhonglemyduo">夺宝号: </span>
							<span class="itemsee_zhonglemyduoma">
							<?php 
								if(strlen(sub_str($my_zhongle_number,20)) != strlen($my_zhongle_number)){
									$more = "<span class='see_more'>&nbsp;&nbsp;更多>><span>";
								}else{
									$more = "";
								}
							 ?>
							<?php echo sub_str($my_zhongle_number,18); ?><?php echo $more; ?>
							</span>
                        </div>
                        <?php endif; ?>
                    </div>
                    <?php endif; ?>
                </div>
            </div>
            <?php if(!$item['q_content']): ?>
            <?php include templates("index","jisuang_no");?>
            <?php  else: ?>
            <?php include templates("index","jisuang");?>
            <?php endif; ?>
        </div>
        <div class="Pro_Detright">
            <p class="Det_money">价值：<span class="rmbgray"><?php echo $item['money']; ?></span></p>
            <div class="Announced_Frame">
                <div class="Announced_FrameT">揭晓结果</div>
                <div class="Announced_FrameCode">
                    <ul class="Announced_FrameCodeMal">
                        <?php $ln=1;if(is_array($q_user_code_arr)) foreach($q_user_code_arr AS $q_code_num): ?>
                        <li class="Code_<?php echo $q_code_num; ?>"><?php echo $q_code_num; ?><b></b></li>
                        <?php  endforeach; $ln++; unset($ln); ?>
                    </ul>
                </div>
                <div class="Announced_FrameGet">
                    <dl>
                        <dd class="gray02">
                            <p>恭喜<a href="<?php echo WEB_PATH; ?>/uname/<?php echo idjia($item['q_uid']); ?>" target="_blank" class="blue"
                                    title="Warm">
                                <?php echo get_user_name($q_user); ?> </a> 获得本期商品
                            </p>
                            <p>揭晓时间：<?php echo microt($item['q_end_time']); ?></p>
                            <p>夺宝时间：<?php echo microt($user_shop_time); ?></p>
                        </dd>
                    </dl>
                </div>
                <!--补丁3.1.5_b.0.1-->
                <div class="Announced_FrameBut">
                    <a href="javascript:;" class="Announced_But">本期详细计算结果</a>
                    <a href="javascript:;" class="Announced_But">看看有谁参与了</a>
                    <a href="javascript:;" class="Announced_But">看看有谁晒单</a>
                </div>
                <!--补丁3.1.5_b.0.1-->
                <div class="Announced_FrameBm"></div>
            </div>
            <div class="MaCenter">
                <p>商品获得者本期总共夺宝了<em class="orange"><?php echo $user_shop_number; ?></em>人次</p>
                <div id="userRnoId" class="MaCenterH">
                    <dl>
                        <dt><?php echo microt($user_shop_time); ?>
                        <dd>
                            <?php echo yunma($user_shop_codes,"li"); ?>
                        </dd>
                    </dl>
                </div>
            </div>
            <div id="divOpen" class="MaOff"><span>展开查看全部<s></s></span></div>
        </div>

        <?php include templates("index","first_qi");?>


    </div>
</div>
<!--登录窗口开始-->
<?php include templates("user","login_ajax");?>
<!--登录窗口结束-->

<script type="text/javascript" src="<?php echo G_TEMPLATES_STYLE; ?>/js/new/index_item.js"></script>


<!-- 计算结果 所有参与记录 晒单 -->
<div id="calculate" class="ProductTabNav">
    <div id="divMidNav" class="DetailsT_Tit">
        <div class="DetailsT_TitP">
            <ul>
                <li class="Product_DetT DetailsTCur"><span class="DetailsTCur">计算结果</span></li>
                <li class="All_RecordT"><span class="">所有参与记录</span></li>
                <li class="Single_ConT"><span class="">晒单</span></li>
            </ul>
        </div>
    </div>
</div>

<!--补丁3.1.6_b.0.1-->
<div id="divCalResult" class="Product_Content">

    <!--时时彩LVDENG修改-->
    <div class="ygRecord" name="div_tab">

        <div class="xhitem_jisuang" style="height: 220px;">
            <div class="xhitem_jisuang_left"><img src="<?php echo G_TEMPLATES_STYLE; ?>/images/new/item_01.png"/></div>
            <div class="xhitem_jisuang_right">
                <ul>
                    <li>
                        <h4>易中夺宝的规则</h4>
                        <div style="margin-bottom:2px;">
                            <p>每件商品参考市场价均分成若干“等份”，每份1元，对应1个幸运豆，支付一个幸运豆即可获得一个幸运码。</p>
                            <p>同一件商品可以多次购买，或者一次购买多份。</p>
                            <p>当一件商品所有“等份”全部售罄，按既定算法计算出中奖幸运码，拥有幸运码者即可获得该商品。</p>
                        </div>
                    </li>
                    <li>
                        <h4>幸运夺宝号计算方式</h4>
                        <div>
                            <p>1、取该商品最后购买时间前网站所有商品的最后100条购买时间记录 （限时购买商品取截止时间前网站所有商品100条购买时间记录）；</p>
                            <p>2、每个时间记录按时、分、秒、毫秒依次排列取数值并将这100个数值求和（A） ；</p>
                            <p>3、为保证公平公正公开，系统还会等待一小段时间，取最近一期中国乐彩彩票“时时彩”的开奖结果（一个五位数值B） ；</p>
                            <p>4、将A与B之和除以该商品总参与人次后取余数，余数加上10000001 即为“中奖幸运码”。
                                如果网站未满100条购买记录，则按照【 10000001 + (揭晓时间总和与时时彩中奖号码求和结果*100/参与人数) 的余数】即为“中奖幸运码”。</p>
                        </div>
                    </li>
                    <li>
                        <h4>时时彩异常处理</h4>
                        <div>
                            <p>如果时时彩数据出现异常，算法不变，时时彩号码默认为 <font color="red">0</font>。</p>
                        </div>
                    </li>
                </ul>
            </div>
        </div>

        <?php if(!$item['q_content']): ?>
        <!--2015/12/20加-->
        <div style="width: 1100px;height:25px;padding-top:15px;background: #ff754a;text-align:center;font-size:24px; color:#fff">
            未满100条计算结果
        </div>
        <div class="jisuang_jieguo">
            <div class="jisuang_jieguo_left"><img src="<?php echo G_TEMPLATES_STYLE; ?>/images/new/item_03.png"/></div>
            <div class="jisuang_jieguo_right">
                <div class="jisuang_jieguo_righttop">
                    <ul>
                        <li><span class="world_01">1</span>、求和：<?php echo $user_shop_time_add; ?>（揭晓时间求和结果）</li>
                        <li><span class="world_01">2</span>、第<?php echo $item['q_sscphase']; ?>期“老时彩”开奖号码：<span
                                class="laoshicai"></span> <a
                                href="http://baidu.lecai.com/lottery/draw/view/200/<?php echo $ssc_opentime; ?>?"
                                style="color:#ffe000" target="_blank">开奖查询>></a></li>
                        <li><span class="world_01">3</span>、求余：（<?php echo $user_shop_time_add; ?>+<span class="laoshicai"></span>）=
                            <?php echo $user_shop_time_addssc; ?> * 100 / <?php echo $item['canyurenshu']; ?>（商品所需人次）= <span class="yushu"><?php echo $user_shop_fmod; ?></span>（余数）
                        </li>
                        <li><span class="world_01">4</span>、<span class="yushu"><?php echo $user_shop_fmod; ?></span>（余数）+
                            10000001 = <span class="zhongjiang"><?php echo $item['q_user_code']; ?></span></li>
                    </ul>
                </div>
                <div class="jisuang_jieguo_rightbottom">
                    中奖幸运码：<?php echo $item['q_user_code']; ?>
                </div>
            </div>
        </div>
        <?php  else: ?>
        <ul class="Record_title">
            <li class="time">夺宝时间</li>
            <li class="nem">会员账号</li>
            <li class="name">商品名称</li>
            <li class="much">夺宝人次</li>
        </ul>
        <div class="RecordOnehundred"><h4>截止该商品揭晓购买时间【<?php echo microt($item['q_end_time']); ?>】最后100条全站购买时间记录</h4>
            <div class="FloatBox"></div>
            <?php $ln=1;if(is_array($item['q_content'])) foreach($item['q_content'] AS $record): ?>
            <?php 
            $itemid = $item['id'];
            $record_time = explode(".",$record['time']);
            $record['time'] = $record_time[0];
             ?>
            <ul class="Record_content">
                <li class="time"><b><?php echo date("Y-m-d",$record['time']); ?></b><?php echo date("H:i:s",$record['time']); ?>.<?php echo $record_time['1']; ?>
                </li>
                <li class="timeVal"><?php echo $record['timeadd']; ?></li>
                <li class="nem"><a class="gray02" href="<?php echo WEB_PATH; ?>/uname/<?php echo idjia($record['uid']); ?>"
                                   target="_blank"><?php echo get_user_name($record['uid']); ?></a></li>
                <li class="name"><a class="gray02" href="<?php echo WEB_PATH; ?>/goods/<?php echo $record['shopid']; ?>" target="_blank">（第<?php echo $record['shopqishu']; ?>期）<?php echo $record['shopname']; ?></a>
                </li>
                <li class="much"><?php echo $record['gonumber']; ?>人次</li>
            </ul>
            <?php  endforeach; $ln++; unset($ln); ?>
        </div>

        <?php 
        $shop_fadd= $item['q_counttime']+$ssc_code;
        $shop_fmod = fmod($shop_fadd,$item['canyurenshu']);

         ?>
        <!--2015/12/20加-->
        <div class="jisuang_jieguo">
            <div class="jisuang_jieguo_left"><img src="<?php echo G_TEMPLATES_STYLE; ?>/images/new/item_03.png"/></div>
            <div class="jisuang_jieguo_right">
                <div class="jisuang_jieguo_righttop">
                    <ul>
                        <li><span class="world_01">1</span>、求和：<?php echo $item['q_counttime']; ?>（上面100条参与记录的时间取值和）</li>
                        <li><span class="world_01">2</span>、第<?php echo $item['q_sscphase']; ?>期“老时彩”开奖号码：<span
                                class="laoshicai"></span> <a
                                href="http://baidu.lecai.com/lottery/draw/view/200/<?php echo $ssc_opentime; ?>?"
                                style="color:#ffe000" target="_blank">开奖查询>></a></li>
                        <li><span class="world_01">3</span>、求余：（<?php echo $item['q_counttime']; ?>+<span
                                class="laoshicai"></span>）% <?php echo $item['zongrenshu']; ?>（商品所需人次）= <span class="yushu"><?php echo $shop_fmod; ?></span>（余数）
                        </li>
                        <li><span class="world_01">4</span>、<span class="yushu"><?php echo $shop_fmod; ?></span>（余数）+ 10000001 =
                            <span class="zhongjiang"><?php echo $item['q_user_code']; ?></span></li>
                    </ul>
                </div>
                <div class="jisuang_jieguo_rightbottom">
                    中奖幸运码：<?php echo $item['q_user_code']; ?>
                </div>
            </div>
        </div>
        <?php endif; ?>

    </div>
    <script type="text/javascript">
        $(function () {
            /*老时彩*/
            var laoshicai = "<?php echo $ssc_code; ?>";
            var arr_lao = laoshicai.split("");
            var str_lao = '';
            for (var i = 0; i < arr_lao.length; i++) {
                str_lao += "<span>" + arr_lao[i] + "</span>";
            }
            $(".jisuang_jieguo_righttop").find(".laoshicai").html(str_lao);

            /*余数*/
            $(".jisuang_jieguo_righttop").find(".yushu").each(function () {
                getyu($(this));
            });

            /*中奖*/
            getyu($(".jisuang_jieguo_righttop").find(".zhongjiang"));

        });
        function getyu(obj) {
            var yushu = obj.text();
            var arr_yu = yushu.split("");
            var str_yu = '';
            for (var i = 0; i < arr_yu.length; i++) {
                str_yu += "<span>" + arr_yu[i] + "</span>";
            }
            obj.html(str_yu);
        }
    </script>
    <!--时时彩LVDENG修改-->

    <!-- 购买记录20条 -->
    <div name="div_tab" id="bitem" class="AllRecordCon">
        <iframe id="iframea_bitem" g_src="<?php echo WEB_PATH; ?>/go/goods/go_record_ifram/<?php echo $itemid; ?>/20"
                style="width:978px; border:none;height:0px;" frameborder="0" scrolling="no"></iframe>
    </div>
    <!-- /购买记录20条 -->

    <!-- 晒单 -->
    <div name="div_tab" id="divPost" class="Single_Content">
        <iframe id="iframea_divPost" g_src="<?php echo WEB_PATH; ?>/go/shaidan/itmeifram/<?php echo $itemid; ?>"
                style="width:978px; border:none;height:0px;" frameborder="0" scrolling="no"></iframe>
    </div>
    <!-- 晒单 -->
</div>

<link rel="stylesheet" type="text/css" href="<?php echo G_TEMPLATES_CSS; ?>/xiaohe_zhe.css"/>

<div class="xiaohe_zhe"></div>
<div class="xiaohe_content">
    <div class="xiaohe_content_first"><span id="xiaohe_content_close">x</span></div>
    <div class="xiaohe_content_sec"><p>商品获得者本期参与了</p><span><?php echo $user_shop_number; ?></span>人次</div>
    <div class="xiaohe_content_three"><?php echo microt($user_shop_time); ?></div>
    <div class="xiaohe_content_fourth">
        <ul></ul>
    </div>
    <div style="height:20px"></div>
    <div class="xiaohe_content_five"></div>
</div>
<script type="text/javascript">
    var q_user_code = "<?php echo $item['q_user_code']; ?>";
    //显示更多得将这夺宝号码
    getzhong_nubmer($("#divOpen"), 0);
    $('#xiaohe_content_close').click(function () {
        $('.xiaohe_content').hide();
    });
    //显示更多登录用户的夺宝号码
    if ($(".mycanyu").find(".see_more").length > 0) {
        getzhong_nubmer($(".mycanyu").find(".see_more"), 1);
    }


    //点击登录
    $(".itemsee_zhonglelogin span").click(function () {
        ajax_login();

    });

    function set_iframe_height(fid, did, height) {
        $("#" + fid).css("height", height);
    }

    $(function () {
        var fouli = $(".DetailsT_TitP ul li");
        var divCalResult = $("div[name='div_tab']");
        fouli.click(function () {
            var index = fouli.index(this);
            fouli.removeClass("DetailsTCur").eq(index).addClass("DetailsTCur");
            var iframe = divCalResult.hide().eq(index).find("iframe");
            if (typeof(iframe.attr("g_src")) != "undefined") {
                iframe.attr("src", iframe.attr("g_src"));
                iframe.removeAttr("g_src");
            }

            divCalResult.hide().eq(index).show();
        });
        <!--补丁3.1.6_b.0.3-->


        $(".Announced_But").click(function () {
            var next_li = $(".DetailsT_TitP ul>li");
            var index = $(this).index()
            next_li.eq(index).click();
        });


        $(window).scroll(function () {
            if ($(window).scrollTop() >= 941) {
                $("#divMidNav").addClass("nav-fixed");
            } else if ($(window).scrollTop() < 941) {
                $("#divMidNav").removeClass("nav-fixed");
            }
        });
    });

    //查看更多夺宝号的弹窗
    /*
     tan_obj 为弹框的对象，0为查看别人的，1为查看自己的
     */
    function getzhong_nubmer(obj, tan_obj) {
        tan_obj = tan_obj ? 1 : 0;
        obj.click(function () {

            $(".xiaohe_zhe").show();
            is_open = false;
            $(".head_nav").css("position", "static");
            $(".xiaohe_content").show().css({"left": "35%", "top": "8%"});
            if (tan_obj == 0) {
                $(".xiaohe_content_sec p").text('商品获得者本期参与了');
                $(".xiaohe_content_three").text('<?php echo microt($user_shop_time); ?>');
                $(".xiaohe_content_sec span").text('<?php echo $user_shop_number; ?>');
                var haoma = $("#userRnoId").find("dd").html();
                var zhongjiang = q_user_code;

                $(".xiaohe_content_fourth").html(haoma);
                $(".xiaohe_content_fourth li").each(function () {
                    if ($(this).text() == zhongjiang) {
                        $(this).css({"color": "red", "font-weight": "700"});
                    }
                });
            } else {
                $(".xiaohe_content_sec p").text('您本期参与了');
                $(".xiaohe_content_three").text('<?php echo $my_zhongle_time; ?>');
                $(".xiaohe_content_sec span").text('<?php echo $my_zhongle_count; ?>');
                var ownner_number = "<?php echo yunma($my_zhongle_number,'li'); ?>";
                $(".xiaohe_content_fourth").html(ownner_number);
            }
        });

        if ($(".xiaohe_zhe").css("display") == "none") {
            $(".xiaohe_content_first span").click(function () {
                $(".xiaohe_zhe,.xiaohe_content").hide();
                is_open = true;
            });

        }

    }

</script>
<?php include templates("index","footer");?>