<?php defined('G_IN_SYSTEM')or exit('No permission resources.'); ?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="renderer" content="webkit|ie-comp|ie-stand">
<!-- <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"> -->
<title>购物车 - <?php echo _cfg("web_name"); ?></title>
<meta name="keywords" content="<?php if(isset($keywords)): ?><?php echo $keywords; ?><?php  else: ?><?php echo _cfg("web_key"); ?><?php endif; ?>" />
<meta name="description" content="<?php if(isset($description)): ?><?php echo $description; ?><?php  else: ?><?php echo _cfg("web_des"); ?><?php endif; ?>" />
<link rel="stylesheet" type="text/css" href="<?php echo G_TEMPLATES_STYLE; ?>/css/Comm.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo G_TEMPLATES_STYLE; ?>/css/CartList.css"/>
<script type="text/javascript" src="<?php echo G_GLOBAL_STYLE; ?>/global/js/jquery-1.8.3.min.js"></script>
<script type="text/javascript" src="<?php echo G_TEMPLATES_STYLE; ?>/js/jquery.cookie.js"></script>
<script type="text/javascript" src="<?php echo G_WEB_PATH; ?>/statics/plugin/layer/new/newlayer.js"></script><!--layer-->  

</head>
<body>
<div class="logo">
	<div class="float">
		<span class="logo_pic"><a href="<?php echo G_WEB_PATH; ?>" class="a" title="<?php echo _cfg("web_name"); ?>">
			<img src="<?php echo G_UPLOAD_PATH; ?>/<?php echo Getlogo(); ?>"/>
		</a></span>
		<span class="tel"><a href="<?php echo G_WEB_PATH; ?>" style="color:#999;">返回首页</a></span>
	</div>
</div>
<div class="shop_process">
	<ul class="process">
		<li class="first_step">第一步：提交订单</li>
		<li class="arrow_1"></li>
		<li class="secend_step">第二步：网银支付</li>
		<li class="arrow_2"></li>
		<li class="third_step">第三步：支付成功 等待揭晓</li>
		<li class="arrow_2"></li>
		<li class="fourth_step">第四步：揭晓获得者</li>
		<!-- <li class="arrow_2"></li>
		<li class="fifth_step">第五步：晒单奖励</li> -->
	</ul>
	<div class="i_tips"></div>
	<div class="submitted">
		<ul class="order">
			<li class="top">
				<span class="goods">商品</span>
				<span class="name">名称</span>
				<span class="moneys">价值</span>
				<span class="money">夺宝单价</span>
				<span class="num">夺宝人次</span>
				<span class="xj">小计</span>
				<span class="do">操作</span>
			</li>
			<?php $ln=1;if(is_array($shoplist)) foreach($shoplist AS $shops): ?>         
			<li class="end" id="shoplist<?php echo $shops['id']; ?>" style="position:relative;">

				<!-- 复选框  2015/12/22 -->
				<span style="position:absolute;top:45px;left:15px;">
					<input type="checkbox" name="choose_lab" value="<?php echo $shops['id']; ?>"/>
				</span>
				<span class="goods">
					<a href="<?php echo WEB_PATH; ?>/goods/<?php echo $shops['id']; ?>">
                   	 <img src="<?php echo G_UPLOAD_PATH; ?>/<?php echo $shops['thumb']; ?>" />
                    </a>                    
				</span>
				<span class="name">
					<a href="<?php echo WEB_PATH; ?>/go/index/item/<?php echo $shops['id']; ?>" style="height:25px;">
						<?php echo $shops['title']; ?>  
						<?php if($shops['is_shi']==1): ?>  
						(5元限购)
						<?php endif; ?>
					</a>
<!--                                            <p style="height:25px;clear: both;">
                                                    已购次数：<span class="userbought_<?php echo $shops['id']; ?>" max-canbuy="<?php echo $shops['user_can_buy']; ?>"><?php echo $shops['user_bought']; ?></span>
                                            </p>
                                            <p>总需 <span class="color"><?php echo $shops['zongrenshu']; ?></span>人次参与，还剩 
                                                <span class="gorenci"><?php echo $shops['cart_shenyu']; ?></span> 人次
                                            </p>-->
                                        <?php if($shops['pk_product']==1): ?>
                                            <p style="height:25px;clear: both;">
                                                    已购次数：<span class="userbought_<?php echo $shops['id']; ?>" max-canbuy="<?php echo $shops['user_can_buy']; ?>"><?php echo $shops['pk_cishu']; ?></span>
                                            </p>
                                            <p>总需 <span class="color"><?php echo $shops['pk_number']; ?></span>人次参与，还剩 
                                                <span class="gorenci"><?php echo $shops['pk_number']-$shops['pk_cishu']; ?></span> 人次
                                            </p>
                                        <?php  else: ?>
                                            <p style="height:25px;clear: both;">
                                                    已购次数：<span class="userbought_<?php echo $shops['id']; ?>" max-canbuy="<?php echo $shops['user_can_buy']; ?>"><?php echo $shops['user_bought']; ?></span>
                                            </p>
                                            <p>总需 <span class="color"><?php echo $shops['zongrenshu']; ?></span>人次参与，还剩 
                                                <span class="gorenci"><?php echo $shops['cart_shenyu']; ?></span> 人次
                                            </p>
                                        <?php endif; ?>
				</span>
				<span class="moneys">￥<?php echo $shops['money']; ?></span>
				<span class="money"><span class="color">￥<b><?php echo $shops['yunjiage']; ?></b></span></span>
				<span class="num">				
					<dl class="add">					
					<dd>    
                                                <?php if($shops['pk_product']==1): ?>
                                                <!--<input type="type" readonly val="<?php echo $shops['id']; ?>" onkeyup="value=value.replace(/\D/g,'')" value="<?php echo $shops['cart_gorenci']; ?>" class="amount" /></dd>-->
                                                <input type="type" val="<?php echo $shops['id']; ?>" readonly onkeyup="value=value.replace(/\D/g,'')" value="<?php echo $shops['cart_gorenci']; ?>" class="amount" /></dd>
                                                    <dd>
<!--							<a href="JavaScript:;" val="<?php echo $shops['id']; ?>" class="jias"></a>
							<a href="JavaScript:;" val="<?php echo $shops['id']; ?>" class="jians"></a>-->
                                                        <a href="JavaScript:;" val="<?php echo $shops['id']; ?>" class="jia"></a>
							<a href="JavaScript:;" val="<?php echo $shops['id']; ?>" class="jian"></a>
                                                        <input type="hidden" name="is_shi" value="<?php echo $shops['is_shi']; ?>" id="is_shi"/>
                                                        <input type="hidden" name="pk_product" value="<?php echo $shops['pk_product']; ?>" id="pk_product"/>
						</dd>
                                                <?php  else: ?>
                                                <input type="type" val="<?php echo $shops['id']; ?>" onkeyup="value=value.replace(/\D/g,'')" value="<?php echo $shops['cart_gorenci']; ?>" class="amount" /></dd>
                                                <dd>
							<a href="JavaScript:;" val="<?php echo $shops['id']; ?>" class="jia"></a>
							<a href="JavaScript:;" val="<?php echo $shops['id']; ?>" class="jian"></a>
                                                        <input type="hidden" name="is_shi" value="<?php echo $shops['is_shi']; ?>" id="is_shi"/>
                                                        <input type="hidden" name="pk_product" value="<?php echo $shops['pk_product']; ?>" id="pk_product"/>
						</dd>
                                                <?php endif; ?>
						
						                        
					</dl>
                    <p class="message" style="clear:both;text-align:center"></p>
				</span>
				<span  class="xj"><?php echo $shops['cart_xiaoji']; ?></span>
				<span class="do"><a href="javascript:;" onclick="delcart(<?php echo $shops['id']; ?>)"  class="delgood">删除</a></span> 
			</li>		
			<?php  endforeach; $ln++; unset($ln); ?>
			<li class="ts">
				<div style="float:left;margin:0 0 0 15px;">
					<input type="button" value="全选" onclick="do_choose(1)"/>&nbsp;&nbsp;&nbsp;&nbsp;
					<a href="javascript:;" onclick="do_del_choose()">删除选中</a>
				</div>
				<p class="right">夺宝金额总计:￥<span id="moenyCount"><?php echo $MoenyCount; ?></span></p>
			</li>
		</ul>
	</div>
	<div class="xcartlist_j ">
    	<a href="<?php echo WEB_PATH; ?>" id="but_on"  class="w-button w-button-xl w-button-main xcartlist_j_left">返回首页</a>
		<div id="but_ok" type="button" value=""  class="w-button w-button-xl w-button-main xcartlist_j_right" name="submit"/>提交订单</div>
    </div>
</div>


<script type="text/javascript"> 
var info=<?php echo $Cartshopinfo; ?>;
console.log(info);
var numberadd=$(".jia");
var numbersub=$(".jian");
var xiaoji=$(".xj");
var num=$(".amount");
var message=$(".message");
var moenyCount=$("#moenyCount");

$(function(){
	$.cookie('Cartlist',$.toJSON(info),{expires:7,path:'/'});//将购物信息存入cookie  保存七天  json数据		
	$("#but_ok").click(function(){
		console.log('111');
		var countmoney=parseInt(moenyCount.text());		
		if(countmoney > 0){		
			//$.cookie('Cartlist','',{path:'/'});
			$.cookie('Cartlist',$.toJSON(info),{expires:7,path:'/'});//将购物信息存入cookie  保存七天  json数据
			window.location.href='<?php echo WEB_PATH; ?>/member/cart/pay/'+new Date().getTime();
			
		}else{
			return false;
		}
	});
});
function UpdataMoney(shopid,number,zindex){		
		var number = parseInt(number);
		info['MoenyCount']=info['MoenyCount']-info[shopid]['money']*info[shopid]['num']+info[shopid]['money']*number;
		info[shopid]['num']=number;
		var xjmoney=xiaoji.eq(zindex+1);
			xjmoney.text(info[shopid]['money']*number+'.00');
			moenyCount.text(info['MoenyCount']+'.00');
}


function delcart(id){
	layer.confirm('确定要删除该商品吗？', {icon: 3, title:'提示', move:false}, function(index){
		info['MoenyCount'] = info['MoenyCount']-info[id]['money']*info[id]['num'];
		$("#shoplist"+id).hide();
		$("#moenyCount").text(info['MoenyCount']+".00");
		delete info[id];
		//$.cookie('Cartlist','',{path:'/'});
		var rel_val = parseInt($("#btnMyCart em").text())-1;
		$("#btnMyCart em").text(rel_val);
		$.cookie('Cartlist',$.toJSON(info),{expires:30,path:'/'});
	    layer.close(index);
		location.reload();
	});

	
}

/*2015/12/06*/
var input_html = '';
num.blur(function(){
	var shopid=$(this).attr("val");
	var current_val = '';
	var zindex=num.index(this);	
	//是否限购
	var is_shi = $(this).parents(".add").find("#is_shi").val();
	//用户最大购买人数
	var max_canbuy = $('.userbought_'+shopid).attr('max-canbuy');
	//已经购买的人数
	var bought_number = parseInt($('.userbought_'+shopid).text());
        //是否PK
        var pk_product = $(this).parents(".add").find("#pk_product").val();
	
	if($.trim($(this).val()) == ''){
		input_html = '<span style="color:red">输入合法的数字</span>';			
		$(this).val(1);
	}

	if(parseInt($(this).val()) > max_canbuy){
		input_html = '<span style="color:red">购买人次不能超过'+max_canbuy+'次</span>';		
		$(this).val(max_canbuy);	
	}
	
	if(is_shi != 1 ){
//		if(current_val>=1 && current_val <=4){
//			input_html = '<span style="color:red">参与人次越多，获奖机会越大哦！</span>';	
//		}else if(current_val>4 && current_val <=10){
//			input_html = '<span style="color:red">参与10人次，极有机会获奖!</span>';	
//		}else{
//			input_html = '<span style="color:red">加大参与人次，夺宝在望</span>';
//		}
	}
	current_val = parseInt($(this).val());
	message.eq(zindex).html(input_html);	
	UpdataMoney(shopid,current_val,zindex);	
});
var add_html = '';
numberadd.click(function(){
	var shopid=$(this).attr('val');		
	var zindex=numberadd.index(this);
	
	var thisnum=num.eq(zindex);
	//用户最大购买人数
	var max_canbuy = $('.userbought_'+shopid).attr('max-canbuy');
	//已经购买的人数
	var bought_number = parseInt($('.userbought_'+shopid).text());
	if(info[shopid]['num'] >= max_canbuy){
		add_html = '<span style="color:red">购买人次不能超过'+max_canbuy+'次</span>';
		message.eq(zindex).html(add_html);
		return;
	}
	var is_shi = $(this).siblings("#is_shi").val();
        var pk_product = $(this).siblings("#pk_product").val();
        if(pk_product == 1){
            if(info[shopid]['num'] >= 1){
		add_html = '<span style="color:red">PK产品夺宝人次不能改变</span>';
		message.eq(zindex).html(add_html);
		return;
            }
        }
	var number=info[shopid]['num']+1;//加减
	if(is_shi == 1){
		add_html = '<span style="color:red">该产品限购5人次';	
	}else{
//		if(number>=1 && number <=4){
//			add_html = '<span style="color:red">参与人次越多，获奖机会越大哦！</span>';	
//		}else if(number>4 && number <=10){
//			add_html = '<span style="color:red">参与10人次，极有机会获奖!</span>';	
//		}else{
//			add_html = '<span style="color:red">加大参与人次，夺宝在望</span>';
//		}	
	}
	if(number<0){
		number = 1;
	}
	
	message.eq(zindex).html(add_html);
	thisnum.val(number);
	UpdataMoney(shopid,number,zindex);
});

var sub_html = '';
numbersub.click(function(){
	var shopid=$(this).attr('val');		
	var zindex=numbersub.index(this);
	var thisnum=num.eq(zindex);
	
	var is_shi = $(this).siblings("#is_shi").val();
	var number=info[shopid]['num']-1;//加减
	if(number < 1){
		number = 1;
		sub_html = '<span style="color:red">夺宝次数不能少于1次</span>';	
	}
	message.eq(zindex).html(sub_html);
        var pk_product = $(this).siblings("#pk_product").val();
        if(pk_product == 1){
            if(info[shopid]['num'] >= 1){
		add_html = '<span style="color:red">PK产品夺宝人次不能改变</span>';
		message.eq(zindex).html(add_html);
		return;
            }
        }
	if(is_shi == 1){
		sub_html = '<span style="color:red">该产品限购5人次</span>';
	}else{
//		if(number>=1 && number <=4){
//			sub_html = '<span style="color:red">参与人次越多，获奖机会越大哦！</span>';	
//		}else if(number>4 && number <=10){
//			sub_html = '<span style="color:red">参与10人次，极有机会获奖!</span>';	
//		}else{
//			sub_html = '<span style="color:red">加大参与人次，夺宝在望</span>';
//		}	
	}

	message.eq(zindex).html(sub_html);
	thisnum.val(number);
	UpdataMoney(shopid,number,zindex);
});

//全选与反选  20151222
function do_choose(type)
{
	if(type==1){
		$('input[name="choose_lab"]').attr("checked",true);
	}else{
		var all_lab = $('input[name="choose_lab"]');
		$.each(all_lab,function(n,v){
			this.checked = !this.checked;
		});
	}	
}
//删除选中   20151222
function do_del_choose()
{
	if($('input[name="choose_lab"]:checked').val() > 0){
		layer.confirm('确定要删除选中的商品吗？', {icon: 3, title:'提示', move:false}, function(index){
			var all_lab = $('input[name="choose_lab"]');
			$.each(all_lab,function(n,v){
				if(this.checked){
					var id = $(this).val();
					info['MoenyCount'] = info['MoenyCount']-info[id]['money']*info[id]['num'];
					$("#shoplist"+id).hide();
					$("#moenyCount").text(info['MoenyCount']+".00");
					delete info[id];
					//$.cookie('Cartlist','',{path:'/'});
					var rel_val = parseInt($("#btnMyCart em").text())-1;
					$("#btnMyCart em").text(rel_val);
					$.cookie('Cartlist',$.toJSON(info),{expires:30,path:'/'});
				}
			});
			layer.close(index);
			location.reload();
		});
	}
}

</script> 
<!--footer 开始-->
<?php include templates("index","footer");?>