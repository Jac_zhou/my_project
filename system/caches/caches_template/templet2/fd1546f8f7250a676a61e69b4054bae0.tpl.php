<?php defined('G_IN_SYSTEM')or exit('No permission resources.'); ?><?php include templates("index","header");?>
<link rel="stylesheet" type="text/css" href="<?php echo G_TEMPLATES_STYLE; ?>/css/layout-Frame.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo G_TEMPLATES_STYLE; ?>/css/layout-setUp.css"/>

<script type="text/javascript" src="<?php echo G_TEMPLATES_STYLE; ?>/js/area.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo G_TEMPLATES_CSS; ?>/new/alert.css"/>
<script type="text/javascript" src='<?php echo G_TEMPLATES_JS; ?>/new/alert.js'></script>
<script type="text/javascript">
$(function(){		
	var demo=$(".registerform").Validform({
		tiptype:2,
		ignoreHidden:true,
		datatype:{
			"tel":/^(([0\+]\d{2,3}-)?(0\d{2,3})-)(\d{7,8})(-(\d{3,}))?$/,
		},
		beforeSubmit:function(curform){
			//在验证成功后，表单提交前执行的函数，curform参数是当前表单对象。
			//这里明确return false的话表单将不会提交;			
			//$.Hidemsg();
			var x_data = $(curform).serialize();
			var action_url = '';
			if($("#div_consignee2").css("display") == "block"){
				action_url = '<?php echo WEB_PATH; ?>/member/home/updateddress';
				x_data += "&action=doupdate";
			}else{
				action_url = '<?php echo WEB_PATH; ?>/member/home/useraddress';
			}
			$.ajax({
				url:action_url,
				data:x_data,
				type:"POST",
				success:function(data){
					var _data = $.parseJSON(data);
					if(_data.error=="1"){
						msg_show.waring(_data.msg);
					}else if(_data.error=="0"){
						window.location.href=_data.url;
					}
				}
			});
			return false;
		}
	});	
	demo.tipmsg.w["tel"]="请正确输入电话号码(区号、号码必填，用“-”隔开)";
	
	
	/*2015/12/09设为默认地址*/
	$(".setdefaultaddress").click(function(){
		var default_id = $(this).siblings("input").val();
		var rurl = '<?php echo WEB_PATH; ?>/member/home/morenaddress';
		$.ajax({
			url:rurl,
			data:{default_id:default_id},
			type:"POST",
			success:function(x_data){
				_data=$.parseJSON(x_data);
				
				if(_data.error=="1"){
					msg_show.waring(_data.msg);
				}else if(_data.error=="0"){
					//msg_show.success(_data.msg,_data.url);
					window.location.href=_data.url;
				}
			}
		});
	});
	
	/*2015/12/09删除地址*/
	$(".del_address").click(function(){
		var currr_obj = $(this);
		msg_show.promptmsg("是否删除此条记录？",{"yes":function(){
			var default_id = currr_obj.siblings("input").val();
			var rurl = '<?php echo WEB_PATH; ?>/member/home/deladdress';
			$.ajax({
				url:rurl,
				data:{default_id:default_id},
				type:"POST",
				success:function(_data){
					_data=$.parseJSON(_data);
					if(_data.error=="1"){
						msg_show.waring(_data.msg);
					}else if(_data.error=="0"){
						msg_show.success(_data.msg,_data.url);
					}
				}
			});
		
		}});
		
	
	});
	
});
$(function(){
	$("#btnAddnewAddr").click(function(){
		$("#div_consignee").show();
		$("#btnAddnewAddr").hide();
	});
	$("#btn_consignee_cancle").click(function(){
		$("#div_consignee").hide();
		$("#btnAddnewAddr").show();
	});
});
$(function(){
	$(".xiugai").click(function(){
		$("#btnAddnewAddr").hide();
		$("#div_consignee").hide();
	});
	$("#btn_consignee_cancle2").click(function(){
		$("#div_consignee2").hide();
		$("#btnAddnewAddr").show();
	});
});
function update(id){	
	$("#div_consignee2").show;
	setup3();
	$("#registerform3").attr("action","<?php echo WEB_PATH; ?>/member/home/updateddress/"+id);
	var str=$("#dizh_"+id).html();
	var spl=str.split(",");
	$("#province3").append('<option selected value="'+spl[0]+'">'+spl[0]+'</option>');
	$("#city3").append('<option selected value="'+spl[1]+'">'+spl[1]+'</option>');
	$("#county3").append('<option selected value="'+spl[1]+'">'+spl[1]+'</option>');
	$("#dizh2").val(spl[3]);
	$("#mob2").val($("#mob_"+id).html());
	$("#qq2").val($("#listqq_"+id).html());
	var youbain =$("#yb_"+id).html();
	if(youbain=='' || youbain =='未填写'){
		youbain = ''; 
	} 
	$("#yb2").val(youbain);
	$("#shr2").val($("#shr_"+id).html());
	$("#div_consignee2").show();
	$("input[name=address_id]").val(id);
};



</script>
<?php include templates("member","member_top");?>
<div class="memberxhcontains">
	<?php include templates("member","member_nav");?>

	<div class="R-content">
	
	<?php if($count==0): ?>
	<div  class="addAddress">
		<dl>添加收货地址</dl>
			
		<input type="hidden" class="address_action" value="add"/>
		<form class="registerform" method="post" action="">
		<table border="0" cellpadding="0" cellspacing="0">
		<tbody>

		<tr>
			<script>var s=["province","city","county"];</script>
			<td><label>所在地区：</label></td>
			<td>
				<select datatype="*" nullmsg="请选择有效的省市区" class="select" id="province" runat="server" name="sheng"></select>
				<select datatype="*" nullmsg="请选择有效的省市区" id="city" runat="server" name="shi"></select>
				<select datatype="*" nullmsg="请选择有效的省市区" id="county" runat="server" name="xian"></select>
				<em id="ship_address_valid_msg" class="red">*</em> 	
				<script type="text/javascript">setup()</script>
			</td>
			<td><div class="Validform_checktip"></div></td>
		</tr>
		<tr>
			<td><label>街道地址：</label></td>
			<td>
				<input datatype="*1-100" nullmsg="请填街道地址！" errormsg="范围在100之间！" name="jiedao" type="text" class="street" maxlength="100" />
				<em id="ship_address_valid_msg" class="red">*</em> 			
			</td>
			<td><div class="Validform_checktip">(不需要重复填写省/市/区)</div></td>
		</tr>
		<tr>
			<td><label>邮政编码：</label></td>
			<td>
				<input datatype="p" ignore="ignore" errormsg="邮政编码错误！" name="youbian" type="text" maxlength="6" id="txt_ship_zip" class="inputTxt" value=""> 
				<font><a href="http://alexa.ip138.com/post/Search.aspx" class="blue" target="_blank">邮编查询</a></font>
			</td>
			<td><div class="Validform_checktip"></div></td>
		</tr>
		<tr>
			<td><label>收货人：</label></td>
			<td>
				<input datatype="*" nullmsg="收货人不能为空" name="shouhuoren" type="text" maxlength="20" id="txt_ship_name" class="inputTxt" value="">
				<em class="red" id="ship_name_valid_msg">*</em>
			</td>
			<td><div class="Validform_checktip"></div></td>
		</tr>
		<tr>
			<td><label>手机号码：</label></td>
			<td>
				<input datatype="m" nullmsg="手机号不能为空" errormsg="手机号不对" name="mobile" type="text" id="txt_ship_mb" value="" class="inputTxt" maxlength="11">
				<em id="ship_mb_valid_msg" class="red">*</em>
				<td><div class="Validform_checktip"></div></td>
			</td>
		</tr>
		<tr>
			<td><label>&nbsp;</label></td>
			<td>
				<input style="margin-right:20px;" name="submit" type="submit" class="orangebut" id="btn_consignee_save" value="保存" title="保存"> 
			</td>
		</tr>
        <tr><td></td></tr>
        <tr><td><span style="font-size:12px; color:#db3752;">注：带*的为必填项</span></td></tr>
		</tbody>
		</table>
		</form>
	</div>
	<?php endif; ?>
	<?php if($count>0): ?>
	<div id="addressListDiv" class="list-tab detailAddress gray01" style="">
		<ul class="listTitle tdTitle">
			<li class="pad" style="width:25%;">详细地址</li>
			<li style="width:10%; text-align:center">邮政编码</li>
			<li style="width:20%;text-align:center">收货人</li>
			<li style="width:10%;text-align:center">电话号码</li>
			<li style="width:20%;text-align:center">操作</li>
		</ul>					
		<?php $ln=1;if(is_array($member_dizhi)) foreach($member_dizhi AS $v): ?>
		<ul class="liBg">
			<li id="dizh_<?php echo $v['id']; ?>" class="pad" style="width:25%;"><?php echo $v['sheng']; ?>,<?php echo $v['shi']; ?>,<?php echo $v['xian']; ?>,<?php echo $v['jiedao']; ?></li>
			<li id="yb_<?php echo $v['id']; ?>" style="width:10%; text-align:center"><?php if(!$v['youbian']): ?>未填写<?php  else: ?><?php echo $v['youbian']; ?><?php endif; ?></li>
			<li id="shr_<?php echo $v['id']; ?>" style="width:20%; text-align:center"><?php echo $v['shouhuoren']; ?></li>
			<li id="mob_<?php echo $v['id']; ?>" style="width:10%; text-align:center"><?php echo $v['mobile']; ?></li>
			<?php if($v['default']=='Y'): ?>
			<li class="orange" style="width:20%;text-align:center">
				默认地址
				<a class="xiugai" href="javascript:;" id="update<?php echo $v['id']; ?>" onclick="update(<?php echo $v['id']; ?>)" title="修改">修改</a>
			</li>			
			<?php  else: ?>
			<li class="lightBlue" style="width:20%;text-align:center">
				<a class="blue setdefaultaddress" href="javascript:void(0)">设为默认地址</a>
                <input type="hidden" name="default_id" value="<?php echo $v['id']; ?>" />
				<a class="xiugai" href="javascript:;"   onclick="update(<?php echo $v['id']; ?>)" title="修改">修改</a>
				<a class='del_address' href="javascript:;" >删除</a>
			</li>		
			<?php endif; ?>			
		</ul>
		<?php  endforeach; $ln++; unset($ln); ?>
	</div>
	<?php if($count<5): ?>
	<div class="add" style=" margin-left:20px;"><input id="btnAddnewAddr" type="button" class="orangebut" value="新增收货地址" style="display: block;"></div>
	<?php endif; ?>
	<?php endif; ?>
	<?php if($count<=5): ?>	            
	<div id="div_consignee" class="addAddress" style="display:none;">
		<dl>添加收货地址</dl>
		
		<form class="registerform" method="post" action="">
		
		<table border="0" cellpadding="0" cellspacing="0">
		<tbody>
		
		<tr>
		<script>var s2=["province2","city2","county2"];</script>
			<td><label>所在地区：</label></td>
			<td>
				<select datatype="*" nullmsg="请选择有效的省市区" class="select" id="province2" runat="server" name="sheng"></select>
				<select datatype="*" nullmsg="请选择有效的省市区" id="city2" runat="server" name="shi"></select>
				<select datatype="*" nullmsg="请选择有效的省市区" id="county2" runat="server" name="xian"></select>
				<em id="ship_address_valid_msg" class="red">*</em> 	
				<script type="text/javascript">setup2()</script>
			</td>
			<td><div class="Validform_checktip"></div></td>
		</tr>
		<tr>
			<td><label>街道地址：</label></td>
			<td>
				<input datatype="*1-100" nullmsg="请填街道地址！" errormsg="范围在100之间！" name="jiedao" type="text" class="street" maxlength="100" />
				<em id="ship_address_valid_msg" class="red">*</em> 			
			</td>
			<td><div class="Validform_checktip">(不需要重复填写省/市/区)</div></td>
		</tr>
		<tr>
			<td><label>邮政编码：</label></td>
			<td>
				<input datatype="p" ignore="ignore" errormsg="邮政编码错误！" name="youbian" type="text" maxlength="6" id="txt_ship_zip" class="inputTxt" value=""> 
				<font><a href="http://alexa.ip138.com/post/Search.aspx" class="blue" target="_blank">邮编查询</a></font>
			</td>
			<td><div class="Validform_checktip"></div></td>
		</tr>
		<tr>
			<td><label>收货人：</label></td>
			<td>
				<input datatype="*" nullmsg="收货人不能为空" name="shouhuoren" type="text" maxlength="20" id="txt_ship_name" class="inputTxt" value="">
				<em class="red" id="ship_name_valid_msg">*</em>
			</td>
			<td><div class="Validform_checktip"></div></td>
		</tr>
		<tr>
			<td><label>手机号码：</label></td>
			<td>
				<input datatype="m" nullmsg="手机号不能为空" errormsg="手机号不对" name="mobile" type="text"  class="inputTxt" maxlength="11">
				<em id="ship_mb_valid_msg" class="red">*</em>
				<td><div class="Validform_checktip"></div></td>
			</td>
		</tr>
		
		<tr>
			<td><label>&nbsp;</label></td>
			<td>
				<input style="margin-right:20px;" name="submit" type="submit" class="orangebut" id="btn_consignee_save" value="保存" title="保存"> 
				<input type="button" class="cancelBtn" value="取消" id="btn_consignee_cancle" title="取消">
			</td>
		</tr>
        <tr><td></td></tr>
        <tr><td><span style="font-size:12px; color:#db3752;">注：带*的为必填项</span></td></tr>
		</tbody>
		</table>
		</form>
        
	</div>
	<?php endif; ?>
	<div id="div_consignee2" class="addAddress" style="display:none;">
		<dl>修改收货地址</dl>
		<input type="hidden" class="address_action" value="updata"/>
		<script>var s3=["province3","city3","county3"];</script>	
		 
		<form id="registerform3" class="registerform" method="post" >
		<table border="0" cellpadding="0" cellspacing="0">
		<tbody>
		<tr>		
			<td><label>所在地区：</label></td>
			<td>
				<select datatype="*" nullmsg="请选择有效的省市区" class="select" id="province3" runat="server" name="sheng"></select>
				<select datatype="*" nullmsg="请选择有效的省市区" id="city3" runat="server" name="shi"></select>
				<select datatype="*" nullmsg="请选择有效的省市区" id="county3" runat="server" name="xian"></select>
				<em id="ship_address_valid_msg" class="red">*</em> 				
			</td>
			<td><div class="Validform_checktip"></div></td>
		</tr>
		<tr>
			<td><label>街道地址：</label></td>
			<td>
				<input  id="dizh2" datatype="*1-100" nullmsg="请填街道地址！" errormsg="范围在100之间！" name="jiedao" type="text" class="street" maxlength="100" />
				<em id="ship_address_valid_msg" class="red">*</em> 			
			</td>
			<td><div class="Validform_checktip">(不需要重复填写省/市/区)</div></td>
		</tr>
		<tr>
			<td><label>邮政编码：</label></td>
			<td>
				<input id="yb2" datatype="p" ignore="ignore" errormsg="邮政编码错误！" name="youbian" type="text" maxlength="6" class="inputTxt" value=""> 
				<font><a href="http://alexa.ip138.com/post/Search.aspx" class="blue" target="_blank">邮编查询</a></font>
			</td>
			<td><div class="Validform_checktip"></div></td>
		</tr>
		<tr>
			<td><label>收货人：</label></td>
			<td>
				<input id="shr2" datatype="*" nullmsg="收货人不能为空" name="shouhuoren" type="text" maxlength="20" class="inputTxt" value="">
				<em class="red" id="ship_name_valid_msg">*</em>
			</td>
			<td><div class="Validform_checktip"></div></td>
		</tr>
		<tr>
			<td><label>手机号码：</label></td>
			<td>
				<input  id="mob2" datatype="m" nullmsg="手机号不能为空" errormsg="手机号不对" name="mobile" type="text" value="" class="inputTxt" maxlength="11">
				<em id="ship_mb_valid_msg" class="red">*</em>
				<td><div class="Validform_checktip"></div></td>
			</td>
		</tr>

		<tr>
			<td><label>&nbsp;</label></td>
			<td>
				<input type="hidden" name="address_id" value="" />
				<input style="margin-right:20px;" name="submit" type="submit" class="orangebut" id="btn_consignee_save" value="保存" title="保存"> 
				<input type="button" class="cancelBtn" value="取消" id="btn_consignee_cancle2" title="取消">
			</td>
		</tr>
        <tr><td></td></tr>
        <tr><td><span style="font-size:12px; color:#db3752;">注：带*的为必填项</span></td></tr>
		</tbody>
		</table>
		</form>
		 
	</div>
</div>
	
	
</div>

<?php include templates("index","footer");?>