<?php defined('G_IN_SYSTEM')or exit('No permission resources.'); ?><!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title></title>
	</head>
	<body>
		
		
		<!--本商品的期数导航start-->
<div class="ng-total-nav clearfix">
	<ul class="ng-sort">
		<?php echo $qishu_nav; ?>
	</ul>
			
	<div id="more_gd" class="more">
		<div id="more_box" class="more_box5" style="cursor:pointer;"><img src="<?php echo G_TEMPLATES_IMAGE; ?>/GUANBI.png"></div>
		<div class="more_box">
			<div class="more_box1">通往第</div>
			<div class="more_box2"><input class="more_new" value="" name="qi_shu" placeholder="请输入" /></div>
			<div class="more_box3">期<span id="go_goods" style="cursor:pointer;"><img src="<?php echo G_TEMPLATES_IMAGE; ?>/href.png"></span></div>
		</div>
		<div class="more_small more_gdt">
		<ul>				
			<?php $ln=1;if(is_array($All_list)) foreach($All_list AS $p): ?>
				<!--正在进行-->
				<?php if($p['q_uid']==null ): ?>
					<?php if($p['q_showtime']=='N'): ?>
						<a href="<?php echo WEB_PATH; ?>/goods/<?php echo $p['id']; ?>">
							<li <?php if($p['id']==$item['id']): ?>class="more_box4" <?php endif; ?>>
								第<?php echo $p['qishu']; ?>期(财购ing...)
							</li>
						</a>	
					<?php  else: ?>
							<a href="<?php echo WEB_PATH; ?>/goods/<?php echo $p['id']; ?>">
								<li <?php if($p['id']==$item['id']): ?>class="more_box4" <?php endif; ?>>
									第<?php echo $p['qishu']; ?>期(揭晓ing..)
								</li>
							</a>
					<?php endif; ?>
				<?php endif; ?>
				<!--已揭晓-->
				<?php if($p[q_uid] !== null): ?>
				<a href="<?php echo WEB_PATH; ?>/dataserver/<?php echo $p['id']; ?>">
					<li <?php if($p['id']==$item['id']): ?>class="more_box4" <?php endif; ?>>
						第<?php echo $p['qishu']; ?>期
					</li>
				</a>
				<?php endif; ?>
			<?php  endforeach; $ln++; unset($ln); ?>
			
			<div style="width:100%;height:20px;clear: both;"></div>
		</div>
		<div class="clearfix"></div>

	</div>

</div>

<script>
	$(document).ready(function(){
		
		$("#li_more").click(function(){
			$("#more_gd").slideToggle();
		});
		$("#more_box").click(function(){
			$("#more_gd").hide();
		});
		$('#go_goods').click(function(){
			var qishu = parseInt($('.more_new').val());
			var sid    = "<?php echo $item['sid']; ?>";
			if(qishu>0){
				$.post("<?php echo WEB_PATH; ?>/go/index/qishu_href",{"qishu":qishu,"sid":sid},function(data){
					$id = parseInt(data);
					if($id>0){
						location.href="<?php echo WEB_PATH; ?>/goods/"+$id;
					}else{
						alert('没有这期');
					}
					
				});
			}else{
				alert('期数不能为空');
			}
		});

	})
	
</script>
	
<!--本商品的期数导航end-->
		
		
		
	</body>
</html>
