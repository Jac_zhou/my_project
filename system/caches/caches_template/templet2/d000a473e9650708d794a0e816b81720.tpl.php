<?php defined('G_IN_SYSTEM')or exit('No permission resources.'); ?><?php include templates("index","header");?>
<?php include templates("member","member_top");?>
<div class="memberxhcontains">
	<?php include templates("member","member_nav");?>
	
    <div class="member_shaidanstatus">
        <a href="javascript:void(0)" class="current">已经晒单</a> | <a href="javascript:void(0)">未晒单</a>
		<span style="font-size:12px; font-weight:bold">&nbsp;&nbsp;(确认收货后才能晒单！)</span>
    </div>
	
    <!--中奖记录-->
    <div class="member_user_winner">
		<?php if(count($shaidan)==0): ?>
		<div class="member_noticle_tishi"><i></i>暂时没有晒单记录</div>
		<?php  else: ?>
    	<ul>
        	<?php $ln=1;if(is_array($shaidan)) foreach($shaidan AS $sd): ?>
            <li class="setheight">
            	<div class="member_user_shaidan"><a href="<?php echo WEB_PATH; ?>/go/shaidan/detail/<?php echo $sd['sd_id']; ?>" target="_blank"><img src="<?php echo G_UPLOAD_PATH; ?>/<?php echo find_orginimg($sd['sd_thumbs']); ?>" /></a>
                </div>
            	<div class="member_shaidantop">
                	<p><a href="<?php echo WEB_PATH; ?>/go/shaidan/detail/<?php echo $sd['sd_id']; ?>" class="membercolor_blur01" target="_blank"><?php echo _strcut($sd['title'],60); ?></a></p>
                    <p>幸运号码：<span class="membercolor_red01"><?php echo $sd['q_user_code']; ?></span></p>
                </div>
                <div class="member_shaidanbottom">
                	<h5 style="font-size:14px"><a href="<?php echo WEB_PATH; ?>/go/shaidan/detail/<?php echo $sd['sd_id']; ?>" target="_blank" class="membercolor_blur01" ><?php echo $sd['sd_title']; ?></a></h5>
					<p style="color:#adadad"><?php echo now_time($sd['sd_time']); ?></p>
                    <p class="content xh_overflow_x"><?php echo _strcut($sd['sd_content'],30); ?></p>
                </div>
            </li>
           <?php  endforeach; $ln++; unset($ln); ?>
        </ul>
		<?php endif; ?>
    </div>
 

    <!--未晒单的-->
    <div class="member_user_winner" style="display:none">
		 <?php if($record==null): ?>
		<div class="member_noticle_tishi"><i></i>暂时没有中奖记录</div>
		  <?php  else: ?>
		<ul >
        	<?php $ln=1;if(is_array($record)) foreach($record AS $sd): ?>
        	<li>
            	<div class="member_user_winnerimg"><a href="<?php echo WEB_PATH; ?>/goods/<?php echo $sd['shopid']; ?>" target="_blank"><img src="<?php echo G_UPLOAD_PATH; ?>/<?php echo shoplisext($sd['shopid'],'thumb'); ?>" /></a></div>
            	<h4 style="height:56px; !important"><a href="<?php echo WEB_PATH; ?>/goods/<?php echo $sd['shopid']; ?>" class="membercolor_blur01" target="_blank"><?php echo _strcut(shoplisext($sd['shopid'],'title'),60); ?></a></h4>
                <div class="member_user_winnerinfo">
                    <p>总共参与：<?php echo $sd['count_gonumber']; ?>人次</p>
                    <p>揭晓时间：<?php echo $sd['time']; ?></p>
                    <p class="member_shaidanbutton addshaidan">添加晒单</p>
                    <input type="hidden" value="<?php echo $sd['id']; ?>" name='sd_id'/>
                
                </div>
            </li>
            <?php  endforeach; $ln++; unset($ln); ?>
        </ul>
		<?php endif; ?>
    </div>
    
</div>
<script type="text/javascript">
$(function(){
	$(".member_shaidanstatus a").click(function(){
		$(".member_shaidanstatus a").removeClass("current");
		$(this).addClass("current");
		$(".member_user_winner").hide();
		
		var _index= parseInt($(this).index());
		$(".member_user_winner").eq(_index).show();
		if($(".member_user_winner").eq(_index).find("li").length>0){
			$(".member_noticle_tishi").hide();
		}
		
	});
	
	//添加晒单
	$(".addshaidan").click(function(){
		var recover_id = $(this).siblings('input[name=sd_id]').val();
		var rurl = '<?php echo WEB_PATH; ?>/member/home/addsinglestatus';
		$.ajax({
			url:rurl,
			data:{recover_id:recover_id,action:'page'},
			type:"POST",
			success:function(data){
				var _data=$.parseJSON(data);
				if(_data.error == "1"){
					msg_show.waring(_data.msg);
				}else if(_data.error == "0"){
					//msg_show.success(_data.msg,_data.url);
					window.location.href = _data.url;
				}
			}
		});
		return false;
	});
});
</script>


<?php include templates("index","footer");?>