<?php defined('G_IN_SYSTEM')or exit('No permission resources.'); ?><!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title></title>
		<script type="text/javascript" src="<?php echo G_TEMPLATES_JS; ?>/index/jquery.reveal.js"></script>
		
		<style type="text/css">
				
			.reveal-modal-bg { 
				position: fixed; 
				height: 100%;
				width: 100%;
				background: #000;
				background: rgba(0,0,0,.8);
				z-index: 100;
				display: none;
				top: 0;
				left: 0; 
				}
			
			.reveal-modal {
				visibility: hidden;
				top: 100px; 
				left: 50%;
				margin-left: -300px;
				width: 520px;
				background: #eee url(<?php echo G_TEMPLATES_IMAGE; ?>/index/modal-gloss.png) no-repeat -200px -80px;
				position: absolute;
				z-index: 101;
				padding: 30px 40px 34px;
				-moz-border-radius: 5px;
				-webkit-border-radius: 5px;
				border-radius: 5px;
				-moz-box-shadow: 0 0 10px rgba(0,0,0,.4);
				-webkit-box-shadow: 0 0 10px rgba(0,0,0,.4);
				-box-shadow: 0 0 10px rgba(0,0,0,.4);
				}
				
			.reveal-modal.small 		{ width: 200px; margin-left: -140px;}
			.reveal-modal.medium 		{ width: 400px; margin-left: -240px;}
			.reveal-modal.large 		{ width: 600px; margin-left: -340px;}
			.reveal-modal.xlarge 		{ width: 800px; margin-left: -440px;}
			
			.reveal-modal .close-reveal-modal {
				font-size: 22px;
				line-height: .5;
				position: absolute;
				top: 8px;
				right: 11px;
				color: #aaa;
				text-shadow: 0 -1px 1px rbga(0,0,0,.6);
				font-weight: bold;
				cursor: pointer;
				} 

			body { font-family: "HelveticaNeue","Helvetica-Neue", "Helvetica", "Arial", sans-serif; }
			.big-link {display:none;}
			.Model_title{
				font-size:16px;
				color:rgb(248,29,0);
			}
			.Model_text{
				font-size:20px;
				text-align: center;
			}
		</style>
	</head>
	<body>
		
		
		<a href="#" class="big-link" id="Model_slide" data-reveal-id="myModal">
			滑动弹窗
		</a>	
		
		<a href="#" class="big-link" id="Model_fade" data-reveal-id="myModal" data-animation="fade">
			渐显弹窗
		</a>
		
		<a href="#" class="big-link" id="Model_none" data-reveal-id="myModal" data-animation="none">
			直接显示
		</a>
		<div id="myModal" class="reveal-modal">
			<p class="Model_title">提示</p>
			<div class="Model_text">This is a default modal in all its glory, but any of the styles here can easily be changed in the CSS.</div>
			<a class="close-reveal-modal">&#215;</a>
		</div>
		
	</body>
</html>
