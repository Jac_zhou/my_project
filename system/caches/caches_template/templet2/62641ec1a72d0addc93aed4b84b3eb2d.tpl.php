<?php defined('G_IN_SYSTEM')or exit('No permission resources.'); ?><?php include templates("index","header");?>
<link rel="stylesheet" type="text/css" href="<?php echo G_TEMPLATES_STYLE; ?>/css/GoodsList.css"/>

<div class="wrap" id="loadingPicBlock">
	<div id="current" class="list_Curtit">
		<h1 class="fl">商品搜索－"<?php echo $search; ?>"</h1> 
		<span id="spTotalCount">(共<em class="orange"><?php echo $list; ?></em>件相关商品)</span>
	</div>
	<?php if($shoplist!=null): ?>	
	<div class="listContent">
	<ul class="item" id="ulGoodsList">
		<?php $ln=1;if(is_array($shoplist)) foreach($shoplist AS $shop): ?>
		<li class="goods-iten" >
			<div class="pic">
				<a href="<?php echo WEB_PATH; ?>/go/index/item/<?php echo $shop['id']; ?>" target="_blank" title="<?php echo $shop['title']; ?> ">
					<!--补丁3.1.5_b.0.1-->                    	 
                    	<?php $i_googd_bj = null; ?>        
                        <!--补丁3.1.5_b.0.1--> 
						
						<?php if($shop['is_new']=='1'): ?>						
								<?php $i_googd_bj = '<i class="goods_xp"></i>'; ?>					
						<?php endif; ?>
                        <?php if($shop['pos']!='0' && !isset($i_googd_bj)): ?>						
                       	 	<?php $i_googd_bj = '<i class="goods_tj"></i>'; ?>	
						<?php endif; ?>
						<?php if($shop['renqi']=='1' && !isset($i_googd_bj)): ?>						
                                <?php $i_googd_bj = '<i class="goods_rq"></i>'; ?>				
						<?php endif; ?>
						<?php if($shop['is_shi'] ==1): ?>						
								<?php $i_googd_bj = '<i class="goods_xp_shi"></i>'; ?>					
						<?php endif; ?>
                        <?php echo $i_googd_bj; ?>  
					    <img alt="<?php echo $shop['title']; ?>" src="<?php echo G_UPLOAD_PATH; ?>/<?php echo $shop['thumb']; ?>">
				</a>
				<p name="buyCount" style="display:none;"></p>
			</div>
			<p class="name">
				<a href="<?php echo WEB_PATH; ?>/go/index/item/<?php echo $shop['id']; ?>" target="_blank" title="<?php echo $shop['title']; ?> "><?php echo $shop['title']; ?></a>
			</p>
			<p class="money">价值：<span class="rmbgray"><?php echo $shop['money']; ?></span></p>
			<div class="Progress-bar">
				<p title="已完成<?php echo percent($shop['canyurenshu'],$shop['zongrenshu']); ?>"><span style="width:<?php echo width($shop['canyurenshu'],$shop['zongrenshu'],213); ?>px;"></span></p>
				<ul class="Pro-bar-li">
					<li class="P-bar01"><em><?php echo $shop['canyurenshu']; ?></em>已参与人次</li>
					<li class="P-bar02"><em><?php echo $shop['zongrenshu']; ?></em>总需人次</li>
					<li class="P-bar03"><em><?php echo $shop['zongrenshu']-$shop['canyurenshu']; ?></em>剩余人次</li>
				</ul>
			</div>
			<div class="gl_buybtn">
				<div class="go_buy" style="margin-left:18%">
					<input type="hidden" value="<?php echo $shop['id']; ?>" />
					<a href="<?php echo WEB_PATH; ?>/go/index/item/<?php echo $shop['id']; ?>" title="立即夺宝" class="go_Shopping fl">立即夺宝</a>
					<a href="javascript:void(0)" title="加入购物车" class="go_cart f1"></a>
				</div>
			</div>
			<div class="Curborid" style="display:none;"><?php echo $shop['id']; ?></div>
			<div class="Curbor_id" style="display:none;"><?php echo $shop['id']; ?></div>
			<div class="Curbor_yunjiage" style="display:none;"><?php echo $shop['yunjiage']; ?></div>
			<div class="Curbor_shenyu" style="display:none;"><?php echo $shop['shenyurenshu']; ?></div>
			<div class="Curbor_default_renci" style="display:none;"><?php echo $shop['default_renci']; ?></div>
		</li>
		<?php  endforeach; $ln++; unset($ln); ?>
	</ul>
	</div>
	<?php  else: ?>
	<div class="NoConMsg"><span>未找到有关“<em class="orange"><?php echo $search; ?></em>”的商品</span></div>
	<?php endif; ?>
</div>
 <script type="text/javascript"> 
$(function(){
//商品购物车
	$("#ulGoodsList li a.go_cart").click(function(){ 
		var sw = $("#ulGoodsList li a.go_cart").index(this);		
		var src = $("#ulGoodsList li .pic a img").eq(sw).attr('src');				
		var $shadow = $('<img id="cart_dh" style="display:none; border:1px solid #aaa; z-index:99999;" width="200" height="200" src="'+src+'" />').prependTo("body");  			
		var $img = $("#ulGoodsList li .pic").eq(sw);
		$shadow.css({ 
			'width' : 200, 
			'height': 200, 
			'position' : 'absolute',      
			"left":$img.offset().left+16, 
			"top":$img.offset().top+9,
			'opacity' : 1    
		}).show();
		
		//顶部购物车显示数目
		var $cart = $("#btnMyCart");

		$shadow.animate({   
			width: 1, 
			height: 1, 
			top: $cart.offset().top,    
			left: $cart.offset().left, 
			opacity: 0
		},500,function(){
			Cartcookie_x(sw,false);
		});	
		
	});
	$("#ulGoodsList li a.go_Shopping").click(function(){	
		var sw = $("#ulGoodsList li a.go_Shopping").index(this);
		Cartcookie_x(sw,true); 
	});	
});
//存到COOKIE
function Cartcookie_x(sw,cook){

	var shopid = $(".Curbor_id").eq(sw).text(),
		shenyu = parseInt($(".Curbor_shenyu").eq(sw).text()),
		money = $(".Curbor_yunjiage").eq(sw).text(),
		default_renci = parseInt($(".Curbor_default_renci").eq(sw).text());
	//重新计算金额和默认人次   20151208 kangpengfei
	if(default_renci > shenyu){
		default_renci = shenyu;
	}
	money = money * default_renci;
	
	var Cartlist = $.cookie('Cartlist');
	if(!Cartlist){
		var info = {};
	}else{
		var info = $.evalJSON(Cartlist);
	}
	if(!info[shopid]){	
		var CartTotal=$("#sCartTotal").text();			
			$("#sCartTotal").text(parseInt(CartTotal)+1);			
			$("a#btnMyCart em").text(parseInt(CartTotal)+1);
	}	
	if(typeof info[shopid] == 'undefined'){
		info[shopid]={};
	}
	info[shopid]['num']=typeof(info[shopid]['num']) == 'undefined' ? default_renci : Number(info[shopid]['num'])+Number(default_renci);
	info[shopid]['shenyu']=shenyu;
	info[shopid]['money']=typeof(info[shopid]['money']) == 'undefined' ? money : Number(info[shopid]['money'])+Number(money);
	info['MoenyCount']='0.00';
	$.cookie('Cartlist',$.toJSON(info),{expires:30,path:'/'});
	if(cook){
		window.location.href="<?php echo WEB_PATH; ?>/member/cart/cartlist";
	}
} 
</script>
</script>
<?php include templates("index","footer");?>