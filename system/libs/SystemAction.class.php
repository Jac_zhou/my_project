<?php 

/**
 *		action.class.php  基础操作动作类
 *
 */
class SystemAction {
    static  private $route_url;

    /**
     * @param string $model         引入Model类
     * @param string $module        目标模块  默认 sys
     * @return 返回实例化对象|返回需要引入的文件
     */
	final protected function DB($model='model',$module='sys'){
		static $classes = array();
		if(isset($classes[$model.$module]))return $classes[$model.$module];
		if($module=='sys'){
			return $classes[$model.$module]=System::load_sys_class($model);
		}else{		
			return $classes[$model.$module]=System::load_app_model($model,$module);
		}
		
	}

    /** 调用模板函数
     * @param string $module            模板目录
     * @param string $template          模板文件名,
     * @param string $StyleTheme        模板方案目录,为空为默认目录
     * @return mixed
     */
	final protected function view($module = '', $template = '',$StyleTheme=''){	
		if(empty($StyleTheme)){
            $style=G_STYLE.DIRECTORY_SEPARATOR.G_STYLE_HTML;
        }else{
			$templates=System::load_sys_config('templates','sys');
			$style=$templates['dir'].DIRECTORY_SEPARATOR.$templates['html'];
		}
		$FileTpl = G_CACHES.'caches_template'.DIRECTORY_SEPARATOR.dirname($style).DIRECTORY_SEPARATOR.md5($module.'.'.$template).'.tpl.php';	
		$FileHtml = G_TEMPLATES.$style.DIRECTORY_SEPARATOR.$module.'.'.$template.'.html';	
		if(file_exists($FileHtml)){		
			if (file_exists($FileTpl) && @filemtime($FileTpl) >= @filemtime($FileHtml)) {					
				return include  $FileTpl;			
			} else {			
				$template_cache=System::load_sys_class('template_cache');	
				if(!is_dir(dirname(dirname($FileTpl)))){
					mkdir(dirname(dirname($FileTpl)),0777, true)or die("Not Dir");		
				    chmod(dirname(dirname($FileTpl)),0777);
				}
				if(!is_dir(dirname($FileTpl))){		
					mkdir(dirname($FileTpl), 0777, true)or die("Not Dir");
					chmod(dirname($FileTpl),0777);
				}	
				$PutFileTpl=$template_cache->template_init($FileTpl,$FileHtml,$module,$template);
				if($PutFileTpl)
					return include  $FileTpl;					
				else				
					_error('template message','The "'.$module.'.'.$template .'" template file does not exist');
			}
		}
		_error('template message','The "'.$module.'.'.$template .'" template file does not exist');
		
	}

    /** 引入模板  后缀名是。tpl.php
     * @param string $module            模块，默认是admin模块，
     * @param string $template          文件名。tpl.php（后缀）
     * @return string
     */
	protected function tpl($module = 'admin', $template = 'index'){	
		$file =  G_SYSTEM.'modules/'.$module.'/tpl/'.$template.'.tpl.php';
		if(file_exists($file))return $file;
		elseif(defined("G_IN_ADMIN")){
			_message("没有找到<font color='red'>".$module."</font>模块下的<font color='red'>".$template.".tpl.php</font>文件!");
		}else{
			_error('template message','The "'.$module.'.'.$template .'" template file does not exist');
		}
	}

     /**    获取url路由参数
      * @param int $n           url域名起，第$n个路由参数
      * @return bool|string     返回参数数值
      */
	final protected function segment($n=1){		
		if(!isset(self::$route_url[$n])){
			return false;
		}else{
			if(strpos(self::$route_url[$n],"&p=") !== false){				
				return '';
			}else{
				return self::$route_url[$n];	
			}
		}
	}

    /**
     * @return 返回路由参数数组
     */
	final protected function segment_array(){
		return self::$route_url;
	}

    /**
     * @param $route_urls  自定义路由参数的方法
     */
	final static public function set_route_url($route_urls){	
		self::$route_url=$route_urls;
		
	}
	
}

