<?php

/**
 * System.class.php
 *
 * @copyright			(C) 2005-2010 战线
 * @license				V3.1.2
 * @lastmodify			2014-05-21
 */
 
final class System {

	/**
	 * 调用类文件	系统/当前模块  下的lib文件夹里面的类文件          后缀名 class.php  文件夹libs/lib
	 * @param	$class_name		类名	
	 * @param  	$module			系统/模块     默认是系统
	 * @param 	$new			是否实例化 （ 默认实例化）
	 * @return  返回实例化对象
	 */
	//调用系统类文件	new 是否实例化 module 是模块还是系统
	public static function load_sys_class($class_name='',$module='sys',$new='yes'){
	
			static $classes = array();
			$path=self::load_class_file_name($class_name,$module);
			$key=md5($class_name.$path.$new);		
			if (isset($classes[$key])) {
				return $classes[$key];
			}
			if(file_exists($path)){
				include_once $path;
				if($new=='yes'){
					$classes[$key] = new $class_name;		
				}else{					
					$classes[$key]=true;
				}				
				return $classes[$key];			
			}else{
				_error('load system class file: '.$module." / ".$class_name,'The file does not exist');
			}
				
	}
	
	/**
	 * 调用当前模块类	系统/当前模块  下的lib文件夹里面的类文件          后缀名 class.php  文件夹libs/lib
	 * @param	$class_name	类名	
	 * @param	$module		模块名（默认是当前所处模块）	
	 * @param	$new		是否实例化该类
	 * @return  返回实例化对象
	 */
	
	public static function load_app_class($class_name='',$module='',$new='yes'){
			if(empty($module)){
				$module=ROUTE_M;
			}
			//self::load_sys_class调用类中的静态或非静态方法
			return self::load_sys_class($class_name,$module,$new);
	}

	/**
	 * 加载类文件     系统/当前模块  下的lib文件夹里面的类文件          后缀名 class.php  文件夹libs/lib
	 * @param   $class_name   	类名
	 * @param	$module			系统/当前模块     默认是系统
	 * 
	 * @return  所需引入的类文件路径
	 */	
	public static function load_class_file_name($class_name='',$module='sys'){		
		static $filename = array();
		if(isset($filename[$module.$class_name])) return $filename[$module.$class_name];
		if($module=='sys'){
		 	$filename[$module.$class_name]=G_SYSTEM.'libs'.DIRECTORY_SEPARATOR.$class_name.'.class.php';
		}else if($module!='sys'){
			$filename[$module.$class_name]=G_SYSTEM.'modules'.DIRECTORY_SEPARATOR.$module.DIRECTORY_SEPARATOR."lib".DIRECTORY_SEPARATOR.$class_name.'.class.php';
		}else{
			return $filename[$module.$class_name];
		}
		return $filename[$module.$class_name];
	}
		
	
	/**
	 * 引入当前系统下的公共配置文件    后缀名 inc.php 	公共文件夹 config
	 * @param 	$filename		配置文件名
	 * @param	$keys			引入该配置文件中的某一项配置（为空便引入所有配置）
	 * @return  返回所需引入的配置（整个文件/某一项配置）
	 */	
	//引入网站基本配置文件 2016.5.23 zl add
	public static function load_sys_config($filename,$keys=''){
		static $configs = array();
		if(isset($configs[$filename])){
			if (empty($keys)) {
				return $configs[$filename];
			} else if (isset($configs[$filename][$keys])) {
				return $configs[$filename][$keys];
			}else{
				return $configs[$filename];
			}
		}			
		
		if (file_exists(G_CONFIG.$filename.'.inc.php')){
				$configs[$filename]=include G_CONFIG.$filename.'.inc.php';
				if(empty($keys)){
					return $configs[$filename];
				}else{
					return $configs[$filename][$keys];
				}
		}
		
		_error('load system config file: '.$filename,'The file does not exist+');	
	}
		
	/**
	 * 引入模块下的配置文件	模块目录下lib文件夹    ini.php后缀名
	 * @param	$filename	配置文件名
	 * @param	$keys			引入该配置文件中的某一项配置（为空便引入所有配置）
	 * @param	$module		模块	（默认是当前模块）
	 * @return	返回需要引入的配置文件/某单独配置项
	 */
	public static function load_app_config($filename,$keys='',$module=''){
		static $configs = array();	
		if(isset($configs[$filename])){
			if (empty($keys)) {
				return $configs[$filename];
			} else if (isset($configs[$filename][$keys])) {
				return $configs[$filename][$keys];
			}else{
				return $configs[$filename];
			}
		}
		if(empty($module))$module=ROUTE_M;
		$path=G_SYSTEM.'modules'.DIRECTORY_SEPARATOR.$module.DIRECTORY_SEPARATOR.'lib'.DIRECTORY_SEPARATOR.$filename.'.ini.php';		
		if (file_exists($path)){
				$configs[$filename]=include $path;
				if(empty($keys)){
					return $configs[$filename];
				}else{
					return $configs[$filename][$keys];
				}
		}	
		_error('load app config file: '.$module." / ".$filename,'The file does not exist');			
	}
	/**
	 * 	引入函数库	系统下的funcs文件夹里面的类文件     后缀名 fun.php 
	 * @param 	$fun_name	函数名
	 * @return	引入所需的函数库
	 */
	public static function load_sys_fun($fun_name){
		static $funcs = array();
		$path=G_SYSTEM.'funcs'.DIRECTORY_SEPARATOR.$fun_name.'.fun.php';	
		$key = md5($path);
		if (isset($funcs[$key])) return true;    //静态$funcs 用来判断当前是否已经引入了所需的函数文件
		if (file_exists($path)){
			$funcs[$key] = true;
			return include $path;
		}else{
			$funcs[$key] = false;
			_error('load system function file: '.$fun_name,'The file does not exist');
		}
	
	}
	
	
	/**
	 * 引入系统下的   当前模块的$module  			函数文件 后缀名 fun.php   文件夹 lib
	 * @param   $module    	模块 
	 * @param	$fun_name	文件名
	 * @return  返回需要引入的文件
	 */
	public static function load_app_fun($fun_name,$module=null){
		static $funcs = array();		
		if(empty($module)){
			$module=ROUTE_M;
		}
		$path=G_SYSTEM.'modules'.DIRECTORY_SEPARATOR.$module.DIRECTORY_SEPARATOR.'lib'.DIRECTORY_SEPARATOR.$fun_name.'.fun.php';	
		$key = md5($path);
		if (isset($funcs[$key])) return true;
		if (file_exists($path)){
			$funcs[$key] = true;
			return include $path;
		}else{			
			_error('load app function file: '.$module." / ".$fun_name,'The file does not exist');
		}	
	}
	
	/**	
	 * 数据模块操作类           后缀名  model.php 文件夹 model
	 * @param  	$model_name	需要引入的模型Model 
	 * @param	$module    	模块 
	 * @param	$new		是否重新实例化这model类
	 * @return  返回需要引入的文件
	 */
	public static function load_app_model($model_name='',$module='',$new='yes'){			
		static $models=array();
		if(empty($module)){
				$module=ROUTE_M;
		}
		$key=md5($module.$model_name.$new);
		if(isset($models[$key])){
			return $models[$key];
		}		
		$path=G_SYSTEM.'modules'.DIRECTORY_SEPARATOR.$module.DIRECTORY_SEPARATOR.'model'.DIRECTORY_SEPARATOR.$model_name.'.model.php';
		if (file_exists($path)){
			include $path;
			if($new=='yes'){			
				$models[$key]=new $model_name;				
			}else if($new=='no'){				
				$models[$key]=true;
			}
			return $models[$key];
		}
		_error('load app model file: '.$module." / ".$model_name,'The file does not exist');
		
	}
	/**
	 * 基础处理系统  路由
	 */
	public static function CreateApp(){
		return self::load_sys_class('application');
	}
	
	/**	
	 * 逻辑操作类           后缀名  action.php 文件夹 $contoller
	 * @param  	$model_name	需要引入的控制器action
	 * @param	$contoller    	模块 
	 * @param	$new		是否重新实例化控制器
	 * @return  返回需要引入的文件
	 */
	public static function load_contorller($contoller_name='',$contoller='',$new='yes'){
		static $controllers=array();
		if(empty($contoller)){
				$contoller=ROUTE_M;
		}
		//防止重复实例化控制器
		$key=md5($controller.$contoller_name.$new);
		if(isset($controllers[$key])){
			return $controllers[$key];
		}		
		$path=G_SYSTEM.'modules'.DIRECTORY_SEPARATOR.$contoller.DIRECTORY_SEPARATOR.$contoller_name.'.action.php';
		if (file_exists($path)){
			include $path;
			if($new=='yes'){			
				$controllers[$key]=new $contoller_name;				
			}else if($new=='no'){				
				$controllers[$key]=true;
			}
			return $controllers[$key];
		}
		_error('引用控制文件: '.$contoller." / ".$contoller_name,'没有该控制文件');
		
	}
	
	
	
	
	
	
	
	
	
}

?>