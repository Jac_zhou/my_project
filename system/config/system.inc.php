<?php 
 defined('G_IN_SYSTEM') or exit('No permission resources.'); 
return array( 
'web_name' => '易中夺宝-最容易中的夺宝平台！',//网站名
'web_key' => '1元,一元,一元夺宝,一元云购,云购,1元夺宝,一元夺宝,1元云购,一元云购,1元云购官网,一元云购官网,云购奇兵,云购,一元购,1元购,云购商城',//网站关键字
'web_des' => '易中夺宝是一个引入众筹理念的新型电子商务平台，手机电脑、数码科技、潮流新品、家居电器，只需1元即有机会获取，凭借技术实力和算法公平，确保100%公平公正、100%正品保障，引领集娱乐、购物于一体的网购潮流。',//网站介绍
'web_path' => 'http://www.yz-db.com',//网站地址
'templates_edit' => '1',//是否允许在线编辑模板
'templates_name' => 'templet2',//当前模板方案
'charset' => 'utf-8',//网站字符集
'timezone' => 'Asia/Shanghai',//网站时区
'error' => '1',//1、保存错误日志到 cache/error_log.php | 0、在页面直接显示
'gzip' => '0',//是否Gzip压缩后输出,服务器没有gzip请不要启用
'lang' => 'zh-cn',//网站语言包
'cache' => '3600',//默认缓存时间
'web_off' => '1',//网站是否开启
'web_off_text' => '网站维护中....',//关闭原因
'tablepre' => 'QCNf',//
'index_name' => '',//隐藏首页文件名
'expstr' => '/',//url分隔符号
'admindir' => 'shop_kc1ycs',//后台管理文件夹
'qq' => '4008117233',//qq
'cell' => '4008-117-233',//联系电话
'web_logo' => 'banner/20160531/80746964677995.png',//logo
'web_copyright' => 'Copyright © 2015 - 2016, 粤ICP备17064399号-1',//版权
'web_name_two' => '易中夺宝',//短网站名
'qq_qun' => '',//QQ群
'goods_end_time' => '481',//开奖动画秒数(单位秒)
); 
 ?>