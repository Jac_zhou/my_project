<?php
/*
* 首页相关API
*/
defined('G_IN_SYSTEM')or exit('no');
System::load_app_class('api');
System::load_app_fun('myfun');

class Index extends api{
	public $max_page;
	/*
	* 1、获取幻灯片列表
	*/
	public function get_slide_list()
	{
		$indata = $this->getData();	//获取数据
		$this->checkSign($indata);		//验证签名
		
		$wh = ' `id` > 0 ';
		
		$is_android = $this->is_android($indata);
		
		/* 2016/01/04 苹果APP上架期间不允许显示苹果相关产品  审核通过后要去掉该代码 start */
		$is_ios = $this->is_ios($indata);
		$version = isset($indata['version']) ? trim($indata['version']) : '1.0.0';    //获取用户的版本号,最新版的在审核阶段是不允许显示的
		if( $is_ios && $this->compare_version($version) ){
		    $wh .= ' AND `is_apple` = 0 ';
		}
		/* 2016/01/04  苹果APP上架期间不允许显示苹果相关产品  审核通过后要去掉该代码 end */
		
		$sql = "SELECT title,img,slide_type,link,slide_val FROM @#_slide WHERE $wh ORDER BY sort_order ASC";
		$slide_list = $this->db->GetList($sql);
		if(count($slide_list)){
			foreach($slide_list as $k=>$v){
				if(strpos($v['img'],G_UPLOAD_PATH) === false){		//如果图片的地址不是完整路径，那么就给它补齐
					$slide_list[$k]['img'] = G_UPLOAD_PATH . '/' . $v['img'];
				}
				if($v['slide_type']==1){        //slide_type:0跳转其他网页  1商品详情   2关键词搜索
				    $goods = $this->db->GetOne("SELECT id,sid,qishu FROM @#_shoplist WHERE `id`='$v[slide_val]' LIMIT 1");
				    if(!isset($goods['id'])){
				        $slide_list[$k]['slide_type']=0;
				        $v['slide_type']=0;
				    }
				}
				$slide_list[$k]['shopid'] = $v['slide_type']==1 ? $v['slide_val'] : '';
				if($slide_list[$k]['shopid']){
				    $new_goods = $this->db->GetOne("SELECT id,qishu FROM @#_shoplist WHERE `sid`='$goods[sid]' ORDER BY `id` DESC LIMIT 1");
				    $slide_list[$k]['shopid'] = $new_goods['id'];
				}
				if($v['slide_type']==0 && strpos($v['link'],'lemon')!==false){
				    $slide_list[$k]['link'] = G_WEB_PATH . "/yungou/m/index.php";
				}
				
				if($v['slide_type']==0 && strpos($v['link'],'zt/konka')!==false){
				    $come = $is_android ? "Android" : ($is_ios ? "IOS" : "Wap");
				    $slide_list[$k]['link'] = $slide_list[$k]['link']."?come=".$come;
				}
				
				$slide_list[$k]['qishu'] = $v['slide_type']==1 ? $goods['qishu'] : '';
				$slide_list[$k]['keywords'] = $v['slide_type']==2 ? $v['slide_val'] : '';
				$slide_list[$k]['cateid'] = $v['slide_type'] == 3 ? $v['slide_val'] : '';   //商品分类ID
				
				unset($slide_list[$k]['slide_val']);
			}
			$this->info = array('status'=>1, 'message'=>'请求成功', 'data'=>$slide_list);
		}else{
			$this->info['message'] = '暂无数据';
		}
		$this->callBack();
	}
	
	/*
	* 2、获取幻灯片下方的四个图标
	*/
	public function get_four_icon()
	{
		$indata = $this->getData();	//获取数据
		$this->checkSign($indata);		//验证签名
		
		$sql = "SELECT * FROM @#_appimg WHERE `is_show`=1 ORDER BY `order_num` DESC";
		$arr_list = $this->db->Getlist($sql);
		if(count($arr_list)){
			$data = array();
			foreach($arr_list AS $key=>$v){
				$data[$key]['name'] = $v['name'];
				$data[$key]['type'] = $v['url'];
				$data[$key]['handle'] = $v['handel'];	//操作
				$data[$key]['img'] = strpos($v['img_path'],G_UPLOAD_PATH) !== false ? $v['img_path'] : G_UPLOAD_PATH."/".$v['img_path'];
			}
			$this->info = array('status'=>1, 'message'=>'请求成功', 'data'=>$data);
		}else{
			$this->info['message'] = '暂无数据';
		}
		$this->callBack();
	}
	
	/*
	* 3、获取最新中奖消息，内容格式为：“恭喜某某某几分钟前获得某某商品"，需要返回这个商品的id
	*/
	public function get_lottery_list()
	{
		$indata = $this->getData();	//获取数据
		$this->checkSign($indata);		//验证签名
		$size = isset($indata['size']) ? intval($indata['size']) : 10;	//默认取10条
		
		$wh = ' `q_uid` IS NOT NULL AND `q_showtime` = "N" ORDER BY `q_end_time` ';
		
		/* 2016/01/04 苹果APP上架期间不允许显示苹果相关产品  审核通过后要去掉该代码 start */
		$version = isset($indata['version']) ? trim($indata['version']) : '1.0.0';    //获取用户的版本号,最新版的在审核阶段是不允许显示的
		if( $this->is_ios($indata) && $this->compare_version($version) ){
		    $wh .= ' AND `is_apple` = 0 ';
		}
		/* 2016/01/04  苹果APP上架期间不允许显示苹果相关产品  审核通过后要去掉该代码 end */
		
		$sql = "SELECT id,sid,q_user,title,q_end_time,qishu FROM `@#_shoplist` WHERE $wh DESC  LIMIT " . $size ;
		$lottery_list = $this->db->GetList($sql);
		if(count($lottery_list)){
			foreach($lottery_list as $k => $v){
				$lottery_list[$k]['title'] = str_replace('&nbsp;',' ',$lottery_list[$k]['title']);
				$lottery_list[$k]['q_user'] = unserialize($v['q_user']);
				$lottery_list[$k]['username'] = get_user_name($lottery_list[$k]['q_user']['uid']);	//取用户名
				$temp = explode('.',$v['q_end_time']);
				$q_end_time = $temp[0];
				$lottery_time = $this->lotteryTime($q_end_time);	//计算揭晓中奖时间
				$lottery_list[$k]['content'] = '恭喜' . $lottery_list[$k]['username'] . $lottery_time . '前获得' . $lottery_list[$k]['title'] . '！';
				
				unset($lottery_list[$k]['q_user'],$lottery_list[$k]['q_end_time'],$temp,$lottery_time,$username);
			}
			$this->info = array('status'=>1, 'message'=>'请求成功', 'data'=>$lottery_list);
		}else{
		    if($indata['partner'] == 'ZL999IOSAPI'){
		        $this->info['status'] = 1;        //ios 与 android 的区别
		    }
			$this->info['message'] = '暂无数据';
		}
		$this->callBack();
	}
	
	/*
	* 4、获取某一类型的商品列表 
	* 参数 >> type:类型,size:每一页几条数据,page:第几页
	*/
	public function get_type_goods_list()
	{
		$indata = $this->getData();	//获取数据
		$this->checkSign($indata);		//验证签名
		$type = isset($indata['type']) ? trim($indata['type']) : 'renqi';	//请求的类型	人气renqi, 最新zuixin, 进度jindu, 总需人次zongxurenci , 价格jiage
		$size = isset($indata['size']) ? intval($indata['size']) : 8;	//分页的大小
		$page = isset($indata['page']) ? intval($indata['page']) : 1;	//第几页
		$do_order = isset($indata['sort']) ? trim($indata['sort']) : 'desc';	//总需人数排序方式  type=zongxurenci时有效 , 默认降序
		$uid  = isset($indata['uid'])  ? intval($indata['uid']) : 0;
		
		//组合查询条件
		$wh = ' `q_uid` IS NULL AND `shenyurenshu` > 0 ';
		
		/* 2016/01/04 苹果APP上架期间不允许显示苹果相关产品  审核通过后要去掉该代码 start */
		$version = isset($indata['version']) ? trim($indata['version']) : '1.0.0';    //获取用户的版本号,最新版的在审核阶段是不允许显示的
		if( $this->is_ios($indata)  && $this->compare_version($version) ){
		    $wh .= ' AND `is_apple` = 0 ';
		}
		/* 2016/01/04  苹果APP上架期间不允许显示苹果相关产品  审核通过后要去掉该代码 end */
		
		if($type == 'zuixin'){    //最新
			$wh .= ' ORDER BY `time` DESC';
		}elseif($type == 'jindu'){	   //进度
			$wh .= ' ORDER BY (`canyurenshu`/`zongrenshu`)*100 DESC ';  //暂时按进度排序
		}elseif($type == 'zongxurenci'){	//总需人次
			$order = strtoupper($do_order) == 'ASC' ? 'ASC' : 'DESC';  //strtoupper小写转大写
			$wh .= ' ORDER BY `zongrenshu` ' . $order;
		}elseif($type == 'jiage'){    //价格
		    $order = strtoupper($do_order) == 'ASC' ? 'ASC' : 'DESC';  //strtoupper小写转大写
		    $wh .= ' ORDER BY `money` ' . $order;
		}else{	//人气
			//$wh .= ' AND  `renqi` = 1 ';
			$wh .= ' ORDER BY `order` DESC ';
		}

//		$field = 'id,sid,title,thumb,yunjiage,money,zongrenshu,canyurenshu,qishu,default_renci,is_shi as is_ten';2017-05-27
                $field = 'id,sid,title,thumb,yunjiage,money,zongrenshu,canyurenshu,qishu,default_renci,pk_product,pk_number,is_shi as is_ten';
		$goods_list = $this->get_shoplist($wh,$field,$page,$size);
		
		if($goods_list == 1){
			$this->info['message'] = '暂无数据';
			$this->callBack();
		}
		if($goods_list == 2){
			$this->info['message'] = '后面没有数据了';
			$this->callBack();
		}
		
		if(count($goods_list)){
			//数据整理
			$goods_list = $this->datalist($goods_list,$uid);
			$data['total'] = $this->total;
			$data['max_page'] = $this->max_page;
			$data['list'] = $goods_list;
			$this->info = array('status'=>1, 'message'=>'请求成功', 'data'=>$data);
		}else{
			$this->info['message'] = '暂无数据';
		}
		$this->callBack();
	}
	/**
	 * 获取商品列表
	 *
	 * @param $where 查询条件 string
	 * @param $field 查询字段 string
	 * @param $size  取出条数 
	 * @return $list 查询出来的数据  array
	 */
	public function get_shoplist($where,$field='*',$page,$size){
		
		$total = $this->db->GetCount('SELECT `id` FROM @#_shoplist WHERE ' . $where); //统计数量
		if( ! $total ){
			return array();die;			
		}
		$page_count = intval($total / $size);	//总页数取整
		$max_page = $total % $size == 0 ? $page_count : $page_count + 1;	//最大页数
		$this->max_page = $max_page;
		$this->total = $total;
		if($page > $max_page){
			return 2;die;	
		}
		
		$start = $page <= 1 ? 0 : ($page-1) * $size;	//从第几条数据开始取		
		
		$sql = 'SELECT '.$field.' FROM @#_shoplist WHERE ' . $where . ' LIMIT ' . $start . ',' . $size;
		$list = $this->db->GetList($sql);		//default_renci 加入清单时默认的数量
		return $list;
	}

	public function Popup(){
		$indata = $this->getData();	//获取数据
		//$this->checkSign($indata);		//验证签名
		$index_adv = System::load_app_config('index_adv','',G_ADMIN_DIR);
		$this->info['data'] = $index_adv;
		$this->info['status'] = 1;
		$this->callback();
	}

	//数据整理
	/**
	 * @param  $good_list 需要整理的商品数据
	 * @param  $uid       用户id
	 * @return $good_list 返回整理后的数据（用户可购买数量wap_ten 本期产品已购人次比例 ratio）
	 */
	public function datalist($goods_list,$uid=0){
		$xiangou_money = 5;											//限购次数
		foreach($goods_list as $k => $v){
			$syrs = $v['zongrenshu'] - $v['canyurenshu'];			//本期产品的剩余人数
			$goods_list[$k]['wap_ten'] = $syrs;						//用户可以购买的次数			
			$goods_list[$k]['is_ten']  = intval($v['is_ten']);		//是否是限购专区
			//查询当前用户能将产品加入购物车的数量
			if($uid > 0){
				//获取用户的购买数量	
				$user_bought_number = $this->get_member_go_record($v['id'],$uid,$goods_list[$k]['qishu']);
				$user_bought_number = $user_bought_number['gonumber'];
			}else{
				$user_bought_number = 0;
			}
			//限购
			if($v['is_ten'] == 1){									//是否是限购专区
				$xiangou_number = $xiangou_money - $user_bought_number;	
				if($xiangou_number > 0){
					//用户购买本期产品的剩余次数  与   本期产品剩余人次
					$goods_list[$k]['wap_ten'] = ($xiangou_number > $syrs) ? $syrs : $xiangou_number;
				}else{
					//用户购买的次数已超额
					$goods_list[$k]['wap_ten'] = 0;
				}	
			}
			$goods_list[$k]['title'] = str_replace('&nbsp;',' ',$v['title']);
			//判断该图片路径是否包含  图片上传目录的路径
			if(strpos($v['thumb'],G_UPLOAD_PATH) === false){
				$goods_list[$k]['thumb'] = G_UPLOAD_PATH . '/' . $v['thumb'];
			}
			//比例   参与人数占总人数的百分比
			$goods_list[$k]['ratio'] = ceil($goods_list[$k]['canyurenshu']/$goods_list[$k]['zongrenshu']   * 100 );
			$goods_list[$k]['yunjiage'] = intval($goods_list[$k]['yunjiage']);
		}
		
		return $goods_list;
	}



	/*
	* 5、获取分类列表
	* 取顶级并且模型为1的栏目 ,  model=1 表示众乐模型
	*/
	public function get_category_list()
	{
		$indata = $this->getData();	//获取数据
		$this->checkSign($indata);		//验证签名
		
		$sql = 'SELECT `cateid`,`name`,`info` FROM `@#_category` WHERE `model`="1" AND `parentid` = "0" ORDER BY `order` DESC';
		$category_list = $this->db->GetList($sql);
		if(count($category_list)){
			foreach($category_list as $k => $v){
				$category_list[$k]['info'] = unserialize($v['info']);
				$thumb = $category_list[$k]['info']['thumb'];	
				if( ! empty($thumb) && strpos($thumb,G_UPLOAD_PATH) === false ){
					$thumb = G_UPLOAD_PATH . '/' . $thumb;
				}
				$category_list[$k]['thumb'] = $thumb;		//图标地址
				unset($category_list[$k]['info'],$thumb);
			}
			$this->info = array('status'=>1, 'message'=>'请求成功', 'data'=>$category_list);
		}else{
			$this->info['message'] = '暂无数据';
		}
		$this->callBack();
	}
	
	/*
	* 6、获取某个分类下的商品列表
	* 参数 >> cateid:分类ID  , size:每一页几条数据 , page:取第几页
	*/
	public function get_cate_goods_list()
	{
		$indata = $this->getData();	//获取数据
		$this->checkSign($indata);		//验证签名
		$cateid = isset($indata['cateid']) ? intval($indata['cateid']) : 0;	//分类的ID,默认取所有分类
		$size = isset($indata['size']) ? intval($indata['size']) : 10;	//分页的大小
		$page = isset($indata['page']) ? intval($indata['page']) : 1;	//第几页
		$type = isset($indata['type']) ? trim($indata['type']) : '';	//类型 如 人气、最新、剩余人次(排序)、总需人次(排序)
		$sort = isset($indata['sort']) ? trim($indata['sort']) : 'ASC';	//排序，type=shenyurenshu  或者 type=zongrenshu 时用得上
		$uid  = isset($indata['uid'])  ? intval($indata['uid']) : 0;
		$cate_name = '';
		
		$wh = ' `q_uid` IS NULL AND `shenyurenshu` > 0 AND `q_end_time` IS NULL ';		//组合查询条件
		
		/* 2016/01/04 苹果APP上架期间不允许显示苹果相关产品  审核通过后要去掉该代码 start */
		$version = isset($indata['version']) ? trim($indata['version']) : '1.0.0';    //获取用户的版本号,最新版的在审核阶段是不允许显示的
		$is_ios = $this->is_ios($indata);
		if( $is_ios  && $this->compare_version($version) ){
		    $wh .= ' AND `is_apple` = 0 ';
		}
		/* 2016/01/04  苹果APP上架期间不允许显示苹果相关产品  审核通过后要去掉该代码 end */
		
		if($cateid > 0){
			$wh .= ' AND `cateid` = ' . $cateid;
			$cate = $this->db->GetOne("SELECT `name` FROM @#_category WHERE `cateid`='$cateid' LIMIT 1");
			$cate_name = $cate['name'];
		}
		if($type == 'renqi'){	//人气
			$wh .= ' AND `renqi` = 1 ORDER BY `order` DESC ';
		}elseif($type == 'zuixin'){	//最新
			$wh .= ' AND `renqi` = 0  ORDER BY `time` DESC ';
		}elseif($type == 'shenyurenshu'){	//剩余人数
			$sort = strtoupper($sort) == 'ASC' ? 'ASC' : 'DESC';
			$wh .= ' ORDER BY `shenyurenshu` ' . $sort;
		}elseif($type == 'zongrenshu'){	//总人数
			$sort = strtoupper($sort) == 'ASC' ? 'ASC' : 'DESC';
			$wh .= ' ORDER BY `zongrenshu` ' . $sort;
		}
//		$field = 'id,sid,title,thumb,zongrenshu,canyurenshu,yunjiage,qishu,default_renci,is_shi as is_ten';2017-5-27
                $field = 'id,sid,title,thumb,zongrenshu,canyurenshu,yunjiage,qishu,default_renci,pk_product,pk_number,is_shi as is_ten';
		$goods_list = $this->get_shoplist($wh,$field,$page,$size);
		if($goods_list == 1){
			$this->info['message'] = '暂无数据';
			$this->callBack();
		}
		if($goods_list == 2){
			$this->info['message'] = '后面没有数据了';
			$this->callBack();
		}
		if(count($goods_list)){
			//数据整理
			$goods_list = $this->datalist($goods_list,$uid);
			$data['total'] = $this->total;
			$data['max_page'] = $this->max_page;
			$data['cate_name'] = $cate_name; //分类名称
			$data['list'] = $goods_list;
			$this->info = array('status'=>1, 'message'=>'请求成功', 'data'=>$data);
		}else{
			$this->info['message'] = '暂无数据';
		}
		$this->callBack();
	}
	
	/*
	* 2015.11.27 13:40 新增加
	* 7、获取限购专区的商品列表
	*/
	public function get_ten_yuan_list()
	{
		$indata = $this->getData();	//获取数据
		$this->checkSign($indata);		//验证签名
		$size = isset($indata['size']) ? intval($indata['size']) : 10;	//分页的大小
		$page = isset($indata['page']) ? intval($indata['page']) : 1;	//第几页
		$type = isset($indata['type']) ? intval($indata['type']) : 0;	//类型 如 人气、最新、剩余人次(排序)、总需人次(排序)
		$wh = " `shenyurenshu` > 0  AND  `q_uid` IS NULL AND `is_shi` = 1 ";	//is_shi =1 表示10元专区
		$uid  = isset($indata['uid'])  ? intval($indata['uid']) : 0;
		
		/* 2016/01/04 苹果APP上架期间不允许显示苹果相关产品  审核通过后要去掉该代码 start */
		$is_ios = $this->is_ios($indata);
		$version = isset($indata['version']) ? trim($indata['version']) : '1.0.0';    //获取用户的版本号,最新版的在审核阶段是不允许显示的
		if( $is_ios  && $this->compare_version($version) ){
		    $wh .= ' AND `is_apple` = 0 ';
		}
		/* 2016/01/04  苹果APP上架期间不允许显示苹果相关产品  审核通过后要去掉该代码 end */
		$type_arr = array('canyurenshu DESC','pos DESC','time DESC','shenyurenshu ASC','zongrenshu ASC');
		$type = $type_arr[$type];
		$wh.= "ORDER BY  $type,`order` DESC";
		$field = 'id,sid,title,zongrenshu,canyurenshu,thumb,yunjiage,qishu,default_renci,is_shi as is_ten';
		$ten_list = $this->get_shoplist($wh,$field,$page,$size);
		if($goods_list == 1){
			$this->info['message'] = '暂无数据';
			$this->callBack();
		}
		if($goods_list == 2){
			$this->info['message'] = '后面没有数据了';
			$this->callBack();
		}
		if(count($ten_list)){
			//数据整理
			$ten_list = $this->datalist($ten_list,$uid);
			$data['total'] = $this->total;
			$data['max_page'] = $this->max_page;
			$data['list'] = $ten_list;
			$this->info = array('status'=>1, 'message'=>'请求成功', 'data'=>$data);
		}else{
			$this->info['message'] = '暂无数据';
		}
		$this->callBack();
	}
	
        /*
	* 2017.05.27 10:00 新增加
	* 7、获取PK专区的商品列表
	*/
        public function get_pk_yuan_list()
	{
		$indata = $this->getData();	//获取数据
		//$this->checkSign($indata);		//验证签名
		$size = isset($indata['size']) ? intval($indata['size']) : 10;	//分页的大小
		$page = isset($indata['page']) ? intval($indata['page']) : 1;	//第几页
		$type = isset($indata['type']) ? intval($indata['type']) : 0;	//类型 如 人气、最新、剩余人次(排序)、总需人次(排序)
		$wh = " `shenyurenshu` > 0  AND  `q_uid` IS NULL AND `pk_product` = 1 ";	//is_shi =1 表示10元专区
		$uid  = isset($indata['uid'])  ? intval($indata['uid']) : 0;
		
		/* 2016/01/04 苹果APP上架期间不允许显示苹果相关产品  审核通过后要去掉该代码 start */
		$is_ios = $this->is_ios($indata);
		$version = isset($indata['version']) ? trim($indata['version']) : '1.0.0';    //获取用户的版本号,最新版的在审核阶段是不允许显示的
		if( $is_ios  && $this->compare_version($version) ){
		    $wh .= ' AND `is_apple` = 0 ';
		}
		/* 2016/01/04  苹果APP上架期间不允许显示苹果相关产品  审核通过后要去掉该代码 end */
		$type_arr = array('canyurenshu DESC','pos DESC','time DESC','shenyurenshu ASC','zongrenshu ASC');
		$type = $type_arr[$type];
		$wh.= "ORDER BY  $type,`order` DESC";
		$field = 'id,sid,title,zongrenshu,canyurenshu,thumb,yunjiage,qishu,default_renci,pk_product,pk_number,pk_cishu,pk_product,is_shi as is_ten';
		$ten_list = $this->get_shoplist($wh,$field,$page,$size);
                if(!empty($ten_list)){
                    foreach ($ten_list as $k => $v){
                        $ten_list[$k]['pk_cishu'] = $v['canyurenshu']/$v['default_renci'];
                    }
                }
		if($goods_list == 1){
			$this->info['message'] = '暂无数据';
			$this->callBack();
		}
		if($goods_list == 2){
			$this->info['message'] = '后面没有数据了';
			$this->callBack();
		}
		if(count($ten_list)){
			//数据整理
			$ten_list = $this->datalist($ten_list,$uid);
			$data['total'] = $this->total;
			$data['max_page'] = $this->max_page;
			$data['list'] = $ten_list;
			$this->info = array('status'=>1, 'message'=>'请求成功', 'data'=>$data);
		}else{
			$this->info['message'] = '暂无数据';
		}
		$this->callBack();
	}
        
	/*
	* 2015.11.27 14:35 新增加
	* 8、获取某一类文章(如[首页]的常见问题、[发现]里面的文章、[会员中心]右上角的通知)		
	* 参数>> 类型:type			取值说明: 1发现   2常见问题   3通知   默认为2
	*/
	public function get_article_list()
	{
		$indata = $this->getData();	//获取数据
		$this->checkSign($indata);		//验证签名
		$type = isset($indata['type']) ? intval($indata['type']) : 2;	//默认获取常见问题的文章列表
		$size = isset($indata['size']) ? intval($indata['size']) : 10;	//分页的大小
		$page = isset($indata['page']) ? intval($indata['page']) : 1;	//第几页

		$total = $this->db->GetCount('SELECT `id` FROM @#_app_article WHERE `type`=' . $type . ' AND `is_show` = 1  ');	//统计总数
		if( ! $total ){
			$this->info['message'] = '暂无数据';
			$this->callBack();
		}
		$page_count = intval($total / $size);	//总页数取整
		$max_page = $total % $size == 0 ? $page_count : $page_count + 1;	//最大页数
		if($page > $max_page){
			$this->info['message'] = '后面没有数据了';
			$this->callBack();
		}
		
		$start = $page <= 1 ? 0 : ($page-1) * $size;	//从第几条数据开始取
		$sql = "SELECT id,title,img_path,content,description,time,is_new FROM `@#_app_article` WHERE `type`='$type' AND `is_show` = 1 ORDER BY order_num LIMIT $start,$size";
		$arr_list = $this->db->Getlist($sql);
		$num = count($arr_list);
		if($num>0){
			foreach($arr_list AS $key=>$val){
				$arr_list[$key]['content'] = str_replace('&nbsp;',' ',$val['content']);
				$arr_list[$key]['title'] = str_replace('&nbsp;',' ',$val['title']);
				$arr_list[$key]['time'] = date("Y-m-d H:i:s",$val['time']);
				if(strpos($val['img_path'],G_UPLOAD_PATH) === false){		//如果图片的地址不是完整路径，那么就给它补齐
					$arr_list[$key]['img_path'] = G_UPLOAD_PATH . '/' . $val['img_path'];
				}
				$arr_list[$key]['sort'] = ($page -1) * $size + ($key +1);
				$arr_list[$key]['link'] = G_WEB_PATH.'/yungou/#/tab/articleInfo?id='.$val['id'];   //文章详情的链接
			}
			$this->info = array("status"=>1,"message"=>"请求成功","data"=>$arr_list);
		}else{
			$this->info['messsage'] = "暂无数据";
		}
		$this->callBack();	
	}
	
	/*
	* 2015.11.27 14:40 新增加
	* 9、获取文章的详细信息(主要针对 通告)		
	* 参数>> 文章ID:id
	*/
	public function get_article_info()
	{
		$indata = $this->getData();	//获取数据
		$this->checkSign($indata);		//验证签名
		$article_id = isset($indata['id']) ? intval($indata['id']) : 0 ;

        if(!$article_id){
			$this->info['messsage'] = "参数错误";
			$this->callBack();
		}
		$sql = "SELECT id,title,time,content FROM `@#_app_article` WHERE id='$article_id'";
		$one_arr = $this->db->GetOne($sql);
		if(isset($one_arr['id'])){
			$one_arr['time'] = date("Y-m-d",$one_arr['time']);
			$this->info = array("status"=>1,"message"=>"请求成功","data"=>$one_arr);
		}else{
			$this->info['messsage'] = "请求失败";
		}
		$this->callBack();
		
	}
	
	/*
	* 2015.12.5
	* 10、获取[热门搜索]关键词	
	*/
	public function get_search_hot_words()
	{
		$indata = $this->getData();	//获取数据
		$this->checkSign($indata);		//验证签名
		
		$data = array();
		$words = System::load_sys_config("hot_words");
		
		/* 2016/01/04 苹果APP上架期间不允许显示苹果相关产品  审核通过后要去掉该代码 start */
		$version = isset($indata['version']) ? trim($indata['version']) : '1.0.0';    //获取用户的版本号,最新版的在审核阶段是不允许显示的
		if( $this->is_ios($indata)  && $this->compare_version($version) ){
		    $words = str_replace('苹果','',$words);
		}
		$words = trim($words,',');
		/* 2016/01/04  苹果APP上架期间不允许显示苹果相关产品  审核通过后要去掉该代码 end */
		
		if( ! empty($words)){
			$words = str_replace('，',',',$words);
			$data = explode(',',$words);
		}
		
		$this->info = array('status'=>1, 'message'=>'请求成功', 'data'=>$data);
		$this->callBack();
	}
	
/***********************************************************************************************	/
/***********************************************************************************************/
	//计算中奖时间
	private function lotteryTime($target = '')
	{
		$result = '';
		if( ! empty($target) ){
			$d = 86400;
			$h = 3600;
			$i = 60;
			$now_time = time();
			$surplus = $now_time - $target;
			if($surplus >= $d){
				$result = intval($surplus/$d) . '天';
			}elseif($surplus >= $h && $surplus < $d){
				$result = intval($surplus/$h) . '小时';
			}elseif($surplus >= $i && $surplus < $h){
				$result = intval($surplus/$i) . '分钟';
			}else{
				$result = $surplus . '秒';
			}
		}
		return $result;
	}

	
	
}