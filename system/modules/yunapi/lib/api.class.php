<?php
/*
* 接口公共
*/
defined('G_IN_SYSTEM')or exit('No permission resources.');
System::load_sys_fun("user");		//用到了里面的get_user_name()  函数

class api extends SystemAction {
	
	public function __construct()
	{
		$this->db=System::load_sys_class('model');
	}
	
	//返回信息
	protected $info = array('status'=>0, 'message'=>'@#_22请求失败');
	
	//来自哪个平台
	protected function get_platform($partner='')
	{
	    if(!$partner){
	        return 'WEB网站';
	    }
	    $plat = array(
	        'ZLAPITOH5WAP'=>'手机网站',
	        'ZL888ANDROID'=>'安卓',
	        'ZL999IOSAPI'=>'苹果',
	        'ZLWEBAPITOWEB'=>'WEB网站'
	    );
	    return isset($plat[$partner]) ? $plat[$partner] : 'WEB网站';
	}
	
	//验证签名
	protected function checkSign($data = array())
	{
	    //合作者 => key
		$key_arr = array(
		    'ZLAPITOH5WAP' => '90782AC08A87C5E750369836C34DB46B',
		    'ZL888ANDROID' => '3860DD4B6E04448D3666B3F94CFA3DD7',
		    'ZL999IOSAPI'  => 'E4394EDA25BD6EA789CCC41CED64495E',
		    'ZLWEBAPITOWEB'  => '8O3F4ADJ600FQCA6D911G41DC38A4H5L'
		);
		$partner 		= isset($data['partner']) 	? trim($data['partner']) : '';
		$timestamp 		= isset($data['timestamp']) ? intval($data['timestamp']) : 0;
		$insign 		= isset($data['sign']) 		? trim($data['sign']) : '';
		
		if( ! $partner || ! $timestamp || ! $insign){
		    $this->info['message'] = '@#_1缺少参数！';
		    $this->callBack();
		}
		
		if( ! isset($key_arr[$partner]) ){
			$this->info['message'] = '@#_2合作者名称错误！';
			$this->callBack();
		}
		
		//web 和 h5只能通过ajax访问
		if($partner == 'ZLWEBAPITOWEB' || $partner == 'ZLAPITOH5WAP'){
		    if(!is_ajax()){
		        $this->info['message'] = '@#_3非法访问！';
		        $this->callBack();
		    }
		}
		
		//暂定过期时间为60秒   >> 不能做时间限制， 不然用户手机时间与服务器时间相差太大的话，签名会过期
		/*if($partner != 'ZLAPITOH5WAP'){   //手机网站暂不限制
    		$time = time();
    		$min_time = $time - 60;
    		if($timestamp < $min_time){
    			//$this->info['message'] = '该签名已过期！';
    			$this->callBack();
    		}
		}*/
		
		$key = $key_arr[$partner];    
		
		$sign = md5($partner.$timestamp.$key);
		if( ! $insign || $insign !== $sign){
			$this->info['message'] = '@#_4签名错误！';
			$this->callBack();
		}
		return true;
	}
	
	//获取数据
	protected function getData()
	{
		$temp =isset($_REQUEST["data"]) ? stripslashes($_REQUEST["data"]) : '';
		file_put_contents("a.txt", $temp);
		$data = json_decode($temp,true); //格式为xxx.com/index.php?data={"a":"a1","b":"b1"}
		if(!count($data)){		
			//格式为xxx.com/index.php?a=a1&b=b1
			$data = $_REQUEST;
		}
		if(count($data)){
			foreach($data as $k => $v){
			    $data[$k] = safe_replace($v);		//字符过滤  safe_replace函数在system/funs/global.fun.php
			}
		}
		
		return $data;
	}
	
	//返回数据
	protected function callBack()
	{
		/*
		* ios如果使用AFNetworking这套网络请求包是接收不到content-type:text/html的数据
		* 解决方法1：把以下header里面改为content-text:application/json
		* 解决方法2：ios端处理，参考链接：http://blog.sina.com.cn/s/blog_62f189570102va6j.html
		*/
		header("content-type:application/json,charset=utf8");		
		$info = $this->info;
		die(json_encode($info));
	}
	
	//是否是安卓设备请求
	protected function is_android($data)
	{
	    return $data['partner']=='ZL888ANDROID' ? true : false;
	}
	
	//是否是苹果设备请求
	protected function is_ios($data)
	{
	    return $data['partner']=='ZL999IOSAPI' ? true : false;
	}
	
	//是否是手机网站请求
	protected function is_wap($data)
	{
	    return $data['partner']=='ZLAPITOH5WAP' ? true : false;
	}
	
	//是否是手机网站请求
	protected function is_web($data)
	{
	    return $data['partner']=='ZLWEBAPITOWEB' ? true : false;
	}
	
	//比较ios用户的版本
	protected  function compare_version_copy($ver)
	{
	    $version_info = System::load_sys_config('ios');
	    $version = $version_info['ver_name'];
	    return $ver == $version ? false : true;
	}

    protected function compare_version($ver){
        $ver = str_replace('.','',$ver);    //将版本信息转换成数字

        $version_info = System::load_sys_config('ios');
        $version = str_replace('.','',$version_info['ver_name']);

        return $ver>$version ? true :false;

    }



	//获取会员的参与次数与号码
	protected function get_member_go_record($id = '', $uid = '', $qishu = '')
	{
		$record = array();
		if( ! $id || ! $uid  || ! $qishu){
			return array();
		}
		$sql = 'SELECT `gonumber`,`goucode` FROM @#_member_go_record WHERE `uid` = ' . $uid . ' AND `shopqishu` = ' . $qishu . ' AND `shopid` = '. $id;
		$data = $this->db->GetList($sql);
		if(count($data)){
			foreach($data as $k => $v){
				$record['gonumber'] += $v['gonumber'];
				$record['goucode'] .= ',' . $v['goucode'];
			}
			$record['goucode'] = ltrim($record['goucode'],',');
		}
		return $record;
	}
	
	//把手机号码中间4位替换成星号
	protected function nick_replace($str = '')
	{
		$str = substr_replace($str,'****',3,4);
		return $str;
	}
	
	//获取支付方式列表
	protected function get_pay_type_list($pay_type=false)
	{
	    $wh = "";
	    if($pay_type){$wh = " AND `pay_class`='$pay_type' ";}
	    $data = $this->db->GetList("SELECT pay_name,pay_class,pay_type FROM @#_pay WHERE `pay_start`=1 $wh");
	    //$data = array(array('pay_name'=>'支付宝','pay_class'=>'alipay','pay_type'=>1));
	    return $data;
	}
	
	//获取商品的某个或某些字段
	protected function get_goods_fields($id = '', $qishu= '', $fields = 'id')
	{
	   if( ! $id || ! $qishu){
		   return '数据异常,请稍后再试...';
	   }
	   $field = $fields;
	   $sql = 'SELECT ' . $field . ' FROM @#_shoplist WHERE id=' . $id . ' AND `qishu` = ' . $qishu . ' LIMIT 1' ;
	   $infos = $this->db->GetOne($sql);
	   if(isset($infos['id'])){
		   return $infos;
	   }else{
		   return '指定商品不存在！';
	   }
	}
	
	
	//获取用户的信息(要什么字段就传什么字段)
   protected function get_user_fields($uid='',$fields='')
   {
	   if(!$uid ||  !$fields){
		   return array();
	   }
	   $infos = $this->db->GetOne("SELECT $fields FROM @#_member WHERE `uid` = $uid LIMIT 1");
	   return $infos;
   }
   
   //替换用户头像
   protected function touimg_replace($source)
   {
	   $touimg = $source;
	   if($touimg != 'photo/member.jpg'){
		   $touimg = str_replace('.','.jpg_160160.',$touimg);
	   }
	   $touimg = G_UPLOAD_PATH . '/' . $touimg;
	   return $touimg;
   }
   
   //返利统计
   protected function get_rebate_total()
   {
      $data = $this->db->GetOne("SELECT distinct `yaoqing` as total FROM @#_member_jifen WHERE `yaoqing`>0 ");
      return $data['total'];
   }
   
   //密码加密规则  base64 编码(6个随机字符+用户密码+6个随机字符)   “+”号只是提示作用
   protected function decode_password($password)
   {
       $temppwd = base64_decode($password);
       $len = strlen($temppwd);
       $password = substr($temppwd, 6 , $len-12);
       return $password;
   }
   
   //编码表情
   protected function emj_encode($content='')
   {
       $text = preg_replace_callback('/[\xf0-\xf7].{3}/', function($r) { return '@E' . base64_encode($r[0]);}, $content);
       return $text;
   }
   
   //解码表情
   protected function emj_decode($content='')
   {
       $text = preg_replace_callback('/[\xf0-\xf7].{3}/', function($r) { return '@E' . base64_encode($r[0]);}, $content);
       return $text;
   }
   
   //获取二维码
   protected function get_member_qrcode($uid='', $type='wap')
   {
       //wap注册链接
       $wap_url = G_WEB_PATH . '/yungou/index.html#/tab/rigister?yaoqing='.$uid;
       
       $filename = $type.'_'.md5($uid).'.png';
       $path = G_UPLOAD . '/user_qrcode/' . $filename;
       $codeImg = G_UPLOAD_PATH . '/user_qrcode/' . $filename;
       
       if(!file_exists($path)){
           $this->create_qrcode($wap_url,$path);
       }     
       
       return $codeImg;
   }
   
   //生成二维码
   private function create_qrcode($url=G_WEB_PATH,$filename='test.png')
   {
       System::load_app_class('qrcode');
       $value = $url; //二维码内容
       $errorCorrectionLevel = 'H';//容错级别
       $matrixPointSize = 8;//生成图片大小 
       
       //生成二维码图片
       QRcode::png($value, 'qrcode.png', $errorCorrectionLevel, $matrixPointSize, 2);
       $logo = 'yyzl.png';//准备好的logo图片
       $QR = 'qrcode.png';//已经生成的原始二维码图
       
       if ($logo !== FALSE) {
           $QR = imagecreatefromstring(file_get_contents($QR));
           $logo = imagecreatefromstring(file_get_contents($logo));
           $QR_width = imagesx($QR);//二维码图片宽度
           $QR_height = imagesy($QR);//二维码图片高度
           $logo_width = imagesx($logo);//logo图片宽度
           $logo_height = imagesy($logo);//logo图片高度
           $logo_qr_width = $QR_width / 5;
           $scale = $logo_width/$logo_qr_width;
           $logo_qr_height = $logo_height/$scale;
           $from_width = ($QR_width - $logo_qr_width) / 2;
           //重新组合图片并调整大小
           imagecopyresampled($QR, $logo, $from_width, $from_width, 0, 0, $logo_qr_width,$logo_qr_height, $logo_width, $logo_height);
       }
       //输出图片
       imagepng($QR, $filename);
   }
}
