<?php
/*
* 自定义函数列表
*/

//打印数据
function pr($arr = array(), $is_pre = 0)
{
	header("content-type:text/html;charset=utf8");
	if($is_pre){
		echo "<pre>";
	}
	print_r($arr);
}

//格式化揭晓日期
function microt($time,$x=null){
	$len=strlen($time);
	if($len<13){
		$time=$time."0";
	}
	$list=explode(".",$time);	
	if($x=="L"){		
		return date("His",$list[0]).substr($list[1],0,3);
	}else if($x=="Y"){
		return date("Y-m-d",$list[0]);
	}else if($x=="H"){
		return date("H:i:s",$list[0]).".".substr($list[1],0,3);
	}else if($x=="r"){
		return date("Y年m月d日 H:i",$list[0]);
	}else{
		return date("Y-m-d H:i:s",$list[0]).".".substr($list[1],0,3);
	}
}

//根据ip获取城市  
function GetIpLookup($ip = ''){  
	if(empty($ip)){  
		return array('province'=>'','city'=>'');
	} 
	$res = @file_get_contents('http://int.dpool.sina.com.cn/iplookup/iplookup.php?format=js&ip=' . $ip);  
	if(empty($res)){ return false; }  
	$jsonMatches = array();  
	preg_match('#\{.+?\}#', $res, $jsonMatches);  
	if(!isset($jsonMatches[0])){ return false; }  
	$json = json_decode($jsonMatches[0], true);  
	if(isset($json['ret']) && $json['ret'] == 1){  
		$json['ip'] = $ip;  
		unset($json['ret']);  
	}else{  
		return false;  
	}  
	return $json;  
}