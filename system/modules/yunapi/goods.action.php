<?php
/*
* 商品相关API
*/
defined('G_IN_SYSTEM')or exit('no');
System::load_app_class('api');
System::load_app_fun('myfun');

class Goods extends api{
	/*
	* 1、获取商品的详细信息
	* 参数 >> id:shoplist里面的ID  , uid:可传可不传，如果传入了，则会返回该用户的参与信息
	*/
	public function get_goods_info()
	{
		$indata = $this->getData();	//获取数据
		$this->checkSign($indata);		//验证签名
		$id = isset($indata['id']) ? intval($indata['id']) : 0;		//shoplist里面的ID 唯一标识
		$uid = isset($indata['uid']) ? intval($indata['uid']) : 0;		//用户id
	
		if( ! $id ){
			$this->info['message'] = '请求失败';
			$this->callBack();
		}
		$field = " id,sid,title,title2,yunjiage,thumb,zongrenshu,canyurenshu,picarr,q_showtime,q_uid,q_user,qishu,q_user_code,q_end_time,maxqishu,time,default_renci,is_shi as is_ten,pk_number ";
//                $field = " id,sid,title,title2,yunjiage,thumb,zongrenshu,canyurenshu,picarr,q_showtime,q_uid,q_user,qishu,q_user_code,q_end_time,maxqishu,time,default_renci,is_shi，pk_product as is_ten ";
		$sql = "SELECT $field FROM `@#_shoplist` WHERE `id`= '$id'  LIMIT 1";
		$goods=$this->db->GetOne($sql);
		if( ! isset($goods['id'])){
			$this->info['message'] = '指定商品不存在';
			$this->callBack();
		}
		//获取该商品最近两期的id(包括自己)
		$qishu_arr = $this->get_more_qishu($goods['sid'],$goods['qishu']);
		
		
		//tag   0已揭晓  1进行中  2倒计时	3期数已满且已揭晓，商品下架
		$goods['tag'] = 1;
		$time = time()-$goods['q_end_time'];
		if($goods['canyurenshu'] == $goods['zongrenshu']){
			if($goods['q_showtime']=='Y'){
				$goods['tag'] = 2;	
			}
			if($goods['q_showtime']=='N' && intval($goods['q_uid'])>0){
				$goods['tag'] = 0;
				if(intval($goods['qishu']) >= $goods['maxqishu']){
					$goods['tag'] = 3;	
				}
			}
		}

		$goods['thumb'] = strpos($goods['thumb'],G_UPLOAD_PATH)===false ? G_UPLOAD_PATH .'/'. $goods['thumb'] : $goods['thumb'];
		$goods['picarr'] = unserialize($goods['picarr']);	//处理商品相册
		if(count($goods['picarr'])){
			foreach($goods['picarr'] as $k => $v){
				$goods['picarr'][$k] = strpos($v,G_UPLOAD_PATH) === false ? G_UPLOAD_PATH . '/' . $v : $v;
			}
		}
		if($goods['tag']==0 ||$goods['tag']==3){
			$goods['q_user'] = unserialize($goods['q_user']);	//获奖者信息
			if( isset($goods['q_user']['uid']) && intval($goods['q_user']['uid']) ){
				$q_user_info = $this->get_user_fields($goods['q_user']['uid'],'user_ip');
				$goods['q_user']['address'] = '';
				if(isset($q_user_info['user_ip'])){
					$ip_info = explode(',',$q_user_info['user_ip']);
					$goods['q_user']['address'] = isset($ip_info[0]) ? trim($ip_info[0]) : '';
				}
				$go_record = $this->get_member_go_record($goods['id'], $goods['q_user']['uid'], $goods['qishu']);
				$goods['q_user']['renci'] = isset($go_record['gonumber']) ? $go_record['gonumber'] : 0 ; //该用户本期参与了几次
				$goods['q_user']['img'] = $this->touimg_replace($goods['q_user']['img']);
				$username = get_user_name($goods['q_user']['uid']);	//用户名
				$goods['q_user']['username'] = $username;	//用户名
				//$goods['q_end_time'] = microt($goods['q_end_time']);	//揭晓时间
			}else{
				unset($goods['q_user']);
			}
		}
		$goods['surplus'] = 0;
		if($goods['tag'] == 2){	//倒计时
		unset($goods['q_user']);
			$goods['surplus'] = ( $goods['q_end_time'] - time() ) * 1000 + 5000; //时间差	(还剩多少毫秒开奖)  +5000表示多加5秒确保调接口时这边已经开奖
		
			/*wap倒计时  请勿删除*/
			$q_time =intval($goods['q_end_time']);
			$q_djstime = $q_time - time();
			$goods['test_time'] = $q_djstime;
			
			/*wap倒计时  请勿删除*/
		}
		
		
		//如果商品状态为已揭晓或倒计时，那么判断商品期数是否已满，如果没有满期那么返回最新一期的ID  2015.12.2 15:59
		$goods['next_id'] = $goods['id'];
		if($goods['tag'] == 0 || $goods['tag'] == 2){
			$next_info = $this->db->GetOne("SELECT id FROM @#_shoplist WHERE `sid` = " . $goods['sid'] . " AND `qishu` > " . $goods['qishu'] . " ORDER BY `qishu` DESC LIMIT 1");
			if(isset($next_info['id']) && $next_info['id']){
				$goods['next_id'] = $next_info['id'];
			}
		}
		
		//当前用户的参与信息(如果接收到了uid的话)
		$curr_uinfo = array('is_join'=>0, 'gonumber'=>'', 'goucode'=>'', 'goucode_all'=>'', 'content'=>'您没有参与本期众乐哦！');
		if($uid > 0){
			$curr_record = $this->get_member_go_record($goods['id'], $uid, $goods['qishu']);
			if( isset($curr_record['gonumber']) && $curr_record['gonumber'] > 0 ){
				$curr_uinfo['is_join'] = 1;
				$curr_uinfo['gonumber'] = $curr_record['gonumber'];
				$curr_uinfo['goucode'] = $curr_record['goucode'];
				$curr_uinfo['goucode_all'] = $curr_record['goucode'];
				if($curr_uinfo['gonumber'] > 6){
                    $all=$str='';
					$goucode = explode(',',$curr_record['goucode']);
					foreach($goucode as $k =>$v){
						if($k < 6){
							$str .= ',' . $v;
						}
						$all .= ',' . $v;
					}
					$curr_uinfo['goucode'] = ltrim($str,',');
				}
				$curr_uinfo['content'] = '您参与了本期众乐';
			}
		}
		$goods['start_time'] = $goods['time'] ? date('Y-m-d H:i:s',$goods['time']) : '';	//所有参与记录中，开始时间
		unset($goods['q_uid'],$goods['maxqishu'],$goods['time']);
		$goods['curr_uinfo'] = $curr_uinfo;
		$goods['yunjiage'] = intval($goods['yunjiage']);
		$goods['is_ten'] = intval($goods['is_ten']);		//是否是10元专区
		$syrs = $goods['zongrenshu'] - $goods['canyurenshu'];
		if($goods['is_ten'] == 1){
			$xiangou_number = 5-$curr_uinfo['gonumber'];
			$goods['wap_ten'] = ($xiangou_number > 0) ? $xiangou_number:0;
		}else if($goods['is_ten'] == 2){

		}else{
			$goods['wap_ten'] = $syrs;											//用户正常状态
		}
		

		$goods['q_end_time'] = date('y-m-d H:i:s',$goods['q_end_time']);
		$goods['title'] = str_replace('&nbsp;',' ',$goods['title']);  //主标题
		$goods['title2'] = str_replace('&nbsp;',' ',$goods['title2']);    //副标题
		$goods['ratio'] = ceil($goods['canyurenshu']/$goods['zongrenshu']   * 100 );		//h5需要的字段  进度条百分比
        $goods['wap_link'] = G_WEB_PATH . "/yungou/index.html#/tab/appProductInfo?id=" . $id;   //跳转至wap商品详情
        $goods['calc_link'] = G_WEB_PATH . "/yungou/index.html#/tab/appCalculation?id=" . $id;   //跳转至wap计算详情
		
		$this->info = array('status'=>1, 'message'=>'请求成功', 'data'=>$goods,'qishu'=>$qishu_arr);
		$this->callBack();
	}
	
	/*
	* 2、获取商品的图文详情
	* 参数 >> id:shoplist里面的ID  
	*/
	public function get_goods_pic_content()
	{
		$indata = $this->getData();	//获取数据
		$this->checkSign($indata);		//验证签名
		$id = isset($indata['id']) ? intval($indata['id']) : 0;		//shoplist里面的ID 唯一标识
		
		if( ! $id ){
			$this->info['message'] = '请求失败';
			$this->callBack();
		}
		$content = $this->db->GetOne('SELECT `id`,`content` FROM @#_shoplist WHERE `id` = ' . $id . ' LIMIT 1');
		if(isset($content['id'])){
			$this->info = array('status'=>1, 'message'=>'请求成功', 'data'=>$content);
		}else{
			$this->info['message'] = '指定商品不存在';
		}
		$this->callBack();
	}

	/*
	* 3、获取商品的往期揭晓
	* 参数 >> sid:商品的ID  , size:每一页几条数据 , page:取第几页
	*/
	public function get_old_lottery()
	{
		$indata = $this->getData();	//获取数据
		$this->checkSign($indata);		//验证签名
		$sid = isset($indata['sid']) ? intval($indata['sid']) : 0;		//shoplist里面的sid 也就是：商品ID
		$size = isset($indata['size']) ? intval($indata['size']) : 10;	//分页的大小
		$page = isset($indata['page']) ? intval($indata['page']) : 1;	//第几页
		$time = time();
		if( ! $sid){
			$this->info['message'] = '请求失败';
			$this->callBack();
		}
		$total = $this->db->GetCount('SELECT id FROM @#_shoplist WHERE `sid` = ' . $sid);
		if( ! $total ){
			$this->info['message'] = '暂无数据';
			$this->callBack();
		}
		$page_count = intval($total / $size);
		$max_page = $total % $size == 0 ? $page_count : $page_count + 1;
		if($page > $max_page){
			$this->info['message'] = '后面没有数据了';
			$this->callBack();
		}
		
		$start = $page <= 1 ? 0 : ($page-1) * $size;	//从第几条数据开始取
		$sql = 'SELECT id,sid,q_end_time,q_showtime,qishu,q_user_code,q_uid,q_user,canyurenshu,zongrenshu FROM @#_shoplist WHERE `sid` = ' . $sid .  '  ORDER BY `qishu` DESC LIMIT ' . $start . ',' . $size ;
		$old_lottery = $this->db->GetList($sql);
		if(count($old_lottery)){
			foreach($old_lottery as $k => $v){
				//tag   0已揭晓  1进行中  2倒计时	3期数已满且已揭晓，商品下架
				$old_lottery[$k]['tag'] = intval($v['q_uid']) ?($v['q_showtime']=='Y' ? 2 : 0): 1 ;
				if($old_lottery[$k]['tag'] == 1){
					$old_lottery[$k]['bili'] = (($v['canyurenshu']/$v['zongrenshu']) * 100).'%';
				}
				if($old_lottery[$k]['tag'] == 2){
					$old_lottery[$k]['endtime'] = date('m-d H:i:s', $v['q_end_time']);
				}
				unset($old_lottery[$k]['canyurenshu'],$old_lottery[$k]['zongrenshu']);
				$old_lottery[$k]['q_uid'] = intval($old_lottery[$k]['q_uid']);
				$old_lottery[$k]['q_user_code'] = trim($old_lottery[$k]['q_user_code']);
				
				$old_lottery[$k]['q_user'] = $old_lottery[$k]['q_user'] ? unserialize($old_lottery[$k]['q_user']) : '';
				if( isset($old_lottery[$k]['q_user']['uid']) && intval($old_lottery[$k]['q_user']['uid']) ){
					$old_lottery[$k]['q_user']['username'] = get_user_name($old_lottery[$k]['q_user']['uid']); 
					$go_record = $this->get_member_go_record($old_lottery[$k]['id'], $old_lottery[$k]['q_user']['uid'], $old_lottery[$k]['qishu']);
					$old_lottery[$k]['q_user']['renci'] = isset($go_record['gonumber']) ? $go_record['gonumber'] : 0 ; //该用户本期参与了几次
					$old_lottery[$k]['q_user']['img'] = $this->touimg_replace($old_lottery[$k]['q_user']['img']);
				}else{
					unset($old_lottery[$k]['q_user']);
				}
				//如果有中奖者，表示已经揭晓了的，否则表示即将揭晓
				if(intval($old_lottery[$k]['q_uid'])){
					$old_lottery[$k]['q_end_time'] = date('m-d H:i',$old_lottery[$k]['q_end_time']);	//揭晓时间
				}
			}
			$data['total'] = $total;
			$data['max_page'] = $max_page;
			$data['list'] = $old_lottery;
			$this->info = array('status'=>1, 'message'=>'请求成功', 'data'=>$data);
		}else{
			$this->info['message'] = '暂无数据';
		}
		$this->callBack();
	}
	
	/*
	* 4、获取商品的晒单分享
	* 参数 >> sid:商品的ID  , size:每一页几条数据 , page:取第几页
	*/
	public function get_shaidan_list()
	{
		$indata = $this->getData();	//获取数据
		$this->checkSign($indata);		//验证签名
		$sid = isset($indata['sid']) ? intval($indata['sid']) : 0;		//默认取所有商品的晒图分享 按 时间最近的排序
		$size = isset($indata['size']) ? intval($indata['size']) : 10;
		$page = isset($indata['page']) ? intval($indata['page']) : 1;
		
		$wh = ' sd.is_audit=2 ';	//is_audit=2 表示： 晒单审核通过的才显示
		if($sid > 0){
			$wh .= ' AND sd.sd_shopsid = ' . $sid;				//本产品的sid
		}
		
		$c_sql = "SELECT sd.sd_id FROM @#_shaidan as sd WHERE " . $wh;
		/* 2016/01/04 苹果APP上架期间不允许显示苹果相关产品  审核通过后要去掉该代码 start */
		$version = isset($indata['version']) ? trim($indata['version']) : '';    //获取用户的版本号,最新版的在审核阶段是不允许显示的
		if( $this->is_ios($indata) && $this->compare_version($version) ){
		    $c_sql = "SELECT sd.sd_id FROM @#_shaidan as sd, @#_shoplist as sl WHERE " . $wh . " AND sl.id=sd.sd_shopid AND sl.is_apple=0 ";
		}
		/* 2016/01/04  苹果APP上架期间不允许显示苹果相关产品  审核通过后要去掉该代码 end */
		$total = $this->db->GetCount($c_sql);
		
		if( ! $total ){
			$this->info['status'] = 1;
			$this->info['message'] = '暂无数据';
			$this->info['data'] = array('total'=>0);
			$this->callBack();
		}
		$page_count = intval($total / $size);
		$max_page = $total % $size == 0 ? $page_count : $page_count + 1;
		if($page > $max_page){
			$this->info['status'] = 1;
			$this->info['message'] = '后面没有数据了';
			$this->info['data'] = array('total'=>0);
			$this->callBack();
		}
		$start = $page <= 1 ? 0 : ($page-1) * $size;	//从第几条数据开始取
		$field = 'sd.sd_id,sd.sd_shopid,sd.sd_shopsid,sd.sd_title,sd.sd_content,sd.sd_photolist,sd.sd_time,sl.title,sl.q_user,sl.qishu';
		
		/* 2016/01/04 苹果APP上架期间不允许显示苹果相关产品  审核通过后要去掉该代码 start */
		$version = isset($indata['version']) ? trim($indata['version']) : '';    //获取用户的版本号,最新版的在审核阶段是不允许显示的
		if( $this->is_ios($indata) && $this->compare_version($version) ){
		    $wh .= ' AND `is_apple` = 0 ';
		}
		/* 2016/01/04  苹果APP上架期间不允许显示苹果相关产品  审核通过后要去掉该代码 end */
		
		$sql = 'SELECT ' . $field . ' FROM @#_shaidan as sd LEFT JOIN @#_shoplist as sl ON sl.id = sd.sd_shopid WHERE ' . $wh . ' ORDER BY `sd_time` DESC LIMIT ' . $start . ',' . $size ;
		$shaidan_list = $this->db->GetList($sql);
		if(count($shaidan_list)){
			foreach($shaidan_list as $k => $v){
				$shaidan_list[$k]['q_user'] = unserialize($v['q_user']);
				$shaidan_list[$k]['title'] = str_replace('&nbsp;',' ',$v['title']);
				$shaidan_list[$k]['title'] = $this->emj_decode($shaidan_list[$k]['title']);
				$shaidan_list[$k]['sd_title'] = str_replace('&nbsp;',' ',$v['sd_title']);
				$shaidan_list[$k]['sd_time'] = date('m-d H:i',$v['sd_time']);
				$sd_photolist = rtrim($v['sd_photolist'],';');
				$sd_photolist = explode(';',$sd_photolist);
				foreach($sd_photolist as $k1 => $v1){
					if(strpos($v1,G_UPLOAD_PATH) === false){
					    $houz = explode('.',$v1);
					    $houz = end($houz);
						$sd_photolist[$k1] = G_UPLOAD_PATH . '/' . $v1 . '_220.' . $houz;
					}
				}
				$shaidan_list[$k]['sd_photolist'] = $sd_photolist;
				
				$user_info = $this->get_user_fields($shaidan_list[$k]['q_user']['uid'],'img');    //获取最新头像
				$shaidan_list[$k]['q_user']['img'] = $this->touimg_replace($user_info['img']);
				
				$shaidan_list[$k]['q_user']['username'] = get_user_name($shaidan_list[$k]['q_user']['uid']); 		//用户名称
				unset($sd_photolist);
				$shaidan_list[$k]['sd_content'] = strip_tags($v['sd_content']);		//去除html标签
				$shaidan_list[$k]['sd_content'] = $this->emj_decode($shaidan_list[$k]['sd_content']);
				$shaidan_list[$k]['sd_link'] = G_WEB_PATH . '/yungou/index.html#/tab/shareDetail?come=Android&sd_id=' . $v['sd_id'];		//晒单详情页面链接
			}
			$data['total'] = $total;
			$data['max_page'] = $max_page;
			$data['list'] = $shaidan_list;
			$this->info = array('status'=>1, 'message'=>'请求成功', 'data'=>$data);
		}else{
			$this->info['status'] = 1;
			$this->info['message'] = '暂无数据';
			$this->info['data'] = array('total'=>0);
		}
		$this->callBack();
	}
	
	/*
	* 5、获取商品的所有参与记录
	* 
	*/
	public function get_record_list(){
		$indata = $this->getData();			//获取数据
		$this->checkSign($indata);			//验证签名
		$size = isset($indata['size']) ? intval($indata['size']) : 10;	//分页的大小
		$page = isset($indata['page']) ? intval($indata['page']) : 1;	//第几页
		$shopid = isset($indata['shopid']) ? intval($indata['shopid']) : 0;
		$qishu = isset($indata['qishu']) ? intval($indata['qishu']) : 0;
		
		if( !$shopid || !$qishu){
			$this->info['message'] = '请求失败';
			$this->callBack();
		}
		$res = "SELECT id FROM `@#_member_go_record` WHERE `shopid`=$shopid AND `shopqishu`=$qishu";
		$total=$this->db->GetCount($res);
		if( ! $total ){
			$this->info['status'] = 1;
			$this->info['message'] = '暂无数据';
			$this->info['data'] = array('total'=>0);
			$this->callBack();
		}
		$page_count = intval($total / $size);
		$max_page = $total % $size == 0 ? $page_count : $page_count + 1;
		if($page > $max_page){
			$this->info['status'] = 1;
			$this->info['message'] = '后面没有数据了';
			$this->info['data'] = array('total'=>0);
			$this->callBack();
		}
		
		$start = $page <= 1 ? 0 : ($page-1) * $size;	//从第几条数据开始取
		$field = 'uid,username,uphoto,ip,time,gonumber AS count_gonumber';
		$sql = "SELECT $field FROM `@#_member_go_record` WHERE `shopid`=$shopid AND `shopqishu`=$qishu ORDER BY `time` DESC  LIMIT $start,$size";
		$arr_list = $this->db->GetList($sql);
		$num = count($arr_list);
		if($num){
			for($i=0;$i < $num;$i++){
				$arr_list[$i]['time'] = microt($arr_list[$i]['time']);
				$ip_info = explode(',',$arr_list[$i]['ip']);
				$arr_list[$i]['address'] = isset($ip_info[0]) ? trim($ip_info[0]) : '';	//地址
				$arr_list[$i]['ip'] = end($ip_info);	//ip
				$arr_list[$i]['uphoto'] = $this->touimg_replace($arr_list[$i]['uphoto']);// G_UPLOAD_PATH ."/".$arr_list[$i]['uphoto'];
				$arr = GetIpLookup($arr_list[$i]['ip']);
				$arr_list[$i]['area'] = $arr['province'].$arr['city'];
				unset($arr);
			}
			$data['total'] = $total;
			$data['max_page'] = $max_page;
			$data['list'] = $arr_list;
			$this->info = array('status'=>1, 'message'=>'请求成功', 'data'=>$data);
		}else{
			$this->info['status'] = 1;
			$this->info['message'] = '暂无数据';
			$this->info['data'] = array('total'=>0);
		}
		$this->callBack();
	} 
   
	/*
    * 6、晒单详情
	* param sd_id   晒单的id
   */
   public function get_shaidan_info(){
	   $indata = $this->getData();			//获取数据
	   $this->checkSign($indata);			//验证签名
	   $sd_id = isset($indata['sd_id']) ? intval($indata['sd_id']) : 0;
	   
		if(!$sd_id){
		   $this->info['message'] = "请求失败";
		   $this->callBack();
		}
	   $field = "s.sd_id,s.sd_userid,s.sd_shopid,s.sd_title,s.sd_content,s.sd_time,s.sd_photolist,g.title,g.q_user_code,g.q_end_time, s.sd_qishu ,g.q_user";
	   $sql="SELECT $field FROM @#_shaidan AS s LEFT JOIN @#_shoplist AS g ON s.sd_shopid=g.id WHERE s.sd_id=$sd_id";
	   $data = $this->db->GetOne($sql);
	   if($data){
		   $q_user = unserialize($data['q_user']);
		   $data['username'] = get_user_name($q_user['uid']);
		   
		   $go_record = $this->get_member_go_record($data['sd_shopid'],$q_user['uid'],$data['sd_qishu']);
		   
		   $data['canyurenshu'] = $go_record['gonumber'];
		   
		   $data['img'] = explode(";",rtrim($data['sd_photolist'],';'));
		   foreach($data['img'] as $k => $v){
			   if(strpos($v,G_UPLOAD_PATH) === false){
				   $data['img'][$k] = G_UPLOAD_PATH . '/' . $v;
			   }
		   }
		   $data['q_end_time'] = microt($data['q_end_time']);
		   $title = str_replace('&nbsp;',' ',$data['title']);
		   $data['title'] = $this->emj_decode($title);
		   $data['sd_title'] = str_replace('&nbsp;',' ',$data['sd_title']);
		   $data['sd_time'] = date('Y-m-d H:i:s',$data['sd_time']);
		   $sd_content = strip_tags($data['sd_content']);		//去除html标签
		   $data['sd_content'] = $this->emj_decode($sd_content);
		   unset($data['q_user'],$data['sd_photolist']);
		   $this->info = array('status'=>1, 'message'=>'请求成功', 'data'=>$data);
	   }else{
		   $this->info['message'] = "暂无数据"; 
	   }
	   $this->callBack();   
   }
   
   /*
    * 7、最新揭晓
   */
   public function get_newest_lottery(){
   	
	   $indata = $this->getData();			//获取数据
	   $this->checkSign($indata);			//验证签名
	   $size = isset($indata['size']) ? intval($indata['size']) : 10;	//分页的大小
	   $page = isset($indata['page']) ? intval($indata['page']) : 1;	//第几页
	   
	   $max_time = time() + 3600;    //只显示1小时内开奖的商品
	   $wh = " `canyurenshu` = `zongrenshu` AND q_uid is not null AND `q_end_time`< $max_time ";
	   
	   /* 2016/01/04 苹果APP上架期间不允许显示苹果相关产品  审核通过后要去掉该代码 start */
	   $version = isset($indata['version']) ? trim($indata['version']) : '';    //获取用户的版本号,最新版的在审核阶段是不允许显示的
	   if( $this->is_ios($indata) && $this->compare_version($version) ){
	       $wh .= ' AND `is_apple` = 0 ';
	   }
	   /* 2016/01/04  苹果APP上架期间不允许显示苹果相关产品  审核通过后要去掉该代码 end */
	   
	   $total = $this->db->GetCount("SELECT id FROM `@#_shoplist` WHERE $wh ");
	   if(!$total){
		   $this->info['message'] = "暂无数据";
		   $this->callBack();
	   }
	  
	   $page_count = intval($total / $size);
		$max_page = $total % $size == 0 ? $page_count : $page_count + 1;
		if($page > $max_page){
			$this->info['message'] = '后面没有数据了';
			$this->callBack();
		}
		
		$start = $page <= 1 ? 0 : ($page-1) * $size;	//从第几条数据开始取
		
		$field = " id,q_uid,title,qishu,q_user,q_user_code,q_end_time,q_showtime,thumb,yunjiage,is_shi as is_ten";
		
		$sql = "SELECT $field FROM `@#_shoplist` WHERE $wh ORDER BY `q_showtime` DESC, `q_end_time` DESC LIMIT $start,$size";
		$arr_list = $this->db->GetList($sql);
		
		foreach($arr_list AS $k=>$val){
			$time = time();
			if($val['q_showtime']=='N' && $time>$val['q_end_time'] && $val['q_end_time']>0){
				$arr_list[$k]['tag'] = 0;	//已揭晓
				$arr_list[$k]['jiexiao_time'] = 0;
			}else{
				$arr_list[$k]['tag'] = 1;	//未揭晓（也就是倒计时...）
				$arr_list[$k]['jiexiao_time'] = intval($val['q_end_time'] - $time) * 1000 + 5000;	//还剩多少毫秒开奖  +5000表示多加5秒确保调接口时这边已经开
			}
			$arr_list[$k]['q_user_code'] = !empty($val['q_user_code']) ? $val['q_user_code'] : '';
			$arr_list[$k]['yunjiage'] = intval($arr_list[$k]['yunjiage']);
			$arr_list[$k]['is_ten'] = intval($val['is_ten']);		//是否是10元专区
			$arr_list[$k]['wap_ten'] = intval($val['is_ten']) ? 10 : 1;		//是否是10元专区(h5需要的字段 10表示10元专区  1表示非10元专区)
			$arr_list[$k]['title'] = str_replace('&nbsp;', ' ', $val['title']);
			$arr = unserialize($arr_list[$k]['q_user']);
			$arr_list[$k]['username'] = get_user_name($arr['uid']);	//用户名称
			$arr_number = $this->get_member_go_record($arr_list[$k]['id'],$arr_list[$k]['q_uid'],$arr_list[$k]['qishu']);
			$arr_list[$k]['gonumber'] = intval($arr_number['gonumber']);
			$arr_list[$k]['q_end_time'] = date("Y年m月d日 H:i:s",$val['q_end_time']);
			
			/*******wap测试倒计时   请勿删除*/
			$q_time = intval(substr($val['q_end_time'],0,10));
			if($q_time-time() <= 3540){
				$q_time = $q_time-time();
			}
			$arr_list[$k]['test_time'] = $q_time;
			/********/
			
			if(strpos($arr_list[$k]['thumb'],G_UPLOAD_PATH) === false){
				$arr_list[$k]['thumb'] = G_UPLOAD_PATH.'/'.$arr_list[$k]['thumb'];
			}
			unset($arr_list[$k]['q_user']);
			unset($arr_number);
			unset($arr);
			$data['total'] = $total;
			$data['max_page'] = $max_page;
		}
		$data['list'] = $arr_list;
		$this->info = array("status"=>1,"message"=>"请求成功","data"=>$data);
        $this->callBack();
	   
   }
   
    /*
    * 8、[查看TA的号码] 云购详情
   */
   public function show_duobao_info()
   {
	   $indata = $this->getData();			//获取数据
	   $this->checkSign($indata);			//验证签名
	   $shopid = isset($indata['shopid']) ? intval($indata['shopid']) : 0;	//shoplist里面的ID
	   $qishu = isset($indata['qishu']) ? intval($indata['qishu']) : 0;	//期号
	   $uid = isset($indata['uid']) ? trim($indata['uid']) : 0;
	   
	   if( ! $shopid ||  ! $qishu  ||  ! $uid ){
		   $this->info['message'] = '请求失败';
		   $this->callBack();
	   }
	   $goods_info = $this->get_goods_fields($shopid,$qishu,'id,title');
	   if(isset($goods_info['id'])){
			$sql = 'SELECT id as rid,gonumber,time FROM @#_member_go_record WHERE `shopid` = ' . $shopid . ' AND `shopqishu` = ' . $qishu . ' AND `uid` = ' . $uid;
			$list = $this->db->GetList($sql);
			if(count($list)){
				foreach($list as $k => $v){
					$list[$k]['time'] = microt($v['time']);
				}
				$data['shopname'] = str_replace('&nbsp;','',$goods_info['title']);
				$data['qishu'] = $qishu;
				$records = $this->get_member_go_record($shopid, $uid, $qishu);
				$data['renci'] = $records['gonumber'];		//总参与人次
				$data['list'] = $list;
				$this->info = array('status'=>1, 'message'=>'请求成功', 'data'=>$data);
			}else{
				$this->info['message'] = '暂无数据';
			}
	   }else{
		   $this->info['message'] = $goods_info;
	   }
	   $this->callBack();
   }
   
   /*
    * 9、[查看TA的号码] 云购号码
   */
   public function show_duobao_number()
   {
	   $indata = $this->getData();			//获取数据
	   $this->checkSign($indata);			//验证签名
	   $id = isset($indata['id']) ? intval($indata['id']) : 0;	//shoplist里面的ID
	   $rid = isset($indata['rid']) ? intval($indata['rid']) : 0;	//member_go_records里面的ID
	   $qishu = isset($indata['qishu']) ? intval($indata['qishu']) : 0;	//期号
	   $uid = isset($indata['uid']) ? trim($indata['uid']) : 0;

	   if( ! $id &&  ! $rid ){
		   $this->info['message'] = '请求失败';
		   $this->callBack();
	   }
	   
	   if($id){	//获取某个用户的所有参与次数
			if( ! $qishu ||  ! $uid ){
				$this->info['message'] = '请求失败';
				$this->callBack();
			}
			$goods_info = $this->get_goods_fields($id,$qishu,'id,title');
			 if(isset($goods_info['id'])){
				 $records = $this->get_member_go_record($id , $uid, $qishu);
				 $infos['shopname'] = str_replace('&nbsp;', ' ', $goods_info['title']);
				 $infos['qishu'] = $qishu;
				 $infos['gonumber'] = $records['gonumber'];
				 $infos['goucode'] = $records['goucode'];
				 $this->info = array('status'=>1, 'message'=>'请求成功', 'data'=>$infos);
			 }else{
				 $this->info['message'] = $goods_info;
			 }
	   }else{	//按每一次参与记录查看
			$infos = $this->db->GetOne("SELECT shopname,shopqishu as qishu,gonumber,goucode FROM @#_member_go_record WHERE `id` = $rid LIMIT 1");
			if( ! isset($infos['shopname'])){
				$this->info['message'] = '暂无数据';
				$this->callBack();
			}
			$infos['shopname'] = str_replace('&nbsp;', ' ', $infos['shopname']);
			$this->info = array('status'=>1, 'message'=>'请求成功', 'data'=>$infos);
	   }
	   
	   $this->callBack();
   }
   
   
   /*
   * 10.获取商品的获奖者信息(倒计时结束时调用)
   * 2015.12.1
   */
   public function get_win_member_info()
   {
	   $indata = $this->getData();			//获取数据
	   $this->checkSign($indata);			//验证签名
	   $id = isset($indata['id']) ? intval($indata['id']) : 0;	//shoplist里面的ID
	   
	   $sql = 'SELECT qishu,q_uid,q_user,q_end_time,q_user_code,q_showtime FROM @#_shoplist WHERE `id` = ' . $id . ' LIMIT 1';
	   $member = $this->db->GetOne($sql);
	   if(isset($member['q_showtime'])){
		   if($member['q_showtime']=='N' && $member['q_uid']){
			   $member['q_user'] = unserialize($member['q_user']);
			   $data['uid'] = $member['q_uid'];
			   $data['q_user_code'] = $member['q_user_code'];
			   $data['username'] = get_user_name($member['q_uid']);  //用户名称
			   $data['img'] = $this->touimg_replace($member['q_user']['img']);
			   $records = $this->get_member_go_record($id, $member['q_uid'] , $member['qishu']);
			   $data['renci'] = $records['gonumber'];
			   $data['qishu'] = $member['qishu'];
			   $ips = $this->db->GetOne("SELECT user_ip FROM @#_member WHERE `uid` = " . $member['q_uid']);
			   $data['address'] = '';
			   if(isset($ips['user_ip'])){
					$ip = explode(',',$ips['user_ip']);
				   	$addr = GetIpLookup($ip[1]);
					$data['address'] = $addr['province'].$addr['city'];
			   }
			   $data['q_end_time'] = microt($member['q_end_time']);
			   $this->info = array('status'=>1, 'message'=>'请求成功', 'data'=>$data);
			   unset($member);
		   }else{
			   $this->info['message'] = '该商品还未揭晓';
		   }
	   }else{
		   $this->info['message'] = '商品不存在';
	   }
	   $this->callBack();
   }
   
   /*
   * 11.搜索商品功能
   * 2015.12.2
   * param >> keywords,size,page
   */
   public function do_search()
   {
		$indata = $this->getData();			//获取数据
		$this->checkSign($indata);			//验证签名
		$keywords = isset($indata['keywords']) ? trim($indata['keywords']) : '';	//关键词
		$size = isset($indata['size']) ? intval($indata['size']) : 10;	//分页的大小
		$page = isset($indata['page']) ? intval($indata['page']) : 1;	//第几页
		$uid  = isset($indata['uid'])  ? intval($indata['uid'])  : 0;
		if( ! $keywords){
		   $this->info['message'] = '关键词不能为空';
		   $this->callBack();
		}
		
		$wh = " ( `title` LIKE '%" . $keywords . "%' OR `keywords` LIKE '%" . $keywords . "%' ) AND `shenyurenshu` > 0 ";
		/* 2016/01/04 苹果APP上架期间不允许显示苹果相关产品  审核通过后要去掉该代码 start */
		/*$is_ios = $this->is_ios($indata);
		$version = isset($indata['version']) ? trim($indata['version']) : '';    //获取用户的版本号,最新版的在审核阶段是不允许显示的
		if( $is_ios && $this->compare_version($version) ){
		    $wh .= ' AND is_apple = 0 ';
		}*/
		/* 2016/01/04  苹果APP上架期间不允许显示苹果相关产品  审核通过后要去掉该代码 end */
		$filed	= ' id,sid,thumb,qishu,title,zongrenshu,canyurenshu,yunjiage,default_renci,is_shi as is_ten ';
		$goods_list =  $this->get_shoplist($wh,$filed,$page, $size);
		
		if($goods_list == 1){
			$this->info['message'] = '暂无数据';
			$this->callBack();
		}
		if($goods_list == 2){
			$this->info['message'] = '后面没有数据了';
			$this->callBack();
		}
		
	   	/*$total = $this->db->GetCount("SELECT `id` FROM @#_shoplist WHERE  $wh ");	//统计总数
		if( ! $total ){
			$this->info['message'] = '暂无数据';
			$this->callBack();
		}
		$page_count = intval($total / $size);	//总页数取整
		$max_page = $total % $size == 0 ? $page_count : $page_count + 1;	//最大页数
		if($page > $max_page){
			$this->info['message'] = '后面没有数据了';
			$this->callBack();
		}
		
		$start = $page <= 1 ? 0 : ($page-1) * $size;	//从第几条数据开始取
		$field = ' id,sid,thumb,qishu,title,zongrenshu,canyurenshu,yunjiage,default_renci,is_shi as is_ten ';
		
		$limit = $is_ios ? "" : " LIMIT $start,$size ";   //如果是iOS 则返回所有数据
		
		$sql = "SELECT $field  FROM @#_shoplist WHERE $wh " . $limit;
		$goods_list = $this->db->GetList($sql);*/
		if(count($goods_list)){
			//数据整理
			$goods_list = $this->datalist($goods_list,$uid);
			$data['total'] = $this->total;
			$data['max_page'] = $this->max_page;
			$data['list'] = $goods_list;
			$this->info = array('status'=>1, 'message'=>'请求成功', 'data'=>$data);
		}else{
			$this->info['message'] = '暂无数据';
		}
		$this->callBack();
   }
   
   //查看   更多云购号码 
   public function get_more_number()
   {
	   $indata = $this->getData();			//获取数据
	   $this->checkSign($indata);			//验证签名
	   $id = isset($indata['id']) ? intval($indata['id']) : 0;	//shoplist里面的ID
	   $uid = isset($indata['uid']) ? intval($indata['uid']) : 0;	//uid
	   $qishu = isset($indata['qishu']) ? intval($indata['qishu']) : 0;	//期数
	   
	   if(!$id || !$uid || !$qishu){
		   $this->info['message'] = '请求失败';
		   $this->callBack();
	   }
	   
	   $data = $this->get_member_go_record($id,$uid,$qishu);
	   if(!isset($data['gonumber'])){
		   $this->info = array('status'=>0, 'message'=>'没有记录');
		   $this->callBack();
	   }
	   $this->info = array('status'=>1, 'message'=>'请求成功', 'data'=>$data);
	   $this->callBack();
   	}

    public function get_gou_list(){
        $indata = $this->getData();			//获取数据
        $this->checkSign($indata);			//验证签名
        $id = isset($indata['id']) ? intval($indata['id']) : 0;	//shoplist里面的ID
        $uid = isset($indata['uid']) ? intval($indata['uid']) : 0;	//uid
        if(!$id || !$uid){
            $this->info['message'] = '请求失败';
            $this->callBack();
        }

        //查询购买记录
        $MemberRecordModel = System::load_app_model('MemberRecord','Zapi');
        $record = $MemberRecordModel->get_shop_list($id,$uid);

        if($record){
            $this->info = array('status'=>0, 'message'=>'没有记录');
        }else{
            $this->info = array('status'=>1, 'message'=>'请求成功', 'data'=>$data);
        }
        $this->callBack();
    }

   
   	//获取   计算详情的内容  
   	public function get_calc_details(){
	   $indata = $this->getData();			//获取数据
	   $this->checkSign($indata);			//验证签名
	   $id = isset($indata['id']) ? intval($indata['id']) : 0;	//shoplist里面的ID
	   
	   if(!$id){
		   $this->info['message'] = '请求失败';
		   $this->callBack();
	   }
	   
	   $item = $this->db->GetOne("SELECT id,q_uid,q_content,q_counttime,q_ssccode,q_user_code,q_sscphase FROM @#_shoplist WHERE `id`='$id' AND `shenyurenshu` = 0 LIMIT 1");
	   if(!isset($item['id'])){
		   $this->info['message'] = '请求失败';
		   $this->callBack();
	   }
	   
	   //揭晓之前不显示数值A和100条记录
	   $data['data_a'] = $item['q_uid'] ? $item['q_counttime'] : '计算中...';	//数值A
	   $data['data_b'] = $item['q_ssccode'] ? : '正在等待开奖...';		//数值B
	   $data['ssc_qishu'] = $item['q_sscphase'] ? : ' ? ';		//时时彩期号
	   $data['data_result'] = $item['q_user_code'] ? : '等待揭晓...';		//计算结果
	   $data['content'] = array();
	   if($item['q_uid'] && $item['q_content']){
			$item_q_content = unserialize($item['q_content']);
			$keysvalue = $new_array = array();
			foreach($item_q_content as $k=>$v){
				$keysvalue[$k] = $v['time'];				
				$h=date("H",$v['time']);
			    $i=date("i",$v['time']);
			    $s=date("s",$v['time']);	
			    list($timesss,$msss) = explode(".",$v['time']);
				$item_q_content[$k]['time_add'] = $h.$i.$s.$msss;	
				$item_q_content[$k]['time'] = date('Y-m-d H:i:s',$timesss) .'.'. $msss;			
				unset($item_q_content[$k]['shopid'],$item_q_content[$k]['shopname'],$item_q_content[$k]['shopqishu'],$item_q_content[$k]['gonumber']);
			}
			arsort($keysvalue);	//asort($keysvalue);正序
			reset($keysvalue);
			foreach ($keysvalue as $k=>$v){
				$new_array[$k] = $item_q_content[$k];
			}			
			$data['content'] = $new_array;
	   }
	   $this->info = array('status'=>1,'message'=>'请求成功','data'=>$data);
	   $this->callBack();
   	}
   
	/**
	 * 
	 * 
	 */
   	public function get_more_qishu($sid,$qishu){
   
		/*$indata = $this->getData();			//获取数据
	   	$this->checkSign($indata);			//验证签名
	*/
		
		$sid 	= intval($sid);				//当前产品的sid
		$qishu	= intval($qishu);				//当前产品的期数
		if($qishu == 1){
			$qishu_min = $qishu;
			$qishu_max = $qishu + 2;
		}else{
			$qishu_min 	= $qishu - 2;
			$qishu_max 	= $qishu + 1;
		}
		$sql 	= "select qishu,id from `@#_shoplist` where sid='$sid' and (qishu>='$qishu_min' and qishu<=$qishu_max) order by qishu desc";
		$result = $this->db->GetList($sql);
	
		$result = array_slice($result,0,3);
	
		return $result;
   	}
   
    public function get_qishu_id(){
    	$indata = $this->getData();			//获取数据
		$this->checkSign($indata);			//验证签名
		
		$qishu 	= $indata['qishu'];
		$sid	= $indata['sid'];
		
		$sql 	= "select id from `@#_shoplist` where sid='$sid' and qishu='$qishu'";
		$result = $this->db->GetOne($sql);
		$this->info = array('status'=>1,'message'=>'请求成功','data'=>$result);
		$this->callBack();

    }
   	
	/**
	 * 获取商品列表
	 *
	 * @param $where 查询条件 string
	 * @param $field 查询字段 string
	 * @param $size  取出条数 
	 * @return $list 查询出来的数据  array
	 */
	public function get_shoplist($where,$field='*',$page,$size){
		
		$total = $this->db->GetCount('SELECT `id` FROM @#_shoplist WHERE ' . $where); //统计数量
		if( ! $total ){
			return 1;die;			
		}
		$page_count = intval($total / $size);	//总页数取整
		$max_page = $total % $size == 0 ? $page_count : $page_count + 1;	//最大页数

		if($page > $max_page){
			return 2;die;	
		}

        if($this->is_ios()){
            $is_ios = 1;
        }

		$start = $page <= 1 ? 0 : ($page-1) * $size;	//从第几条数据开始取		
		$limit = $is_ios ? "" : " LIMIT $start,$size ";   //如果是iOS 则返回所有数据
		
		$sql = 'SELECT '.$field.' FROM @#_shoplist WHERE ' .$where.$limit;
		$list = $this->db->GetList($sql);		//default_renci 加入清单时默认的数量
		
		$this->max_page = $max_page;
		$this->total = $total;
		return $list;
	}
	
	
   	/**
	 * @param  $good_list 需要整理的商品数据
	 * @param  $uid       用户id
	 * @return $good_list 返回整理后的数据（用户可购买数量wap_ten 本期产品已购人次比例 ratio）
	 */
	public function datalist($goods_list,$uid=0){
		$xiangou_money = 5;											//限购次数
		foreach($goods_list as $k => $v){
			$syrs = $v['zongrenshu'] - $v['canyurenshu'];			//本期产品的剩余人数
			$goods_list[$k]['wap_ten'] = $syrs;						//用户可以购买的次数			
			$goods_list[$k]['is_ten']  = intval($v['is_ten']);		//是否是限购专区
			//查询当前用户能将产品加入购物车的数量
			if($uid > 0){
				//获取用户的购买数量	
				$user_bought_number = $this->get_member_go_record($v['id'],$uid,$goods_list[$k]['qishu']);
				$user_bought_number = $user_bought_number['gonumber'];
			}else{
				$user_bought_number = 0;
			}
			//限购
			if($v['is_ten'] == 1){									//是否是限购专区
				$xiangou_number = $xiangou_money - $user_bought_number;	
				if($xiangou_number > 0){
					//用户购买本期产品的剩余次数  与   本期产品剩余人次
					$goods_list[$k]['wap_ten'] = ($xiangou_number > $syrs) ? $syrs : $xiangou_number;
				}else{
					//用户购买的次数已超额
					$goods_list[$k]['wap_ten'] = 0;
				}	
			}
			$goods_list[$k]['title'] = str_replace('&nbsp;',' ',$v['title']);
			//判断该图片路径是否包含  图片上传目录的路径
			if(strpos($v['thumb'],G_UPLOAD_PATH) === false){
				$goods_list[$k]['thumb'] = G_UPLOAD_PATH . '/' . $v['thumb'];
			}
			//比例   参与人数占总人数的百分比
			$goods_list[$k]['ratio'] = ceil($goods_list[$k]['canyurenshu']/$goods_list[$k]['zongrenshu']   * 100 );
			$goods_list[$k]['yunjiage'] = intval($goods_list[$k]['yunjiage']);
		}
		
		return $goods_list;
	}
   
   
   
}