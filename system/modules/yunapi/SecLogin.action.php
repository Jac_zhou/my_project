<?php
header("Content-type:text/html;charset=UTF-8");
/**
 * 第三方登录
 * 
 */	
defined('G_IN_SYSTEM')or exit('no');
System::load_app_class('api');	
Class SecLogin extends api{
	public $db;
	private $indata;			//接收到的数据
	
	
	/*public function __get($name){
		return $this->$name;
	}*/
	public function __set($name,$value){
		$this->$name = $value;
	}
	public function __construct(){
		
		parent::__construct();
		
		$this->indata = $this->getData();		//获取数据

		//$this->checkSign($this->indata);//验证签名
		$this->info   = array('status'=>0,'message'=>'请求失败');
	}
	
	/**
	 * 其他平台登录对接接口
	 * @param $openid
	 * @param $type      微信、QQ
	 * @param $userinfo  用户信息
	 * @return  返回第三方用户的信息，是否绑定账号的状态    0 未绑定   1已绑定   
	 * 
	 * 				（未绑定则跳转到登录页面，已绑定则直接登录）
	 */
	public function index(){
		
		//获取openid
		$openid 	= trim($this->indata['openid']);		//微信的unionid、QQ的openid
		$type   	= trim($this->indata['type']);		//weixin / qq

		if(!$openid || !$type){
			$this->info['status'] = 0;
			$this->info['message']='检查用户信息是否传值正确';
			$this->callBack();
		}
		$MemberBandModel = System::load_app_model('MemberBand','yunapi');

		$field = "*";
		$where =array();
		$where['b_code'] = $openid;
		$where['b_type'] = $type;
		$data  = $MemberBandModel->get_one($where,$field);


		//查看当前第三方用户的资料是否入库
		if(!empty($data)){
			//判断当前第三方用户是否已绑定夺宝账户
			if($data['b_uid']>0){
				unset($where);
				$where['uid']=$data['b_uid'];
				$filed = 'uid,mobile,email,username,mobilecode,emailcode,money,img,yaoqing';
				
				$MemberInfo = $MemberBandModel->get_member_info($where,$filed);	
				$MemberInfo['img'] = G_UPLOAD_PATH.'/'.$MemberInfo['img'];
				$this->info = array('status'=>1,'message'=>'@#_success','data'=>$MemberInfo);
			}else{
				$this->info = array('status'=>0,'message'=>'@#_success数据库已有第三方登录信息，没有绑定财神账户，前去绑定！');
			}
		}else{
			//获取当前第三方用户的资料   并且入库             $b_data  获取到的额外数据，备用
			$data = array();

			if(!in_array($type,array('qq','weixin'))){
				$this->info['status'] = 0;
				$this->info['message']="type值不合法！";
				$this->callBack();
			}

			$data['b_code'] 	= $openid;
			$data['b_type']		= $type;
			$data['b_time'] 	= time();	

			$id = $MemberBandModel->insert_into($data);
			
			if($id>0){
				$this->info['message']="@#_success用户数据入库，请到登录绑定页面";
				$this->info['status'] = 0;
			}else{
				$this->info['status'] = 2;
				$this->info['message']="@#_err用户数据入库失败，插入语句出错";
			}
			
		}
		$this->callBack();
	}

	
	public function Sec_login_userinfo(){
		$openid = $this->indata['openid'];	
		$type 	= $this->indata['type'];	//登录类型
		//缓存的微信用户信息
		$userinfo = S('userinfo_'.trim($openid));
		$userinfo = unserialize($userinfo);
		if($userinfo){
			if($type=='qq'){
				$userinfo['headimgurl'] = $userinfo['figureurl_qq_1'];
			}else if($type=='wx'){
				$userinfo['headimgurl'] = $userinfo['headimgurl'];
			}else{
				$this->info['status'] = 0;
				$this->info['message']="type值不合法！";
				$this->callBack();
			}
			$this->info = array('status'=>1, 'message'=>'请求成功', 'data'=>$userinfo);
		}else{
			$this->info['message']='数据没有缓存';
		}
		$this->callBack();
	}
	


    /**   接口地址           yunapi/Seclogin/check_user
     * @param $mobile       需要验证的手机号码
     * @param $type         微信/QQ    ( 'wx' / 'qq'  ) 固定是两者之一
     *
     * @return   成功后返回的data数据    用户的uid
     *
     *          status  状态码   0,1,2               1,2   成功  1   用户可以通过密码进行登录并绑定
     *                                                          2   用户通过验证码进行注册并绑定
     *          message 提示信息（报错信息）
     *
     *          data    成功时返回的数据  uid
     */
    public function check_user(){
        $indata 	= $this->indata;
        $mobile     = trim($indata['mobile']);
        $type       = trim($indata['type']);

        if(!_checkmobile($mobile)){
            $this->info['message']  =   '手机号码不正确！';
            $this->info['status']   =   0;
            $this->callback();
        }
        $sql = "select uid,wx_id,qq_id from `@#_member` where mobile='{$mobile}'";
        $member = $this->db->GetOne($sql);

        if($type=='wx'){
            if($member['wx_id']>0){
                $this->info['message']='被账户被其他微信号绑定，您可以先登录官网用户中心进行解绑！！';
                $this->callBack();
            }
        }else if($type=='qq'){
            if($member['qq_id']>0){
                $this->info['message']='被账户被其他QQ号绑定，您可以先登录官网用户中心进行解绑！！';
                $this->callBack();
            }
        }else{
            $this->info['message']='传入的type值不对';
            $this->callBack();
        }
        if(isset($member['uid']) && $member['uid'] > 0){
            $this->info['message']  =   '手机号已注册！';
            $this->info['status']   =   1;
            $this->info['data']     =   array('uid'=>$member['uid']);
        }else{
            $this->info['message']  =   '手机号码未注册！';
            $this->info['status']   =   2;
        }
        $this->callback();


    }

    /**       接口地址           yunapi/Seclogin/check_password
     * @param $openid           第三方的唯一标识码
     * @param $uid              check_user方法返回的 uid（验证用户账户后返回的UID）
     * @param $password         用户密码
     * @param $type             微信/QQ    ( 'wx' / 'qq'  ) 固定是两者之一
     * @return
     *          status  状态码   0,1
     *          message 提示信息（报错信息）
     *          data 成功时返回的数据   (APP:) uid,mobile,password,email,username,mobilecode,emailcode,money,img,yaoqing,wx_id
     */
    public function check_password(){
        $indata 	= $this->indata;
        $openid 	= safe_replace($indata['openid']);
        $uid        = intval(trim($indata['uid']));
        if($uid>0){
            if($this->is_android($indata) || $this->is_ios($indata)){
                $password  	= md5($this->decode_password(trim($indata['password'])));		//密码
            }else{
                $password   = md5(htmlspecialchars(trim($indata['password'])));
            }

            $type = isset($indata['type'])?$indata['type']:'';
            //手机账号登录
            $member=$this->db->GetOne("select password from `@#_member` where `uid`='$uid'");
            if(!$member){
                $this->info['message']='该手机号没有注册！';
                $this->callBack();
            }
            if($type=='wx'){
                $type = 'wx_id';
            }else if($type=='qq'){
                $type = 'qq_id';
            }else{
                $this->info['message']='传入的type值不对';
                $this->callBack();
            }

            $sql 	= "select b_id from `@#_member_band` where b_code='$openid'";
            $result = $this->db->GetOne($sql);
            $band_id = $result['b_id'];

            $this->db->Autocommit_start();
            //用户的id
            $sql = "update `@#_member_band` set `b_uid`='$uid' where b_code='$openid'";
            $q1  = $this->db->Query($sql);
            $sql = "update `@#_member` set `$type`='$band_id' where uid='$uid'";
            $q2  = $this->db->Query($sql);
            if($q2 && $q1){
                $this->db->Autocommit_commit();
                if($this->is_android($this->indata) || $this->is_ios($this->indata)){
                    $this->app_login($uid);		//APP登录
                }else{
                    $this->go_login($uid);		//wap登录
                }
            }else{
                $this->db->Autocommit_rollback();
                $this->info = array('status'=>0,'message'=>'未知错误！');
            }
            $this->callBack();
        }else{
            $this->info['message']='uid参数错误';
            $this->callBack();
        }

    }

    /**
     * 验证用户发送的验证码
     */
    public function check_mobile_code(){

        $mobile     = trim($this->indata['mobile']);
        $code       = trim($this->indata['code']);

        $MemberCon  = System::load_contorller('member','yunapi');
        //验证注册码是否正确
        $status     = $MemberCon->check_code_is_pass($mobile,$code,'is_reg');

        if($status){
            $this->info['status']   = 1;
            $this->info['message']  = 'check_successed';
            $this->info['data']     = array('mobile'=>_encrypt($mobile,'ENCODE'));
        }else{
            $this->info['status'] = 0;
            $this->info['message']= 'check_error';
        }
        $this->callback();
    }


    /**
     *  用户注册登录
     */
    public function sec_do_register(){

        $mobile     = _encrypt(trim($this->indata['mobile']),'DECODE');         //加密后的手机号码
        $password   = trim($this->indata['password']);                          //用户自己填写的手机号码
        $code       = trim($this->indata['code']);
        $type       = trim($this->indata['type']);
        $openid     = htmlspecialchars(trim($this->indata['openid']));
        $MemberCon  = System::load_contorller('member','yunapi');
        if($this->is_android($this->indata) || $this->is_ios($this->indata)){
            $password  	= md5($this->decode_password($password));		//密码
        }else{
            $password   = md5(htmlspecialchars($password));
        }

        if(!_checkmobile($mobile)){
            $this->info['status'] = 0;
            $this->info['message']= '非法访问!';
            $this->callback();
        }
        $status     = $MemberCon->check_mobile_is_register($mobile);
        if(!$status){
            $this->info['status'] = 0;
            $this->info['message']= 'code_err 该手机已被注册!';
            $this->callback();
        }

        //确认该手机是否填写过验证码
        $is_reg = $this->db->GetCount("SELECT `id` FROM @#_mobile_code WHERE `mobile` = '$mobile' AND `is_reg` = 1");
        if( ! $is_reg){
            $this->info['message'] = '数据来源不合法,请稍后再试';
            $this->callBack();
        }


        if($type=='wx'){
            $type = 'wx_id';
            $band_type = 'weixin';
        }else if($type=='qq'){
            $type = 'qq_id';
            $band_type = 'qq';
        }else{
            $this->info['message']='传入的type值不对';
            $this->callBack();
        }


        $MemberBandModel = System::load_app_model('MemberBand','yunapi');
        $field = 'b_id';
        $where =array();
        $where['b_code'] = $openid;
        $where['b_type'] = $band_type;
        $data  = $MemberBandModel->get_one($where,$field);
        $band_id = $data['b_id'];
        $this->db->Autocommit_start();
        $uid = $this->insert_member($mobile,$password,$code,$type,$band_id);

        if($uid>0){
            $sql = "update @#_member_band set b_uid = $uid where b_id='$band_id'";
            $r1 = $MemberBandModel->Query($sql);

            if($r1){
                //登录
                $this->db->Autocommit_commit();
                if($this->is_android($this->indata) || $this->is_ios($this->indata)){

                    $this->app_login($uid);		//APP登录
                }else{

                    $this->go_login($uid);		//wap登录
                }

            }else{
                $this->db->Autocommit_rollback();
                $this->info['status'] = 0;
                $this->info['message'] = '注册失败！';
            }
        }

        $this->callback();
    }


    /**
     * @param $mobile                       注册账号
     * @param string $password              注册密码
     * @param $code                         注册验证码
     * @param $type                         第三方注册类型
     * @param $band_id                      第三方用户id
     * @return mixed                        成功注册后返回用户id
     */
    private function insert_member($mobile,$password='123456',$code,$type,$band_id){
        $reg_plat   = $this->get_platform($this->indata['partner']);
        $user_ip    = _get_ip_dizhi();		//获取地址和ip 格式如：  广东深圳,192.168.1.50
        $img        = 'photo/member.jpg';	//头像
        $username   = '会员'.mt_rand(10000,99999);
        $time       = $_SERVER['REQUEST_TIME'];
        $sql = "INSERT INTO @#_member
                                    (username,mobile,password,mobilecode,user_ip,time,reg_key,login_time,img,reg_plat,passcode,$type)
                                VALUES
                                    ('$username','$mobile','$password','1','$user_ip','$time','$mobile',$time,'$img','$reg_plat','$code','$band_id')";

        $this->db->Query($sql);
        $uid = $this->db->insert_id();

        return $uid;
    }












    /**
     *
     *
     *
     */
    public function check_code(){
        //验证二维码
        $indata 	= $this->indata;
        $mobile     = trim($indata['mobile']);
        $password   = trim($indata['password']);
        $code       = trim($indata['code']);

        $reg_plat   = $this->get_platform($indata['partner']);
        $type       = trim($indata['type']);
        $openid     = htmlspecialchars(trim($indata['openid']));
        if($type=='wx'){
            $type = 'wx_id';
            $band_type = 'weixin';
        }else if($type=='qq'){
            $type = 'qq_id';
            $band_type = 'qq';
        }else{
            $this->info['message']='传入的type值不对';
            $this->callBack();
        }
        $MemberBandModel = System::load_app_model('MemberBand','yunapi');

        $field = "*";
        $where =array();
        $where['b_code'] = $openid;
        $where['b_type'] = $band_type;
        $data  = $MemberBandModel->get_one($where,$field);
        $band_id = $data['b_id'];
        $MemberCon  = System::load_contorller('member','yunapi');

        $status     = $MemberCon->check_mobile_is_register($mobile);
        if(!$status){
            $this->info['status'] = 0;
            $this->info['message']= 'code_err 该手机已被注册!';
            $this->callback();
        }

        $status = $MemberCon->check_code_is_pass($mobile,$code,'is_reg');//默认是验证注册码

        if($status){
            //确认该手机是否填写过验证码
            $is_reg = $this->db->GetCount("SELECT `id` FROM @#_mobile_code WHERE `mobile` = '$mobile' AND `is_reg` = 1");
            if( ! $is_reg){
                $this->info['message'] = '数据来源不合法,请稍后再试';
                $this->callBack();
            }

            $user_ip    = _get_ip_dizhi();		//获取地址和ip 格式如：  广东深圳,192.168.1.50
            $img        = 'photo/member.jpg';	//头像
            $username   = '财神'.mt_rand(10000,99999);
            $time       = $_SERVER['REQUEST_TIME'];
            $password   = md5($password);
            $this->db->Autocommit_start();

            $sql = "INSERT INTO @#_member
                                    (username,mobile,password,mobilecode,user_ip,time,reg_key,login_time,img,reg_plat,passcode,$type)
                                VALUES
                                    ('$username','$mobile','$password','1','$user_ip','$time','$mobile',$time,'$img','$reg_plat','$code','$band_id')";
            $this->db->Query($sql);
            $uid = $this->db->insert_id();
            if( ! $uid){	//注册失败
                $this->db->Autocommit_rollback();
                $this->info['message'] = '注册失败,请稍后再试';
            }else{

                $sql = "update @#_member_band set b_uid = '$uid' where b_id='$band_id'";
                $r1 = $this->db->Query($sql);

                if($r1){

                    //登录
                    if($this->is_android($this->indata) || $this->is_ios($this->indata)){
                        $this->app_login($uid);		//APP登录
                    }else{

                        $this->go_login($uid);		//wap登录
                    }
                    $this->db->Autocommit_commit();
                }else{
                    $this->db->Autocommit_rollback();
                    $this->info['status'] = 0;
                    $this->info['message']= 'code_err 验证码错误!';
                }
            }
        }else{
            $this->info['status'] = 0;
            $this->info['message']= 'code_err 验证码错误!';
        }
        $this->callback();
    }

    /**
     * APP端  第三方用户绑定账号之后  APP所需要的用户数据
     */
    public function app_login($uid){
        if($uid>0){
            $filed = 'uid,mobile,password,email,username,mobilecode,emailcode,money,img,yaoqing';

            $sql = "select $filed from `@#_member` where uid='$uid' ";

            $member = $this->db->GetOne($sql);
            $member['img'] = G_UPLOAD_PATH.'/'.$member['img'];
            if($this->is_android($this->indata)){
                unset($member['password']);
            }
            $this->info = array('status'=>1,'message'=>'绑定成功','data'=>$member);
        }else{
            $this->info = array('status'=>0,'message'=>'绑定失败bind_002');
        }

    }

    /**
     * 第三方用户绑定账号之后，执行登录操作   b_uid
     * 	WEB  WAP
     */
    public function go_login($uid){

        if($uid>0){
            $time = $_SERVER['REQUEST_TIME'];
            $filed   = 'uid,mobile,password,email,username,mobilecode,emailcode,money,img,yaoqing';
            $sql = "select $filed from `@#_member` where uid='$uid' ";
            $member  = $this->db->GetOne($sql);

            $user_ip = _get_ip_dizhi();
            $_COOKIE['uid'] 	= null;
            $_COOKIE['ushell'] 	= null;
            $_COOKIE['UID'] 	= null;
            $_COOKIE['USHELL'] 	= null;
            $_COOKIE['holdUID'] = null;


            $this->db->GetOne("UPDATE `@#_member` SET `user_ip` = '$user_ip',`login_time` = '$time' where `uid` = '$uid'");
            $s0 = _setcookie("holdUID",$member['uid'],60*60*24*7);
            $s2 = _setcookie("ushell",_encrypt(md5($member['uid'].$member['password'].$member['mobile'].$member['email'])),60*60*24*7);
            if($s0 && $s2){
                $this->info['message']=$member;
                $this->info['status']=1;
            }else{
                $this->info['message']='未知错误！';
                $this->info['status']=0;
            }
        }

    }

    /**
     * 绑定操作：  根据用户正确输入用户名和密码，然后将第三方账号对该用户进行绑定
     *
     * @param $openid
     * @param $username
     * @param $password
     * @param $type
     * @return 返回绑定状态，   绑定成功的话，将直接存储用户数据，然后在来源端 进行登录
     */
    public function do_bind(){

        $indata 	= $this->indata;
        $openid 	= safe_replace($indata['openid']);		//
        if(!$openid){
            $this->info['message']='缺少参数!';
            $this->callBack();
        }
        $username  	= trim($indata['username']);			//手机账号
        if($this->is_android($indata) || $this->is_ios($indata)){
            $password  	= md5($this->decode_password(trim($indata['password'])));		//密码
        }else{
            $password   = md5(htmlspecialchars(trim($indata['password'])));
        }

        $type		= isset($indata['type'])? $indata['type']:'wx';
        $this->info['status']=0;
        if(!_checkmobile($username)){
            $this->info['message']='用户账号格式不正确';
            $this->callBack();
        }
        //手机账号登录
        $member=$this->db->GetOne("select * from `@#_member` where `mobile`='$username'");
        if(!$member){
            $this->info['message']='该手机号没有注册！';
            $this->callBack();
        }
        $uid= $member['uid'];

        if($type=='wx'){
            $type = 'wx_id';
            if($member['wx_id']>0){
                $this->info['message']='被账户被其他微信号绑定，您可以登录用户中心进行解绑！！';
                $this->callBack();
            }
        }else if($type=='qq'){
            $type = 'qq_id';
            if($member['qq_id']>0){
                $this->info['message']='被账户被其他QQ号绑定，您可以登录用户中心进行解绑！！';
                $this->callBack();
            }
        }else{
            $this->info['message']='传入的type值不对';
            $this->callBack();
        }

        if($password!= $member['password']){
            $this->info['message']='密码错误';
            $this->callBack();
        }

        $sql 	= "select b_id from `@#_member_band` where b_code='$openid'";
        $result = $this->db->GetOne($sql);
        $band_id = $result['b_id'];

        $this->db->Autocommit_start();
        //用户的id
        $sql = "update `@#_member_band` set `b_uid`='$uid' where b_code='$openid'";
        $q1  = $this->db->Query($sql);
        $sql = "update `@#_member` set `$type`='$band_id' where uid='$uid'";
        $q2  = $this->db->Query($sql);
        if($q2 && $q1){
            $this->db->Autocommit_commit();
            if($this->is_android($this->indata) || $this->is_ios($this->indata)){
                $this->app_login($uid);		//APP登录
            }else{
                $this->go_login($uid);		//wap登录
            }
        }else{
            $this->db->Autocommit_rollback();
            $this->info = array('status'=>0,'message'=>'未知错误！');
        }
        $this->callBack();
    }






}	
	
	
	
	
	