<?php defined('G_IN_ADMIN')or exit('No permission resources.'); ?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<link rel="stylesheet" href="<?php echo G_GLOBAL_STYLE; ?>/global/css/global.css" type="text/css">
<link rel="stylesheet" href="<?php echo G_GLOBAL_STYLE; ?>/global/css/style.css" type="text/css">
<link rel="stylesheet" href="<?php echo G_PLUGIN_PATH; ?>/calendar_2/jquery.datetimepicker.css" type="text/css"> 
<script type="text/javascript" charset="utf-8" src="<?php echo G_PLUGIN_PATH; ?>/calendar_2/jquery.js"></script>
<script type="text/javascript" charset="utf-8" src="<?php echo G_PLUGIN_PATH; ?>/calendar_2/build/jquery.datetimepicker.full.js"></script>

<style>
tbody tr{ line-height:30px; height:30px;} 
</style>
</head>
<body>
<div class="header lr10">
	<?php echo $this->headerment();?>
</div>
<div class="bk10"></div>


<div class="table-list lr10">
<!--start-->
  <table width="100%" cellspacing="0">
    <thead>
		<tr>
            <th width="100px" align="center">编号</th>
            <th width="100px" align="center">充值金额</th>
            <th width="100px" align="center">奖励百分比</th>
			<th width="100px" align="center">操作</th>
           
		</tr>
    </thead>
    <tbody>
    	<?php foreach($member_percentage as $key =>$brand){ ?>
		<tr>
			
			<td align="center"><?php echo $brand['id']; ?></td>	
			<td align="center"><?php echo $brand['charge']; ?></td>	
			<td align="center"><?php echo $brand['percentage']; ?>%</td>	
			<td align="center">[<a href="<?php echo G_MODULE_PATH; ?>/member/edit/<?php echo $brand['id']; ?>">修改</a>]	</td>	
		</tr>
       <?php }  ?>
	
  	</tbody>
	
</table>
<style>
.Page_This{color:red}
</style>
</div><!--<div id="pages"><ul><li>共 <?php echo $total; ?> 条</li><?php echo $page->show('two','li'); ?></ul></div>table-list end-->

<script>
$.datetimepicker.setLocale('zh');
var logic = function( currentDateTime ){
	if (currentDateTime && currentDateTime.getDay() == 6){
		this.setOptions({
			minTime:'11:00'
		});
	}else
		this.setOptions({
			minTime:'8:00'
		});
};
$('.posttime').datetimepicker({
	onChangeDateTime:logic,
	onShow:logic
});

$("#reset_button").click(function(){
	$(".posttime").val('');
	
});
</script>
</body>
</html> 