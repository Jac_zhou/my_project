<?php defined('G_IN_ADMIN')or exit('No permission resources.'); ?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<link rel="stylesheet" href="<?php echo G_GLOBAL_STYLE; ?>/global/css/global.css" type="text/css">
<link rel="stylesheet" href="<?php echo G_GLOBAL_STYLE; ?>/global/css/style.css" type="text/css">
<link rel="stylesheet" href="<?php echo G_PLUGIN_PATH; ?>/calendar/calendar-blue.css" type="text/css"> 
<script type="text/javascript" charset="utf-8" src="<?php echo G_PLUGIN_PATH; ?>/calendar/calendar.js"></script>
<script type="text/javascript" src="<?php echo G_GLOBAL_STYLE; ?>/global/js/jquery-1.8.3.min.js"></script>
<style>
tbody tr{ line-height:30px; height:30px;} 
.header-data li{ line-height:40px;float:left;}
.soso_message{ text-align:center; height:80px; line-height:80px;  border-top:5px solid #FFBE7A;border-bottom:5px solid #FFBE7A;}

.success{color:green;}
.fail{color:red;}

#content{height:160px;border:1px solid #ffbe7a;padding:5px 10px 10px;overflow:hidden;}
#siteResult{float:left;width:300px;height:160px;border-right:1px solid #ccc;}
#wxResult{float:left;width:300px;height:160px;border-right:1px solid #ccc;padding-left:10px;}
.ctit{height:35px;line-height: 35px;}

</style>
</head>
<body>
<div class="bk10"></div>
<div class="header-data lr10">
	<div style="display:inline-block; ">
    <li>站内充值单号：<input type="text" id="out_trade_no" name="out_trade_no" class="input-text wid300" /> <input id="doSearch" type="submit" name="usersubmit" class="button" value=" 查 询 " />
    </li>
    </div>
</div>
<div class="bk10"></div>
<div class="bk10"></div>
<div class="table-list lr10">

<div id="content">
    <div id="siteResult">
        <h3 class="ctit">站内返回结果</h3>
    </div>
    <div id="wxResult">
        <h3 class="ctit">微信返回结果</h3>
    </div>
</div>


<div class="btn_paixu"></div>
<div id="pages"></div>
</div><!--table-list end-->

<script>
var wx_url = "<?php echo WEB_PATH . "/member/member/do_show_weixin_info"?>";
var site_url = "<?php echo WEB_PATH . "/member/member/do_show_addmoney_info"?>";
var state = true;

$("#doSearch").click(function(){
	var out_trade_no = $("#out_trade_no").val();

	if( !out_trade_no){
		alert("请填写充值单号！");
		return false;
	}

	var _html = "<h3 class='ctit'>微信返回结果</h3>";
	var _shtml = "<h3 class='ctit'>站内返回结果</h3>";
	
	//发送请求之前，先置空
	$("#siteResult").html(_shtml + "<p>查询中...</p>");
	$("#wxResult").html(_html + "<p>查询中...</p>");
	
	var obj = {};
	var sobj = {};
	$.ajaxSetup({async:false});

	//站内订单查询
	$.get(site_url,{out_trade_no:out_trade_no},function(data){
		if(data.status == '1'){
			var _datas = data.data;
			var _status = _datas.status == "未付款" ? "<span class='fail'>未付款</span>" : "<span class='success'>已付款</span>";
			_shtml += "<p>会员ID&nbsp;&nbsp;&nbsp;："+ _datas.uid +"</p>";
			_shtml += "<p>手机号码："+ _datas.mobile +"</p>";
			_shtml += "<p>充值金额："+ _datas.money +"</p>";
			_shtml += "<p>充值状态："+ _status +"</p>";
			_shtml += "<p>充值单号："+ _datas.code +"</p>";
			_shtml += "<p>充值日期："+ _datas.time +"</p>";
			state = true;
		}else{
			_shtml += "<p>请求失败</p>";
			state = false;
		}
		//$("#siteResult").html(_shtml);
	},'json');

	if( ! state){
		return false;
	}
	
	//微信支付平台查询结果
	$.get(wx_url,{out_trade_no:out_trade_no},function(data){
		//alert(data);return false;
		if(data.status == '1'){
			var _data = data.data;
			obj.return_code = _data.return_code;
			obj.result_code = _data.result_code;
			//通信成功 并且 返回业务结果成功
			if(obj.return_code == "SUCCESS" && obj.result_code == "SUCCESS"){
				obj.money = typeof(_data.total_fee)=='undefined'?'':(_data.total_fee)/100;	//交易金额
				obj.transaction_id = typeof(_data.transaction_id)=='undefined'?'':_data.transaction_id;	//微信单号
				obj.out_trade_no = _data.out_trade_no;	//充值单号
				var trade_state = _data.trade_state;
				obj.trade_state = get_state(trade_state);	//交易状态
				obj.time_end  = typeof(_data.time_end)=='undefined'?'':_data.time_end;	//交易时间
				obj.trade_type = get_pay_type(_data.trade_type);	//交易类型
				
				_html += "<p>交易类型："+ obj.trade_type +"</p>";
				_html += "<p>微信支付单号："+ obj.transaction_id +"</p>";
				_html += "<p>交易金额："+ obj.money +"</p>";
				_html += "<p>交易状态："+ obj.trade_state +"</p>";
				_html += "<p>商户订单号："+ obj.out_trade_no +"</p>";
				_html += "<p>交易时间："+ obj.time_end +"</p>";
			}else{
				_html += "<p>请求失败</p>";
			}

			$("#siteResult").html(_shtml);
			$("#wxResult").html(_html);
			
		}else{
			alert(data.message);
		}
	},'json');
});


//返回支付状态
function get_state(state)
{
	$info = "";
	switch(state){
		case "SUCCESS" : $info = "<span class='success'>支付成功</span>"; break;
		case "REFUND" : $info = "转入退款"; break;
		case "NOTPAY" : $info = "<span class='fail'>未支付</span>"; break;
		case "CLOSED" : $info = "已关闭"; break;
		case "REVOKED" : $info = "已撤销"; break;
		case "USERPAYING" : $info = "用户支付中"; break;
		case "PAYERROR" : $info = "支付失败"; break;
	}
	return $info;
}
//返回交易类型
function get_pay_type(type)
{
	$info = "";
	switch(type){
		case "JSAPI" : $info = "公众号支付"; break;
		case "NATIVE" : $info = "扫码支付"; break;
		case "APP" : $info = "APP支付"; break;
		case "MICROPAY" : $info = "刷卡支付"; break;
	}
	return $info;
}
</script>
</body>
</html> 