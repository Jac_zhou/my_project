<?php defined('G_IN_ADMIN')or exit('No permission resources.'); ?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<link rel="stylesheet" href="<?php echo G_GLOBAL_STYLE; ?>/global/css/global.css" type="text/css">
<link rel="stylesheet" href="<?php echo G_GLOBAL_STYLE; ?>/global/css/style.css" type="text/css">
<link rel="stylesheet" href="<?php echo G_PLUGIN_PATH; ?>/calendar_2/jquery.datetimepicker.css" type="text/css"> 
<script type="text/javascript" charset="utf-8" src="<?php echo G_PLUGIN_PATH; ?>/calendar_2/jquery.js"></script>
<script type="text/javascript" charset="utf-8" src="<?php echo G_PLUGIN_PATH; ?>/calendar_2/build/jquery.datetimepicker.full.js"></script>

<style>
tbody tr{ line-height:30px; height:30px;} 
</style>
</head>
<body>
<div class="header lr10">
	<?php echo $this->headerment();?>
</div>
<div class="bk10"></div>


<div class="header-data lr10">
<div style="margin-bottom:5px;">搜索的总消费金额：<span style="color:red;font-weight:bold;"><?php echo $summoeny['sum_money'];?>元</span></div>
<form action="" method="get">
 时间搜索: <input name="posttime1" type="text" id="posttime1" class="input-text posttime"  readonly="readonly" value="<?php echo !empty($posttime1)?date("Y-m-d H:i:s",$posttime1):''?>"/> -  
 		  <input name="posttime2" type="text" id="posttime2" class="input-text posttime"  readonly="readonly" value="<?php echo !empty($posttime2)?date("Y-m-d H:i:s",$posttime2):''?>"/>

			<select name="yonghu" id="yonghu">
			<option value="0" <?php if($yonghu==0){echo 'selected';}?>>请选择用户类型</option>
			<option value="1" <?php if($yonghu==1){echo 'selected';}?>>用户id</option>
			<option value="2" <?php if($yonghu==2){echo 'selected';}?> >用户名称</option>
			<option value="3" <?php if($yonghu==3){echo 'selected';}?> >用户邮箱</option>
			<option value="4" <?php if($yonghu==4){echo 'selected';}?> >用户手机</option>
			</select>
			
			<input type="text" name="yonghuzhi" class="input-text wid100" value="<?php echo !empty($yonghuzhi)?$yonghuzhi:''; ?>"/>
			<input class="button" type="submit" name="sososubmit" value="搜索">
			<input class="button" type="button" name="" id="reset_button" value="清空">
</form>
</div>


<div class="table-list lr10">
<!--start-->
  <table width="100%" cellspacing="0">
    <thead>
		<tr>
            <th width="100px" align="center">用户名</th>
            <th width="100px" align="center">商品名称</th>
            <th width="100px" align="center">商品期数</th>
            <th width="100px" align="center">云购次数</th>
            <th width="100px" align="center">消费金额</th>
            <th width="100px" align="center">时间</th>  
		</tr>
    </thead>
    <tbody>
    	<?php 
		for($j=0;$j<count($pay_list);$j++){
		?>
		<tr>
			<td align="center">
				<?php  echo $members[$j];?>
			</td>
			<td align="center"><?php echo $pay_list[$j]['shopname']; ?></td>	
			<td align="center"><?php echo $pay_list[$j]['shopqishu']; ?></td>	
			<td align="center"><?php echo $pay_list[$j]['gonumber']; ?></td>	
			<td align="center"><?php echo $pay_list[$j]['moneycount']; ?></td>	
			<td align="center"><?php echo date("Y-m-d H:i:s",$pay_list[$j]['time']); ?></td>	   	
		</tr>
       <?php }  ?>
	
  	</tbody>
	
</table>
</div><!--table-list end-->
<div id="pages"><ul><?php echo $page->show('two','li'); ?></ul></div>
<script type="text/javascript">
$.datetimepicker.setLocale('zh');
var logic = function( currentDateTime ){
	if (currentDateTime && currentDateTime.getDay() == 6){
		this.setOptions({
			minTime:'11:00'
		});
	}else
		this.setOptions({
			minTime:'8:00'
		});
};
$('.posttime').datetimepicker({
	onChangeDateTime:logic,
	onShow:logic
});

$("#reset_button").click(function(){
	$(".posttime").val('');
	$("input[name=yonghuzhi]").val('');
	
});
</script>
</body>
</html> 