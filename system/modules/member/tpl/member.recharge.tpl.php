<?php defined('G_IN_ADMIN')or exit('No permission resources.'); ?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<link rel="stylesheet" href="<?php echo G_GLOBAL_STYLE; ?>/global/css/global.css" type="text/css">
<link rel="stylesheet" href="<?php echo G_GLOBAL_STYLE; ?>/global/css/style.css" type="text/css">
<link rel="stylesheet" href="<?php echo G_PLUGIN_PATH; ?>/calendar_2/jquery.datetimepicker.css" type="text/css"> 
<script type="text/javascript" charset="utf-8" src="<?php echo G_PLUGIN_PATH; ?>/calendar_2/jquery.js"></script>
<script type="text/javascript" charset="utf-8" src="<?php echo G_PLUGIN_PATH; ?>/calendar_2/build/jquery.datetimepicker.full.js"></script>

<style>
tbody tr{ line-height:30px; height:30px;} 
</style>
</head>
<body>
<div class="header lr10">
	<?php echo $this->headerment();?>
</div>
<div class="bk10"></div>
<div class="header-data lr10">
<div style="margin-bottom:5px;">
搜索的总金额数：<span style="color:red;font-weight:bold;"><?php echo $summoeny['sum_money'] ? : 0;?>元</span>
</div>
<form action="" method="get">
    时间搜索:
    <input name="posttime1" type="text" id="posttime1" class="input-text posttime"   value="<?php echo !empty($posttime1)?date("Y-m-d H:i:s",$posttime1):''?>"/> -
    <input name="posttime2" type="text" id="posttime2" class="input-text posttime"   value="<?php echo !empty($posttime2)?date("Y-m-d H:i:s",$posttime2):''?>"/>
			
        <select name="chongzhi">
            <option value="-1"
            <?php
                if(isset($chongzhi)){
                    echo ($chongzhi=='-1')?'selected':'';
                }
            ?>
            >请选择充值来源</option>

            <option value="0"
            <?php
                if(isset($chongzhi)){
                    echo ($chongzhi=='0')?'selected':'';
                }

            ?>
            >网站</option>
            <option value="1"
            <?php
                if(isset($chongzhi)){
                    echo ($chongzhi=='1')?'selected':'';
                }

            ?>
            >APP</option>
            <option value="2"
                <?php
                if(isset($chongzhi)){
                    echo ($chongzhi=='2')?'selected':'';
                }

                ?>
            >微信公众号</option>
        </select>



            <!-- 支付方式 -->
        <select name="paytype">
            <option value="0"
            <?php
                if(isset($paytype)){
                    echo ($paytype=='0')?'selected':'';
                }

            ?>
            >请选择支付方式</option>

            <option value="1"
            <?php
                if(isset($paytype)){
                    echo ($paytype=='1')?'selected':'';
                }

            ?>
            >微信支付</option>
            <option value="2"
            <?php
                if(isset($paytype)){
                    echo ($paytype=='2')?'selected':'';
                }

            ?>
            >爱贝支付</option>
            <option value="3"
                <?php
                if(isset($paytype)){
                    echo ($paytype=='3')?'selected':'';
                }

                ?>
            >京东网关</option>
        </select>
            <!-- 状态 -->
        <select name="status">
            <option value="1"
            <?php
                if(isset($status)){
                    echo ($status=='1')?'selected':'';
                }

            ?>
            >已付款</option>
            <option value="2"
            <?php
                if(isset($status)){
                    echo ($status=='2')?'selected':'';
                }

            ?>
            >未付款</option>
            <option value="0"
            <?php
                if(isset($status)){
                    echo ($status=='0')?'selected':'';
                }

            ?>
            >全部</option>
        </select>

        <!-- 用户类型 -->
        <!-- <select name="yonghu">
        <option value="请选择用户类型"
        <?php
            if(isset($yonghu)){
                echo ($yonghu=='请选择用户类型')?'selected':'';
            }
        ?>
        >请选择用户类型</option>
        <option value="用户id"
        <?php
            if(isset($yonghu)){
                echo ($yonghu=='用户id')?'selected':'';
            }
        ?>
        >用户id</option>
        <option value="用户名称"
        <?php
            if(isset($yonghu)){
                echo ($yonghu=='用户名称')?'selected':'';
            }
        ?>
        >用户名称</option>
        <option value="用户邮箱"
        <?php
            if(isset($yonghu)){
                echo ($yonghu=='用户邮箱')?'selected':'';
            }
        ?>
        >用户邮箱</option>
        <option value="用户手机"
        <?php
            if(isset($yonghu)){
                echo ($yonghu=='用户手机')?'selected':'';
            }
        ?>
        >用户手机</option>
        </select>
        <input type="text" name="yonghuzhi" class="input-text wid100" value="<?php echo !empty($yonghuzhi)?$yonghuzhi:''; ?>"/>
         -->

        <input class="button" type="submit" name="sososubmit" value="搜索">
        <input class="button" type="button" name="" id="reset_button" value="清空">
</form>
</div>

<div class="bk10"></div>

<div class="table-list lr10">
<!--start-->
  <table width="100%" cellspacing="0">
    <thead>
		<tr>
            <th width="100px" align="center">用户名</th>
            <th width="140px" align="center">单号</th>
            <th width="100px" align="center">充值金额</th>
            <th width="100px" align="center">充值来源</th>
            <th width="100px" align="center">支付方式</th>
            <th width="100px" align="center">状态</th>
            <th width="100px" align="center">时间</th>
		</tr>
    </thead>
    <tbody>
    	<?php 
		foreach($recharge as $k => $v){ 
		?>
		<tr>
			<td align="center"><?php  echo $v['username'];?></td>
			<td align="center"><?php echo $v['code']; ?></td>	
			<td align="center"><?php echo $v['money']; ?></td>	
			<td align="center"><?php echo $v['plat_type']; ?></td>
			<td align="center"><?php echo $v['pay_type']; ?></td>	
			<td align="center"><?php echo $v['status']; ?></td>	
			<td align="center"><?php echo $v['time']; ?></td>	   	
		</tr>
       <?php }  ?>
	
  	</tbody>
	
</table>
<style>
.Page_This{color:red}
</style>
</div><!--table-list end-->
<div id="pages"><ul><li>共 <?php echo $total; ?> 条</li><?php echo $page->show('two','li'); ?></ul></div>
<script>
$.datetimepicker.setLocale('zh');
var logic = function( currentDateTime ){
	if (currentDateTime && currentDateTime.getDay() == 6){
		this.setOptions({
			minTime:'11:00'
		});
	}else
		this.setOptions({
			minTime:'8:00'
		});
};
$('.posttime').datetimepicker({
	onChangeDateTime:logic,
	onShow:logic
});

$("#reset_button").click(function(){
	$(".posttime").val('');
	
});
</script>
</body>
</html> 