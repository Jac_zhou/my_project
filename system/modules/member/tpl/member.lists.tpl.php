<?php defined('G_IN_ADMIN')or exit('No permission resources.'); ?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<link rel="stylesheet" href="<?php echo G_GLOBAL_STYLE; ?>/global/css/global.css" type="text/css">
<link rel="stylesheet" href="<?php echo G_GLOBAL_STYLE; ?>/global/css/style.css" type="text/css">

<link rel="stylesheet" href="<?php echo G_PLUGIN_PATH; ?>/calendar_2/jquery.datetimepicker.css" type="text/css"> 
<script type="text/javascript" charset="utf-8" src="<?php echo G_PLUGIN_PATH; ?>/calendar_2/jquery.js"></script>
<script type="text/javascript" charset="utf-8" src="<?php echo G_PLUGIN_PATH; ?>/calendar_2/build/jquery.datetimepicker.full.js"></script>

</head>
<body>
<div class="header lr10">
	<?php echo $this->headerment();?>
	<select name="member_list" id="select_way">
		<option value="1">导出注册成功用户</option>
		<option value="2">导出注册失败用户</option>
	</select>
	<input type="button" value="确定" id="explore_register"/>
</div>
<div class="bk10"></div>

<div class="header-data lr10">

	<span class="lr5"></span><input class="button" type="button" onclick="window.location.href='<?php echo $this_path; ?>/day_new'" value=" 今日新增 ">	
	<span class="lr5"></span><input class="button" type="button" onclick="window.location.href='<?php echo $this_path; ?>/day_shop'" value=" 今日消费 ">
	<span class="lr5"></span><input class="button" type="button" onclick="window.location.href='<?php echo $this_path; ?>/noreg'" value=" 未认证 ">
	<span class="lr5"></span><input class="button" type="button" onclick="window.location.href='<?php echo $this_path; ?>/del'" value=" 已删除 ">	
	<span class="lr5"></span>
	账号绑定:
	<select name="sousuo">
					<option value="b_qq">绑定QQ账户</option>					
	</select>
	<input class="button" type="button" onclick="window.location.href='<?php echo $this_path; ?>/b_qq'"  value="查看">
	
	<span class="lr10"></span>
	排序:
	<select id="user_paixu">
		<option value="uid">会员uid</option>
		<option value="money" >账户金额</option>
		<option value="score">会员福分</option>
		<option value="jingyan">会员经验</option>
		<option value="login_time">登陆时间</option>
		<option value="time">注册时间</option>
	</select>
	<input class="button" type="button" onclick="order_fun_sub('desc')" value="倒序">
	<input class="button" type="button" onclick="order_fun_sub('asc')" value="正序">
	
	<script>
		var user_paixu_value = 'uid';
		function order_fun_sub(type){		
			window.location.href='<?php echo $this_path.'/'.$user_type."/"; ?>'+user_paixu_value+"/"+type;	
		}
		document.getElementById("user_paixu").onchange=function(){		
			user_paixu_value = this.value;		
		}
	</script>
    <!--2016/1/6start-->
	<form action='<?php echo $this_path; ?>/new_add/' method='get' style='display:inline-block'>
		时间搜索:
		<input name="posttime1" type="text" class="datetimepicker7 input-text"  readonly="readonly" value="<?php echo !empty($posttime1)?date("Y/m/d H:i:s",$posttime1):''?>" /> -  
 		<input name="posttime2" type="text" class="datetimepicker7 input-text"  readonly="readonly" value="<?php echo !empty($posttime2)?date("Y/m/d H:i:s",$posttime2):''?>" />
			
		<select name="add_add">
			<option value="1" <?php if($add_add=='1'){echo "selected";} ?>>新增</option>
			<option value="2" <?php if($add_add=='2'){echo "selected";} ?>>消费</option>
		</select>
		<input class="button" type="submit" id="time_add" name="sousuo" value="搜索" />
		<input class="button" type="button" id="reset_button" value="清空" />
	</form>	
	<!--2016/1/6end-->	
	<div class="lr10" style="display:inline-block;margin-left:20px;">
		共有会员: <font color="#0c0"> <?php echo $this->member_count_num; ?> </font>人
		<span class="lr10"></span>
		今日注册: <font color="#f60"> <?php echo $this->member_new_num; ?> </font>人
		<span class="lr10"></span>
		删除会员: <font color="red"> <?php echo $this->member_del_num; ?> </font>人
	</div>
</div>

<div class="lr10" style="line-height:30px;color:#f60">
	<b>会员列表:</b> <?php echo $select_where; ?> &nbsp;&nbsp;&nbsp; 共找到 <?php echo $total; ?> 个会员
</div>
<div class="table-list lr10">        
  <!--start-->
  <table width="100%" cellspacing="0">
    <thead>
		<tr>
            <th align="center">UID</th>
            <th align="center">用户名</th>
            <!-- <th align="center">邮箱</th> -->
            <th align="center">手机</th>
            <th align="center">金额</th>
			<th align="center">积分</th> 
			<!--  <th align="center">经验值</th>	-->		
			<th align="center">登陆时间,地址,IP</th>
			<th align="center">注册时间</th>
			<th align="center">注册平台</th>		
			<th align="center">账户绑定</th>
            <th align="center">管理</th>
		</tr>
    </thead>
    <tbody>
    	<?php foreach($members as $v){ ?>
			<tr>
				<td align="center"><?php echo $v['uid']; ?></td>
				<td align="center"><?php echo $v['username']; ?></td>	
				<!--  <td align="center"><?php echo $v['email']; ?> <?php if($v['emailcode']==1){?><span style="color:#0c0">√</span><?php }else{ ?><span style="color:red">×</span><?php } ?></td>-->	
				<td align="center"><?php echo $v['mobile']; ?> <?php if($v['mobilecode']==1){?><span style="color:#0c0">√</span><?php }else{ ?><span style="color:red">×</span><?php } ?></td>	
				<td align="center"><?php echo $v['money']; ?></td>
				<td align="center"><?php echo $v['score']; ?></td>
				<!--<td align="center"><?php echo $v['jingyan']; ?></td>-->				
				<td align="center"><?php echo _put_time($v['login_time'],"未登录"); ?>,<?php echo trim($v['user_ip'],","); ?></td>	
				<td align="center"><?php echo _put_time($v['time']); ?></td>
				<td align="center"><?php echo $v['reg_plat']; ?></td>
				<td align="center"><?php echo trim($v['band'],","); ?></td>
				<td align="center">
					<?php if($table=='@#_member_del'): ?>
					<a href="<?php echo G_MODULE_PATH; ?>/member/huifu/<?php echo $v['uid'];?>">恢复</a>				
					<a href="<?php echo G_MODULE_PATH; ?>/member/del_true/<?php echo $v['uid'];?>" onClick="return confirm('是否真的删除！');">删除</a>
					<?php else: ?>
					[<a href="<?php echo G_MODULE_PATH; ?>/member/modify/<?php echo $v['uid'];?>">改</a>]					
					[<a href="<?php echo G_MODULE_PATH; ?>/member/del/<?php echo $v['uid'];?>" onClick="return confirm('是否真的删除！');">删</a>]
					<?php endif; ?>
			   </td>            	
			</tr>
            <?php } ?>
  	</tbody>
</table>
</div><!--table-list end-->

<div id="pages" style="margin:10px 10px">		
	<ul><?php echo $page->show('two','li'); ?></ul>
</div>
<script type="text/javascript">
/*时间插件*/
$.datetimepicker.setLocale('zh');
var logic = function( currentDateTime ){
	if (currentDateTime && currentDateTime.getDay() == 6){
		this.setOptions({
			minTime:'11:00'
		});
	}else
		this.setOptions({
			minTime:'8:00'
		});
};
$('.datetimepicker7').datetimepicker({
	onChangeDateTime:logic,
	onShow:logic
});

/*清空时间搜索*/
$("#reset_button").click(function(){
	$("input[name=posttime1],input[name=posttime2]").val('');
});
/*导出注册的用户*/
$("#explore_register").click(function(){
	var select_way = $("#select_way").val();
	window.location.href="<?php echo G_MODULE_PATH.'/member/export_register/'?>"+select_way;
});

</script>
</body>
</html> 