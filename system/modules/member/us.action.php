<?php 
defined('G_IN_SYSTEM')or exit('No permission resources.');
System::load_app_class('base','member','no');
System::load_app_fun('my','go');
System::load_app_fun('user','go');
System::load_sys_fun("user");

class us extends base{
	public function __construct() {	
		//parent::__construct();
		$uid = abs(intval($this->segment(4)));
		if(!$uid)_message("参数不正确!");	
		if($uid > 1000000000)$uid = $uid-1000000000;		
		$this->uid = $uid;
		$member=$this->userinfo;
		$member_uid = $_COOKIE['uid'];
		$jimi = _encrypt($member_uid,"DECODE");
		if($this->uid == $jimi){
			$rul = WEB_PATH."/member/home/userbuylist";
			header("Location:".$rul);die;
		}
		
		/* 数据统计 20160108 start */
		set_su_sk();
		/* 数据统计 20160108 end */
		
	}
	public function uname(){
		$mysql_model=System::load_sys_class('model');
		
		
		$title="个人中心  - "._cfg("web_name");
		$index = $this->uid;
		$current_status = 'uname';
		$tab=$this->segment(3);
		$_url = $this->_cfg['param_arr'][4];
		$member=$mysql_model->GetOne("select * from `@#_member` where uid='$index'");
		
		//云购记录
        $sql = "SELECT
                      COUNT(m.id)
                FROM `@#_member_go_record` AS m
                LEFT JOIN `@#_shoplist` AS g ON m.shopid=g.id
                where
                    m.uid='$index' AND
                    g.id !=''
                    GROUP BY g.id";
		$total=$mysql_model->GetList($sql);
        $total = count($total);
		$page=System::load_sys_class('page');
		if(isset($_GET['p'])){$pagenum=$_GET['p'];}else{$pagenum=1;}	
		$page->config($total,10,$pagenum,"0");
		$record = $mysql_model->GetPage("select m.id,m.username,m.uphoto,m.uid,m.shopid,m.shopname,m.shopqishu,SUM(m.gonumber) AS count_gonumber,g.shenyurenshu as gsheng,g.q_end_time AS jie_time from `@#_member_go_record` AS m LEFT JOIN `@#_shoplist` AS g ON m.shopid=g.id where m.uid='$index' AND g.id !='' GROUP BY m.shopid order by gsheng DESC,jie_time DESC",array("num"=>10,"page"=>$pagenum,"type"=>1,"cache"=>0));
		
		
		if($member){
			//$membergo=$mysql_model->GetList("select * from `@#_member_go_record` where `uid`='$index' order by `shenyurenshu` DESC,`q_end_time` DESC limit 0,10 ");
			include templates("us","index");
		}else{
			_message("页面错误",WEB_PATH,3);
		}
	}
	
	//该商品的得奖者信息
	private function get_winner_info(){
		
	}
	public function userbuy(){
		$mysql_model=System::load_sys_class('model');
		$title="个人中心  - "._cfg("web_name");
		$index = $this->uid;		
		$tab=$this->segment(3);
		$_url = $this->_cfg['param_arr'][4];
		$current_status = 'userbuy';
		
		//晒单
		$page=System::load_sys_class('page');
		$total=$mysql_model->GetCount("select * from `@#_shaidan` where `sd_userid`='$index'");
		if(isset($_GET['p'])){$pagenum=$_GET['p'];}else{$pagenum=1;}	
		$page->config($total,12,$pagenum,"0");
		$shaidan=$mysql_model->GetPage("select s.*,g.id as shopid,title,g.q_user_code,g.q_end_time as jiexiao_tme,g.shenyurenshu as gsheng from `@#_shaidan` AS s LEFT JOIN `@#_shoplist` AS g ON s.sd_shopid=g.id where `sd_userid`='$index' order by gsheng DESC,jiexiao_tme DESC",array("num"=>12,"page"=>$pagenum,"type"=>1,"cache"=>0));
		
		//用户
		$member=$mysql_model->GetOne("select * from `@#_member` where uid='$index'");
		
		
		if($member){
			$membergo=$mysql_model->GetList("select * from `@#_member_go_record` where uid='$index' order by `id` DESC limit 0,10");		
			include templates("us","userbuy");
		}else{
			_message("页面错误",WEB_PATH,3);
		}
	}
	public function userraffle(){
        //echo G_ADMIN_DIR;
		$mysql_model=System::load_sys_class('model');
		$title="个人中心 - "._cfg("web_name");
		$index = $this->uid;		
		$tab=$this->segment(3);
		$_url = $this->_cfg['param_arr'][4];
		$current_status = 'userraffle';
		
		//$total=$mysql_model->GetCount("select id from `@#_member_go_record` where `uid`='$index' and `huode`>'10000000'");

        $sql ="select id from `@#_member_go_record` where `uid`='$index' and `huode`>'10000000'";

        $total = $mysql_model->GetList($sql);
        $total = count($total);
		$page=System::load_sys_class('page');
		if(isset($_GET['p'])){$pagenum=$_GET['p'];}else{$pagenum=1;}	
		$page->config($total,12,$pagenum,"0");
		
		$record = $mysql_model->GetPage("select m.*,g.zongrenshu,g.q_end_time from `@#_member_go_record` AS m LEFT JOIN `@#_shoplist` AS g ON m.shopid=g.id where m.uid='$index' and `huode`>'10000000' ORDER BY m.id DESC",array("num"=>12,"page"=>$pagenum,"type"=>1,"cache"=>0));
		
		$member=$mysql_model->GetOne("select * from `@#_member` where uid='$index'");	
		if($member){
			include templates("us","userraffle");
		}else{
			_message("页面错误",WEB_PATH,3);
		}
	}
	
	//众乐记录详细
	public function userbuydetail(){
		$mysql_model=System::load_sys_class('model');
		$current_status = 'uname';
		$_url = $this->_cfg['param_arr'][4];
		
		$index = $this->uid;
		
		$member=$mysql_model->GetOne("select * from `@#_member` where uid='$index'");
		
		$title="个人中心  - "._cfg("web_name");
		$crodid=intval($this->segment(5));
        //查询商品的信息
        $filed = 'id,thumb,qishu';
        $sql = "select $filed from @#_shoplist where id='$crodid'";
        $shopinfo = $mysql_model->GetOne($sql);
        //查询购买记录
        $MemberRecordModel = System::load_app_model('MemberRecord','Zapi');
        $record = $MemberRecordModel->get_shop_list($crodid,$member['uid']);
		
		if($crodid>0){
			include templates("member","seedetail");
		}else{
			_message("页面错误",WEB_PATH."/member/us/seedetail",3);
		}
	}
	
}

?>