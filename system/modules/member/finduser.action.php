<?php 

defined('G_IN_SYSTEM')or exit('No permission resources.');
System::load_app_class('base',null,'no');
System::load_app_fun('user','go');
System::load_app_fun('my','go');

class finduser extends SystemAction {
	public function __construct(){	
		
	}
	//找回密码
	public function findpassword(){
		if(isset($_POST['submit'])){	
		
			$info = array('error'=>0,'msg'=>'');
			$name=isset($_POST['name']) ? trim($_POST['name']) : "";
			//$txtRegSN=strtoupper($_POST['txtRegSN']);
			//if(md5($txtRegSN)!=_getcookie('checkcode')){
			//	$info = array('error'=>1,'msg'=>'验证码错误');
			//	die(json_encode($info));
			//}
			$regtype=null;
			if(_checkmobile($name)){
				$regtype='mobile';				
			}
			if(_checkemail($name)){
				$regtype='email';
			}
			
			if($regtype==null){
				$info = array('error'=>1,'msg'=>'帐号类型不正确!');
				die(json_encode($info));
				
			}
			
			$info=$this->DB()->GetOne("SELECT * FROM `@#_member` WHERE $regtype = '$name' LIMIT 1");
			if(!$info){
				$info = array('error'=>1,'msg'=>'帐号不存在!');
				die(json_encode($info));
			}
			$info = array('error'=>0,'msg'=>'成功!','url'=>WEB_PATH."/member/finduser/find".$regtype."check"."/"._encrypt($name));
			die(json_encode($info));
			
		}
		$title="找回密码";
		include templates("user","findpassword");
	}
	//手机重置密码
	public function findsendmobile(){
		$name=_encrypt($this->segment(4),"DECODE");
		_message($name);
		$member=$this->DB()->GetOne("SELECT * FROM `@#_member` WHERE `mobile` = '$name' LIMIT 1");
		if(!$member)_message("参数不正确!");	
		$checkcode=explode("|",$member['mobilecode']);
		$times=time()-$checkcode[1];		
		if($times > 120){
			//重发验证码
			$mobile_code=rand(100000,999999);
			$mobile_time=time();				
			$mobilecodes=$mobile_code.'|'.$mobile_time;//验证码		
			$this->DB()->Query("UPDATE `@#_member` SET passcode='$mobilecodes' where `uid`='$member[uid]'");
            //2014-11-24 lq
            $temp_m_pwd = $this->DB()->GetOne("select value from `@#_caches` where `key` = 'template_mobile_pwd' LIMIT 1");
            $text=str_replace("000000",$mobile_code,$temp_m_pwd['value']);

            $sendok=_sendmobile($name,$text);
			if($sendok[0]!=1){
				_message($sendok[1]);
			}
			_message("正在重新发送...",WEB_PATH."/member/finduser/findmobilecheck/"._encrypt($member['mobile']),2);				
		}else{
			_message("重发时间间隔不能小于2分钟!",WEB_PATH."/member/finduser/findmobilecheck/"._encrypt($member['mobile']));
		}		
	}
	public function findmobilecheck(){
		
		$title="手机找回密码";	
		$time=120;	
		$namestr=$this->segment(4);
		$mobile=_encrypt($namestr,"DECODE");	//用户账号
		if(!_checkmobile($mobile))_message("参数错误！");
		
		$member = $this->DB()->GetOne("SELECT * FROM `@#_member` WHERE `mobile` = '$mobile' LIMIT 1");
		
		if(!$member)_message("没有该用户!");
		$result = $this->DB()->GetOne("SELECT time FROM `@#_mobile_code` WHERE `mobile` = '$mobile' and type='3' order by time LIMIT 1");
		//var_export($result);
		
		if(isset($result['time']) && time()-$result['time']>$time){
			//验证码过期，重新发送验证码
			$member['passcode'] = -1;
		} 
		//var_dump($member);die;
		if($member['passcode']==-1){
			
			$checkcodes = rand(100000, 999999);
			$SendCon = System::load_contorller('Send','Zapi');
			$SendCon->type = 3;					//验证码类型  int型
			$SendCon->mobile = $mobile;			//手机号码
			$SendCon->code   = $checkcodes;		//验证码
			//var_dump($mobile.'--'.$checkcodes);die;
			$result = $SendCon->send_msg();		//发送内容
			
			if($result){
				$info = array('1','发送成功');	
				//header("location:".WEB_PATH."/member/finduser/findmobilecheck/"._encrypt( ['mobile']));exit;			
			}else{
				_message('短信额度已满');
			}
		}else{
			var_dump($member['passcode']);
		}
		$enname=substr($mobile,0,3).'****'.substr($mobile,7,10);
		$time=120;
		include templates("user","findmobilecheck");
	}
	
	public function findmobilecheck_add(){
		if(isset($_POST['submit'])){
			$namestr=$this->segment(4);
			$name=_encrypt($namestr,"DECODE");
			if(strlen($name)!=11){
				$info = array('error'=>1,'msg'=>'参数错误!');
				die(json_encode($info));
			}
		
			$member=$this->DB()->GetOne("SELECT * FROM `@#_member` WHERE `mobile` = '$name' LIMIT 1");
			if(!$member){
				$info = array('error'=>1,'msg'=>'参数不正确!');
				die(json_encode($info));
			}
			
			$checkcodes=isset($_POST['checkcode']) ? trim($_POST['checkcode']) : '';
			if($checkcodes == ''){
				$info = array('error'=>1,'msg'=>'参数不正确!');
				die(json_encode($info));
			}
			
			if(strlen($checkcodes)!=6){
				$info = array('error'=>1,'msg'=>'验证码是6位数!');
				die(json_encode($info));
			}
			if($checkcodes!==$member['passcode']){
				$info = array('error'=>1,'msg'=>'验证码输入不正确!');
				die(json_encode($info));
			}
			//
			$urlcheckcode=_encrypt($member['mobile']."|".$member['passcode']."|".time());
			//_setcookie("uid",_encrypt($member['uid']));	
			//_setcookie("ushell",_encrypt(md5($member['uid'].$member['password'].$member['mobile'].$member['email'])));
			$info = array('error'=>0,'msg'=>'手机验证成功!','url'=>WEB_PATH."/member/finduser/findok/".$urlcheckcode);
			die(json_encode($info));
		}
	}
	
	//邮箱找回密码
	public function findsendemail(){
		$name=_encrypt($this->segment(4),"DECODE");	
		$member=$this->DB()->GetOne("SELECT * FROM `@#_member` WHERE `email` = '$name' LIMIT 1");
		if(!$member)_message("参数错误!");	
		$this->DB()->Query("UPDATE `@#_member` SET passcode='-1' where `uid`='$member[uid]'");
		_message("正在重新发送...",WEB_PATH."/member/finduser/findemailcheck/".$this->segment(4),2);				
		exit;	
	}	
	public function findemailcheck(){
		$title="通过邮箱找回密码";				
		$enname=$this->segment(4);
		$name=_encrypt($this->segment(4),"DECODE");	
		$info=$this->DB()->GetOne("SELECT * FROM `@#_member` WHERE `email` = '$name' LIMIT 1");
		if(!$info)_message("未知错误!");		
		$emailurl=explode("@",$info['email']);

		if($info['passcode']==-1){	
			$passcode=_getcode(10);
			$passcode=$passcode['code'].'|'.$passcode['time'];//验证码
			$urlcheckcode=_encrypt($info['email']."|".$passcode);
			$url=WEB_PATH.'/member/finduser/findok/'.$urlcheckcode;			
			$this->DB()->Query("UPDATE `@#_member` SET `passcode`='$passcode' where `uid`='$info[uid]'");		
			$tit=_cfg("web_name")."邮箱找回密码";
            $con='<a href="'.WEB_PATH.'/member/finduser/findok/'.$urlcheckcode.'">';
            $con.=$url;
            $con.='</a>';
            $temp_e_pwd = $this->DB()->GetOne("select value from `@#_caches` where `key` = 'template_email_pwd' LIMIT 1");

            $content=str_replace("{地址}",$con,$temp_e_pwd['value']);

			_sendemail($info['email'],'',$tit,$content);			
		}
		include templates("user","findemailcheck");		
	}
	//验证码正确
	public function findok(){		
		$key=$this->segment(4);
		if(empty($key)){
			_message("未知错误");	
		}else{
			$key = $this->segment(4);
		}
		
		$checkcode=explode("|",_encrypt($key,"DECODE"));		
		if(count($checkcode)!=3)_message("未知错误",NULL,3);
		
		$sql="select time from `@#_mobile_code` where `mobile`='$checkcode[0]' AND `mobilecode`= '$checkcode[1]' order by time LIMIT 1";	
		$member=$this->DB()->GetOne($sql);	
				
		if(!$member)_message("帐号或验证码错误",NULL,2);
		//判断是否过期
		$timec=time()-$member['time'];					
		if($timec<(3600*24)){
			$title="重置密码";
			include templates("user","findok");
		}else{
			$title="验证失败";
			include templates("user","finderror");		
		}
	}
	public function resetpassword(){
		if(isset($_POST['submit'])){
			$key=trim($_POST["hidKey"]);
			$password=md5($_POST["userpassword"]);
			$checkcode=explode("|",_encrypt($key,"DECODE"));	
			
			if(count($checkcode)!=3){
				$info = array('error'=>1,'msg'=>'未知错误!');
				die(json_encode($info));
			}			
			
			$sql="select * from `@#_member` where `mobile`='$checkcode[0]' LIMIT 1";		
			$member=$this->DB()->GetOne($sql);
			if(!$member){
				$info = array('error'=>1,'msg'=>'未知错误!');
				die(json_encode($info));
			}
			$this->DB()->Query("UPDATE `@#_member` SET `password`='$password',`passcode`='-1' where `uid`='$member[uid]'");

			$xu_id= _setcookie("uid","",time()-3600);
			$xu_ushell = _setcookie("ushell","",time()-3600);
			$info = array('error'=>0,'msg'=>'密码重置成功!','url'=>WEB_PATH."/member/user/login");
			die(json_encode($info));
		}
	}
	
}

?>