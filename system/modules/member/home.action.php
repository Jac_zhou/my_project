<?php 
defined('G_IN_SYSTEM')or exit('No permission resources.');
System::load_app_class('base','member','no');
System::load_app_fun('my','go');
System::load_app_fun('user','go');
System::load_sys_fun('send');
System::load_sys_fun('user');
System::load_app_fun('member',ROUTE_M);
class home extends base {
	protected $member = '';
	protected $_controll = '';
	public function __construct(){ 
		parent::__construct();
		if(ROUTE_A!='userphotoup' and ROUTE_A!='singphotoup'){
			//if(!$this->userinfo)_message("请登录",WEB_PATH."/member/user/login",3);
			/*2015/12/08start*/
			$is_ajax = $this->is_ajax();
			if(!$this->userinfo){
				if(!$is_ajax){
					_message("请登录",WEB_PATH."/member/user/login",1);
				}else{
					die(json_encode(array('status'=>0,'message'=>"您还没有登录!")));
				}
			}
			
			$url = isset($_REQUEST['backurl']) ? trim($_REQUEST['backurl']) : '';
			
			
			$xaction = isset($_REQUEST['xaction']) ? trim($_REQUEST['xaction']) : '';
			
			if($is_ajax && !empty($url) && $xaction == 'ajax'){
				die(json_encode(array('status'=>1)));
			}	
			/*2015/12/08end*/
		}
		
		
		$this->db = System::load_sys_class('model');
		
		
	}
	private function is_ajax()
	{
		if(isset($_SERVER["HTTP_X_REQUESTED_WITH"]) && strtolower($_SERVER["HTTP_X_REQUESTED_WITH"])=="xmlhttprequest"){
			return true;
		}else{
			return false;
		}
	}
	public function init(){
		$member=$this->userinfo;
		$title="我的夺宝中心 - "._cfg("web_name");
		$quanzi=$this->db->GetList("select * from `@#_quanzi_tiezi` order by id DESC LIMIT 5");
		
		$jingyan = $member['jingyan'];
		
		$dengji_1 = $this->db->GetOne("select * from `@#_member_group` where `jingyan_start` <= '$jingyan' and `jingyan_end` >= '$jingyan'"); 
		$max_jingyan_id = $dengji_1['groupid'];
		$dengji_2 = $this->db->GetOne("select * from `@#_member_group` where `groupid` > '$max_jingyan_id' order by `groupid` asc limit 1");
		if($dengji_2){
			$dengji_x = $dengji_2['jingyan_start'] - $jingyan;
 		}else{
			$dengji_x = $dengji_1['jingyan_end'] - $jingyan;	
		}
		
		
				
		include templates("member","index");
	}
	
	public function activing(){
		$member=$this->is_login();
		$uid = $member['uid'];
		$current_status = 'activing';  //判断当前是哪个控制下
		//注册立即赠送 3 元
		$sql = "select * from `@#_active` WHERE uid='$uid' AND type=1";
		$active_list = $this->db->GetList($sql);
		//首次充值立即赠送 5 元
		$sql = "select * from `@#_active` WHERE uid='$uid' AND type=3";
		$first_addmoney = $this->db->GetList($sql);
		//第一次邀请好友赠送10元，需好友充值随意金额
		$sql = "select * from `@#_active` WHERE uid='$uid' AND type=4";
		$yaoqingmoney_list = $this->db->GetList($sql);
		if($active_list){
			if($active_list['is_active'] == 1){
				$you_quan = 'have_ticket_ji';
			}else{
				$you_quan = 'have_ticket_no';
			}
			
		}else{
			$you_quan = 'none_ticket';
		}
		
		/*红包*/
		$sql_h = "select * from `@#_active`  WHERE uid='$uid' AND type=2";
		$total=$this->db->GetCount($sql_h);
		$num = 20;
		$page=System::load_sys_class('page');
		if(isset($_GET['p'])){$pagenum=$_GET['p'];}else{$pagenum=1;}	
		$page->config($total,$num,$pagenum,"0");		
		$hactive_list = $this->db->GetPage($sql_h,array("num"=>$num,"page"=>$pagenum,"type"=>1,"cache"=>0));
		
		foreach($hactive_list as $key=>$val){
			$hactive_list[$key]['r_time'] = date("Y-m-d H:i:s",$val['r_time']);
		}

		include templates("member","activing");
	}
	
	/*红包兑换成乐斗*/
	public function hongbao_duihuan(){
		$member = $this->is_login();
		$hong_id = isset($_POST['hong_id']) ? trim($_POST['hong_id']) : 0;
		$this->is_empty('红包');
		
		$sql = "SELECT * FROM `@#_active` WHERE uid='$member[uid]' AND id='$hong_id'";
		$res = $this->db->GetOne($sql);
		if(!$res){
			$info = array('error'=>1,'msg'=>'这个红包不属于你的');
			die(json_encode($info));
		}
		$ac_cash = intval($res['cash_value']);
		$ac_uid = $res['uid'];
		
		$sql_u = "UPDATE `@#_active` SET is_use=1,cash_value=0 WHERE id='$hong_id'";
		$this->db->Query($sql_u);
		if($this->db->affected_rows()){
			$sql_m = "UPDATE `@#_member` SET money=money+'$ac_cash' WHERE uid = '$ac_uid'";
			$this->db->Query($sql_m);
			if($this->db->affected_rows()){
				$info = array('error'=>0,'msg'=>'兑换成功！','url'=>WEB_PATH."/member/home/activing");
				die(json_encode($info));
			}else{
				$info = array('error'=>1,'msg'=>'兑换失败！');
				die(json_encode($info));
			}
		}
		
		
	}
	public function activechong(){
		$current_status = 'activing';  //判断当前是哪个控制下
		$member=$this->is_login();
		$title="账户充值 - "._cfg("web_name");
		$paylist = $this->db->GetList("SELECT * FROM `@#_pay` where `pay_start` = '1'");	
		
		include templates("member","chong_five");
	}
	
	
	//2015/12/11加获取某件商品最新一起的云购记录，这里主要是为了拿到最新一期的商品id，以便 猜你喜欢中 通过id来跳转到别的页面
	private function cget_zuixin_goods_shoplist($sid){
		if($sid){
			$sql = "SELECT `id`,`canyurenshu`,`zongrenshu`,`money`,`shenyurenshu`,title,thumb FROM `@#_shoplist` WHERE sid = '$sid' AND `q_uid` IS NULL ORDER BY `id` DESC LIMIT 1";
		$one_arr = $this->db->GetOne($sql);
		}
		return $one_arr;	
	}
	
	
	//个人设置
	public function userphoto(){
		$mysql_model=System::load_sys_class('model');
		$member=$this->userinfo;
		$current_status = 'modify';  //判断当前是哪个控制下
		$title="修改头像 - "._cfg("web_name"); ;
		$uid=_getcookie('uid');
		$ushell=_getcookie('ushell');
		include templates("member","photo");
	}
	
	//头像上传
	public function userphotoup(){

		if(!empty($_FILES)){			
			$uid=isset($_POST['uid']) ? $_POST['uid'] : NULL;		
			$ushell=isset($_POST['ushell']) ? $_POST['ushell'] : NULL;
			$login=$this->checkuser($uid,$ushell);			
			if(!$login){echo "未登陆";exit;}
			
			System::load_sys_class('upload','sys','no');
			upload::upload_config(array('png','jpg','jpeg'),500000,'touimg');
			upload::go_upload($_FILES['Filedata'],true);
			$files=$_POST['typeCode'];
			if(!upload::$ok){
				echo upload::$error;
			}else{
				$img=upload::$filedir."/".upload::$filename;				
				$size=getimagesize(G_UPLOAD."/touimg/".$img);
				$max=300;$w=$size[0];$h=$size[1];				
				if($w>300 or $h>300){
					if($w>$h){
						$w2=$max;
						$h2 = intval($h*($max/$w));
						upload::thumbs($w2,$h2,true);					
					}else{
						$h2=$max;
						$w2 = intval($w*($max/$h));
						upload::thumbs($w2,$h2,true);
					}
				}			
				echo "touimg/".$img;
			}					
		}
	}
	
	//2015/12/09改头像裁剪
	public function userphotoinsert(){
		$member = $this->is_login();
		$uid = $member['uid'];	
		$current_status = $this->segment(3);  //判断当前是哪个控制下
		$info = array('error'=>1,'msg'=>'');
		$tname  = trim(str_ireplace(" ","",$_POST['img']));
		$this->is_empty($tname,'图片不能为空');
		$tname  = _htmtocode($tname);
		if(!file_exists(G_UPLOAD.$tname)){
			$info = array('error'=>1,'msg'=>'头像修改失败','url'=>WEB_PATH."/member/home/userphoto");
			die(json_encode($info));
		}
		$x = isset($_POST['x']) ? (int)$_POST['x'] : 0;
		$y = isset($_POST['y']) ? (int)$_POST['y'] : 0;
		$w = isset($_POST['w']) ? (int)$_POST['w'] : 0;
		$h = isset($_POST['h']) ? (int)$_POST['h'] : 0;
		if(!$w || !$h){
			$info = array('error'=>1,'msg'=>'所选区域不在图片之内，请重新选择！');
			die(json_encode($info));
		}
		$point = array("x"=>$x,"y"=>$y,"w"=>$w,"h"=>$h);
		
		System::load_sys_class('upload','sys','no');
		upload::thumbs(160,160,false,G_UPLOAD.$tname,$point);
		upload::thumbs(80,80,false,G_UPLOAD.$tname,$point);
		upload::thumbs(30,30,false,G_UPLOAD.$tname,$point);			
		
		$this->db->Query("UPDATE `@#_member` SET img='$tname' where uid='$uid'");
		$info = array('error'=>0,'msg'=>'头像修改成功','url'=>WEB_PATH."/member/home/userphoto");
		die(json_encode($info));
	}
	
	//个人资料
	public function modify(){
	
		$mysql_model=System::load_sys_class('model');
		$member=$this->userinfo;
		$current_status = "modify";  //判断当前是哪个控制下
		$title="编辑个人资料 - "._cfg("web_name");;
		$member_qq=$this->db->GetOne("select * from `@#_member_band` where `b_uid`=$member[uid]");
		include templates("member","modify");
	}
	//邮箱验证
	public function mailchecking(){	
		$mysql_model=System::load_sys_class('model');
		$current_status = $this->segment(3);  //判断当前是哪个控制下
		$member=$this->userinfo;
		$title="邮箱验证 - "._cfg("web_name");;
		$_submit = isset($_POST['submit']) ? trim($_POST['submit']) : '';
		if($_submit){
			if($member['email'] && $member['emailcode'] == 1){
				$info = array('error'=>'1','msg'=>'您的邮箱已经验证成功,请勿重复验证！');
			}else{
				$info = array('error'=>'0','msg'=>'成功','url'=>WEB_PATH.'/member/home/mailchecking');
			}
			die(json_encode($info));
			
		}		
		include templates("member","mailchecking");
		
	}
	public function mailchackajax(){
		$mysql_model=System::load_sys_class('model');
		$member=$this->userinfo;		
		$member2=$mysql_model->GetOne("select uid from `@#_member` where `email`='".$_POST['param']."'");
		if(!empty($member2)){
			echo "邮箱已经存在";
		}else{
			echo '{
					"info":"",
					"status":"y"
				}';
		}
	}
	
	
	
	//发送验证邮件
	public function sendsuccess(){
		$info = array();
		$_submit = isset($_POST['submit']) ? trim($_POST['submit']) : '';
		if($_submit){
			$email = isset($_POST['email']) ? trim($_POST['email']) : '';
			$this->is_empty($email,'邮箱地址不能为空');
			$email = isset($_POST['email']) ? trim($_POST['email']) : '';
			$this->is_empty($email,'邮箱地址不能为空');
			if(!_checkemail($_POST['email'])){
				$info = array('error'=>1,'msg'=>'邮箱格式错误!');
				die(json_encode($info));
			}
			
			$config_email = System::load_sys_config("email");
			if(empty($config_email['user']) && empty($config_email['pass'])){
				$info = array('error'=>1,'msg'=>'系统邮箱配置不正确!');
				die(json_encode($info));
			}
			
			$member=$this->userinfo;
			$title="发送成功 - "._cfg("web_name");;
			
			$member2=$this->db->GetOne("select * from `@#_member` where `email`='$email' and `uid` != '$member[uid]'");
			if(!empty($member2) && $member2['emailcode'] == 1){
				$info = array('error'=>1,'msg'=>'该邮箱已经存在，请选择另外的邮箱验证！!');
				die(json_encode($info));
			}
			
			$strcode1=$email.",".$member['uid'].",".time();
			$strcode= _encrypt($strcode1);
			
			$tit=$this->_cfg['web_name_two']."激活注册邮箱";
			$content='<span>请在24小时内绑定邮箱</span>，点击链接：<a href="'.WEB_PATH.'/member/home/emailcheckingok/'.$strcode.'">';
			$content.=WEB_PATH.'/member/home/emailcheckingok/'.$strcode.'</a>';
			$succ=_sendemail($email,'',$tit,$content,'yes','no');
			if($succ=='no'){
				$info = array('error'=>1,'msg'=>'邮件发送失败!');
			}else{
				$info = array('error'=>0,'msg'=>'邮件发送成功',"url"=>WEB_PATH."/member/home/sendsuccess");
					
			}
			die(json_encode($info));
		}
		$current_status = 'modify';
		include templates("member","sendsuccess");	
		
	}
	
	//邮箱认证返回
	public function emailcheckingok(){
		$member=$this->userinfo;
		$key=$this->segment(4);
		if($this->segment(5)){
			$key.='/'.$this->segment(5);
		}
		
		$emailcode=_encrypt($key,'DECODE');				
		if(empty($emailcode)){
			 _message("认证失败,参数不正确！",null,3);
		}		
		$memberx=explode(",",$emailcode);		
		$email=$memberx[0];
		$timec=(time()-$memberx[2])/(60*60);	
		$qmember=$this->db->GetOne("select * from `@#_member` where `email`='$email' and `uid` != '$member[uid]'");
		if($qmember && $qmember['emailcode']==1){
			_message("该邮箱已被认证,请勿重复认证!",WEB_PATH.'/member/home');
		}		
		if($timec<24){
			$this->db->Query("UPDATE `@#_member` SET email='".$memberx[0]."',emailcode='1' where uid='$member[uid]'");			
			$title="邮箱验证完成 - "._cfg("web_name");;
			include templates("member","sendsuccess2");
		}else{
			_message("认证时间已过期!",null,3);
		}
	}
	public function mobilechecking(){
		$mysql_model=System::load_sys_class('model');
		$member=$this->userinfo;
		$title="手机验证 - "._cfg("web_name");;
		$_submit = isset($_POST['submit']) ? trim($_POST['submit']) : '';
		if($_submit){
			if($member['mobile'] && $member['mobilecode'] == 1){
				$info = array('error'=>1,'msg'=>'您的手机已经验证成功,请勿重复验证！');
			}else{
				$info = array('error'=>0,'msg'=>'成功','url'=>WEB_PATH."/member/home/mobilechecking");
			}
			die(json_encode($info));
		}
		
		include templates("member","mobilechecking");
	}
	
	//2015/12/13该手机验证
	public function mobilesuccess(){
		
		$title="手机验证 - "._cfg("web_name");;
		$member=$this->userinfo;
		
		if(isset($_POST['submit'])){
			$info = array();
			$mobile=isset($_POST['mobile']) ? trim($_POST['mobile']) : "";
			if(!_checkmobile($mobile) || $mobile==null){
				$info = array('error'=>1,'msg'=>'手机号格式错误');
				die(json_encode($info));
			}
			$member2=$this->db->GetOne("select mobilecode,uid,mobile from `@#_member` where `mobile`='$mobile' and `uid` != '$member[uid]'");
			if($member2 && $member2['mobilecode'] == 1){
				$info = array('error'=>1,'msg'=>'手机号已被注册！');
				die(json_encode($info));
			}					
			if($member['mobilecode']!=1){
				//验证码
				$ok = send_mobile_reg_code($mobile,$member['uid']);			
				if($ok[0]!=1){
					$info = array('error'=>1,'msg'=>"发送失败,失败状态:".$ok[1]);
				}else{
					$info = array('error'=>0,'msg'=>'发送成功',"url"=>WEB_PATH."/member/home/mobilesuccess");
					_setcookie("mobilecheck",base64_encode($mobile));
				}
				
			}
			$time=120;
			die(json_encode($info));
			
		}
		include templates("member","mobilesuccess");
	}
	//重发手机验证码
	public function sendmobile(){
		$member=$this->userinfo;
		$mobilecodes=rand(100000,999999).'|'.time();//验证码

		if($member['mobilecode']==1){_message("该账号验证成功",WEB_PATH."/member/home");}			
		
		$checkcode=explode("|",$member['mobilecode']);
		$times=time()-$checkcode[1];
		if($times > 120){
			//重发验证码			
				$ok = send_mobile_reg_code($member['mobile'],$member['uid']);
				if($ok[0]!=1){
					_message("发送失败,失败状态:".$ok[1]);
				}
			
			_message("正在重新发送...",WEB_PATH."/member/user/mobilecheck/"._encrypt($member['mobile']),2);				
		}else{
			_message("重发时间间隔不能小于2分钟!",WEB_PATH."/member/user/mobilecheck/"._encrypt($member['mobile']));
		}
		
	}
	public function mobilecheck(){	
		$member=$this->userinfo;
		$_submit = isset($_POST['submit']) ? trim($_POST['submit']) : '';
		$this->is_empty($_submit,'参数');
		
		$info = array();
		$shoujimahao =  base64_decode(_getcookie("mobilecheck"));
		if(!_checkmobile($shoujimahao)){
			$info = array('error'=>1,'msg'=>'手机号码格式错误!');
			die(json_encode($info));
		}		
		$checkcodes=isset($_POST['mobile']) ? trim($_POST['mobile']) : '';
		$this->is_empty($checkcodes,'参数不正确!');
		
		if(strlen($checkcodes)!=6){
			$info = array('error'=>1,'msg'=>'验证码输入不正确!');
			die(json_encode($info));
		}
		$usercode=explode("|",$member['mobilecode']);	

		if($checkcodes!=$usercode[0]){
			$info = array('error'=>1,'msg'=>'验证码输入不正确!');
			die(json_encode($info));
		}
		$this->db->Query("UPDATE `@#_member` SET `mobilecode`='1',`mobile` = '$shoujimahao' where `uid`='$member[uid]'");
		//福分、经验添加			
		$isset_user=$this->db->GetList("select `uid` from `@#_member_account` where `content`='手机认证完善奖励' and `type`='1' and `uid`='$member[uid]' and (`pay`='经验' or `pay`='福分')");	
		if(empty($isset_user)){
			$config = System::load_app_config("user_fufen");//福分/经验
			$time=time();
			$this->db->Query("insert into `@#_member_account` (`uid`,`type`,`pay`,`content`,`money`,`time`) values ('$member[uid]','1','福分','手机认证完善奖励','$config[f_phonecode]','$time')");
			$this->db->Query("insert into `@#_member_account` (`uid`,`type`,`pay`,`content`,`money`,`time`) values ('$member[uid]','1','经验','手机认证完善奖励','$config[z_phonecode]','$time')");			
			$this->db->Query("UPDATE `@#_member` SET `score`=`score`+'$config[f_phonecode]',`jingyan`=`jingyan`+'$config[z_phonecode]' where uid='".$member['uid']."'");
		}
		_setcookie("uid",_encrypt($member['uid']));	
		_setcookie("ushell",_encrypt(md5($member['uid'].$member['password'].$member['mobile'].$member['email'])));		
//福分、经验添加			
		$isset_user=$this->db->GetOne("select `uid` from `@#_member_account` where `pay`='手机认证完善奖励' and `type`='1' and `uid`='$member[uid]' or `pay`='经验'");	
		if(empty($isset_user)){
			$config = System::load_app_config("user_fufen");//福分/经验
			$time=time();

			$this->db->Query("insert into `@#_member_account` (`uid`,`type`,`pay`,`content`,`money`,`time`) values ('$member[uid]','1','福分','手机认证完善奖励','$config[f_overziliao]','$time')");
			$this->db->Query("insert into `@#_member_account` (`uid`,`type`,`pay`,`content`,`money`,`time`) values ('$member[uid]','1','经验','手机认证完善奖励','$config[z_overziliao]','$time')");			
			$this->db->Query("UPDATE `@#_member` SET `score`=`score`+'$config[f_overziliao]',`jingyan`=`jingyan`+'$config[z_overziliao]' where uid='".$member['uid']."'");
		}
		$info = array('error'=>0,'msg'=>'验证成功!','url'=>WEB_PATH."/member/home/modify");
		die(json_encode($info));
	}
	/*2015/12/09改*/
	public function usermodify(){
		$mysql_model=System::load_sys_class('model');
		$member=$this->userinfo;
		$info = array("error"=>1,"msg"=>'');
		$username=_htmtocode(trim($_POST['username']));
		$this->is_empty($username,'用户名');
		$username = str_ireplace("'","",$username);
		$qianming=_htmtocode(trim($_POST['qianming']));
		$reg_user_str = $this->db->GetOne("select value from `@#_caches` where `key` = 'member_name_key' limit 1");
		$reg_user_str = explode(",",$reg_user_str['value']);
		if(is_array($reg_user_str) && !empty($username)){
			foreach($reg_user_str as $rv){
				if($rv == $username){
					$info = array("error"=>1,"msg"=>'此昵称禁止使用');
					die(json_encode($info));
				}
			}
		
		}			
		//福分、经验添加
		$isset_user=$this->db->GetOne("select `uid` from `@#_member_account` where (`content`='手机认证完善奖励' or `content`='完善昵称奖励') and `type`='1' and `uid`='$member[uid]' and (`pay`='经验' or `pay`='福分')");	
		if(!$isset_user){			
			$config = System::load_app_config("user_fufen");//福分/经验
			$time=time();

			$this->db->Query("insert into `@#_member_account` (`uid`,`type`,`pay`,`content`,`money`,`time`) values ('$member[uid]','1','福分','完善昵称奖励','$config[f_overziliao]','$time')");
			$this->db->Query("insert into `@#_member_account` (`uid`,`type`,`pay`,`content`,`money`,`time`) values ('$member[uid]','1','经验','完善昵称奖励','$config[z_overziliao]','$time')");			
			$mysql_model->Query("UPDATE `@#_member` SET username='".$username."',qianming='".$qianming."',`score`=`score`+'$config[f_overziliao]',`jingyan`=`jingyan`+'$config[z_overziliao]' where uid='".$member['uid']."'");
		}	
		$mysql_model->Query("UPDATE `@#_member` SET username='".$username."',qianming='".$qianming."' where uid='".$member['uid']."'");
		//_message("修改成功",WEB_PATH."/member/home/modify",3);
		$info = array("error"=>0,"msg"=>'修改成功','url'=>WEB_PATH.'/member/home/modify');
		die(json_encode($info));
	}
	
	
	public function address(){
		$mysql_model=System::load_sys_class('model');
		$current_status = $this->segment(3);
		$member=$this->userinfo;
		$title="收货地址 - "._cfg("web_name");;
		$member_dizhi=$mysql_model->Getlist("select * from `@#_member_dizhi` where uid='".$member['uid']."' limit 5");
		foreach($member_dizhi as $k=>$v){		
			$member_dizhi[$k] = _htmtocode($v);
		}
		$count=count($member_dizhi);
		include templates("member","address");
	}
	
	/*2015/12/09改*/
	public function morenaddress(){
		$member=$this->is_login();
		
		$info = array();
		$id = isset($_POST['default_id']) ? intval($_POST['default_id']) : '';
		if($id == ''){
			$info = array('error'=>1,'msg'=>'参数错误');
			die(json_encode($info));
		}
		$mysql_model=System::load_sys_class('model');
		
		$member_dizhi=$mysql_model->Getlist("select * from `@#_member_dizhi` where uid='".$member['uid']."'");
		foreach($member_dizhi as $dizhi){
			if($dizhi['default']=='Y'){
				$mysql_model->Query("UPDATE `@#_member_dizhi` SET `default`='N' where uid='".$member['uid']."'");
			}
		}
		$id = abs(intval($id));
		$mysql_model->Query("UPDATE `@#_member_dizhi` SET `default`='Y' where id='".$id."'");
			
		$info = array('error'=>0,'msg'=>'修改成功','url'=>WEB_PATH."/member/home/address");
		die(json_encode($info));
	}
	
	/*2015/12/09改*/
	public function deladdress(){
		$member=$this->is_login();
		
		$mysql_model=System::load_sys_class('model');
		$info = array();
		$id = isset($_POST['default_id']) ? intval($_POST['default_id']) : '';
		if($id == ''){
			$info = array('error'=>1,'msg'=>'参数错误');
			die(json_encode($info));
		}
		
		$id = abs(intval($id));
		$dizhi=$mysql_model->Getone("select * from `@#_member_dizhi` where `uid`='$member[uid]' and `id`='$id'");
		if(!empty($dizhi)){
			if($dizhi['default'] == 'Y'){
				$info = array('error'=>1,'msg'=>'地址为默认地址，不能删除','url'=>WEB_PATH."/member/home/address");
				die(json_encode($info));
			}
			$mysql_model->Query("DELETE FROM `@#_member_dizhi` WHERE `uid`='$member[uid]' and `id`='$id'");
			$info = array('error'=>0,'msg'=>'删除成功','url'=>WEB_PATH."/member/home/address");
		}else{
			$info = array('error'=>1,'msg'=>'该条地址不是你的','url'=>WEB_PATH."/member/home/address");
		}
		die(json_encode($info));
	}
	/*2015/12/10改*/
	public function updateddress(){
		$mysql_model=System::load_sys_class('model');
		$member=$this->userinfo;
		$uid=$member['uid'];
		$id = $this->segment(4);
		$id = abs(intval($id));
		
		$action = isset($_POST['action']) ? trim($_POST['action']) : '';
		if($action == 'doupdate'){
			$info = array();
			$address_id = isset($_POST['address_id']) ? $_POST['address_id'] : '';
			$this->is_empty($address_id,'参数');
			
			$sheng=isset($_POST['sheng']) ? $_POST['sheng'] : "";
			$this->is_empty($sheng,'省');
			$shi=isset($_POST['shi']) ? $_POST['shi'] : "";
			$this->is_empty($shi,'市');
			$xian=isset($_POST['xian']) ? $_POST['xian'] : "";
			$this->is_empty($xian,'县');
			$jiedao=isset($_POST['jiedao']) ? trim($_POST['jiedao']) : "";
			$this->is_empty($jiedao,'街道');
			$youbian=isset($_POST['youbian']) ? trim($_POST['youbian']) : "";
			$shouhuoren=isset($_POST['shouhuoren']) ? trim($_POST['shouhuoren']) : "";
			$this->is_empty($shouhuoren,'收货人');
			$tell=isset($_POST['tell']) ? trim($_POST['tell']) : "";
			
			$mobile=isset($_POST['mobile']) ? trim($_POST['mobile']) : "";
			$this->is_empty($mobile,'手机号');
			$qq=isset($_POST['qq']) ? trim($_POST['qq']) : "";
			$time=time();
					
			if(!_checkmobile($mobile)){
				$info = array('error'=>1,'msg'=>'手机号格式错误');
				die(json_encode($info));
			}
			$update_id = $mysql_model->Query("UPDATE `@#_member_dizhi` SET 
			`uid`='".$uid."',		
			`sheng`='".$sheng."',
			`shi`='".$shi."',
			`xian`='".$xian."',
			`jiedao`='".$jiedao."',
			`youbian`='".$youbian."',
			`shouhuoren`='".$shouhuoren."',
			`tell`='".$tell."',
			`qq`='".$qq."',
			`mobile`='".$mobile."' where `id`='".$address_id."'");
			
			if($update_id){
				$info = array('error'=>0,'msg'=>'修改成功','url'=>WEB_PATH."/member/home/address");
			}else{
				$info = array('error'=>1,'msg'=>'修改失败');
			}
			die(json_encode($info));
		}
	}
	
	/*2015/12/09*/
	public function useraddress(){
		$mysql_model=System::load_sys_class('model');
		
		/*2015/12/09*/
		$member = $this->is_login();
		$uid=$member['uid'];
		
		$sheng = isset($_POST['sheng']) ? trim($_POST['sheng']) : "";
		$this->is_empty($sheng,'省');
		$shi = isset($_POST['shi']) ? trim($_POST['shi']) : "";
		$this->is_empty($shi,'市');
		$xian=isset($_POST['xian']) ? trim($_POST['xian']) : "";
		$this->is_empty($xian,'县');
		$jiedao=isset($_POST['jiedao']) ? trim($_POST['jiedao']) : "";
		$this->is_empty($jiedao,'街道');
		$youbian=isset($_POST['youbian']) ? $_POST['youbian'] : "";
		$shouhuoren=isset($_POST['shouhuoren']) ? trim($_POST['shouhuoren']) : "";
		$this->is_empty($shouhuoren,'收货人');
		$tell=isset($_POST['tell']) ? $_POST['tell'] : "";
		$mobile=isset($_POST['mobile']) ? $_POST['mobile'] : "";
		$this->is_empty($mobile,'手机号码');
		$qq=isset($_POST['qq']) ? $_POST['qq'] : "";
		$time=time();
				
		if(!_checkmobile($mobile)){

			die(json_encode(array('error'=>1,'msg'=>'手机号格式错误')));
		}
		$member_dizhi=$mysql_model->GetOne("select * from `@#_member_dizhi` where `uid`='".$member['uid']."'");
		if(!$member_dizhi){
			$default="Y";
		}else{
			$default="N";
		}
		$sql = "INSERT INTO `@#_member_dizhi`(`uid`,`sheng`,`shi`,`xian`,`jiedao`,`youbian`,`shouhuoren`,`tell`,`mobile`,`qq`,`default`,`time`)VALUES
		('$uid','$sheng','$shi','$xian','$jiedao','$youbian','$shouhuoren','$tell','$mobile','$qq','$default','$time')";
		$insert_id = $mysql_model->Query($sql);
		
		if($insert_id){
			$backaddress = $sheng.",".$shi.",".$xian.",".$jiedao;
			$user_confirm = array("address"=>$backaddress,"shouhuoren"=>$shouhuoren,"mobile"=>$mobile);
			die(json_encode(array('error'=>0,'msg'=>'收货地址添加成功','url'=>WEB_PATH.'/member/home/address','address_info'=>$user_confirm)));
		}else{
			die(json_encode(array('error'=>1,'msg'=>'收货地址添加失败！')));
		}
	}
	
	public function password(){
		$mysql_model=System::load_sys_class('model');
		$member=$this->userinfo;
		$current_status = 'modify';
		$title="密码修改 - "._cfg("web_name");;	
		include templates("member","password");
	}
	public function oldpassword(){
		$mysql_model=System::load_sys_class('model');
		$member=$this->userinfo;
		if($member['password']==md5($_POST['param'])){
			echo '{
					"info":"",
					"status":"y"
				}';
		}else{
			echo "原密码错误";
		}
	}
	public function userpassword(){
		$mysql_model=System::load_sys_class('model');
		$member = $this->is_login();
		$info = array();
		//$member=$mysql_model->GetOne("select * from `@#_member` where uid='".$member['uid']."'");	
		$password=isset($_POST['password']) ? $_POST['password'] : "";
		$userpassword=isset($_POST['userpassword']) ? $_POST['userpassword'] : "";
		$userpassword2=isset($_POST['userpassword2']) ? $_POST['userpassword2'] : "";
		if($password==null or $userpassword==null or $userpassword2==null){
			$info = array('error'=>1,'msg'=>'密码不能为空');
			die(json_encode($info));
		}
		
		if(strlen($_POST['password'])<6 || strlen($_POST['password'])>20){
			$info = array('error'=>1,'msg'=>'密码不能小于6位或者大于20位');
			die(json_encode($info));
		}
		if($_POST['userpassword']!==$_POST['userpassword2']){
			$info = array('error'=>1,'msg'=>'二次密码不一致');
			die(json_encode($info));
		}		
		$password=md5($password);
		$userpassword=md5($userpassword);
		if($member['password']!=$password){
			$info = array('error'=>1,'msg'=>'原密码错误');
		}else{
			$mysql_model->Query("UPDATE `@#_member` SET password='".$userpassword."' where uid='".$member['uid']."'");
			$info = array('error'=>0,'msg'=>'密码修改成功','url'=>WEB_PATH.'/member/home/password');
		}
		die(json_encode($info));
	}
	//夺宝记录
	public function userbuylist(){
		$mysql_model=System::load_sys_class('model');
		$current_status = $this->segment(3);  //判断当前是哪个控制下
		$member=$this->userinfo;
		$uid = $member['uid'];
		$title="夺宝记录 - "._cfg("web_name");
		
		/*2015/12/07*/
		$num = 10;
		$action = $this->segment(4);
		if($action == 'today'){
			$con = " AND m.time >".strtotime("-1 day"."23:59:59");
		}elseif($action == 'weekend'){
			$con = " and m.time>".strtotime("-1 week"."23:59:59");
		}elseif($action == 'month'){
			$con = " and m.time>".strtotime("-1 month"."23:59:59");
		}else{
			$con = '';
		}
		$sql = "SELECT COUNT(m.id) as num FROM `@#_member_go_record` AS m LEFT JOIN `@#_shoplist` AS g ON m.shopid=g.id AND g.id!='' WHERE `uid`='$uid' $con GROUP BY m.shopid";
		$total=$mysql_model->GetList($sql);
        $total=count($total);
		$page=System::load_sys_class('page');
		if(isset($_GET['p'])){$pagenum=$_GET['p'];}else{$pagenum=1;}	
		$page->config($total,$num,$pagenum,"0");		
		$record = $mysql_model->GetPage("select m.id,m.username,m.uphoto,m.uid,m.shopid,m.shopname,m.shopqishu,SUM(m.gonumber) AS count_gonumber,g.shenyurenshu as gsheng,g.q_end_time AS jie_time from `@#_member_go_record` AS m LEFT JOIN `@#_shoplist` AS g ON m.shopid=g.id where m.uid='$uid' AND g.id !='' $con GROUP BY m.shopid order by gsheng DESC,jie_time DESC",array("num"=>$num,"page"=>$pagenum,"type"=>1,"cache"=>0));

		
		/* 统计pv 20160109 start */
		$this->set_pv();
		/* 统计pv 20160109 end */
		
		include templates("member","userbuylist");
	}
	//夺宝记录详细
	public function userbuydetail(){
		$mysql_model=System::load_sys_class('model');
		$current_status = 'userbuylist';  //判断当前是哪个控制下
		$member=$this->userinfo;
		$title="夺宝详情 - "._cfg("web_name");;
		$crodid=intval($this->segment(4));

        //查询商品的信息
        $filed = 'id,thumb,qishu';
        $sql = "select $filed from @#_shoplist where id='$crodid'";
        $shopinfo = $mysql_model->GetOne($sql);

        //查询购买记录
        $MemberRecordModel = System::load_app_model('MemberRecord','Zapi');
        $record = $MemberRecordModel->get_shop_list($crodid,$member['uid']);
        
		if($crodid>0){
			include templates("member","userbuydetail");
		}else{
			_message("页面错误",WEB_PATH."/member/home/userbuylist",3);
		}
	}
	//获得的商品
	public function orderlist(){
		$member=$this->userinfo;
		$current_status = $this->segment(3);  //判断当前是哪个控制下		
		$uid = $member['uid'];
		$title="获得的商品 - "._cfg("web_name");
		
		/*2015/12/07*/
		$action = $this->segment(4);
		if($action == 'today'){
			$con = " and m.time>".strtotime("-1 day"."23:59:59");
		}elseif($action == 'weekend'){
			$con = " and m.time>".strtotime("-1 week"."23:59:59");
		}elseif($action == 'month'){
			$con = " and m.time>".strtotime("-1 month"."23:59:59");
		}else{
			$con = '';
		}
		
		$total=$this->db->GetCount("select id from `@#_member_go_record` AS m where m.uid='$uid' $con AND `huode`>'10000000'");

		$page=System::load_sys_class('page');
		if(isset($_GET['p'])){$pagenum=$_GET['p'];}else{$pagenum=1;}	
		$page->config($total,12,$pagenum,"0");		
		$record = $this->db->GetPage("select m.*,g.zongrenshu,g.q_end_time from `@#_member_go_record` AS m LEFT JOIN `@#_shoplist` AS g ON m.shopid=g.id where m.uid='$uid' $con and `huode`>'10000000' and g.q_uid is not null and g.q_showtime='N' ORDER BY m.id DESC",array("num"=>12,"page"=>$pagenum,"type"=>1,"cache"=>0));
		
		include templates("member","orderlist");
	}
	//账户管理
	public function userbalance(){
		$member=$this->userinfo;
		$current_status = $this->segment(3);  //判断当前是哪个控制下
		$uid = $member['uid'];
		$title="账户记录 - "._cfg("web_name");
		
		/*2015/12/07*/
		$action = $this->segment(4);
		if($action == 'today'){
			$con = " and time>".strtotime("-1 day"."23:59:59");
		}elseif($action == 'weekend'){
			$con = " and time>".strtotime("-1 week"."23:59:59");
		}elseif($action == 'month'){
			$con = " and time>".strtotime("-1 month"."23:59:59");
		}else{
			$con = '';
		}
	
		
		
		//$x_sql = "select * from `@#_member_account` where `uid`='$uid' and `pay` = '账户' $con $_where";
		//$total=$this->db->GetCount($x_sql);
		$page=System::load_sys_class('page');
		if(isset($_GET['p'])){$pagenum=$_GET['p'];}else{$pagenum=1;}	
		//$page->config($total,20,$pagenum,"0");		
		//$account = $this->db->GetPage("select * from `@#_member_account` where `uid`='$uid' and `pay` = '账户'  $con $_where ORDER BY time DESC",array("num"=>20,"page"=>$pagenum,"type"=>1,"cache"=>0));
		$x_sql = "select * from `@#_member_addmoney_record` where `uid`='$uid' $con";
		$total=$this->db->GetCount($x_sql);
		$page=System::load_sys_class('page');
		$page->config($total,20,$pagenum,"0");
		$account = $this->db->GetPage("select * from `@#_member_addmoney_record` where `uid`='$uid'  $con ORDER BY time DESC",array("num"=>20,"page"=>$pagenum,"type"=>1,"cache"=>0));
		
		include templates("member","userbalance");
	}
	
	//账户福分
	public function userfufen(){
		$member=$this->userinfo;	
		$uid = $member['uid'];
		$title="账户积分 - "._cfg("web_name");
	
		$total=$this->db->GetCount("select * from `@#_member_account` where `uid`='$uid' and `pay` = '福分'");
		$page=System::load_sys_class('page');
		if(isset($_GET['p'])){$pagenum=$_GET['p'];}else{$pagenum=1;}	
		$page->config($total,20,$pagenum,"0");		
		$account = $this->db->GetPage("select * from `@#_member_account` where `uid`='$uid' and `pay` = '福分' ORDER BY time DESC",array("num"=>20,"page"=>$pagenum,"type"=>1,"cache"=>0));
				
		include templates("member","userfufen");
	}	
	public function userrecharge(){
		$member=$this->userinfo;
		$title="账户充值 - "._cfg("web_name");
		$PayModel = System::load_app_model('Pay','Zapi');
		$paylist = $PayModel->get_pay_type_list('web','pay_id,pay_class,pay_thumb,pay_name');
		file_put_contents('log/pay_type.log', print_r($paylist,1));
		//$paylist = $this->db->GetList("SELECT * FROM `@#_pay` where `pay_start` = '1'");
		
		/*查询该用户在这个钱段上是否已近充值过了*/
		/*$chong_100 = is_song($member['uid'],'100');
		$chong_200 = is_song($member['uid'],'200');
		$chong_300 = is_song($member['uid'],'300');
		$chong_400 = is_song($member['uid'],'400');
		$chong_500 = is_song($member['uid'],'500');*/
		
		$member_percentage=$PayModel->Getlist("select *  from @#_recharge_percentage");
		
		include templates("member","userrecharge");
	}	
	//二次充值界面  20151225
	public function dopay()
	{
	    $member=$this->userinfo;
	    

	    $title="账户充值 - "._cfg("web_name");
	    include templates("member","dopay");
	}
	//充值成功界面  20151224
	public function paysuccess()
	{
	    $member=$this->userinfo;
	    $prev_url = WEB_PATH . '/member/cart/addmoney';    //判断来源
	    
		//惨你喜欢
		$canyoulist = guess_you_love();
		
		$title="充值成功 - "._cfg("web_name");
		include templates("member","paysuccess");
		
	    /*if( isset($_SERVER['HTTP_REFERER']) && $_SERVER['HTTP_REFERER'] == $prev_url){
	        $title="充值成功 - "._cfg("web_name");
	        include templates("member","paysuccess");
	    }else{
	        echo '<title>易中夺宝</title>';
	        die;
	    }*/
	}

	//圈子管理
	public function joingroup(){
		$member=$this->userinfo;
		$title="加入的圈子 - "._cfg("web_name");
		$addgroup=rtrim($member['addgroup'],",");
		if($addgroup){
			$group=$this->db->GetList("select * from `@#_quanzi` where `id` in ($addgroup)");		
		}else{
			$group=null;
		}	
		include templates("member","joingroup");
	}
	public function topic(){
		$member=$this->userinfo;
		$title="圈子话题 - "._cfg("web_name");
		$tiezi=$this->db->GetList("select * from `@#_quanzi_tiezi` where `hueiyuan`='$member[uid]'");	
		$hueifu=$this->db->GetList("select * from `@#_quanzi_hueifu` where `hueiyuan`='$member[uid]'");	
		include templates("member","topic");
	}
	public function tiezidel(){
		$member=$this->userinfo;
		$id = $this->segment(4);
		$id = abs(intval($id));
		$tiezi=$this->db->Getone("select * from `@#_quanzi_tiezi` where `hueiyuan`='$member[uid]' and  `id`='$id'");
		if($tiezi){
			$this->db->Query("DELETE FROM `@#_quanzi_tiezi` WHERE `hueiyuan`='$member[uid]' and  `id`='$id'");
			_message("删除成功",WEB_PATH."/member/home/topic");
		}else{
			_message("删除失败",WEB_PATH."/member/home/topic");
		}
	}
	public function hueifudel(){
		$member=$this->userinfo;
		$id = $this->segment(4);
		$id = abs(intval($id));
		$hueifu=$this->db->Getone("select * from `@#_quanzi_hueifu` where `id`='$id'");
		if($hueifu){
			$this->db->Query("DELETE FROM `@#_quanzi_hueifu` WHERE `id`='$id'");
			_message("删除成功",WEB_PATH."/member/home/topic");
		}else{
			_message("删除失败",WEB_PATH."/member/home/topic");
		}
	}

	
	//晒单
	public function singlelist(){
		$member=$this->userinfo;
		$current_status = $this->segment(3);  //判断当前是哪个控制下
		$title="我的晒单 - "._cfg("web_name");
		$cord=$this->db->Getlist("select shopid,shopname from `@#_member_go_record` where `uid`='$member[uid]' and `huode` > '10000000'");		
		
		//2015/12/12gai已晒单		
		$shaidan=$this->db->Getlist("select s.*,g.id as shopid,title,g.q_user_code from `@#_shaidan` AS s LEFT JOIN `@#_shoplist` AS g ON s.sd_shopid=g.id where `sd_userid`='$member[uid]' order by `sd_id` DESC limit 10");
		
		$sd_id = $r_id = array();
		foreach($shaidan as $key=>$sd){
			$sd_id[]=$sd['sd_shopid'];
			$shaidan[$key]['sd_content'] = emj_decode($sd['sd_content']);
		}
		
		foreach($cord as $rd){
			if(!in_array($rd['shopid'],$sd_id)){
				$r_id[]=$rd['shopid'];
			}					
		}
		if(!empty($r_id)){
			$rd_id=implode(",",$r_id);
			$rd_id = trim($rd_id,',');
		}else{
			$rd_id="0";
		}
		
		
		$total=$this->db->GetCount("select id from `@#_member_go_record` where shopid in ($rd_id) and `uid`='$member[uid]' and `huode`>'10000000' AND status LIKE '%已完成%'");
		$page=System::load_sys_class('page');
		if(isset($_GET['p'])){$pagenum=$_GET['p'];}else{$pagenum=1;}	
		$page->config($total,10,$pagenum,"0");
		
		//没有晒单的
		$sql = "select shopid,id,uid from `@#_member_go_record` where shopid in ($rd_id) and `uid`='$member[uid]' and `huode`>'10000000' AND status LIKE '%已完成%'";
		
		$record = $this->db->GetPage($sql,array("num"=>10,"page"=>$pagenum,"type"=>1,"cache"=>0));
		foreach($record AS $key=>$val){
			$arr = getcountgonumber($val['shopid'],$val['uid']);
			$record[$key]['count_gonumber'] = $arr['count_gonumber'];
			$record[$key]['time'] = microt($arr['time']);
			
		}
		unset($arr);
		include templates("member","singlelist");
	}
	
	//晒单详情
	public function singledetail(){
		$record_id = $this->segment(4);
		$current_status = "orderlist";  //判断当前是哪个控制下
		
		$member = $this->is_login();	
		$sql = "SELECT m.*,g.thumb,g.q_end_time FROM `@#_member_go_record` AS m LEFT JOIN `@#_shoplist` AS g ON m.shopid=g.id WHERE m.id=$record_id ";
		$recover_info = $this->db->GetOne($sql);
		$status=@explode(",",$recover_info['status']);               
		if($status[2]=='未完成' || $status[2]=='待收货'){       	
			if($status[1]=='未发货'){
				if(!$recover_info['confrim_addr'] || !$recover_info['confrim_addr_time']){
					$recover_info['status_info'] = "请确认收货地址";
				}else{
					$recover_info['status_info'] = "等待发货";
				}
				
			} 
			if($status[1]=='已发货'){
				$recover_info['status_info'] = "已发货";                  
			}  
		} 
		if($status[2]=='已完成'){
			$recover_info['status_info'] = "已签收";
		}   
		if($status[2]=='已作废'){
			$recover_info['status_info'] = "已作废";
		}
		
		//查询出该商品是否已经晒单了
		$sql_x = "SELECT sd_id FROM `@#_shaidan` WHERE sd_shopid=$recover_info[shopid] AND sd_userid=$member[uid]";
		$arr_action = $this->db->GetOne($sql_x);
		
		//确认收获地址
		System::load_app_fun('member','member');
		$address_list = getuser_addresslist($member['uid']);
		
		
		include templates("member","singledetail");
	}


	/*判断该商品的晒单状态2015/12/09加*/
	public function addsinglestatus(){
		$member = $this->is_login();
		$info = array();
		
		$recordid = isset($_POST['recover_id']) ? intval($_POST['recover_id']) : '';
		$this->is_empty($recordid,'要晒单的商品');
		$action = isset($_POST['action']) ? intval($_POST['action']) : '';
		if($recordid != '' && $action == 'page'){
			
		}
		$shaidan=$this->db->GetOne("select * from `@#_member_go_record` where `id`='$recordid' and `uid` = '$member[uid]'");
		if(!$shaidan){
			$info = array('error'=>1,'msg'=>'该商品您不可晒单');
			die(json_encode($info));
		}
		
		$ginfo=$this->db->GetOne("select id,sid,qishu from `@#_shoplist` where `id`='$shaidan[shopid]' LIMIT 1");
		if(!$ginfo){
			$info = array('error'=>1,'msg'=>'该商品已不存在');
			die(json_encode($info));
		}
		$shaidanyn=$this->db->GetOne("select sd_id from `@#_shaidan` where `sd_shopid`='$shaidan[shopid]' and `sd_userid` = '$member[uid]'");
		if($shaidanyn){
			$info = array('error'=>1,'msg'=>'不可重复晒单');
			die(json_encode($info));
		}
		$info = array('error'=>0,'msg'=>'该商品您还没有晒单呢','url'=>WEB_PATH."/member/home/singleinsert/".$recordid);
		die(json_encode($info));
		
		
	}
	/*添加晒单2015/12/09改*/
	public function singleinsert(){	
		$member=$this->userinfo;
		$current_status = 'singlelist';  //判断当前是哪个控制下
		$uid=_getcookie('uid');
		$ushell=_getcookie('ushell');
		$title="添加晒单 - "._cfg("web_name");
        		
		$recoverid = $this->segment(4);
		
		if(isset($_POST['submit'])){		
			$info = array();
			$sd_recoverid = isset($_POST['sd_recoverid']) ? intval($_POST['sd_recoverid']) : '';
			$this->is_empty($sd_recoverid,'晒单的商品');
			
			$sql_f = "select shopid from `@#_member_go_record` where `id`='$sd_recoverid' and `uid` = '$member[uid]'";
			$shaidan=$this->db->GetOne($sql_f);
			if(!$shaidan){
				$info = array('error'=>1,'msg'=>'您没有购买此商品！');
				die(json_encode($info));
			}
			
			$sd_shopid = $shaidan['shopid'];
			$ginfo=$this->db->GetOne("select id,sid,qishu from `@#_shoplist` where `id`='$sd_shopid' LIMIT 1");
			
			/*判断该商品是否晒过单了*/
			$sql_p = "SELECT sd_id FROM `@#_shaidan` WHERE sd_shopid=".$shaidan['shopid']." AND sd_userid=".$member['uid'];
			if($this->db->GetOne($sql_p)){
				$info = array('error'=>1,'msg'=>'该商品您已经晒过单了，请不要重复晒！');
				die(json_encode($info));
			}
			
			$title = isset($_POST['title']) ? trim($_POST['title']) : '';

			$this->is_empty($title,'标题');
			
			$content = isset($_POST['content']) ? strip_tags(trim($_POST['content'])) : '';
            
			$this->is_empty($content,'内容');
			
			$fileurl_tmp = isset($_POST['fileurl_tmp']) ? $_POST['fileurl_tmp'] : '';
			$this->is_empty($fileurl_tmp,'图片');
			
				
			System::load_sys_class('upload','sys','no');
			$img = $_POST['fileurl_tmp'];
			$num = count($img);
			
			$pic="";
			for($i=0;$i<$num;$i++){
			    $pic.=trim($img[$i]).";";
			}
				
			$src=trim($img[0]);
				
			if(!file_exists(G_UPLOAD.$src)){
			    $info = array('error'=>1,'msg'=>'晒单图片不正确');
			    die(json_encode($info));
			}
			$size=getimagesize(G_UPLOAD.$src);
			$width=220;
			$height=intval($size[1]*($width/$size[0]));
			
			$src_houzhui = upload::thumbs($width,$height,false,G_UPLOAD.'/'.$src);
			$thumbs=$src."_".intval($width).intval($height).".".$src_houzhui;
			
			//裁剪图片 20151224  裁剪两份   start
			if( $num > 0 ){
    			foreach($img as $k => $v){
    			    $v = trim($v);
    			    if( ! file_exists( G_UPLOAD.$v ) ){
    			        continue;
    			    }
    			    $m_size = getimagesize(G_UPLOAD.$v);
    			    $t_width = 220;
    			    $t_height = $m_size[1]*($t_width/$m_size[0]);
    			    upload::thumbs($t_width, $t_height, false, G_UPLOAD.'/'.$v, '', '_220');
    			    
    			    $f_width = 540;
    			    $f_height=$m_size[1]*($f_width/$m_size[0]);
    			    upload::thumbs($f_width, $f_height, true, G_UPLOAD.'/'.$v);  //宽度为540的 覆盖原图  
    			}
			}
			//裁剪图片 20151224  裁剪两份   end
			
			$sd_userid = $this->userinfo['uid'];
			$sd_shopid = $ginfo['id'];	
			$sd_shopsid = $ginfo['sid'];	
			$sd_qishu = $ginfo['qishu'];	
			$sd_title = _htmtocode($_POST['title']);
			$sd_thumbs = $thumbs;
			$sd_content = editor_safe_replace(stripslashes($_POST['content']));
			$sd_photolist= $pic;
			$sd_time=time();		
			$sd_ip = _get_ip_dizhi();

			$this->db->Query("INSERT INTO `@#_shaidan`(`sd_userid`,`sd_shopid`,`sd_shopsid`,`sd_qishu`,`sd_ip`,`sd_title`,`sd_thumbs`,`sd_content`,`sd_photolist`,`sd_time`)VALUES
			('$sd_userid','$sd_shopid','$sd_shopsid','$sd_qishu','$sd_ip','$sd_title','$sd_thumbs','$sd_content','$sd_photolist','$sd_time')");
			$info = array('error'=>0,'msg'=>'晒单分享成功','url'=>WEB_PATH."/member/home/singlelist");
			
			/*晒单成功，给用户添加2块钱*/
			shaidan_get_money($sd_userid);
			
			die(json_encode($info));
		}
		include templates("member","singleinsert");
		
		//$recordid=intval($this->segment(4));
		
		
	}
	
	
	//编辑
	public function singleupdate(){
		_message("不可编辑!");
		if(isset($_POST['submit'])){
			System::load_sys_class('upload','sys','no');
			if($_POST['title']==null)_message("标题不能为空");	
			if($_POST['content']==null)_message("内容不能为空");				
			$sd_id=$_POST['sd_id'];
			$shaidan=$this->db->GetOne("select * from `@#_shaidan` where `sd_id`='$sd_id'");			
			$pic=null;$thumbs=null;
			if(isset($_POST['fileurl_tmp'])){
				if($shaidan['sd_photolist']==null){				
					$img=$_POST['fileurl_tmp'];
					$num=count($img);
					for($i=0;$i<$num;$i++){
						$pic.=trim($img[$i]).";";
					}
					$src=trim($img[0]);
					$size=getimagesize(G_UPLOAD_PATH."/".$src);
					$width=220;
					$height=$size[1]*($width/$size[0]);			
					$thumbs=tubimg($src,$width,$height);
				}else{
					$img=$_POST['fileurl_tmp'];
					$num=count($img);
					for($i=0;$i<$num;$i++){
						$pic.=$img[$i].";";
					}
				}
			}
			if($thumbs!=null){
				$sd_thumbs=$thumbs;
			}else{
				$sd_thumbs=$shaidan['sd_thumbs'];
			}
			$uid=$this->userinfo;
			$sd_userid=$uid['uid'];
			$sd_shopid=$shaidan['sd_shopid'];
			$sd_title=$_POST['title'];
			$sd_content=$_POST['content'];
			$sd_photolist=$pic.$shaidan['sd_photolist'];
			$sd_time=time();			
			$this->db->Query("UPDATE `@#_shaidan` SET
			`sd_userid`='$sd_userid',
			`sd_shopid`='$sd_shopid',
			`sd_title`='$sd_title',
			`sd_thumbs`='$sd_thumbs',
			`sd_content`='$sd_content',
			`sd_photolist`='$sd_photolist',
			`sd_time`='$sd_time' where sd_id='$sd_id'");
			_message("晒单修改成功",WEB_PATH."/member/home/singlelist");
		}
		$member=$this->userinfo;
		$title="修改晒单 - "._cfg("web_name");	
		$uid=_getcookie('uid');
		$ushell=_getcookie('ushell');
		$sd_id=intval($this->segment(4));
		if($sd_id>0){
			$shaidan=$this->db->GetOne("select * from `@#_shaidan` where `sd_id`='$sd_id'");
			include templates("member","singleupdate");
		}else{
			_message("页面错误");
		}	
	}
	public function singoldimg(){
		if($_POST['action']=='del'){
			$sd_id=$_POST['sd_id'];
			$sd_id = abs(intval($sd_id));
			$oldimg=$_POST['oldimg'];
			$shaidan=$this->db->GetOne("select * from `@#_shaidan` where `sd_id`='$sd_id'");
			$sd_photolist=str_replace($oldimg.";","",$shaidan['sd_photolist']);
			$this->db->Query("UPDATE `@#_shaidan` SET sd_photolist='".$sd_photolist."' where sd_id='".$sd_id."'");
		}
	}
	
	//晒单上传
	public function singphotoup(){
		
		if(!empty($_FILES)){
			/*
				更新时间：2014-04-28
				xu
			*/
			/*
			$uid=isset($_POST['uid']) ? $_POST['uid'] : NULL;		
			$ushell=isset($_POST['ushell']) ? $_POST['ushell'] : NULL;
			$login=$this->checkuser($uid,$ushell);
			if(!$login){echo "上传失败";exit;}
			
			*/
			System::load_sys_class('upload','sys','no');
			upload::upload_config(array('png','jpg','jpeg','gif'),1000000,'shaidan');
			upload::go_upload($_FILES['Filedata']);
			if(!upload::$ok){
				echo _message(upload::$error,null,3);
			}else{
				$img=upload::$filedir."/".upload::$filename;					
				$size=getimagesize(G_UPLOAD_PATH."/shaidan/".$img);
				$max=700;$w=$size[0];$h=$size[1];
				if($w>700){
					$w2=$max;
					$h2=$h*($max/$w);
					upload::thumbs($w2,$h2,1);						
				}					
				echo trim("shaidan/".$img);
			}					
		} 
	}	
	public function singdel(){
		$action=isset($_GET['action']) ? $_GET['action'] : null; 
		$filename=isset($_GET['filename']) ? $_GET['filename'] : null;
		if($action=='del' && !empty($filename)){
			$filename=G_UPLOAD_PATH.'shaidan/'.$filename;			
			$size=getimagesize($filename);			
			$filetype=explode('/',$size['mime']);			
			if($filetype[0]!='image'){
				return false;
				exit;
			}
			unlink($filename);
			exit;
		}
	}
	//晒单删除
	public function shaidandel(){
		_message("已添加的晒单不可删除!");
		$member=$this->userinfo;
		//$id=isset($_GET['id']) ? $_GET['id'] : "";
		$id=$this->segment(4);
		$id=intval($id);
		$shaidan=$this->db->Getone("select * from `@#_shaidan` where `sd_userid`='$member[uid]' and `sd_id`='$id'");
		if($shaidan){
			$this->db->Query("DELETE FROM `@#_shaidan` WHERE `sd_userid`='$member[uid]' and `sd_id`='$id'");
			_message("删除成功",WEB_PATH."/member/home/singlelist");
		}else{
			_message("删除失败",WEB_PATH."/member/home/singlelist");
		}
	}
	
			//邀请好友
	public function invitefriends(){
		$mysql_model=System::load_sys_class('model');
		$member=$this->userinfo;		 
		$uid=_getcookie('uid');
		$notinvolvednum=0;  //未参加夺宝的人数
		$involvednum=0;     //参加预购的人数
		$involvedtotal=0;   //邀请人数		 
		
       
        //查询邀请好友信息		
		$invifriends=$mysql_model->GetList("select * from `@#_member` where `yaoqing`='$member[uid]' ORDER BY `time` DESC");	
		$involvedtotal=count($invifriends);
		
            
		//var_dump($invifriends);
                		
		for($i=0;$i<count($invifriends);$i++){
		   $sqluid=$invifriends[$i]['uid'];
		   $sqname=get_user_name($invifriends[$i]); 
		   $invifriends[$i]['sqlname']=	 $sqname;  
		   
		   //查询邀请好友的消费明细		   
		   $accounts[$sqluid]=$mysql_model->GetList("select * from `@#_member_account` where `uid`='$sqluid'  ORDER BY `time` DESC");	
          
		
		//判断哪个好友有消费		
		 if(empty($accounts[$sqluid])){
		    $notinvolvednum +=1;
		    $records[$sqluid]='未参与夺宝';
		 }else{
		    $involvednum +=1;
		    $records[$sqluid]='已参与夺宝';
		 }
		
		
		}  
        		
		include templates("member","invitefriends");
	}
	
		//佣金明细
	public function commissions(){
		$mysql_model=System::load_sys_class('model');
		$member=$this->userinfo;
		$uid = $member['uid'];
		$recodetotal=0;   // 判断是否为空
		$shourutotal=0;
		$zhichutotal=0;
		
		$invifriends=$mysql_model->GetList("select * from `@#_member` where `yaoqing`='$member[uid]' ORDER BY `time` DESC");
		
		 
		  //查询佣金表
		  for($i=0;$i<count($invifriends);$i++){
			   $sqluid=$invifriends[$i]['uid'];
			   
			   //查询邀请好友给我反馈的佣金  
			   $recodes[$sqluid]=$mysql_model->GetList("select * from `@#_member_recodes` where `uid`='$sqluid' and `type`=1 ORDER BY `time` DESC");
			   
			   $user[$sqluid]['username']=	get_user_name($invifriends[$i]);	   
				 
			}
		  //自己提现或充值
		  $recodes[$uid]=$mysql_model->GetList("select * from `@#_member_recodes` where `uid`='$uid' and `type`!=1 ORDER BY `time` DESC");
		  $user[$uid]['username']= get_user_name($member);
		  
		  
         $recodearr='';
		 $i=0;	
		 if(!empty($recodes)){
		 foreach($recodes as $key=>$val){
			$username[$key]=$user[$key]['username'];
			foreach($val as $key2=>$val2){
			  $recodearr[$i]=$val2;		  
			  $i++;  
			} 
		 }
		 }
		 
		 $recodetotal=count($recodes);
		 
		 
		 //查询   累计收入：元    累计(提现/充值)：元    佣金余额：元
		 
		 if(!empty($recodes)){
			 foreach($recodes as $key=>$val){
			  if($uid==$key){
			     foreach($val as $key2=>$val2){  		   
					 
					$zhichutotal+=$val2['money'];	 //总佣金支出		 
					 
				   }
			    }else{
				    foreach($val as $key3=>$val3){  		   
					 
					$shourutotal+=$val3['money'];	 //总佣金收入		 
				   }
				
				}		
			  }
			     
		  }		  
		
		  
		 $total=$shourutotal-$zhichutotal;  //计算佣金余额	 
		 			 
		include templates("member","commissions");
	}
	
	//申请提现
	public function cashout(){
		$mysql_model=System::load_sys_class('model');
		$member=$this->userinfo;
		$uid = $member['uid'];
		$total=0;
		$shourutotal=0;
		$zhichutotal=0;
		$cashoutdjtotal=0;
		$cashouthdtotal=0;
        //查询邀请好友id
    	$invifriends=$mysql_model->GetList("select * from `@#_member` where `yaoqing`='$member[uid]' ORDER BY `time` DESC");

		//查询佣金收入
		for($i=0;$i<count($invifriends);$i++){
			   $sqluid=$invifriends[$i]['uid'];			   
			   //查询邀请好友给我反馈的佣金  
			   $recodes[$sqluid]=$mysql_model->GetList("select * from `@#_member_recodes` where `uid`='$sqluid' and `type`=1 ORDER BY `time` DESC");			 
		}
		
		//查询佣金消费(提现,充值)	
		$zhichu=$mysql_model->GetList("select * from `@#_member_recodes` where `uid`='$uid' and `type`!=1 ORDER BY `time` DESC");	
		  
		//查询被冻结金额		  
		$cashoutdj=$mysql_model->GetOne("select SUM(money) as summoney  from `@#_member_cashout` where `uid`='$uid' and `auditstatus`!='1' ORDER BY `time` DESC");	
		
		 if(!empty($recodes)){
			 foreach($recodes as $key=>$val){
			    foreach($val as $key2=>$val2){					 
					$shourutotal+=$val2['money'];	 //总佣金收入	 
				}
			 }		 
		 }		  
		 if(!empty($zhichu)){
			foreach($zhichu as $key=>$val3){  	   
				$zhichutotal+=$val3['money'];	//总支出的佣金		  
			}
		 }  
	

		$total=$shourutotal-$zhichutotal;  //计算佣金余额
		$cashoutdjtotal= $cashoutdj['summoney'];  //冻结佣金余额
		$cashouthdtotal= $total-$cashoutdj['summoney'];  //活动佣金余额


       if(isset($_POST['submit1'])){ //提现	     
		 $money      = abs(intval($_POST['money']));
		 $username   =htmlspecialchars($_POST['txtUserName']);
		 $bankname   =htmlspecialchars($_POST['txtBankName']);
		 $branch     =htmlspecialchars($_POST['txtSubBank']);
		 $banknumber =htmlspecialchars($_POST['txtBankNo']);
		 $linkphone  =htmlspecialchars($_POST['txtPhone']);
		 $time       =time();
		 $type       = -3;  //收取1/消费-1/充值-2/提现-3
		 
		 if($total<100){
		     _message("佣金金额大于100元才能提现！");exit;
		 }elseif($cashouthdtotal<$money){
		    _message("输入额超出活动佣金金额！");exit;
		 }elseif($total<$money ){  
		     _message("输入额超出总佣金金额！");exit;
		 }else{
		 
		 //插入提现申请表  这里不用在佣金表中插入记录 等后台审核才插入
		 $this->db->Query("INSERT INTO `@#_member_cashout`(`uid`,`money`,`username`,`bankname`,`branch`,`banknumber`,`linkphone`,`time`)VALUES
			('$uid','$money','$username','$bankname','$branch','$banknumber','$linkphone','$time')"); 
			_message("申请成功！请等待审核！");
		 }	   
	   }
	   
	   if(isset($_POST['submit2'])){//充值			
		  $money      = abs(intval($_POST['txtCZMoney']));		
		  $type       = 1;
		  $pay        ="佣金";
		  $time       =time();
		  $content    ="使用佣金充值到夺宝账户";
		  
		 if($money <= 0 || $money > $total){
			  _message("佣金金额输入不正确！");exit;
		 }	
		 if($cashouthdtotal<$money){
		    _message("输入额超出活动佣金金额！");exit;
         }			
		  
		  //插入记录
		 $account=$this->db->Query("INSERT INTO `@#_member_account`(`uid`,`type`,`pay`,`content`,`money`,`time`)VALUES
			('$uid','$type','$pay','$content','$money','$time')");
		 
		 // 查询是否有该记录
		 if($account){		    
			 //修改剩余金额
			 $leavemoney=$member['money']+$money;			 
			 $mrecode=$this->db->Query("UPDATE `@#_member` SET `money`='$leavemoney' WHERE `uid`='$uid' ");			 
			 //在佣金表中插入记录		 
		     $recode=$this->db->Query("INSERT INTO `@#_member_recodes`(`uid`,`type`,`content`,`money`,`time`)VALUES
			('$uid','-2','$content','$money','$time')");
			_message("充值成功！");
		 }else{
		     _message("充值失败！");
		 }	
	   }
		include templates("member","cashout");
	}
	
	/*确定收获地址*/
	public function confirm_address(){
		$mysql_model=System::load_sys_class('model');
		$member = $this->is_login();
		$uid = $member['uid'];
		$record_id = isset($_POST['record_id']) ? intval($_POST['record_id']) : '';
		$info_address = isset($_POST['info_address']) ? trim($_POST['info_address']) : '';
		$confrim_addr_uinfo = isset($_POST['confrim_addr_uinfo']) ? trim($_POST['confrim_addr_uinfo']) : '';
		$this->is_empty($record_id,"记录");
		$this->is_empty($info_address,"用户信息");
		$this->is_empty($info_address,"地址");
		
		$sql = "SELECT confrim_addr,confrim_addr_time FROM `@#_member_go_record` WHERE uid='$uid' AND id='$record_id'";
		$arr_one = $mysql_model->GetOne($sql);

		if($arr_one['confrim_addr_time']){
			$info = array("error"=>1,"msg"=>'该商品你已确认收货地址，不能重复确认!');
			die(json_encode($info));
		}
		
		$time = time();
		
		$sql_r = "UPDATE `@#_member_go_record` SET confrim_addr='$info_address',confrim_addr_time='$time',confrim_addr_uinfo='$confrim_addr_uinfo' WHERE uid='$uid' AND id='$record_id' ";
		$affect_id = $mysql_model->Query($sql_r);
		if($affect_id){
			$info = array("error"=>0,"msg"=>'确认成功，我们会尽快给您发货!');
		}else{
			$info = array("error"=>1,"msg"=>'确认失败!');
		}
		die(json_encode($info));
		
	}
	
	
	//提现记录
	public function record(){
		$mysql_model=System::load_sys_class('model');
		$member=$this->userinfo;
		$uid = $member['uid'];
		$recount=0;
		$fufen = System::load_app_config("user_fufen",'','member');
		//查询提现记录	 
		//$recordarr=$mysql_model->GetList("select * from `@#_member_recodes` a left join `@#_member_cashout` b on a.cashoutid=b.id where a.`uid`='$uid' and a.`type`='-3' ORDER BY a.`time` DESC");		$recordarr=
		
		$recordarr=$mysql_model->GetList("select * from  `@#_member_cashout`  where `uid`='$uid' ORDER BY `time` DESC limit 0,30");
        
		if(!empty($recordarr)){
		  $recount=1;
		}		
		include templates("member","record");
	}
	//qq绑定
	public function qqclock(){
		$member=$this->userinfo;
		include templates("member","qqclock");
	}
	
	//2015/12/09/判断是否登录
	private function is_login(){
		$info = array();
		if(!$member=$this->userinfo){
			$info = array('error'=>1,'msg'=>'您还没有登录!','url'=>WEB_PATH.'islogin=0');
			die(json_encode($info));
		}else{
			return $member;
		}
		
	}
	
	//2015/12/09/判断信息是否存在
	private function is_empty($str,$msg=''){
		$info = array();
		if($str == ''){
			$info = array('error'=>1,'msg'=>$msg.'不能为空');
			die(json_encode($info));
		}
	}
	
	//ajax修改用户名
	public function ajaxupdate_username(){
		$member = $this->is_login();
		$username = isset($_POST['username']) ? trim($_POST['username']) : '';
		$this->is_empty($username,'用户名');
		$info = array();
		$sql = "UPDATE `@#_member` SET username='$username' WHERE uid='$member[uid]'";
		
		$account = $this->db->Query($sql);
		if(!$account){
			$info = array('error'=>1,'msg'=>'*修改失败','sql'=>$sql);
		}else{
			$info = array('error'=>0,'msg'=>'*修改成功','username'=>$username);
		}
		die(json_encode($info));
	}
	
	//绑定手机号码页面   20160106
	public function bind_mobile()
	{
	    $uid=intval(_encrypt(_getcookie("uid"),'DECODE'));
	    $member=$this->userinfo;
	    if($uid && !empty($member['mobile'])){
	        header("location:" . WEB_PATH . "/member/home/userbuylist");
	    }
	    $title = "请绑定手机号码 - " . _cfg('web_name');
	    include templates("member","bind_mobile");
	}
	
	//邀请好友页面
	public function friends()
	{
	    $current_status = $this->segment(3);  //判断当前是哪个控制下
	    $title = "邀请好友 - " . _cfg('web_name');
	    $member=$this->userinfo;
	    $uid=_getcookie('uid');
	    $tg_uid = _encrypt($uid,'DECODE');
	    
	    $db = System::load_sys_class('model');
	    $page=System::load_sys_class('page');
	    
	    $total_jifen = $this->get_yaoqing_jifen($tg_uid);
	    
	    $total = $db->GetCount("select * from `@#_member` where `yaoqing`='$tg_uid' ORDER BY `time` DESC");
	    
	    if($total > 0){
	    
    	    if(isset($_GET['p'])){$pagenum=$_GET['p'];}else{$pagenum=1;}
    	    $page->config($total,10,$pagenum,"0");
    	    
    	    $sql = "select * from `@#_member` where `yaoqing`='$tg_uid' ORDER BY `time` DESC";
    	    
    	    $list = $this->db->GetPage($sql,array("num"=>10,"page"=>$pagenum,"type"=>1,"cache"=>0));
    	    foreach($list as $k => $v){
    	       $list[$k]['time'] = date('Y-m-d H:i:s',$v['time']);
    	       //$list[$k]['username'] = substr_replace($v['mobile'],'****',3,4);
    	       
    	       $list[$k]['jifen'] = $this->get_yaoqing_jifen($v['yaoqing'],$v['uid']);   
    	    }

	    }
		$sql = "select * from `@#_member` where uid='{$member['yaoqing']}'";
	    $yaoqing_user = $this->db->GetOne($sql);
	    include templates("member","friends");
	}
	
	//获取某个好友带来的积分
	private function get_yaoqing_jifen($yaoqing='',$uid='')
	{
	    $data['total_jifen'] = 0;
	    $db = System::load_sys_class('model');
	    $wh = '';
	    if($uid){
	       $wh = " AND `uid`='$uid' "; 
	    }
	    $data = $db->GetOne("SELECT SUM(jifen) as total_jifen FROM @#_member_jifen WHERE `yaoqing`='$yaoqing' $wh ");
	    return intval($data['total_jifen']);
	}
	
}

?>