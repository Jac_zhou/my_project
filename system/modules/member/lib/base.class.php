<?php 

defined('G_IN_SYSTEM')or exit('No permission resources.');
define("MEMBER",true);
System::load_sys_fun("user");
class base extends SystemAction {
	protected $userinfo=NULL;	
	public function __construct(){
		$this->db = System::load_sys_class("model");
		$uid=intval(_encrypt(_getcookie("uid"),'DECODE'));		
		$ushell=_encrypt(_getcookie("ushell"),'DECODE');
		if(!$uid)$this->userinfo=false;
		$this->userinfo=$this->db->GetOne("SELECT * from `@#_member` where `uid` = '$uid'");
		if(!$this->userinfo)$this->userinfo=false;	
		
		//如果没有绑定手机号 就跳转至绑定手机号码页面  20160106
		$acct = $this->segment(3);
		if($uid && !$this->userinfo['mobile'] && $this->userinfo['band'] == 'weixin' && $acct != 'bind_mobile' ){
		    header("location:".WEB_PATH.'/member/home/bind_mobile');
		}
		
		$shell=md5($this->userinfo['uid'].$this->userinfo['password'].$this->userinfo['mobile'].$this->userinfo['email']);		
		if($ushell!=$shell)$this->userinfo=false;
		global $_cfg;		
		$_cfg['userinfo']=$this->userinfo;
	}
	
	protected function checkuser($uid,$ushell){
		$uid=intval(_encrypt($uid,'DECODE'));
		$ushell=_encrypt($ushell,'DECODE');	
		if(!$uid)return false;
		if($ushell===NULL)return false;
		$this->userinfo=$this->db->GetOne("SELECT * from `@#_member` where `uid` = '$uid'");
		if(!$this->userinfo){
			$this->userinfo=false;
			return false;
		}
		$shell=md5($this->userinfo['uid'].$this->userinfo['password'].$this->userinfo['mobile'].$this->userinfo['email']);
		if($ushell!=$shell){
			$this->userinfo=false;
			return false;
		}else{
			return true;
		}
		
	}
	public function get_user_info(){
		if($this->userinfo){
			return $this->userinfo;
		}else{
			return false;
		}
	}
	protected function HeaderLogin(){
		_message("你还未登录，无权限访问该页！",WEB_PATH."/member/user/login");
	}
	
	//统计pv
	protected function set_pv($in_page='')
	{
	    $su = _getcookie('su');
	    $sk = _getcookie('sk');
	    $source = isset($_SERVER['HTTP_REFERER']) ? trim($_SERVER['HTTP_REFERER']) : '';   //上一页面
	    $source = trim($source,',');
	    $ip = _get_ip();
	    $time = time();
	    if($in_page){
	        $page = $in_page;
	    }else{
	        $page = "http://" . $_SERVER['HTTP_HOST'] . ( isset($_SERVER['PHP_SELF']) ? trim($_SERVER['PHP_SELF']) : '' );    //当前页面
	    }
	    $type = _is_mobile() ? 1 : 0;
	    
	    $sql = "INSERT INTO @#_pv(`page`,`source`,`ip`,`su`,`sk`,`time`,`type`) VALUES('$page','$source','$ip','$su','$sk','$time','$type')";
	    $this->db->Query($sql);
	}
	
	//统计uv
	protected function set_uv($in_page='')
	{
	    $su = _getcookie('su');
	    $sk = _getcookie('sk');
	    $source = isset($_SERVER['HTTP_REFERER']) ? trim($_SERVER['HTTP_REFERER']) : '';   //上一页面
	    $source = trim($source,',');
	    $ip = _get_ip();
	    $time = time();
	    if($in_page){
	        $page = $in_page;
	    }else{
	        $page = "http://" . $_SERVER['HTTP_HOST'] . ( isset($_SERVER['PHP_SELF']) ? trim($_SERVER['PHP_SELF']) : '' );    //当前页面
	    }
	    $type = _is_mobile() ? 1 : 0;
	    
	    $today_start = strtotime(date('Y-m-d').' 00:00:00');
	    $today_end = strtotime(date('Y-m-d').' 23:59:59');
	    $is_count = $this->db->GetCount("SELECT id FROM @#_uv WHERE `ip`='$ip' AND `page`='$page' AND `su`='$su' AND `time`>='$today_start' AND `time`<= '$today_end' ");
	    
	    if(!$is_count){
    	    $sql = "INSERT INTO @#_uv(`page`,`source`,`ip`,`su`,`sk`,`time`,`type`) VALUES('$page','$source','$ip','$su','$sk','$time','$type')";
    	    $this->db->Query($sql);
	    }
	}
}
?>