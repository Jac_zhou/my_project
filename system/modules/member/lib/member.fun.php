<?php 


/*
*	取得用户的收货地址
*	@uid  用户ID
*	@key  返回类型, bool 真假值,array 返回地址数组
*/
function member_get_dizhi($uid='',$key='bool'){
	$uid = abs(intval($uid));
	if(!$uid)return false;
	$db = System::load_sys_class("model");
	$info = $db->GetOne("SELECT * FROM `@#_member_dizhi` WHERE `uid` = '$uid' and `default` = 'Y'");
	if($info){
		return $info;
	}else{
		return false;
	}
}

/************ 插入好友关系 start  2015/12/16  不要随意改动,关系到web注册、APP接口调用 **************/
function do_insert_friends($uid='', $yaoqing='', $lev=1)
{
	$uid = intval($uid);
	$yaoqing = intval($yaoqing);
	$lev = intval($lev);
	$db = System::load_sys_class("model");
	if($uid && $yaoqing && $lev && $lev<=3){
		$addtime = time();
		$lev_info = $db->GetOne("SELECT uid,yaoqing FROM `@#_member` WHERE `uid`='$yaoqing' LIMIT 1 ");
		if( isset($lev_info['uid']) && intval($lev_info['uid']) > 0 ){
			$sql = " INSERT INTO `@#_member_friendship`(uid,yaoqing,lev,addtime) VALUES('$uid','$lev_info[uid]','$lev','$addtime')";
			$db->Query($sql);
			$lev++;
			do_insert_friends($uid, $lev_info['yaoqing'], $lev);
		}
	}
}
/************ 插入好友关系 end  2015/12/16 *********************************************************/


/*
取得用户的收货地址
*/
function getuser_addresslist($uid){
	$sql = "SELECT * FROM `@#_member_dizhi` WHERE uid='$uid' ORDER BY `default` DESC,time DESC LIMIT 5";
	$db = System::load_sys_class("model");
	$res = $db->GetList($sql);
	return $res;
}


/*注册则送5元现金*/
function send_ticket($uid,$mobile){
	$start_time = strtotime("2016-1-1 00:00:01");
	$end_time = strtotime("2016-1-31 23:59:59");
	$time = time();
	if($time<$start_time || $time>$end_time){
		return true;
	}
	
	$db = System::load_sys_class("model");
	$sql = "SELECT * FROM `@#_active` WHERE mobile='$mobile'";
	$res = $db->GetOne($sql);
	if(!$res){
		$sql_s = "INSERT INTO `@#_active`(uid,type,cash_value,is_active,r_time,mobile,is_use) VALUES('$uid',1,5,0,'$time','$mobile',0)";
		$db->Query($sql_s);
	}else{
		return true;
	}	
}

/*活动冲值100,送8%，冲200，送9%，300,11%，400,14%，500,20%,大于15，激活，奖券，*/
function active_cash($uid,$money,$mobile){
	if(!$uid){return true;}
	$start_time = strtotime("2016-1-1 00:00:01");
	$end_time = strtotime("2016-1-31 23:59:59");
	$time = time();
	if($time<$start_time || $time>$end_time){
		return true;
	}
	$money = trim($money);
	$moeny_count = 0;
	$db = System::load_sys_class("model");
	if($money > 15){
		switch($money){
			case 100:
				if(is_song($uid,'100')=="you"){
					return true;
				}
				$moeny_count = 100*0.08;
			break;
			case 200:
				if(is_song($uid,'200') =="you"){
					return true;
				}
				$moeny_count = 200*0.09;
			break;
			case 300:
				if(is_song($uid,'300')=="you"){
					return true;
				}
				$moeny_count = 300*0.11;
			break;
			case 400:
				if(is_song($uid,'400')=="you"){
					return true;
				}
				$moeny_count = 400*0.14;
			break;
			case 500:
				if(is_song($uid,'500') =="you"){
					return true;
				}
				$moeny_count = 500*0.2;
			break;
		}
		if($moeny_count <= 0){
		    return true;
		}
		$sql_x = "INSERT INTO `@#_active`(uid,type,cash_value,r_time,mobile,is_use,money) VALUES('$uid',2,'$moeny_count','$time','$mobile',1,'$money')";
		$db->Query($sql_x);
		
		/*给原来的member表加相应的钱*/
		if($db->affected_rows()){
			$sql_m = "UPDATE `@#_member` SET money=money+".$moeny_count." WHERE uid='$uid'";
			$db->Query($sql_m);
			
			$sql_member_f = "INSERT INTO `@#_member_addmoney_record`(uid,money,pay_type,status,time) VALUES('$uid','$moeny_count','充值送红包','已付款','$time')";
			return $db->Query($sql_member_f);
		}
		
	}elseif($money==15){
		/*激活红包*/
		jihuo_hong($uid);

	}else{
		return true;
	}
}

/*活动期间给用户晒一单，添加2块钱*/
function shaidan_get_money($uid){
	$start_time = strtotime("2016-1-1 00:00:01");
	$end_time = strtotime("2016-1-31 23:59:59");
	$time = time();
	if($time<$start_time || $time>$end_time){
		return true;
	}
	$db = System::load_sys_class("model");
	$arr_mobile = getchongzhi_user($uid);
	
	$sql_x  = "INSERT INTO `@#_active`(uid,type,cash_value,r_time,mobile,is_use,money) VALUES('$uid',2,2,'$time','$arr_mobile[mobile]',1,0)";
	$db->Query($sql_x);
	if($db->affected_rows()){
		$sql = "UPDATE `@#_member` SET money=money+2 WHERE uid='$uid'";
		$db->Query($sql);
		
		$sql_member_f = "INSERT INTO `@#_member_addmoney_record`(uid,money,pay_type,status,time) VALUES('$uid',2,'分享送红包','已付款','$time')";
		return $db->Query($sql_member_f);
	}
}
/*激活现金券*/
function jihuo_hong($uid){
	if(!$uid){
		return true;
	}
	$time = time();
	$db = System::load_sys_class("model");
	$sql = "SELECT * FROM `@#_active` WHERE type='1' AND uid='$uid'";
	$res  = $db->GetOne($sql);
	if($res){
		if($res['is_active'] == '1'){
			return true;
		}else{
			$sql_x = "UPDATE `@#_active` SET is_active=1,is_use=1 WHERE uid='$uid'";
			$db->Query($sql_x);	
			$sql_u = "UPDATE `@#_member` SET money=money+5 WHERE uid='$uid'";
			$db->Query($sql_u);
			
			$sql_member_f = "INSERT INTO `@#_member_addmoney_record`(uid,money,pay_type,status,time) VALUES('$uid',5,'激活送奖金券','已付款','$time')";
			return $db->Query($sql_member_f);
		}
	}else{
		return true;
	}
}

/*判断该用户在钱段上是否已送过*/
function is_song($uid,$money){
	if(!$uid || !$money){
		return true;
	}
	$money = intval($money);
	$sql = "SELECT id FROM `@#_active` WHERE uid='$uid' AND type=2 AND money='$money'";
	$db = System::load_sys_class("model");
	$res = $db->GetOne($sql);
	if($res){
		return "you";
	}
}
/*查询用户信息*/
function getchongzhi_user($uid){
	$sql = "SELECT mobile FROM `@#_member` WHERE uid='$uid'";
	$db = System::load_sys_class("model");
	$res = $db->GetOne($sql);
	return $res;
}

/*判断该用户是否晒单*/
function is_shaidan($uid,$shopid){
	$sql = "SELECT sd_id FROM `@#_shaidan` WHERE sd_userid='$uid' AND sd_shopid='$shopid'";
	$db = System::load_sys_class("model");
	$res = $db->GetOne($sql);
	return $res;	
}
/*
*	根据区段时间来查询用户注册
*	@start_time  开始时间
	@end_time  结束时间
*	@add_type  'add_new'是注册，'add_cost'是消费
*/

function divio_time_user($start_time,$end_time,$add_type){
	$str = '';
	$start_time = strtotime($start_time);
	$end_time = $end_time == '' ? time() : strtotime($end_time);
	if($start_time > $end_time){
		_message('开始时间不能大于结束时间');
	}
	$add_add = $add_type == '' ? 'add_new' : $add_type;
	if($start_time == ''){
		$str = "`time` <= '$end_time'";
	}else{
		$str = "`time` >= '$start_time' AND `time` <= '$end_time'";
	}
		
	if($add_add == 'add_cost'){
		$uids = '';
		$db = System::load_sys_class("model");
		$conutc = $db->GetList("SELECT uid FROM `@#_member_go_record` WHERE $str");				
		foreach($conutc as $c){
			$uids .= "'".$c['uid']."',";
		}
		$uids = trim($uids,",");
		if(!empty($uids)){
			$str = "`uid` in($uids)";
		}else{
			$str = "`uid` in('0')";
		}
	}
	return $str;
}
