<?php 

defined('G_IN_SYSTEM')or exit('No permission resources.');
System::load_app_class('base',null,'no');
System::load_app_fun('user','go');
System::load_app_fun('member');		//方便调用 do_insert_friends
System::load_app_fun('my','go');
System::load_sys_fun('send');
class user extends base {
	public function __construct(){	
		
		parent::__construct();
		$this->db = System::load_sys_class("model");
		
		/* 数据统计 20160108 start */
		 set_su_sk();
		/* 数据统计 20160108 end */
	}

	public function cook_end(){
		_setcookie("uid","",time()-3600);
		_setcookie("ushell","",time()-3600);		
		_message("退出成功",WEB_PATH);
	}
	/**
	 * @param  G_HTTP_REFERER 上一个页面的URL  （常量）
	 * 	
	 * get_web_url();获取当前的URl
	 * 
	 */
	
	public function login(){	
	    
	    //如果是手机访问
	    if(_is_mobile()){
	        header("location:".G_WEB_PATH.'/yungou/index.html#/tab/login');
	    }
				
		$user = $this->userinfo;
		
		if($user){
			header("Location:".G_WEB_PATH);exit;
		}else if(!$this->segment(4)){			
			global $_cfg;				
			$url = WEB_PATH.'/'.$_cfg['param_arr']['url'];			
			$url = rtrim($url,'/');	
			$url .= '/'.base64_encode(trim(G_HTTP_REFERER));
			if($url != get_web_url()){
					header("Location:".$url);exit;
			}
		}
		
		//判断验证码显示
		$arr_yan = System::load_app_config("user_reg_type","",ROUTE_M);
		$is_yan = $arr_yan['is_yan'];
			
		
		/* 统计pv,uv 20160109 start */
		$this->set_pv();
		$this->set_uv();
		/* 统计pv,uv 20160109 end */
		
		include templates("user","login");
		
	}
	public function register(){
	    $su_val = _getcookie('su');
	    $sk_val = _getcookie('sk');
        //邀请码
        $yaoqing_code = $this->segment(4);
        //邀请人的uid
        $yaoqing_uid = intval(_encrypt($yaoqing_code,'DECODE'));
        //如果是手机访问
        if(is_mobile_request()){
            header("location:".G_WEB_PATH.'/yungou/index.html#/tab/rigister?yaoqing='.$yaoqing_uid);
            die;
        }
        //来源
        $come = isset($_GET['come']) ? trim($_GET['come']): '';        	

		if($this->userinfo){
			header("Location:".WEB_PATH."/member/home/");exit;
		}
		$title="注册"._cfg("web_name");
		/* 统计pv,uv 20160109 start */
		$this->set_pv();
		$this->set_uv();
		/* 统计pv,uv 20160109 end */
		
		include templates("user","register");
	}
	
	/* 用户注册邮箱注册验证邮件发送 */
	public function emailcheck(){
	
		$title="邮箱验证 -"._cfg("web_name");
		$check_code = _encrypt($this->segment(4),"DECODE");
		$check_code = @unserialize($check_code);
		
		if(!$check_code || !isset($check_code['name']) || !isset($check_code['time'])){
			_message("参数不正确或者验证已过期!",WEB_PATH.'/register');
		}				
				
		$info=$this->db->GetOne("SELECT * FROM `@#_member` WHERE `reg_key` = '$check_code[name]' and `time` = '$check_code[time]' LIMIT 1");
		if(!$info)_message("错误的来源!",WEB_PATH.'/register');
		$emailurl = explode("@",$info['reg_key']);
		$name = $info['reg_key'];
		$enname = $this->segment(4);
		
		$reg_message = '';
		if($info['emailcode']=='1')_message("恭喜您,验证成功!",WEB_PATH."/login");
		if($info['emailcode']=='-1'){	
			$reg_message = send_email_reg($info['reg_key'],$info['uid']);
		}elseif((time() - $check_code['time']) > 3600){
			//未验证时间大于1小时 重发邮件
			$reg_message = send_email_reg($info['reg_key'],$info['uid']);
		}
		
		include templates("user","emailcheck");
	}
	
	
	/*
	*	重发验证邮件
	*/
	public function sendemail(){
		$check_code = _encrypt($this->segment(4),"DECODE");
		$check_code = @unserialize($check_code);
		if(!$check_code || !isset($check_code['name']) || !isset($check_code['time'])){
			_message("参数不正确或者验证已过期1!",WEB_PATH.'/register');
		}		
		$member=$this->db->GetOne("SELECT * FROM `@#_member` WHERE `reg_key` = '$check_code[name]' and `time` = '$check_code[time]' LIMIT 1");
		if(!$member)_message("错误的来源!",WEB_PATH.'/register');
		
		if($member['emailcode']=='1')_message("邮箱已验证",WEB_PATH.'/member/home');
		$this->db->Query("UPDATE `@#_member` SET emailcode='-1' where `uid`='$member[uid]'");
		_message("正在重新发送...",WEB_PATH."/member/user/emailcheck/".$this->segment(4));	
		exit;
	}
	
	/*
		邮箱验证成功页面
	*/
	public function emailok(){	
	
		$check_code = _encrypt($this->segment(4),"DECODE");
		$check_code = @unserialize($check_code);
		
		
		if(!isset($check_code['email']) || !isset($check_code['code']) || !isset($check_code['time'])){
			_message("未知的来源!",WEB_PATH,'/register');
		}	
		$sql_code = $check_code['code'].'|'.$check_code['time'];
		
		$member=$this->db->GetOne("select * from `@#_member` where `reg_key`='$check_code[email]' AND `emailcode`= '$sql_code' LIMIT 1");
		if(!$member)_message("未知的来源!",WEB_PATH,'/register');
		
		$timec=time() - $check_code['time'];		
		if($timec < (3600*24)){	
				$title="邮件激活成功";
				$tiebu="完成注册";
				$success="邮件激活成功";					
				$fili_cfg = System::load_app_config("user_fufen");		
				if($member['yaoqing']){
							$time = time();
							$yaoqinguid = $member['yaoqing'];
							//福分			
							if($fili_cfg['f_visituser']){							
								$this->db->Query("insert into `@#_member_account` (`uid`,`type`,`pay`,`content`,`money`,`time`) values ('$yaoqinguid','1','福分','邀请好友奖励','$fili_cfg[f_visituser]','$time')");
							}						
							$this->db->Query("UPDATE `@#_member` SET `score`=`score`+'$fili_cfg[f_visituser]',`jingyan`=`jingyan`+'$fili_cfg[z_visituser]' where uid='$yaoqinguid'");
				}
				$this->db->Query("UPDATE `@#_member` SET emailcode='1',email = '$member[reg_key]' where `uid`='$member[uid]'");				
				
				//_setcookie("uid",_encrypt($member['uid']),60*60*24*7);	
				//_setcookie("ushell",_encrypt(md5($member['uid'].$member['password'].$member['mobile'].$member['reg_key'])),60*60*24*7);	
				
				include templates("user","emailok");
				
			}else{
					$title="邮箱验证失败";
					$tiebu="验证失败,请重发验证邮件";
					$guoqi="对不起，验证码已过期或不正确！";
					$this->db->Query("UPDATE `@#_member` SET emailcode='-1' where `uid`='$member[uid]'");					
					$name = array("name"=>$member['reg_key'],"time"=>$member['time']);
					$name = _encrypt(serialize($name),"ENCODE");
					include templates("user","emailok");
			}			
	}
	
	//重发手机验证码
	public function sendmobile(){
			$check_code = _encrypt($this->segment(4),"DECODE");
			$check_code = @unserialize($check_code);
			if(!$check_code || !isset($check_code['name']) || !isset($check_code['time'])){
				_message("参数不正确或者验证已过期!",WEB_PATH.'/register');
			}	
			$name = $check_code['name'];
		
			$member=$this->db->GetOne("SELECT * FROM `@#_member` WHERE `reg_key` = '$check_code[name]' and `time` = '$check_code[time]' LIMIT 1");
			if(!$member)_message("参数不正确!");
			if($member['mobilecode']=='1'){_message("该账号验证成功,请直接登录！",WEB_PATH."/login");}	
			$checkcode=explode("|",$member['mobilecode']);
			$times=time()-$checkcode[1];		
			if($times > 120){
				$sendok = send_mobile_reg_code($member['reg_key'],$member['uid']);			
				if($sendok[0]!=1){
					_message("短信发送失败,代码:".$sendok[1]);exit;			
				}
				_message("正在重新发送...",WEB_PATH."/member/user/mobilecheck/".$this->segment(4));				
			}else{
				_message("重发时间间隔不能小于2分钟!",WEB_PATH."/member/user/mobilecheck/".$this->segment(4));
			}
		
	}
	public function mobilecheck(){
		$title="手机认证 - "._cfg("web_name");	
		$check_code = _encrypt($this->segment(4),"DECODE");
		$check_code = @unserialize($check_code);
		if(!$check_code || !isset($check_code['name']) || !isset($check_code['time'])){
			_message("参数不正确或者验证已过期!",WEB_PATH.'/register');
		}	
		$name = $check_code['name'];
		$member=$this->db->GetOne("SELECT * FROM `@#_member` WHERE `reg_key` = '$check_code[name]' and `time` = '$check_code[time]' LIMIT 1");
		if(!$member)_message("未知的来源!",WEB_PATH.'/register');		
		if($member['mobilecode'] == '1'){
			_message("该账号验证成功",WEB_PATH."/login");
		}
		
		if($member['mobilecode'] == '-1'){
			$sendok = send_mobile_reg_code($member['reg_key'],$member['uid']);		
			if($sendok[0]!=1){
					_message($sendok[1]);
			}
			header("location:".WEB_PATH."/member/user/mobilecheck/".$this->segment(4));
			exit;
		}
		
		$action = isset($_POST['submit']) ? trim($_POST['submit']) : '';
		if($action){
			$checkcodes=isset($_POST['checkcode']) ? trim($_POST['checkcode']) : '';
			if($checkcodes == ''){
				$arr = array('error'=>1,'msg'=>'验证码不能为空!');
				die(json_encode($arr));
			}
			
				
			//if(strlen($checkcodes)!=6)_message("验证码输入不正确!");
			$usercode=explode("|",$member['mobilecode']);
			if($checkcodes!=$usercode[0]){
				$arr = array('error'=>1,'msg'=>'验证码输入不正确!');
				die(json_encode($arr));
				
			}

			$fili_cfg = System::load_app_config("user_fufen");
			if($member['yaoqing']){
				$time = time();
				$yaoqinguid = $member['yaoqing'];
				//福分、经验添加
				if($fili_cfg['f_visituser']){
					$this->db->Query("insert into `@#_member_account` (`uid`,`type`,`pay`,`content`,`money`,`time`) values ('$yaoqinguid','1','福分','邀请好友奖励','$fili_cfg[f_visituser]','$time')");
				}				
				$this->db->Query("UPDATE `@#_member` SET `score`=`score`+'$fili_cfg[f_visituser]',`jingyan`=`jingyan`+'$fili_cfg[z_visituser]' where uid='$yaoqinguid'");
			}			
			$check = $this->db->Query("UPDATE `@#_member` SET mobilecode='1',mobile='$member[reg_key]' where `uid`='$member[uid]'");

            //2014-11-26  lq 手机注册时，自动加福分和经验
            $config = System::load_app_config("user_fufen");//福分/经验
            $time=time();
            $this->db->Query("insert into `@#_member_account` (`uid`,`type`,`pay`,`content`,`money`,`time`) values ('$member[uid]','1','福分','手机认证完善奖励','$config[f_phonecode]','$time')");
            $this->db->Query("insert into `@#_member_account` (`uid`,`type`,`pay`,`content`,`money`,`time`) values ('$member[uid]','1','经验','手机认证完善奖励','$config[z_phonecode]','$time')");
            $this->db->Query("UPDATE `@#_member` SET `score`=`score`+'$config[f_phonecode]',`jingyan`=`jingyan`+'$config[z_phonecode]' where uid='".$member['uid']."'");

			/*注册即送5元奖金券*/
			send_ticket($member['uid'],$member['reg_key']);
			
			
			//_setcookie("uid",_encrypt($member['uid']),60*60*24*7);	
			//_setcookie("ushell",_encrypt(md5($member['uid'].$member['password'].$member['reg_key'].$member['email'])),60*60*24*7);	
			
			
			$arr = array('error'=>0,'msg'=>'恭喜您注册成功,立即登录!',"url"=>WEB_PATH."/login");
			die(json_encode($arr));
		}
		$enname=substr($name,0,3).'****'.substr($name,7,10);
		$time=120;
		$namestr = $this->segment(4);
		include templates("user","mobilecheck");
	}
	
	//ajax登录
	public function ajax_login(){
		$user = $this->userinfo;
		if($user){
			header("Location:".G_WEB_PATH);exit;
		}
		
		$username = isset($_REQUEST['username']) ? trim($_REQUEST['username']) : '';
		//die(json_encode(array('status'=>1,"message"=>$username)));
		if($username == ''){
			$arr = array('status'=>1,'message'=>'帐号未填写!');
			die(json_encode($arr));
		}
		$password = isset($_REQUEST['password']) ? trim($_REQUEST['password']) : '';
		if($password == ''){
			$arr = array('status'=>2,'message'=>'密码未填写!');
			die(json_encode($arr));
		}
		
		$hidurl = empty($_REQUEST['hidurl']) ? trim($_REQUEST['hidurl']) : G_WEB_PATH ;
		$logintype = '';
		/*if(strpos($username,'@')==false){
			//手机				
			$logintype='mobile';			
		}else{
			//邮箱
			$logintype='email';
		}*/
		if(!_checkmobile($username)){
			$arr = array('status'=>1,'message'=>'账号格式不正确!');
			die(json_encode($arr));
		}
		$logintype='mobile';
		
		$member=$this->db->GetOne("select * from `@#_member` where `$logintype`='$username'");
		if(!$member){
			$arr = array('status'=>1,'message'=>'帐号不存在!');
			die(json_encode($arr));
		}
		
		if(md5($password) != $member['password']){
			$arr = array('status'=>2,'message'=>'密码错误!');
			die(json_encode($arr));
		}
		
		
					
		$check=$logintype.'code';
		if($member[$check] != 1){
			$strcode=_encrypt($member['email']);
			$arr = array('status'=>1,'message'=>'帐号未认证!');
			die(json_encode($arr));
		}	
				
		$time = time();
		$userips = '';
		if(!intval($member['auto_user'])){
			$user_ip = _get_ip_dizhi();
			$userips = " `user_ip` = '$user_ip', ";
		}
		
		$this->db->Query("UPDATE `@#_member` SET  $userips `login_time` = '$time' where `uid` = '$member[uid]'");
		_setcookie("uid",_encrypt($member['uid']),60*60*24*7);			
		_setcookie("ushell",_encrypt(md5($member['uid'].$member['password'].$member['mobile'].$member['email'])),60*60*24*7);
		
		$arr = array("status"=>0,"message"=>"登录成功","url"=>$hidurl);
		die(json_encode($arr));

	}
	
	public function ajax_loginout(){
		$arr = array();
		$xu_id= _setcookie("uid","",time()-3600);
		$xu_ushell = _setcookie("ushell","",time()-3600);		
		if($xu_id == 1 && $xu_ushell ==1){
			$arr = array('status'=>1,"message"=>"退出成功!");
		}else{
			$arr = array('status'=>0,"message"=>"退出失败!");
		}
		die(json_encode($arr));
	}
}

?>