<?php 
defined('G_IN_SYSTEM')or exit('No permission resources.');
System::load_app_class('base','member','no');
System::load_app_fun('user','go');
class cart extends base {
	private $Cartlist;
	public function __construct() {			
		header("Last-Modified: " . gmdate( "D, d M Y H:i:s" ) . "GMT" ); 
		header("Cache-Control: no-cache, must-revalidate" ); 
		header("Pragma:no-cache");
		
		$this->Cartlist = _getcookie('Cartlist');
		$this->db = System::load_sys_class("model");
	}

	//获取购物车的商品信息到头部
	public function cartheader(){
		$Cartlist=json_decode(stripslashes($this->Cartlist),true);	
		$cartModel = System::load_app_model('cartModel');
		
		$data = $cartModel->get_cartlistinfo($Cartlist);
		
		$Cartshopinfo = $data['Cartshopinfo'];
		$shoplist	  = $data['shoplist'];
		$MoenyCount   = $data['MoenyCount'];
		$shoplen	  = $data['shoplen'];	
		
		$li="";
		foreach($shoplist as $st){
			$li.='<dl class="mycartcur" id="mycartcur'.$st['id'].'">';
			$li.='<dt class="img"><a href="#"><img src="'.G_UPLOAD_PATH.'/'.$st['thumb'].'"></a></dt>';
			$li.='<dd class="title"><a href="'.WEB_PATH.'/goods/'.$st['id'].'">'.$st['title'].'</a>';
			$li.='<span class="rmbred">'.$st['yunjiage'].'×'.$st['cart_gorenci'].'</span>';
			$li.='<input type="hidden" value='.$st['cart_gorenci'].' name="mycartcur'.$st['id'].'"/>';
			$li.='</dd>';
			$li.='<dd class="del"><a class="delGood" onclick="delheader('.$st['id'].')" href="javascript:;">删除</a></dd>';
			$li.='</dl>';
		}
		/*if(count($shoplist)>=5){
			$li.='<dl class="mycartcur" style=" background:#fff;height:20px; text-align:right;"><a style=" color:#777;" target="_blank" href="'.WEB_PATH.'/member/cart/cartlist" class="canmore_x">查看更多<i>&gt;</i></a></dl>';
		}
		*/
		$shop['li']=$li;
		if(!is_array($Cartlist)){
			$shop['num']=0;
		}else{
			$shop['num']=count($Cartlist)-1;		
		}	
	
		$shop['sum']=$MoenyCount;
		echo json_encode($shop);
	}
	//获取购物车的商品信息
	public function cartshop(){		
		$Cartlist=json_decode(stripslashes($this->Cartlist),true);	
		$cartModel = System::load_app_model('cartModel');
		$data = $cartModel->get_cartlistinfo($Cartlist);
		$Cartshopinfo = $data['Cartshopinfo'];
		$shoplist	  = $data['shoplist'];

		$MoenyCount   = $data['MoenyCount'];
		$shoplen	  = $data['shoplen'];	

		$li="";
		foreach($shoplist as $st){			
			$li.='<li id="shopid'.$st['id'].'">';
			$li.='<a href="javascript:;" onclick="delshop('.$st['id'].');" title="删除" class="Close"></a>';
			$li.='<a href="'.WEB_PATH.'/goods/'.$st['id'].'"><img src="'.G_UPLOAD_PATH.'/'.$st['thumb'].'" title="'.$st['title'].'"></a>';
			$li.='<span class="orange">'.$st['cart_gorenci'].'</span>人次';
			$li.='</li>';
		}
		/*if(count($shoplist)>=7){
			$li.='<li class="Roll_CartMore"><a target="_blank" title="查看更多" href="'.WEB_PATH.'/member/cart/cartlist">更多<i>&gt;</i></a></li>';
		}
		*/
		$shop['li']=$li;
		if(!is_array($Cartlist)){
			$shop['num']=0;
		}else{
			$shop['num']=count($Cartlist)-1;		
		}	
		$shop['sum']=$MoenyCount;
		echo json_encode($shop);
	}
	//获取购物车的商品数量ajax
	public function getnumber(){		
		$Cartlist=json_decode(stripslashes($this->Cartlist),true);
		if(!is_array($Cartlist)){
			echo 0;
		}else{
			echo count($Cartlist)-1;		
		}
	}
	
	//购物车商品列表
	public function cartlist(){
		parent::__construct();	
		$member=$this->userinfo;
		$Cartlist=json_decode(stripslashes($this->Cartlist),true);
		if(!empty($_POST)){
			$gid 	= intval($_POST['gid']);
			$number = intval($_POST['number']);
			$is_shi = intval($_POST['is_shi']);
			$shenyu = intval($_POST['shenyu']);
			$money = intval($_POST['money']);
			
			if(isset($Cartlist[$gid])){
				$Cartlist[$gid]['num']=$Cartlist[$gid]['num']+$number;
			}else{
				$Cartlist[$gid]['num']=$number;
				$Cartlist[$gid]['money']=$money;
			}
			$Cartlist[$gid]['shuyu']=$shenyu;
			
			_setcookie('Cartlist',json_encode($Cartlist));
			header("Location:".WEB_PATH."/member/cart/cartlist");die;
		}
		
		
		
		
		$cartModel = System::load_app_model('cartModel');

		$data = $cartModel->get_cartlistinfo($Cartlist,$member['uid']);

		$Cartshopinfo = $data['Cartshopinfo'];
		$shoplist	  = $data['shoplist'];

                foreach ($shoplist as $k => $v){
                    $shoplist[$k]['pk_cishu'] = $v['canyurenshu']/$v['default_renci'];
                    
                }
                
		$MoenyCount   = $data['MoenyCount'];
		$shoplen	  = $data['shoplen'];
		
		/* 统计pv,uv 20160109 start */
		$this->set_pv();
		$this->set_uv();
		/* 统计pv,uv 20160109 end */
		
		include templates("cart","cartlist");
	}
	//支付界面
	public function pay(){
		//用户信息
		parent::__construct();	
		$member=$this->userinfo;
		$Cartlist=json_decode(stripslashes($this->Cartlist),true);

	
		$cartModel = System::load_app_model('cartModel');
		$data = $cartModel->get_cartlistinfo($Cartlist,$member['uid']);
		$Cartshopinfo = $data['Cartshopinfo'];
		$shoplist	  = $data['shoplist'];
                foreach ($shoplist as $k => $v){
                    $shoplist[$k]['pk_cishu'] = $v['canyurenshu']/$v['default_renci'];
                }
		$MoenyCount   = $data['MoenyCount'];
		$shoplen	  = $data['shoplen'];	//商品数量
		
		if($shoplen<1){
			_setcookie('Cartlist',NULL);
			_message("购物车没有商品!",WEB_PATH);
		}
		
		//会员余额
		$Money=$member['money'];
		//商品数量
		$shoplen=count($shoplist);		
		
		$fufen = System::load_app_config("user_fufen");
		if($fufen['fufen_yuan']){
			$fufen_dikou = intval($member['score'] / $fufen['fufen_yuan']);
		}else{
			$fufen_dikou = 0;
		}
		
		$paylist = $this->db->GetList("select * from `@#_pay` where `pay_start` = '1' and `web` = '1'");
		$cookies = base64_encode($this->Cartlist);

		
		$submitcode = uniqid();
		
		/*$redis = new Redis();
		$redis->connect('127.0.0.1',6379);
		$result = $redis->get('submitcode_'.$member['uid']);
		
		if($result){
			
		}else{
			$redis->set('submitcode_'.$member['uid'],$submitcode);
		}*/
		
		session_start();		
		$_SESSION['submitcode'] = $submitcode = uniqid();				
		/* 统计pv 20160109 start */
		$this->set_pv();
		/* 统计pv 20160109 end */		
		include templates("cart","pay");
	}
	
	//账户余额不足，吊起支付
	public function paysubmit(){
		parent::__construct();	
		$member=$this->userinfo;
		if(!isset($_POST['submit'])){	
			_message("正在返回购物车...",WEB_PATH.'/member/cart/cartlist');
			exit;
		}			
		session_start();
		/*if(isset($_POST['submitcode'])) {
			if(isset($_SESSION['submitcode'])){
				$submitcode = $_SESSION['submitcode'];
			}else{
				$submitcode = null;
			}		
			if($_POST['submitcode'] == $submitcode){			
				unset($_SESSION["submitcode"]);      
			}else{				
				_message("请不要重复提交...",WEB_PATH.'/member/cart/cartlist');
			}	
		}else{
			_message("正在返回购物车...",WEB_PATH.'/member/cart/cartlist');
		}*/
	

		parent::__construct();	
		if(!$this->userinfo)$this->HeaderLogin();
		$uid = $this->userinfo['uid'];

				
		$pay_checkbox=isset($_POST['moneycheckbox']) ? true : false;	
		$pay_type_bank=isset($_POST['pay_bank']) ? $_POST['pay_bank'] : false;
		$pay_type_id=isset($_POST['account']) ? $_POST['account'] : false;
		
		if(isset($_POST['shop_score'])){
			$fufen_cfg = System::load_app_config("user_fufen",'','member');	
			$fufen = intval($_POST['shop_score_num']);			
			if($fufen_cfg['fufen_yuan']){				
				$fufen = intval($fufen / $fufen_cfg['fufen_yuan']);
				$fufen = $fufen * $fufen_cfg['fufen_yuan'];
			}			
		}else{
			$fufen = 0;
		}		
		/*************
			start
		*************/
		
		$Cartlist=json_decode(stripslashes($this->Cartlist),true);
		
		//echo "<pre>";print_r($_REQUEST);
		//echo "<pre>";print_r($Cartlist);die;
	

		$pay=System::load_app_class('pay','pay');
		//$pay->scookie = json_decode(base64_decode($_POST['cookies']));
		
			
		$pay->fufen = $fufen;
		$pay->pay_type_bank = $pay_type_id;
		//购买初始化
		$ok = $pay->init($uid,$pay_type_id,'go_record');
		if($ok !== 'ok'){
			$_COOKIE['Cartlist'] = NULL;
			_setcookie("Cartlist",null);
			_message($ok,G_WEB_PATH);
		}


		$check = $pay->go_pay($pay_checkbox);

		if($check === 'not_pay'){
			_message('未选择支付平台!',WEB_PATH.'/member/cart/cartlist');
		}
		if(!$check){
		    /////////////////////////////////////////////////////////////////////////////////////
		    /*处理异常商品  kpf  20160102 start */
		    if(count($Cartlist) >= 2){    //提取出商品的id
		        $db = System::load_sys_class('model');
		        foreach($Cartlist as $k => $v){
		            if( intval($k) > 0 ){
		                $goods_info = $db->GetOne("SELECT id,zongrenshu,shenyurenshu FROM @#_shoplist WHERE `id`='$k' LIMIT 1");
		                if(!isset($goods_info['id'])) continue;
		                $ab_num = $db->GetOne("SELECT SUM(gonumber) as num FROM @#_member_go_record WHERE `shopid`='$k' ");
		                $code_num = $db->GetOne("SELECT SUM(s_len) as num FROM @#_$goods_info[codes_table] WHERE `s_id`='$k' ");
		                //如果购买记录已经满了100条，并且众乐码也已经分配完 ， 但是商品的剩余数量却大于0，那么该商品出现异常
		                if($goods_info['zongrenshu'] == $ab_num['num'] && $code_num['num']==0 && $goods_info['shenyurenshu'] > 0){
		                    //@file_put_contents($k.'-'.time().'abnormal.log', print_r($goods_info,1));
		                    $query = $db->Query("UPDATE @#_shoplist SET `canyurenshu` = `zongrenshu`,`shenyurenshu`=0 WHERE `id`=$goods_info[id] ");
		                    if($query){
    		                    $post_arr= array("gid"=>$goods_info['id']);
    		                    _g_triggerRequest(WEB_PATH.'/pay/pay_user_ssc_time/init',false,$post_arr);
		                    }
		                }
		            }
		        }
		    }
		    /*处理异常商品 kpf  20160102 end */
		    /////////////////////////////////////////////////////////////////////////////////////
		    
			//_message("商品支付失败!",WEB_PATH.'/member/cart/cartlist');  //这是原始代码
			
		    //失败直接清空购物车，跳转首页  kpf 20160102
		    $_COOKIE['Cartlist'] = NULL;
		    _setcookie("Cartlist",null);
		    _message("商品已被抢空!",WEB_PATH);
		}
		if(!$check){
		    //失败
		    $_COOKIE['Cartlist'] = NULL;
		    _setcookie("Cartlist",null);
		    header("location: ".WEB_PATH);
		}else{
			//成功
			header("location: ".WEB_PATH."/member/cart/paysuccess");
		}	
		
		exit;
	}
	
	
	//成功页面
	public function paysuccess(){	
		$_COOKIE['Cartlist'] = NULL;
		_setcookie("Cartlist",null);
		include templates("cart","paysuccess");		
	}
	
	//充值
	public function addmoney(){
//		var_dump($_POST);die;
		parent::__construct();	
		if(!isset($_POST['submit'])){	
			_message("正在返回充值页面...",WEB_PATH.'/member/home/userrecharge');
			exit;
		}	
		//var_dump($_POST);die;
		if(!$this->userinfo)$this->HeaderLogin();		
		$pay_type_bank=isset($_POST['pay_bank']) ? $_POST['pay_bank'] : false;
		$pay_type_id=isset($_POST['account']) ? $_POST['account'] : false;
		$money=intval($_POST['money']);		
		$uid = $this->userinfo['uid'];

		$pay=System::load_app_class('pay','pay');
		$pay->pay_type_bank = $pay_type_bank;
		$ok = $pay->init($uid,$pay_type_id,'addmoney_record',$money);
		if($ok === 'not_pay'){
			_message("未选择支付平台");
		}
		
	}
}


?>