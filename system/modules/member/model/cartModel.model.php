<?php   

Class cartModel extends model{
	/**
	 * @param 	$Cartlist	购物车信息
	 */	
	public function get_cartlistinfo($Cartlist,$uid=0){
		
		$shopids='';	
		if(is_array($Cartlist)){			
			foreach($Cartlist as $key => $val){
				$shopids.=intval($key).',';				
			}
			$shopids=str_replace(',0','',$shopids);
			$shopids=trim($shopids,',');
		}
		
		$shoplist=array();
		//购物车商品信息列表
		if($shopids!=NULL){
			$shoplist=$this->GetList("SELECT * FROM `@#_shoplist` where `id` in($shopids)",array("key"=>"id"));
//                        print_r($shoplist);
			$shoplen=count($shoplist);						//商品数量
//                        print_r($shoplen);
                        
		}
		$goindexModel = System::load_app_model('goindex','go');
		
		$MoenyCount=0;
		$Cartshopinfo='{';
		if($shoplen>=1){
			$state 	= 0;
			foreach($Cartlist as $key => $val){
				$key=intval($key);					
				if(isset($shoplist[$key])){									
					$shoplist[$key]['cart_gorenci']=$val['num'] ? $val['num'] : 1;
					//该产品的剩余购买次数
					$shoplist[$key]['cart_shenyu']=$shoplist[$key]['zongrenshu']-$shoplist[$key]['canyurenshu'];
					if($shoplist[$key]['cart_shenyu']==0){
						unset($shoplist[$key]);
					}
					if($uid>0){
						//用户已经购买的数量
						$user_bought_number = $goindexModel->user_bought_number($uid,$shoplist[$key]['id']);
					}else{
						$user_bought_number = 0;
					}
					
//					if($shoplist[$key]['is_shi']==1){					//五元限购
//						//用户剩余的购买数量
//						$user_can_buy_number = 5 - $user_bought_number;
//						$user_can_buy_number = $user_can_buy_number>$shoplist[$key]['cart_shenyu'] ? $shoplist[$key]['cart_shenyu'] : $user_can_buy_number;
//						
//						if($shoplist[$key]['cart_gorenci']>$user_can_buy_number){
//							$shoplist[$key]['cart_gorenci'] = $user_can_buy_number;//五元限购    用户购买次数超过用户剩余的购买人次														
//						}
//							
//						//用户能购买的数量
//						$shoplist[$key]['user_can_buy']=$user_can_buy_number;
//					}else{
//						$shoplist[$key]['user_can_buy']=$shoplist[$key]['cart_shenyu'];
//					}
                                        
                                        //2017-6-7
                                        if($shoplist[$key]['is_shi']==1){					//限购
						//用户剩余的购买数量
						$user_can_buy_number = 5 - $user_bought_number;
						$user_can_buy_number = $user_can_buy_number>$shoplist[$key]['cart_shenyu'] ? $shoplist[$key]['cart_shenyu'] : $user_can_buy_number;
						
						if($shoplist[$key]['cart_gorenci']>$user_can_buy_number){
							$shoplist[$key]['cart_gorenci'] = $user_can_buy_number;//限购    用户购买次数超过用户剩余的购买人次														
						}
							
						//用户能购买的数量
						$shoplist[$key]['user_can_buy']=$user_can_buy_number;
					}else if($shoplist[$key]['pk_product']==1){

                                                $user_can_buy_number = $shoplist[$key]['default_renci'] - $user_bought_number;
                                                if($shoplist[$key]['cart_gorenci']>$user_can_buy_number){
							$shoplist[$key]['cart_gorenci'] = $user_can_buy_number;//PK限购    用户购买次数超过用户剩余的购买人次														
						}
                                                $shoplist[$key]['user_can_buy']=$user_can_buy_number;
                                        }else{
						$shoplist[$key]['user_can_buy']=$shoplist[$key]['cart_shenyu'];
					}
                                        
					$shoplist[$key]['user_bought']=$user_bought_number;
					$shoplist[$key]['cart_xiaoji']=sprintf("%.2f",$shoplist[$key]['yunjiage']*$shoplist[$key]['cart_gorenci']);
				
					if($shoplist[$key]['user_can_buy']<=0){
						unset($shoplist[$key]);
						$state=1;
                                        }else{
						$MoenyCount+=$shoplist[$key]['yunjiage']*$shoplist[$key]['cart_gorenci'];
						$Cartshopinfo.=$key.':{"shenyu":'.$shoplist[$key]['cart_shenyu'].',"num":'.$shoplist[$key]['cart_gorenci'].',"money":'.$shoplist[$key]['yunjiage'].'},';
					}
				}
			}
		}
		$MoenyCount=sprintf("%.2f",$MoenyCount);		//总价格
		$Cartshopinfo.='"MoenyCount":'.$MoenyCount.'}';		//json格式的数据购物车内商品缓存信息
		
		$data=array('Cartshopinfo'=>$Cartshopinfo,'shoplist'=>$shoplist,'MoenyCount'=>$MoenyCount,'shoplen'=>$shoplen);
		return $data;
	}
	
	
	
	
	
	
	
	
	
	
	
}




