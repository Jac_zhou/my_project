<?php 
defined('G_IN_SYSTEM')or exit('No permission resources.');
System::load_app_class('base','member','no');
System::load_app_fun('my');//system下的load_app_fun()方法
System::load_app_fun('user');
System::load_sys_fun('user');

class index extends base {
	
	public function __construct() {	
		parent::__construct();
		$this->db=System::load_sys_class('model');	
		
		/* 数据统计 20160108 start */
		set_su_sk();
		/* 数据统计 20160108 end */
		
		//如果是手机访问，直接跳转到手机端
		if(_is_mobile()) header("location:".G_WEB_PATH);
	}		
	
	public function init(){
		$su = isset($_GET['su']) ? trim($_GET['su']) : '';
	    $sk = isset($_GET['sk']) ? trim($_GET['sk']) : '';
	    /**	
		* 数据模块操作类           后缀名  model.php 文件夹 model
		* @param   $model_name	需要引入的模型Model
		* @param   $module    	模块 
		* @param   $new		是否重新实例化这model类
		* @return  返回需要引入的文件
		*/
	    //load_app_model($model_name='',$module='',$new='yes')
		$ShoplistModel = System::load_app_model('Shoplist','Zapi');//System类下的load_app_model方法
		$GoodListCon = System::load_contorller('GoodList','Zapi');

		//首页右侧推荐位
		/**
		* 引入模块下的配置文件	模块目录下lib文件夹    ini.php后缀名
		* @param	$filename	配置文件名
		* @param	$keys			引入该配置文件中的某一项配置（为空便引入所有配置）
		* @param	$module		模块	（默认是当前模块）
		* @return	返回需要引入的配置文件/某单独配置项
		*/
		//load_app_config($filename,$keys='',$module='')
		$recom_a = System::load_app_config('recom_a','',G_ADMIN_DIR);//常量G_ADMIN_DIR定义位于system/config/global.php
		$recom_b = System::load_app_config('recom_b','',G_ADMIN_DIR);
		//file_put_contents('error.txt', json_encode($recom_b).PHP_EOL,FILE_APPEND);
		
		if(isset($recom_a['is_open']) && intval($recom_a['is_open']) == 1){
			$where = '`sid`="'.$recom_a['sid'].'" and `q_uid` is null ORDER BY `id` DESC LIMIT 1';
            $field = 'id,title,is_new,pos,renqi,is_shi,thumb,money,canyurenshu,zongrenshu,shenyurenshu,qishu,default_renci';
		}else{
			$where = '`pos` = 1 and `q_uid` is null ORDER BY `id` DESC LIMIT 1';
            $field = 'id,title,is_new,pos,renqi,is_shi,thumb,money,canyurenshu,zongrenshu,shenyurenshu,qishu,default_renci';
		}
		$new_shop = $ShoplistModel->get_good_info($where,$field);
		$new_shop = $GoodListCon->list_add_user_bought_number($new_shop,$this->userinfo['uid']);
		//首页推荐位 2
        //var_dump($recom_b);
		if(isset($recom_a['is_open']) && intval($recom_a['is_open']) == 1){
			$sid_str = $recom_b['sid_str'];
			$where = '`sid` in('.$sid_str.') and `q_uid` is null ORDER BY `id` DESC LIMIT 1';
            $field = 'id,title,is_new,pos,renqi,is_shi,thumb,money,canyurenshu,zongrenshu,shenyurenshu,qishu,default_renci';
		}else{
			$where = '`pos` = 1 and `q_uid` is null ORDER BY `id` DESC LIMIT 1';
            $field = 'id,title,is_new,pos,renqi,is_shi,thumb,money,canyurenshu,zongrenshu,shenyurenshu,qishu,default_renci';
		}

		$new_shop_mun=$ShoplistModel->get_good_info($where,$field);


		//最新上架
		$where = '`q_end_time` is  null and `q_showtime` = "N" and `shenyurenshu`!=0 AND `renqi` =0  AND is_new=1 order by `order` desc LIMIT 0,5';
        $field = 'id,title,is_new,pos,renqi,is_shi,thumb,money,canyurenshu,zongrenshu,shenyurenshu,qishu,default_renci,pk_product,pk_number';
		$shoplistnew = $ShoplistModel->get_good_list($where,$field);

		//人气商品
		$where = '`renqi`=1 and `q_uid` is null and `shenyurenshu` > 0 ORDER BY `order` DESC LIMIT 20';
		$field = 'id,title,is_new,pos,renqi,is_shi,thumb,money,canyurenshu,zongrenshu,shenyurenshu,qishu,default_renci,pk_product,pk_number,pk_cishu';
		$shoplistrenqi	= $ShoplistModel->get_good_list($where,$field);

		//即将揭晓
		$field = 'id,title,is_new,pos,renqi,is_shi,thumb,money,canyurenshu,zongrenshu,shenyurenshu,qishu,pk_product,pk_number,default_renci,(canyurenshu/zongrenshu) AS bili';
		$where = '`q_uid` is null AND shenyurenshu !=0 ORDER BY bili DESC LIMIT 8';
		$shoplist	= $ShoplistModel->get_good_list($where,$field);

		//即将揭晓
		$shoplist 	= $GoodListCon->list_add_user_bought_number($shoplist,$this->userinfo['uid']);
		//最新上架
		$shoplistnew = $GoodListCon->list_add_user_bought_number($shoplistnew,$this->userinfo['uid']);
                foreach ($shoplistnew as $k => $v){
                        $shoplistnew[$k]['pk_cishu'] = $v['canyurenshu']/$v['default_renci'];
                }
		//人气商品
		$shoplistrenqi 	= $GoodListCon->list_add_user_bought_number($shoplistrenqi,$this->userinfo['uid']);
                foreach ($shoplistrenqi as $k => $v){
                        $shoplistrenqi[$k]['pk_cishu'] = $v['canyurenshu']/$v['default_renci'];
                }

		$max_renqi_qishu = 1;
		$max_renqi_qishu_id = 1;

		if(!empty($shoplistrenqi)){
			foreach ($shoplistrenqi as $renqikey =>$renqiinfo){
				if($renqiinfo['qishu'] >= $max_renqi_qishu){
					$max_renqi_qishu = $renqiinfo['qishu'];
					$max_renqi_qishu_id = $renqikey;
				}
			}
			$shoplistrenqi[$max_renqi_qishu_id]['t_max_qishu'] = 1;
		}
		$this_time = time();
		if(count($shoplistrenqi) > 1){
					if($shoplistrenqi[0]['time'] > $this_time - 86400*3)
					$shoplistrenqi[0]['t_new_goods'] = 1;
		}


		//最新参与记录
		//$field = 'b.uid,b.username,b.email,b.mobile,b.img,a.shopname,a.shopid';
		//$where = 'b.uid = a.uid and a.status LIKE "%已付款%" group by a.uid ORDER BY a.time DESC LIMIT 0,15';
        $sql = "select
                      b.uid,b.username,b.email,b.mobile,b.img,a.shopname,a.shopid
                from  @#_member_go_record as a
                LEFT JOIN  `@#_member` as b  on b.uid = a.uid
                where
                      a.status LIKE \"%已付款%\" group by a.uid ORDER BY a.time DESC LIMIT 0,15
                ";
		//$go_recordb=$this->db->GetList('select '.$field.' from `@#_member_go_record` as a,`@#_member` as b where '.$where);
        $go_recordb=$this->db->GetList($sql);
//		print_r($go_recordb);
		//最新揭晓
		$shopqishu = $ShoplistModel->get_will_kown();
		//晒单分享 sd_id,sd_thumbs,sd_content,sd_userid,sd_time
		$shaidan=$this->db->GetList('select sd_id,sd_thumbs,sd_content,sd_userid,sd_time from `@#_shaidan` where `is_audit` = 2 order by `sd_id` DESC LIMIT 12');
		if($shaidan){
			foreach($shaidan AS $key=>$val){
				$shaidan[$key]['sd_titile'] = emj_decode($val['sd_titile']);
				$shaidan[$key]['sd_content'] = emj_decode($val['sd_content']);
				$shaidan[$key]['sd_time'] = date("Y-m-d H:i:s",$val['sd_time']);
			}
		}
        //首页幻灯片
        $SlideModel = System::load_app_model('Slide','Zapi');
        $slides = $SlideModel->get_slide();
        $slides = $SlideModel->add_goodlink($slides);
		//$shaidan_two=$this->db->GetList("select * from `@#_shaidan` order by `sd_id` DESC LIMIT 2,6");

		//首页弹窗广告 20151213
		$index_adv = System::load_app_config('index_adv','',G_ADMIN_DIR);
		$index_adv['pics'] = G_UPLOAD_PATH .'/'. $index_adv['pics'];
		$index_adv['link'] = empty($index_adv['link']) ? "javascript:;" : $index_adv['link'];

		/* 统计pv,uv 20160109 start */
		$this->set_pv();
		$this->set_uv();
		/* 统计pv,uv 20160109 end */
	    include templates("index","index");

	}
	/*
		商品列表
	*/
    public function glist(){
        $select = ($this->segment(4)) ?  ($this->segment(4)) : '0_0_1';
        $select = explode("_",$select);
        $select[] = '0';
        $select[] = '0';
        $cid   = abs(intval($select[0]));
        $bid   = abs(intval($select[1]));
        $order = abs(intval($select[2]));
        //排序方式
        $orders = '';
        switch($order){
            case  '1':
                $orders = 'order by (`canyurenshu`/`zongrenshu`) DESC,`order` DESC,`money` DESC';
                break;
            case  '2':
                $orders = "and `renqi` = '1' order by `order` DESC,`money` DESC";
                break;
            case  '3':
                $orders = 'order by `shenyurenshu` ASC,`order` DESC';
                break;
            case  '4':
                $orders = "and `is_new` = '1' order by `order` DESC,`money` DESC";
                break;
            case  '5':
                $orders = 'order by `order` DESC,`money` DESC,time DESC';
                break;
            case  '6':
                $orders = 'order by `order` DESC,`money` ASC';
                break;
            default:
                $orders = 'order by `order` DESC,`id` DESC';
        }

        /* 设置了查询分类ID 和品牌ID*/
        if($cid){
            //$brand=$this->db->GetList("select id,cateid,name from `@#_brand` where `cateid` LIKE '%$cid%' order by `order` DESC");
            //2014-11-18 lq 修复品牌错乱问题
            if($cid == 14){     //潮流新品
                $sql = "select id,cateid,name from `@#_brand` where CONCAT(',',`cateid`,',') LIKE '%,5,%' or '%,6,%' order by `order` DESC";
            }else{
                $sql = "select id,cateid,name from `@#_brand` where CONCAT(',',`cateid`,',') LIKE '%,$cid,%' order by `order` DESC";
            }
            //获取品牌信息
            $brand=$this->db->GetList($sql);
            $sql = "select cateid,name,parentid,info from `@#_category` where `cateid` = '$cid' LIMIT 1";
            $daohang=$this->db->GetOne($sql);
            $daohang['info'] = unserialize($daohang['info']);

            $daohang_title  = !empty($daohang['name']) ? $daohang['name'] : $daohang['info']['meta_title'];
            $keywords       = $daohang['info']['meta_keywords'];
            $description    = $daohang['info']['meta_description'];
            if($cid == 14){
                $sun_id_str = "'{$cid}','5','6'";
                $sql = "SELECT cateid from `@#_category` where `parentid` IN ({$sun_id_str})";
            }else{
                $sun_id_str = "'{$cid}'";
                $sql = "SELECT cateid from `@#_category` where `parentid` = '{$cid}'";
            }
            $sun_cate = $this->db->GetList($sql);

            foreach($sun_cate as $v){
                $sun_id_str .= ","."'".$v['cateid']."'";
            }

            if($bid){
                $sql = "select id from `@#_shoplist` WHERE `shenyurenshu` > 0 AND  `q_uid` is null and `brandid`='$bid'  and `cateid` in ($sun_id_str) $orders";
            }else{
                $sql = "select id from `@#_shoplist` WHERE  `shenyurenshu` > 0 AND `q_uid` is null and `cateid` in ($sun_id_str) $orders";
            }
        }else{
            $sql = "select id,cateid,name from `@#_brand` where 1 order by `order` DESC";
            $brand=$this->db->GetList($sql);
            $daohang_title = '所有分类';

            if($bid){
                $sql = "select id from `@#_shoplist` WHERE  `shenyurenshu` > 0 AND `q_uid` is null and `brandid`='$bid' $orders";
            }else{
                $sql = "select id from `@#_shoplist` WHERE  `shenyurenshu` > 0 AND `q_uid` is null $orders";
            }
        }
        //满足当前查询条件的数量
        $total=$this->db->GetCount($sql);
        //分页
        $num=20;
        $page=System::load_sys_class('page');
        if(isset($_GET['p'])){$pagenum=$_GET['p'];}else{$pagenum=1;}
        $page->config($total,$num,$pagenum,"0");
        if($pagenum > $page->page){
            $pagenum=$page->page;
        }
        $field = 'id,title,is_shi,thumb,money,canyurenshu,zongrenshu,default_renci,yunjiage,shenyurenshu,pk_product,pk_number';
        if($cid){
            if($bid){
                $sql = "select {$field} from `@#_shoplist` where `shenyurenshu` > 0 AND `q_uid` is null and `brandid`='$bid' and `cateid` in ($sun_id_str) $orders";
            }else{
                $sql = "select {$field} from `@#_shoplist` where `shenyurenshu` > 0 AND `q_uid` is null and `cateid` in ($sun_id_str) $orders";
            }
        }else{
            if($bid){
                $sql = "select {$field} from `@#_shoplist` where `shenyurenshu` > 0 AND `q_uid` is null and `brandid`='$bid' $orders";
            }else{
                $sql = "select {$field} from `@#_shoplist` where `shenyurenshu` > 0 AND `q_uid` is null $orders";
            }
        }
        //满足当前查询商品的列表
        $shoplist = $this->db->GetPage($sql,array("num"=>$num,"page"=>$pagenum,"type"=>1,"cache"=>0));
        /* 统计pv,uv 20160109 start */
        $this->set_pv();
        $this->set_uv();
        /* 统计pv,uv 20160109 end */
        //_alert();
        include templates("index","glist");
    }
	
	//商品详细
	public function item(){
		
		//$mysql_model=System::load_sys_class('model');
		$itemid=abs(intval(safe_replace($this->segment(4))));	
		//当前商品信息
		$item=$this->db->GetOne("select * from `@#_shoplist` where `id`='".$itemid."' LIMIT 1");
                $item['pk_cishu'] = $item['canyurenshu']/$item['default_renci'];
        $member=$this->userinfo;
		$GoodListCon = System::load_contorller('GoodList','Zapi');
        $item = $GoodListCon->list_add_user_bought_number($item,$member['uid']);

		$frist_one_arr = unserialize($item['picarr']);
		if(!$item)_message("没有这个商品！",WEB_PATH,3);

		
		//结束时间    倒计时0   已经关闭揭晓动画
		if($item['q_uid'] && $item['q_showtime'] == 'N'){
			header("location: ".WEB_PATH."/dataserver/".$item['id']);			
			exit;			
		}
		if($item['q_uid'] &&$item['q_showtime'] == 'Y'){
			$q_showtime = 'Y';
		}
		
		//本期中登录的用户参与了几次和号码
        //$user_can_buy_number = $item['wap_ten'];
        //本期中登录的用户参与了几次和号码
        $user_can_buy=1;			//判断用户是否可以购买
        $user_can_buy_number= $syrs = $item['shenyurenshu'];
        if($item['wap_ten'] ==0 ){
            $user_can_buy=0;
        }
        
        $member=$this->userinfo;
        if($member['uid']){
            $arr_member_info = getcountgonumber($itemid,$member['uid']);
            $arr_member_info['all_goucode'] = getcontgocode($itemid,$member['uid']);
            $arr_member_info['uid'] =  $member['uid'];
        }
        //用户已经购买的数量
        $user_bought_number = $item['bought'];
        //剩余人数   用户可以购买的数目
        $user_can_buy_number = $syrs = $item['wap_ten'];

		//获取栏目信息
		$category=$this->db->GetOne("select * from `@#_category` where `cateid` = '$item[cateid]' LIMIT 1");
		//获取品牌信息
		$brand=$this->db->GetOne("select * from `@#_brand` where `id`='$item[brandid]' LIMIT 1");
		
		$title=$item['title'].' ('.$item['title2'].')' .' - '. _cfg("web_name");
		$keywords = $item['keywords'];
		$description = $item['description'];

		$item['picarr'] = unserialize($item['picarr']);
		//本产品所有的购买记录
        $field = 'username,uphoto,time,ip,uid,id,gonumber,shopqishu';

		$us=$this->db->GetList("select {$field} from `@#_member_go_record` where `shopid`='{$itemid}' AND `shopqishu`='{$item['qishu']}'ORDER BY id DESC LIMIT 3");

		 //2017-6-16
		if($item['pk_product']==1){
			$us_pkbuy= $this->db->GetOne("select * from `@#_member_go_record` where `shopid`='{$item['id']}' AND `shopqishu`='{$item['qishu']}' AND `uid`='{$member['uid']}'");
			$user_can_pkbuy=1; //判断用户是否可以购买
			if(!empty($us_pkbuy)){
				$user_can_pkbuy=0;
			}
		}
		/*2015_12_24推荐+人气商品一个*/
		$tuijian_renqi = get_tuijian_renqi_shop($itemid,1,1);
		//期数导航  
		$list = $this->item_list_nav($item['id'],$item['sid']);
		$qishu_nav 	= $list['qishu_list'];
		$All_list	= $list['all_list'];
		/* 统计pv,uv 20160109 start */
		$this->set_pv();
		$this->set_uv();
		/* 统计pv,uv 20160109 end */
		include templates("index","item");
	}
	
	//往期商品查看
	public function dataserver(){

		$itemid=abs(intval($this->segment(4)));
		$item=$this->db->GetOne("select * from `@#_shoplist` where `id`='$itemid' LIMIT 1");
		if(!$item){
			_message("没有这个商品!");
		}
		//该产品的中奖code为空
		if(empty($item['q_user_code'])){
			_message("该商品正在进行中...",WEB_PATH.'/goods/'.$itemid);
		}
		
		//如果揭晓结束时间是空的并且还有揭晓动画        
		if($item['q_showtime'] == 'Y'){
			header("location: ".WEB_PATH."/goods/".$item['id']);
			exit;			
		}
		
		
		//栏目信息
		$category=$this->db->GetOne("select * from `@#_category` where `cateid` = '$item[cateid]' LIMIT 1");
		//品牌信息
		$brand=$this->db->GetOne("select * from `@#_brand` where `id` = '$item[brandid]' LIMIT 1");
		//夺宝中奖码
		$q_user = unserialize($item['q_user']);		
		$q_user_code_len = strlen($item['q_user_code']);
		$q_user_code_arr = array();
		for($q_i=0;$q_i < $q_user_code_len;$q_i++){	
			$q_user_code_arr[$q_i] = substr($item['q_user_code'],$q_i,1);
		}

		//中奖用户的总夺宝次数
		$user_shop_number = $this->db->GetOne("select sum(gonumber) as gonumber from `@#_member_go_record` where `uid`= '$item[q_uid]' and `shopid` = '$itemid' and `shopqishu` = '$item[qishu]'");

        $user_shop_number = $user_shop_number['gonumber'];

		//中奖用户的夺宝时
		$user_shop_time = $this->db->GetOne("select time from `@#_member_go_record` where `uid`= '$item[q_uid]' and `shopid` = '$itemid' and `shopqishu` = '$item[qishu]' and `huode` = '$item[q_user_code]'");
		$user_shop_time = $user_shop_time['time'];

		//中奖用户的所有夺宝码	
		$user_shop_codes_arr = $this->db->GetList("select goucode from `@#_member_go_record` where `uid`= '$item[q_uid]' and `shopid` = '$itemid' and `shopqishu` = '$item[qishu]'");
		$user_shop_codes = '';
		foreach($user_shop_codes_arr as $v){
			$user_shop_codes .= $v['goucode'].',';		
		}
		
		$user_shop_codes = rtrim($user_shop_codes,',');//移除最右边的 '，'
		/**时时彩LVDEND添加**/
	    $ssc_code=$item['q_ssccode'];				//对应数据库的时时彩中奖号码
        if($ssc_code>0){}else{
            $ssc_code=0;
        }
		if(empty($item['q_sscopen']))$item['q_sscopen']=0;
		$ssc_opentime=date('Y-m-d',$item['q_sscopen']);//对应数据库的时时彩开奖时间
		/**时时彩LVDEND添加**/
		
		$h=abs(date("H",$item['q_end_time']));
		$i=date("i",$item['q_end_time']);
		$s=date("s",$item['q_end_time']);
		$w=substr($item['q_end_time'],11,3);	
		$user_shop_time_add = $h.$i.$s.$w;
		/**时时彩LVDEND修改**/
		$user_shop_time_addssc=$user_shop_time_add+$ssc_code;
		$user_shop_fmod = fmod($user_shop_time_addssc*100,$item['canyurenshu']);//fmod()求余
		/**时时彩LVDEND修改**/		
		
		//商品详情文
		if($item['q_content']){
			$item_q_content = unserialize($item['q_content']);
			$keysvalue = $new_array = array();
			foreach($item_q_content as $k=>$v){
				$keysvalue[$k] = $v['time'];				
				$h=date("H",$v['time']);
			    $i=date("i",$v['time']);
			    $s=date("s",$v['time']);	
			    list($timesss,$msss) = explode(".",$v['time']);
				$item_q_content[$k]['timeadd'] = $h.$i.$s.$msss;			
			
			}
			arsort($keysvalue);	//asort($keysvalue);正序
			reset($keysvalue);
			foreach ($keysvalue as $k=>$v){
				$new_array[$k] = $item_q_content[$k];
			}			
			$item['q_content'] = $new_array;
		}
		
	
		
		$title=$item['title'].' ('.$item['title2'].')' .' - '. _cfg("web_name");;
		//产品关键字
		$keywords = $item['keywords'];
		//产品介绍
		$description = $item['description'];
		//获取该商品的购物记录
		$go_record_list = $this->db->GetList("select * from `@#_member_go_record` where `shopid` = '$item[id]' and `shopqishu` = '$item[qishu]' order by `id` DESC limit 50");
		//最新一期的商品信息
		//$itemzx=$this->db->GetOne("select * from `@#_shoplist` where `sid`='$item[sid]' and `qishu`>'$item[qishu]' order by `qishu` DESC LIMIT 1");
		
		
		//期数导航  
		$list = $this->item_list_nav($item['id'],$item['sid']);

		$qishu_nav 	= $list['qishu_list'];
		$All_list	= $list['all_list'];
		
		//登录的用户参与了几次和号码   		2015/12/03改期中      
		$member=$this->userinfo;
		if($member['uid']){
			$arr_member_info = getcountgonumber($itemid,$member['uid']);
			$arr_member_info['all_goucode'] = getcontgocode($itemid,$member['uid']);
			$arr_member_info['uid'] =  $member['uid'];
		
		}
		/*登录的用户在该商品中夺宝的次数和所有的夺宝号码*/
		$my_sql = "select goucode,gonumber from `@#_member_go_record` where `uid`= '$member[uid]' and `shopid` = '$itemid' and `shopqishu` = '$item[qishu]'";
		$my_zhongle_arr = $this->db->GetList($my_sql);
		$my_zhongle_number = '';
		$my_zhongle_count = 0;
		foreach($my_zhongle_arr as $val){
			$my_zhongle_number .= $val['goucode'].',';	
			$my_zhongle_count += intval($val['gonumber']);
		}
		$my_zhongle_number = rtrim($my_zhongle_number,",");
		//本期参与时间
		$my_zhongle_time_arr = $this->db->GetOne("select time from `@#_member_go_record` where `uid`= '$member[uid]' and `shopid` = '$itemid' and `shopqishu` = '$item[qishu]'");
		$my_zhongle_time = microt($my_zhongle_time_arr['time']);
		
		unset($member,$my_zhongle_arr,$my_zhongle_time_arr);
		/*2015_12_24推荐+人气商品一个*/
		$tuijian_renqi = get_tuijian_renqi_shop($itemid,1,1);
		
		
		/* 统计pv,uv 20160109 start */
		$this->set_pv();
		$this->set_uv();
		/* 统计pv,uv 20160109 end */
		
		$ssclottery=$this->db->GetOne("SELECT * from `@#_caches` WHERE `key`='ssclottery'");
		if($ssclottery && $ssclottery['value']==1){
			include templates("index","dataserver_ex");
		}else{
			include templates("index","dataserver");				
		}

	}

	//最新揭晓
	public function lottery(){	

		//最新揭晓
		$page=System::load_sys_class('page');		
		$total=$this->db->GetCount("select id from `@#_shoplist` where shenyurenshu=0");
		
		if(isset($_GET['p'])){
			$pagenum=$_GET['p'];
		}else{
			$pagenum=1;
		}
		$num=22;
		$page->config($total,$num,$pagenum,"0");
        //已经揭晓
        $field = 'q_uid,q_user,q_showtime,title,id,money,qishu,q_end_time,q_user_code,zongrenshu,thumb';
		$shopqishu=$this->db->GetPage("select $field from `@#_shoplist` where shenyurenshu=0 ORDER BY q_end_time DESC",array("num"=>$num,"page"=>$pagenum,"type"=>1,"cache"=>0));


		/*即将揭晓*/
		$will_shoplist=$this->db->GetList("select zongrenshu,yunjiage,canyurenshu,shenyurenshu,title,id,sid,thumb,(canyurenshu/zongrenshu) AS bili from `@#_shoplist` where `q_uid` is null AND shenyurenshu !=0 ORDER BY bili DESC LIMIT 7");
		
		/* 统计pv,uv 20160109 start */
		$this->set_pv();
		$this->set_uv();
		/* 统计pv,uv 20160109 end */

		include templates("index","lottery");
	}
	
	
	public function ajax_next_pre(){
		//$mysql_model=System::load_sys_class('model');
		$action = trim($_POST['action']);
		$shopid = intval($_POST['shopid']);
		$sid = intval($_POST['sid']);
        $_html = '';
		if(!$shopid || !$sid){
			$_html ='<div style="height:15px;">参数错误</div>';
			die($_html);
		}
		
		
		$con = "where `sid`='$sid'";
		if($action == "next"){
			$con .= " AND id < '$shopid' ORDER BY id DESC";
		}else{
			$con .= " AND id > '$shopid' ORDER BY id ASC";
		}
		
		$res = "select id,title,q_uid,sid,q_user_code,qishu,q_end_time,thumb,time,q_showtime from `@#_shoplist` $con LIMIT 0,1";
		$arr_list = $this->db->GetOne($res);
		$arr_recover = getcountgonumber($arr_list['id'],$arr_list['q_uid']);
		$arr_list['time'] = microt($arr_recover['time']);
		$arr_list['q_end_time'] = microt($arr_list['q_end_time']);
		$arr_list['count_gonumber'] = $arr_recover['count_gonumber'];
		
		$user_1 = WEB_PATH."/uname/".$arr_list['q_uid'];
		$img_1 = G_UPLOAD_PATH."/".userid($arr_list['q_uid'],'img','160160'); //获取头像
		$user_2 = get_user_name($arr_list['q_uid']);
		$href_1 = WEB_PATH."/dataserver/".$arr_list['id'];
		$href_2 = WEB_PATH."/goods/".$arr_list['id'];
		//print_r($arr_list);
		if(!$arr_list['q_uid']){
			$q_time = intval(substr($arr_list['q_end_time'],0,10));
			if($q_time-time()<=3540){
				$q_time = $q_time-time();
			}
		} 
		
		if($arr_list){
			$_html .='<div style="height:15px;"></div>';
			$_html .='<div class="xjiexiao_xuanze">';
			$_html .='<div class="xjiexiao_pre"></div>';
			$_html .='<div class="xjiexiao_qishu">第<span style="">'.$arr_list['qishu'].'</span>期</div>';
			$_html .= '<div class="xjiexiao_next"></div>';
			$_html .='<input type="hidden" name="shopid" value="'.$arr_list['id'].'"/>';
			$_html .='<input type="hidden" name="sid" value="'.$arr_list['sid'].'"/>';
			$_html .='</div>';
			
			if(intval($arr_list['q_uid'])){
				$_html .='<div class="xjiexiao_img"><a href="'.$user_1.'" target="_blank"><img src="'.$img_1.'"></a></div>';
				$_html .='<div class="xjiexiao_detail">';
				$_html .='<p>恭喜<span class="txt-blue">'.$user_2.'</span>获得该期商品</p>';
				$_html .='<p>用户名：<span class="txt-red">'.$user_2.'</span></p>';
				$_html .='<p>幸运号码：<span class="txt-red">'.$arr_list['q_user_code'].'</span></p>';
				$_html .='<p>本期参与：<span>'.$arr_list['count_gonumber'].'</span>人次</p>';
				$_html .='<p>揭晓时间：<span>'.$arr_list['q_end_time'].'</span></p>';
				$_html .='<p>夺宝时间：<span>'.$arr_list['time'].'</span></p>';
				$_html .='</div>';
				$_html .='<div class="xjiexiao_seedetail"><a href="'.$href_1.'" target="_blank">查看详情</a></div>';
			}else{
				$_html .= '<div class="daojishi">';
				$_html .= '<div class="line tit">谁会是本期的幸运者</div>';
				$_html .= '<div class="line">获奖者：<span class="username">马上揭晓</span></div>';
				$_html .= '<div class="line">本期幸运号码：<span class="usercode">马上揭晓</span></div>';
				$_html .= '<div class="line times">倒计时：';
				$_html .= '<span class="red" id="fen"></span><span class="djs"> : </span>';
				$_html .= '<span class="red" id="miao"></span><span class="djs"> : </span>';
				$_html .= '<span class="red" id="haomiao"></span></div>';
				$_html .='<div class="xjiexiao_seedetail"><a href="'.$href_2.'" target="_blank">查看详情</a></div>';
				$_html .= '</div>';
				$_html .= '<script type="text/javascript">';
				$_html .= 'var times = "'.$q_time.'";';
				$_html .= 'var shopid = "'.$arr_list['id'].'";';
				$_html .= '$(function(){';
				$_html .= 'times = (new Date().getTime())+(parseInt(times))*1000;';
				$_html .= 'gg_show_Time_fun(times);';
				$_html .= '});';
				$_html .= '</script>';
			}
			die($_html);

		}
		
	   
		
	}
	
	//手机端下载APP页面   （扫码跳转过来）
	public function scancode(){
		
		include templates("index","scancode");
	}

	
	//大转盘提交手机号码
	public function submit_mobile()
	{
	    if(!is_ajax()){
	        die('error,操作失败！');
	    }
	    
	    $mobile = isset($_POST['mobile']) ? trim($_POST['mobile']) : '';
	    $jiang_item = isset($_POST['jiang_item']) ? trim($_POST['jiang_item']) : '10M流量';
	    
	    if(!_checkmobile($mobile)){
	        die("error,手机号码不合法！");
	    }
	    $rows = @file_get_contents("dzp_mobile.txt");
	    if(strpos($rows,$mobile)!==false){
	        die("error,该手机号已参与过本次活动！");
	    }else{
	        setcookie('is_send','1',time()+3600,'/');
	        setcookie('lingqu','1',time()+3600,'/');
    	    $temp = $mobile . '|' . $jiang_item . '|' . time() . ',';
    	    @file_put_contents("dzp_mobile.txt",$temp,FILE_APPEND);
    	    $arr = array('500M流量'=>'一','100M流量'=>'二','80M流量'=>'三','50M流量'=>'四','30M流量'=>'五');
    	    $role = $arr[$jiang_item];
    	    $content = "【欢乐颂】恭喜您获得".$role."等奖".$jiang_item."，流量于次月自动充值到您手机中。手机搜索下载“易中夺宝”APP，一元赢取iPhone6S！回N退订";  //“恭喜您获得二等奖，该奖项可以兑换80M流量，流量将于次月自动到账您手机账号，么么哒！”回T退订
    	    ycsend_message($mobile,$content);    //亿晨短信
    	    die("success,领取成功！");
	    }
	}
	
	
	//苹果6s购买中转   20160111  start
	public function jump()
	{
	    $sid = isset($_REQUEST['sid']) ? intval($_REQUEST['sid']) : '2991';        //苹果6s
	    if($sid){
	        $gid = $this->db->GetOne("SELECT id FROM @#_shoplist WHERE `sid`='$sid' AND `q_uid` IS NULL ORDER BY id DESC LIMIT 1");
	        if(isset($gid['id'])){
	            
	            /* 统计pv,uv 20160109 start */
	            $this->set_pv();
	            $this->set_uv();
	            /* 统计pv,uv 20160109 end */
	            
	            if(_is_mobile()){
	                header("location:".G_WEB_PATH.'/yungou/index.html#/tab/productDetail?id='.$gid['id']);
	            }else{
	                header("location:".G_WEB_PATH."/index.php/goods/".$gid['id']);
	            }
	        }
	    }else{
	        header("location:".G_WEB_PATH);
	    }
	}
	
	//下载ios版易中夺宝app 中转 20160112 start
	public function download_ios()
	{
	    /* 统计pv,uv 20160109 start */
	    $this->set_pv();
	    $this->set_uv();
	    /* 统计pv,uv 20160109 end */
	    
	    header("location:http://itunes.apple.com/cn/app/id1071871513?mt=8");   //下载链接
	}
	
	
	//ajax设置su  针对wap页面
	public function ajax_set_su()
	{
	    if( ! is_ajax()){
	        die();
	    }
	    
	    $su = isset($_GET['su']) ? trim($_GET['su']) : '';
	    $page = isset($_GET['page']) ? trim($_GET['page']) : '';
	    
	    /* 数据统计 20160108 start */
	    set_su_sk();
	    /* 数据统计 20160108 end */
	    
	    /* 统计pv,uv 20160109 start */
	    $this->set_pv($page);
	    $this->set_uv($page);
	    /* 统计pv,uv 20160109 end */
	    
	}
	
	/**
	 * @param $id 	当前期数的产品id
	 * @param $sid	第一期产品id（父id）
	 */
	public function item_list_nav($id,$sid){
		//本产品所有的期数
		$All_list = $this->db->GetList("select id,sid,qishu,q_uid,q_showtime from `@#_shoplist` where sid='$sid' order by qishu DESC");
		//期数显示
		$itemlist = array_slice($All_list,0,9);
	
		//$itemlist = $this->db->GetList("select id,sid,q_uid,qishu from `@#_shoplist` where `sid`='$sid' order by qishu DESC LIMIT 9");
		//总共有多少期
		$item_count = $this->db->GetOne("select count(1) from `@#_shoplist` where `sid`='$sid'");

		//期数导航start
		$qishu_list='';
		foreach($itemlist as $key => $value){
				
			if($value['id']==$id){
				$qishu_list.='<li class="now ">';
			}else{
				$qishu_list.='<li>';
			}
			if($key==0){
				$qishu_list.='<a href="'.WEB_PATH.'/goods/'.$value['id'].'">第'.$value['qishu'].'期进行中<span style="color:red;" class="dotting"></span></a>';
			}else{
				if(is_null($value['q_uid'])){
					$qishu_list.='<a href="'.WEB_PATH.'/goods/'.$value['id'].'">第'.$value['qishu'].'期(正在揭晓)</a>';
				}else{
					$qishu_list.='<a href="'.WEB_PATH.'/dataserver/'.$value['id'].'">第'.$value['qishu'].'期</a>';
				}
			}
			$qishu_list.='</li>';
		}
		if($item_count['count(1)']>9){
			$qishu_list.='<li id="li_more"><a title="更多" href="javascript:;">更多<i>+</i></a></li>';
		}
		//期数导航end
		return array('all_list'=>$All_list,'qishu_list'=>$qishu_list);
	}
	
	public function qishu_href(){
		$sid = intval(htmlspecialchars($_POST['sid']));
		$qishu = intval(htmlspecialchars($_POST['qishu']));
		$result = $this->db->GetOne("select id from `@#_shoplist` where sid='$sid' and  qishu='$qishu'");
		if(!empty($result)){
			echo $result['id'];
		}else{
			echo '0';
		}
	}
	/**
	 * @param 	$uid						用户的id
	 * @param	$sid						产品的id
	 * @return	$user_bought_number 		该用户已经购买本产品的次数
	 */
	public function get_user_can_buy($uid,$sid){	
		//计算用户已经购买的数量
		$sql = "SELECT sum(gonumber) as number FROM `@#_member_go_record` WHERE uid='$uid' AND shopid='$sid'";
		$result = $this->db->GetOne($sql);
		$user_bought_number = intval($result['number']);			//该用户已经购买本产品的次数
		return $user_bought_number;
	}
	
	
	
	
	
	
}
?>