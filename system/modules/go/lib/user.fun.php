<?php
function tubimg($src,$width,$height){
	$url=G_UPLOAD_PATH."/".$src;
	$size=getimagesize($url);
	$name=rand(10,99).substr(microtime(),2,6).substr(time(),4,6);
	$filetype=explode("/",$src);
	$img = imagecreatefromjpeg($url);		
	$dst = ImageCreateTrueColor( $width,$height);
	imagecopyresampled($dst,$img,0,0,0,0,$width,$height,$size[0],$size[1]);											
	imagejpeg($dst,"statics/uploads/".$filetype[0]."/".$filetype[1]."/".$name.".jpg");
	return $filetype[1]."/".$name.".jpg";
}


function get_ip($id,$ipmac=null){
	$db=System::load_sys_class('model');
	$sql = "select * from `@#_member_go_record` where `id`='$id'";
	$record=$db->GetOne("select * from `@#_member_go_record` where `id`='$id'");
	$ip=explode(',',$record['ip']);
	if($ipmac=='ipmac'){
		return $ip[1];
	}elseif($ipmac=='ipcity'){
		return $ip[0];
	}
	return $ip[0].'IP:'.$ip[1];
}

/* 20151221前
function get_database_ip($id){
	$db=System::load_sys_class('model');
	$record=$db->GetOne("select ip from `@#_member_go_record` where `id`='$id'");
	
	$ip = ltrim($record['ip'],",");
	$arr = _GetIpLookupAddress($ip);
	if($arr['province'] == $arr['city']){
		$str = $arr['country'].$arr['province'];
	}else{
		$str = $arr['country'].$arr['province'].$arr['city'];
	}
	return $str;
}
*/
function get_database_ip($id,$type="all"){ 
    $db=System::load_sys_class('model');
    $record=$db->GetOne("select ip from `@#_member_go_record` where `id`='$id'");

    $ip = explode(',',$record['ip']);   //直接用逗号分割就行  20151221 kpf
    if($type=='address'){
        $str = $ip[0];
    }else{
        $str = $ip[0] . ' IP:'. end($ip);
    }
    return $str;
}

function sdimg($sd_id){
	$mysql_model=System::load_sys_class('model');
	$shaidan=$mysql_model->GetOne("select * from `@#_shaidan` where `sd_id`='$sd_id'");
	$img=explode(";",$shaidan['sd_photolist']);
	$ul_li="";
	for($i=0;$i<count($img)-1;$i++){
		$ul_li.='<li id="ulli_'.$i.'"><img src="'.G_UPLOAD_PATH.'/'.$img[$i].'" width="100" height="100"><input type="hidden" value="'.$img[$i].'"><a href="javascript:;" rel="ulli_'.$i.'">删除</a></li>';
	}
	return $ul_li;
}
function img($img){		
	$img=explode(".",$img);
	return $img[1];
}
function Getlogo(){
	$mysql_model=System::load_sys_class('model');	
	$web_logo=$mysql_model->GetOne("select * from `@#_config` where `name`='web_logo'");
	return $web_logo['value'];
}

function Getheader($type='index'){
	$mysql_model=System::load_sys_class('model');	
	$navigation=$mysql_model->GetList("select * from `@#_navigation` where `status`='Y'  and `type` = '$type' order by `order` DESC");
	$url="";
	if($type=='foot'){
		foreach($navigation as $v){		
			$url.='<a  href="'.WEB_PATH.$v['url'].'">'.$v['name'].'</a><b></b>';
		}
		return $url;
	}
	
	foreach($navigation as $v){		
		$url.='<li class="sort-all" ><a  href="'.WEB_PATH.$v['url'].'">'.$v['name'].'</a></li>';
	}
	return $url;
}

/*注册和登录的底部*/
function getdaohang_login(){
	$mysql_model=System::load_sys_class('model');	
	$navigation=$mysql_model->GetList("select * from `@#_navigation` where `status`='Y' AND type='foot' AND parentid=0 order by `order` DESC LIMIT 5");
	$url="";
	foreach($navigation as $key=>$v){
		$url .= '<div class="foot-a"><a href="'.WEB_PATH.$v['url'].'">'.$v['name'].'</a></div>';
		if($key == '4'){
			$url .= '<div></div>';
		}else{
			$url .= '<div><img src="'.G_TEMPLATES_STYLE.'/images/login_new/footer_icon_21.png"></div>';
		}
		
	}
	return $url;
}

/*templet2专用函数设置start*/	
function Getheaderb($type='index'){
	$mysql_model=System::load_sys_class('model');	
	$navigation=$mysql_model->GetList("select * from `@#_navigation` where `status`='Y'  and `type` = '$type' order by `order` DESC");
	$url="";
	
	if($type=='foot'){
		foreach($navigation as $v){		
			$url.='<a target="_blank"  href="'.WEB_PATH.$v['url'].'">'.$v['name'].'</a><b></b>';
		}
		return $url;
	}
	$urld=get_web_url();
	if($urld==WEB_PATH){
		$url.='<li class="selected"><a style="padding:15px;padding-right:15px;" href='.WEB_PATH.'>首页</a></li>';
	}
	else{
		$url.='<li class=""><a style="padding:15px;padding-right:15px;" href='.WEB_PATH.'>首页</a></li>';
	}
	foreach($navigation as $v){	
		$urlr=WEB_PATH.$v['url'];
		if($urlr==$urld){
			$url.='<li class="sort-all selected"><span>|</span><a target="_blank" href="'.WEB_PATH.$v['url'].'">'.$v['name'].'</a></li>';
		}else{
			$url.='<li class="sort-all"><span>|</span><a target="_blank" href="'.WEB_PATH.$v['url'].'">'.$v['name'].'</a></li>';
		}			
	}
	return $url;
}

function uidcookie($get_name=null){
	$member = System::load_app_class("base","member");
	$member = $member->get_user_info();
	if(!$member)return false;	
	if(isset($member[$get_name])){
		return $member[$get_name];
	}else{
		return null;
	}
}

//总云购人次
function go_count_renci(){
	$mysql_model=System::load_sys_class('model');	
	$recordx=$mysql_model->GetOne("select * from `@#_caches` where `key` = 'goods_count_num'");	
	return $recordx['value'];
}


//20151219 增加size参数    针对取头像，  size的取值为  3030 或 8080 或 160160
function userid($uid,$zhi,$size=''){
	$mysql_model=System::load_sys_class('model');
	$member=$mysql_model->GetOne("select * from `@#_member` where `uid`='$uid'");		
	if($zhi=='username'){
		if($member['username']!=null){
			return _strcut($member['username'],8,"");
		}else if($member['mobile']!=null){
			return _strcut($member['mobile'],7,"");
		}else{
			return _strcut($member['email'],7,"");
		}
	}else{
		$data = $member[$zhi];
		if($zhi =='img' && $size && $data != 'photo/member.jpg'){
		    $houz = explode('.',$data);
		    $houzui = end($houz);
			$data = str_replace(".", ".".$houzui."_$size.", $data);
		}
		return $data;
	}
}
function quanzid($qzid){
	$mysql_model=System::load_sys_class('model');
	$quanzi=$mysql_model->GetOne("select * from `@#_quanzi` where `id`='$qzid'");
	return $quanzi['title'];
}
function huifu($tzid){
	$mysql_model=System::load_sys_class('model');
	$quanzi=$mysql_model->GetList("select * from `@#_quanzi_hueifu` where `tzid`='$tzid'");
	return count($quanzi);
}
function huati($lex){
	$mysql_model=System::load_sys_class('model');
	$uid=_encrypt(_getcookie('uid'),'DECODE');
	if($lex=='tiezi'){
		$tiezi=$mysql_model->GetList("select * from `@#_quanzi_tiezi` where `hueiyuan`='$uid'");
		return count($tiezi);
	}if($lex=='hueifu'){
		$hueifu=$mysql_model->GetList("select * from `@#_quanzi_hueifu` where `hueiyuan`='$uid'");
		return count($hueifu);
	}
}
function qznum(){
	$mysql_model=System::load_sys_class('model');
	$uid=_encrypt(_getcookie('uid'),'DECODE');
	$member=$mysql_model->GetOne("select * from `@#_member` where `uid`='$uid'");
	$addgroup=rtrim($member['addgroup'],",");
	if($addgroup){
		$group=$mysql_model->GetList("select * from `@#_quanzi` where `id` in ($addgroup)");		
		return count($group);
	}else{
		$group=null;
		return false;
	}	
}
function tztitle($tzid){
	$mysql_model=System::load_sys_class('model');
	$tiezi=$mysql_model->GetOne("select * from `@#_quanzi_tiezi` where `id`='$tzid'");
	return $tiezi['title'];
}



function help($cateid){
	$mysql_model=System::load_sys_class('model');
	$bangzhu=$mysql_model->GetList("select * from `@#_article` where `cateid`='$cateid' and is_my=0");
	$li="";
	foreach($bangzhu as $bangzhutu){			
		$li.='<li><a href="'.WEB_PATH.'/help/'.$bangzhutu['id'].'" class="cur'.$bangzhutu['id'].'"><b></b>'.$bangzhutu['title'].'</a></li>';				
	}
	return $li;
}

/*登录页面底部的banner*/
function Getfooter_x($type='index'){
	$mysql_model=System::load_sys_class('model');	
	$navigation=$mysql_model->GetList("select * from `@#_navigation` where `status`='Y'  and `type` = '$type' order by `order` DESC");
	$_html="";
	$num = count($navigation);
	
	if($type=='foot'){
		foreach($navigation as $key=>$v){
			if($key == ($num-1)){
				$_html .= '<div class="foot-a"><a href="'.WEB_PATH.$v['url'].'">'.$v['name'].'</a></div>';
			}else{
				$_html .= '<div class="foot-a"><a href="'.WEB_PATH.$v['url'].'">'.$v['name'].'</a></div><div><img src="'.G_TEMPLATES_STYLE.'/images/new/footer_icon_21.png"></div>';
			}
			
		}
		return $_html;
	}
}

?>