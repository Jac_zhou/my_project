<?php 
defined('G_IN_SYSTEM')or exit('No permission resources.');
System::load_app_class('base','member','no');
System::load_app_fun('my');
System::load_app_fun('user');
System::load_sys_fun('user');

class lemon extends base {
	private $title;
	private $keywords;
	private $description;
	public function __construct() {	
		parent::__construct();
		$this->title = "易中夺宝-好运连连";
		$this->keywords = "1元,一元,一元云购,1元云购,1元云购,易中夺宝,1元云购,易中夺宝,1元云购官网,一元云购官网,云购奇兵,云购,一元购,1元购,云购商城";
		$this->description = "易中夺宝是一个引入众筹理念的新型电子商务平台，手机电脑、数码科技、潮流新品、家居电器，只需1元即有机会获取，凭借技术实力和算法公平，确保100%公平公正、100%正品保障，引领集娱乐、购物于一体的网购潮流。";
		
		/* 数据统计 20160108 start */
		 set_su_sk();
		/* 数据统计 20160108 end */
	}
	public function index(){
	    
	    if(is_mobile_request()){
	        header("location:".G_WEB_PATH.'/yungou/index.html#/tab/lemon');
	    }
	
		$title = $this->title;
		$title = $this->keywords;
		$description = $this->description;
		$su = isset($_GET['su']) ? trim($_GET['su']) : '';
		$sk = isset($_GET['sk']) ? trim($_GET['sk']) : '';
		
		if($sk && $su){
			$url_str = "?su=".$su."&sk=".$sk;
		}else{
			$url_str = '';
		}
		
		/* 统计pv 20160109 start */
		$this->set_pv();
		/* 统计pv 20160109 end */
		
		include templates("lemon","index");
	}
	
	public function detail(){
	    $from = $_GET['f'];				//来源
	    //$this->tongji($from);
	    if(is_mobile_request()){
	        header("location:".G_WEB_PATH.'/yungou/index.html#/tab/lemon?f='.trim($_GET['f']));
	    }
		if($from){
			$UvpvCon = System::load_contorller('Uvpv','Zapi');
			$result = $UvpvCon->tongji($from,'pc');
		}
		$title = $this->title;
		$title = $this->keywords;
		$description = $this->description;
		
	    $su = isset($_GET['su']) ? trim($_GET['su']) : '';
		$sk = isset($_GET['sk']) ? trim($_GET['sk']) : '';
		
		if($sk && $su){
			$url_str = "?su=".$su."&sk=".$sk;
		}else{
			$url_str = '';
		}
		
		/*中奖信息*/
		$db=System::load_sys_class('model');
		$sql = "select m.*,g.zongrenshu,g.q_end_time,g.q_user,g.thumb from `@#_member_go_record` AS m LEFT JOIN `@#_shoplist` AS g ON m.shopid=g.id WHERE `huode`>'10000000' ORDER BY m.time DESC LIMIT 8";
		//$sql = 'SELECT id,zongrenshu,title,qishu,canyurenshu,shenyurenshu,thumb FROM `@#_shoplist` WHERE is_shi=1 ORDER BY time DESC';
		$zhongjian_list = $db->GetList($sql);
		
		/* 统计pv,uv 20160109 start */
		$this->set_pv();
		$this->set_uv();
		/* 统计pv,uv 20160109 end */
		
		include templates("lemon","detail");
	}
	
	public function shai_share(){
		$member_uid = $_COOKIE['uid'];
		$demember_uid = _encrypt($member_uid,"DECODE");
		if($demember_uid){
			$rul = WEB_PATH."/member/home/singlelist";
			header("Location:".$rul);
		}else{
			$drul = WEB_PATH."/login";
			header("Location:".$drul);
	
		}
	}
	
	//立即充值
	public function chongzhi(){
		$member_uid = $_COOKIE['uid'];
		$demember_uid = _encrypt($member_uid,"DECODE");
		if($demember_uid){
			$rul = WEB_PATH."/member/home/userrecharge";
			header("Location:".$rul);
		}else{
			$drul = WEB_PATH."/login";
			header("Location:".$drul);
		}
	}	
	
	
	//最新揭晓  活动页面调用
	public function get_new_lottery()
	{
	    if(is_ajax()){
	        $db=System::load_sys_class('model');
	        
	        $sql = "select m.username,m.shopname,m.shopqishu as qishu,g.thumb from `@#_member_go_record` AS m LEFT JOIN `@#_shoplist` AS g ON m.shopid=g.id WHERE `huode`>'10000000' ORDER BY m.time DESC LIMIT 8";
	        $list = $db->GetList($sql);
	        
	        if(count($list)){
    	        foreach($list as $k => $v){
    	            $list[$k]['shopname'] = str_replace('&nbsp;', ' ', $v['shopname']);
    	            $hz = explode('.',$v['thumb']);
    	            $houzui = end($hz);
    	            $list[$k]['thumb'] = G_UPLOAD_PATH . '/' . $v['thumb'] . '_200200.' . $houzui;
    	        }
    	        $data = array('status'=>1,'data'=>$list);
    	        die(json_encode($data));
	        }else{
	            $data = array('status'=>0,'data'=>array());
	            die(json_encode($data));
	        }  
	    }else{
	        die(json_encode('error'));
	    }
	}
	
	
}
?>