<?php 
defined('G_IN_SYSTEM')or exit('No permission resources.');
System::load_app_fun('user');
System::load_sys_fun('user');
class article extends SystemAction {
	private $db;
	public function __construct(){
		$this->db=System::load_sys_class('model');
	}
	public function init(){}	
	
	/*显示单篇文章*/
	public function show(){	
		$articleid = safe_replace($this->segment(4));
		$article=$this->db->GetOne("SELECT * FROM `@#_article` where `id` = '$articleid' and is_my=0 LIMIT 1");
		if($article){
			$sql = "SELECT * FROM `@#_category` where `cateid` = '$article[cateid]' LIMIT 1";
			$cateinfo = $this->db->GetOne("SELECT * FROM `@#_category` where `cateid` = '$article[cateid]' LIMIT 1");
		}else{
			$cateinfo = array("info"=>null);
		}		
		$info=unserialize($cateinfo['info']);
		$title = $article['title']."_"._cfg("web_name");
		$keywords=$article['keywords'];
		$description=$article['description'];
		if(empty($info['template_show'])){
			$info['template_show'] = "article_show.help.html";
		}

		$template=explode('.',$info['template_show']);
		include templates($template[0],$template[1]);
		
	}

	/*列表页面*/
	public function lists(){
		$single= safe_replace($this->segment(4));
		if(intval($single)){			
			$article=$this->db->GetOne("SELECT * FROM `@#_category` where `cateid` = '$single' LIMIT 1");
		}else{		
			$article=$this->db->GetOne("SELECT * FROM `@#_category` where `catdir` = '$single' LIMIT 1");
		}
		if(!$article){_message("参数错误!");}
		
		$cid = $article['cateid'];
		$cids = "'$cid',";
		$cateinfos = $this->db->GetList("SELECT cateid,parentid,cids,name FROM `@#_category` where `cids` LIKE '%$cid,%'",array("key"=>"cateid"));
		$cateinfos[$cid]=$cateinfos;
		foreach($cateinfos as $v){
			$cids .= "'".$v['cateid']."',";
		}
		$cids = rtrim($cids,",");
		
		$info=unserialize($article['info']);
		if(!isset($info['template_list'])){
			$info['template_list'] = "article_list.list.html";
		}

		$article['thumb']=$info['thumb'];
		$article['des']=$info['des'];
		$article['content']= base64_decode($info['content']);
		$title=empty($info['meta_title']) ? $article['name'] : $info['meta_title'];
		$keywords=$info['meta_keywords'];
		$description=$info['meta_description'];
		$template=explode('.',$info['template_list']);
		include templates($template[0],$template[1]);
	}
	
	/*显示单网页和列表页面*/
	public function single(){
		$single = safe_replace($this->segment(4));
		if(intval($single)){			
			$article=$this->db->GetOne("SELECT * FROM `@#_category` where `cateid` = '$single' LIMIT 1");
		}else{		
			$article=$this->db->GetOne("SELECT * FROM `@#_category` where `catdir` = '$single' LIMIT 1");
		}
		if(!$article){_message("参数错误!");}		
		
		$info=unserialize($article['info']);

		$article['thumb']=$info['thumb'];
		$article['des']=$info['des'];
		$article['content']= base64_decode($info['content']);
		$title=empty($info['meta_title']) ? $article['name'] : $info['meta_title'];
		$keywords=$info['meta_keywords'];
		$description=$info['meta_description'];
		
		if($single=='newbie'){	//新手指南页面的关键词、标题、描述
			$title = "新手指南-易中夺宝";
			$keywords = "新手指南，一元购，一元云购,1元云购,1元云购,一元云购,1元云购,一元云购,云购,1元购物,1元购电脑,1元购笔记本,云购商城";
			$description = "易中夺宝是一个引入众筹理念的新型电子商务平台，手机电脑、数码科技、潮流新品、家居电器，只需1元即有机会获取，凭借技术实力和算法公平，确保100%公平公正、100%正品保障，引领集娱乐、购物于一体的网购潮流。";
		}
		$template=explode('.',$info['template']);
		include templates($template[0],$template[1]);
	}
}


?>