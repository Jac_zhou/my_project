<?php 
defined('G_IN_SYSTEM')or exit('No permission resources.');
System::load_app_class('base','member','no');
System::load_app_fun('my');
System::load_app_fun('user');
System::load_sys_fun('user');

class ten extends base {
	
	public function __construct() {	
		parent::__construct();
		$this->db=System::load_sys_class('model');	
		/* 数据统计 20160108 start */
		set_su_sk();
		/* 数据统计 20160108 end */
	}		
	
	//十元专区
	public function index(){
		
		//页面标题、关键词、描述等配置
		$title = "限购专区-易中夺宝";
		$daohang_title = "限购专区";
		$ShoplistModel = System::load_app_model('Shoplist','Zapi');
		$GoodListCon = System::load_contorller('GoodList','Zapi');
		
		$field = 'title,id,sid,thumb,money,zongrenshu,canyurenshu,shenyurenshu,default_renci,yunjiage,qishu,is_shi';
		$where = 'q_uid IS NULL AND is_shi=1 AND q_showtime="N"';
		$shoplist = $ShoplistModel->get_good_list($where,$field);
		
		$shoplist = $GoodListCon->list_add_user_bought_number($shoplist,$this->userinfo['uid']);
		
		/* 统计pv,uv 20160109 start */
		$this->set_pv();
		$this->set_uv();
		/* 统计pv,uv 20160109 end */
		
		include templates("index","ten");
	}
	
}
?>