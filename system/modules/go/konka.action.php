<?php 
defined('G_IN_SYSTEM')or exit('No permission resources.');
System::load_app_class('base','member','no');
System::load_app_fun('my');
System::load_app_fun('user');
System::load_sys_fun('user');

class konka extends base {
	private $title;
	private $keywords;
	private $description;
	public function __construct() {	
		parent::__construct();
		$this->title = "易中夺宝-好运连连";
		$this->keywords = "1元,一元,易中夺宝,1元云购,1元云购,易中夺宝,1元云购,一元云购,1元云购官网,易中夺宝官网,云购奇兵,云购,一元购,1元购,云购商城";
		$this->description = "易中夺宝是一个引入众筹理念的新型电子商务平台，手机电脑、数码科技、潮流新品、家居电器，只需1元即有机会获取，凭借技术实力和算法公平，确保100%公平公正、100%正品保障，引领集娱乐、购物于一体的网购潮流。";
		
		/* 数据统计 20160108 start */
		 set_su_sk();
		/* 数据统计 20160108 end */
	}
	public function init(){
		$title = $this->title;
		$title = $this->keywords;
		$description = $this->description;
		$su = isset($_GET['su']) ? trim($_GET['su']) : '';
		$sk = isset($_GET['sk']) ? trim($_GET['sk']) : '';
		if($sk && $su){
			$url_str = "?su=".$su."&sk=".$sk;
		}else{
			$url_str = '';
		}
		
		$kangjia_1 = $this->get_zuixin_info(3087);
		$kangjia_2 = $this->get_zuixin_info(3088);
		$renqi_list = json_decode($this->goods_renqi(true),true);
		
		$lotter_list = json_decode($this->get_lottery_list(),true);
		
		/* 统计pv,uv 20160109 start */
		$this->set_pv();
		$this->set_uv();
		/* 统计pv,uv 20160109 end */
		
		if(_is_mobile()){
		    include templates("konka","index.m");
		}else{
		    include templates("konka","index");
		}
	}
	
	//人气商品
	public function goods_renqi($is_sys = false){
		$db=System::load_sys_class("model");
		$sql = "SELECT id,title,keywords,thumb,money FROM `@#_shoplist` WHERE renqi=1 AND q_uid IS NULL order by `order` DESC LIMIT 6";
		$arr_list = $db->GetList($sql);
		return json_encode($arr_list);
	}
	
	//获取10条获奖记录
	public function get_lottery_list()
	{
		$db=System::load_sys_class("model");
		$wh = ' `q_uid` IS NOT NULL AND `q_showtime` = "N" ORDER BY `q_end_time` ';
	
		$sql = "SELECT id,sid,q_user,title,q_end_time,qishu FROM `@#_shoplist` WHERE $wh DESC  LIMIT 10";
		$lottery_list = $db->GetList($sql);
		if(count($lottery_list)){
			foreach($lottery_list as $k => $v){
				$lottery_list[$k]['title'] = str_replace('&nbsp;',' ',$lottery_list[$k]['title']);
				$lottery_list[$k]['q_user'] = unserialize($v['q_user']);
				$lottery_list[$k]['username'] = get_user_name($lottery_list[$k]['q_user']['uid']);	//取用户名
				$temp = explode('.',$v['q_end_time']);
				$q_end_time = $temp[0];
				$lottery_time = $this->lotteryTime($q_end_time);	//计算揭晓中奖时间
				$lottery_list[$k]['content'] = "<span>". $lottery_time ."</span>前获得";
				
				unset($lottery_list[$k]['q_user'],$lottery_list[$k]['q_end_time'],$temp,$username);
			}
		}
		if(is_ajax()){
		    die(json_encode($lottery_list));
		}
		return json_encode($lottery_list);
	}
	
	//计算中奖时间
	private function lotteryTime($target = '')
	{
		$result = '';
		if( ! empty($target) ){
			$d = 86400;
			$h = 3600;
			$i = 60;
			$now_time = time();
			$surplus = $now_time - $target;
			if($surplus >= $d){
				$result = intval($surplus/$d) . '天';
			}elseif($surplus >= $h && $surplus < $d){
				$result = intval($surplus/$h) . '小时';
			}elseif($surplus >= $i && $surplus < $h){
				$result = intval($surplus/$i) . '分钟';
			}else{
				$result = $surplus . '秒';
			}
		}
		return $result;
	}
	
	//得到最新一期
	private function get_zuixin_info($sid){
		$sql = "SELECT * FROM `@#_shoplist` WHERE sid=$sid AND q_uid IS NULL limit 1";
		$db=System::load_sys_class("model");
		$arr = $db->GetOne($sql);
		return $arr['id'];
	}

}
?>