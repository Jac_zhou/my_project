<?php 

defined('G_IN_SYSTEM')or exit('no');
System::load_app_fun('global',G_ADMIN_DIR);
System::load_app_fun('my','go');
System::load_app_fun('user','go');
System::load_app_class("base","member","no");
System::load_sys_fun('user');
class shaidan extends base {
	public $db;
	public function __construct(){	
		parent::__construct();
		$this->db=System::load_sys_class('model');		
		/* 数据统计 20160108 start */
		set_su_sk();
		/* 数据统计 20160108 end */
	}
	
	//晒单分享
	public function init(){
		$title = "晒单分享 - 易中夺宝";
		$keywords = "易中夺宝,1元财神,1元财神,易中夺宝,1元云购,易中夺宝,云购,一元购,1元购,晒单,晒单分享,云购商城";
		$description = "易中夺宝是一个引入众筹理念的新型电子商务平台，手机电脑、数码科技、潮流新品、家居电器，只需1元即有机会获取，凭借技术实力和算法公平，确保100%公平公正、100%正品保障，引领集娱乐、购物于一体的网购潮流。";
			
		$num=40;
		$total=$this->db->GetCount("select * from `@#_shaidan` where `is_audit` = 2 ");
		$page=System::load_sys_class('page');	
		if(isset($_GET['p'])){
			$pagenum=$_GET['p'];
		}else{$pagenum=1;}			
		$page->config($total,$num,$pagenum,"0");
		if($pagenum>$page->page){
			$pagenum=$page->page;
		}	
		$shaidan=$this->db->GetPage("select * from `@#_shaidan` where `is_audit` = 2 order by `sd_id` DESC",array("num"=>$num,"page"=>$pagenum,"type"=>1,"cache"=>0));
		$lie=4;$sum=$num;
		$yushu=$total%$num;
		$yeshu=floor($total/$num)+1;
		if($yushu>0 && $yeshu==$pagenum){
			$sum=$yushu;
		}
		$sa_one=array();$sa_two=array();
		$sa_tree=array();$sa_for=array();		
		
		foreach($shaidan as $sk=>$sv){
			$shaidan[$sk]['sd_title'] = emj_decode(_htmtocode($sv['sd_title']));	
			$shaidan[$sk]['sd_content'] = emj_decode($sv['sd_content']);	
		}
		if($shaidan){
			for($i=0;$i<$lie;$i++){
				$n=$i;			
				while($n<$sum){
					if($i==0){
						$sa_one[]=$shaidan[$n];
					}else if($i==1){
						$sa_two[]=$shaidan[$n];

					}else if($i==2){
						$sa_tree[]=$shaidan[$n];
					}else if($i==3){
						$sa_for[]=$shaidan[$n];
					}
					$n+=$lie;
				}
			}
		}
		
		
		/* 统计pv,uv 20160109 start */
		$this->set_pv();
		$this->set_uv();
		/* 统计pv,uv 20160109 end */
		
		include templates("index","shaidan");
	}
	
	public function detail(){					
		$member=$this->userinfo;	
		$sd_id=abs(intval($this->segment(4)));
		$shaidan=$this->db->GetOne("select * from `@#_shaidan` where `sd_id`='$sd_id'");
		$goods = $this->db->GetOne("select sid from `@#_shoplist` where `id` = '$shaidan[sd_shopid]'");
		$goods = $this->db->GetOne("select id,qishu,money,q_uid,maxqishu,thumb,title from `@#_shoplist` where `sid` = '$goods[sid]' order by `qishu` DESC");

							
		$shaidannew=$this->db->GetList("select * from `@#_shaidan` order by `sd_id` DESC limit 5");
		$shaidan_hueifu=$this->db->GetList("select * from `@#_shaidan_hueifu` where `sdhf_id`='$sd_id' LIMIT 10");
		
		foreach($shaidan_hueifu as $k=>$v){
			$shaidan_hueifu[$k]['sdhf_content'] = _htmtocode($shaidan_hueifu[$k]['sdhf_content']);
		}	
	
		if(!$shaidan){
			_message("页面错误");
		}
		$substr=substr($shaidan['sd_photolist'],0,-1);
		$sd_photolist=explode(";",$substr);
		
		$title = $shaidan['sd_title'] . "_" . _cfg("web_name");
		$keywords = $shaidan['sd_title'];
		$description = $shaidan['sd_title'];
		
		/* 统计pv,uv 20160109 start */
		$this->set_pv();
		$this->set_uv();
		/* 统计pv,uv 20160109 end */
		
		include templates("index","detail");
	}
	
	/*2015/12/09改添加晒单评论*/
	function shaidan_comment(){
		$member=$this->userinfo;
		if(!$member){
			$url = WEB_PATH."islogin=0";
			header('Location:'.$url);
		}
				
		$info = array();
		$sdhf_id=isset($_POST['sd_id']) ? intval($_POST['sd_id']) : '';
		if(empty($sdhf_id)){
			$info = array('error'=>1,'msg'=>'你来晚了，晒单已经不存在了');
			die(json_encode($info));
		}
		
		//判断该用户是否已经评论过了
		$sql = "SELECT id FROM `@#_shaidan_hueifu` WHERE sdhf_id='$sdhf_id' AND sdhf_userid ='$member[uid]'";
		$arr_one = $this->db->GetOne($sql);
		if($arr_one){
			$info = array('error'=>1,'msg'=>'*您已评论过');
			die(json_encode($info));
		}
		$sdhf_syzm = _getcookie("checkcode");			
		$sdhf_pyzm = isset($_POST['sdhf_code']) ? strtoupper(trim($_POST['sdhf_code'])) : '';				
		
		$sdhf_userid=$member['uid'];		
		$sdhf_content=trim($_POST['sdhf_content']);			
		$sdhf_time=time();
		$sdhf_username = _htmtocode(get_user_name($member));
		$sdhf_img = _htmtocode($member['img']);
		
		if(empty($sdhf_pyzm)){
			$info = array('error'=>1,'msg'=>'*请输入验证码');
			die(json_encode($info));
		}
		if($sdhf_syzm !=md5($sdhf_pyzm)){
			$info = array('error'=>1,'msg'=>'*验证码不正确');
			die(json_encode($info));
		}
		if(empty($sdhf_content)){
			$info = array('error'=>1,'msg'=>'*请输入评论内容！');
			die(json_encode($info));
		}
		
		$shaidan = $this->db->GetOne("select * from `@#_shaidan` where `sd_id`='$sdhf_id'");
			
		$this->db->Query("INSERT INTO `@#_shaidan_hueifu`(`sdhf_id`,`sdhf_userid`,`sdhf_content`,`sdhf_time`,`sdhf_username`,`sdhf_img`)VALUES
		('$sdhf_id','$sdhf_userid','$sdhf_content','$sdhf_time','$sdhf_username','$sdhf_img')");
		$sd_ping=$shaidan['sd_ping']+1;
		$this->db->Query("UPDATE `@#_shaidan` SET sd_ping='$sd_ping' where sd_id='$sdhf_id'");
		$info = array('error'=>0,'msg'=>'评论成功','url'=>WEB_PATH."/go/shaidan/detail/".$sdhf_id);
		die(json_encode($info));
	}
	
	public function xianmu(){
		$sd_id=intval($_POST['id']);
		$shaidan=$this->db->GetOne("select * from `@#_shaidan` where `sd_id`='$sd_id'");
		$sd_zhan=$shaidan['sd_zhan']+1;
		$this->db->Query("UPDATE `@#_shaidan` SET sd_zhan='".$sd_zhan."' where sd_id='".$sd_id."'");
		echo $sd_zhan;
	}
	
		
	/*商品晒单列表ifram*/
	public function itmeifram(){
	
		$itemid=safe_replace($this->segment(4));		
		$item=$this->db->GetOne("select id,sid,qishu from `@#_shoplist` where `id`='$itemid' LIMIT 1");		
		
		if(!$item){
			$error = 1;
		}else{			
				$error = 0;
				$page=System::load_sys_class('page');		
				$total=$this->db->GetCount("select id from `@#_shaidan` where `sd_shopsid`='$item[sid]' and `is_audit`=2 ");
				if(!$total){
					$error = 1;
				}
				if(isset($_GET['p'])){
					$pagenum=$_GET['p'];
				}else{
					$pagenum=1;
				}
				$num=10;
				$page->config($total,$num,$pagenum,"0");
				$shaidan=$this->db->GetPage("select * from `@#_shaidan` where `sd_shopsid` = '$item[sid]' and `is_audit`=2 order by sd_id  DESC",array("num"=>$num,"page"=>$pagenum,"type"=>1,"cache"=>0));	

				foreach($shaidan as $key=>$val){
					$member_info=$this->db->GetOne("select * from `@#_member` where `uid`='$val[sd_userid]'");
					$member_img[$val['sd_id']]=$member_info['img'];
					$member_id[$val['sd_id']]=$member_info['uid'];
					$member_username[$val['sd_id']]=$member_info['username'];
				}	
		
		}		
		
		include templates("index","itemifram"); 
	}
}
?>