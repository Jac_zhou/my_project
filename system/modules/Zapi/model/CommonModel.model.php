<?php
System::load_sys_class('model');
//不支持锁机制
Class CommonModel extends model{


    /**
     * @param $map              更新条件
     * @param $data             更新数据
     * @param $table_name       表名
     * @return array
     */
    public function update_table_data($map,$data,$table_name){

        $where = '';
        foreach($map as $key=>$value){
            $where .= $key."='".str_replace("'","_", $value)."' and ";
        }
        $where 	= rtrim($where,' and ');
        $data_set = '';
        foreach($data as $key=>$value){
            $data_set .= $key."='".str_replace("'","_", $value)."',";
        }
        $data_set 	= rtrim($data_set,',');
        $sql = 'update '.$table_name.' set '.$data_set.' WHERE '.$where;

        return $this->Query($sql);
    }

    /**查询数据列表
     * @param $map              查询条件
     * @param $table_name       表名
     * @param $order            排序方式
     */
    public function select_table_list($map,$filed='*',$table_name,$order){
        $where = '';
        if(isset($map{0})){
            foreach($map as $key=>$value){
                $where .= $key."='".str_replace("'","_", $value)."' and ";
            }
            $where = rtrim($where,' and ');
            $where = 'where '.$where.' ';
        }
        $sql = 'select '.$filed.' from `'.$table_name.'` '.$where.' '.$order;
        return $this->GetList($sql);
    }
    /**查询数据列表
     * @param $map              查询条件
     * @param $table_name       表名
     * @param $order            排序方式
     */
    public function select_table_one($map,$filed='*',$table_name,$order = 'limit 1'){
        $where = '';
        foreach($map as $key=>$value){
            $where .= $key."='".str_replace("'","_", $value)."' and ";
        }
        $where 	= rtrim($where,' and ');

        $sql = 'select '.$filed.' from '.$table_name.' where '.$where.' '.$order;

        return $this->GetOne($sql);
    }

    /**
     * @param $data             需要插入的数据 array('字段'=>'值')
     * @param $table_name       插入某表
     * @return mixed            返回插入数据的id
     */
    public function insert_to_table($data,$table_name){
        $field = '';
        $val= '';

        foreach($data as $key=>$value){

            $field .= "`".str_replace("'","_", $key)."`,";
            $val   .= "'".str_replace("'","_", $value)."',";
        }

        $field = rtrim($field,',');
        $val   = rtrim($val,',');

        $sql = 'insert 	into '.$table_name.' ('.$field.') values('.$val.')';
        $this->Query($sql);
        return $this->insert_id();
    }








}









