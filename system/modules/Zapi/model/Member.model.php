<?php   
System::load_sys_class('model');

Class Member extends model{
	
	public 	$table_name='Member';
	private $field = '*';
	private $where = '';
	
	
	public function select_arr($data,$field='*'){
		
		$where = '';
		foreach($map as $key=>$value){	
			$where .= $key."='".str_replace("'","_", $value)."' and ";	
		}
		$where 	= rtrim($where,' and ');
		
		//$sql = "select $field from `@#_member_band` where $where";
		
		
		$this->where =  $where;
		
		return $this;
	}
	
	public function select(){
		
		$sql = 'select '.$this->field.' from '.$this->table_name.' where '.$this->where;
		
		$this->GetOne($sql);
	}
	
	public function get_one($map,$field='*'){
		$where = '';
		foreach($map as $key=>$value){	
			$where .= $key."='".str_replace("'","_", $value)."' and ";	
		}
		$where 	= rtrim($where,' and ');
		
		$sql = "select $field from `@#_member` where $where";
		
		$result = $this->GetOne($sql);
		
		return $result;
	}
	
	public function insert_into($data){
		$field = '';
		$val= '';
		
		foreach($data as $key=>$value){
			
			$field .= "`".str_replace("'","_", $key)."`,";
			$val   .= "'".str_replace("'","_", $value)."',";
		}
		
		$field = rtrim($field,',');
		$val   = rtrim($val,',');
		
		$sql = "insert 	into `@#_member`($field) values($val)";
		$this->Query($sql);
		$id=$this->insert_id();
		return $id;
	}
	
	/**
	 * @param $where   array()
	 */
	
	public function get_member_info($map,$field='*'){
		$where = '';
		foreach($map as $key=>$value){	
			$where .= $key."='".str_replace("'","_", $value)."' and ";	
		}
		$where 	= rtrim($where,' and ');
		$sql 	= "select $field from `@#_member` where $where";
		$result = $this->GetOne($sql);
		
		return $result;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
