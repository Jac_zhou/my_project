<?php
defined('G_IN_SYSTEM')or exit('no');
Class Send extends SystemAction{
	public 	$model;
	private $mobile;		//手机号
	private $content;		//要发送的内容
	private $type;			//发送类型
	private $call='4008-117-233';//客服电话
	private $code;			//验证码
	
	public function __get($name){
		return $this->$name;
	}
	public function __set($name,$value){
		$this->$name = $value;
	}
	public function __construct(){
		
		$this->model = System::load_sys_class('model');
		
	}

	public function send_msg(){
		$type = $this->type;		//发送类型
		//中奖
		if($type==1){
			//获取中奖用户手机号信息
			$data = $this->get_mobile_msg();
			if($data){
				$this->send_win_mobile($data);	
				$result = $this->send();//发送 
				return $result;die;
			}else{
				_message('数据出错');
			}
		}else{
			
			if($this->mobile && $this->code){
				
				$time   = time();
				$mobile = $this->mobile;
				$code   = $this->code;
				$sql="select time from @#_mobile_code where mobile='$mobile' and type='$type'";
				$result = $this->model->Query($sql);
				
				if(!empty($result)&& $result['time']>0){
					if($time-$result['time']>60){
						return false;die;
					}
				}else{
					
					//将手机号、验证码、时间戳插入数据库
					$sql = "INSERT INTO `@#_mobile_code`(mobile,mobilecode,type,time) VALUES('$mobile','$code','$type','$time')";
					$this->model->Query($sql);
					
				}
				
				//注册验证码
				if($type==2){
					$this->send_mobile_reg_code();
				}
				//找回密码验证码
				if($type==3){
					$this->send_mobile_pwd_code();
					//更新验证码	
					$this->model->Query("UPDATE `@#_member` SET passcode='$code' where `mobile`='$mobile'");
				}
				//账号绑定手机验证码
				if($type==4){
					$this->send_mobile_change_code();
					//更新验证码	
				}
				$sql = "update  @#_mobile_code set mobilecode='$code',time='$time' where mobile='$mobile' and type='$type'";
				
				$result = $this->model->Query($sql);
				//return $result;die;
				if($result){
					$result=$this->send();//发送
				}
				return $result;	
			}else{
				return false;die;
			}
		}
	}
	/**
	 * 中奖信息
	 */
	//发送手机消息
	private function get_mobile_msg(){
		$uid = intval($this->uid);
		$gid = intval($this->gid);
		if($uid>0 && $gid>0){
			//查询中奖用户信息
			$sql = "select mobile,email,username from @#_member where uid='$uid'";
			$result = $this->model->GetOne($sql);
			$this->mobile = $result['mobile'];
			$this->email  = $result['email'];
			$sql = "select qishu,title,q_user_code from @#_shoplist where id='$gid'";
			$data = $this->model->GetOne($sql);
			$data['username']=$result['username'];
			//return $data;die;
			if(empty($data)){//信息出现错误
				return false;
			}else{
				return $data;
			}
		}else{
			return false;
		}
	}
	//发送手机注册验证码  template_mobile_reg
	private function send_mobile_reg_code(){
		//获取注册验证码模板
		$content =$this->get_content_template('template_mobile_reg');
		if(!$content || empty($content['value']) ){
			$content =  "【易中夺宝】您好，您的注册验证码是：".strtolower($this->code);
		}else{			
			$content = str_ireplace('000000',strtolower($this->code),$content);			
		}
		$this->content = $content;
	}
	
	//发送手机找回密码验证码  template_mobile_pwd
	private function send_mobile_pwd_code(){
		//获取手机找回密码验证码模板
		$content =$this->get_content_template('template_mobile_pwd');
		if(!$content || empty($content['value']) ){
			$content =  '【易中夺宝】您好，您现在正在找回密码，您的验证码是'.strtolower($this->code).'打死都不要告诉别人哦！';
		}else{			
			$content = str_ireplace('000000',strtolower($this->code),$content);			
		}
		$this->content = $content;
	}
	//发送中奖信息到手机
	private function send_win_mobile($data){
		//获取手机中奖模板
		$content =$this->get_content_template('template_mobile_shop');
		$content = str_ireplace('400-000-000',$this->call,$content);
		/*$content= str_ireplace("{中奖码}",$data['q_user_code'],$content);
		$content= str_ireplace("{商品名称}",$data['title'].'[第'.$data['qishu'].'期]',$content);
		$content= str_ireplace("{用户名}",$data['username'],$content);*/
		$this->content = $content;	
	}
	//更换绑定手机号码验证码 send_mobile_change_code
	private function send_mobile_change_code(){
		//获取手机找回密码验证码模板
		$content =$this->get_content_template('template_mobile_change');
		if(!$content || empty($content['value']) ){
			$content =  "【易中夺宝】您的财神账户正在更换手机绑定，验证码是：".strtolower($this->code)."，打死都不要告诉别人哦！";
		}else{			
			$content = str_ireplace('000000',strtolower($this->code),$content);			
		}
		$this->content = $content;
	}
	
	//获取内容模板
	public function get_content_template($str){
		$sql = "select `value` from @#_caches where `key`='$str'";	
		$result = $this->model->GetOne($sql);
		$content = $result['value'];
		return $content;
	}
	
	//发送
	private function send(){
		if($this->mobile){
			//调用发送消息接口
			$SendMsgCon = System::load_contorller('SendMsg2','api');
			//回调信息
			$result = $SendMsgCon->send_msg($this->mobile,$this->content);
			if($result){
				return true;
			}else{
				return false;
			}
		}
	}
	
	//短信回调地址
	public function msg_notify(){







	}



}
	
	
	
	
	
	
	