<?php  



Class GoodList extends SystemAction{
	
	
	/**
	 * @param	$shopid 商品id	（int）
	 * @param	$uid	用户id	(int)
	 * 
	 * @return  $list	商品的详细信息
	 */
	public function get_goodlist($shopid,$uid){
		
		$ShoplistModel = System::load_app_class('Shoplist','Zapi');
		
		$where = "id='".$shopid."'";
		$field = '*';
		$good_info = $ShoplistModel->get_good_info($where,$field);
		
		
		
		
		
		return $good_info;
	}
	
	/**
	 * @param $goodlist  array   产品列表
	 * 
	 * 
	 */
	public function list_add_user_bought_number($goodlist,$uid){
		if($goodlist){
			if(isset($goodlist['id'])){
				$config = array(
						'id'	 =>$goodlist['id'],
						'uid'	 =>$uid,
						'syrs'	 =>$goodlist['shenyurenshu'],
						'is_shi' =>$goodlist['is_shi'],
						'qishu'	 =>$goodlist['qishu']	
					);
				$data = $this->get_user_can_buy_number($config);
				//return $config;
				$goodlist['wap_ten']=$data['wap_ten'];
				$goodlist['can_buy']=$data['can_buy'];
				
			}else{
				
				foreach($goodlist as $key=>$value){
					
					if(isset($goodlist[$key]['id'])){
						$config = array(
								'id'	 =>$goodlist[$key]['id'],
								'uid'	 =>$uid,
								'syrs'	 =>$goodlist[$key]['shenyurenshu'],
								'is_shi' =>$goodlist[$key]['is_shi'],
								'qishu'	 =>$goodlist[$key]['qishu']	
							);
						$data = $this->get_user_can_buy_number($config);
						
						$goodlist[$key]['wap_ten'] = $data['wap_ten'];
						$goodlist[$key]['bought']  = $data['bought'];
					}
					
				}
	
			}
		}
		return $goodlist;
	}
	
	/**
	 * 获取用户可以购买的数量
	 * @param 	(array)	$config array(
	 * 							id		=>	$id		产品的id			(int)
	 * 							uid 	=> 	$uid   	用户的id			(int)
	 * 							syrs	=>	$syrs	产品的剩余数量		(int)
	 * 							is_ten	=>	$is_ten 是否是限购产品		(int)
	 * 							qishu   =>  $qishu  商品期数			(int)
	 * 							
	 * 							)
	 * @return	(int)	$wap_ten用户剩余的购买数量					
	 */
	public function get_user_can_buy_number($config){
		
		$syrs	=	$config['syrs'];
		$xiangou_money = 5;			//限购数量
	
		if($config['uid'] > 0){
			//获取用户的购买数量	
			$MemberRecord = System::load_app_model('MemberRecord','Zapi');	
			//用户该买该期产品的数量
			$user_bought_number = $MemberRecord->user_bought_number($config['id'],$config['uid']);
			
		}else{
			$user_bought_number = 0;
		}
		$wap_ten = $syrs;
		//限购
		if($config['is_shi'] == 1){									//是否是限购专区
			$xiangou_number = $xiangou_money - $user_bought_number;	
			if($xiangou_number > 0){
				//用户购买本期产品的剩余次数  与   本期产品剩余人次
				$wap_ten = ($xiangou_number > $syrs) ? $syrs : $xiangou_number;
				
			}else{
				//用户购买的次数已超额
				$wap_ten = 0;
			}	
		}
		$data['wap_ten'] = $wap_ten;
		$data['bought']  = $user_bought_number;
		return $data;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}

