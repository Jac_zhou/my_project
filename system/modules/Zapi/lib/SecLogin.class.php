<?php
header("Content-type:text/html;charset=UTF-8");
/**
 * 第三方登录
 * 
 */	


Class SecLogin{
	
	private $indata;			//接收到的数据
	
	
	public function __get($name){
		return $this->$name;
	}
	public function __set($name,$value){
		$this->$name = $value;
	}
	public function __construct(){
		
		
		$this->db = System::load_sys_class('model');
	
		
	}
	
	
	public function index(){
		
		//获取openid
		$openid = $this->indata['openid'];
		$type   = $this->indata['type'];		//微信、QQ
		$MemberBandModel = System::load_app_model('MemberBand','yunapi');

		$field = "*";
		//$where = "b_code='$openid'";
		$where =array();
		$where['b_code'] = $openid;
		$where['b_type'] = $type;
		$data  = $MemberBandModel->get_one($where,$field);
		
		//查看当前第三方用户的资料是否入库
		if(!empty($data)){
			//判断当前第三方用户是否已绑定财神账户
			if($data['b_uid']>0){
				unset($where);
				$where['uid']=$data['b_uid'];
				$filed = "uid,mobile,email,username,mobilecode,emailcode,money,img,yaoqing";
				
				$MemberInfo = $MemberBandModel->get_member_info($where,$filed);	
				//var_dump($MemberInfo);die;
				$this->info = array('status'=>1,'message'=>$MemberInfo);
			}else{
				$this->info = array('status'=>2,'message'=>'没有绑定财神账户！');
			}
		}else{
			//获取当前第三方用户的资料   并且入库             $b_data  获取到的额外数据，备用
			$data = $b_data = array();
			$access_token =  $this->indata['access_token'];		//
			
			if($type=='weixin'){
				//获取微信用户的信息
				$userinfo = $this->get_weixin_info($access_token, $openid);
				$b_data['unionid'] = $userinfo['unionid'];		//微信用户应用唯一标识码
				$b_data['headimgurl'] = $userinfo['headimgurl'];		//用户头像				
			}
			if($type=='qq'){
				//获取qq用户的信息
				$userinfo = $this->get_qq_info($access_token,$openid);				
				$b_data['figureurl_qq_1'] = $userinfo['figureurl_qq_1']; //用户头像				
			}
			$data['b_data']		= serialize($b_data);
			$data['b_code'] 	= $openid;
			$data['b_type']		= $type;
			$data['time'] 		= time();	
			
			
			$id = $MemberBandModel->insert_into($data);
			if($id>0){
				$this->info['message']="用户数据入库";
			}else{
				$this->info['message']="用户数据入库失败，插入语句出错";
			}
			$this->info['status'] = 2;
		}
		
		
	}

	//手机微信 绑定页面   获取用户信息
	public function wapwxlogin(){
		
		$openid = $this->indata['openid'];
		//缓存的微信用户信息
		$userinfo = unserialize(S('userinfo_'.$openid));
		if($userinfo){
			$this->info['status']=1;
			$this->info['message']=$userinfo;
		}
		
		
	}

	

	public function do_bind(){
		
		$indata 	= $this->indata;	
		$openid 	= safe_replace($indata['openid']);		//
		$username  	= trim($indata['username']);				//手机账号
		$password  	= md5(trim($indata['password']));		//密码
		
		$this->info['status']=0;
		if(!_checkmobile($username)){
			$this->info['message']=$username;
			$this->callBack();
		}
		//手机账号登录
		$member=$this->db->GetOne("select * from `@#_member` where `mobile`='$username'");
		if(!$member){
			$this->info['message']='不存在该用户账号！';
			$this->callBack();
		}
		$uid= $member['uid'];
		if($member['wx_id']>0){
			$this->info['message']='被账户被其他微信号绑定，您可以登录用户中心进行解绑！！';
			$this->callBack();
		}
		if($password!= $member['password']){
			$this->info['message']='密码错误';
			$this->callBack();		
		}
		
		$sql 	= "select b_id from `@#_member_band` where b_code='$openid'";
		$result = $this->db->GetOne($sql);
		$band_id = $result['b_id'];
		
		$this->db->Autocommit_start();
		//用户的id
		$sql = "update `@#_member_band` set `b_uid`='$uid' where b_code='$openid'";
		$q1  = $this->db->Query($sql);
		$sql = "update `@#_member` set wx_id='$band_id' where uid='$uid'";
		$q2  = $this->db->Query($sql);
		if($q2 && $q1){
			$this->db->Autocommit_commit();
			
			$this->info = $this->go_login($uid);
			
			
		}else{
			$this->db->Autocommit_rollback();
			$this->info = array('status'=>0,'message'=>'未知错误！');
		}
		
	}
	
	/**   WEB手机端/PC端  绑定账号之后登录
	 *    $uid
	 */
	
	public function go_login($uid){
		
		if($uid>0){
			$member = $this->db->GetOne("select uid,password,mobile,email from `@#_member` where `uid` = '$uid' LIMIT 1");	
			
			$_COOKIE['uid'] = null;
			$_COOKIE['ushell'] = null;
			$_COOKIE['UID'] = null;
			$_COOKIE['USHELL'] = null;	
			$_COOKIE['holdUID'] = null;
			$time = time();
			$user_ip = _get_ip_dizhi();
			$this->db->GetOne("UPDATE `@#_member` SET `user_ip` = '$user_ip',`login_time` = '$time' where `uid` = '$uid'");
			$s0 = _setcookie("holdUID",$member['uid'],60*60*24*7);			
			//$s1 = _setcookie("uid",_encrypt($member['uid']),60*60*24*7);		
			//$s2 = _setcookie("ushell",_encrypt(md5($member['uid'].$member['password'].$member['mobile'].$member['email'])),60*60*24*7);
			//if($s1 && $s2){
			if($s0){					
				$info['message']=$member;
				$info['status']=1;
			}else{
				$info['message']='未知错误！';
				$info['status']=0;
			}
		}
		return $info;
	}
	
	
	
	
	
	public function get_qq_info($access_token,$openid){
		$appid = '101317125';
		$url = "https://graph.qq.com/user/get_user_info?access_token=$access_token&openid=$openid&format=json&oauth_consumer_key=$appid";
		$result = $this->curl_get_json($url);
		$result =  json_decode($result,true);
		return $result;
	}
	
	public function get_weixin_info($access_token,$openid){
		$url="https://api.weixin.qq.com/sns/userinfo?access_token=$access_token&openid=$openid";
		$result = $this->curl_get_json($url);
		$result =  json_decode($result,true);
		return $result;
	}
	
	public function curl_get_json($url){
		$ch = curl_init();
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_URL, $url);
        $response =  curl_exec($ch);
        curl_close($ch);
		return $response;
	}
	
	
	//续期微信token
	public function refresh_wx_token(){
		$url = "https://api.weixin.qq.com/sns/oauth2/refresh_token?appid=APPID&grant_type=refresh_token&refresh_token=REFRESH_TOKEN";
		
	}
	
	
	public function get_qq_code_url(){
		
		$WapqqCon =  System::load_contorller('wapqqlogin','Zapi');
		
		$url = $WapqqCon->get_code();
		$this->info['status'] = 1;
		$this->info['message']= $url;
		
	}
	
	
	public function qq_login(){
		$WapqqCon =  System::load_contorller('wapqqlogin','api');
		$WapqqCon->code = $this->indata['code'];
		$WapqqCon->get_token();
		$WapqqCon->get_openid();
		$qquserinfo = $WapqqCon->get_qq_info();
		
		if($qquserinfo){
			$this->info['status']=1;
			$this->info['message']=$qquserinfo;
		}else{
			$this->info['status']=0;
			$this->info['message']="获取失败";
		}
		
	}
	
	/**
	 * 根据access_token  openid 获取用户信息，并且存入数据库
	 */
	public function get_SecUserInfo($access_token,$openid,$type){
		
		$data = $b_data = array();	
		if($type=='weixin'){
			//获取微信用户的信息
			$userinfo = $this->get_weixin_info($access_token, $openid);
		}
		if($type=='qq'){
			//获取qq用户的信息
			$userinfo = $this->get_qq_info($access_token,$openid);				
		}
		S('userinfo_'.$openid,serialize($userinfo));
		$data['b_code'] 	= $openid;
		$data['b_type']		= $type;
		$data['b_time'] 	= time();	
	
		return $data;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}	
	
	
	
	
	