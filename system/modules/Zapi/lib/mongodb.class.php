<?php
header('Content-type:text/html;charset=utf-8');
Class MongoClass {

	public $db;
	//数据库
	public $collection;
	//集合
	//数据库 $database 集合$collection
	public function __construct($database, $collection) {
		$m = new MongoClient();
		$this->db = $m -> $database;
		$this -> collection = $this->db->$collection;
	}

	//插入数据
	public function insert($data) {
		$document = $data;
		$this -> collection -> insert($document, array('safe' => true));
		return $document;
	}

	//创建集合
	public function set_collection($collection_name=false) {
		if($collection_name){
			return $this->db->createCollection($collection_name);
		}else{
			return false;
		}
		
	}

	public function find($where = array()) {
		$result = $this -> collection -> find($where);
		$data = array();
		foreach ($result as $key => $value) {
			$data[$key] = $value;
		}
		return $data;
	}

	public function GetOne($where = array()) {
		// 更新文档
		$result = $this -> collection -> findOne($where);

		return $result;
	}

	//更新的查询条件$where   要更新的数据 $setDate     都是array()
	public function update($where, $setDate) {

		// 更新文档
		$result = $this -> collection -> update($where, array('$set' => $setDate));

		return $result;
	}

	//条件$where 是否只删除一条数据$justOne
	public function remove($where, $justOne = true) {
		$result = $this -> collection -> remove($where, array("justOne" => $justOne));
		if ($result['ok'] == 1) {
			return true;
		} else {
			return false;
		}
	}
	
	//查询当前集合文档的数目
	public function GetCount(){
		return $this -> collection ->count();
	}
	
	//删除集合
	public function drop_collection($collection_name){
		
		$result = $this->db->$collection_name->drop();
		if ($result['ok'] == 1) {
			return true;
		} else {
			return false;
		}
	}
	
	

}



