﻿<?php


defined('G_IN_SYSTEM')or exit('no');
System::load_sys_class('model'); 
Class wapqqlogin extends SystemAction{
	
	private $appid   = '101401039';

	private $app_key = "363ba44ff24f982603ef14a919d87149";

	private $openid;
	private $access_token;
	
	private $state 	 = "yiyuancaishen";
	
	private $code;
	
//	private $redirect_uri = "http://www.yz-db.com/Zapi/wapqqlogin/qq_callback"; 
        private $redirect_uri = "http://www.yz-db.com/Zapi/wapqqlogin/qq_callback"; 

	/*public function __get($name){
		return $this->$name;
	}
	public function __set($name,$value){
		$this->$name = $value;
	}*/
	public function __construct(){
		
		
	}


	public function init(){
		
		$url = $this->get_code();
		
		header("Location:$url");	
	}
	
	
	
	//手机端
	public function qq_callback(){
		$state 		= $_GET['state'];
		$usercancel = $_GET['usercancel'];		//用户是否退出登录
		$this->code = $_GET['code'];
		//var_dump($_GET['code']);die;
		if($usercancel){
			header("Location:".WEB_PATH."/yungou/index.html#/tab/login");
			exit;
		}
			
		if($state == $this->state){
			$this->get_token();
			$this->get_openid();
			if($this->openid && $this->access_token){
				//查看数据库中是否有绑定用户
				
				$SecLoginCon = System::load_app_class('SecLogin','Zapi');
				
				$MemberBandModel = System::load_app_model('MemberBand','yunapi');
				
				$field = "*";
				//$where = "b_code='$openid'";
				$where =array();
				$where['b_code'] = $this->openid;
				$where['b_type'] = 'qq';
				$data  = $MemberBandModel->get_one($where,$field);
				
				//数据库是否有存入数据
				if($data){
					if($data['b_uid']>0){
						unset($where);
						$where['uid']=$data['b_uid'];
						$filed = "uid,mobile,email,username,mobilecode,emailcode,money,img,yaoqing";
						
						$MemberInfo = $MemberBandModel->get_member_info($where,$filed);
						
						if($MemberInfo){
							$info = $SecLoginCon->go_login($MemberInfo['uid']);		
							
							if($info['status']=='1'){
								header('Location:'.WEB_PATH.'/yungou/index.html#/tab/personCenter?uid='.$MemberInfo['uid']);
							}else{
								_message('未知',WEB_PATH.'/yungou/index.html#/tab/login');
							}
						}else{
							_message('已绑定的用户已经注销',WEB_PATH.'/yungou/index.html#/tab/login');
						}
					}else{
						//没有绑定用户   去绑定
						//header('Location:'.WEB_PATH.'/yungou/index.html#/tab/bindwx');
						_message('没有绑定用户   去绑定',WEB_PATH.'/yungou/index.html#/tab/bindwx?type=qq&openid='.$this->openid);
					}	
				}else{
					
					//获取第三方用户信息并且存入缓存
					$data 	= $SecLoginCon->get_SecUserInfo($this->access_token, $this->openid,'qq');
					//存入数据库
					
					$id 	= $MemberBandModel->insert_into($data);
					if($id>0){
						_message('该QQ第一次登陆，前去绑定',WEB_PATH.'/yungou/index.html#/tab/bindwx?type=qq&openid='.$this->openid);
					}else{
						_message('位置错误',WEB_PATH.'/yungou/index.html#/tab/login');
					}
				}
			}
			
		}
		
	}

	public function get_code(){
		
		$url = "https://graph.qq.com/oauth2.0/authorize";
		
		$response_type  = 'code';
		$client_id		= $this->appid;
		$redirect_uri	= $this->redirect_uri;			//
		$state			= $this->state;
		$scope			= 'get_user_info';
		$display		= 'mobile';
		$g_ut			= '1';
		
		$url .= "?response_type=$response_type&client_id=$client_id&redirect_uri=".urlencode($redirect_uri)."&state=$state&scope=$scope&display=$display&g_ut=$g_ut";
		
		return $url;
		
	}

	//获取access_token
	public function get_token(){
		$url 			= "https://graph.qq.com/oauth2.0/token";
		
		$grant_type 	= "authorization_code";
		
		$client_id		= $this->appid;
		
		$client_secret 	= $this->app_key;
		
		$code			= $this->code;
		
		$redirect_uri 	= $this->redirect_uri;
		
		
		$url .= "?grant_type=$grant_type&client_id=$client_id&client_secret=$client_secret&redirect_uri=".urlencode($redirect_uri)."&code=$code";
		
		$result = $this->curl_get_json($url);
		if(strpos($response, "callback") !== false){

            $lpos = strpos($response, "(");
            $rpos = strrpos($response, ")");
            $response  = substr($response, $lpos + 1, $rpos - $lpos -1);
            $response = json_decode($response,true);
			_message('获取失败！');
        }
		parse_str($result,$result);
		$this->access_token = $result['access_token'];
	
		return $result;
	}


	public function get_openid(){
		
		$url 			= "https://graph.qq.com/oauth2.0/me";
		
		$access_token   = $this->access_token;
		
		$url .= "?access_token=$access_token";
		
		$response = $this->curl_get_json($url);
		
		if(strpos($response, "callback") !== false){

            $lpos = strpos($response, "(");
            $rpos = strrpos($response, ")");
            $response  = substr($response, $lpos + 1, $rpos - $lpos -1);
            $response = json_decode($response,true);
        }
		
		$this->openid = $response['openid'];
		
		return $response;
	}


	
	

	
	
	public function get_qq_info(){
		
		$appid 			= $this->appid;
		$access_token 	= $this->access_token;
		$openid 		= $this->openid;
		$url = "https://graph.qq.com/user/get_user_info?access_token=$access_token&openid=$openid&format=json&oauth_consumer_key=$appid";
		$result = $this->curl_get_json($url);
		$result =  json_decode($result,true);
		
		S('userinfo_'.$openid,serialize($result));
		return $result;	
	}
	
	
	
	
	
	public function curl_get_json($url){
		$ch = curl_init();
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_URL, $url);
        $response =  curl_exec($ch);
        curl_close($ch);
		return $response;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}















