<?php
/**
 * Created by PhpStorm.
 * User: zhoulang
 * Date: 2016/8/24
 * Time: 9:35
 */
Class UserBank extends SystemAction{


    public function frist_addmoney(){





    }

    /**
     * @param $total_fee_t          充值金额
     * @param $dingdaninfo          充值的订单信息
     * @param $pay_type             充值方式                    //微信支付，京东支付，爱贝支付
     * @return bool
     */
    public function user_recharge($total_fee_t,$dingdaninfo,$paytype){
        $mysql_model = System::load_sys_class('model');
        //查询充值订单对应的充值记录
        $time = $_SERVER['REQUEST_TIME'];
        //首次充值  赠送  5元
        //查询当前用户已经充值的次数
        $sql = "SELECT count(id) as num  from `@#_member_addmoney_record` WHERE uid='{$dingdaninfo['uid']}' AND `status`='已付款'";
        $result = $mysql_model->GetOne($sql);
        $number = $result['num'];
		
        $money = 0;
		
        if ($number == 0 && $total_fee_t >= 10) {      //首次充值 10元以上    增加 5 元
            $money = 5;
        }
        //添加金额
        $total_fee_t = $total_fee_t + $money;

        $sql = "UPDATE `@#_member_addmoney_record` SET `pay_type` = '$paytype', `status` = '已付款' where `id` = '{$dingdaninfo['id']}' and `code` = '{$dingdaninfo['code']}'";
        $up_q1 = $mysql_model->Query($sql);
        $sql = "UPDATE `@#_member` SET `money` = `money` + $total_fee_t where (`uid` = '{$dingdaninfo['uid']}')";
        $up_q2 = $mysql_model->Query($sql);
        $sql = "INSERT INTO `@#_member_account` (`uid`, `type`, `pay`, `content`, `money`, `time`) VALUES ('{$dingdaninfo['uid']}', '1', '账户', '充值', '$total_fee_t', '$time')";
        $up_q3 = $mysql_model->Query($sql);
        if ($money > 0) {
            //活动 type 3 首次充值立即赠送
            $sql = "INSERT INTO
						`@#_active`(uid,type,cash_value,is_active,r_time,mobile,is_use,money)
					VALUES
						('{$dingdaninfo['uid']}',3,'$money',1,'$time','{$dingdaninfo['mobile']}',1,'$money')";
            $mysql_model->Query($sql);
            $id2 = $mysql_model->insert_id();
            if ($id2 > 0) {
                $up_q4 = true;
            } else {
                $up_q4 = false;
            }
        } else {
            $up_q4 = true;
        }
        
		$result = $this->first_yaoqing_get_money($number,$dingdaninfo);
       
        if ($up_q1 && $up_q2 && $up_q3 && $up_q4 && $result) {
            return true;
        } else {
        	file_put_contents('log/pay.log',date('Y-m-d H:i:s')."爱贝充值失败，请及时修复！$up_q1 && $up_q2 && $up_q3 && $up_q4 && $result".PHP_EOL,FILE_APPEND);
            return false;
        }

    }


    /**
     * [ipay_recharge description]
     * @param  [arr] $transdata   [description]
     * @param  [arr] $dingdaninfo [订单信息]
     * @param  [str] $pay_type    [支付类型]
     * @return [bool]              [description]
     */
    public function ipay_recharge($transdata,$dingdaninfo,$paytype){
        $mysql_model = System::load_sys_class('model');
        //查询充值订单对应的充值记录
        $time = $_SERVER['REQUEST_TIME'];
        $total_fee_t = $dingdaninfo['money'];
        //首次充值  赠送  5元
        //查询当前用户已经充值的次数
        $sql = "SELECT count(id) as num  from `@#_member_addmoney_record` WHERE uid='{$dingdaninfo['uid']}' AND `status`='已付款'";
        $result = $mysql_model->GetOne($sql);
        $number = $result['num'];
        $money = 0;
        if ($number == 0 && $total_fee_t >= 10) {      //首次充值 10元以上    增加 5 元
            $money = 5;
        }
        //添加金额
        $total_fee_t = $total_fee_t + $money;
        //方便与爱贝对账
        $ipay_transid = $transdata['transid'];    //交易流水号
        $ipay_transtime = $transdata['transtime'];  //交易时间
        $ipay_paytype = $transdata['paytype'];    //支付方式
        $ipay_result = $transdata['result'];     //支付结果
        $in_fields = " , `plat_type` = 1 ,`transid` = '$ipay_transid', `transtime`='$ipay_transtime', `paytype`='$ipay_paytype',`result`='$ipay_result' ";
        
        $up_q1 = $mysql_model->Query("UPDATE `@#_member_addmoney_record` SET `pay_type` = '$paytype', `status` = '已付款' $in_fields where `id` = '$dingdaninfo[id]' and `code` = '$dingdaninfo[code]'");
        $sql = "UPDATE `@#_member` SET `money` = `money` + $total_fee_t where `uid` = '{$dingdaninfo['uid']}' ";
        $up_q2 = $mysql_model->Query($sql);
		
        $up_q3 = $mysql_model->Query("INSERT INTO `@#_member_account` (`uid`, `type`, `pay`, `content`, `money`, `time`) VALUES ('{$dingdaninfo['uid']}', '1', '账户', '充值', '$total_fee_t', '$time')");
        
        if ($money > 0) {
            //活动 type 3 首次充值10元立即赠送
            $sql = "INSERT INTO
                        `@#_active`(uid,type,cash_value,is_active,r_time,mobile,is_use,money)
                    VALUES
                        ('{$dingdaninfo['uid']}',3,'$money',1,'$time','{$dingdaninfo['mobile']}',1,'$money')";
            $mysql_model->Query($sql);
            $id2 = $mysql_model->insert_id();
            if ($id2 > 0) {
                $up_q4 = true;
            } else {
                $up_q4 = false;
            }
        } else {
            $up_q4 = true;
        }
        
        $result = $this->first_yaoqing_get_money($number,$dingdaninfo);
        
        if($up_q1 && $up_q2 && $up_q3 && $up_q4 && $result){
            return true;
        }else{
        	file_put_contents('log/pay.log',date('Y-m-d H:i:s')."爱贝充值失败，请及时修复！$up_q1 && $up_q2 && $up_q3 && $up_q4 && $result".PHP_EOL,FILE_APPEND);
            return false;
        }




    }

	//活动 type 4 第一次邀请好友赠送10元
	public function first_yaoqing_get_money($number,$dingdaninfo){
		$mysql_model = System::load_sys_class('model');
		$time = $_SERVER['REQUEST_TIME'];
		$up_q5 = true;
		$up_q6 = true;
        $yaoqing_money = 10;	//邀请人可获得的金额
        //给邀请人赠送金额
        if($number==0){
        	$sql = "select yaoqing from @#_member where uid='{$dingdaninfo['uid']}'";
			$yaoqing = $mysql_model->GetOne($sql);
			if($yaoqing['yaoqing']>0){
				$sql = "select get_yaoqing_money,money,mobile,uid from `@#_member` where uid='{$yaoqing['yaoqing']}' for update";
				$yaoqing_info = $mysql_model->GetOne($sql);
				if($yaoqing_info['get_yaoqing_money']==0){
					$sql = "update `@#_member` set `money`=( `money` + $yaoqing_money ),`get_yaoqing_money`=1 where uid='{$yaoqing['yaoqing']}' ";
					$up_q5 = $mysql_model->Query($sql);
					//活动 type 4 第一次邀请好友赠送10元
		            $sql = "INSERT INTO
								`@#_active`(uid,type,cash_value,is_active,r_time,mobile,is_use,money)
							VALUES
								('{$yaoqing_info['uid']}',4,'$yaoqing_money',1,'$time','{$yaoqing_info['mobile']}',1,'$yaoqing_money')";
		            $mysql_model->Query($sql);
		            $id2 = $mysql_model->insert_id();
		            if ($id2 > 0) {
		                $up_q6 = true;
		            } else {
		                $up_q6 = false;
		            }
				}
			}
		}
		if($up_q5 && $up_q6){
			return true;
		}else{
			return false;
		}
		
	}


}















