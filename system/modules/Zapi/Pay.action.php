<?php
defined('G_IN_SYSTEM')or exit('no');
require_once __DIR__.'/vendor/JHF/lib/jhpay_core.function.php';
require_once __DIR__.'/vendor/JHF/lib/jhpay_notify.class.php';
require_once __DIR__.'/vendor/JHF/lib/jhpay.class.php';
System::load_app_fun('myfun','yunapi');
System::load_app_fun("pay","pay");
Class Pay extends SystemAction{

    public $db;

    public $config = [
        'partner' => '26100535',//商户号
        //MD5 key值，可以从数据库获取或配置此处，建议数据库获取
        'key' => '82y5d877sod1t87a0j4gspevz78xkg0j',
        //签名方式 不需修改，也可以使用RSA
        'sign_type' => 'MD5',
        //字符编码格式 目前支持 gbk 或 utf-8
        'input_charset' => 'utf-8',
        //ca证书路径地址，用于curl中ssl校验，目前暂不用
        //'cacert' => getcwd().'\\cacert.pem',
        //访问模式,根据自己的服务器是否支持ssl访问，若支持请选择https；若不支持请选择http
        'transport' => 'http',
    ];



    //notify_url　　回调地址
    public function index(){
        $jhpayNotify = new JhpayNotify($this->config);
        $verify_result = $jhpayNotify->verifyMD5();
        if($verify_result){
            $postData = $_POST;
            //记录状态（交易记录数据）
            file_put_contents('log/pay_log.log','支付回调数据：'.json_encode($postData).PHP_EOL,FILE_APPEND);

            array_walk_recursive($postData,function( &$value){
                $value = htmlspecialchars(stripslashes(trim($value)));
            });
            //商户订单号
            $out_trade_no = $postData['merorderid'];

            //聚合富交易号

            $trade_no = $postData['tradeid'];

            //交易状态
            $trade_status = $postData['success'];

            $success_money = $postData['successmoney'];//交易金额
            $transdata = [
                'transid' => $trade_no,
                'transtime' => $postData['tradedate'],
                'pay_type' => '',
                'result' => $trade_status,
            ];
            $this->order_success($out_trade_no,$success_money,$transdata);
            if ($trade_status == '1') {

                //方便与第三方对账
//                $ipay_transid = $transdata['transid'];    //交易流水号
//                $ipay_transtime = $transdata['transtime'];  //交易时间
//                $ipay_paytype = $transdata['paytype'];    //支付方式
//                $ipay_result = $transdata['result'];     //支付结果
                //判断该笔订单是否在商户网站中已经做过处理

                //如果没有做过处理，根据订单号（out_trade_no）在商户网站的订单系统中查到该笔订单的详细，并执行商户的业务程序

                //如果有做过处理，不执行商户的业务程序

                //注意：
                //付款完成后，聚合富系统发送该交易状态通知
                //注意校验订单金额



            }
        }
    }

    //建议：在后台读取订单信息之后，通过传参直接调用本方法
    public function topay($data=[]){
        //$postData = empty($data)? $_POST : $data;
        $postData = $data;
        $postData['appid'] = '26100535';
        $postData['key'] = '82y5d877sod1t87a0j4gspevz78xkg0j';

        /*array_walk_recursive($postData,function( &$value){
            $value = htmlspecialchars(stripslashes(trim($value)));
        });*/
        //以下字段参考接口文档
        $jhpay_version = '1.0';
        $jhpay_merid = $postData['appid'];//商户ID
        $jhpay_mername= isset($postData['company_name'])?$postData['company_name']:'易中夺宝';//商户名称
        $jhpay_policyid = '000001';
        $jhpay_merorderid = $postData['orderid'];//订单号
        $jhpay_paymoney = $postData['money'];//金额
        $jhpay_productname = $postData['productName'];//商品名称，尽量不要用云购，1元云购等
        $jhpay_productdesc = '易中夺宝';//商品描述
        $jhpay_userid = (int) $postData['u_id'];    //用户id
        $jhpay_username = $postData['username'];    //用户名字
        $jhpay_email = $postData['email'];          //邮箱帐号
        $jhpay_phone = $postData['mobile'];         //用户手机号
        $jhpay_extra = $postData['from'];//添加自定义内容  可以用来标示 来源 android/web/ios
        $jhpay_custom = '';
        $jhpay_redirect = '2';  // 0
        $jhpay_redirecturl = $postData['redirecturl'];      //自己前台回调地址（不影响后台的数据同步）
        $jhpay_md5 = $postData['key'];    //尽量不要明文赋值（可以通过后台配置到数据库,获取数据库便可）

        $jhpay_config_input_charset = strtolower('utf-8');

        //构造要请求的参数数组，无需改动
        $parameter = array(
            "version" => $jhpay_version,
            "merid" =>$jhpay_merid,
            "mername"	=> $jhpay_mername,
            "policyid"	=> $jhpay_policyid,
            "merorderid"	=> $jhpay_merorderid,
            "paymoney"	=> $jhpay_paymoney,
            "productname"	=> $jhpay_productname,
            "productdesc"	=> $jhpay_productdesc,
            "userid"	=> $jhpay_userid,
            "username"	=> $jhpay_username,
            "email"	=> $jhpay_email,
            "phone"	=> $jhpay_phone,
            "extra"	=> $jhpay_extra,
            "custom" => $jhpay_custom,
            "redirect"	=> $jhpay_redirect,
            "redirecturl"	=> $jhpay_redirecturl
            // "md5"	=> $jhpay_md5

        );

        //签名方式 不需修改
        $jhpay_config_sign_type = strtoupper('MD5');
        //字符编码格式 目前支持 gbk 或 utf-8
        $jhpay_config_input_charset = strtolower('utf-8');

        $jhpay_config_transport   = 'http';

        $jhpay_config=array(
            "partner"      =>$jhpay_merid,
            "key"          =>$jhpay_md5,
            "sign_type"    =>$jhpay_config_sign_type,
            "input_charset"=>$jhpay_config_input_charset,
            "transport"    =>$jhpay_config_transport
        );

        $jhpaySubmit = new JhpaySubmit($jhpay_config);
        $url = $jhpaySubmit->buildRequestForm($parameter,'POST','submit');
        //$url = $jhpaySubmit->buildRequestHttp($parameter);
        echo $url;
        //var_dump($url);die;
        //return $url;
        exit;
    }


    public function gopay(){

        $getData = $_GET;

        array_walk_recursive($getData,function( &$value){
            $value = htmlspecialchars(stripslashes(trim($value)));
        });

        $this->db=System::load_sys_class('model');

        $type = $getData['pay_type'];
        $uid = (int) $getData['uid'];
        $money = (int) $getData['money'];

        $this->db->Autocommit_start();
        $members = $this->db->GetOne("SELECT * FROM `@#_member` where `uid` = '$uid' for update");

        if(!isset($members['uid'])){
            $this->db->Autocommit_rollback();
            _messagemobile('参数错误2！');
        }

        //插入充值记录
        $score = 0;
        $dingdanhao = pay_get_dingdan_code('C');    //生成充值订单号
        $time = time();
        $scookies = '0';


        if ($type == 'alipay') {
            $t = "支付宝";
        }elseif ($type == 'wxpay') {
            $t = "微信支付";
        }elseif ($type == 'iapppay') {
            $t = "聚合富支付";
        }
        $sql = "INSERT INTO
					`@#_member_addmoney_record`
							(`uid`, `code`, `money`, `pay_type`, `status`,`time`,`score`,`scookies`,`plat_type`)
	    		VALUES
					('{$uid}', '{$dingdanhao}', '{$money}', '{$t}','未付款', '{$time}','{$score}','{$scookies}','2')";
        $query = $this->db->Query($sql);
        if($query){
            $this->db->Autocommit_commit();
        }else{
            $this->db->Autocommit_rollback();
            _messagemobile('失败！');
        }

        //现在  聚合富支付
        //$PayCon = System::load_contorller('Pay','Zapi');

        $data = array(
            'orderid' => $dingdanhao,
            'money' => $money,
            'productName' => '易中夺宝商品',
            'u_id' => $uid,
            'username' => $members['username'],
            'email' => $members['email'],
            'mobile' => $members['mobile'],
            'from' => 'WAP',
            'redirecturl' => G_WEB_PATH."/yungou/index.html#/tab/payResultState?code=".$dingdanhao."&uid=".$uid,
        );
//        print_r($data);
        return $this->topay($data);
    }



    public function ceshi(){
        $data = Array (
            'orderid' => 'C14974258993613667',
            'money' => 1,
            'productName' => '易中夺宝商品',
            'u_id' => 14352,
            'username' => '周郎1094', 'email' => '',
            'mobile' => '15717008609',
            'from' => 'WAP',
            'redirecturl' => 'http://www.yz-db.com/yungou/index.html#/tab/payResultState?code=C14974258993613667&uid=14352',
        );

        $this->topay($data);
    }

    public function order_success($out_trade_no,$success_money,$transdata){
        $this->db=System::load_sys_class('model');
        file_put_contents('log/pay.log',PHP_EOL."支付单号：{$out_trade_no},开始更新信息".PHP_EOL,FILE_APPEND);
        $this->db->Autocommit_start();
        $dingdaninfo = $this->db->GetOne("select * from `@#_member_addmoney_record` where `code` = '$out_trade_no' and `money`='$success_money'  and  `status` = '未付款' for update");


        if($dingdaninfo){
            file_put_contents('log/pay.log',"----数据库订单数据：".print_r($out_trade_no)." :end".PHP_EOL,FILE_APPEND);
        }else{
            //没有该订单,失败
            file_put_contents('log/pay.log',"数据库没有该订单数据  :end".PHP_EOL.PHP_EOL,FILE_APPEND);
            echo 'error';
            exit;
        }

        $c_money = $dingdaninfo['money'];
        $uid = $dingdaninfo['uid'];
        $time = time();


        //方便与第三方对账
        $ipay_transid = $transdata['transid'];    //交易流水号
        $ipay_transtime = $transdata['transtime'];  //交易时间
        $ipay_paytype = $transdata['paytype'];    //支付方式
        $ipay_result = $transdata['result'];     //支付结果
        $in_fields = " , `plat_type`=1 ,`transid` = '$ipay_transid', `transtime`='$ipay_transtime', `paytype`='$ipay_paytype',`result`='$ipay_result' ";


        $up_q1 = $this->db->Query("UPDATE `@#_member_addmoney_record` SET `pay_type` = '爱贝支付', `status` = '已付款' $in_fields where `id` = '$dingdaninfo[id]' and `code` = '$dingdaninfo[code]'");
        $up_q2 = $this->db->Query("UPDATE `@#_member` SET `money` = `money` + $c_money where (`uid` = '$uid')");
        $up_q3 = $this->db->Query("INSERT INTO `@#_member_account` (`uid`, `type`, `pay`, `content`, `money`, `time`) VALUES ('$uid', '1', '账户', '充值', '$c_money', '$time')");
        /************ 好友获得积分 start  2015/12/15 **************/
        $up_q4 = true;
        $up_q5 = true;
        $up_q6 = true;
        $record_id = $this->db->insert_id();		//充值记录的ID
        $addtime = time();		//添加时间
        $score = array('1'=>4, '2'=>3, '3'=>2);		//积分表
        $friends = $this->db->GetList("SELECT yaoqing,lev FROM `@#_member_friendship` WHERE `uid`='$uid' ");
        if(count($friends)){
            foreach($friends as $k => $v){
                $yaoqing = $v['yaoqing'];
                $lev = $v['lev'];
                $jifen = $c_money * $score[$lev] ;
                $this->db->Query("UPDATE `@#_member` SET `score` = `score` + $jifen where (`uid` = '$yaoqing')");
                $upp[$k] = $this->db->Query("INSERT INTO `@#_member_jifen`(`uid`,`yaoqing`,`lev`,`jifen`,`record_id`,`addtime`) VALUES('$uid','$yaoqing','$lev','$jifen','$record_id','$addtime')");
            }
            $up_q4 = isset($upp[0]) ? $upp[0] : true;
            $up_q5 = isset($upp[1]) ? $upp[1] : true;
            $up_q6 = isset($upp[2]) ? $upp[2] : true;
        }
        /************ 好友获得积分 end  2015/12/15 ********以下的判断中添加了up_q4  up_q5  up_q6的判断******/
        /*活动的start*/
        System::load_app_fun('member','member');
        $chong_userinfo = getchongzhi_user($dingdaninfo['uid']);
        active_cash($dingdaninfo['uid'],$c_money,$chong_userinfo['mobile']);
        /*活动的end*/

        file_put_contents('log/pay.log',"----数据库订单更新状态：1.".print_r($up_q1)
            ." 2. ".print_r($up_q2)
            ."3.".print_r($up_q3)
            ."4.".print_r($up_q4)
            ."5.".print_r($up_q5)
            ."6.".print_r($up_q6)
            .PHP_EOL,FILE_APPEND);
        if($up_q1 && $up_q2 && $up_q3 && $up_q4 && $up_q5 && $up_q6){
            $this->db->Autocommit_commit();

            file_put_contents('log/pay.log',"数据库订单数据更新成功!".PHP_EOL,FILE_APPEND);
        }else{
            $this->db->Autocommit_rollback();
            echo "failed";exit;
        }

        //////////////////////////////////////////////////////
        ///如果是购买商品，需要继续执行余额支付   20151227 kpf///
        /////////////////////////////////////////////////////
        $scookies = unserialize($dingdaninfo['scookies']);
        $wpay = System::load_app_class('wpay','pay');
        $wpay->scookie = $scookies;

        $ok = $wpay->init($uid,"聚合富支付",'go_record',$scookies);	//众乐商品
        if($ok != 'ok'){
            echo "failed";exit;	//商品购买失败
        }
        $check = $wpay->go_pay(1,$dingdaninfo['code']);
        if($check){
            //$this->db->Query("UPDATE `@#_member_addmoney_record` SET `scookies` = '1' where `code` = '$out_trade_no' and `status` = '已付款'");
            _setcookie('Cartlist',NULL);
            echo "success";exit;
        }else{
            echo "failed";exit;
        }
    }
    //充值（只针对聚合富支付）
    public function add_money($uid,$money){
        $pay_type = "聚合富支付";
        $this->db=System::load_sys_class('model');

        $this->db->Autocommit_start();
        $members = $this->db->GetOne("SELECT * FROM `@#_member` where `uid` = '$uid' for update");

        if(!isset($members['uid'])){
            $this->db->Autocommit_rollback();
            _messagemobile('参数错误2！');
        }


        $dingdancode = pay_get_dingdan_code('C');		//订单号
        $time = time();
        if(!empty($data)){
            $scookies = $data;
        }else{
            $scookies = '0';
        }
        $score = $this->fufen;
        $query = $this->db->Query("INSERT INTO
                                      `@#_member_addmoney_record` (`uid`, `code`, `money`, `pay_type`, `status`,`time`,`score`,`scookies`)
                                      VALUES ('$uid', '$dingdancode', '$money', '$pay_type','未付款', '$time','$score','$scookies')");
        if($query){
            $this->db->Autocommit_commit();
        }else{
            $this->db->Autocommit_rollback();
            return false;
        }


        $data = array(
            'orderid' => $dingdancode,
            'money' => $money,
            'productName' => '易中夺宝商品',
            'u_id' => $uid,
            'username' => $members['username'],
            'email' => $members['email'],
            'mobile' => $members['mobile'],
            'from' => 'WAP',
            'redirecturl' => G_WEB_PATH.'/index.php/member/home/paysuccess',
        );
        return $this->topay($data);
    }

    public function two_notify_url(){
        $postData = file_get_contents('php://input');
        /*$postData = '{
    "amount":10,
    "receive_time":"20170619153947",
    "complete_time":"20170619153956",
    "merch_id":"m1706140102",
    "charge_id":"wx170619153944GmHte0U",
    "order_no":"C14978579857452050",
    "ret_code":"0000",
    "ret_info":"支付成功",
    "optional":"",
    "version":"V2.1.1",
    "sign_type":"MD5",
    "sign_info":"bbbed399b95120cd9f77d88edb3e6021"
}';*/

        file_put_contents('log/pay_log.log','付钱啦 回调数据1：'.date('m-d H:i:s').$postData.PHP_EOL,FILE_APPEND);
        $postData = json_decode($postData,true);
//        var_dump($postData);
//        file_put_contents('log/pay_log.log','付钱啦 回调数据1：'.date('m-d H:i:s').$postData.PHP_EOL,FILE_APPEND);
        if($postData['ret_code']=='0000'){
            file_put_contents('log/pay_log.log','付钱啦 ：支付成功：'.PHP_EOL,FILE_APPEND);
            $out_trade_no = $postData['order_no'];
            $success_money = $postData['amount'];
            $transdata['result'] = 1;
            $this->order_success($out_trade_no,$success_money/100,$transdata);
        }
    }


}