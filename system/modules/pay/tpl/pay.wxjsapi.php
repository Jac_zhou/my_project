<!DOCTYPE html>

<html>

	<head>

		<title>微信安全支付</title>

		<meta http-equiv="Content-Type" content="text/html;charset=utf-8">

		<meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=0" />

		<meta name="apple-mobile-web-app-capable" content="yes">

		<meta name="apple-mobile-web-app-status-bar-style" content="black">

		<meta name="format-detection" content="telephone=no">
		<script src="https://res.wx.qq.com/open/js/jweixin-1.1.0.js" type="text/javascript" charset="utf-8"></script>
        <script src="http://cdn.bootcss.com/jquery/3.1.0/jquery.min.js"></script>
        <script src="http://cdn.bootcss.com/bootstrap/4.0.0-alpha.3/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="http://cdn.bootcss.com/bootstrap/4.0.0-alpha.3/css/bootstrap.css">
		<style type="text/css">

			body{padding: 0;margin:0;background-color:#eeeeee;font-family: '黑体';}
			
			.pay-main{background-color: #4cb131;padding-top: 20px;padding-left: 20px;padding-bottom: 20px;}
			
			.pay-main img{margin: 0 auto;display: block;}
			
			.pay-main .lines{margin: 0 auto;text-align: center;color:#cae8c2;font-size:12pt;margin-top: 10px;}
			
			.tips .img{margin: 20px;}
			
			.tips .img img{width:20px;}
			
			.tips span{vertical-align: top;color:#ababab;line-height:18px;padding-left: 10px;padding-top:0px;}
			
			.action{background:#4cb131;padding: 10px 0;color:#ffffff;text-align: center;font-size:14pt;border-radius: 10px 10px; margin: 15px;}
			
			.action:focus{background:#4cb131;}
			
			.action.disabled{background-color:#aeaeae;}
			
			.footer{position: absolute;bottom:0;left:0;right:0;text-align: center;padding-bottom: 20px;font-size:10pt;color:#aeaeae;}
			
			.footer .ct-if{margin-top:6px;font-size:8pt;}

		</style>

	</head>

	<body>

		<div class="conainer">

			<div class="pay-main">

				<img src="http://www.yz-db.com/statics/templates/templet2/images/mobile/pay_logo.png"/>

				<div class="lines">
					<span>微信安全支付</span>
				</div>

			</div>

			<div class="tips">

				<div class="img">

					<img src="http://www.yz-db.com/statics/templates/templet2/images/mobile/pay_ok.png"/>

					<span>已开启支付安全</span>

				</div>

			</div>

			<div id="action" class="action" onclick="wxpay();">
				确认支付
			</div>

			<div class="footer">
				<div>
					支付安全由中国人民财产保险股份有限公司承保
				</div>
				<div class="ct-if">
					7x24小时热线：0755-86010333
				</div>
			</div>

		</div>
        <div style="height: 60px;" class="modal" id="loadingModal" tabindex="-1" role="dialog"
             aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog" style="width: 100px;margin: 0 auto;">
                <div class="modal-content">
                    <div class="modal-body" style="text-align: center;padding: 0;">
                        <img src="http://www.yz-db.com/yungou/images/icon/loading.gif" style="width: 50px;">
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal -->
        </div>
		<script type="text/javascript">		
			var jsApiParameters =<?php echo $jsApiParameters; ?>;
			var jsApiConfig =<?php echo $jsApi_config; ?>;
			console.log(jsApiParameters);
			//wxpay();
            var can_click = 1;
            function showLoading(){
                $('#loadingModal').modal({backdrop: 'static', keyboard: false});
                //$('#loadingModal').modal('show');
                $('#loadingModal').css('margin-top', $(window).height()/2 - 30);
            }

            function hideLoading(){
                $('#loadingModal').modal('hide');
            }
			//调用微信JS api 支付
			function wxpay(){
                showLoading();
                setTimeout(function(){
                    hideLoading();
                },2000);
                if(can_click ==0){
                    console.log('不能点击');
                    return false;
                }
				wx.config({
					debug: false, // 开启调试模式,调用的所有api的返回值会在客户端alert出来，若要查看传入的参数，可以在pc端打开，参数信息会通过log打出，仅在pc端时才会打印。
					appId: jsApiConfig.appId, // 必填，公众号的唯一标识
					timestamp: jsApiConfig.timeStamp, // 必填，生成签名的时间戳
					nonceStr: jsApiConfig.nonceStr, // 必填，生成签名的随机串
					signature: jsApiConfig.signature, // 必填，签名，见附录1
					jsApiList: ['chooseWXPay'] // 必填，需要使用的JS接口列表，所有JS接口列表见附录2
				});
				wx.ready(function() {
                    can_click = 0;

                    setTimeout(function(){
                        can_click = 1;
                    },3000);
					wx.hideOptionMenu();		//隐藏右上角接口菜单
					wx.chooseWXPay({
						timestamp: jsApiParameters.timeStamp, // 支付签名时间戳，注意微信jssdk中的所有使用timestamp字段均为小写。但最新版的支付后台生成签名使用的timeStamp字段名需大写其中的S字符
						nonceStr: jsApiParameters.nonceStr, // 支付签名随机串，不长于 32 位
						package: jsApiParameters.package, // 统一支付接口返回的prepay_id参数值，提交格式如：prepay_id=***）
						signType: jsApiParameters.signType, // 签名方式，默认为'SHA1'，使用新版支付需传入'MD5'
						paySign: jsApiParameters.paySign, // 支付签名
						success: function(res) {
                            //回到用户中心
                            window.location.href = 'http://www.yz-db.com/yungou/index.html#/tab/personCenter';
						},

					});
				});

			}
			function backTo() {
				window.location.href = "http://www.yz-db.com/yungou/index.html#/tab/recharge";
			}
		</script>

	</body>
</html>
