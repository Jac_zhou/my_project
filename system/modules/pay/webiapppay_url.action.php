<?php 
/*
* APP 爱贝支付回调
* 2015/12/29
*/
defined('G_IN_SYSTEM')or exit('No permission resources.');
ini_set("display_errors","OFF");

class webiapppay_url extends SystemAction {
	public function __construct(){			
		$this->db=System::load_sys_class('model');		
	} 	
	
	public function qiantai(){
	    sleep(3);
	    
	    $transdata = stripslashes($_GET['transdata']);  //删除反斜杠
		$transdata = json_decode($transdata,true);
	    $out_trade_no = $transdata['cporderid'];	//商户订单号
	    
	    $dingdaninfo = $this->db->GetOne("select * from `@#_member_addmoney_record` where `code` = '$out_trade_no'");
	    
	    if(!$dingdaninfo || $dingdaninfo['status'] == '未付款'){
	        if(empty($dingdaninfo['scookies'])){
	            header("location:".G_WEB_PATH."/yungou/#/tab/rechargeResult?ledou=-1"); //充值失败
	        }else{
	            header("location:".G_WEB_PATH."/yungou/#/tab/payResult?code=0");  //支付失败
	        }
	    }else{
	        if(empty($dingdaninfo['scookies'])){
	            header("location:".G_WEB_PATH."/yungou/#/tab/rechargeResult?ledou=".$dingdaninfo['money']);
	        }else{
	            if($dingdaninfo['scookies'] == '1'){
	                header("location:".G_WEB_PATH."/yungou/#/tab/payResult?code=".$dingdaninfo['code']);
	            }else{
	                //商品剩余人次不足,请重新购买!
	                header("location:".G_WEB_PATH."/yungou/#/tab/payResult?code=0");
	            }
	        }
	    }
	}
	
	public function houtai(){
	    include "lib/iapppay/web_base.php";
	    
	    $iapppayCpUrl="http://ipay.iapppay.com:9999";
	    $tokenCheckUrl=$iapppayCpUrl . "/payapi/tokencheck";
	    $orderUrl=$iapppayCpUrl . "/payapi/order";
	    $queryResultUrl=$iapppayCpUrl . "/payapi/queryresult";
	    
	    //cp私钥
		$cpvkey="MIICWwIBAAKBgQCIBp43wqFy/f6Jj+fm2wsDW3TJEDuTRRnBbNZv5iS6JpKkooacoKBo0lwqPRGgh1U+6cL2RgX210XdNunvO3JN8ZzuSnujcsMaRfBRdiA/z4WiMzoRBYU4GqI7AhJ+grL6LK7jVV4RfhLPYl79jfbVhdewBS2sLYI4o4dnxSc1kwIDAQABAoGALPAaxwsasVWuCJ4tG7xTqBlHvMELHX0fpCIjubYLfiAOJCu5UvdR5y9P2XhxYVo0VGN/0EKT59d4sjtZD3rRKOeNIwcgl86ROFyo6dVCTUzLawwp16+IW9qZDpZ06ej4JKneZuhofUxWSFcTt3i1bnFYfLvhUARcbSDWHwLIoXECQQDfpNONdtxd5PjmXK6DYe6j4wuVIqCkv9taTFEPx8YWvunN9hHV0iZIxD+NFoUZ6stElfCe3xxmFxNUrPfIBbCJAkEAm7SnZItt7VoOz6oSkGn7bxgPnedXS4XW7bSAzUz5dGgn2im9VC75bDLTo580VKHJ1DScPiPzC3XFCNExZN3WOwJAJhRzrRe7CAXP17GaSWaocqFbPIaL4eGFIQfzmYNDmGXje51VhhnlSOiZhZV7DmGrQL9jHDoTJNLzCKz298j4OQJAJHvD7S8uZrr/VXFIQEZU/8bngdw+/Bc0MSZmN7SdjsUI8XluP1dyYgPWyzP2kw4FuI9LPNQ3kqvZqyM5K6U/GQJAdpVsmeW2t+cKjm92WQ9fup2jG0Wuj+U9VZaJuFfcAr8emROLcNCfDAfSUQwaH6csgAO/dfGGp7Uauu0Mu6fDlw==";
		
		//平台公钥
		$platpkey="MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCRUbUV1ThcXUytn0RXQ1fAR+UOE/x+T1zNhY6jIcEeZQWiamebyexarDeEsFjq/9Esr+yoeGSBfw/xxElumvChsZ78pzbh2buluXvIF07qWF/6ePhueG+oq6yHgBvpXj02nnZ0gOOrnFTIpmNKpW9GOLGuqhI2bqOZUrZKvipQDQIDAQAB";
	    
		$string = $_POST;//接收post请求数据
		
		$transdata = stripslashes($string['transdata']);  //删除反斜杠
		$transdata = json_decode($transdata,true);
	    $out_trade_no = $transdata['cporderid'];	//商户订单号
		$tmoney = intval($transdata['money']);		//交易金额
		$cppriva = $transdata['cpprivate'];
		
		//支付结果查询接口
		$queryReq["appid"] = "3004640340";        // 300347668  android  ios
		$queryReq["cporderid"] = $out_trade_no;
		
		//组装请求报文
		$reqData = composeReq($queryReq, $cpvkey);
		
		//发送到爱贝服务后台
		$respData = request_by_curl($queryResultUrl, $reqData, "queryResult test");
		 
		//返回报文解析
		if( ! parseResp($respData, $platpkey, $respJson)) {
		    echo "failed";exit;
		}else{
		    $this->db->Autocommit_start();
		    $dingdaninfo = $this->db->GetOne("select * from `@#_member_addmoney_record` where `code` = '$out_trade_no' and `money`='$tmoney'  and  `status` = '未付款' for update");
		    if(!$dingdaninfo){	echo "failed";exit;}	//没有该订单,失败
		    
		    $c_money = $dingdaninfo['money'];
		    $uid = $dingdaninfo['uid'];
		    $time = time();
		    
		    
		    //方便与爱贝对账
		    $ipay_transid = $transdata['transid'];    //交易流水号
		    $ipay_transtime = $transdata['transtime'];  //交易时间
		    $ipay_paytype = $transdata['paytype'];    //支付方式
		    $ipay_result = $transdata['result'];     //支付结果
		    $in_fields = " , `plat_type`=1 ,`transid` = '$ipay_transid', `transtime`='$ipay_transtime', `paytype`='$ipay_paytype',`result`='$ipay_result' ";
		     
		    
		    $up_q1 = $this->db->Query("UPDATE `@#_member_addmoney_record` SET `pay_type` = '爱贝支付', `status` = '已付款' $in_fields where `id` = '$dingdaninfo[id]' and `code` = '$dingdaninfo[code]'");
		    $up_q2 = $this->db->Query("UPDATE `@#_member` SET `money` = `money` + $c_money where (`uid` = '$uid')");
		    $up_q3 = $this->db->Query("INSERT INTO `@#_member_account` (`uid`, `type`, `pay`, `content`, `money`, `time`) VALUES ('$uid', '1', '账户', '充值', '$c_money', '$time')");
		    /************ 好友获得积分 start  2015/12/15 **************/
		    $up_q4 = true;
		    $up_q5 = true;
		    $up_q6 = true;
		    $record_id = $this->db->insert_id();		//充值记录的ID
		    $addtime = time();		//添加时间
		    $score = array('1'=>4, '2'=>3, '3'=>2);		//积分表
		    $friends = $this->db->GetList("SELECT yaoqing,lev FROM `@#_member_friendship` WHERE `uid`='$uid' ");
		    if(count($friends)){
		        foreach($friends as $k => $v){
		            $yaoqing = $v['yaoqing'];
		            $lev = $v['lev'];
		            $jifen = $c_money * $score[$lev] ;
		            $this->db->Query("UPDATE `@#_member` SET `score` = `score` + $jifen where (`uid` = '$yaoqing')");
		            $upp[$k] = $this->db->Query("INSERT INTO `@#_member_jifen`(`uid`,`yaoqing`,`lev`,`jifen`,`record_id`,`addtime`) VALUES('$uid','$yaoqing','$lev','$jifen','$record_id','$addtime')");
		        }
		        $up_q4 = isset($upp[0]) ? $upp[0] : true;
		        $up_q5 = isset($upp[1]) ? $upp[1] : true;
		        $up_q6 = isset($upp[2]) ? $upp[2] : true;
		    }
		    /************ 好友获得积分 end  2015/12/15 ********以下的判断中添加了up_q4  up_q5  up_q6的判断******/
		    /*活动的start*/
		    System::load_app_fun('member','member');
		    $chong_userinfo = getchongzhi_user($dingdaninfo['uid']);
		    active_cash($dingdaninfo['uid'],$c_money,$chong_userinfo['mobile']);
		    /*活动的end*/
		    
		    
		    if($up_q1 && $up_q2 && $up_q3 && $up_q4 && $up_q5 && $up_q6){
		        $this->db->Autocommit_commit();
		    }else{
		        $this->db->Autocommit_rollback();
		        echo "failed";exit;
		    }
		    
		    //////////////////////////////////////////////////////
			///如果是购买商品，需要继续执行余额支付   20151227 kpf///
			/////////////////////////////////////////////////////
			$scookies = unserialize($dingdaninfo['scookies']);			
			$wpay = System::load_app_class('wpay','pay');
			$wpay->scookie = $scookies;	

			$ok = $wpay->init($uid,"爱贝支付",'go_record',$scookies);	//众乐商品	
			if($ok != 'ok'){
				echo "failed";exit;	//商品购买失败			
			}			
			$check = $wpay->go_pay(1,$dingdaninfo['code']);
			if($check){
				//$this->db->Query("UPDATE `@#_member_addmoney_record` SET `scookies` = '1' where `code` = '$out_trade_no' and `status` = '已付款'");
				_setcookie('Cartlist',NULL);
				echo "success";exit;			
			}else{
				echo "failed";exit;
			}
		}
	}

/*	public function testpay() {
		$wpay = System::load_app_class('wpay','pay');
		$info =$this->db->GetOne("SELECT * FROM `@#_member_addmoney_record` WHERE uid = 15 and code='C14620027123971118'");
		print_r($info);
		$scookies = unserialize($info['scookies']);
		$uid = $info['uid'];
		print_r($scookies);
		$wpay->scookie = $scookies;
		$ok = $wpay->init($uid,"爱贝支付",'go_record',$scookies);	//众乐商品	
		echo $ok;
			if($ok != 'ok'){
				echo "failed";exit;	//商品购买失败			
			}			
		$check = $wpay->go_pay(1,$info['code']);
		print_r($check);
	}*/
	
   
}

?>