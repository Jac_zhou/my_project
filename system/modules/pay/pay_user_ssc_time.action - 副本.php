<?php 

defined('G_IN_SYSTEM')or exit('No permission resources.');
System::load_app_class('base','member','no');
System::load_app_fun('global',G_ADMIN_DIR);
System::load_app_fun("pay","pay");
System::load_sys_fun("user");
System::load_app_fun('user','go');
System::load_app_class("tocode","pay",'no');
class pay_user_ssc_time extends base {
	public function __construct(){		
		parent::__construct();		
		$this->db=System::load_sys_class('model');
	} 	

	//支付列表
	public function init(){
		
	    $shopid = isset($_POST['gid']) ? $_POST['gid'] : false;	
	    $shop=$this->db->GetOne("select * from `@#_shoplist` where `id` = '$shopid'   LIMIT 1 for update"); 
		ignore_user_abort(true);
		set_time_limit(0);
		if($shop['canyurenshu'] >= $shop['zongrenshu'] && $shop['maxqishu'] >= $shop['qishu']){	
			$time = sprintf('%.2f',microtime(TRUE));
			$JiexiaoAction = System::load_app_class('Jiexiao','Model');	
			//先计算出本期产品的中奖号码   并且更新本期产品的中奖信息
			//开始进行开奖
			$this->db->Autocommit_start();
			$query_insert  = $JiexiaoAction->index($shop,$time);
			//然后计算出倒计时的时间并且休眠   直到倒计时为0（休眠结束）  更改本期产品数据，关闭揭晓动画
			if(!$query_insert){
				$this->db->Autocommit_rollback();								
			}else{
				$this->db->Autocommit_commit();		
				//读取更新后的数据
				$shop = $this->db->GetOne("select * from `@#_shoplist` where `id` = '$shopid'"); 				
			}

			$xsjx_time = $shop['xsjx_time'];
			//限时揭晓的商品
			if($xsjx_time !='0'){
				$wait_time=30;								//等待时间
				
				$new_xsjx_time=$xsjx_time+$wait_time;	
				$q_djstime=$new_xsjx_time-$time;			//倒计时
				$sql = "UPDATE `@#_shoplist` SET `xsjx_time` = '$new_xsjx_time',`q_showtime` = 'N',`q_djstime` = '$q_djstime',`q_end_time` = '$new_xsjx_time' where `id` = '$shop[id]'";
				$q3 = $this->db->Query($sql);
				$timeout = $q_djstime;
				
			}else{
				//倒计时>0
				if($shop['q_end_time'] > 0){
					$timeout=$shop['q_end_time']-time();									
				}else{
					$this->GetTimeOut($shop);
					$post_arr= array("gid"=>$shopid);
					_g_triggerRequest(WEB_PATH.'/pay/pay_user_ssc_time/init',false,$post_arr);					 			
				}						
			}				
			$this->db->Query("UPDATE `@#_shoplist` SET `canyurenshu`=`zongrenshu`,`shenyurenshu` = '0' where `id` = '$shop[id]'");
			sleep($timeout);
			
			//关闭揭晓动画,让用户可以看到  中奖码
			$this->db->Query("UPDATE `@#_shoplist` SET `q_show_time`='N' where `id` = '$shop[id]'");
        }
    }
	
	public function SocketTimeOut($timeout,$shopid){
		ignore_user_abort(true);
		set_time_limit(0);					
		sleep($timeout);
		$post_arr= array("gid"=>$shopid);
		_g_triggerRequest(WEB_PATH.'/pay/pay_user_ssc_time/init',false,$post_arr);		
	
	}	
	public function GetTimeOut($shop){
		//分解购买结束时的时间
		$sj_y=date("Y",microtime(true));
		$sj_m=date("m",microtime(true));
		$sj_d=date("d",microtime(true));
		$sj_h=date("H",microtime(true));
		$sj_i=date("i",microtime(true));
		$sj_s=date("s",microtime(true));
		$shi_1=substr(date("i",microtime(true)),0,1);
		$shi_2=substr(date("i",microtime(true)),1,2);
		$zjsi=(int)($sj_h.$sj_i.$sj_s);
		$wait_time=30;//等待时间
		$q_djstime_update1 = "UPDATE `@#_shoplist` SET `q_showtime` = 'Y' where `id` = '$shop[id]'";
		$q1 = $this->db->Query($q_djstime_update1);
		//中间时时彩没有开奖的区域
		if($zjsi>=15600&&$zjsi<=95100){
			//$q_djstime=1;			//这个是修改之前的  注释日期:2015.12.7 9:58
			//2015.12.7 9:58 处理10点之前,商品倒计时问题。以下三行为添加的代码
			$kaijiang_time = strtotime($sj_y.'-'.$sj_m.'-'.$sj_d.' 10:00:00');
			$jiange_time = $kaijiang_time - time();
			$q_djstime = $jiange_time;				
		}
		if($zjsi>95100&&$zjsi<220100){//时时彩时隔10分钟开奖一次的区域
			if($shi_2==1&&$sj_s==30){//判断分钟数是否为整数
				$q_djstime=599;//等待时间					
			}
			else{
			   if($shi_1==5){
				$shi_15=(int)($shi_2);
				$q_djstime=(10-$shi_15)*60;				   
			   }
			   else{
				$sj_i=((int)($shi_1)+1)."1";
				if($sj_s<30)
				$redytime=$sj_y.$sj_m.$sj_d.$sj_h.$sj_i.'30';
				if($sj_s>=30)
				$redytime=$sj_y.$sj_m.$sj_d.$sj_h.$sj_i.$sj_s;
				$redytime=strtotime($redytime);
				$nowtime=sprintf("%.0f",microtime(true));
				$q_djstime=$redytime-$nowtime;						
			   }						
			}
		}
							
		if(($zjsi>=220100&&$zjsi<=235959)||($zjsi>=0&&$zjsi<15600)){//时时彩时隔5分钟开奖一次的区域
			 if(($shi_2==1||$shi_2==6)&&$sj_s==30){
				$q_djstime=299;//等待时间
				}
			if(($shi_1==5&&$shi_2>6)||($shi_1==5&&$shi_2==6&&$sj_s>=30)){
			$sj_h=(int)($sj_h)+1;
			$redytime=$sj_y.$sj_m.$sj_d.$sj_h.'01'.'30';
			$redytime=strtotime($redytime)+$wait_time;
			$nowtime=sprintf("%.0f",microtime(true));
			$q_djstime=$redytime-$nowtime;	//等待时间
			}							
			if(($shi_2>=1&&$shi_2<6)||($shi_2==6&&$sj_s<30)){
			$sj_i=(int)($shi_1).'6';
			if($sj_s<30)
			$redytime=$sj_y.$sj_m.$sj_d.$sj_h.$sj_i.'30';
			if($sj_s>=30)
			$redytime=$sj_y.$sj_m.$sj_d.$sj_h.$sj_i.$sj_s;
			$redytime=strtotime($redytime);
			$nowtime=sprintf("%.0f",microtime(true));
			$q_djstime=$redytime-$nowtime;	//等待时间
				}
			if(($shi_2>6&&$shi_2<=9)||($shi_2==6&&$sj_s>=30)||($shi_2==0)){
			$sj_i=((int)($shi_1)+1)."1";
			if($sj_s<30)
			$redytime=$sj_y.$sj_m.$sj_d.$sj_h.$sj_i.'30';
			if($sj_s>=30)
			$redytime=$sj_y.$sj_m.$sj_d.$sj_h.$sj_i.$sj_s;
			$redytime=strtotime($redytime);
			$nowtime=sprintf("%.0f",microtime(true));
			$q_djstime=$redytime-$nowtime;	//等待时间
			}							
		}
		$this->db->Autocommit_start();
		$yjq_djstime=time()+$q_djstime;	
		$yjq_djstime=sprintf("%.3f",$yjq_djstime);
		$q_djstime_update3 = "UPDATE `@#_shoplist` SET `q_djstime` = '$q_djstime',`q_end_time` = '$yjq_djstime' where `id` = '$shop[id]'";
		$q2 = $this->db->Query($q_djstime_update3);	
		$this->db->Autocommit_commit();				
	}	
	
	
}
?>