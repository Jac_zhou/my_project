<?php 



/*
*   生成购买的众乐码
*	user_num 		@生成个数
*	shopinfo		@商品信息
*	ret_data		@返回信息
*/
function pay_get_shop_codes($user_num=1,$shopinfo=null,&$ret_data=null){
 
		$db = System::load_sys_class("model");
		$ret_data['query'] = true;
		$table = '@#_'.$shopinfo['codes_table'];
		$codes_arr = array();
		$codes_one = $db->GetOne("select id,s_id,s_cid,s_len,s_codes from `$table` where `s_id` = '$shopinfo[id]' order by `s_cid` DESC  LIMIT 1 for update");
		$codes_arr[$codes_one['s_cid']] = $codes_one;		
		$codes_count_len = $codes_arr[$codes_one['s_cid']]['s_len'];

		if($codes_count_len < $user_num && $codes_one['s_cid'] > 1){		
			for($i=$codes_one['s_cid']-1;$i>=1;$i--):
				$codes_arr[$i] = $db->GetOne("select id,s_id,s_cid,s_len,s_codes from `$table` where `s_id` = '$shopinfo[id]' and `s_cid` = '$i'  LIMIT 1 for update");
				$codes_count_len += $codes_arr[$i]['s_len'];			
				if($codes_count_len > $user_num)  break;
			endfor;
		}
		
		if($codes_count_len < $user_num) $user_num = $codes_count_len;
		
		$ret_data['user_code'] = '';
		$ret_data['user_code_len'] = 0;
		
		foreach($codes_arr as $icodes){			
			$u_num = $user_num;			
			$icodes['s_codes'] = unserialize($icodes['s_codes']);	
			$code_tmp_arr = array_slice($icodes['s_codes'],0,$u_num);
			$ret_data['user_code'] .= implode(',',$code_tmp_arr);	
			$code_tmp_arr_len = count($code_tmp_arr);
			
			if($code_tmp_arr_len < $u_num){
				$ret_data['user_code'] .= ',';
			}
			
			$icodes['s_codes'] = array_slice($icodes['s_codes'],$u_num,count($icodes['s_codes']));
			$icode_sub = count($icodes['s_codes']);		
			$icodes['s_codes'] = serialize($icodes['s_codes']);

			if(!$icode_sub){
				$query = $db->Query("UPDATE `$table` SET `s_cid` = '0',`s_codes` = '$icodes[s_codes]',`s_len` = '$icode_sub' where `id` = '$icodes[id]'");
				if(!$query)$ret_data['query'] = false;
			}else{		
				$query = $db->Query("UPDATE `$table` SET `s_codes` = '$icodes[s_codes]',`s_len` = '$icode_sub' where `id` = '$icodes[id]'");
				if(!$query)$ret_data['query'] = false;
			}
			$ret_data['user_code_len'] += $code_tmp_arr_len;
			$user_num  = $user_num - $code_tmp_arr_len;
			
			
		}
		
}


//生成订单号
function pay_get_dingdan_code($dingdanzhui=''){
	return $dingdanzhui.time().substr(microtime(),2,6).rand(0,9);
}


/*
	揭晓与插入商品
	@shop   商品数据
*/

function pay_insert_shop_x($shop='',$type=''){
	
	$g_c_x = System::load_app_config("get_code_x",'',"pay");
	if(is_array($g_c_x) && isset($g_c_x['class'])){
		$gcx_db = System::load_app_class($g_c_x['class'],"pay");
	}else{
		$g_c_x = array("class"=>"tocode");
		$gcx_db = System::load_app_class($g_c_x['class'],"pay");
	}
		
	$gcx_db->config($shop,$type);
	$gcx_db->get_run_tocode();
	$ret_data = $gcx_db->returns();
	
	
	
}

/*
	揭晓与插入商品
	@shop   商品数据
*/
function pay_insert_shop($shop='',$type=''){
	$time=sprintf("%.3f",microtime(true));
	$db = System::load_sys_class("model");
	if($shop['xsjx_time'] != '0'){
		return $db->Query("UPDATE `@#_shoplist` SET `canyurenshu`=`zongrenshu`,	`shenyurenshu` = '0' where `id` = '$shop[id]'");
	}
	$tocode = System::load_app_class("tocode","pay");
	$tocode->shop = $shop;	
	@file_put_contents("qq.txt","pay_insert_shop\n") ;
	$tocode->run_tocode($time,100,$shop['canyurenshu'],$shop);
	/**时时彩LVDENG修改**/
	$code =$tocode->go_code;
	$ssc_code=$tocode->ssc_code;
	$ssc_opentime=$tocode->ssc_opentime;
	$ssc_phase=$tocode->ssc_phase;
	$content = addslashes($tocode->go_content);
	$counttime = $tocode->count_time;
	while(!$tocode->go_code){		sleep(1);
		$time=sprintf("%.3f",microtime(true));
		$tocode->run_tocode($time,100,$shop['canyurenshu'],$shop);
		$code =$tocode->go_code;
		$ssc_code=$tocode->ssc_code;
		$ssc_opentime=$tocode->ssc_opentime;
		$ssc_phase=$tocode->ssc_phase;
		$content = addslashes($tocode->go_content);
		$counttime = $tocode->count_time;
	}
	/**时时彩LVDENG修改**/	
	
	$u_go_info = $db->GetOne("select * from `@#_member_go_record` where `shopid` = '$shop[id]' and `shopqishu` = '$shop[qishu]' and `goucode` LIKE '%$code%'");	
	$u_info = $db->GetOne("select uid,username,email,mobile,img from `@#_member` where `uid` = '$u_go_info[uid]'");
	
	//更新商品
	$query = true;
	if($u_info){		
		$u_info['username'] = _htmtocode($u_info['username']);
		$q_user = serialize($u_info);
		$gtimes = (int)System::load_sys_config('system','goods_end_time');
		if($gtimes == 0 || $gtimes == 1){
			$q_showtime = 'N';
		}else{
			$q_showtime = 'Y';
		}
		
		/** kpf 20151219  如果人数满了的时候统计过最后100条记录，那么这里就不写入数据库    start **/
		$in_sql = " ";
		$is_content = $db->GetOne("SELECT q_content,q_counttime,q_sscphase FROM @#_shoplist WHERE `id`='$shop[id]' ");
		if( ! $is_content['q_content']  ){
		    $in_sql .= " `q_content` = '$content',";
		}
		if( ! $is_content['q_counttime'] ){
		    $in_sql .=" `q_counttime` ='$counttime' ,  ";
		}
		if(!$is_content['q_sscphase']){
		    $in_sql .= " `q_sscphase` = '$ssc_phase' , ";
		}
		/** kpf 20151219  如果人数满了的时候统计过最后100条记录，那么这里就不写入数据库    end **/
		
	/**时时彩LVDENG修改**/		
		$sqlss = "UPDATE `@#_shoplist` SET `canyurenshu`=`zongrenshu`,`shenyurenshu` = '0',`q_uid` = '$u_info[uid]',".
				 " `q_user` = '$q_user',`q_user_code` = '$code', $in_sql `q_end_time` = '$time',".
				 " `q_showtime` = '$q_showtime',`q_ssccode` = '$ssc_code',`q_sscopen` = '$ssc_opentime' ".
				 "  where `id` = '$shop[id]'";							 
		$q = $db->Query($sqlss);
	/**时时彩LVDENG修改**/		
		if(!$q)$query = false;	
				
		if($q){
			$q = $db->Query("UPDATE `@#_member_go_record` SET `huode` = '$code' where `id` = '$u_go_info[id]' and `code` = '$u_go_info[code]' and `uid` = '$u_go_info[uid]' and `shopid` = '$shop[id]' and `shopqishu` = '$shop[qishu]'");
			if(!$q) {
				$query = false;
			}else{
				$post_arr= array("uid"=>$u_info['uid'],"gid"=>$shop['id'],"send"=>1);
				_g_triggerRequest(WEB_PATH.'/api/send/send_shop_code',false,$post_arr);
			}
		}else{
			$query =  false;
		}
	}else{	
		$query =  false;
	}
	
	/******************************/
	
	if($query){
		do_create_next_qishu($shop);
	}
	/*新建*/
	/*	20151207 kangpengfei  注释182—192行的代码 用于测试 人数一满就创建下一期产品； 若测试失败，则以下代码需取消注释
	if($query){
		if($shop['qishu'] < $shop['maxqishu']){		
			$maxinfo = $db->GetOne("select * from `@#_shoplist` where `sid` = '$shop[sid]' order by `qishu` DESC LIMIT 1");
			if(!$maxinfo){
				$maxinfo=array("qishu"=>$shop['qishu']);
			}			
			System::load_app_fun("content",G_ADMIN_DIR);
			$intall = content_add_shop_install($maxinfo,false);		
			if(!$intall) return $query;
		}
	}
	*/
	
	return $query;
}

/*
* 20151207 用于测试人数一满就创建下一期产品；如测试失败，则以下代码不可用，应调正
* kangpengfei
*/
function do_create_next_qishu($shop)
{
	$db = System::load_sys_class("model");
	
	/*** kpf 20151219 人数一满就计算出最后100条记录，并统计出和    start ***/
	$shop['q_counttime'] = $shop['q_counttime'] ? : '';
    if( $shop['q_counttime']=='' ){
        $mic_time=sprintf("%.3f",microtime(true));
        $tocode = System::load_app_class("tocode","pay");
    	$tocode->shop = $shop;	
    	$tocode->run_tocode($mic_time,100,$shop['canyurenshu'],$shop);
    	$content = addslashes($tocode->go_content);
    	$counttime = $tocode->count_time;
    	$ssc_phase=$tocode->ssc_phase;
    	while(!$tocode->go_code){
    		$mic_time=sprintf("%.3f",microtime(true));
    		$tocode->run_tocode($mic_time,100,$shop['canyurenshu'],$shop);
    		$content = addslashes($tocode->go_content);
    		$counttime = $tocode->count_time;
    		$ssc_phase=$tocode->ssc_phase;
    	}
    	$today = date("Ymd");
    	if(strpos($ssc_phase,$today) !== false){
    	    $ssc_phase = $ssc_phase + 1;
    	}else{
    	    $ssc_phase = $today.'001';
    	}
    	//计算下一期时时彩期号
    	$sql = "UPDATE @#_shoplist SET `q_content`='$content' , `q_counttime`='$counttime' , `q_sscphase`='$ssc_phase'  WHERE `id`='$shop[id]' AND `shenyurenshu`=0 ";
    	$db->Query($sql);
	}
	/** kpf 20151219 人数一满就计算出最后100条记录，并统计出和    start **/
	
	//清空app端购物车信息  2015/12/31
	if(isset($shop['shenyurenshu']) && $shop['shenyurenshu'] == 0){
	    $db->Query("DELETE FROM @#_app_cart WHERE `shopid` = '$shop[id]' ");
	}
	
	$time = time();
	$res = $db->GetCount("SELECT id FROM @#_shoplist WHERE `sid` = '$shop[sid]' AND `id` > '$shop[id]' AND `shenyurenshu` > 0");
	if( ! $res){
		if($shop['qishu'] < $shop['maxqishu']){		
			$maxinfo = $db->GetOne("select * from `@#_shoplist` where `sid` = '$shop[sid]' order by `qishu` DESC LIMIT 1");
			if(!$maxinfo){
				$maxinfo=array("qishu"=>$shop['qishu']);
			}			
			System::load_app_fun("content",G_ADMIN_DIR);
			$intall = content_add_shop_install($maxinfo,false);		
			if(!$intall) return $intall;
		}
	}	
}


/*
	众乐基金
	go_number @众乐人次
*/
function pay_go_fund($go_number=null){
	if(!$go_number)return true;
	$db = System::load_sys_class("model");
	$fund = $db->GetOne("select * from `@#_fund` where 1");
	if($fund && $fund['fund_off']){
		$money = $fund['fund_money'] * $go_number + $fund['fund_count_money'];
		return $db->Query("UPDATE `@#_fund` SET `fund_count_money` = '$money'");
	}else{
		return true;
	}
}


/*
	用户佣金
	uid 		用户id
	dingdancode	@订单号
*/
function pay_go_yongjin($uid=null,$dingdancode=null){
	if(!$uid || !$dingdancode)return true;
	$db = System::load_sys_class("model");$time=time();
	$config = System::load_app_config("user_fufen",'','member');//福分/经验/佣金
	$yesyaoqing=$db->GetOne("SELECT `yaoqing` FROM `@#_member` WHERE `uid`='$uid'");
	if($yesyaoqing['yaoqing']){
		$yongjin=$config['fufen_yongjin']; //每一元返回的佣金				
	}else{
		return true;
	}	
	$yongjin = floatval(substr(sprintf("%.3f",$yongjin), 0, -1));
	$gorecode=$db->GetList("SELECT * FROM `@#_member_go_record` WHERE `code`='$dingdancode'");
	foreach($gorecode as $val){
		$y_money=$val['moneycount'] * $yongjin;
		$content="(第".$val['shopqishu']."期)".$val['shopname'];
		$db->Query("INSERT INTO `@#_member_recodes`(`uid`,`type`,`content`,`shopid`,`money`,`ygmoney`,`time`)VALUES('$uid','1','$content','$val[shopid]','$y_money','$val[moneycount]','$time' )"); 				
	}
	
}

?>

