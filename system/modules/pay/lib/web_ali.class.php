<?php

//支付宝wap端支付

include dirname(__FILE__).DIRECTORY_SEPARATOR."alipay".DIRECTORY_SEPARATOR."alipay_submit.class.php";
class web_ali{
	private $config;
	private $info;
	private $key = "2088901039824985";
	private $script = "vudx4el82wvdvmteu84320h1231vxo5n";
	private $mail = "329174302@qq.com";

	public function init($config) {
		$this->config = $config;
	}

	//及时到账
	public function getInfo(){
		$config = $this->config;

		$payment_type = "1";
		 //服务器异步通知页面路径
        $notify_url = $config['NotifyUrl'];
        //页面跳转同步通知页面路径
        $return_url = $config['ReturnUrl'];		
        //卖家支付宝帐户 必填
        $seller_email = $this->mail;
        //商户订单号 必填
        $out_trade_no = $config['code'];
        //订单名称 必填
        $subject = $config['title'];
        //付款金额 必填
        $total_fee = $config['money'];
		//$total_fee = 0.01;
        //订单描述
        $body = '';
        //商品展示地址
        $show_url = '';
        //需以http://开头的完整路径，例如：http://www.xxx.com/myorder.html
        //防钓鱼时间戳
        $anti_phishing_key = "";
        //若要使用请调用类文件submit中的query_timestamp函数
        //客户端的IP地址
        $exter_invoke_ip = "";
        //非局域网的外网IP地址，如：221.0.0.1		
	

	
		$alipay_config_id = $this->key;                                  	//合作身份者id，以2088开头的16位纯数字
		$alipay_config_key = $this->script;									//安全检验码，以数字和字母组成的32位字符
		$alipay_config_input_charset = strtolower('utf-8');
		
		//构造要请求的参数数组，无需改动
		/*$parameter = array(
				"service" => "alipay.wap.create.direct.pay.by.user",
				"partner" => $config['id'],
				'seller_id' => $alipay_config_id,
				"payment_type"	=> $payment_type,
				"notify_url"	=> $notify_url,
				"return_url"	=> $return_url,
				"seller_email"	=> $seller_email,
				"out_trade_no"	=> $out_trade_no,
				"subject"	=> $subject,
				"total_fee"	=> $total_fee,
				"body"	=> $body,
				"show_url"	=> $show_url,
				"anti_phishing_key"	=> $anti_phishing_key,
				"exter_invoke_ip"	=> $exter_invoke_ip,
				"_input_charset"	=> $alipay_config_input_charset
		);*/
/*$parameter = array(
		"service" => "alipay.wap.create.direct.pay.by.user",
		"partner" => $alipay_config_id,
		"seller_id"  => $alipay_config_id,
		"payment_type"	=> $payment_type,
		"notify_url"	=> $notify_url,
		"return_url"	=> $return_url,
		"_input_charset"	=> $alipay_config_input_charset,
	);*/

		$parameter = array(
				"service"       => "alipay.wap.create.direct.pay.by.user",
				"partner"       => $alipay_config_id,
				"seller_id"  => $alipay_config_id,
				"payment_type"	=> $payment_type,
				"notify_url"	=> $notify_url,
				"return_url"	=> $return_url,
				"_input_charset"	=> $alipay_config_input_charset,
				"out_trade_no"	=> $out_trade_no,
				"subject"	=> $subject,
				"total_fee"	=> $total_fee,
				"show_url"	=> $show_url,
				"body"	=> $body,
				);

		
		//签名方式 不需修改
		$alipay_config_sign_type = strtoupper('MD5');		
		//字符编码格式 目前支持 gbk 或 utf-8
		$alipay_config_input_charset = strtolower('utf-8');
		//ca证书路径地址，用于curl中ssl校验
		//请保证cacert.pem文件在当前文件夹目录中
		//$alipay_config_cacert    = getcwd().'\\cacert.pem';	
		$alipay_config_cacert =  dirname(__FILE__).DIRECTORY_SEPARATOR."alipay".DIRECTORY_SEPARATOR."cacert.pem";		
		$alipay_config_transport   = 'http';
		
		$alipay_config=array(
			"partner"      =>$alipay_config_id,
			"key"          =>$alipay_config_key,
			"sign_type"    =>$alipay_config_sign_type,
			"input_charset"=>$alipay_config_input_charset,
			"cacert"       =>$alipay_config_cacert,
			"transport"    =>$alipay_config_transport
		);
		
		$alipaySubmit = new AlipaySubmit($alipay_config);
		/*
			$html_text = $alipaySubmit->buildRequestForm($parameter,"get", "确认");
			echo $html_text;
		*/
		//$this->url =  urldecode($alipaySubmit->buildRequestGet($parameter));
		$this->url = $alipaySubmit->buildRequestForm($parameter,'POST','submit');
		return $this->url;
	}


}
