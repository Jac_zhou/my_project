﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>微信扫码支付</title>
<link rel="stylesheet" type="text/css" href="<?php echo G_TEMPLATES_STYLE?>/css/Comm_weixin.css" />
<link rel="stylesheet" type="text/css" href="<?php echo G_TEMPLATES_STYLE?>/css/WeixinPay.css" />
<script language="javascript" type="text/javascript" src="<?php echo G_GLOBAL_STYLE; ?>/global/js/weixin/JQuery132.js"></script>
<script language="javascript" type="text/javascript">
//$(function(){var e=$("#qr_box");var c=$("#guide");c.css({left:"50%",opacity:0});e.hover(function(){c.css("display","block").stop().animate({marginLeft:"+156px",opacity:1},900,"swing",function(){c.animate({marginLeft:"+143px"},300)})},function(){c.stop().animate({marginLeft:"-101px",opacity:0},"400","swing",function(){c.hide()})});var d=$("#dingdan").attr("ddcode");var url="<?php echo WEB_PATH;?>/pay/wxpay_url/houtai/";var sinval = setInterval(function(){$.ajaxSetup({async:true});$.post(url,{out_trade_no:d},function(_status){if(_status=='success'){clearInterval(sinval);location.replace("<?php echo $qiantai_url; ?>")})},5000)});
var is_tips = true;
$(function() {
    var e = $("#qr_box");
    var c = $("#guide");
    c.css({
        left: "50%",
        opacity: 0
    });
    e.hover(function() {
        c.css("display", "block").stop().animate({
            marginLeft: "+156px",
            opacity: 1
        },
        900, "swing", 
        function() {
            c.animate({
                marginLeft: "+143px"
            },
            300)
        })
    },
    function() {
        c.stop().animate({
            marginLeft: "-101px",
            opacity: 0
        },
        "400", "swing", 
        function() {
            c.hide()
        })
    });
    var d = $("#dingdan").attr("ddcode");
    var check_url = "<?php echo WEB_PATH;?>/pay/wxpay_url/check_is_pay/";
    var url = "<?php echo WEB_PATH;?>/pay/wxpay_url/houtai/";
    var qiantai = "<?php echo $qiantai_url; ?>";
    setInterval(function() {
            $.ajaxSetup({
                async: false
            });
            $.post(check_url, {out_trade_no: d},function(_status) {
                if (_status == 'NOPAY'){
               	 	$.post(url, {out_trade_no: d},function(){});
                }else if(_status == 'PAY'){
                	is_tips = false;
					window.location.href=qiantai;
                }else{
                	window.location.href="<?php echo WEB_PATH?>";
                }
            });
        },
        3000)
    });
    window.onbeforeunload = function(){
        if(is_tips){
    		return '您还没有支付成功，确定要关闭页面吗？';
        }
    }
</script>	
</head>
<body>
    <input name="hidShopID" type="hidden" id="hidShopID" value="141027161101330617" />
    <input name="hidIsBuyPay" type="hidden" id="hidIsBuyPay" value="1" />
    <div class="wx_header">
        <div class="wx_logo"><img title="云购系统微信支付" alt="微信支付标志" src="<?php echo G_TEMPLATES_STYLE?>/images/wxlogo_pay.png" /></div>
    </div>
    <div class="weixin">
        <div class="weixin2">
            <b class="wx_box_corner left pngFix"></b><b class="wx_box_corner right pngFix"></b>
            <div class="wx_box pngFix">

                <div class="wx_box_area">
                    <div class="pay_box qr_default">
                        <div class="area_bd"><span class="wx_img_wrapper"  id="qr_box">
                           <div align="center" id="qrcode" class="ewm_wrapper"></div>
                            <img style="left: 50%; opacity: 0; display: none; margin-left: -101px;" class="guide pngFix" src="<?php echo G_TEMPLATES_STYLE?>/images/wxwebpay_guide.png" alt="" id="guide" />
                        </span>
                            <div class="msg_default_box"><i class="icon_wx pngFix"></i>
                                <p>
                                    请使用微信扫描<br/>
                                    二维码以完成支付
                                </p>
                            </div>
                            <div class="msg_box"><i class="icon_wx pngFix"></i>
                                <p><strong>扫描成功</strong>请在手机确认支付</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="wx_hd">
                    <div class="wx_hd_img icon_wx"></div>
                </div>
                <div class="wx_money"><span>￥</span><?php echo $config['money'];?></div>
                <!--支付订单号-->
                <div class="wx_pay">
                    <p><span  class="wx_left">支付订单号</span>
					<span id="dingdan"  ddcode="<?php echo $config['code'];?>"  class="wx_right" ><?php echo $config['code'];?></span></p>
					 <p><span  class="wx_left">订单时间</span>
					<span  class="wx_right" ><?php echo date("Y-m-d H:i:s",time());?></span></p>
                    <p><span class="wx_left">商品名称</span><span class="wx_right">财神商品</span></p>
                </div>
                <div class="wx_kf">
                    <div class="wx_kf_img icon_wx"></div>
                    <div class="wx_kf_wz">
                        <p><?php echo _cfg('web_name');?></p>
                        <p><?php echo _cfg('cell');?></p>
                    </div>
                </div>
            </div>
        </div>
    </div> 

	<script type="text/javascript"  src="<?php echo G_GLOBAL_STYLE; ?>/global/js/weixin/qrcode.js"></script>
	<script type="text/javascript" >
		if(<?php echo $unifiedOrderResult["code_url"] != NULL; ?>)
		{
			var url = "<?php echo $code_url;?>";
			console.log(url);
			//参数1表示图像大小，取值范围1-10；参数2表示质量，取值范围'L','M','Q','H'
			var qr = qrcode(10, 'M');
			qr.addData(url);
			qr.make();
			var wording=document.createElement('p');
			wording.innerHTML = "支付完成前，请勿关闭此页面！";
			var code=document.createElement('DIV');
			code.innerHTML = qr.createImgTag();
			var element=document.getElementById("qrcode");
			element.appendChild(wording);
			element.appendChild(code);
		}
	</script>
</body>
</html>
