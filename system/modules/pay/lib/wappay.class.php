<?php 

include "wappay/lib/alipay_submit.class.php";
class wappay {
	
	private $config;
	private $url;
	//主入口
	public function config($config=null){
		$this->config = $config;
		$config['type'] = 1;  //支付类型   1及时到账
		if($config['type'] == 1){
			$this->config_jsdz();
		}
	}
	
	//及时到账
	private function config_jsdz(){
	    
	    $config = $this->config;
	    
        //支付类型
        $payment_type = "1";
        
        //服务器异步通知页面路径
        $notify_url = $config['NotifyUrl'];

        //页面跳转同步通知页面路径
        $return_url = $config['ReturnUrl'];

        //商户订单号
        $out_trade_no = $config['Code'];

        //订单名称
        $subject = $config['Title'];

        //付款金额
        $total_fee = $config['Money'];

        //商品展示地址
        $show_url = $config['ShowUrl'];

        //订单描述
        $body = $config['Body'];

        //超时时间
        $it_b_pay = '';

        //钱包token
        $extern_token = '';
        
        $alipay_config['partner'] = $config['Partner'];
        $alipay_config['seller_id']	= $alipay_config['partner'];
        $alipay_config['private_key_path']	= G_SYSTEM."modules/pay/lib/wappay/key/rsa_private_key.pem";
        $alipay_config['ali_public_key_path']= G_SYSTEM."modules/pay/lib/wappay/key/alipay_public_key.pem";
        $alipay_config['sign_type']    = strtoupper('RSA');
        $alipay_config['input_charset']= strtolower('utf-8');
        $alipay_config['cacert']    = G_SYSTEM."modules/pay/lib/wappay/cacert.pem";	//ca证书路径地址;
        $alipay_config['transport']    = 'http';


        //构造要请求的参数数组，无需改动
        $parameter = array(
    		"service" => "alipay.wap.create.direct.pay.by.user",
    		"partner" => trim($alipay_config['partner']),
    		"seller_id" => trim($alipay_config['seller_id']),
    		"payment_type"	=> $payment_type,
    		"notify_url"	=> $notify_url,
    		"return_url"	=> $return_url,
    		"out_trade_no"	=> $out_trade_no,
    		"subject"	=> $subject,
    		"total_fee"	=> $total_fee,
    		"show_url"	=> $show_url,
    		"body"	=> $body,
    		"it_b_pay"	=> $it_b_pay,
    		"extern_token"	=> $extern_token,
    		"_input_charset"	=> trim(strtolower($alipay_config['input_charset']))
        );
        
        $alipaySubmit = new AlipaySubmit($alipay_config);
        $this->url = $alipaySubmit->buildRequestForm($parameter,"get","确认");
	}
	
	//发送
	public function send_pay(){
		 echo  $this->url;
		 exit;
	}
	
	//获取
	public function get_pay()
	{
	    return $this->url;
	}
}

?>
