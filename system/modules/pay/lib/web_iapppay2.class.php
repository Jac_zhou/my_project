<?php
/*
* 爱贝支付
* 2015/12/30
*/
include dirname(__FILE__)."/iapppay/web_base.php";
include dirname(__FILE__)."/iapppay/web_config.php";


class web_iapppay {
	private $config;
	private $url;

	//主入口
	public function init($config=null){
		//echo dirname(__FILE__)."/iapppay/web_base.php";
	   	$iapppayCpUrl="http://ipay.iapppay.com:9999";
		//登录令牌认证接口 url
		$tokenCheckUrl=$iapppayCpUrl . "/openid/openidcheck";
		
		//下单接口 url
		$orderUrl=$iapppayCpUrl . "/payapi/order";
		
		//支付结果查询接口 url
		$queryResultUrl=$iapppayCpUrl ."/payapi/queryresult";
		
		//契约查询接口url
		$querysubsUrl=$iapppayCpUrl."/payapi/subsquery";
		
		//契约鉴权接口Url
		$ContractAuthenticationUrl=$iapppayCpUrl."/payapi/subsauth";
		
		//取消契约接口Url
		$subcancel=$iapppayCpUrl."/payapi/subcancel";
		
		//H5跳转版支付接口Url
		$h5url="https://web.iapppay.com/h5/exbegpay";
		
		//cp私钥
		$cpvkey="MIICXAIBAAKBgQCvIdY2O27qYTVzh0hWgeZdvhH3UsFtL4D1ztF/HuxGxTt2h6G3paeXdfOxhtDKSaTHv2rCobTeIKZ0Hba9r2O3DidAfjdJM3YCIHKse9nfjuKQhARzQD4rcc0seahd0PPEtONdYJzdCaQn7a76pMz9R+lAmJBHWSxE6mXq9qLwNwIDAQABAoGAJ2snSvefpHOS01kMyCPe1RS1+IQQ82Fw1mLhtoogRmGYW7p1hN9tVGMBIeElV5Kx2x/TiNFa43BX5uTVP+adO7QHwO0qXTzY3NFvuvlNwUIBAoN3J678RwIr7T8P31WrTIu2Qd2B+StKOWmBZzVYA7lajm6BMPiDT15GHBFiAcECQQDkHB5bsWBXqsjM/vvlWvcsvYiEys8ncGeDzpNueMXtnWHSedahTL0HYRQD1w4mIIqXBlBWi8KuTaNWJ2j4yMVhAkEAxIuB2QVJWOt+Ast6+nsvICPOs1+O/bazc8QfVcHaMO/Zw6m6m37d87qxIob8dzCacb65DSTtvQAgvp2+p8EElwJAdH47rr4WIo9QfwhVIhtjkdC1cIOWaWDJLJIJzugUxLWUKIYaa0OiYatdKlzgl+4UeO47hwdXA+cYXD5CKJN34QJAOZh8K6Gd9d3EpoMEfcR9cdisaOoW2AijG8icOiA9lVukH+9sDMcnuZW69NhDwZXPnId8aPqqZFlWPyWwyi4I7wJBANH9se7NKfxFOhKa86pwbcjXS4SYJRoALZ0hze6VVEvmB1NGl1CXrJEcDFlxLYbtPmOHwPd/bEoA332Hngf87AA=";

		//平台公钥
		$platpkey="MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQC/qaY/WWjOaHBwTjVeib/ntOJrK6XAe3NPC2exRKzlffqRz8DJwyWdUNsqE35Ua2R8wc7abdIwGHsLB62FIhXUOvRS8YtN8K0awzHavvGH14y5ylYCO6Gw8UtjFlf3erUZGYm6xTq7/EtkE6JA1Njzg4XnEYoPI9jNRbUf83EF3QIDAQAB";

		//pc测试
		//$cpvkey = "MIICXgIBAAKBgQCQS63lxskov7LSj0VhQlXZATD/roqFs61kpjZWmqazLZK6nctZOQWeY+mLfXd863bpWQiYTeIedXx/73+LA5nMom/1kDCuBHrPgkv9xBcyLvMyniKGrFjicWpgkJGElSOt/9sW6E5K8Ix/RNx/+tEJ//twGAbN+RDxtMzB/jXSOQIDAQABAoGAWQgpCy7FbbwiZSL7hxA8EgJXLluOowKDSSf5PSR9GO9Uqoq0LqAtZFZ3gpbuxAgxPbeuwvLDQ1HweKvYwXh+Sz3EVqEAfqs5MePxyXjZV5CoPC6qd8z+v9ghrsTXOTGhVI1lSDzMlfdBzQyFEQS1RO0MYLPJxpbxob95rn1P2NECQQD11q51Vja6a+Kms0AmjQhk2XkjKjPKvIhwlc/wblaY48asLnJDAGnROoz3f76/h7Ysqw7SI6ySm/kvyR5NsIKVAkEAlkKHtxqcsZMcwylijhF9ElSLm5UVv3CTKkTF90GPo7DJn9KeKZTxytylluTtfE1jSIioBn5Y9h8E25Kw8kOsFQJBAIIw3hUvwi35wn9LwHZqC/70mM2BpQoBzDPrcsK+fEfdoLJAqqxHBMMUQeE0mrztzxFaya+JyvOdfFrNJKo6Bv0CQQCGg/zXMN3CIr8HAA+Yu5NKS6HHoImdnnxAxNys42Zh1ixKBdVNyGdFgo5DLaJlOyp8jspNNeMSQuaoYwRBfc+ZAkEAnm/0f+/XUXg0Irk/ev4HjsAai2Jf+cZsxuoW2cjyZi2k9aIMFAfudsywSTi4N/jOSsQlKmnsyMscztPe6Ntj5g==";
		//$platpkey = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQC2ZUSC/GQj99g2NHAZYvridH7I4J3YHUFF+BumsAYo9+Ka8nexsl9IoaM7GrIyM+EphiWO0h+DXiw5+2yRrpMPUD7FRClMWQ0L6pujdJraoj+5+9ToLE80K1HCC/a+MaI00wM4g+BO6kUkrQvxkbn3wKhgOwwylD9po9w9ln1v3wIDAQAB";

		$this->config = $config;
		
	    //下单接口
		$orderReq["appid"] = "300600634";        //web:300350443     //h5:3003502964
		$orderReq["waresid"] = $this->config['shopid'];
		$orderReq["cporderid"] = $this->config['code'];
		$orderReq["price"] = $this->config['money'];   //单位：元
		//$orderReq['price'] = 0.01;
		$orderReq["currency"] = "RMB";
		$orderReq["appuserid"] = $this->config['appuserid'];
		$orderReq["cpprivateinfo"] = "WAP";
		$orderReq["notifyurl"] = $this->config['notifyurl'];
		
		//组装请求报文
		$reqData = composeReq($orderReq, $cpvkey);
		
		//发送到爱贝服务后台请求下单
		$respData = request_by_curl($orderUrl, $reqData, "order test");

		//验签数据并且解析返回报文
		if( ! parseResp($respData, $platpkey, $respJson)) {
			echo "parse resp data failed!\n";
		}

		//下单成功之后获取 transid
		$transid = $respJson->transid;

		//下单接口
		$doCreateOrder["transid"] = "$transid";
		$doCreateOrder["redirecturl"] = $this->config['redirecturl'];
		$doCreateOrder["cpur"] = $this->config['cpur'];

		//组装请求报文
		$doReqData = composeReq($doCreateOrder, $cpvkey);
		$this->url = $h5url."?".$doReqData;//这里组装的最终数据 就可以用浏览器访问调出收银台。
	}

	//发送
	public function send_pay(){
		 header("Location:" . $this->url);
		 die;
	}

	//发送
	public function get_pay(){
		 return $this->url;
	}
}

?>
