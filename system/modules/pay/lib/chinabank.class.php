<?php 

class chinabank {
	//配置
	private $config;
	public function config($config=null){	
			$this->config=$config;	
	}

	//支付页面
	public function send_pay(){
		$config = $this->config;
			$v_mid = $config['id'];                                  	//商户号
			$key = $config['key'];										//密钥
			$return_url = $config['ReturnUrl'];				//显示支付结果页面,*替换成payReturnUrl.php所在路径
			$notify_url = $config['NotifyUrl'];				//支付完成后的回调处理页面,*替换成payNotifyUrl.php所在路径
			
			//
			$v_url = $return_url;
			$remark2 = '[url:='.$notify_url.']'; 
			$v_oid = $config['code'];
		 
			$v_amount = $config['money'];           
			$v_moneytype = "CNY";                                            //币种

			$text = $v_amount.$v_moneytype.$v_oid.$v_mid.$v_url.$key;        //md5加密拼凑串,注意顺序不能变
			$v_md5info = strtoupper(md5($text));                             //md5函数加密并转化成大写字母

			 $remark1 = "";
			 $pmode_id=$config['pay_bank'];
			 if($pmode_id=="5"||$pmode_id=="DEFAULT"){
				$pmode_id="";
			 }
	 
			$html='<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head><meta http-equiv="Content-Type" content="text/html;charset=UTF-8"><title>payment</title></head><body>
	<form method="post" name="frm1" id="frm1" action="https://tmapi.jdpay.com/PayGate">
	<input type="hidden" name="v_mid"         value="'.$v_mid.'">
	<input type="hidden" name="v_oid"         value="'.$v_oid.'">
	<input type="hidden" name="v_amount"      value="'.$v_amount.'">
	<input type="hidden" name="v_moneytype"   value="'.$v_moneytype.'">
	<input type="hidden" name="v_url"         value="'.$v_url.'">
	<input type="hidden" name="v_md5info"     value="'.$v_md5info.'">
	<input type="hidden" name="remark1"       value="'.$remark1.'">
	<input type="hidden" name="remark2"       value="'.$remark2.'">
	<input type="hidden" name="pmode_id"       value="'.$pmode_id.'">
	<script language="javascript">document.getElementById("frm1").submit();</script></form></body></html>';
			//
			echo $html;
		exit();
	}

}

?>
