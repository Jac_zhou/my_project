<?php
/**
 *功能：配置文件
 *版本：1.0
 *修改日期：2014-06-26
 '说明：
 '以下代码只是为了方便商户测试而提供的样例代码，商户可以根据自己的需要，按照技术文档编写,并非一定要使用该代码。
 '该代码仅供学习和研究爱贝云计费接口使用，只是提供一个参考。
 */

//爱贝商户后台接入url
$iapppayCpUrl="http://ipay.iapppay.com:9999";

//登录令牌认证接口 url
$tokenCheckUrl=$iapppayCpUrl . "/payapi/tokencheck";

//下单接口 url
$orderUrl=$iapppayCpUrl . "/payapi/order";

//支付结果查询接口 url
$queryResultUrl=$iapppayCpUrl . "/payapi/queryresult";

//cp私钥
$cpvkey="MIICXAIBAAKBgQCvIdY2O27qYTVzh0hWgeZdvhH3UsFtL4D1ztF/HuxGxTt2h6G3paeXdfOxhtDKSaTHv2rCobTeIKZ0Hba9r2O3DidAfjdJM3YCIHKse9nfjuKQhARzQD4rcc0seahd0PPEtONdYJzdCaQn7a76pMz9R+lAmJBHWSxE6mXq9qLwNwIDAQABAoGAJ2snSvefpHOS01kMyCPe1RS1+IQQ82Fw1mLhtoogRmGYW7p1hN9tVGMBIeElV5Kx2x/TiNFa43BX5uTVP+adO7QHwO0qXTzY3NFvuvlNwUIBAoN3J678RwIr7T8P31WrTIu2Qd2B+StKOWmBZzVYA7lajm6BMPiDT15GHBFiAcECQQDkHB5bsWBXqsjM/vvlWvcsvYiEys8ncGeDzpNueMXtnWHSedahTL0HYRQD1w4mIIqXBlBWi8KuTaNWJ2j4yMVhAkEAxIuB2QVJWOt+Ast6+nsvICPOs1+O/bazc8QfVcHaMO/Zw6m6m37d87qxIob8dzCacb65DSTtvQAgvp2+p8EElwJAdH47rr4WIo9QfwhVIhtjkdC1cIOWaWDJLJIJzugUxLWUKIYaa0OiYatdKlzgl+4UeO47hwdXA+cYXD5CKJN34QJAOZh8K6Gd9d3EpoMEfcR9cdisaOoW2AijG8icOiA9lVukH+9sDMcnuZW69NhDwZXPnId8aPqqZFlWPyWwyi4I7wJBANH9se7NKfxFOhKa86pwbcjXS4SYJRoALZ0hze6VVEvmB1NGl1CXrJEcDFlxLYbtPmOHwPd/bEoA332Hngf87AA=";

//平台公钥
$platpkey="MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQC/qaY/WWjOaHBwTjVeib/ntOJrK6XAe3NPC2exRKzlffqRz8DJwyWdUNsqE35Ua2R8wc7abdIwGHsLB62FIhXUOvRS8YtN8K0awzHavvGH14y5ylYCO6Gw8UtjFlf3erUZGYm6xTq7/EtkE6JA1Njzg4XnEYoPI9jNRbUf83EF3QIDAQAB";

?>
