<?php
require_once __DIR__.'/iapppay/lib/jhpay_core.function.php';
require_once __DIR__.'/iapppay/lib/jhpay_notify.class.php';
require_once __DIR__.'/iapppay/lib/jhpay.class.php';
class iapppay {
 
	private $config;
	/**
	*	支付入口
	**/
	
	public function config($config=null){
			$this->config = $config;
	}
	
	public function send_pay(){
		$config = $this->config;
		//以下是聚合支付
		$data = array(
			'orderid' => $config['code'],
			'money' => $config['money'],
			'productName' => '易中夺宝商品',
			'u_id' => rand(1,1000),
			'username' => '   ',
			'email' => '   ',
			'mobile' => '  ',
			'from' => 'WAP',
			'redirecturl' => $config['ShoppingUrl'],
		);

		$postData = $data;
		$postData['appid'] = '26100535';
		$postData['key'] = '82y5d877sod1t87a0j4gspevz78xkg0j';

		/*array_walk_recursive($postData,function( &$value){
            $value = htmlspecialchars(stripslashes(trim($value)));
        });*/
		//以下字段参考接口文档
		$jhpay_version = '1.0';
		$jhpay_merid = $postData['appid'];//商户ID
		$jhpay_mername= isset($postData['company_name'])?$postData['company_name']:'易中夺宝';//商户名称
		$jhpay_policyid = '000001';
		$jhpay_merorderid = $postData['orderid'];//订单号
		$jhpay_paymoney = $postData['money'];//金额
		$jhpay_productname = $postData['productName'];//商品名称，尽量不要用云购，1元云购等
		$jhpay_productdesc = '易中夺宝';//商品描述
		$jhpay_userid = (int) $postData['u_id'];    //用户id
		$jhpay_username = $postData['username'];    //用户名字
		$jhpay_email = $postData['email'];          //邮箱帐号
		$jhpay_phone = $postData['mobile'];         //用户手机号
		$jhpay_extra = $postData['from'];//添加自定义内容  可以用来标示 来源 android/web/ios
		$jhpay_custom = '';
		$jhpay_redirect = '2';  // 0
		$jhpay_redirecturl = $postData['redirecturl'];      //自己前台回调地址（不影响后台的数据同步）
		$jhpay_md5 = $postData['key'];    //尽量不要明文赋值（可以通过后台配置到数据库,获取数据库便可）

		$jhpay_config_input_charset = strtolower('utf-8');

		//构造要请求的参数数组，无需改动
		$parameter = array(
			"version" => $jhpay_version,
			"merid" =>$jhpay_merid,
			"mername"	=> $jhpay_mername,
			"policyid"	=> $jhpay_policyid,
			"merorderid"	=> $jhpay_merorderid,
			"paymoney"	=> $jhpay_paymoney,
			"productname"	=> $jhpay_productname,
			"productdesc"	=> $jhpay_productdesc,
			"userid"	=> $jhpay_userid,
			"username"	=> $jhpay_username,
			"email"	=> $jhpay_email,
			"phone"	=> $jhpay_phone,
			"extra"	=> $jhpay_extra,
			"custom" => $jhpay_custom,
			"redirect"	=> $jhpay_redirect,
			"redirecturl"	=> $jhpay_redirecturl
			// "md5"	=> $jhpay_md5

		);

		//签名方式 不需修改
		$jhpay_config_sign_type = strtoupper('MD5');
		//字符编码格式 目前支持 gbk 或 utf-8
		$jhpay_config_input_charset = strtolower('utf-8');

		$jhpay_config_transport   = 'http';

		$jhpay_config=array(
			"partner"      =>$jhpay_merid,
			"key"          =>$jhpay_md5,
			"sign_type"    =>$jhpay_config_sign_type,
			"input_charset"=>$jhpay_config_input_charset,
			"transport"    =>$jhpay_config_transport
		);

		$jhpaySubmit = new JhpaySubmit($jhpay_config);
		$url = $jhpaySubmit->buildRequestForm($parameter,'POST','submit');
		//$url = $jhpaySubmit->buildRequestHttp($parameter);
		echo $url;
		die;
	}

 }

?>