<?php

defined('G_IN_SYSTEM')or exit('No permission resources.');

//ini_set("display_errors","OFF");
class chinabank_url extends SystemAction {

    public function __construct() {
        $this->db = System::load_sys_class('model');
    }

    public function qiantai() {
        $this->chuli();
        sleep(2);
        $out_trade_no = $_REQUEST['v_oid']; //商户订单号
        $dingdaninfo = $this->db->GetOne("select * from `@#_member_addmoney_record` where `code` = '$out_trade_no'");
        if (!$dingdaninfo || $dingdaninfo['status'] == '未付款') {
            _message("支付失败");
        } else {
            if (empty($dingdaninfo['scookies'])) {
                _message("充值成功!", WEB_PATH . "/member/home/userbalance");
            } else {
                if ($dingdaninfo['scookies'] == '1') {
                    _message("支付成功!", WEB_PATH . "/member/cart/paysuccess");
                } else {
                    _message("商品还未购买,请重新购买商品!", WEB_PATH . "/member/cart/cartlist");
                }
            }
        }
    }

    public function houtai() {

        $pay_type = $this->db->GetOne("SELECT * from `@#_pay` where `pay_class` = 'chinabank' and `pay_start` = '1'");
        $pay_type_key = unserialize($pay_type['pay_key']);
        $key = $pay_type_key['key']['val'];  //支付KEY
        $v_mid = $pay_type_key['id']['val'];  //支付商号ID

        $v_oid = trim($_POST['v_oid']);
        $v_pmode = trim($_POST['v_pmode']);
        $v_pstatus = trim($_POST['v_pstatus']);
        $v_pstring = trim($_POST['v_pstring']);
        $v_amount = trim($_POST['v_amount']);
        $v_moneytype = trim($_POST['v_moneytype']);
        $remark1 = trim($_POST['remark1']);
        $remark2 = trim($_POST['remark2']);
        $v_md5str = trim($_POST['v_md5str']);

        $md5string = strtoupper(md5($v_oid . $v_pstatus . $v_amount . $v_moneytype . $key));

        if ($v_md5str == $md5string) {
            if ($v_pstatus == "20") {
                $total_fee = $v_amount;
                $out_trade_no = $v_oid;

                $total_fee_t = $total_fee;
                $this->db->Autocommit_start();
                $dingdaninfo = $this->db->GetOne("select * from `@#_member_addmoney_record` where `code` = '$out_trade_no' and `money` = '$total_fee_t' and `status` = '未付款' for update");
                if (!$dingdaninfo) {
                    echo "fail";
                    exit;
                }

                //用户充值
                $UserBankCon = System::load_contorller('UserBank', 'Zapi');
                $result = $UserBankCon->user_recharge($total_fee_t, $dingdaninfo, $pay_type['name']);

                if ($result) {
                    $this->db->Autocommit_commit();
                } else {
                    $this->db->Autocommit_rollback();
                    echo "fail";
                    exit;
                }

                if (empty($dingdaninfo['scookies'])) {
                    echo "success";
                    exit;
                }

                $uid = $dingdaninfo['uid'];

                $scookies = unserialize($dingdaninfo['scookies']);
                $pay = System::load_app_class('pay', 'pay');
                $pay->scookie = $scookies;
                $ok = $pay->init($uid, $pay_type['pay_id'], 'go_record'); //云购商品	
                if ($ok != 'ok') {
                    _setcookie('Cartlist', NULL);
                    echo "fail";
                    exit; //商品购买失败			
                }
                $check = $pay->go_pay(1);
                if ($check) {
                    $this->db->Query("UPDATE `@#_member_addmoney_record` SET `scookies` = '1' where `code` = '$out_trade_no' and `status` = '已付款'");
                    _setcookie('Cartlist', NULL);
                    echo "success";
                    exit;
                } else {
                    echo "fail";
                    exit;
                }
            } else {
                echo "支付失败";
                exit();
            }
        } else {
            echo "签名失败";
            exit();
        }
    }

//function end

    public function chuli() {
        $pay_type = $this->db->GetOne("SELECT * from `@#_pay` where `pay_class` = 'chinabank' and `pay_start` = '1'");
        $pay_type_key = unserialize($pay_type['pay_key']);
        $key = $pay_type_key['key']['val'];  //支付KEY
        $v_mid = $pay_type_key['id']['val'];  //支付商号ID

        $v_oid = trim($_POST['v_oid']);
        $v_pmode = trim($_POST['v_pmode']);
        $v_pstatus = trim($_POST['v_pstatus']);
        $v_pstring = trim($_POST['v_pstring']);
        $v_amount = trim($_POST['v_amount']);
        $v_moneytype = trim($_POST['v_moneytype']);
        $remark1 = trim($_POST['remark1']);
        $remark2 = trim($_POST['remark2']);
        $v_md5str = trim($_POST['v_md5str']);

        $md5string = strtoupper(md5($v_oid . $v_pstatus . $v_amount . $v_moneytype . $key));

        if ($v_md5str == $md5string) {
            if ($v_pstatus == "20") {
                $total_fee = $v_amount;
                $out_trade_no = $v_oid;

                $total_fee_t = $total_fee;


                $dingdaninfo = $this->db->GetOne("select * from `@#_member_addmoney_record` where `code` = '$out_trade_no' and `status` = '未付款'");
                if (!$dingdaninfo) {
                    return false;
                    exit;
                } //没有该订单,失败
                $c_money = intval($dingdaninfo['money']);
                $uid = $dingdaninfo['uid'];
                $time = time();
                $this->db->Autocommit_start();
                $up_q1 = $this->db->Query("UPDATE `@#_member_addmoney_record` SET `pay_type` = '京东网关', `status` = '已付款' where `id` = '$dingdaninfo[id]' and `code` = '$dingdaninfo[code]'");
                $up_q2 = $this->db->Query("UPDATE `@#_member` SET `money` = `money` + $c_money where (`uid` = '$uid')");
                $up_q3 = $this->db->Query("INSERT INTO `@#_member_account` (`uid`, `type`, `pay`, `content`, `money`, `time`) VALUES ('$uid', '1', '账户', '充值', '$c_money', '$time')");
                if ($up_q1 && $up_q2 && $up_q3) {
                    $this->db->Autocommit_commit();
                } else {
                    $this->db->Autocommit_rollback();
                    return false;
                    exit;
                }
                if (empty($dingdaninfo['scookies'])) {
                    return true;
                    exit; //充值完成			
                }
                $scookies = unserialize($dingdaninfo['scookies']);
                $pay = System::load_app_class('pay', 'pay');
                $pay->scookie = $scookies;

                $ok = $pay->init($uid, $pay_type['pay_id'], 'go_record'); //V购商品	
                if ($ok != 'ok') {
                    _setcookie('Cartlist', NULL);
                    return false;
                    exit; //商品购买失败			
                }
                $check = $pay->go_pay(1);
                if ($check) {
                    $this->db->Query("UPDATE `@#_member_addmoney_record` SET `scookies` = '1' where `code` = '$out_trade_no' and `status` = '已付款'");
                    _setcookie('Cartlist', NULL);
                    return true;
                    exit;
                } else {
                    return false;
                    exit;
                }
            }
        } else {
            echo "交易信息被篡改";
        }
    }

}

//
?>