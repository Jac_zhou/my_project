<?php 
/*
* h5 支付宝支付
* 2015/12/27
*/
defined('G_IN_SYSTEM')or exit('No permission resources.');
ini_set("display_errors","OFF");
class wappay_url extends SystemAction {
	public function __construct(){			
		$this->db=System::load_sys_class('model');		
	} 	
	
	public function qiantai(){	
		sleep(3);
		$out_trade_no = $_GET['out_trade_no'];	//商户订单号
		
		$dingdaninfo = $this->db->GetOne("select * from `@#_member_addmoney_record` where `code` = '$out_trade_no'");
		
		if(!$dingdaninfo || $dingdaninfo['status'] == '未付款'){
		    if(empty($dingdaninfo['scookies'])){  
		        header("location:".G_WEB_PATH."/yungou/#/tab/rechargeResult?ledou=-1"); //充值失败
		    }else{
		        header("location:".G_WEB_PATH."/yungou/#/tab/payResult?code=0");  //支付失败
		    }		
		}else{
			if(empty($dingdaninfo['scookies'])){
			    header("location:".G_WEB_PATH."/yungou/#/tab/rechargeResult?ledou=".$dingdaninfo['money']);
			}else{
				if($dingdaninfo['scookies'] == '1'){
				    header("location:".G_WEB_PATH."/yungou/#/tab/payResult?code=".$dingdaninfo['code']);
				}else{
					//商品剩余人次不足,请重新购买!
					header("location:".G_WEB_PATH."/yungou/#/tab/payResult?code=0");
				}	
			}
		}
	}
	
	public function houtai(){
	    require_once "lib/wappay/lib/alipay_notify.class.php";
	    
		$pay_type =$this->db->GetOne("SELECT * from `@#_pay` where `pay_class` = 'alipay' and `pay_start` = '1'");
		$pay_type_key = unserialize($pay_type['pay_key']);
		$partner =  $pay_type_key['id']['val'];		//支付商号ID
		
		
		$alipay_config['partner'] = $partner;
		$alipay_config['seller_id']	= $alipay_config['partner'];
		$alipay_config['private_key_path']	= G_SYSTEM."modules/pay/lib/wappay/key/rsa_private_key.pem";
		$alipay_config['ali_public_key_path']= G_SYSTEM."modules/pay/lib/wappay/key/alipay_public_key.pem";
		$alipay_config['sign_type']    = strtoupper('RSA');
		$alipay_config['input_charset']= strtolower('utf-8');
		$alipay_config['cacert'] = G_SYSTEM."modules/pay/lib/wappay/cacert.pem";	//ca证书路径地址;
		$alipay_config['transport']    = 'http';
		
		$alipayNotify = new AlipayNotify($alipay_config);
        $verify_result = $alipayNotify->verifyNotify();
        
		if(!$verify_result) {echo "fail";exit;} //验证失败
		
		$out_trade_no = $_POST['out_trade_no'];	//商户订单号
		$trade_no = $_POST['trade_no'];			//支付宝交易号
		$trade_status = $_POST['trade_status'];	//交易状态
		
		//开始处理
		if($trade_status == 'TRADE_FINISHED' || $trade_status == 'TRADE_SUCCESS' ) {
			
			$this->db->Autocommit_start();
			$dingdaninfo = $this->db->GetOne("select * from `@#_member_addmoney_record` where `code` = '$out_trade_no' and `status` = '未付款' for update");
			if(!$dingdaninfo){	echo "fail";exit;}	//没有该订单,失败
			$c_money = intval($dingdaninfo['money']);			
			$uid = $dingdaninfo['uid'];
			$time = time();			
			$up_q1 = $this->db->Query("UPDATE `@#_member_addmoney_record` SET `pay_type` = '支付宝', `status` = '已付款' where `id` = '$dingdaninfo[id]' and `code` = '$dingdaninfo[code]'");
			$up_q2 = $this->db->Query("UPDATE `@#_member` SET `money` = `money` + $c_money where (`uid` = '$uid')");				
			$up_q3 = $this->db->Query("INSERT INTO `@#_member_account` (`uid`, `type`, `pay`, `content`, `money`, `time`) VALUES ('$uid', '1', '账户', '充值', '$c_money', '$time')");
			/************ 好友获得积分 start  2015/12/15 **************/
			$up_q4 = true;
			$up_q5 = true;
			$up_q6 = true;
			$record_id = $this->db->insert_id();		//充值记录的ID
			$addtime = time();		//添加时间
			$score = array('1'=>4, '2'=>3, '3'=>2);		//积分表
			$friends = $this->db->GetList("SELECT yaoqing,lev FROM `@#_member_friendship` WHERE `uid`='$uid' ");
			if(count($friends)){
				foreach($friends as $k => $v){
					$yaoqing = $v['yaoqing'];
					$lev = $v['lev'];
					$jifen = $c_money * $score[$lev] ; 
					$this->db->Query("UPDATE `@#_member` SET `score` = `score` + $jifen where (`uid` = '$yaoqing')");
					$upp[$k] = $this->db->Query("INSERT INTO `@#_member_jifen`(`uid`,`yaoqing`,`lev`,`jifen`,`record_id`,`addtime`) VALUES('$uid','$yaoqing','$lev','$jifen','$record_id','$addtime')");
				}
				$up_q4 = isset($upp[0]) ? $upp[0] : true;
				$up_q5 = isset($upp[1]) ? $upp[1] : true;
				$up_q6 = isset($upp[2]) ? $upp[2] : true;
			}
			/************ 好友获得积分 end  2015/12/15 ********以下的判断中添加了up_q4  up_q5  up_q6的判断******/
			/*活动的start*/
			System::load_app_fun('member','member');
			$chong_userinfo = getchongzhi_user($dingdaninfo['uid']);
			active_cash($dingdaninfo['uid'],$c_money,$chong_userinfo['mobile']);
			/*活动的end*/
			
			
			if($up_q1 && $up_q2 && $up_q3 && $up_q4 && $up_q5 && $up_q6){
				$this->db->Autocommit_commit();			
			}else{
				$this->db->Autocommit_rollback();
				echo "fail";exit;
			}
			
			if(empty($dingdaninfo['scookies'])){					
					echo "success";exit;	//充值完成			
			}
            
			//////////////////////////////////////////////////////
			///如果是购买商品，需要继续执行余额支付   20151227 kpf///
			/////////////////////////////////////////////////////
			$scookies = unserialize($dingdaninfo['scookies']);
			
			$wpay = System::load_app_class('wpay','pay');			
			$wpay->scookie = $scookies;	

			$ok = $wpay->init($uid,'支付宝','go_record',$scookies);	//众乐商品
			
			if($ok != 'ok'){
				echo "fail";exit;	//商品购买失败			
			}			
			$check = $wpay->go_pay(1,$dingdaninfo['code']);
			if($check){
				$this->db->Query("UPDATE `@#_member_addmoney_record` SET `scookies` = '1' where `code` = '$out_trade_no' and `status` = '已付款'");
				_setcookie('Cartlist',NULL);
				echo "success";exit;			
			}else{
				echo "fail";exit;
			}
		
		}//开始处理订单结束
				

	}
}

?>