<?php

defined('G_IN_SYSTEM')or exit('No permission resources.');
ini_set("display_errors", "OFF");

class alipay_url extends SystemAction {

    public function __construct() {
        $this->db = System::load_sys_class('model');
    }

    public function qiantai() {
        sleep(2);
        $out_trade_no = $_GET['out_trade_no']; //商户订单号
        $dingdaninfo = $this->db->GetOne("select * from `@#_member_addmoney_record` where `code` = '$out_trade_no'");
        if (!$dingdaninfo || $dingdaninfo['status'] == '未付款') {
            _message("支付失败");
        } else {
            if (empty($dingdaninfo['scookies'])) {
                //_message("充值成功!",WEB_PATH."/member/home/userbalance");
                header("location:" . WEB_PATH . "/member/home/paysuccess"); //充值成功
            } else {
                if ($dingdaninfo['scookies'] == '1') {
                    //_message("支付成功!",WEB_PATH."/member/cart/paysuccess");
                    header("location:" . WEB_PATH . "/member/cart/paysuccess"); //支付并购物成功
                } else {
                    _message("商品还未购买,请重新购买商品!", WEB_PATH . "/member/cart/cartlist");
                }
            }
        }
    }

    public function houtai() {
        include G_SYSTEM . "modules/pay/lib/alipay/alipay_notify.class.php";
        $pay_type = $this->db->GetOne("SELECT * from `@#_pay` where `pay_class` = 'alipay' and `pay_start` = '1'");
        $pay_type_key = unserialize($pay_type['pay_key']);
        $key = $pay_type_key['key']['val'];  //支付KEY
        $partner = $pay_type_key['id']['val'];  //支付商号ID

        $alipay_config_sign_type = strtoupper('MD5');  //签名方式 不需修改
        $alipay_config_input_charset = strtolower('utf-8'); //字符编码格式		
        $alipay_config_cacert = G_SYSTEM . "modules/pay/lib/alipay/cacert.pem"; //ca证书路径地址
        $alipay_config_transport = 'http';

        $alipay_config = array(
            "partner" => $partner,
            "key" => $key,
            "sign_type" => $alipay_config_sign_type,
            "input_charset" => $alipay_config_input_charset,
            "cacert" => $alipay_config_cacert,
            "transport" => $alipay_config_transport
        );

        $alipayNotify = new AlipayNotify($alipay_config);
        $verify_result = $alipayNotify->verifyNotify();

        if (!$verify_result) {
            echo "fail";
            exit;
        } //验证失败

        $out_trade_no = $_POST['out_trade_no']; //商户订单号
        $trade_no = $_POST['trade_no'];   //支付宝交易号
        $trade_status = $_POST['trade_status']; //交易状态
        //开始处理及时到账和担保交易订单
        if ($trade_status == 'TRADE_FINISHED' || $trade_status == 'TRADE_SUCCESS' || $trade_status == 'WAIT_SELLER_SEND_GOODS') {

            $this->db->Autocommit_start();
            $dingdaninfo = $this->db->GetOne("select * from `@#_member_addmoney_record` where `code` = '$out_trade_no' and `status` = '未付款' for update");
            if (!$dingdaninfo) {
                echo "fail";
                exit;
            } //没有该订单,失败
            $c_money = intval($dingdaninfo['money']);
            $uid = $dingdaninfo['uid'];
            $time = time();


            $mysql_model = System::load_sys_class('model');

            $percentage1 = $mysql_model->GetOne("select percentage  from `@#_recharge_percentage`  where  `charge`=$c_money");

            $percentage2 = $percentage1['percentage'];

            $total_fee_t1 = $percentage2 / 100 * $c_money + $c_money;

            $up_q1 = $this->db->Query("UPDATE `@#_member_addmoney_record` SET `pay_type` = '微信支付', `status` = '已付款' where `id` = '$dingdaninfo[id]' and `code` = '$dingdaninfo[code]'");
            $up_q2 = $this->db->Query("UPDATE `@#_member` SET `money` = `money` + $total_fee_t1 where (`uid` = '$dingdaninfo[uid]')");
            $up_q3 = $this->db->Query("INSERT INTO `@#_member_account` (`uid`, `type`, `pay`, `content`, `money`, `time`) VALUES ('$dingdaninfo[uid]', '1', '账户', '充值', '$c_money', '$time')");



            /*             * ********** 好友获得积分 start  2015/12/15 ************* */
            $up_q4 = true;
            $up_q5 = true;
            $up_q6 = true;
            $record_id = $this->db->insert_id();  //充值记录的ID
            $addtime = time();  //添加时间
            $score = array('1' => 4, '2' => 3, '3' => 2);  //积分表
            $friends = $this->db->GetList("SELECT yaoqing,lev FROM `@#_member_friendship` WHERE `uid`='$uid' ");
            if (count($friends)) {
                foreach ($friends as $k => $v) {
                    $yaoqing = $v['yaoqing'];
                    $lev = $v['lev'];
                    $jifen = $c_money * $score[$lev];
                    $this->db->Query("UPDATE `@#_member` SET `score` = `score` + $jifen where (`uid` = '$yaoqing')");
                    $upp[$k] = $this->db->Query("INSERT INTO `@#_member_jifen`(`uid`,`yaoqing`,`lev`,`jifen`,`record_id`,`addtime`) VALUES('$uid','$yaoqing','$lev','$jifen','$record_id','$addtime')");
                }
                $up_q4 = isset($upp[0]) ? $upp[0] : true;
                $up_q5 = isset($upp[1]) ? $upp[1] : true;
                $up_q6 = isset($upp[2]) ? $upp[2] : true;
            }
            /*             * ********** 好友获得积分 end  2015/12/15 ********以下的判断中添加了up_q4  up_q5  up_q6的判断***** */

            if ($up_q1 && $up_q2 && $up_q3 && $up_q4 && $up_q5 && $up_q6) {
                $this->db->Autocommit_commit();
            } else {
                $this->db->Autocommit_rollback();
                echo "fail";
                exit;
            }
            if (empty($dingdaninfo['scookies'])) {
                echo "success";
                exit; //充值完成			
            }
            $scookies = unserialize($dingdaninfo['scookies']);
            $pay = System::load_app_class('pay', 'pay');
            $pay->scookie = $scookies;

            $ok = $pay->init($uid, $pay_type['pay_id'], 'go_record'); //众乐商品	
            if ($ok != 'ok') {
                _setcookie('Cartlist', NULL);
                echo "fail";
                exit; //商品购买失败			
            }
            $check = $pay->go_pay(1);
            if ($check) {
                //$this->db->Query("UPDATE `@#_member_addmoney_record` SET `scookies` = '1' where `code` = '$out_trade_no' and `status` = '已付款'");
                _setcookie('Cartlist', NULL);
                echo "success";
                exit;
            } else {
                echo "fail";
                exit;
            }
        }//开始处理订单结束
    }

//function end
}

//
?>