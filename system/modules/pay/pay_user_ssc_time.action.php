<?php 

defined('G_IN_SYSTEM')or exit('No permission resources.');
System::load_app_class('base','member','no');
System::load_app_fun('global',G_ADMIN_DIR);
System::load_app_fun("pay","pay");
System::load_sys_fun("user");
System::load_app_fun('user','go');
System::load_app_class("tocode","pay",'no');
class pay_user_ssc_time extends base {
	public function __construct(){		
		parent::__construct();		
		$this->db=System::load_sys_class('model');
	} 	

	//支付列表
	public function init()
    {
        $model = System::load_sys_class('model');//实例化Model类
        $shopid = isset($_POST['gid']) ? $_POST['gid'] : false;
        $shop = $model->GetOne("select * from `@#_shoplist` where `id` = '$shopid'   LIMIT 1 for update");//去的单数据
        ignore_user_abort(true);//函数设置与客户机断开是否会终止脚本的执行。如果设置为 true，则忽略与用户的断开，如果设置为 false，会导致脚本停止运行
        set_time_limit(0);//设置允许脚本运行的时间，单位为秒
        if ($shop['canyurenshu'] >= $shop['zongrenshu'] && $shop['maxqishu'] >= $shop['qishu']) {
            //把时间转成12.00
            $time = sprintf('%.2f', microtime(TRUE));
            //先计算出本期产品的中奖号码   并且更新本期产品的中奖信息
            //开始进行开奖
            //开启事物
            $model->Autocommit_start();

            //计算出获奖云购码
            /**
             * 调用当前模块类  系统/当前模块  下的lib文件夹里面的类文件          后缀名 class.php  文件夹libs/lib
             * @param   $class_name 类名  
             * @param   $module     模块名（默认是当前所处模块）  
             * @param   $new        是否实例化该类
             * @return  返回实例化对象
             */
            $tocode = System::load_app_class("tocode", "pay");//调用pay模块的tocode函数包
            $tocode->shop = $shop;//本期的产品是
            $tocode->run_tocode($time, 100, $shop['canyurenshu'], $shop);
            while (!$tocode->go_code) {

                $tocode->run_tocode($time, 100, $shop['canyurenshu'], $shop);
            }

            $code = $tocode->go_code;                //中奖云购码

            $ssc_code = $tocode->ssc_code;                //时时彩号码
            $ssc_opentime = $tocode->ssc_opentime;            //时时彩开奖时间
            $ssc_phase = $tocode->ssc_phase;            //时时彩的开奖期数
            $content = addslashes($tocode->go_content);
            $counttime = $tocode->count_time;            //100条购物记录的总计时间

            //获取中奖用户的信息
            $sql = "select * from `@#_member_go_record` where `shopid` = '$shop[id]' and `shopqishu` = '$shop[qishu]' and `goucode` LIKE '%$code%'";

            //查询购买本期中奖码的玩家信息
            $user_go_info = $model->GetOne($sql);
            if (!$user_go_info) {
                $sql = "select * from `go_member_go_record` where shopid='$shop[id]'";
                $all_recorde = $model->GetList($sql);
                $codes = '';
                foreach ($all_recorde as $value) {
                    $codes .= implode(',', $value['goucode']) . ',';
                }
                var_dump($code);
                var_dump($codes);
                var_dump('该云购码，暂时每人没人获得');
                die;
            }
            $sql = "select uid,username,email,mobile,img from `@#_member` where uid='$user_go_info[uid]'";
            $userinfo = $model->GetOne($sql);

            if ($userinfo) {
                $uid = $userinfo['uid'];
                $q_user = serialize($userinfo);
                //读取静态缓存中的基本配置信息
                global $_cfg;
                $goods_end_time = intval($_cfg['goods_end_time']);
                $q_end_time = $time + $goods_end_time;            //揭晓时间是购买停止时间+开奖需要的时间
                //更新本期商品的信息
                $sql = "UPDATE
							`@#_shoplist` 
						SET 
							`canyurenshu`	= `zongrenshu`,
							`shenyurenshu` 	= '0',
							`q_uid` 		= '$uid',
				 			`q_user` 		= '$q_user',
				 			`q_user_code`	= '$code',
				 			`q_sscphase` 	= '$ssc_phase' , 
				 			`q_content` 	= '$content',
				 			`q_counttime` 	= '$counttime' ,
				 			`q_end_time` 	= '$q_end_time',
				  			`q_showtime` 	= 'Y',
				  			`q_ssccode` 	= '$ssc_code',
				  			`q_sscopen` 	= '$ssc_opentime',
                            `zdrange`       = '43739-52814'
				   		Where 
				   			`id` = '$shop[id]'";//		
                //更新的时候在原来的基础上添加`zdrange`       = '13902-14242'  
                $query1 = $model->Query($sql);

                if ($query1) {
                    $sql = "UPDATE
								`@#_member_go_record` 
							SET 
								`huode` = '$code' 
							where 
								`id` = '$user_go_info[id]' 
								and `code` = '$user_go_info[code]' 
								and `uid` = '$user_go_info[uid]' 
								and `shopid` = '$shop[id]' 
								and `shopqishu` = '$shop[qishu]'";

                    $query2 = $model->Query($sql);
                } else {
                    $query1 = false;
                }
                if ($query1 && $query2) {
                    //如果期数未满
                    if ($shop['maxqishu'] > $shop['qishu']) {
                        //创建下一期
                        do_create_next_qishu($shop);
                    }
                }
            }
            //然后计算出倒计时的时间并且休眠   直到倒计时为0（休眠结束）  更改本期产品数据，关闭揭晓动画

            if ($query1 && $query2) {
                //成功执行
                $model->Autocommit_commit();
                //读取更新后的数据
                $shop = $model->GetOne("select * from `@#_shoplist` where `id` = '$shopid'");
            } else {
                //回滚事务
                $model->Autocommit_rollback();
            }
            //var_dump($query1.'555'.$query2);die;
            $xsjx_time = $shop['xsjx_time'];
            //限时揭晓的商品
            if ($xsjx_time != '0') {
                $wait_time = 10;                                //等待时间

                $new_xsjx_time = $xsjx_time + $wait_time;
                $q_djstime = $new_xsjx_time - $time;            //倒计时
                $sql = "UPDATE `@#_shoplist` SET `xsjx_time` = '$new_xsjx_time',`q_showtime` = 'N',`q_djstime` = '$q_djstime',`q_end_time` = '$new_xsjx_time' where `id` = '$shop[id]'";
                $q3 = $model->Query($sql);
                $timeout = $q_djstime;
            } else {
                //倒计时>0
                if ($shop['q_end_time'] > 0) {
                    $timeout = $shop['q_end_time'] - time();
                } else {
                    $this->GetTimeOut($shop);
                    $post_arr = array("gid" => $shopid);
                    _g_triggerRequest(WEB_PATH . '/pay/pay_user_ssc_time/init', false, $post_arr);
                    die;
                }
            }
            $model->Query("UPDATE `@#_shoplist` SET `canyurenshu`=`zongrenshu`,`shenyurenshu` = '0' where `id` = '$shop[id]'");
            //查询中奖用户是不是机器人
            $sql = "SELECT auto_user from `@#_member` where uid='{$shop['q_uid']}'";
            $result_auto = $model->GetOne($sql);


            if (!empty($shop['zdrange']) && $result_auto['auto_user']!=1){
                $zdrange = explode('-', $shop['zdrange']);
                $zdrange_start = intval($zdrange[0]);
                $zdrange_end = intval($zdrange[1]);
                if ($zdrange_start > 0 && $zdrange_end > 0) {
                    $ZdCon = System::load_contorller('Zdcharge', 'Zapi');
                    $result = $ZdCon->robots_rand($shop['id'],$shop['qishu'],$zdrange_start,$zdrange_end);
                    if($result){
                    	log_append('log/zd/zd_result.log','指定成功sid:'.$shop['id'].';qishu:'.$shop['qishu']);                        
                    }else{
                    	log_append('log/zd/zd_result.log','指定失败sid:'.$shop['id']);                    
                    }
                }
            }else{
                if($result_auto['auto_user']==1){
                	log_append('log/zd/zd_result.log',"该产品({$shop['id']})已经是机器人中奖！");                   
                }
            }

            //关闭揭晓动画,让用户可以看到  中奖码
            sleep($timeout);
            $model->Query("UPDATE `@#_shoplist` SET `q_showtime`='N',`q_djstime`='0' where `id` = '{$shop['id']}'");
            if ($query1 && $query2) {
                if($result_auto['auto_user']!=1){        //不是机器人中奖便发送短信
                	$sql = "select id,q_uid from go_shoplist WHERE id='{$shop['id']}' ";
                	$shop = $model->GetOne($sql);
                	$post_arr = array("uid" => $shop['q_uid'], "gid" => $shop['id'], "type" => 1);
                    _g_triggerRequest(WEB_PATH . '/Zapi/Dosend/win_lottery', false, $post_arr);
                }else{
                	log_append('log/zd/zd_send.log','机器人中奖，无需发送短信');  
                }
            }
        }

    }

	public function SocketTimeOut($timeout,$shopid){
		ignore_user_abort(true);
		set_time_limit(0);					
		sleep($timeout);
		$post_arr= array("gid"=>$shopid);
		_g_triggerRequest(WEB_PATH.'/pay/pay_user_ssc_time/init',false,$post_arr);		
	
	}	
	public function GetTimeOut($shop){
		//分解购买结束时的时间
		$sj_y=date("Y",microtime(true));
		$sj_m=date("m",microtime(true));
		$sj_d=date("d",microtime(true));
		$sj_h=date("H",microtime(true));
		$sj_i=date("i",microtime(true));
		$sj_s=date("s",microtime(true));
		$shi_1=substr(date("i",microtime(true)),0,1);
		$shi_2=substr(date("i",microtime(true)),1,2);
		$zjsi=(int)($sj_h.$sj_i.$sj_s);
		$wait_time=30;//等待时间
		$q_djstime_update1 = "UPDATE `@#_shoplist` SET `q_showtime` = 'Y' where `id` = '$shop[id]'";
		$q1 = $this->db->Query($q_djstime_update1);
		//中间时时彩没有开奖的区域
		if($zjsi>=15600&&$zjsi<=95100){
			//$q_djstime=1;			//这个是修改之前的  注释日期:2015.12.7 9:58
			//2015.12.7 9:58 处理10点之前,商品倒计时问题。以下三行为添加的代码
			$kaijiang_time = strtotime($sj_y.'-'.$sj_m.'-'.$sj_d.' 10:04:00');
			$jiange_time = $kaijiang_time - time();
			$q_djstime = $jiange_time;				
		}
		if($zjsi>95100&&$zjsi<220100){//时时彩时隔10分钟开奖一次的区域
			if($shi_2==1&&$sj_s==30){//判断分钟数是否为整数
				$q_djstime=599;//等待时间					
			}
			else{
			   if($shi_1==5){
				$shi_15=(int)($shi_2);
				$q_djstime=(10-$shi_15)*60;				   
			   }
			   else{
				$sj_i=((int)($shi_1)+1)."1";
				if($sj_s<30)
				$redytime=$sj_y.$sj_m.$sj_d.$sj_h.$sj_i.'30';
				if($sj_s>=30)
				$redytime=$sj_y.$sj_m.$sj_d.$sj_h.$sj_i.$sj_s;
				$redytime=strtotime($redytime);
				$nowtime=sprintf("%.0f",microtime(true));
				$q_djstime=$redytime-$nowtime;						
			   }						
			}
		}
							
		if(($zjsi>=220100&&$zjsi<=235959)||($zjsi>=0&&$zjsi<15600)){//时时彩时隔5分钟开奖一次的区域
			 if(($shi_2==1||$shi_2==6)&&$sj_s==30){
				$q_djstime=299;//等待时间
				}
			if(($shi_1==5&&$shi_2>6)||($shi_1==5&&$shi_2==6&&$sj_s>=30)){
			$sj_h=(int)($sj_h)+1;
			$redytime=$sj_y.$sj_m.$sj_d.$sj_h.'01'.'30';
			$redytime=strtotime($redytime)+$wait_time;
			$nowtime=sprintf("%.0f",microtime(true));
			$q_djstime=$redytime-$nowtime;	//等待时间
			}							
			if(($shi_2>=1&&$shi_2<6)||($shi_2==6&&$sj_s<30)){
			$sj_i=(int)($shi_1).'6';
			if($sj_s<30)
			$redytime=$sj_y.$sj_m.$sj_d.$sj_h.$sj_i.'30';
			if($sj_s>=30)
			$redytime=$sj_y.$sj_m.$sj_d.$sj_h.$sj_i.$sj_s;
			$redytime=strtotime($redytime);
			$nowtime=sprintf("%.0f",microtime(true));
			$q_djstime=$redytime-$nowtime;	//等待时间
				}
			if(($shi_2>6&&$shi_2<=9)||($shi_2==6&&$sj_s>=30)||($shi_2==0)){
			$sj_i=((int)($shi_1)+1)."1";
			if($sj_s<30)
			$redytime=$sj_y.$sj_m.$sj_d.$sj_h.$sj_i.'30';
			if($sj_s>=30)
			$redytime=$sj_y.$sj_m.$sj_d.$sj_h.$sj_i.$sj_s;
			$redytime=strtotime($redytime);
			$nowtime=sprintf("%.0f",microtime(true));
			$q_djstime=$redytime-$nowtime;	//等待时间
			}							
		}
		$this->db->Autocommit_start();
		$yjq_djstime=time()+$q_djstime;	
		$yjq_djstime=sprintf("%.3f",$yjq_djstime);
		$q_djstime_update3 = "UPDATE `@#_shoplist` SET `q_djstime` = '$q_djstime',`q_end_time` = '$yjq_djstime' where `id` = '$shop[id]'";
		$q2 = $this->db->Query($q_djstime_update3);	
		$this->db->Autocommit_commit();				
	}	
	
	
}
?>