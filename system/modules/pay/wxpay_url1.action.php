<?php

defined('G_IN_SYSTEM') or exit('No permission resources.');
class wxpay_url extends SystemAction {
	public function __construct() {
		$this -> db = System::load_sys_class('model');
	}

	//前台扫码页面定时请求  判断是否已付款
	public function check_is_pay() {
		$out_trade_no = $_POST["out_trade_no"] ? trim($_POST["out_trade_no"]) : '';
		$out_trade_no = safe_replace($out_trade_no);

		if (!$out_trade_no) {
			die('FAIL');
		}

		$info = $this -> db -> GetOne("select id,status from `@#_member_addmoney_record` where `code` = '$out_trade_no' ");
		if (!isset($info['id'])) {
			die('FAIL');
		}
		if ($info['status'] == '未付款') {
			die('NOPAY');
		}
		if ($info['status'] == '已付款') {
			die('PAY');
		}
	}

	public function houtai() {
		$this -> db = System::load_sys_class('model');
		$pay_type = $this -> db -> GetOne("SELECT * from `@#_pay` where `pay_class` = 'wxpay' and `pay_start` = '1'");
		include_once dirname(__FILE__) . "/lib/wxpay/WxPayPubHelper.php";
		//引入文件需求
		//$out_trade_no = "C14618063505625259";
		$xml = $GLOBALS['HTTP_RAW_POST_DATA'];
		$ob = simplexml_load_string($xml, 'SimpleXMLElement', LIBXML_NOCDATA);
		$json = json_encode($ob);
		$ar = json_decode($json, true);
		//$a = json_encode($_REQUEST);
		$sql1 = "insert into `@#_zyy_log` values(null,'" . $json . "  zyy')";

		$this -> db -> Query($sql1);

		$out_trade_no = $ar['out_trade_no'];

		//$out_trade_no = $_POST["out_trade_no"];

		//使用订单查询接口
		$orderQuery = new OrderQuery_pub();
		//设置必填参数
		//appid已填,商户无需重复填写
		//mch_id已填,商户无需重复填写
		//noncestr已填,商户无需重复填写
		//sign已填,商户无需重复填写
		$orderQuery -> setParameter("out_trade_no", "$out_trade_no");
		//商户订单号
		$time = time();

		//获取订单查询结果
		$orderQueryResult = $orderQuery -> getResult();
		//商户根据实际情况设置相应的处理流程,此处仅作举例
		if ($orderQueryResult["return_code"] == "FAIL") {
			echo "通信出错：" . $orderQueryResult['return_msg'] . "<br>";
			//file_put_contents("ccc.txt","通信出错：".$orderQueryResult['return_msg']."\n",FILE_APPEND);
		} elseif ($orderQueryResult["result_code"] == "FAIL") {
			echo "错误代码：" . $orderQueryResult['err_code'] . "<br>";
			echo "错误代码描述：" . $orderQueryResult['err_code_des'] . "<br>";
			//file_put_contents("ccc.txt","错误代码：".$orderQueryResult['err_code']."\n",FILE_APPEND);
			//file_put_contents("ccc.txt","错误代码描述：".$orderQueryResult['err_code_des']."\n",FILE_APPEND);
		} else {
			//file_put_contents("ccc.txt","交易状态：".$orderQueryResult['trade_state']."\n",FILE_APPEND);
			$total_fee_t = $orderQueryResult['total_fee'] / 100;
			//otal_fee_t = 200;
			$out_trade_no = $orderQueryResult['out_trade_no'];
			$this -> db -> Autocommit_start();
			$dingdaninfo = $this -> db -> GetOne("select * from `@#_member_addmoney_record` where `code` = '$out_trade_no' and `money` = '$total_fee_t' and `status` = '未付款' for update");
			if (!$dingdaninfo) {
				echo "fail";
				exit ;
			}
			$time = time();

			$mysql_model = System::load_sys_class('model');

			$percentage1 = $mysql_model -> GetOne("select percentage  from `@#_recharge_percentage`  where  `charge`=$total_fee_t");

			$percentage2 = $percentage1['percentage'];

			$total_fee_t1 = $percentage2 / 100 * $total_fee_t + $total_fee_t;

			$up_q1 = $this -> db -> Query("UPDATE `@#_member_addmoney_record` SET `pay_type` = '微信支付', `status` = '已付款' where `id` = '$dingdaninfo[id]' and `code` = '$dingdaninfo[code]'");
			$up_q2 = $this -> db -> Query("UPDATE `@#_member` SET `money` = `money` + $total_fee_t1 where (`uid` = '$dingdaninfo[uid]')");
			$up_q3 = $this -> db -> Query("INSERT INTO `@#_member_account` (`uid`, `type`, `pay`, `content`, `money`, `time`) VALUES ('$dingdaninfo[uid]', '1', '账户', '充值', '$total_fee_t', '$time')");
			/************ 好友获得积分 start  2015/12/17 **************/
			$up_q4 = true;
			$up_q5 = true;
			$up_q6 = true;
			$record_id = $dingdaninfo['id'];
			//充值记录的ID		对应member_addmoney_record表的id字段
			$addtime = time();
			//添加时间
			$score = array('1' => 4, '2' => 3, '3' => 2);
			//积分表
			$friends = $this -> db -> GetList("SELECT yaoqing,lev FROM `@#_member_friendship` WHERE `uid`='$dingdaninfo[uid]' ");
			if (count($friends)) {
				foreach ($friends as $k => $v) {
					$yaoqing = $v['yaoqing'];
					$lev = $v['lev'];
					$jifen = $total_fee_t * $score[$lev];
					$this -> db -> Query("UPDATE `@#_member` SET `score` = `score` + $jifen where (`uid` = '$yaoqing')");
					$upp[$k] = $this -> db -> Query("INSERT INTO `@#_member_jifen`(`uid`,`yaoqing`,`lev`,`jifen`,`record_id`,`addtime`) VALUES('$dingdaninfo[uid]','$yaoqing','$lev','$jifen','$record_id','$addtime')");
				}
				$up_q4 = isset($upp[0]) ? $upp[0] : true;
				$up_q5 = isset($upp[1]) ? $upp[1] : true;
				$up_q6 = isset($upp[2]) ? $upp[2] : true;
			}
			/************ 好友获得积分 end  2015/12/17 ********以下的判断中添加了up_q4  up_q5  up_q6的判断******/

			/*活动的start*/
			System::load_app_fun('member', 'member');
			$chong_userinfo = getchongzhi_user($dingdaninfo['uid']);
			active_cash($dingdaninfo['uid'], $total_fee_t, $chong_userinfo['mobile']);
			/*活动的end*/

			if ($up_q1 && $up_q2 && $up_q3 && $up_q4 && $up_q5 && $up_q6) {
				$this -> db -> Autocommit_commit();
			} else {
				$this -> db -> Autocommit_rollback();
				echo "fail";
				exit ;
			}
			if (empty($dingdaninfo['scookies'])) {
				_setcookie('urecharge', md5('1y-cs' . $dingdaninfo['uid']));
				echo "success";
				exit ;
			}
			$uid = $dingdaninfo['uid'];
			$scookies = unserialize($dingdaninfo['scookies']);
			$pay = System::load_app_class('pay', 'pay');
			$pay -> scookie = $scookies;
			$ok = $pay -> init($uid, $pay_type['pay_id'], 'go_record', $scookies);
			//众乐商品
			if ($ok != 'ok') {
				_setcookie('Cartlist', NULL);
				echo "fail";
				exit ;
				//商品购买失败
			}
			$check = $pay -> go_pay(1);
			if ($check) {
				//$this->db->Query("UPDATE `@#_member_addmoney_record` SET `scookies` = '1' where `code` = '$out_trade_no' and `status` = '已付款'");
				_setcookie('Cartlist', NULL);
				//file_put_contents(time().'issuccess.log', $check);
				echo "success";
				exit ;
			} else {
				//file_put_contents(time().'isfail.log', $check);
				echo "fail";
				exit ;
			}
			//------------------------------
			//处理业务完毕
			//------------------------------
			//log_result("即时到帐后台回调成功");
		}

	}

}//
?>