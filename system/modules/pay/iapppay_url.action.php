<?php 
/*
* APP 爱贝支付回调
* 2015/12/29
*/
defined('G_IN_SYSTEM')or exit('No permission resources.');
ini_set("display_errors","OFF");

class iapppay_url extends SystemAction {
	public function __construct(){			
		$this->db=System::load_sys_class('model');		
	} 	
	
	public function houtai(){
	    include "lib/iapppay/base.php";
	    $iapppayCpUrl="http://ipay.iapppay.com:9999";
	    $tokenCheckUrl=$iapppayCpUrl . "/payapi/tokencheck";
	    $orderUrl=$iapppayCpUrl . "/payapi/order";
	    $queryResultUrl=$iapppayCpUrl . "/payapi/queryresult";
	    
	    //ios私钥与公钥
	    $ios_cpvkey="MIICXQIBAAKBgQC+b2kspF6mHmY7CrpvC7jBtp+ceNxhyi4vW9i9/fyP5VHtp+qrHDtpZHDW4IZu2FakGt2coHLvxD/EdIll6Juv3B6Lvg2AxxyC+UpbcBDVV6K89agt+GkC3RvLXLG6KrbHtnXNvf+bFcn6B0GhFUaxkecY9xiwHT7RS/b/Om6JBQIDAQABAoGADdTZtrxZg4dBtZgZ4mbfjHJPNXYxy4h7wkTRgTspVOo7AR/pE3SBfC4nY/P6z6I26MRqxC2DK5b9aq9sK1Kxu/vkVA0XvyStibY7956RcXqCjJtmjgHoZ3QSVLfNpuhG993X0uhJyHhNYHG11kEQaz7pfL6woRf6mleVP9F+nyECQQD2cj0GRv10QJ6RH8HQ1tHvapoFk0+E/NCP9CTlkRzxcnFGxr3GnFVbU5eFQ7AHWfIVVexfT73EnAKIxUQhYv1/AkEAxdFP7qS5ukxGTS9yw0+F/br7CN24kFXACzr4GTR5n8f8r2C3VBHp+Pb9cphnbs2JvbX6rJQJjBFB89WwaLnDewJAOspRyHeS6vKnYRNkVv+IIUca6w2TQLRWxKCHVMRjVS3p2+p5RzHwItM0KDRXf8lzOKfAtZP+PzLAYAQ/Fn/Y3QJBAK/zxupY4M6hi6KaArVQ5bjTePjg5oqTi1cx433f3BLQOqTwHQENUhTd9H/aqZ23+r2gwVCB+J/6tbQiLWBpY0MCQQCJjAscqwQ+BbJybZeIzSQ3/aGwHxZe/NEeeAFQi4YBai3ib69tLsAd7ZRT1d9a19ZPlE0Ct4WlW1roRyg/nqS4";
	    $ios_platpkey="MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCCWESSTK8IwJD9AIm1/665tjZbYkBaPpqaWS8Auevbv7lEJVnku9dqsDpt/1VUT45YS6dTDSfEI32soX15C805A+QVwBgfP0zVKzmPgfvAp+x73BryISbdvwPA7Kla5WLo9nN6G6ZJYAJZI2xzC/3neJ72JWTBowF3aywd+W32DQIDAQAB";
	    
	    //android私钥 与公钥
	    $android_cpvkey="MIICWwIBAAKBgQCB5pCZtypALfMAdPE3/apy+5lgo6cDowcbqCIdIVIsW03lkJIg/rbhQslGaikSLSLKPmteBhFJwC5biyUfSslJzYzL0tDwXFG4ZTs3eo1YsbfDHVuiS3n2STFLreIY5aYX/6WbzCc57epUa2QOiou7rpyU4yvn3cyydBePfWXQMQIDAQABAoGAUmhaw6mXfC+qQVfuz351a4yBuPUZNw1trPX07lX9M3aY7FHS4mu0ySL9XVZrx8QqlfMTWgqvXQI2ZozXeAqRtv/vuMAraqWeI4FvqF5VckgnIdeIPDVa6wnyny9b/uXnIsIWtc1mv3WMKRjTy+TJKwRGIIsLRtEf3oDvifHN/yECQQDIZMUAIRt6C0K4s1/GaVInrFUs2NUsEy5YG4x9FKUMSSXbrrfa+zuznarVupT0xabnpdSnHHgB89G476lE2YM7AkEApfI7PEXbk6cZagLhhMRPp5dpanbCTOwfizs54hEYm/+L3vF4+Z3NFwHx0S+tWlYEiZRy/S6bRz2EREGYSNprgwJAZCfROj5R0E6ee1fTb4QmNoPwUijRamTvzcN/FOjjkN9iV4jchUr5zMzpnEbTuf6ra627OgaFiFxHHopR7IJWDwJADfG73oW0PMUG0qypTKG+W8PSXNrb2uDgia+RXpi3K6yekT5TPw5uhGdqbXyHIZcmzHTH8NRwwdy+d4nXhQ+thwJAKCyxiLGfaJW5cpLQP3xoJfBsO2ADykkw/cLnW6qSuCqZkZwJmuQzLqgHnVXQ/Y+Q1KXaCgr7j393OLEyVxqB2w==";
	    $android_platpkey="MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCM2Cxt3YWckE9j1+vFxkJNuKcCkteG8nHABTg6OhxvCOy+koK7OKdegvHmw/cDuq15EPjCzoyk0kMXDwGmKq5EsPxDtBU2HXCeJDRqmlW2ytK/aFUE0i3DNGlb8erlKwc80aNVQ700XxmzXG2j9AtH0nJafHbHHEcnMk+iKd2KTQIDAQAB";
	    
	    $appid_arr = array('ios'=>'3003477112','android'=>'300347668');
	    $cpvkey_arr = array('ios'=>$ios_cpvkey,'android'=>$android_cpvkey);
	    $platpkey_arr = array('ios'=>$ios_platpkey,'android'=>$android_platpkey);
	    
		$string = $_POST;//接收post请求数据
		
		$transdata = stripslashes($string['transdata']);  //删除反斜杠
		$transdata = json_decode($transdata,true);
	    $out_trade_no = $transdata['cporderid'];	//商户订单号
		$tmoney = intval($transdata['money']);		//交易金额
		$cppriva = $transdata['cpprivate'];
		
		//支付方式
		$paytype_list = array(
		    '401'=>'支付宝','402'=>'财付通','501'=>'支付宝','502'=>'财付通',
		    '403'=>'微信支付','5'=>'爱贝币','6'=>'爱贝币','16'=>'百度钱包'
		);
		$paytype = isset($transdata['paytype']) ? trim($transdata['paytype']) : '401';
		$paytype = $paytype_list[$paytype];   //支付方式
		
		//支付结果查询接口
		$queryReq["appid"] = $appid_arr[$cppriva];        // 300347668  android  ios
		$queryReq["cporderid"] = $out_trade_no;
		
		$cpvkey = $cpvkey_arr[$cppriva];
		$platpkey = $platpkey_arr[$cppriva];
		
		//组装请求报文
		$reqData = composeReq($queryReq, $cpvkey);
		
		//发送到爱贝服务后台
		$respData = request_by_curl($queryResultUrl, $reqData, "queryResult test");
		 
		//返回报文解析
		if( ! parseResp($respData, $platpkey, $respJson)) {
		    echo "failed";exit;
		}else{
		    $this->db->Autocommit_start();
		    $dingdaninfo = $this->db->GetOne("select * from `@#_member_addmoney_record` where `code` = '$out_trade_no' and `money`='$tmoney'  and  `status` = '未付款' for update");
		    if(!$dingdaninfo){	echo "failed";exit;}	//没有该订单,失败
		    
		    $UserBankCon = System::load_contorller('UserBank','Zapi');
		    $result = $UserBankCon->ipay_recharge($transdata,$dingdaninfo,$paytype);
		    
		    if($result){
		        $this->db->Autocommit_commit();
		        echo "success";exit;
		    }else{
		        $this->db->Autocommit_rollback();
		        echo "failed";exit;
		    }
		    
		    //以下流程省略，APP支付支付成功后再次发起余额支付
		    
		    /*if(empty($dingdaninfo['scookies']) || ! $dingdaninfo['scookies']){
		        echo "success";exit;	//充值完成
		    }*/
		    
		    //////////////////////////////////////////////////////
		    ///如果是购买商品，需要继续执行余额支付   20151227 kpf///
		    /////////////////////////////////////////////////////
		    /*$scookies = unserialize($dingdaninfo['scookies']);
		     $wpay = System::load_app_class('wpay','pay');
		     $wpay->scookie = $scookies;
		    
		     $ok = $wpay->init($uid,$pay_type['pay_id'],'go_record');	//众乐商品
		     if($ok != 'ok'){
		     echo "failed";exit;	//商品购买失败
		     }
		     $check = $wpay->go_pay(1,$dingdaninfo['code']);
		     if($check){
		     $this->db->Query("UPDATE `@#_member_addmoney_record` SET `scookies` = '1' where `code` = '$out_trade_no' and `status` = '已付款'");
		     _setcookie('Cartlist',NULL);
		     echo "success";exit;
		     }else{
		     echo "failed";exit;
		     }*/
		}
	}
	
   
}

?>