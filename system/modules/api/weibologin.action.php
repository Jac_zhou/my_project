<?php
/*
* weibo登录
* kangpengfei  2015.12.09
*/
defined('G_IN_SYSTEM')or exit("no");
include_once dirname(__FILE__).'/lib/weibo/config.php';
include_once dirname(__FILE__).'/lib/weibo/saetv2.ex.class.php';

class weibologin extends SystemAction {
	
	private $weibo;
	private $db;
	private $token;
	public function __construct(){
		$this->weibo = new SaeTOAuthV2( WB_AKEY , WB_SKEY );
		$this->db = System::load_sys_class("model");
	}
	
	//weibo登录
	public function init(){
		if(! WB_OFF){
			_message("暂未开通微博登录",G_WEB_PATH);
		}
		$code_url = $this->weibo->getAuthorizeURL( WB_CALLBACK_URL );
		header("Location:$code_url");
	}
	
	//weibo回调
	public function callback(){	
		if (isset($_REQUEST['code'])) {
			$keys = array();
			$keys['code'] = $_REQUEST['code'];
			$keys['redirect_uri'] = WB_CALLBACK_URL;
			try {
				$token = $this->weibo->getAccessToken( 'code', $keys ) ;
			} catch (OAuthException $e) {
			}
		}
		if ( ! $token) {
			header("Location:".G_WEB_PATH);exit;	//授权失败
		}
		
		//开始处理
		$this->token = $token;
		$_SESSION['token'] = $token;
		setcookie( 'weibojs_'.$this->weibo->client_id, http_build_query($token) );
		
		$go_user_info = $this->db->GetOne("select * from `@#_member_band` where `b_code` = '$token' and `b_type` = 'weibo' LIMIT 1");
		
		if(!$go_user_info){
			$this->weibo_add_member();	// 添加新会员
		}else{		
			$uid = intval($go_user_info['b_uid']);
			$go_member_info = $this->db->GetOne("select uid from `@#_member` where `uid` = '$uid' LIMIT 1");
			if(!$go_member_info){
				$this->db->Query("DELETE FROM `@#_member_band` WHERE `b_uid` = '$uid'");
				$this->weibo_add_member();		// 添加新会员
			}else{
				$this->weibo_set_member($uid,'login_bind');		// 更新会员
			}
		}
	}

	//添加会员
	private function weibo_add_member(){	
		$c = new SaeTClientV2( WB_AKEY , WB_SKEY , $_SESSION['token']['access_token'] );
		$uid_get = $c->get_uid();
		$uid = $uid_get['uid'];
		$weibo_user_info = $c->show_user_by_id( $uid);//根据ID获取用户等基本信息
		
		$member_db=System::load_app_class('base','member');
		$memberone=$member_db->get_user_info();
		if($memberone){
			$go_user_id = $memberone['uid'];
			$token    = $this->token;
			$go_user_time = time();
			$this->db->Query("INSERT INTO `@#_member_band` (`b_uid`, `b_type`, `b_code`, `b_time`) VALUES ('$go_user_id', 'weibo', '$token', '$go_user_time')");
			$bands = trim($memberone['band'],",");			
			$bands = $bands.",weibo";
			$this->db->Query("UPDATE `@#_member` SET `band` = '$bands' where `uid` = '$go_user_id' limit 1");
			_message("微博绑定成功",G_WEB_PATH);
			return;
		}
		
		$go_user_time = time();
		if(!$weibo_user_info)$weibo_user_info=array('screen_name'=>'WBU'.$go_user_time.rand(0,9));
		$go_y_user = $this->db->GetOne("select * from `@#_member` where `username` = '$weibo_user_info[screen_name]' LIMIT 1");
		
		if($go_y_user)$weibo_user_info['screen_name'] .= rand(1000,9999);
		$go_user_name = _htmtocode($weibo_user_info['screen_name']);
		
		$go_user_img  = $this->save_touimg($weibo_user_info['profile_image_url']);	//保存头像
		
		$go_user_pass = md5('123456');
		$token    = $this->token;
		$this->db->Autocommit_start();
		$q1 = $this->db->Query("INSERT INTO `@#_member` (`username`,`password`,`img`,`band`,`time`) VALUES ('$go_user_name','$go_user_pass','$go_user_img','weibo','$go_user_time')");
		$go_user_id = $this->db->insert_id();
		$q2 = $this->db->Query("INSERT INTO `@#_member_band` (`b_uid`, `b_type`, `b_code`, `b_time`) VALUES ('$go_user_id', 'weibo', '$token', '$go_user_time')");
		if($q1 && $q2){
			$this->db->Autocommit_commit();
			$this->weibo_set_member($go_user_id,'add');

		}else{
			$this->db->Autocommit_rollback();
			_message("登录失败!",G_WEB_PATH);
		}
		
	}

	//更新会员
	private function weibo_set_member($uid=null,$type='bind_add_login'){	
		$member_db=System::load_app_class('base','member');
		$memberone=$member_db->get_user_info();
		if($memberone){
			_message("该微博号已经被其他用户所绑定！",WEB_PATH.'/login');
		}
		$member = $this->db->GetOne("select uid,password,mobile,email from `@#_member` where `uid` = '$uid' LIMIT 1");		
		$_COOKIE['uid'] = null;
		$_COOKIE['ushell'] = null;
		$_COOKIE['UID'] = null;
		$_COOKIE['USHELL'] = null;	
		
		$time = time();
		$user_ip = _get_ip_dizhi();
		$this->db->GetOne("UPDATE `@#_member` SET `user_ip` = '$user_ip',`login_time` = '$time' where `uid` = '$uid'");
				
		$s1 = _setcookie("uid",_encrypt($member['uid']),60*60*24*7);			
		$s2 = _setcookie("ushell",_encrypt(md5($member['uid'].$member['password'].$member['mobile'].$member['email'])),60*60*24*7);
	
	
		$domain = System::load_sys_config('domain');
		
		if(isset($domain[$_SERVER['HTTP_HOST']])){
			if($domain[$_SERVER['HTTP_HOST']]['m'] == 'mobile'){
					$callback_url =  WEB_PATH."/mobile/home";
			}else{
					$callback_url =  WEB_PATH."/member/home";					
			}				
		}else{
			$callback_url =  WEB_PATH."/member/home";	
		}	

		if($s1 && $s2){
			if(!$member['email'] || !$member['mobile']){
				_message("登录成功，请绑定邮箱或手机号和及时修改默认密码!",$callback_url);
			}
				_message("登录成功!",$callback_url);
		}else{
			_message("登录失败请检查cookie!",G_WEB_PATH);
		}		
	}
	
	//保存头像至服务器
	private function save_touimg($path)
	{
		$go_user_img = 'photo/member.jpg';
		$touimg = file_get_contents($path);		
		$path = 'touimg/' . date('Ymd');
		if(!file_exists(G_UPLOAD . $path)){
			@mkdir($path);
		}
		$filename = $path . '/'. time() . mt_rand(10000,9999).'.jpg';
		$filenames = G_UPLOAD . $filename;
		@file_put_contents($filenames, $touimg);
		if(file_exists($filenames)){
			$go_user_img =  $filename;
			System::load_sys_class('upload','sys','no');
			upload::thumbs(160,160,false,$filenames);
			upload::thumbs(80,80,false,$filenames);
			upload::thumbs(30,30,false,$filenames);	
		}
		return $go_user_img;
	}
	
	//参数配置
	public function weibo_set_config(){
		System::load_app_class("admin",G_ADMIN_DIR,'no');
		$objadmin = new admin();		
		$config = System::load_app_config("connect");
		if(isset($_POST['dosubmit'])){
			$weibo_off = intval($_POST['type']);
			$akey = $_POST['akey'];
			$skey = $_POST['skey'];
			$config['weibo'] = array("off"=>$weibo_off,"akey"=>$akey,"skey"=>$skey);
			$html = var_export($config,true);
			$html = "<?php return ".$html."; ?>";
			$path =  dirname(__FILE__).'/lib/connect.ini.php';
			if(!is_writable($path)) _message('Please chmod  connect.ini.php  to 0777 !');
			$ok=file_put_contents($path,$html);
			
			//把配置写入微博平台配置文件
			$wb_config = dirname(__FILE__).'/lib/weibo/config.php';
			$back_url = 'http://'.$_SERVER['HTTP_HOST'].'/index.php/api/weibologin/callback/';
			if(!is_writable($wb_config)) _message('Please chmod  connect.ini.php  to 0777 !');
			$config_txt = <<<EOF
<?php
header('Content-Type: text/html; charset=UTF-8');

define( "WB_OFF" , "$weibo_off" );
define( "WB_AKEY" , "$akey" );
define( "WB_SKEY" , "$skey" );
define( "WB_CALLBACK_URL" , "$back_url" );
EOF;
			@file_put_contents($wb_config,$config_txt);
			
			_message("配置更新成功!");
		}	
	
		$config = $config['weibo'];	
		include $this->tpl(ROUTE_M,'weibo_set_config');
	}

	
}

?>