<?php
/*
* 微信登录
* kangpengfei  2015.12.11
*/
defined('G_IN_SYSTEM')or exit("no");
include_once dirname(__FILE__).'/lib/weixin/config.php';
include_once dirname(__FILE__).'/lib/weixin/weixin.class.php';

class wxlogin extends SystemAction {
	
	private $weixin;
	private $db;
	private $openid;
	public function __construct(){
		$this->weixin = new WeiXin( WX_APPID , WX_SECRET );
		$this->db = System::load_sys_class("model");
	}
	
	//微信登录
	public function init(){
		if(! WX_OFF){
			_message("暂未开通微信登录",G_WEB_PATH);
		}
		$code_url = $this->weixin->getCodeURL( WX_REDIRECT_URI , WX_STATE );
		//var_dump($code_url);die;
		header("Location:$code_url");
	}
	
	//微信回调
	public function callback(){
		if (isset($_REQUEST['code'])) {
			$code = $_REQUEST['code'];
			$state = $_REQUEST['state'];
			if($state != WX_STATE){
				header("Location:".G_WEB_PATH);exit;	//非法来源
			}
			//print_r($this->weixin);die;
			$wx_token = $this->weixin->getAccessTokenURL($code);
			if( ! isset($wx_token['access_token'])){
				header("Location:".G_WEB_PATH);exit;	// 请求失败
			}
			$this->openid = $wx_token['openid'];
			$openid = $wx_token['openid'];
			$_SESSION['wx_token'] = $wx_token;
		}else{
			header("Location:".G_WEB_PATH);exit;	//授权失败
		}
		$weixin_user_info = $this->weixin->getUserInfoURL($wx_token['access_token'],$openid);
		
		$openid = $weixin_user_info['unionid'];
		//开始处理
		//1.查询当前第三方用户是否绑定财神账号
		$go_user_info = $this->db->GetOne("select * from `@#_member_band` where `b_code` = '$openid' and `b_type` = 'weixin' LIMIT 1");
		
		
		//判断当前的用户是否已有绑定的账号 stop here 2016-06-29
		if(!empty($go_user_info)){
			$uid = intval($go_user_info['b_uid']);
			if($uid > 0){
				$go_member_info = $this->db->GetOne("select uid from `@#_member` where `uid` = '$uid' LIMIT 1");
			
				if(!$go_member_info){
					
					$this->db->Query("update `@#_member_band` set  b_uid=0  WHERE `b_code` = '$openid' ");
					_message('该用户已被注销！',WEB_PATH.'/api/wxlogin/bindwx?openid='.$openid);
				}else{
					$this->go_login($uid);		//直接登录
				}
			}else{
				//如果该QQ用户没有绑定账号  跳转到QQ用户登陆绑定页面
				_message('没有绑定财神账户，无法快速快捷登陆！',WEB_PATH.'/api/wxlogin/bindwx?openid='.$openid);
			}
		}else{
			//如果没有绑定用户，那么先将微信用户的信息存入缓存，并且跳转到绑定页面
			$time     = time();	
			
			$data = array();
			$data['unionid'] 	= $weixin_user_info['unionid'];
			$data['headimgurl'] = $weixin_user_info['headimgurl'];
			//额外存储的数据
			$data 		= serialize($data);
			$userinfo 	= serialize($weixin_user_info);				//序列化用户信息
			S('userinfo_'.$openid,$userinfo);
			
			$sql = "INSERT INTO 
						`@#_member_band` 
							(`b_type`, `b_code`,`b_data`,`b_time`)
						VALUES 
							('weixin', '$openid','$data','$time')
					";	
			$this->db->Query($sql);
			$id = $this->db->insert_id();
			if($id > 0){
				//如果该QQ用户没有绑定账号  跳转到QQ用户登陆绑定页面
				header('Location:'.WEB_PATH.'/api/wxlogin/bindwx?openid='.$openid);die; 
			}else{
				_message('出现未知错误！');
			}
			
			
		}
	}
	//绑定页面
	public function bindwx(){
		
		$openid = safe_replace($_GET['openid']);
		
		$userinfo = unserialize(S('userinfo_'.$openid));
		
		include templates('second_login','bindwx');
		
	}
	public function do_bind(){	
		$openid = safe_replace($_POST['openid']);
		$username  = trim($_POST['username']);
		if(!_checkmobile($username)){
			_message('手机号码格式不正确！');die;	
			die;
		}
		$password  = md5(trim($_POST['password']));
		//手机账号登录
		$member=$this->db->GetOne("select * from `@#_member` where `mobile`='$username'");
		if(!$member){
			_message('不存在该用户账号！');die;	
		}
		$uid= $member['uid'];
		if($member['wx_id']>0){
			_message("被账户被其他微信号绑定，您可以登录用户中心进行解绑！",WEB_PATH.'/login');die;
		}
		if($password!= $member['password']){
			_message('密码错误');die;
		}
		
		$sql 	= "select b_id from `@#_member_band` where b_code='$openid'";
		$result = $this->db->GetOne($sql);
		$band_id = $result['b_id'];
		
		$this->db->Autocommit_start();
		//用户的id
		$sql = "update `@#_member_band` set `b_uid`='$uid' where b_code='$openid'";
		$q1  = $this->db->Query($sql);
		$sql = "update `@#_member` set wx_id='$band_id' where uid='$uid'";
		$q2  = $this->db->Query($sql);
		if($q2 && $q1){
			$this->db->Autocommit_commit();
			$this->go_login($uid);
		}else{
			$this->db->Autocommit_rollback();
		}
	}
	
	public function go_login($uid){
		if($uid>0){
			
			$member = $this->db->GetOne("select uid,password,mobile,email from `@#_member` where `uid` = '$uid' LIMIT 1");	
			$user_ip = _get_ip_dizhi();
			$_COOKIE['uid'] = null;
			$_COOKIE['ushell'] = null;
			$_COOKIE['UID'] = null;
			$_COOKIE['USHELL'] = null;	
			
			$time = time();
			$user_ip = _get_ip_dizhi();
			$this->db->GetOne("UPDATE `@#_member` SET `user_ip` = '$user_ip',`login_time` = '$time' where `uid` = '$uid'");
					
			$s1 = _setcookie("uid",_encrypt($member['uid']),60*60*24*7);			
			$s2 = _setcookie("ushell",_encrypt(md5($member['uid'].$member['password'].$member['mobile'].$member['email'])),60*60*24*7);
			if($s1 && $s2){				
				_message('登录成功！',WEB_PATH."/member/home");
			}else{
				_message('未知错误');
			}
		}		
	}	
	//保存头像至服务器
	private function save_touimg($path)
	{
		$go_user_img = 'photo/member.jpg';
		$touimg = file_get_contents($path);		
		$path = 'touimg/' . date('Ymd');
		if(!file_exists(G_UPLOAD . $path)){
			@mkdir($path);
		}
		$filename = $path . '/'. time() . mt_rand(10000,9999).'.jpg';
		$filenames = G_UPLOAD . $filename;
		@file_put_contents($filenames, $touimg);
		if(file_exists($filenames)){
			$go_user_img =  $filename;
			System::load_sys_class('upload','sys','no');
			upload::thumbs(160,160,false,$filenames);
			upload::thumbs(80,80,false,$filenames);
			upload::thumbs(30,30,false,$filenames);	
		}
		return $go_user_img;
	}
	
	//参数配置
	public function weixin_set_config(){
		System::load_app_class("admin",G_ADMIN_DIR,'no');
		$objadmin = new admin();		
		$config = System::load_app_config("connect");
		if(isset($_POST['dosubmit'])){
			$wx_off = intval($_POST['type']);
			$appid = $_POST['appid'];
			$secret = $_POST['secret'];
			$config['weixin'] = array("off"=>$wx_off,"appid"=>$appid,"secret"=>$secret);
			$html = var_export($config,true);
			$html = "<?php return ".$html."; ?>";
			$path =  dirname(__FILE__).'/lib/connect.ini.php';
			if(!is_writable($path)) _message('Please chmod  connect.ini.php  to 0777 !');
			$ok=file_put_contents($path,$html);
			
			$state = 'YGCMS';
			//把配置写入微信平台配置文件
			$wx_config = dirname(__FILE__).'/lib/weixin/config.php';
			$redirect_uri = 'http://'.$_SERVER['HTTP_HOST'].'/index.php/api/wxlogin/callback';
			if(!is_writable($wx_config)) _message('Please chmod  config.php  to 0777 !');
			$config_txt = <<<EOF
<?php
header('Content-Type: text/html; charset=UTF-8');

define( "WX_OFF" , "$wx_off" );
define( "WX_APPID" , "$appid" );
define( "WX_SECRET" , "$secret" );
define( "WX_REDIRECT_URI" , "$redirect_uri" );
define( "WX_STATE" , "$state" );
EOF;
			@file_put_contents($wx_config,$config_txt);
			
			_message("配置更新成功!");
		}	
	
		$config = $config['weixin'];	
		include $this->tpl(ROUTE_M,'weixin_set_config');
	}

	//添加会员
	private function weixin_add_member(){
		$openid = $this->openid;
		$sql = "select b_data from @#_member_band where b_code='$openid' and b_type='weixin'";
		$b_data = $this->db->GetOne($sql);
		$weixin_user_info = unserialize($b_data);
		if(empty($weixin_user_info)){
			$weixin_user_info = $this->weixin->getUserInfoURL($_SESSION['wx_token']['access_token'], $this->openid);
		}
		$member_db=System::load_app_class('base','member');
		$memberone=$member_db->get_user_info();
		if($memberone){
			$go_user_id = $memberone['uid'];
			$openid    = $this->openid;
			$go_user_time = time();
			$this->db->Query("INSERT INTO `@#_member_band` (`b_uid`, `b_type`, `b_code`, `b_time`) VALUES ('$go_user_id', 'weixin', '$openid', '$go_user_time')");
			$bands = trim($memberone['band'],",");			
			$bands = $bands.",weixin";
			$this->db->Query("UPDATE `@#_member` SET `band` = '$bands' where `uid` = '$go_user_id' limit 1");
			_message("微信绑定成功",G_WEB_PATH);
			return;
		}
		$data = '';				//用户数据
		$go_user_time = time();
		if(!$weixin_user_info){
			$weixin_user_info=array('nickname'=>'WXU'.$go_user_time.rand(0,9));
		}else{
			$data = serialize($weixin_user_info);
		}
		//不需要
		/*$go_y_user = $this->db->GetOne("select * from `@#_member` where `username` = '$weixin_user_info[nickname]' LIMIT 1");
		
		if($go_y_user)$weixin_user_info['nickname'] .= rand(1000,9999);*/
		$go_user_name = _htmtocode($weixin_user_info['nickname']);
		
		$go_user_img  = $this->save_touimg($weixin_user_info['headimgurl']);	//保存头像
		
		$go_user_pass = md5('123456');
		$openid  = $this->openid;
		$this->db->Autocommit_start();
		$q1 = $this->db->Query("INSERT INTO `@#_member` (`username`,`password`,`img`,`band`,`time`) VALUES ('$go_user_name','$go_user_pass','$go_user_img','weixin','$go_user_time')");
		$go_user_id = $this->db->insert_id();
		$q2 = $this->db->Query("INSERT INTO `@#_member_band` (`b_uid`, `b_type`, `b_code`, `b_time`,`b_data`) VALUES ('$go_user_id', 'weixin', '$openid', '$go_user_time','$data')");
		if($q1 && $q2){
			$this->db->Autocommit_commit();
			$this->weixin_set_member($go_user_id,'add');
		}else{
			$this->db->Autocommit_rollback();
			_message("登录失败!",G_WEB_PATH);
		}
	}
	
	//更新会员
	private function weixin_set_member($uid=null,$type='bind_add_login'){	
		$member_db=System::load_app_class('base','member');
		$memberone=$member_db->get_user_info();
		if($memberone){
			_message("该微信号已经被其他用户所绑定！",WEB_PATH.'/login');
		}
		$member = $this->db->GetOne("select uid,password,mobile,email from `@#_member` where `uid` = '$uid' LIMIT 1");		
		$_COOKIE['uid'] = null;
		$_COOKIE['ushell'] = null;
		$_COOKIE['UID'] = null;
		$_COOKIE['USHELL'] = null;	
		
		$time = time();
		$user_ip = _get_ip_dizhi();
		$this->db->GetOne("UPDATE `@#_member` SET `user_ip` = '$user_ip',`login_time` = '$time' where `uid` = '$uid'");
				
		$s1 = _setcookie("uid",_encrypt($member['uid']),60*60*24*7);			
		$s2 = _setcookie("ushell",_encrypt(md5($member['uid'].$member['password'].$member['mobile'].$member['email'])),60*60*24*7);
	
	   $callback_url =  WEB_PATH."/member/home/userbuylist";	
	   
	   $bind_url = WEB_PATH."/member/home/bind_mobile";	   //绑定手机号的链接

		if($s1 && $s2){
			if( !$member['mobile'] ){
				_message("登录成功，请绑定手机号!",$bind_url);
			}
				_message("登录成功!",$callback_url);
		}else{
			_message("登录失败请检查cookie!",G_WEB_PATH);
		}		
	}
	
}

?>