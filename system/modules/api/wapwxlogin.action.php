<?php
/*
* 微信登录
* kangpengfei  2015.12.11
*/
defined('G_IN_SYSTEM')or exit("no");
include_once dirname(__FILE__).'/lib/weixin/config.php';
include_once dirname(__FILE__).'/lib/weixin/weixin.class.php';

class wapwxlogin extends SystemAction {
	
	private $weixin;
	private $db;
	private $openid;
	private $wx_appid = 'wx560b8c295327f632';
	private $wx_secret = '87a870f0c62e919585567eadd5df385a';
	private $wx_state = 'caishen123456';
	
	public function __construct(){
		$this->weixin = new WeiXin( $this->wx_appid , $this->wx_secret );
		$this->db = System::load_sys_class("model");
	}
	
	//微信登录
	public function init(){
		if(! WX_OFF){
			_message("暂未开通微信登录",G_WEB_PATH);
		}
		$recert_url = "http://www.yz-db.com/index.php/api/wapwxlogin/callback";
		$code_url = $this->weixin->getWapCodeURL( $recert_url , $this->wx_state , 'snsapi_login' );
		header("Location:$code_url");
	}
	
	//微信回调
	public function callback(){
		if (isset($_REQUEST['code'])) {
			$code = $_REQUEST['code'];
			$state = $_REQUEST['state'];
			if($state != $this->wx_state){
				header("Location:".G_WEB_PATH);exit;	//非法来源
			}
			$wx_token = $this->weixin->getAccessTokenURL($code);
			
			if( ! isset($wx_token['access_token'])){
				header("Location:".G_WEB_PATH);exit;	// 请求失败
			}
			$this->openid = $wx_token['unionid'];
			$openid = $this->openid;
			$wx_userinfo = $this->weixin->getUserInfoURL($wx_token['access_token'],$wx_token['openid']);
		}else{
		    header("location:http://www.yz-db.com/yungou/index.html#/tab/home");
		}
		$MemberBandModel =	System::load_app_model('MemberBand','Zapi'); 
		$openid = $wx_userinfo['unionid'];
		//开始处理
		$go_user_info = $this->db->GetOne("select * from `@#_member_band` where `b_code` = '$openid' and `b_type` = 'weixin' LIMIT 1");
		
		if($go_user_info){
			$uid = intval($go_user_info['b_uid']);
			
			if($uid>0){
				$go_member_info = $this->db->GetOne("select uid from `@#_member` where `uid` = '$uid' LIMIT 1");
				if($go_member_info){
					$result = $this->go_login($uid);		//直接登录
					if($result){
						_message('登录成功！',WEB_PATH.'/yungou/index.html#/tab/login',1);
					}else{
						_message('未知错误！',WEB_PATH.'/yungou/index.html#/tab/login');
					}
					die;
				}else{
					$data = array('b_uid'=>0);
					$map  = array('b_code'=>$openid,);
					$MemberBandModel->update($data,$map);
					_message('用户被注销！',WEB_PATH.'/yungou/index.html#/tab/bindwx?type=wx&openid='.$openid,1);
				}
			}else{
				_message('没有绑定用户！',WEB_PATH.'/yungou/index.html#/tab/bindwx?type=wx&openid='.$openid,1);
			}
		}else{
			S('userinfo_'.$openid,serialize($wx_userinfo));
			//如果没有绑定用户，那么先将微信用户的信息存入缓存，并且跳转到绑定页面
			$time     = time();	
			$map = array(
					'b_type'=>'weixin',
					'b_code'=>$openid,
					'b_time'=>$time
				);
			$id = $MemberBandModel->insert_into($map);
			if($id > 0){
				//如果该QQ用户没有绑定账号  跳转到QQ用户登陆绑定页面
				_message('前去绑定账户！',WEB_PATH.'/yungou/index.html#/tab/bindwx?openid='.$openid);
			}else{
				_message('出现未知错误！');
			}	
		}
	}



	public function go_login($uid){
		
		if($uid>0){			
			$member = $this->db->GetOne("select uid,password,mobile,email from `@#_member` where `uid` = '$uid' LIMIT 1");	
			
			$_COOKIE['uid'] = null;
			$_COOKIE['ushell'] = null;
			$_COOKIE['UID'] = null;
			$_COOKIE['USHELL'] = null;	
			$_COOKIE['holdUID'] = null;
			$time = time();
			$user_ip = _get_ip_dizhi();
			$this->db->GetOne("UPDATE `@#_member` SET `user_ip` = '$user_ip',`login_time` = '$time' where `uid` = '$uid'");
			if(is_mobile_request()){
				$s0 = _setcookie("holdUID",$member['uid'],60*60*24*7);		
				if($s0)
					return TRUE;
				else
					return false;
			}else{
				$s1 = _setcookie("uid",_encrypt($member['uid']),60*60*24*7);		
				$s2 = _setcookie("ushell",_encrypt(md5($member['uid'].$member['password'].$member['mobile'].$member['email'])),60*60*24*7);
				if($s1 && $s2){
					return $member;				
				}else{
					return false;
				}
			}
		}	
		return false;
	}	

	
	
	
	//保存头像至服务器   $path 图片路径
	private function save_touimg($path)
	{
		$go_user_img = 'photo/member.jpg';
		$touimg = file_get_contents($path);		
		$path = 'touimg/' . date('Ymd');
		if(!file_exists(G_UPLOAD . $path)){
			@mkdir($path);
		}
		$filename = $path . '/'. time() . mt_rand(10000,9999).'.jpg';
		$filenames = G_UPLOAD . $filename;
		@file_put_contents($filenames, $touimg);
		if(file_exists($filenames)){
			$go_user_img =  $filename;
			System::load_sys_class('upload','sys','no');
			upload::thumbs(160,160,false,$filenames);
			upload::thumbs(80,80,false,$filenames);
			upload::thumbs(30,30,false,$filenames);	
		}
		return $go_user_img;
	}

	
}

?>