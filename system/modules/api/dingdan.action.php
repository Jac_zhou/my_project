<?php 


class dingdan extends SystemAction {

	private $db;
	public function __construct(){
		$this->db = System::load_sys_class("model");
	}
	
	/*
	*	设置发货
	**/
	public function set(){
		
		$oid = isset($_POST['oid']) ? intval($_POST['oid']) : '';
		$uid = isset($_POST['uid']) ? intval($_POST['uid']) : '';
		if(!$oid || !$uid){
			$info = array('error'=>1,'msg'=>'参数有误！');
			die(json_encode($info));
		}
		$info = $this->db->GetOne("SELECT uid,status FROM `@#_member_go_record` WHERE `id` = '$oid' and `uid` = '$uid' limit 1");
		$status = @explode(",",$info['status']);
		if(is_array($status) &&  $status[1]=='已发货'){
			$status = '已付款,已发货,已完成';
			$time = time();
			$q = $this->db->Query("UPDATE `@#_member_go_record` SET `status` = '$status', `confirm_time` = '$time' WHERE `id` = '$oid'");
			$info = array('error'=>0,'msg'=>'已完成！','url'=>WEB_PATH.'/member/home/orderlist');
		}else{
			$info = array('error'=>1,'msg'=>'请确认是否已经发货！');
		}
		die(json_encode($info));
		
	}

}

?>