<?php
/*
 * 2015、01、02
 * 商品没有正常开奖时，则去百度彩票中心查询这一期的开奖号码
 * 
 * */

/*
* 计算中奖号码
*/
function get_q_user_code($data=array())
{
	
    $q_user = serialize(array());
	if(!isset($data['q_sscphase']) || !isset($data['shopid']) || !isset($data['q_counttime']) || !isset($data['zongrenshu'])){
		return array('ssc_code'=>'', 'q_user_code'=>'', 'q_uid'=>'');
	}
	$ssc_code = get_ssc_code($data['q_sscphase']);
	if(strpos($data['q_counttime'],'.')!==false){
	    $time = $data['q_counttime'];
	    $h=abs(date("H",$time));
	    $i=date("i",$time);
	    $s=date("s",$time);
	    $w=substr($time,11,3);
	    $data['q_counttime'] = $h.$i.$s.$w;
	}
	
	$code = 10000001 + fmod($data['q_counttime'] + $ssc_code, $data['zongrenshu']);
	
	$db = System::load_sys_class('model');
	
	$shopid = $data['shopid'];
	//中奖人信息
	$user_info = $db->GetOne("SELECT id,uid FROM @#_member_go_record WHERE `shopid`='$shopid' AND `goucode` like '%$code%' LIMIT 1");
	
	if(!isset($user_info['id'])){
	    return array('ssc_code'=>'', 'q_user_code'=>'', 'q_uid'=>'');
	}else{
	    $userinfo = $db->GetOne("SELECT uid,username,email,mobile,img FROM @#_member WHERE `uid`='".$user_info['uid']."' LIMIT 1");
	    $q_user = serialize($userinfo);
	    //更新中奖人数据
	    $db->Query("UPDATE @#_shoplist SET `q_uid`='".$user_info['uid']."',`q_user`='$q_user',`q_ssccode`='$ssc_code',`q_showtime`='N',`q_user_code`='$code' WHERE `id`='$shopid' ");
	    $db->Query("UPDATE @#_member_go_record SET `huode`='$code' WHERE `id`='".$user_info['uid']."' LIMIT 1");

	}
	
	return array('ssc_code'=>$ssc_code, 'q_user_code'=>$code, 'q_uid'=>$user_info['uid'],'q_user'=>$q_user);
}


/*
* 当开不出奖时 应急查询开奖号码
*/
function get_ssc_code($ssc_phase='')
{
	
	if(!$ssc_phase){
		return '00000';
	}
	
	$temp_phase = substr($ssc_phase, 0, 8);
	$date = substr($temp_phase, 0, 4).'-'.substr($temp_phase, 4, 2).'-'.substr($temp_phase, 6, 2);
	$url = "http://baidu.lecai.com/lottery/draw/list/200?d=$date";   //查询链接
	
	$data = array("d" => $date);  
	$data = http_build_query($data);  
	$opts = array(  
		'http'=>array(  
			 'method'=>"POST",  
			 'header'=>"Content-type:text/html; charset=utf-8\r\n". 
						 "Content-length:".strlen($data)."\r\n" .  
						 "Host:baidu.lecai.com\r\n" .
						 "Referer:http://baidu.lecai.com/lottery/draw/list/200?d=$date\r\n".
						 "User-Agent:Mozilla/5.0 (Windows NT 6.1; WOW64; rv:43.0) Gecko/20100101 Firefox/43.0\r\n".
						 "\r\n",  
			 'content' => $data,  
		)  
	);  
	$header = stream_context_create($opts);  
	$back_data = file_get_contents($url, false, $header); 
	 
	include G_SYSTEM."modules/api/lib/simple_html_dom.class.php";     //抓取类
	$dom = str_get_html($back_data);
	
	$table = $dom->find('#draw_list',0);
	$ssc_list = array();
	foreach($table->find('tr') as $tr){
	  $key = '';
	  $q = $tr->find('td',1);
	  $v = $tr->find('td',2);
	  $key = trim($q->plaintext);
	  $val = strip_tags($v->plaintext);
	  
	  $ssc_list[$key] = str_replace(' ','',$val);
	}
	
	if(isset($ssc_list[$ssc_phase])){
		return $ssc_list[$ssc_phase];
	}else{
		return '00000';
	}
}