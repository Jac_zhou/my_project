<?php
/*
* 微信API
* kangpengfei  20151211
*/

class WeiXin{
	
	public $appid;		//应用唯一标识

	public $secret;		//应用密钥AppSecret，在微信开放平台提交应用审核通过后获得>>获取access_token时用到
	
	public function codeURL(){return "https://open.weixin.qq.com/connect/qrconnect";}		//获取code 的链接
	
	public function wapCodeURL(){return "https://open.weixin.qq.com/connect/oauth2/authorize";}    //wap 获取code
	
	public function accessTokenURL(){return "https://api.weixin.qq.com/sns/oauth2/access_token";}	//获取access_token
	
	public function refreshTokenURL(){return "https://api.weixin.qq.com/sns/oauth2/refresh_token";}	//刷新access_token
	
	public function userInfoURL(){return "https://api.weixin.qq.com/sns/userinfo";}	//获取用户个人信息
	
	public function __construct($appid , $secret)
	{
		$this->appid = $appid;
		$this->secret = $secret;
	}
	
	//获取code的链接
	public function getCodeURL($redirect_uri, $state, $scope="snsapi_login", $response_type="code")
	{
		$params = array();
		$params['appid'] = $this->appid;
		$params['redirect_uri'] = $redirect_uri;
		$params['state'] = $state;
		$params['response_type'] = $response_type;
		$params['scope'] = $scope;
		return $this->codeURL() . "?" . http_build_query($params);
	}
	
	//wap获取code
	public function getWapCodeURL($redirect_uri, $state, $scope="snsapi_userinfo", $response_type="code")
	{
	    $params = array();
	    $params['appid'] = $this->appid;
	    $params['redirect_uri'] = $redirect_uri;
	    $params['response_type'] = $response_type;
	    $params['scope'] = $scope;
	    $params['state'] = $state;
	    return $this->wapCodeURL() . "?" . http_build_query($params) . "#wechat_redirect";
	}
	
	//获取access_token
	public function getAccessTokenURL($code)
	{
		$params = array();
		$params['appid'] = trim($this->appid);
		$params['secret'] = trim($this->secret);
		$params['code'] = trim($code);
		$params['grant_type'] = "authorization_code";
		$url = $this->accessTokenURL() . "?" . http_build_query($params);
		//$result = file_get_contents($url);
		$result = $this->curl($url);
		return json_decode($result, true);
	}
	public function curl($url){
		$ch = curl_init();
		curl_setopt($ch,CURLOPT_URL,$url);
		curl_setopt($ch,CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch,CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		$output = curl_exec($ch);
		curl_close($ch);
		return $output;
	}
	//刷新access_token
	public function getRefreshTokenURL($refresh_token)
	{
		$params = array();
		$params['appid'] = $this->appid;
		$params['grant_type'] = "refresh_token";
		$params['refresh_token'] = $refresh_token;
		$url = $this->refreshTokenURL() . "?" . http_build_query($params);
		$result = file_get_contents($url);
		return json_decode($result, true);
	}
	
	//拉取用户信息
	public function getUserInfoURL($access_token, $openid, $lang="zh_CN")
	{
		$params = array();
		$params['access_token'] = $access_token;
		$params['openid'] = $openid;
		$params['lang'] = $lang;
		$url = $this->userInfoURL() . "?" . http_build_query($params);
		$result =$this->curl($url);
		return json_decode($result, true);
	}
	
	
	
}