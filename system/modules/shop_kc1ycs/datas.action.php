<?php 
defined('G_IN_SYSTEM')or exit('no');
System::load_app_class('admin',G_ADMIN_DIR,'no');
System::load_app_fun('global',G_ADMIN_DIR);
System::load_sys_fun('user');
class datas extends admin {
	private $db;
	public function __construct(){		
		parent::__construct();		
		$this->db=System::load_sys_class('model');
	}
	
	//数据统计
	public function lists()
	{
		$this->ment=array(
			array("lists","数据统计",ROUTE_M.'/'.ROUTE_C."/lists"),
		);
		include $this->tpl(ROUTE_M,'datas.lists');
	}
	
	//渠道列表
	public function source()
	{
		$this->ment=array(
			array("source","渠道列表",ROUTE_M.'/'.ROUTE_C."/source"),
			array("add_source","添加渠道",ROUTE_M.'/'.ROUTE_C."/add_source"),
		);
		include $this->tpl(ROUTE_M,'datas.source');
	}
	
	//添加渠道
	public function add_source()
	{
		$this->ment=array(
			array("source","渠道列表",ROUTE_M.'/'.ROUTE_C."/source"),
			array("add_source","添加渠道",ROUTE_M.'/'.ROUTE_C."/add_source"),
		);
		include $this->tpl(ROUTE_M,'datas.add_source');
	}
	
}//
?>