<?php 


defined('G_IN_SYSTEM')or exit('no');
System::load_app_class('admin',G_ADMIN_DIR,'no');
System::load_sys_fun('user');
System::load_sys_fun('global');
class dingdan extends admin {
	private $db;
	public function __construct(){		
		parent::__construct();		
		$this->db=System::load_sys_class('model');		
		$this->ment=array(
						array("lists","订单列表",ROUTE_M.'/'.ROUTE_C."/lists"),					
						array("lists","中奖订单",ROUTE_M.'/'.ROUTE_C."/lists/zj"),					
						array("lists","已发货",ROUTE_M.'/'.ROUTE_C."/lists/sendok"),
						array("lists","未发货",ROUTE_M.'/'.ROUTE_C."/lists/notsend"),						
						array("insert","已完成",ROUTE_M.'/'.ROUTE_C."/lists/ok"),
						array("insert","已作废",ROUTE_M.'/'.ROUTE_C."/lists/del"),
						array("insert","待收货",ROUTE_M.'/'.ROUTE_C."/lists/shouhuo"),
						array("genzhong","<b>快递跟踪</b>",ROUTE_M.'/'.ROUTE_C."/genzhong"),
		);
	}
	
	public function genzhong(){	
		include $this->tpl(ROUTE_M,'dingdan.genzhong');	
	}
	public function lists(){	
		
		/*
			已付款,未发货,已完成
			未付款,已发货,已作废
			已付款,未发货,待收货
		*/
		$this_path = WEB_PATH."/".ROUTE_M."/".ROUTE_C."/export";
		$this_path_zhong = WEB_PATH."/".ROUTE_M."/".ROUTE_C."/export_zhongjiang";
		$where = $this->segment(4);
		if(!$where){
			$list_where = "where `status` LIKE  '%已付款%'";
		}elseif($where == 'zj'){
			//中奖		
			$list_where = "where `huode` != '0'";
		}elseif($where == 'sendok'){
			//已发货订单
			$list_where = "where `huode` != '0' and  `status` LIKE  '%已发货%'";
		}elseif($where == 'notsend'){
			//未发货订单
			$list_where = "where `huode` != '0' and `status` LIKE  '%未发货%'";
		}elseif($where == 'ok'){
			//已完成
			$list_where = "where `huode` != '0' and  `status` LIKE  '%已完成%'";
		}elseif($where == 'del'){
			//已作废		
			$list_where = "where `status` LIKE  '%已作废%'";
		}elseif($where == 'gaisend'){
			//该发货			
			$list_where = "where `huode` != '0' and `status` LIKE  '%未发货%'";
		}elseif($where == 'shouhuo'){
			//该发货			
			$list_where = "where `status` LIKE  '%待收货%'";
		}
		
		if(isset($_POST['paixu_submit'])){
			$paixu = $_POST['paixu'];
			if($paixu == 'time1'){
				$list_where.=" order by `time` DESC";
			}
			if($paixu == 'time2'){
				$list_where.=" order by `time` ASC";
			}
			if($paixu == 'num1'){
				$list_where.=" order by `gonumber` DESC";
			}
			if($paixu == 'num2'){
				$list_where.=" order by `gonumber` ASC";
			}
			if($paixu == 'money1'){
				$list_where.=" order by `moneycount` DESC";
			}
			if($paixu == 'money2'){
				$list_where.=" order by `moneycount` ASC";
			}
		
		}else{
			$list_where.=" order by `time` DESC";
			$paixu = 'time1';
		}
			
		$num=40;
	
		$total=$this->db->GetCount("SELECT COUNT(*) FROM `@#_member_go_record` $list_where");
		$page=System::load_sys_class('page');
		if(isset($_GET['p'])){$pagenum=$_GET['p'];}else{$pagenum=1;}	
		$page->config($total,$num,$pagenum,"0");
		$recordlist=$this->db->GetPage("SELECT * FROM `@#_member_go_record` $list_where",array("num"=>$num,"page"=>$pagenum,"type"=>1,"cache"=>0));	
		foreach($recordlist as $key=>$val){
			if(strpos($val['status'],"已发货") !== false){
				$recordlist[$key]['status'] = str_replace("已发货","<span style='color:red'>已发货</span>",$val['status']);
			}
		}
		
		include $this->tpl(ROUTE_M,'dingdan.list');	
	}
	
	//订单详细
	public function get_dingdan(){
		$code=abs(intval($this->segment(4)));
		$record=$this->db->GetOne("SELECT * FROM `@#_member_go_record` where `id`='$code'");
		if(!$record)_message("参数不正确!");
		$record['confrim_addr_time'] = date("Y-m-d H:i:s",$record['confrim_addr_time']);
		if(isset($_POST['submit'])){
			$confrim_addr = $record['confrim_addr'];
			$confrim_addr_time = $record['confrim_addr_time'];
			if(!$confrim_addr || !$confrim_addr_time){
				_message("客户还没有确认收货地址，请不要发货!");die;
			}
			$record_code =explode(",",$record['status']);
			$status = $_POST['status'];
			$company = $_POST['company'];
			if(!$company)_message("请填写快递公司的名称!");
			$company_code = $_POST['company_code'];
			if(!$company_code)_message("请填写订单号码!");
			$company_money = floatval($_POST['company_money']);
			$code = abs(intval($_POST['code']));
			if(!$company_money){
				$company_money = '0.01';
			}else{
				$company_money = sprintf("%.2f",$company_money);
			}
			
			$time = 0;
			if($status == '未完成'){
				$status = $record_code[0].','.$record_code[1].','.'未完成';		
			}
			if($status == '已发货'){
				$status = '已付款,已发货,待收货';
				$time = time();	//发货时间
			}
			if($status == '未发货'){
				$status = '已付款,未发货,未完成';
			}
			if($status == '已完成'){
				$status = '已付款,已发货,已完成';	
			}
			if($status == '已作废'){
				$status = $record_code[0].','.$record_code[1].','.'已作废';				
			}			
			
			$ret = $this->db->Query("UPDATE `@#_member_go_record` SET `status`='$status',`company` = '$company',`company_code` = '$company_code',`company_money` = '$company_money',`company_time` = '$time' where id='$code'");
			if($ret){
				if($status=='已付款,已发货,待收货'){
                    $member_info=$this->db->GetOne("SELECT mobile FROM `@#_member` where `uid`=".$record['uid']);
                    $express_info="快递公司:".$company.","."快递单号:".$company_code;
                    $content="手机号为".$member_info["mobile"]."的用户，您购买的 ".$record["shopname"]." 商品已经发货;".$express_info;  
                    if(_sendmobile($member_info["mobile"],$content))_message("更新成功");
				}
				_message("更新成功");
			}else{
				_message("更新失败");
			}
		}
		
		System::load_sys_fun("user");
		$uid= $record['uid'];
		$user = $this->db->GetOne("select * from `@#_member` where `uid` = '$uid'");
		$user_dizhi = $this->db->GetOne("select * from `@#_member_dizhi` where `uid` = '$uid' and `default` = 'Y'");
		$go_time = $record['time'];
		include $this->tpl(ROUTE_M,'dingdan.code');	
	}
	
	//订单搜索
	public function select(){
		$record = '';
		if(isset($_POST['codesubmit'])){
			$code = htmlspecialchars($_POST['text']);		
			$record = $this->db->GetList("SELECT * FROM `@#_member_go_record` where `code` = '$code'");			
		}
		if(isset($_POST['usersubmit'])){	
			if($_POST['user'] == 'uid'){
				$uid = intval($_POST['text']);
				$record = $this->db->GetList("SELECT * FROM `@#_member_go_record` where `uid` = '$uid'");	
			}
		}
		if(isset($_POST['shopsubmit'])){
			if($_POST['shop'] == 'sid'){
				$sid = intval($_POST['text']);
				$record = $this->db->GetList("SELECT * FROM `@#_member_go_record` where `shopid` = '$sid'");	
			}
			if($_POST['shop'] == 'sname'){
				$sname= htmlspecialchars($_POST['text']);
				$record = $this->db->GetList("SELECT * FROM `@#_member_go_record` where `shopname` = '$sname'");	
			}
		}
		
		if(isset($_POST['timesubmit'])){
				$start_time = strtotime($_POST['posttime1']) ? strtotime($_POST['posttime1']) : time();				
				$end_time   = strtotime($_POST['posttime2']) ? strtotime($_POST['posttime2']) : time();
				$record = $this->db->GetList("SELECT * FROM `@#_member_go_record` where `time` > '$start_time' and `time` < '$end_time'");				
		}
		
		
		include $this->tpl(ROUTE_M,'dingdan.soso');	
	}
	
	//导出未发货的（客户已经确认过收货地址的）
	public function export(){
		header("Content-type:application/vnd.ms-excel;");
		header("Content-Disposition:attachment;filename=未发货的订单（已确认收货地址）.xls");
		$tab="\t"; 
		$br="\n";
		$head="商品sid".$tab."物品名称".$tab."订单号".$tab."期数".$tab."确认地址".$tab."确认时间".$tab."确认用户信息".$br;
		
		$sql = "SELECT shopname,title,shopid,sid,code,status,shopqishu,confrim_addr,confrim_addr_time,confrim_addr_uinfo FROM `@#_member_go_record` AS m LEFT JOIN `@#_shoplist` AS s ON m.shopid=s.id WHERE m.huode !=0 AND `status` LIKE '%已付款,未发货%' AND confrim_addr !='' AND confrim_addr_time IS NOT NULL ORDER BY sid DESC";
		$arr_list = $this->db->GetList($sql);
		//echo "<pre>";print_r($arr_list);die;
		$num = count($arr_list);
		echo iconv("UTF-8","GBK",$head);
		for($i=0;$i<$num;$i++){
			echo iconv("UTF-8","GBK",$arr_list[$i]['sid']).$tab;
			echo iconv("UTF-8","GBK//IGNORE",$arr_list[$i]['title']).$tab;
			echo iconv("UTF-8","GBK",$arr_list[$i]['code']).$tab;
			echo iconv("UTF-8","GBK","第".$arr_list[$i]['shopqishu']."期").$tab;
			echo iconv("UTF-8","GBK//IGNORE",$arr_list[$i]['confrim_addr']).$tab;
			echo iconv("UTF-8","GBK",date("Y-m-d H:i:s",$arr_list[$i]['confrim_addr_time'])).$tab;
			echo iconv("UTF-8","GBK",$arr_list[$i]['confrim_addr_uinfo']).$tab;
			echo $br;
		}
		
	
	}
	
	//导出所有中奖的已确认收货地址的
	public function export_zhongjiang(){
		header("Content-type:application/vnd.ms-excel");
		header("Content-Disposition:attachment;filename=中奖订单.xls");
		$tab="\t"; 
		$br="\n";
		$start_time = (isset($_POST['start_time']) && !empty($_POST['start_time'])) ? trim($_POST['start_time']) : date('Y-m-d')."00:00:01";
		$end_time = (isset($_POST['end_time']) && !empty($_POST['end_time'])) ? trim($_POST['end_time']) : date('Y-m-d')."23:59:59";
		$start_time = strtotime($start_time);
		$end_time = strtotime($end_time);
		$con = " AND m.time >='$start_time' AND m.time<='$end_time' ";
		
		$head="商品sid".$tab."物品名称".$tab."期数".$tab."订单号".$tab."购买用户".$tab."收货人".$tab."收货电话".$tab."收货地址".$tab."总需人次".$tab."购买次数".$tab."确认时间".$tab."订单状态".$br;
		
		$sql = "SELECT m.*,s.sid,s.zongrenshu FROM `@#_member_go_record` AS m LEFT JOIN `@#_shoplist` AS s ON m.shopid=s.id WHERE huode!=0 AND `status` LIKE '%已付款%' AND confrim_addr!='' $con ORDER BY sid";
		$arr_list = $this->db->GetList($sql);
		$num = count($arr_list);
		echo iconv("UTF-8","GBK",$head);
		if($num>0){
			for($i=0;$i<$num;$i++){
				echo iconv("UTF-8","GBK",$arr_list[$i]['sid']).$tab;
				echo iconv("UTF-8","GBK//IGNORE",$arr_list[$i]['shopname']).$tab;
				echo iconv("UTF-8","GBK","第".$arr_list[$i]['shopqishu']."期").$tab;
				echo iconv("UTF-8","GBK",$arr_list[$i]['code']).$tab;
				echo iconv("UTF-8","GBK",$arr_list[$i]['username']).$tab;
				
				$shuohuoren_info = explode("|",$arr_list[$i]['confrim_addr_uinfo']);
				echo iconv("UTF-8","GBK",$shuohuoren_info[0]).$tab;
				echo iconv("UTF-8","GBK",$shuohuoren_info[1]).$tab;
				echo iconv("UTF-8","GBK//IGNORE",$arr_list[$i]['confrim_addr']).$tab;
				echo iconv("UTF-8","GBK",$arr_list[$i]['zongrenshu']).$tab;
				echo iconv("UTF-8","GBK",$arr_list[$i]['gonumber']).$tab;
				echo iconv("UTF-8","GBK",date("Y-m-d H:i:s",$arr_list[$i]['time'])).$tab;	
				echo iconv("UTF-8","GBK",$arr_list[$i]['status']).$tab;
				echo $br;
			}
		}else{
			echo iconv("UTF-8","GBK","暂无记录");
		}
	
	}
	
}
?>