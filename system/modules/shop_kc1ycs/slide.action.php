<?php 
defined('G_IN_SYSTEM')or exit('no');
System::load_app_class('admin','','no');
class slide extends admin {
	
	public function __construct(){
		parent::__construct();
		$this->db=System::load_sys_class('model');
		$this->ment=array(
						array("navigation","幻灯管理",ROUTE_M.'/'.ROUTE_C),
						array("navigation","添加幻灯片",ROUTE_M.'/'.ROUTE_C."/add"), 
		);
		
	}
	public function init(){
		$lists=$this->db->GetList("SELECT * FROM `@#_slide` where id>0 order by `sort_order` asc");		
		include $this->tpl(ROUTE_M,'slide_list');
	}
	
	public function add(){
		if(isset($_POST['submit'])){
			$title=htmlspecialchars(trim($_POST['title']));
			
			$link=htmlspecialchars(trim($_POST['link']));
			
			$arr = explode('/',$link);
			$slide_val = end($arr); 
			
			$slide_type = isset($_POST['slide_type']) ? intval($_POST['slide_type']) : 0;  //类型  0跳转网页  1商品详情  2关键词搜索  3商品列表
			if(strpos($link,'/s_tag/') !== false){
			    $slide_type = 2;  
			}elseif(strpos($link,'/goods/') !== false){
			    $slide_type = 1;
			}elseif(strpos($link,'/goods_list/') !== false){
			    $slide_type = 3;
			    $temp_val = explode('_',$slide_val);
			    $slide_val = $temp_val[0];   //商品分类id
			}else{
			    $slide_type = 0;
			    $slide_val = ''; 
			}
			
			$is_apple = isset($_POST['is_apple'])?intval($_POST['is_apple']):0;  //是否是苹果或三星产品
			$sort_order = isset($_POST['sort_order'])?intval($_POST['sort_order']):10;  //排序  越小越靠前
			
			if(isset($_POST['image'])){
				$img=$_POST['image'];
			}else{
				$img='';
			}
				 
			$this->db->Query("insert into `@#_slide`(`title`,`link`,`img`,`slide_type`,`slide_val`,`is_apple`,`sort_order`) values('$title','$link','$img','$slide_type','$slide_val','$is_apple','$sort_order') ");	
				if($this->db->affected_rows()){
						_message("添加成功",WEB_PATH.'/'.ROUTE_M.'/'.ROUTE_C."/init");
				}else{
						_message("添加失败");
				}
		}
		include $this->tpl(ROUTE_M,'slide_add');
	}
	
	public function delete(){
		$id=intval($this->segment(4)); 
			$this->db->Query("DELETE FROM `@#_slide` WHERE (`id`='$id')");
			if($this->db->affected_rows()){
			
					_message("删除成功",WEB_PATH.'/'.ROUTE_M.'/'.ROUTE_C."/init");
			}else{
					_message("删除失败");
			}  
	}
	
	public function update(){
		$id=intval($this->segment(4));
		$slideone=$this->db->Getone("SELECT * FROM `@#_slide` where `id`='$id'");	
		
		if(isset($_POST['submit'])){
			$title=htmlspecialchars(trim($_POST['title']));	
			$link=htmlspecialchars(trim($_POST['link']));
			
			$arr = explode('/',$link);
			$slide_val = end($arr);
				
			$slide_type = isset($_POST['slide_type']) ? intval($_POST['slide_type']) : 0;  //类型  0跳转网页  1商品详情  2关键词搜索 3商品列表
			if(strpos($link,'/s_tag/') !== false){
			    $slide_type = 2;
			}elseif(strpos($link,'/goods/') !== false){
			    $slide_type = 1;
			}elseif(strpos($link,'/goods_list/') !== false){
			    $slide_type = 3;
			    $temp_val = explode('_',$slide_val);
			    $slide_val = $temp_val[0];   //商品分类id
			}else{
			    $slide_type = 0;
			    $slide_val = ''; 
			}
			
			$is_apple = isset($_POST['is_apple'])?intval($_POST['is_apple']):0;  //是否是苹果或三星产品
			$sort_order = isset($_POST['sort_order'])?intval($_POST['sort_order']):10;  //排序  越小越靠前
			
			if(isset($_POST['image'])){
				$img=$_POST['image'];
			}else{
				$img=$slideone['img'];
			}		
			$this->db->Query("UPDATE `@#_slide` SET `sort_order`='$sort_order', `is_apple`='$is_apple', `img`='$img',`title`='$title',`slide_type`='$slide_type',`slide_val`='$slide_val',`link`='$link' WHERE `id`=$id");
			if($this->db->affected_rows()){
					_message("修改成功",WEB_PATH.'/'.ROUTE_M.'/'.ROUTE_C."/init");
			}else{
					_message("修改失败");
			}
		}
		include $this->tpl(ROUTE_M,'slide_update');
	}
	
	
}



?>