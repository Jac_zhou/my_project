<?php 
defined('G_IN_SYSTEM')or exit('no');
System::load_app_class('admin',G_ADMIN_DIR,'no');
System::load_app_fun('global',G_ADMIN_DIR);
System::load_app_fun('my','go');
System::load_sys_fun('user');
class auto_shopmanage extends admin {
	private $db;
	public function __construct(){		
		parent::__construct();		
		$this->db=System::load_app_model('admin_model');
	}
	
	public function lists(){
		$page = System::load_sys_class("page");
		$num = 20;
		$total = $this->db->GetCount("SELECT id from `@#_shoplist` WHERE zdrange!='' ");
		if(isset($_GET['p'])){
			$pagenum = intval($_GET['p']);
		}else{
			$pagenum = 1;
		}
		$page->config($total,$num,$pagenum);
		$sql = "SELECT * FROM `@#_shoplist` WHERE zdrange!='' ORDER BY q_uid asc,q_end_time asc";
		$arr_list = $this->db->GetPage($sql,array('key'=>'id','num'=>$num,"page"=>$pagenum));
		foreach($arr_list as $key=>$val){
			$arr_zi = $this->zidong_zong($val['id'],$val['zongrenshu']);
			$arr_list[$key]['auto_go'] = $arr_zi['auto_count']?:0;
			$arr_list[$key]['noauto_count'] = $this->not_zidong_zong($val['id'],$val['zongrenshu']);
			$arr_list[$key]['z_uid'] = $val['q_uid'];
			$arr_list[$key]['username'] = get_user_name($val['q_uid']);
			$arr_list[$key]['jiexiao_time'] = $val['q_end_time'] ? microt($val['q_end_time']) : '';
			unset($arr_zhong,$arr_zi);
		}
		
		include $this->tpl(ROUTE_M,'auto_shopmanage.list');
	}
	
	/*自动够买的总次数*/
	private function zidong_zong($shopid='',$zongrenci=''){
		if(!$shopid || !$zongrenci){
			return false;
		}
		$sql = "SELECT SUM(gonumber) AS auto_count FROM `@#_member_go_record` AS m LEFT JOIN `@#_member` AS mem ON m.uid=mem.uid WHERE m.shopid='$shopid' AND mem.auto_user=1";
		$arr = $this->db->GetOne($sql);
		$arr['noauto_count'] = $arr['auto_count'] ? $zongrenci - $arr['auto_count'] : 0;
		return $arr;
	}
	
	//真实购买总数
	private function not_zidong_zong($shopid='',$zongrenci=''){
	    if(!$shopid || !$zongrenci){
	        return false;
	    }
	    $sql = "SELECT SUM(gonumber) AS total FROM `@#_member_go_record` AS m LEFT JOIN `@#_member` AS mem ON m.uid=mem.uid WHERE m.shopid='$shopid' AND mem.auto_user=0";
	    $arr = $this->db->GetOne($sql);
	    $total = $arr['total'] ? $arr['total'] : 0;
	    return $total;
	}
	
	/*该件商品的获奖信息*/
	private function shop_winning($shopid=''){
		if(!$shopid){
			return false;
		}
		$sql = "SELECT uid AS z_uid,username,zdrange FROM `@#_member_go_record` WHERE shopid='$shopid' AND huode !=0";
		$arr = $this->db->GetOne($sql);
		return $arr;
	}
}
?>