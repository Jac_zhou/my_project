<?php

defined('G_IN_SYSTEM')or exit('No permission resources.');
System::load_app_class('admin',G_ADMIN_DIR,'no');
class search extends admin {
	private $db;
	public function __construct(){
		parent::__construct();
		$this->db=System::load_sys_class("model");
		$this->ment=array(
				array("words","热门搜索词",ROUTE_M.'/'.ROUTE_C."/web_words"),
		);	
	}
	
	//热门搜索
	public function web_words(){
		if(isset($_POST['submit'])){
			$web_words = isset($_POST['web_words']) ? htmlspecialchars(trim($_POST['web_words'])) : '';
			$web_words = str_replace('，',',',$web_words);
			$html=<<<HTML
<?php 
	return '{$web_words}';	//热门搜索 关键词
?>
HTML;
			if(!is_writable(G_CONFIG.'web_words.inc.php')) _message('Please chmod  email  to 0777 !');
			$ok=file_put_contents(G_CONFIG.'web_words.inc.php',$html);
			if($ok){
				_message("操作成功");
			}
		}
		
		$words = System::load_sys_config("web_words");	
		$words = str_replace("，",',',$words);
		include $this->tpl(ROUTE_M,'web_words');
	}
	
	
}
?>