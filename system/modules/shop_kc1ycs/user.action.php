<?php

defined('G_IN_SYSTEM') or exit('no');
System::load_app_class('admin', G_ADMIN_DIR, 'no');

class user extends admin
{
    protected $ment;
    private $db;

    public function __construct()
    {

        $this->db = System::load_sys_class("model");
        $this->ment = array(
            array("lists", "管理员管理", ROUTE_M . '/' . ROUTE_C . "/lists"),
            array("reg", "添加管理员", ROUTE_M . '/' . ROUTE_C . "/reg"),
            array("edit", "修改管理员", ROUTE_M . '/' . ROUTE_C . "/reg", 'hide'),
            array("group_list", "角色管理", ROUTE_M . '/' . ROUTE_C . "/group_list"),
            array("add_group", "添加角色", ROUTE_M . '/' . ROUTE_C . "/add_group"),
        );

    }

    public function reg()
    {
        if (isset($_POST['submit-1'])) {
            $username = safe_replace($_POST['username']);
            if ($username != $_POST['username'] || empty($username)) {
                _message("用户名格式错误!");
            }
            if (_strlen($username) > 15) {
                _message("用户名长度为2-15个字符,1个汉字等于2个字符!");
            }
            $password1 = trim($_POST['password']);
            $password2 = trim($_POST['pwdconfirm']);
            if (empty($password2) || ($password1 != $password2)) {
                _message("2次密码不一致!");
            }
            if (!_checkemail($_POST['email'])) {
                _message("邮箱格式错误!");
            }
            $pmid = isset($_POST['mid']) ? intval($_POST['mid']) : 0;
            $password = md5(md5($password2));
            $addtime = time();
            $ip = _get_ip();
            $this->db->Query("INSERT INTO `@#_admin` (`mid`, `username`, `userpass`, `useremail`, `addtime`, `logintime`, `loginip`) VALUES ('$pmid', '$username', '$password', '$_POST[email]','$addtime','0','$ip')");
            if ($this->db->affected_rows()) {
                $path = WEB_PATH . '/' . ROUTE_M . '/user/lists';
                _message("添加管理员成功!", $path);
            } else {
                _message("添加管理员失败!");
            }
        }

        $group_list = $this->db->GetList("SELECT * FROM @#_admin_group WHERE `status`=1 ");

        include $this->tpl(ROUTE_M, 'user.reg');


    }

    public function lists()
    {
        $num = 50;
        $total = $this->db->GetCount("SELECT COUNT(*) FROM `@#_admin` WHERE 1");
        $page = System::load_sys_class('page');
        if (isset($_GET['p'])) {
            $pagenum = $_GET['p'];
        } else {
            $pagenum = 1;
        }
        $page->config($total, $num, $pagenum, "0");
        $AdminList = $this->db->GetPage("SELECT m.*,g.name FROM `@#_admin` AS m LEFT JOIN `@#_admin_group` AS g ON g.mid = m.mid WHERE 1", array("num" => $num, "page" => $pagenum, "type" => 1, "cache" => 0));
        include $this->tpl(ROUTE_M, 'user.list');
    }

    public function edit()
    {
        $path = WEB_PATH . '/' . ROUTE_M . '/user/lists';
        $uid = intval($this->segment(4));
        $info = $this->db->GetOne("SELECT * FROM `@#_admin` WHERE `uid`='$uid'");
        if (!is_array($info)) {
            _message("没有这个用户!", $path);
        }
        if (isset($_POST['submit-1'])) {
            $password1 = trim($_POST['password']);
            $password2 = trim($_POST['pwdconfirm']);
            $mid = $_POST['mid'];
            if (empty($password2) || ($password1 != $password2)) {
                _message("2次密码不一致!");
            }

            $password = md5(md5($password2));
            $ok = $this->db->Query("UPDATE `@#_admin` SET `mid`='$mid', `userpass`='$password' WHERE (`uid`='$uid')");
            if ($ok) {
                _message("修改成功!", G_MODULE_PATH . '/user/lists');
            } else {
                _message("修改失败!");
            }

        } else {
            $group_list = $this->db->GetList("SELECT * FROM @#_admin_group WHERE `status`=1 ");

            include $this->tpl(ROUTE_M, 'user.edit');
        }
    }

    public function del()
    {

        if (isset($_POST['ajax'])) {
            $uid = intval($this->segment(4));
            if ($uid == 1) {
                echo 0;
                return;
            }
            //如果是超级管理员 则不允许删除
            $admins = $this->db->GetOne("SELECT mid FROM @#_admin_group WHERE `uid`='$uid' LIMIT 1");
            if ($admins['mid'] == 1) {
                echo 0;
                return;
            }

            $path = WEB_PATH . '/' . ROUTE_M . '/user/lists';
            $this->db->Query("DELETE FROM `@#_admin` WHERE (`uid`='$uid')");
            if ($this->db->affected_rows())
                echo $path;
            else
                echo 'no';
            exit;
        }
    }


    public function login(){
        $check = $this->CheckAdminInfo();
        if($check){
            header('Location:'.WEB_PATH.DIRECTORY_SEPARATOR.ROUTE_M.DIRECTORY_SEPARATOR.'index');
            die;
        }
        if (isset($_POST['ajax'])) {
            $location = WEB_PATH . '/' . ROUTE_M . '/index';
            $message = array("error" => false, 'text' => $location);
            $username = $_POST['username'];
            $password = $_POST['password'];
            $code = strtoupper($_POST['code']);
            if (empty($username)) {
                $message['error'] = true;
                $message['text'] = "请输入用户名!";
                echo json_encode($message);
                exit;
            }
            if (empty($password)) {
                $message['error'] = true;
                $message['text'] = "请输入密码!";
                echo json_encode($message);
                exit;
            }

            if (_cfg("web_off")) {
                if (empty($code)) {
                    $message['error'] = true;
                    $message['text'] = "请输入验证码!";
                    echo json_encode($message);
                    exit;
                }
                if (md5($code) != _getcookie('checkcode')) {
                    $message['error'] = true;
                    $message['text'] = "验证码输入错误";
                    echo json_encode($message);
                    exit;
                }
            }

            $info = $this->db->GetOne("SELECT * FROM `@#_admin` WHERE `username` = '$username' LIMIT 1");
            if (!$info) {
                $message['error'] = true;
                $message['text'] = "登录失败,请检查用户名或密码!";
                echo json_encode($message);
                exit;
            }

            if ($info['userpass'] != md5(md5($password))) {
            // if ($info['userpass'] != md5($password)) {
                $message['error'] = true;
                $message['text'] = "登陆失败!";
                echo json_encode($message);
                exit;
            }

            if (!$message['error']) {
                //后台管理员登录有效期24小时
                _setcookie("AID", _encrypt($info['uid'], 'ENCODE'),24 * 60 * 60, '/');
                _setcookie("ASHELL", _encrypt(md5($info['username'] . $info['userpass']) . md5($_SERVER['HTTP_USER_AGENT'])),24 * 60 * 60, '/');

                /* 权限  20160105 start */
                $auth_info = $this->db->GetOne("SELECT * FROM @#_admin_group WHERE `mid`='$info[mid]' ");
                $auth = base64_encode($auth_info['auth']);
                _setcookie("AUTH", _encrypt($auth, 'ENCODE'),24 * 60 * 60, '/');
                _setcookie("MID", _encrypt($info[mid], 'ENCODE'),24 * 60 * 60, '/');
                /* 权限  20160105 end */

                $this->AdminInfo = $info;
                $time = time();
                $ip = _get_ip();
                $this->db->Query("UPDATE `@#_admin` SET `logintime`='$time' WHERE (`uid`='$info[uid]')");
                $this->db->Query("UPDATE `@#_admin` SET `loginip`='$ip' WHERE (`uid`='$info[uid]')");
            }
            echo json_encode($message);
            exit;
        } else {
            include $this->tpl(ROUTE_M, 'user.login');
        }
    }

    public function out()
    {
        _setcookie("AID", '');
        _setcookie("ASHELL", '');
        _setcookie("AUTH", '');
        _message("退出成功", G_MODULE_PATH . '/user/login');
    }

    //角色管理
    public function group_list()
    {
        $num = 50;
        $total = $this->db->GetCount("SELECT COUNT(*) FROM `@#_admin_group` WHERE 1");
        $page = System::load_sys_class('page');
        if (isset($_GET['p'])) {
            $pagenum = $_GET['p'];
        } else {
            $pagenum = 1;
        }
        $page->config($total, $num, $pagenum, "0");
        $group_list = $this->db->GetPage("SELECT * FROM `@#_admin_group` WHERE 1", array("num" => $num, "page" => $pagenum, "type" => 1, "cache" => 0));

        $auth_temp = $this->db->GetList("SELECT * FROm @#_auth_list WHERE 1");
        foreach ($auth_temp as $k => $v) {
            $auth['code'][] = $v['code_one'];
            $auth['name'][] = $v['name'];
        }

        foreach ($group_list as $k => $v) {
            $admin_list = $this->db->GetList("SELECT uid,username FROM @#_admin WHERE `mid`='$v[mid]'");
            $group_list[$k]['admin'] = $admin_list;
            $group_list[$k]['auth'] = str_replace($auth['code'], $auth['name'], $v['auth']);
        }


        include $this->tpl(ROUTE_M, 'user.group_list');
    }

    //添加角色
    public function add_group()
    {
        if (isset($_POST['submit-1'])) {
            $name = isset($_POST['name']) ? trim($_POST['name']) : '';
            $auth = isset($_POST['auth']) ? $_POST['auth'] : '';
            if (!$name) {
                _message("名称不能为空!");
            }
            if (!$auth) {
                _message("请至少分配一个权限!");
            }

            $auth = implode(',', $auth);

            $is_only = $this->db->GetCount("SELECT `mid` FROM @#_admin_group WHERE `name`='$name'");
            if ($is_only) {
                _message("该名称已存在，请更换!");
            }

            $ok = $this->db->Query("INSERT INTO @#_admin_group(name,auth) VALUES('$name','$auth')");
            if ($ok) {
                _message("添加成功!", G_MODULE_PATH . '/user/group_list');
            } else {
                _message("添加失败!");
            }

        } else {
            $auth_list = $this->db->GetList("SELECT * FROm @#_auth_list WHERE `pid`=0");
            include $this->tpl(ROUTE_M, 'user.add_group');
        }
    }

    //修改角色
    public function edit_group()
    {
        $mid = intval($this->segment(4));
        if (isset($_POST['submit-1'])) {
            $name = isset($_POST['name']) ? trim($_POST['name']) : '';
            $auth = isset($_POST['auth']) ? $_POST['auth'] : '';
            $mid = isset($_POST['mid']) ? $_POST['mid'] : '';
            if (!$mid) {
                _message("参数错误!");
            }
            if (!$name) {
                _message("名称不能为空!");
            }
            if (!$auth) {
                _message("请至少分配一个权限!");
            }

            $auth = implode(',', $auth);

            $is_only = $this->db->GetCount("SELECT `mid` FROM @#_admin_group WHERE `name`='$name' AND `mid` != '$mid'");
            if ($is_only) {
                _message("该名称已存在，请更换!");
            }

            $ok = $this->db->Query("UPDATE @#_admin_group SET `name` = '$name' , `auth`='$auth' WHERE `mid`=$mid");
            if ($ok) {
                _message("修改成功!", G_MODULE_PATH . '/user/group_list');
            } else {
                _message("修改失败!");
            }

        } elseif ($mid) {
            $group = $this->db->GetOne("SELECT * FROm @#_admin_group WHERE `mid`=$mid");
            if (!$group) {
                _message("记录不存在!");
            }

            $group['auth_arr'] = explode(',', $group['auth']);

            $auth_list = $this->db->GetList("SELECT * FROm @#_auth_list WHERE `pid`=0");
            include $this->tpl(ROUTE_M, 'user.edit_group');
        } else {
            _message("参数错误!");
        }
    }

    //删除角色
    public function del_group()
    {
        $mid = intval($this->segment(4));
        if (!$mid) {
            _message("参数错误!");
        }
        $is_mid = $this->db->GetCount("SELECT mid FROM @#_admin_group WHERE `mid`=$mid ");
        if (!$is_mid) {
            _message("记录不存在!");
        }

        $admin_num = $this->db->GetCount("SELECT uid FROM @#_admin WHERE `mid`=$mid ");
        if ($admin_num) {
            _message("该角色组下有用户，所以不允许直接删除!");
        }

        $ok = $this->db->Query("DELETE FROM @#_admin_group WHERE `mid` = $mid");
        if ($ok) {
            _message("删除成功!", G_MODULE_PATH . '/user/group_list');
        } else {
            _message("删除失败!");
        }
    }

}

?>