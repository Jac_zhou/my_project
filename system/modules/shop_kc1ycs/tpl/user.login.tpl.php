<?php defined('G_IN_ADMIN')or exit('No permission resources.'); ?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>云购系统CMS后台登陆</title>
<link rel="stylesheet" href="<?php echo G_GLOBAL_STYLE; ?>/global/css/global.css" type="text/css">
<link rel="stylesheet" href="<?php echo G_GLOBAL_STYLE; ?>/global/css/login.css" type="text/css">
<STYLE type=text/css>BODY {
	PADDING-BOTTOM: 0px; PADDING-LEFT: 0px; WIDTH: 100%; PADDING-RIGHT: 0px; BACKGROUND: #666666 fixed no-repeat center top; PADDING-TOP: 0px; background-clip: border-box; background-size: cover; background-origin: padding-box
}
</STYLE>

</head>


<!-- style="background-image: url(&quot;/plugin/images/bg_5.jpg&quot;);" -->
<body >
<div class="bg-dot"></div>
<div class="login-layout">
<div class="top">
<h5><em></em></h5>
<h2>云购系统CMS后台登陆</h2>
</div>
<div class="box">
 <form action="#" method="post" id="form">
<span><label>账号</label> <input type="text" id="input-u" name="username" style="color:#f17564;" class="input-text" value="请输入用户名" onfocus="javascript:if(this.value=='请输入用户名')this.value='';"> </span><span>
<label>密码</label> 
<input type="password" id="input-p" name="password" style="color:#8ccfb3;" class="input-password" value="********" onfocus="javascript:if(this.value=='********')this.value='';"> </span><span>
<div class="code">
	<div class="arrow"></div>
	<div class="code-img"><img id="codeimage" border="0" name="codeimage" src="/api/checkcode/image/80_27/"></div>
	<a id="hide" class="close" title="关闭" href="javascript:void(0);"><i></i></a>
</div>
<input type="text" id="captcha" name="code" style="color:#9dcc5a;width:60px;text-transform:uppercase;" class="input-code" value="code" onfocus="javascript:if(this.value=='code')this.value='';"> </span><span>


<input type="button" id="form_but" class="input-button" value="登录">
</span></form></div></div>
<div class="bottom"></div>
<script src="/statics/plugin/style/global/js/jquery-1.8.3.min.js"></script>
<script src="/statics/plugin/style/global/js/global.js"></script>
<script src="/statics/plugin/layer/layer.min.js"></script>

<script type="text/javascript">
$(document).ready(function(){
    //Random background image
    var random_bg=Math.floor(Math.random()*5+1);
    var bg='url(/statics/plugin/style/images/login/login_bg.png)';
    $("body").css("background-image",bg);
    //Hide Show verification code
    $("#hide").click(function(){
        $(".code").fadeOut("slow");
    });
    $("#captcha").focus(function(){
        $(".code").fadeIn("fast");
    });
    //跳出框架在主窗口登录
   if(top.location!=this.location)	top.location=this.location;
    $('#user_name').focus();
    if ($.browser.msie && $.browser.version=="6.0"){
        window.location.href='/admin/admin/templates/default/ie6update.html';
    }
    $("#captcha").nc_placeholder();
});
</script>
<script type="text/javascript">
var loading;
var form_but;
window.onload=function(){
	
	 document.onkeydown=function(){
		if(event.keyCode == 13){
             ajaxsubmit();
        }          
	}
	form_but=document.getElementById('form_but');
	form_but.onclick=ajaxsubmit;	
		var checkcode=document.getElementById('checkcode');	
	checkcode.src = checkcode.src + new Date().getTime();	
	var src=checkcode.src;
		checkcode.onclick=function(){
				this.src=src+'/'+new Date().getTime();
	}
   		
}

$(document).ready(function(){$.focusblur("#input-u");$.focusblur("#input-p");$.focusblur("#input-c");});

function ajaxsubmit(){
		var name=document.getElementById('form').username.value;
		var pass=document.getElementById('form').password.value;
			var codes=document.getElementById('form').code.value;
	    //document.getElementById('form').submit();
		$.ajaxSetup({
			async : false
		});				
		$.ajax({
			   "url":window.location.href,
			   "type": "POST",
			   "data": ({username:name,password:pass,code:codes,ajax:true}),
			   "beforeSend":beforeSend, //添加loading信息
			   "success":success//清掉loading信息
		});
	
}
function beforeSend(){
	 form_but.value="登录中...";
	 loading=$.layer({
		type : 3,
		time : 0,
		shade : [0.5 , '#000' , true],
		border : [5 , 0.5 , '#7298a6', true],
		loading : {type : 4}
	});
}

function success(data){
	layer.close(loading);
	form_but.value="登录";
	var obj = jQuery.parseJSON(data);
	if(!obj.error){	
		//console.log(obj);return false;
		window.location.href=obj.text;
	}else{
		$.layer({
			type :0,
			area : ['auto','auto'],
			title : ['信息',true],
			border : [5 , 0.5 , '#7298a6', true],
			dialog:{msg:obj.text}
		});
		var checkcode=document.getElementById('checkcode');
		var src=checkcode.src;
			checkcode.src='';
			checkcode.src=src;
		}
}
</script>

</body>




</html> 
