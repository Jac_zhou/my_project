<?php defined('G_IN_ADMIN')or exit('No permission resources.'); ?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>后台首页</title>
<link rel="stylesheet" href="<?php echo G_GLOBAL_STYLE; ?>/global/css/global.css" type="text/css">
<link rel="stylesheet" href="<?php echo G_GLOBAL_STYLE; ?>/global/css/style.css" type="text/css">
<script src="<?php echo G_GLOBAL_STYLE; ?>/global/js/jquery-1.8.3.min.js"></script>
<script src="<?php echo G_PLUGIN_PATH; ?>/uploadify/api-uploadify.js" type="text/javascript"></script>
</head>
<body>

<div class="header lr10">
	<?php echo $this->headerment();?>
</div>
<div class="bk10"></div>
<div class="table-form lr10">
<form action="" method="post" enctype="multipart/form-data" onsubmit="return checkData()" id="myform">
<table cellpadding="2" cellspacing="1" class="table_form" width="100%">
	<!--<tr>
		<th width="100">介绍：</th>
		<td>该广告位是指：用户打开首页时，正中间弹出一个窗口用来显示一张广告图。</td>
	</tr>-->
	<tr style="display:none;">
		<th width="100">　　标题：</th>
		<td><input type="text" name="title" id="title" size="30" value="<?php echo $data['title']?>" class="input-text"></td>
	</tr>
	 <tr>
         <th width="100">广告图片：</th>
        <td>
           	<input type="text" id="imagetext" name="pics" value="<?php echo $data['pics']?>" class="input-text wid300">
			<input type="button" class="button"
             onClick="GetUploadify('<?php echo WEB_PATH; ?>','uploadify','缩略图上传','image','advimg',1,500000,'imagetext');show_pic();" 
             value="上传图片" />  <span id="pic_tips" style="color:red;">(必填项)</span>
			
        </td>
      </tr>
	  <tr>
		<th></th>
		<td><img id="showpic" style="height:180px;" src="<?php echo G_UPLOAD_PATH . '/' . $data['pics']; ?>"/></td>
	  </tr>
	  <tr>
		<th width="100">窗口大小：</th>
		<td>宽: <input type="text" name="width" id="width" value="<?php echo $data['width']?>" class="input-text wid40">px 　
		高: <input type="text" name="height" id="height" value="<?php echo $data['height']?>" class="input-text wid40">px　(最好是根据图片大小设置,默认宽1200px,高500px)</td>
	</tr>
	<tr>
		<th width="100">跳转地址：</th>
		<td><input type="text" name="link" id="link" value="<?php echo $data['link']?>" class="input-text wid300">　(如果不需要跳转,请留空)</td>
	</tr>
	<tr>
		<th width="100">显示时长：</th>
		<td><input type="text" name="times" id="times" value="<?php echo $data['times']?>" class="input-text wid30"> 秒</td>
	</tr>
	<tr>
		<th width="100">显示次数：</th>
		<td><input name="show_type" type="radio"　<?php if($data['show_type']==1){  ?> checked <?php  } ?> value="1" >&nbsp;每次刷新都显示
		&nbsp;&nbsp;<input name="show_type" type="radio" value="0" <?php if($data['show_type']!=1){  ?> checked <?php  } ?>>&nbsp;打开只显示一次
			&nbsp;&nbsp;<input name="show_type" type="radio" value="2" <?php if($data['show_type']!=1){  ?> checked <?php  } ?>>&nbsp;每天只显示一次</td>
	</tr>
	<tr>
		<th>是否启用：</th>
		<td><input name="is_open" type="radio"　<?php if($data['is_open']==1){  ?> checked <?php  } ?> value="1" >&nbsp;是&nbsp;&nbsp;<input name="is_open" type="radio" value="0" <?php if($data['is_open']!=1){  ?> checked <?php  } ?>>&nbsp;否</td>
	</tr>
	<tr>
		<th></th>
		<td><input type="submit" name="dosubmit" id="submit" value=" 保存 "></td>
	</tr>
</table>
</form>
</div>
<script type="text/javascript">
	var path = '<?php echo G_UPLOAD_PATH; ?>';
	show_pic();
	function show_pic(){
		var src = $('#imagetext').val();
		var oldsrc = $('#showpic').attr('src');
		totime = setTimeout("show_pic()",1000);
		if(src && src != oldsrc){
			$('#showpic').attr('src',path+'/'+src);
		}
	}
	//验证是否上传图片
	function checkData()
	{
		var src = $('#imagetext').val();
		if( ! src){
			$('#pic_tips').html("(请上传一张图片)");
			$('#imagetext').focus();
			return false;
		}
	}
</script>
</body>
</html> 