<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>一键应急</title>
		<link rel="stylesheet" href="<?php echo G_GLOBAL_STYLE; ?>/global/css/global.css" type="text/css">
		<style type="text/css">
			.list_table th,.list_table td{
				border: 1px solid rgb(222,222,222);
				padding:10px;
			}
			.list_table tr:nth-child(even){
				background: rgb(211,232,242);
			}
		</style>
	</head>
	<body>
		<table border="0" style="width:100%;font-size:14px;text-align: center;" class="list_table">
			<tr style="text-align: left;font-size:14px;">
				<td colspan="3"><font color="red" size="4">异常</font>：揭晓时间已到，一直无法揭晓</td>
			</tr>
			<tr>
				<th>id</th>
				<th>商品标题</th>
				<th>查看</th>
			</tr>
			<?php
			if(count($goodlist)>0){
				foreach($goodlist as $value){
				?>
			<tr>
				<td><?php echo $value['id'];?></td>
				<td><?php echo $value['title']?></td>
				<td><a href="<?php echo WEB_PATH;?>/goods/<?php echo $value['id'];?>" style="color:rgb(42,139,187);">查看</a></td>
			</tr>
			<?php
				}
			}else{
				?>
			<tr style="text-align: center;font-size:20px;">
				<td colspan="3">暂时没有异常状态的商品!</td>
			</tr>	
				
			<?php
				}
				?>
		</table>
		<?php if(isset($ids)){?>
		<form action="" method="post">
			<input type="hidden" value="<?php echo $ids;?>" name="ids">
			<input type="submit" value="一键开奖" name="submitcode"/>
		</form>
		<?php }?>
	</body>
</html>
