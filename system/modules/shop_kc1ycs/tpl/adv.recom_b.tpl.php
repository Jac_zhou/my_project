<?php defined('G_IN_ADMIN')or exit('No permission resources.'); ?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>后台首页</title>
<link rel="stylesheet" href="<?php echo G_GLOBAL_STYLE; ?>/global/css/global.css" type="text/css">
<link rel="stylesheet" href="<?php echo G_GLOBAL_STYLE; ?>/global/css/style.css" type="text/css">
<script src="<?php echo G_GLOBAL_STYLE; ?>/global/js/jquery-1.8.3.min.js"></script>
<script src="<?php echo G_PLUGIN_PATH; ?>/uploadify/api-uploadify.js" type="text/javascript"></script>
</head>
<body>

<div class="header lr10">
	<?php echo $this->headerment();?>
</div>
<div class="bk10"></div>
<div class="table-form lr10">
<form action="" method="post" enctype="multipart/form-data" id="myform" onsubmit="return checkData()">
<table cellpadding="2" cellspacing="1" class="table_form" width="100%">
	<tr>
		<th width="100">查询商品：</th>
		<td id="param"><span style="color:red;">通过</span> 商品ID:<input type="text" name="shopid" class="input-text wid100"/>　
		<span style="color:red;">或</span> 商品名称:<input type="text" name="shopname" class="input-text wid200"/>
		　<input type="button" id="showBtn"  value="查询"/>
		</td>
	</tr>
	<tr>
		<th width="100">查询结果：</th>
		<td><select name="shopid_select" id="shopid" class="wid400" onchange="do_get_info()">
			<option value="0">请先根据条件查询...</option>
		</select>　<span id="showTips"></span></td>
	</tr>
	<style>
		#shopinfo dl{width:210px;height:170px;border:1px solid #ccc;float:left;margin-right:20px;overflow:hidden;position:relative;}
		#shopinfo dl dt img{width:200px;height:150px;}
		#shopinfo dl dd{text-align:center;}
		#shopinfo .cancel{position:absolute;right:2px;top:2px;width:25px;height:25px;line-height:25px;background:#ccc;cursor:pointer;}
	</style>
	<tr>
		<th width="100">当前推荐：</th>
		<td id="shopinfo">
		<?php 
			if(count($recom_b['list'])){
				foreach($recom_b['list'] as $k=>$v){
		?>
			<dl>
				<dt><img src="<?php echo $v['thumb'] ?>"/></dt>
				<dd>商品编号:<?php echo $v['sid'] ?>　总需:<?php echo $v['zong'] ?>人次</dd>
				<dd class="cancel" title="点击后将会取消推荐！">X</dd>
				<dd>
					<input type="hidden" name="id[<?php echo $v['sid'] ?>]" value="<?php echo $v['id'] ?>"/>
					<input type="hidden" name="sid[<?php echo $v['sid'] ?>]" value="<?php echo $v['sid'] ?>"/>
					<input type="hidden" name="zong[<?php echo $v['sid'] ?>]" value="<?php echo $v['zong'] ?>"/>
					<input type="hidden" name="thumb[<?php echo $v['sid'] ?>]" value="<?php echo $v['thumb'] ?>"/>
				</dd>
			</dl>
		<?php 
				}
			}	
		?>
		</td>
	</tr>
	<tr>
		<th>是否启用：</th>
		<td><input name="is_open" type="radio" value="1" <?php if($recom_b['is_open']){ ?> checked <?php } ?> >&nbsp;是&nbsp;&nbsp;
		<input name="is_open" type="radio" value="0" <?php if(!$recom_b['is_open']){ ?> checked <?php } ?>>&nbsp;否
			　(如果选择否的话，则自动从所有商品中选三个展示)
		</td>
	</tr>
	<tr>
		<th></th>
		<td><input type="submit" name="dosubmit" id="submit" value=" 保存 "></td>
	</tr>
</table>
</form>
</div>
<script type="text/javascript">
var info = {};		//商品的信息
var default_sid = "<?php echo $recom_a['sid'] ?>";
var url = '<?php echo WEB_PATH ?>'+'/<?php echo G_ADMIN_DIR;?>/adv/get_goods_list';
info.sid = default_sid;


//查询商品
$("#showBtn").click(function(){
	var shopid = $("input[name='shopid']").val();
	var shopname = $("input[name='shopname']").val();
	ajax_get_goods_list(shopid,shopname);
});
   
//ajax拉取商品列表
function ajax_get_goods_list(shopid,shopname)
{
	$.post(url,{shopid:shopid,shopname:shopname},function(_data){
		if(_data.status == 1){
			var _html = '';
			_html += '<option value="0">请选择一个商品...</option>';
			$.each(_data.data, function(k, v){
				_html += '<option value="'+v.id+'" sid="'+v.sid+'" thumb="'+v.thumb+'" zong="'+v.zong+'">'+v.title+'</option>';
			});
			$('#showTips').html('<span style="color:green;">'+_data.message+',共'+_data.total+'个商品!</span>');
			$("#shopid").html(_html);
		}else{
			$('#showTips').html('<span style="color:red;">'+_data.message+'</span>');
		}
	},'json');
}
   
//拉取信息
function do_get_info()
{
	var shopid = $("#shopid").val();
	var dl_len = $('#shopinfo dl').length;
	if(dl_len>=3){
		$('#showTips').html('<span style="color:#ff8502;">已经有3个商品了！</span>');
		return false;
	}
	if(shopid > 0){
	   info.option = $("#shopid").find('option[value="'+shopid+'"]');
	   info.id = shopid;
	   info.sid = info.option.attr('sid');
	   info.thumb = info.option.attr('thumb');
	   info.zong = info.option.attr('zong');
	   var _dd = '<dd><input type="hidden" name="id['+info.sid+']" value="'+info.id+'"/><input type="hidden" name="sid['+info.sid+']" value="'+info.sid+'"/>'+
					'<input type="hidden" name="zong['+info.sid+']" value="'+info.zong+'"/><input type="hidden" name="thumb['+info.sid+']" value="'+info.thumb+'"/></dd>';
	   var _html = '<dl><dt><img src="'+info.thumb+'"/></dt><dd>商品编号:'+info.sid+'　总需:'+info.zong+'人次</dd><dd class="cancel" title="点击后将会取消推荐！">X</dd>'+_dd+'</dl>';
	   $("#shopinfo").append(_html);
   }else{
	   info.sid = default_sid;
   }
}
   
//提交前验证数据
function checkData()
{
   var dl_len = $('#shopinfo dl').length;
   var is_open = $('input[name="is_open"]:checked').val();
   if( ! dl_len && is_open > 0){
	   $('#showTips').html('<span style="color:red;">请至少选择一个商品！</span>');
	   return false;
   }
}

//取消推荐
$("#shopinfo .cancel").die().live("click",function(){
	$(this).parent().remove();
});
</script>
</body>
</html> 