<?php defined('G_IN_ADMIN')or exit('No permission resources.'); ?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<link rel="stylesheet" href="<?php echo G_GLOBAL_STYLE; ?>/global/css/global.css" type="text/css">
<link rel="stylesheet" href="<?php echo G_GLOBAL_STYLE; ?>/global/css/style.css" type="text/css">
<script src="<?php echo G_GLOBAL_STYLE; ?>/global/js/jquery-1.8.3.min.js"></script>
</head>
<body>
<div class="header lr10">
   	<?php echo $this->headerment();?>
</div>
<div class="bk10"></div>

<div class="table_form lr10">
<form action="" method="post" id="myform">
<table width="100%" class="lr10">
    <tr>
    	<td width="80">用户名</td> 
   		<td><input type="text" name="username"  class="input-text" id="username" placeholder="只允许输入5-15个字符内的 字母 或 数字" size="30"></input></td>
    </tr>
    <tr>
    	<td>密码</td>
    	<td><input type="password" name="password" class="input-text" id="password" value="" size="30"></input></td>
    </tr>
    <tr>
    	<td>确认密码</td> 
    	<td><input type="password" name="pwdconfirm" class="input-text" id="pwdconfirm" value="" size="30"></input></td>
    </tr>
    <tr>
    	<td>E-mail</td>
    	<td><input type="text" name="email" value="" class="input-text" id="email" size="30"></input></td>
	</tr>
    <tr>
    <td>所属角色</td>
    <td>
        <select name="mid">
        <?php foreach($group_list as $k => $v){?>
            <option value="<?php echo $v['mid']?>" ><?php echo $v['name']?></option>
        <?php }?>
        </select>
    </td>
    </tr>
</table>
   	<div class="bk15"></div>
    <input type="hidden" name="submit-1" />
    <input type="button" value=" 提交 " id="dosubmit" class="button">
</form>
</div><!--table-list end-->
<script type="text/javascript">
var error='';
var bool=false;
var id='';

function Alert_msg(error,id){
    window.parent.message(error,8,2);
    $('#'+id).focus();
    return false;
}
$(document).ready(function(){
    document.getElementById('dosubmit').onclick=function(){
        var myform=document.getElementById('myform');
        if(!myform.username.value){
            Alert_msg('用户名不能为空','username');return false;
        }
        var reg = /^[0-9a-zA-Z]{5,15}$/;
        if(!reg.test(myform.username.value)){
            Alert_msg('用户名只允许输入5-15个字符内的 字母 或 数字','username');return false;
        }
        if(!myform.password.value){
            Alert_msg('密码不能为空','password');return false;
        }
        if(!myform.pwdconfirm.value){
            Alert_msg('请在次输入密码','pwdconfirm');return false;
        }
        if(!myform.email.value){
            Alert_msg('邮箱不能为空','email');return false;
        }
        reg = /^([a-zA-Z0-9]+[_|\_|\.]?)*[a-zA-Z0-9]+@([a-zA-Z0-9]+[_|\_|\.]?)*[a-zA-Z0-9]+\.[a-zA-Z]{2,3}$/;
        if(!reg.test(myform.email.value)){
            Alert_msg('邮箱格式不对','email');return false;
        }
        if(myform.password.value!=myform.pwdconfirm.value){
            Alert_msg('2次密码不相等');return false;
        }else{
            document.getElementById('myform').submit();
        }
    }
				
	
		
});

</script>
</body>
</html> 