<?php defined('G_IN_ADMIN')or exit('No permission resources.'); ?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<link rel="stylesheet" href="<?php echo G_GLOBAL_STYLE; ?>/global/css/global.css" type="text/css">
<link rel="stylesheet" href="<?php echo G_GLOBAL_STYLE; ?>/global/css/style.css" type="text/css">
<script type="text/javascript" src="<?php echo G_GLOBAL_STYLE; ?>/global/js/jquery-1.8.3.min.js"></script>
<script src="<?php echo G_PLUGIN_PATH; ?>/uploadify/api-uploadify.js" type="text/javascript"></script> 
<style>
table th{ border-bottom:1px solid #eee; font-size:12px; font-weight:100; text-align:right; width:200px;}
table td{ padding-left:10px;}
input.button{ display:inline-block}
</style>
</head>
<body>
<div class="header lr10">
	<?php echo $this->headerment();?>
</div>
<div class="bk10"></div>
<div class="table_form lr10">
<!--start-->
<form name="myform" action="" method="post" enctype="multipart/form-data">
  <table width="100%" cellspacing="0">
		<tr>
			<td width="120" align="right">幻灯名称:</td>
			<td>
				<input type="text" name="title" value="<?php echo $slideone['title'];?>" class="input-text wid300" />
			</td>
		</tr>
		<tr>
			<td width="120" align="right">幻灯链接:</td>
			<td>
				<input type="text" name="link" id="link" onblur="getDataType()" value="<?php echo $slideone['link'];?>" class="input-text wid300"/>
				 地址前,系统会默认添加 <?php echo WEB_PATH; ?>/   
			</td>
		</tr>
		   <tr>
        	<td width="120" align="right">图片:</td>
            <td>	
            <img height="50px" src="<?php echo G_UPLOAD_PATH; ?>/<?php echo $slideone['img']; ?>"/>
            <input type="text" name="image" id="imagetext" value="<?php echo $slideone['img'];?>" id="imagetext" class="input-text wid300">
			<input type="button" class="button"
             onClick="GetUploadify('<?php echo WEB_PATH; ?>','uploadify','缩略图上传','image','banner',1,500000,'imagetext')" 
             value="上传图片"/>
            </td>
        </tr>
		<tr id="slide_type">
			<td width="120" align="right">类型：</td>
			<td>
				<label><input type="radio" name="slide_type" class="input-text" value="0" <?php if($slideone['slide_type']<=0){echo "checked";}?>>跳转网页</label>　
				<label><input type="radio" name="slide_type" class="input-text" value="1" <?php if($slideone['slide_type']==1){echo "checked";}?>>商品详情</label>　 
				<label><input type="radio" name="slide_type" class="input-text" value="2" <?php if($slideone['slide_type']==2){echo "checked";}?>>关键词搜索</label>　
				<label><input type="radio" name="slide_type" class="input-text" value="3" <?php if($slideone['slide_type']==3){echo "checked";}?>>商品列表</label>　
				(注意：跳转至商品详情页,请选择[商品详情];其他请选择[无]。仅APP适用)
			</td>
		</tr>
		<tr <?php if($slideone['slide_val']==''){echo "style='display:none'";}?> id="slide_val">
			<td width="120" align="right">参数:</td>
			<td>
				<input type="text" name="slide_val" value="<?php echo $slideone['slide_val'];?>" class="input-text wid300" />
			</td>
		</tr>

		<tr>
			<td width="120" align="right">苹果或三星产品？:</td>
			<td>
				<label><input type="radio" name="is_apple" <?php if($slideone['is_apple']==1){?> checked <?php }?> value="1"/> 是</label>&nbsp;&nbsp;
				<label><input type="radio" name="is_apple" <?php if($slideone['is_apple']==0){?> checked <?php }?> value="0" /> 否</label>
				&nbsp;&nbsp;&nbsp;(如果是苹果或三星的产品，请选择“是”)
			</td>
		</tr>
		<tr>
			<td width="120" align="right">排序:</td>
			<td>
				<input type="text" name="sort_order"  class="input-text wid100" value="<?php echo $slideone['sort_order']?>"/>&nbsp;&nbsp;(越小越靠前)
			</td>
		</tr>
		
		<tr>
        	<td width="120" align="right"></td>
            <td>		
            <input type="submit" class="button" name="submit" value="提交" >
            </td>
		</tr>
</table>
</form>
</div><!--table-list end-->

<script>
$("#slide_type").find("input[name=slide_type]").click(function(){
	var cur_val = $(this).val();
	if(cur_val==0){
		$("#slide_val").hide();
	}else{
		$("#slide_val").show();
		var _link = $("#link").val();
		var arr = _link.split('/');
		var _hou = parseInt(arr.length-1);
		$("#slide_val").find("input[name=slide_val]").val(arr[_hou]);
	}
});

function upImage(){
	return document.getElementById('imgfield').click();
}

function getDataType()
{
	var link = $("#link").val();
	var str = [];
	if(link.indexOf('/s_tag/') >= 0){	//搜索
		str = link.split('/');
		var keywords = str[str.length-1];
		$("input[name='slide_type']").eq(2).click();
		$("input[name='slide_val']").val(keywords);
	}else if(link.indexOf('/goods/') >= 0 || link.indexOf('/dataserver/') >= 0){	//商品详情
		str = link.split('/');
		var id = str[str.length-1];
		$("input[name='slide_type']").eq(1).click();
		$("input[name='slide_val']").val(id);
	}else if(link.indexOf('/goods_list/') >= 0){
		str = link.split('/');
		var temp = str[str.length-1];
		var temp_id = temp.split('_');
		var id = temp_id[0];
		$("input[name='slide_type']").eq(3).click();
		$("input[name='slide_val']").val(id);
	}else{
		$("input[name='slide_type']").eq(0).click();
	}
}
</script>
</body>
</html> 