<?php defined('G_IN_ADMIN')or exit('No permission resources.'); ?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<link rel="stylesheet" href="<?php echo G_GLOBAL_STYLE; ?>/global/css/global.css" type="text/css">
<link rel="stylesheet" href="<?php echo G_GLOBAL_STYLE; ?>/global/css/style.css" type="text/css">
</head>
<body>
<div class="header lr10">
	<?php echo $this->headerment();?>
</div>

<div class="bk10"></div>

<div class="table-list lr10">
<form name="myform" action="" method="post">
  <table width="100%" cellspacing="0">
    <thead>
		<tr>
		<th width="5%" align="center">序号</th>
		<th width="10%" align="left">名称</th>
		<th width="40%" align="left">权限</th>
		<th width="20%" align="left">成员</th>
		<th width="10%" align="left">管理操作</th>
		</tr>
    </thead>
  	<tbody>
	<?php foreach($group_list as $k => $v){ ?>
    	<tr>
			<td align="center"><?php echo $v['mid'] ?></td>
			<td><?php echo $v['name'] ?></td>
			<td>
				<?php echo str_replace(',','&nbsp;|&nbsp;',$v['auth']); ?>
			</td>
			<td>
			<?php if(count($v['admin'])){ ?>
				<?php foreach($v['admin'] as $k1 => $v1){ ?>
					<a href="<?php echo G_ADMIN_PATH; ?>/user/edit/<?php echo $v1['uid']; ?>"><?php echo $v1['username']; ?></a>&nbsp;|&nbsp;
				<?php } ?>
			<?php } ?>
			</td>
			<td>
				<a href="<?php echo G_ADMIN_PATH; ?>/user/edit_group/<?php echo $v['mid']; ?>">修改</a>
				<?php if(!count($v['admin'])){ ?>
					&nbsp;|&nbsp;<a href="<?php echo G_ADMIN_PATH; ?>/user/del_group/<?php echo $v['mid']; ?>">删除</a>
				<?php } ?>
			</td>
		</tr>
	<?php } ?>
	</tbody>
</table>
 <div id="pages"><ul><li>共 <?php echo $total; ?> 条</li><?php echo $page->show('one','li'); ?></ul></div>
</form>
</div><!--table-list end-->

<script>
	
</script>
</body>
</html> 