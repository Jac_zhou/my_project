<?php defined('G_IN_ADMIN')or exit('No permission resources.'); ?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<script type="text/javascript" src="<?php echo G_GLOBAL_STYLE; ?>/global/js/jquery-1.8.3.min.js"></script>
<script type="text/javascript" src="<?php echo G_WEB_PATH; ?>/statics/plugin/layer/new/newlayer.js"></script>
<link rel="stylesheet" href="<?php echo G_GLOBAL_STYLE; ?>/global/css/global.css" type="text/css">
<link rel="stylesheet" href="<?php echo G_GLOBAL_STYLE; ?>/global/css/style.css" type="text/css">

<link rel="stylesheet" href="<?php echo G_PLUGIN_PATH; ?>/calendar_2/jquery.datetimepicker.css" type="text/css"> 
<script type="text/javascript" charset="utf-8" src="<?php echo G_PLUGIN_PATH; ?>/calendar_2/jquery.js"></script>
<script type="text/javascript" charset="utf-8" src="<?php echo G_PLUGIN_PATH; ?>/calendar_2/build/jquery.datetimepicker.full.js"></script>
<style>
tbody tr{ line-height:30px; height:30px;} 
</style>
</head>
<body>
<div class="header lr10">
	<?php echo $this->headerment();?>
	<span class="lr10"> </span><span class="lr10"> </span>
    <form action="" method="post" style="display:inline-block; ">
	<select name="paixu">
    	<option value="time1"> 按购买时间倒序 </option>
        <option value="time2"> 按购买时间正序 </option>
		<option value="num1"> 按购买次数倒序 </option>
        <option value="num2"> 按购买次数正序 </option>
        <option value="money1"> 按购买总价倒序 </option>
        <option value="money2"> 按购买总价正序 </option>
        
	</select>    
	<input type ="submit" value=" 排序 " name="paixu_submit" class="button"/>
    </form>
	<style>
		.explode_wei{margin-left:15px;padding:1px 5px; background:yellow;font-size:12px;color:#2a8bbb}
	</style>
	<a class="explode_wei" href="<?php echo $this_path?>">导出未发货的产品(客户已确认收货地址的)</a>
	<a class="explode_wei" href="javascript:void(0)">导出中奖订单(客户已确认收货地址的)</a>
</div>
<div class="bk10"></div>
<div class="table-list lr10">
<!--start-->
  <table width="100%" cellspacing="0">
    <thead>
		<tr>
        	<th align="center">订单号</th>
            <th align="center">商品标题</th>
            <th align="center">购买用户</th>
			<th align="center">总需人次</th>
            <th align="center">购买次数</th>
            <th align="center">购买总价</th>
            <th align="center">购买日期</th>
            <th align="center">中奖</th>
            <th align="center">订单状态</th>
            <th align="center">管理</th>
		</tr>
    </thead>
    <tbody>
		<?php foreach($recordlist AS $v) {	?>		
            <tr>
                <td align="center"><?php echo $v['code'];?> <?php if($v['code_tmp'])echo " <font color='#ff0000'>[多]</font>"; ?></td>
                <td align="center">
                <a  target="_blank" href="<?php echo WEB_PATH.'/goods/'.$v['shopid']; ?>">
                第(<?php echo $v['shopqishu'];?>)期<?php echo _strcut($v['shopname'],0,25);?></a>
                </td>
				<td align="center"><?php echo $v['username']; ?></td>
                 <td align="center"><?php echo get_zongrenshu($v['shopid']); ?>人次</td>
                <td align="center"><?php echo $v['gonumber']; ?>人次</td>
                <td align="center">￥<?php echo $v['moneycount']; ?>元</td>
                <td align="center"><?php echo date("Y-m-d H:i:s",$v['time']);?></td>
                 <td align="center"><?php  echo $v['huode'] ? "中奖" : '未中奖';?></td>
                <td align="center">
				<?php
				 if($v['confrim_addr'] == ''){
					 echo "<span style='color:red'>未确认收货地址</span>";
				 }else{
					 echo $v['status'];
				 }
				?>
				</td>
                <td align="center"><a href="<?php echo G_MODULE_PATH;?>/dingdan/get_dingdan/<?php echo $v['id']; ?>">详细</a></td>
            </tr>
            <?php } ?>
  	</tbody>
</table>
<div class="btn_paixu"></div>
<div id="pages"><ul><?php echo $page->show('two','li'); ?></ul></div>
</div><!--table-list end-->

<script type="text/javascript">
$(".explode_wei").eq(1).click(function(){
	var _html = '<form action="<?php echo $this_path_zhong?>" method="post">'; 
	    _html += '<input name="start_time" type="text" id="posttime1" class="input-text posttime"  readonly="readonly" value="<?php echo date("Y/m/d 00:00:01")?>"/> - ';
 		_html += '<input name="end_time" type="text" id="posttime2" class="input-text posttime"  readonly="readonly" value="<?php echo date("Y/m/d 23:59:59")?>" />';
		_html += '<input type="submit" id="explore_zhongjiang" value="确定"/>';
		_html += '&nbsp;&nbsp;<input type="button" id="explore_qing" value="清空"/>';
		_html += '</form>';
	//页面层
	layer.open({
		type: 1,
		title:'请选择时间(默认为当天时间)',
		skin: 'layui-layer-rim', //加上边框
		area: ['520px', '160px'], //宽高
		content: _html
	});
	
	/*时间选择*/
	$.datetimepicker.setLocale('zh');
	var logic = function( currentDateTime ){
		if (currentDateTime && currentDateTime.getDay() == 6){
			this.setOptions({
				minTime:'11:00'
			});
		}else
			this.setOptions({
				minTime:'8:00'
			});
	};
	$('.posttime').datetimepicker({
		onChangeDateTime:logic,
		onShow:logic
	});

	$("#explore_qing").click(function(){
		$("#posttime1,#posttime2").val('');
		
	});
	
});

</script>
</body>
</html> 