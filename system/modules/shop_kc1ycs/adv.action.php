<?php 
defined('G_IN_SYSTEM')or exit('no');
System::load_app_class('admin',G_ADMIN_DIR,'no');
class adv extends admin {
	
	public function __construct(){
		parent::__construct();
		$this->db=System::load_sys_class('model');
		$this->ment=array(
						array("lists","首页弹窗",ROUTE_M.'/'.ROUTE_C."/lists"),
						array("lists","首页推荐位一",ROUTE_M.'/'.ROUTE_C."/recom_a"),
						array("lists","首页推荐位二",ROUTE_M.'/'.ROUTE_C."/recom_b"),
		);
		$this->models=$this->db->GetList("SELECT * FROM `@#_model` where 1",array('key'=>'modelid'));
	}
	
	//首页弹窗
	public function lists(){
		if(isset($_POST['dosubmit'])){
			$title = $_POST['title'] ? trim($_POST['title']) : '';
			$link = $_POST['link'] ? trim($_POST['link']) : '';
			$pics = $_POST['pics'] ? trim($_POST['pics']) : '';
			$times = $_POST['times'] ? trim($_POST['times']) : 3;	//显示时间 默认3秒
			$width = $_POST['width'] ? intval($_POST['width']) : 1200;	//窗口宽度
			$height = $_POST['height'] ? trim($_POST['height']) : 500;	//窗口高度
			$is_open = $_POST['is_open'] ? trim($_POST['is_open']) : 0;	//1开启  0关闭
			$show_type = $_POST['show_type'] ? intval($_POST['show_type']) : 0;	//1每次刷新都显示  0打开浏览器只显示一次
			$config_file = dirname(__FILE__).'/lib/index_adv.ini.php';
			if(!is_writable($config_file)) _message('保存失败,请检查是否有写入权限！');
			$content = <<<EOF
<?php
return array(
	'title'=>'$title',
	'link'=>'$link',
	'pics'=>'$pics',
	'times'=>'$times',
	'width'=>'$width',
	'height'=>'$height',
	'is_open'=>'$is_open',
	'show_type'=>'$show_type'
);
EOF;
			@file_put_contents($config_file,$content);
			_message("保存成功!");
		}	
		$data = System::load_app_config('index_adv');
		include $this->tpl(ROUTE_M,"adv.list");
	}
	
	//首页推荐位一
	public function recom_a(){
		if(isset($_POST['dosubmit'])){
			$id = $_POST['id'] ? intval($_POST['id']) : '';
			$sid = $_POST['sid'] ? intval($_POST['sid']) : '';
			$zong = $_POST['zong'] ? intval($_POST['zong']) : '';
			$thumb = $_POST['thumb'] ? trim($_POST['thumb']) : '';
			$is_open = $_POST['is_open'] ? intval($_POST['is_open']) : 0;
			$config_file = dirname(__FILE__).'/lib/recom_a.ini.php';
			if(!is_writable($config_file)) _message('保存失败,请检查是否有写入权限！');
$content = <<<EOF
<?php
return array(
	'id'=>'$id',
	'sid'=>'$sid',
	'zong'=>'$zong',
	'thumb'=>'$thumb',
	'is_open'=>'$is_open'
);
EOF;
			@file_put_contents($config_file,$content);
			_message("保存成功!");
		}
		$recom_a = System::load_app_config('recom_a');
		include $this->tpl(ROUTE_M,"adv.recom_a");
	}
	
	//首页推荐位二
	public function recom_b(){
		if(isset($_POST['dosubmit'])){
			$is_open = $_POST['is_open'] ? intval($_POST['is_open']) : 0;
			$ids = $_POST['id'] ? $_POST['id'] : '';
			$sids = $_POST['sid'] ? $_POST['sid'] : '';
			$thumbs = $_POST['thumb'] ? $_POST['thumb'] : '';
			$zongs = $_POST['zong'] ? $_POST['zong'] : '';
			$list = 'array()';
			$sid_str = '';
			if(count($ids)){
				$list = 'array(';
				foreach($ids as $k => $v){
					$list .= "array('id'=>'$v','sid'=>'$sids[$k]','zong'=>'$zongs[$k]','thumb'=>'$thumbs[$k]'),";
					$sid_str .= $sids[$k].',';
				}
				$list = rtrim($list,',');
				$list .= '),';
				$sid_str = rtrim($sid_str,',');
			}
			$config_file = dirname(__FILE__).'/lib/recom_b.ini.php';
			if(!is_writable($config_file)) _message('保存失败,请检查是否有写入权限！');
$content = <<<EOF
<?php
return array(
	'is_open'=>'$is_open',
	'sid_str'=>'$sid_str',
	'list'=>$list
);
EOF;
				@file_put_contents($config_file,$content);
				_message("保存成功!");
		}
		$recom_b = System::load_app_config('recom_b');
		include $this->tpl(ROUTE_M,"adv.recom_b");
	}
	
	
	//查询商品
	public function get_goods_list()
	{
		$info = array('status'=>0 , 'message'=>'查询失败！', 'data'=>'');
		$shopid = isset($_POST['shopid']) ? safe_replace(intval($_POST['shopid'])) : '';
		$shopname = isset($_POST['shopname']) ? safe_replace(trim($_POST['shopname'])) : '';
		if(!$shopid && !$shopname){
			//die(json_encode($info));
		}
		if($shopid) $wh = " AND `id` = $shopid ";
		if($shopname) $wh = " AND ( `title` LIKE '%$shopname%' OR `keywords` LIKE '%$shopname%' ) ";
		
		$list = $this->db->GetList("SELECT id,sid,title,thumb,zongrenshu as zong FROM @#_shoplist WHERE `q_uid` IS NULL AND `shenyurenshu` > 0  $wh ");
		$total = count($list);
		if(!$total){
			$info = array('status'=>0,'message'=>'没有查询到相关商品！','data'=>array());
			die(json_encode($info));
		}
		foreach($list as $k => $v){
			$list[$k]['title'] = str_replace('&nbsp;',' ',$v['title']);
			$list[$k]['thumb'] = G_UPLOAD_PATH . '/' . $v['thumb'];
		}
		$data = $list;
		$info = array('status'=>1,'message'=>'查询成功','data'=>$data, 'total'=>$total);
		die(json_encode($info));
	}
	
	
}