<?php

defined('G_IN_SYSTEM')or exit('No permission resources.');
System::load_app_class('base', 'member', 'no');

class index extends base {

    public function __construct() {
        parent::__construct();
        $this->db = System::load_sys_class('model');
    }

    //开奖
    public function init() {
        if (file_exists(G_SYSTEM . 'modules/ssclottery/')) {
            $ssclottery = "ssclottery";
            $config = $this->db->GetOne("select value from `@#_caches` where `key`='$ssclottery'");
            if (isset($config) && $config['value'] == 1) {
                date_default_timezone_set('PRC');
                $Lotteryconf = include G_CONFIG . 'dolotterycon.inc.php';
                //判断今天的开奖号出来没有
                $url = "http://baidu.lecai.com/api/hao123/new_lottery_all.php"; //彩票链接
                $url_content = file_get_contents($url); //获取彩票全部内容
                $url_content = str_replace('Hao.caiPiaoInit(', '', $url_content); //截取彩票信息	
                $url_content = str_replace(',-1);', '', $url_content);
                $json = json_decode($url_content, true); //转化json格式
                //$json = $json['category']['gaopin'][0];//获取所有中奖类别信息
                //print_r($json);
                $ssc_content = $json['caipiao']['200']; //获取时时彩信息 
                //echo "<pre/>";print_r($ssc_content);	
                $ssc_open = $ssc_content['open']; //echo "<br>";//时时彩开奖时间戳
                if ($Lotteryconf['open'] == $ssc_open) {//判断日志文件中是否记录此条数据
                    //echo date('Y-m-d H:i:s',$ssc_open);
                    //echo "<br>";
                    //echo date('Y-m-d H:i:s',time());echo "<br>";
                    //$curday=strtotime(date('Y-m-d H:i:s',time()));
                    //echo $curday-$Lotteryconf["open"];
                    $ssc_content = null;
                    $json = null;
                    exit;
                } else {
                    $ssc_phase = $ssc_content['phase']; //echo "<br>";//时时彩期数				
                    $ssc_datetime = date("Y-m-d H:i:s", $ssc_open); //echo "<br>";//中奖时间显示
                    $timenow = date("Y-m-d H:i:s", time());
                    //print_r($ssc_content['detail']);//时时彩中奖号码	 
                    $code = implode($ssc_content['detail']);
                    //记录时时彩开奖信息open->开奖时间戳,code->开奖号码,phase->开奖期数
                    $html = "<?php return array('open'=>'" . $ssc_open . "','code'=>'" . $code . "','phase'=>'" . $ssc_phase . "'); ?>";
                    file_put_contents(G_CONFIG . 'dolotterycon.inc.php', $html);
                    $ssc_content = null;
                    $json = null;
                    exit;
                }
            } else {
                exit;
            }
        } else {
            exit;
        }
    }

}

?>