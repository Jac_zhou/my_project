<?php
/*
* 清单相关API
*/
defined('G_IN_SYSTEM')or exit('no');
System::load_app_class('api');
System::load_app_fun('myfun');
System::load_app_fun("pay","pay");

class Cart extends api{

	private $members = array();
	private $shoplist = array();
	private $moneyCount = 0;
	private $dingdancode = '';

	/*
	* 1.添加商品到清单
	* param >> id,num,uid,cache_id
	*/
	public function do_add_cart()
	{
		$indata = $this->getData();			//获取数据
		$this->checkSign($indata);			//验证签名
		$id = isset($indata['id']) ? intval($indata['id']) : '';	//商品id
		$num = isset($indata['num']) ? intval($indata['num']) : 1;	//购买人次
		$uid = isset($indata['uid']) ? intval($indata['uid']) : '';	//用户id
		$cache_id = isset($indata['cache_id']) ? trim($indata['cache_id']) : '';	//客户端唯一标识.
		$info = isset($indata['info']) ? trim($indata['info']) : '';		//添加多个，格式如： id,num|id,num|id,num

		if( ( (!$id || !$num) &&  !$info)  || ( ! $cache_id &&  ! $uid ) ) {
			$this->info['message'] = '请求失败';
			$this->callBack();
		}

		$cart = array(array('id'=>$id,'num'=>$num));	//单个添加
		if($info){	//多个添加
			$info = str_replace('，',',',rtrim($info,'|'));
			$arrs = explode('|',$info);
			foreach($arrs as $k => $v){
				if(empty($v)) continue;
				$rows = explode(',',$v);
				if(!isset($rows[0]) || !isset($rows[1])) continue;
				$cart[$k]['id'] = intval($rows[0]);
				$cart[$k]['num'] = intval($rows[1]);
			}
		}
		$info = $this->add_to_cart($uid,$cache_id,$cart);
		$this->info = $info;
		$this->callBack();
	}

	/*
	* 2.删除清单中的商品
	*/
	public function do_del_cart()
	{
		$indata = $this->getData();			//获取数据
		$this->checkSign($indata);			//验证签名

		$id = isset($indata['id']) ? trim($indata['id']) : '';	//清单记录ID，多个用逗号隔开
		$cache_id = isset($indata['cache_id']) ? trim($indata['cache_id']) : '';	//清单记录ID
		$uid = isset($indata['uid']) ? intval($indata['uid']) : '';	//清单记录ID

		if( ! $id ||  ( ! $cache_id && ! $uid) ){
			$this->info['message'] = '请求失败';
			$this->callBack();
		}

		$id = trim(str_replace('，',',',$id),',');
		if($uid) $wh = " `uid` = $uid ";
		if($cache_id) $wh = " `cache_id` = '$cache_id' ";
		if($uid && $cache_id) $wh = " ( `uid` = '$uid' OR `cache_id` = '$cache_id' ) ";
		$sql = "DELETE FROM @#_app_cart WHERE `id` in($id) AND $wh ";
		$is_del = $this->db->Query($sql);
		if($is_del){
			$this->info = array('status'=>1, 'message'=>'删除成功');
		}else{
			$this->info['message'] = '删除失败！';
		}
		$this->callBack();
	}

	/*
	* 3.修改清单商品数量
	* param >> id,shopid,num,cache_id   需 补上 uid
	*/
	public function do_mod_cart()
	{
		$indata = $this->getData();			//获取数据
		$this->checkSign($indata);			//验证签名
		$id = isset($indata['id']) ? intval($indata['id']) : '';	//单个清单记录ID
		$shopid = isset($indata['shopid']) ? intval($indata['shopid']) : '';	//商品ID
		$num = isset($indata['num']) ? intval($indata['num']) : 1;	//购买人次
		$cache_id = isset($indata['cache_id']) ? trim($indata['cache_id']) : '';	//客户端唯一标识

		if(!$id || !$shopid || !$num || !$cache_id){
			$this->info['message'] = '请求失败';
			$this->callBack();
		}
		$goods = $this->goods_info($shopid);
		if( ! isset($goods['id']) || $goods['shenyurenshu'] == 0){
			$this->info['message'] = '商品不存在或本期已满';
			$this->callBack();
		}
		if($goods['is_shi'] == 1){
		    $num = $num > 5 ? 5 : $num;

		}
		$num = $num > $goods['shenyurenshu'] ? $goods['shenyurenshu'] : $num;

		$sql = "UPDATE @#_app_cart SET `shenyurenshu` = " . $goods['shenyurenshu'] . " , `gonumber` = $num WHERE `id` = $id AND  `cache_id` = '$cache_id' ";
		$this->db->Query($sql);
		$is_mod = $this->db->affected_rows();
		if($is_mod){
			$this->info = array('status'=>1, 'message'=>'修改成功');
		}else{
			$this->info['message'] = '修改失败！';
		}
		$this->callBack();
	}

	/*
	* 4.查看清单列表
	* param >> uid OR cache_id
	*/
	public function do_show_cart()
	{
		$indata = $this->getData();			//获取数据
		$this->checkSign($indata);			//验证签名
		$uid = isset($indata['uid']) ? intval($indata['uid']) : '';	//UID
		$cache_id = isset($indata['cache_id']) ? trim($indata['cache_id']) : '';	//客户端唯一标识

		if( ! $cache_id &&  ! $uid){
			$this->info['message'] = '请求失败';
			$this->callBack();
		}
		$data = $this->cart_list($cache_id,$uid);
		if(count($data)){

		    $return_data = array('status'=>1, 'message'=>'请求成功' ,'data'=>$data);

		    /* 如果是苹果设备 需要对其做特殊处理  20160107 start */
		    $is_ios = $this->is_ios($indata);
		    $version = isset($indata['version']) ? trim($indata['version']) : '2.0.0'; //2.0.0
		    if($is_ios){
		        if( $this->compare_version($version) ){
		            $return_data['is_hidden'] = 1;   //1隐藏   2显示    3苹果审核改成1,审核通过改成2
		        }else{
		            $return_data['is_hidden'] = 2;
		        }
		        $return_data['do_link'] = 'http://www.yz-db.com/yungou/index.html#/tab/shopList';
		    }
		    /* 如果是苹果设备 需要对其做特殊处理  20160107 end */

			$this->info = $return_data;
		}else{
			$this->info = array('status'=>1, 'message'=>'暂无数据！' ,'data'=>array());
		}
		$this->callBack();
	}

	/*
	* 5.获取支付详细信息(APP内点击结算按钮后)
	* param >> uid
	*/
	public function get_pay_info()
	{
	    $indata = $this->getData();			//获取数据
	    $this->checkSign($indata);			//验证签名
	    $uid = isset($indata['uid']) ? intval($indata['uid']) : 0;		//会员ID
	    $cache_id = isset($indata['cache_id']) ? trim($indata['cache_id']) : '';		//设备标识
		$info = isset($indata['info']) ? trim($indata['info']) : '';		//最终清单信息,格式如： id,num|id,num|id,num

		if(!$uid ||  !$info){
			$this->info['message'] = '请求失败';
			$this->callBack();
		}

		//获取会员余额
		$uinfo = $this->db->GetOne("SELECT money FROM @#_member WHERE `uid` = '$uid' LIMIT 1");
		if(!isset($uinfo['money'])){
			$this->info['message'] = '用户不存在！';
			$this->callBack();
		}
		$umoney = $uinfo['money'];

		//修改用户的购物清单
		$this->update_cart_info($uid,$info,$cache_id);

		//获取购物清单
		$cart_list = $this->cart_list($cache_id,$uid);
		$total_money = 0;
		$list = array();
		if(count($cart_list)){
			foreach($cart_list as $k => $v){
				$list[$k]['id'] = $v['id'];
				$list[$k]['title'] = $v['title'];
				$list[$k]['gonumber'] = $v['gonumber'];
				$total_money += ($v['yunjiage'] * $v['gonumber']);
			}
		}else{
			$this->info['message'] = '购物车中没有商品！';
			$this->callBack();
		}
		$pay_money = ($umoney > $total_money) ? 0 : ($total_money - $umoney);	//其他方式需支付金额(也就是用账户余额扣完钱后还要支付多少金额)

		$pay_type = array();
		if($pay_money > 0){
			$pay_type = $this->get_pay_type_list(); //支付方式列表
		}
		$data = array('total_money' => $total_money,'user_money' => $umoney, 'pay_money'=>$pay_money, 'list'=>$list);
		$this->info = array('status'=>1, 'message'=>'请求成功', 'data'=>$data);
		$this->callBack();
	}

	/*
	* 6.使用账户余额支付
	* param >> uid
	*/
	public function do_pay()
	{
	    $indata = $this->getData();			//获取数据
	    $this->checkSign($indata);			//验证签名
	    $uid = isset($indata['uid']) ? intval($indata['uid']) : 0;		//会员ID
	    $cache_id = isset($indata['cache_id']) ? trim($indata['cache_id']) : '';		//设备号
		$info = isset($indata['info']) ? trim($indata['info']) : '';		//最终清单信息,格式如： id,num|id,num|id,num

		if(!$uid ||  !$info){
			$this->info['message'] = '请求失败';
			$this->callBack();
		}

		$is_ios = $this->is_ios($indata);
		$is_android = $this->is_android($indata);
		if($is_ios || $is_android){
		    sleep(2);
		}

		//修改用户的购物清单
		$this->update_cart_info($uid,$info,$cache_id);

		$this->db->Autocommit_start();
		$user_info = $this->db->GetOne("SELECT * FROM @#_member WHERE `uid` = '$uid' for update");
		$this->members = $user_info;
		if( ! isset($user_info['uid'])){
		    $this->db->Autocommit_rollback();
			$this->info['message'] = '用户不存在！';
			$this->callBack();
		}
		$cart_list = $this->cart_list($cache_id,$uid);
		$money_count = 0;	//商品总价
		$shoplist = array();
		if(count($cart_list)){
			foreach($cart_list as $k => $v){
				$money_count += $v['yunjiage'] * $v['gonumber'];		//计算应付总额
				//重组shoplist,便于之后的步骤调用
				$shoplist[$k]['id'] = $v['shopid'];
				$shoplist[$k]['title'] = $v['title'];
				$shoplist[$k]['yunjiage'] = $v['yunjiage'];
				$shoplist[$k]['codes_table'] = $v['codes_table'];
				$shoplist[$k]['canyurenshu'] = $v['canyurenshu'];
				$shoplist[$k]['zongrenshu'] = $v['zongrenshu'];
				$shoplist[$k]['cart_gorenci'] = $v['gonumber'];
				$shoplist[$k]['maxqishu'] = $v['maxqishu'];
				$shoplist[$k]['qishu'] = $v['qishu'];
			}
			//判断余额
			if($this->members['money'] < $money_count){
				$this->db->Autocommit_rollback();
				$this->info['message'] = '余额不足！';
				$this->callBack();
			}

			$this->shoplist = $shoplist;
			$this->moneyCount = $money_count;

			$shenyu_money = $this->members['money'] - $money_count;	//剩余金额

			$query_1 = $this->set_app_dingdan('账户','A');	//添加购买记录并且生成众乐码
			//修改会员金额
			$time = time();
			$query_2 = $this->db->Query("UPDATE `@#_member` SET `money`='$shenyu_money' WHERE (`uid`='$uid')");			//金额
			$query_3 = $info = $this->db->GetOne("SELECT * FROM  `@#_member` WHERE (`uid`='$uid') LIMIT 1");
			$query_4 = $this->db->Query("INSERT INTO `@#_member_account` (`uid`, `type`, `pay`, `content`, `money`, `time`) VALUES ('$uid', '-1', '账户', '众乐了商品', '$money_count', '$time')");
			$query_5 = true;

			//修改商品的信息
			$goods_count_num = 0;
			foreach($this->shoplist as $shop){
				if($shop['canyurenshu'] >= $shop['zongrenshu'] && $shop['maxqishu'] >= $shop['qishu']){
						$this->db->Query("UPDATE `@#_shoplist` SET `canyurenshu`=`zongrenshu`,`shenyurenshu` = '0' where `id` = '$shop[id]'");
				}else{
					$shenyurenshu = $shop['zongrenshu'] - $shop['canyurenshu'];
					$query = $this->db->Query("UPDATE `@#_shoplist` SET `canyurenshu` = '$shop[canyurenshu]',`shenyurenshu` = '$shenyurenshu' WHERE `id`='$shop[id]'");
					if(!$query)$query_5=false;
				}
				$goods_count_num += $shop['cart_gorenci'];	//这里应该是统计网站的购买人次
			}

			$dingdancode=$this->dingdancode;
			//修改订单状态
			$query_6 = $this->db->Query("UPDATE `@#_member_go_record` SET `status`='已付款,未发货,未完成' WHERE `code`='$dingdancode' and `uid` = '$uid'");
			$query_7 = $this->dingdan_query;
			$query_8 = $this->db->Query("UPDATE `@#_caches` SET `value`=`value` + $goods_count_num WHERE `key`='goods_count_num'");
			$this->goods_count_num = $goods_count_num;

			 //清空该用户的购物车
			$wh = '';
			if($cache_id) $wh=" OR `cache_id` = '$cache_id' ";
			$query_9 = $this->db->Query("DELETE FROM @#_app_cart WHERE `uid` = '$uid' " . $wh);

			if($query_1 && $query_2 && $query_3 && $query_4 && $query_5 && $query_6 && $query_7 && $query_8 && $query_9){
				if($info['money'] == $shenyu_money){
					$this->db->Autocommit_commit();
						foreach($this->shoplist as $shop){
							if($shop['canyurenshu'] >= $shop['zongrenshu'] && $shop['maxqishu'] >= $shop['qishu']){
								/**时时彩LVDENG修改**/
								$post_arr= array("gid"=>$shop['id']);
								_g_triggerRequest(WEB_PATH.'/pay/pay_user_ssc_time/init',false,$post_arr);
								/**时时彩LVDENG修改**/
							}
						}
						//如果使用余额支付成功，则返回参与信息
						$canyu_info = $this->canyu_info($uid,$dingdancode);
						$this->info = array('status'=>1, 'message'=>'支付成功！', 'code'=>$dingdancode, 'data'=>$canyu_info);
						$this->callBack();
				}else{
					$this->db->Autocommit_rollback();
					$this->info['message'] = '系统异常,请稍后再试';
					$this->callBack();
				}
			}else{
				$this->db->Autocommit_rollback();
				$this->info['message'] = '系统异常,请稍后再试';
				$this->callBack();
			}
			die;
		}else{
			$this->db->Autocommit_rollback();
			$this->info['message'] = '您的商品可能已被抢购！！';
			$this->callBack();
		}
	}

	/*
	* h5支付
	*/
	public function wap_do_pay()
	{
	    $indata = $this->getData();			//获取数据
	    //$this->checkSign($indata);			//验证签名
	    $uid = isset($indata['uid']) ? intval($indata['uid']) : 0;		//会员ID
		$info = isset($indata['info']) ? trim($indata['info']) : '';		//最终清单信息,格式如： id,num|id,num|id,num
		$is_yue = isset($indata['is_yue']) ? intval($indata['is_yue']) : 0;		//是否使用余额
		$pay_type = $indata['pay_type'];//手机目前只有支付宝
		if ($indata['pay_type'] != 'wxpay') {
			$uid = $_POST['uid'];
			if( ! is_ajax() ){
				$this->info['message']='请求失败1';
				$this->callBack();
			}
		}
		


		if( ! $uid &&  ! $info){
			$this->info['message']='请求失败2';
			$this->callBack();
		}
		if( ! $pay_type && !$is_yue){
		    $this->info['message']='参数错误';
		    $this->callBack();
		}

		$this->db->Autocommit_start();
		$user_info = $this->db->GetOne("SELECT * FROM @#_member WHERE `uid` = '$uid' for update");
		$this->members = $user_info;
		if( ! isset($user_info['uid'])){
			$this->db->Autocommit_rollback();
			$this->info['message'] = '请求失败3';
			$this->callBack();
		}

		$infos = explode('|',$info);
		$shoplist = array();
		$scookies = array();
		$money_count = 0;	//需支付的总金额
		if(count($infos)){
			foreach($infos as $k => $v){
				if(! $v) continue;
				$gd = explode(',',$v);
				if(!$gd[0] ||  !$gd[1]) continue;
				$id = intval($gd[0]);	//商品id
				$num = intval($gd[1]);	//商品数量
				$rows = $this->db->GetOne("SELECT id,qishu,maxqishu,title,yunjiage,codes_table,canyurenshu,zongrenshu,shenyurenshu,is_shi FROM @#_shoplist WHERE `id` = '$id' AND `shenyurenshu` > 0 LIMIT 1");
                $GoodListCon = System::load_contorller('GoodList','Zapi');
                $rows =  $GoodListCon->list_add_user_bought_number($rows,$uid);
				if(!$rows) continue;
				$shoplist[$k]['id'] = $rows['id'];
				$shoplist[$k]['cart_gorenci'] = $num > $rows['wap_ten'] ? $rows['wap_ten'] : $num ;
				$shoplist[$k]['title'] = $rows['title'];
				$shoplist[$k]['yunjiage'] = $rows['yunjiage'];
				$shoplist[$k]['codes_table'] = $rows['codes_table'];
				$shoplist[$k]['canyurenshu'] = $rows['canyurenshu'];
				$shoplist[$k]['zongrenshu'] = $rows['zongrenshu'];
				$shoplist[$k]['maxqishu'] = $rows['maxqishu'];
				$shoplist[$k]['qishu'] = $rows['qishu'];

				//在线支付时用到
				$scookies[$rows['id']]['shenyu'] = $rows['shenyurenshu'];
				$scookies[$rows['id']]['num'] = $shoplist[$k]['cart_gorenci'];
				$scookies[$rows['id']]['money'] = $rows['yunjiage'];

				$money_count += ($rows['yunjiage'] *  $shoplist[$k]['cart_gorenci']) ;
			}
		}
		$scookies['MoenyCount'] = $money_count;
		$scookies = serialize($scookies);

		//没有商品或已抢空的清空
		if(!count($shoplist) || $money_count <= 0){
			$this->db->Autocommit_rollback();
			$this->info['message'] = '购物车中没有商品或本期商品已抢空';
			if($indata['pay_type'] == 'wxpay'){
				include './tpl/payResult.tpl.php';
				die;
			}
			$this->callBack();
		}
		
		
		//是否使用余额支付
		if($is_yue){
    		if($this->members['money'] < $money_count){//判断余额是否足够支付
    			$recharge_money = $money_count - $this->members['money'];
    			$indata['pay_type'] = $pay_type;
				
    			$this->wap_add_money(true, $uid, $recharge_money, $scookies,$indata);	//直接跳转至充值页面
    		}else{ //余额充足  使用余额支付

    			$this->shoplist = $shoplist;
    			$this->moneyCount = $money_count;

    			$shenyu_money = $this->members['money'] - $money_count;	//剩余金额
    			$time = time();

    			$query_1 = $this->set_app_dingdan('账户','A');	//添加购买记录并且生成众乐码
    			//修改会员金额
    			$query_2 = $this->db->Query("UPDATE `@#_member` SET `money`='$shenyu_money' WHERE (`uid`='$uid')");			//金额
    			$query_3 = $info = $this->db->GetOne("SELECT * FROM  `@#_member` WHERE (`uid`='$uid') LIMIT 1");
    			$query_4 = $this->db->Query("INSERT INTO `@#_member_account` (`uid`, `type`, `pay`, `content`, `money`, `time`) VALUES ('$uid', '-1', '账户', '众乐了商品', '$money_count', '$time')");
    			$query_5 = true;

    			//修改商品的信息
    			$goods_count_num = 0;
    			foreach($this->shoplist as $shop){
    				if($shop['canyurenshu'] >= $shop['zongrenshu'] && $shop['maxqishu'] >= $shop['qishu']){
    						$this->db->Query("UPDATE `@#_shoplist` SET `canyurenshu`=`zongrenshu`,`shenyurenshu` = '0' where `id` = '$shop[id]'");
    				}else{
    					$shenyurenshu = $shop['zongrenshu'] - $shop['canyurenshu'];
    					$query = $this->db->Query("UPDATE `@#_shoplist` SET `canyurenshu` = '$shop[canyurenshu]',`shenyurenshu` = '$shenyurenshu' WHERE `id`='$shop[id]'");
    					if(!$query)$query_5=false;
    				}
    				$goods_count_num += $shop['cart_gorenci'];	//这里应该是统计网站的购买人次
    			}

    			$dingdancode=$this->dingdancode;
    			//修改订单状态
    			$query_6 = $this->db->Query("UPDATE `@#_member_go_record` SET `status`='已付款,未发货,未完成' WHERE `code`='$dingdancode' and `uid` = '$uid'");
    			$query_7 = $this->dingdan_query;
    			$query_8 = $this->db->Query("UPDATE `@#_caches` SET `value`=`value` + $goods_count_num WHERE `key`='goods_count_num'");
    			$this->goods_count_num = $goods_count_num;

    			if($query_1 && $query_2 && $query_3 && $query_4 && $query_5 && $query_6 && $query_7 && $query_8){
    				if($info['money'] == $shenyu_money){
    					$this->db->Autocommit_commit();
    						foreach($this->shoplist as $shop){
    							if($shop['canyurenshu'] >= $shop['zongrenshu'] && $shop['maxqishu'] >= $shop['qishu']){
    								/**时时彩LVDENG修改**/
    								$post_arr= array("gid"=>$shop['id']);
    								_g_triggerRequest(WEB_PATH.'/pay/pay_user_ssc_time/init',false,$post_arr);
    								/**时时彩LVDENG修改**/
    							}
    						}
    						//如果使用余额支付成功，则返回参与信息
    						//status_type  1表示余额支付成功    2表示余额不够，需要发起在线支付
    						$canyu_info = $this->canyu_info($uid,$dingdancode);
    						$this->info = array('status'=>1, 'message'=>'支付成功','status_type'=>1, 'data'=>$canyu_info);
    						$this->callBack();
    				}else{
    					$this->db->Autocommit_rollback();
    					$this->info['message'] = '系统异常,请稍后再试';
    					$this->callBack();
    				}
    			}else{
    				$this->db->Autocommit_rollback();
    				$this->info['message'] = '系统异常,请稍后再试';
    				$this->callBack();
    			}
    		}
		}else{
		    $recharge_money = $money_count;
		    $indata['pay_type'] = $pay_type;
		    $this->wap_add_money(true, $uid, $recharge_money, $scookies,$indata);//直接跳转至充值页面
		}
	}

	/*
    * 查看订单状态
	*/
	/*public function get_pay_state() {
		if (isset($_SESSION['dingdanhao'])) {
			$code = $_SESSION['dingdanhao'];
			$uid = $_SESSION['uid'];
			$this->info['message'] = "4654564";
			$this->callBack();
		}else{
			$code = $_POST['code'];
			$uid = $_POST['uid'];
		}

		if (isset($code)) {
			$money_arr = $this->db->GetOne("SELECT money FROM `@#_member` WHERE uid = $uid");
			$money = $money_arr['money'];
			$moneyInfo = $this->db->GetOne("SELECT * FROM `@#_member_addmoney_record` WHERE uid = $uid AND `code` = '$code' LIMIT 1");
		}else{
			$money_arr = $this->db->GetOne("SELECT money FROM `@#_member` WHERE uid = $uid");
			$money = $money_arr['money'];
			$moneyInfo = $this->db->GetOne("SELECT * FROM `@#_member_addmoney_record` WHERE uid = $uid AND `pay_type` = '微信wap' ORDER BY `time` DESC LIMIT 1");
		}


		$all = $moneyInfo['money']*1;

		// echo $sql = "SELECT * FROM `@#_member_go_record` WHERE uid = $uid AND `moneycount` = '" . $all . "'";
		// die;
		$recordInfo = $this->db->GetOne("SELECT * FROM `@#_member_go_record` WHERE uid = $uid AND status = 1 AND `moneycount` = '" . $all . "'");
		if ($recordInfo) {
			$canyu_info = $this->canyu_info($uid,$recordInfo['code']);
			if ($recordInfo && $recordInfo['status'] == '已付款,未发货,未完成') {
				$this->info = array('status'=>1, "message"=>'支付成功', 'status_type'=>1, 'data'=>$canyu_info);
				$this->callBack();
			}else{
				$this->info['message'] = "支付失败";
				$this->callback();
			}
		}elseif ($recordInfo && $moneyInfo) {
			if ($moneyInfo['status'] == '已付款') {
				$this->info = array('status'=>1, "message"=>'充值成功1', 'status_type'=>3, 'data'=>'');
				$this->callback();
			}else{
				$this->info = array('status'=>1, "message"=>'充值失败2', 'status_type'=>2, 'data'=>'');
				$this->callback();
			}
		}else{
			$this->info['message'] = "支付失败";
			$this->callback();
		}
	}*/

	public function get_pay_state2() {
		$uid = $_POST['uid'];
		$code = isset($_POST['code'])?$_POST['code']:'';

		if ($code) {
			//确定支付宝或者爱贝
			$state = $this->jump_state($uid,$code);
		}else{
			//这个直接是微信端
			$state = $this->jump_state($uid);
		}

		$jump_state = $state['state'];
		//print_r($state);
		if ($jump_state == 1) {
			$info = $this->db->GetOne("SELECT * FROM `@#_member_go_record` WHERE uid = $uid AND `code` = '".$state['code']."'");
		}elseif ($jump_state == 2) {
			$info = $this->db->GetOne("SELECT * FROM `@#_member_addmoney_record` WHERE uid = $uid AND `code` = '".$state['code']."'");
		}else {
			$this->info['message'] = "系统错误";
			$this->callback();
		}

		//判断是否支付成功
		if ($info['status'] == "已付款" || $info['status'] == "已付款,未发货,未完成") {
			if ($jump_state == 1) {
				$canyu_info = $this->canyu_info($uid,$state['code']);
				$this->info = array("status"=>1,"message"=>"支付成功","pay_state"=>1,"data"=>$canyu_info);
			}else{
				$this->info = array("status"=>2,"message"=>"充值成功","pay_state"=>1,"data"=>"");
			}
		}else{
			if ($jump_state == 1) {
				$this->info = array("status"=>1,"message"=>"支付失败","pay_state"=>2,"data"=>"");
			}else{
				$this->info = array("status"=>2,"message"=>"充值失败","pay_state"=>2,"data"=>"");
			}
		}
		$this->callback();
		//$this->jump_status($state);
	}

	//用于判断支付/或者充值   1--支付   2---充值
	protected function jump_state($uid,$code='') {
		if ($code) {
			$wh = " code = '$code'";
		}else{
			$wh = " `pay_type` = '微信wap' or `pay_type` = '微信支付' ORDER BY `time` DESC LIMIT 1";
		}

		// $sql = "SELECT * FROM `@#_member_addmoney_record` WHERE `uid` = $uid AND $wh";
		// echo $sql;
		// die;
		$add_code = $this->db->GetOne("SELECT * FROM `@#_member_addmoney_record` WHERE `uid` = $uid AND $wh");
		$scookies = isset($add_code['scookies'])?unserialize($add_code['scookies']):'';
		$money = isset($add_code['scookies'])?$scookies['MoenyCount']:$add_code['money'];
		//$money = $add_code['money']*1;
		$time = $add_code['time']-5;
		$sql = "SELECT * FROM `@#_member_go_record` WHERE uid = $uid AND `time`>'".$time."'  AND `moneycount` = '" . $money . "'";
		// echo $sql;
		// die;
		$recordInfo = $this->db->GetOne("SELECT * FROM `@#_member_go_record` WHERE uid = $uid AND `time`>'$time' AND `moneycount` = '".$money."' OR `moneycount` > '" . $money . "' ORDER BY id DESC LIMIT 1");
		if ($add_code) {
			if ($add_code['scookies']) {
				//表示支付
				$re = array(
					"state" => 1,
					"code" => $recordInfo['code']
					);
			}else{
				//表示充值
				$re = array(
					"state" => 2,
					"code" => $add_code['code']
					);
			}
		}else{
			$re = array(
				"state" => 0,
				"code" => ''
				);
		}
		return $re;

	}

	/*
	* H5跳转页面充值 支付宝
	*/
	/*  支付宝已取消
	public function wap_add_money($is_sys=false,$uid=null,$money=null,$pay_class=null,$data=null)
	{

	    if( ! $is_sys){    //通过个人中心充值
	        $indata = $this->getData();			//获取数据
	        $this->checkSign($indata);
	        $uid = isset($indata['uid']) ? intval($indata['uid']) : 0;		//会员ID
	        $money = isset($indata['money']) ? intval($indata['money']) : 0;   //金额只能是整数
	        $pay_class = isset($indata['pay_type']) ? trim($indata['pay_type']) : 'alipay'; //支付方式 目前只有alipay
	        $scookies = '0';
	    }else{     //购买商品充值
	        $uid = $uid ? intval($uid) : '';
	        $money = $money ? intval($money) : '';
	        $pay_class = $pay_class ? trim($pay_class) : 'alipay';
	        $scookies = $data;
	    }

	    $pay_class = 'alipay';

	    if( ! $uid || ! $money || !$pay_class){
	        $this->info['message'] = '参数错误';
	        $this->callBack();
	    }

	    $this->db->Autocommit_start();
	    $members = $this->db->GetOne("SELECT * FROM `@#_member` where `uid` = '$uid' for update");
	    if(!isset($members['uid'])){
	        $this->db->Autocommit_rollback();
	        $this->info['message'] = '参数错误';
	        $this->callBack();
	    }

	    //支付方式
	    $pay_type = $this->db->GetOne("SELECT * from `@#_pay` where `pay_class` = '$pay_class' and `pay_start` = '1'");

	    //插入充值记录
	    $score = 0;
	    $dingdanhao = pay_get_dingdan_code('C');    //生成充值订单号
	    $time = time();
	    $sql = "INSERT INTO `@#_member_addmoney_record` (`uid`, `code`, `money`, `pay_type`, `status`,`time`,`score`,`scookies`)
	           VALUES ('$uid', '$dingdanhao', '$money', '$pay_type[pay_name]','未付款', '$time','$score','$scookies')";
	    $query = $this->db->Query($sql);
	    if($query){
	        $this->db->Autocommit_commit();
	    }else{
	        $this->db->Autocommit_rollback();
	        $this->info['message'] = '操作失败,请稍后再试！';
	        $this->callBack();
	    }

	    $pay_type_key = unserialize($pay_type['pay_key']);
	    $partner =  $pay_type_key['id']['val'];		//支付商号ID

	    $paydb = System::load_app_class('wappay','pay');
	    $config = array(
	        'Partner' =>  $partner,
	        'NotifyUrl' => G_WEB_PATH . '/index.php/pay/wappay_url/houtai/',
	        'ReturnUrl' => G_WEB_PATH . '/index.php/pay/wappay_url/qiantai/',
	        'Code' => $dingdanhao, //订单号
	        'Title' => '一元众乐商品',
	        'Money' => $money,
	        'Body' => '上一元众乐就中了',
	        'ShowUrl' => G_WEB_PATH
	    );
	    $paydb->config($config);
	    $html = $paydb->get_pay();

	    //status_type  1表示余额支付成功    2表示余额不够，需要发起在线支付
	    $this->info = array('status'=>1, 'message'=>'操作成功','status_type'=>2, 'data'=>$html);
	    $this->callBack();
	}
	*/
	//手机网站充值接口
	public function wap_add_money($is_sys=false,$uid=null,$money=null,$scook=null,$indata=null)
	{

	    if( ! $is_sys){    											//通过个人中心充值
	        $indata = $this->getData();					//获取数据
	        if ($indata['pay_type'] != 'wxpay') {
	        	$this->checkSign($indata);
	        }
					$uid 		=	isset($indata['uid']) ? intval($indata['uid']) : 0;		//会员ID
					$money	=	isset($indata['money']) ? intval($indata['money']) : 0;   //金额只能是整数
					$type 	= isset($indata['pay_type']) ?$indata['pay_type'] : '';
	        $scookies = '0';
	    }else{     //购买商品充值
	        $uid = $uid ? intval($uid) : '';
	        $money = $money ? intval($money) : '';
	        $scookies = $scook;
			$type = $indata['pay_type'];
	    }
	   
	    if( ! $uid || ! $money){
	        $this->info['message'] = '参数错误1';
	        $this->callBack();
	    }

	    $this->db->Autocommit_start();
	    $members = $this->db->GetOne("SELECT * FROM `@#_member` where `uid` = '$uid' for update");
	    if(!isset($members['uid'])){
	        $this->db->Autocommit_rollback();
	        $this->info['message'] = '参数错误2';
	        $this->callBack();
	    }

	    //插入充值记录
	    $score = 0;
	    $dingdanhao = pay_get_dingdan_code('C');    //生成充值订单号
	    $time = time();


	    if ($type == 'alipay') {
	    	$t = "支付宝";
	    }elseif ($type == 'wxpay') {
	    	$t = "微信支付";
	    }elseif ($type == 'iapppay') {
	    	$t = "爱贝支付";
	    }
	    $sql = "INSERT INTO
					`@#_member_addmoney_record`
							(`uid`, `code`, `money`, `pay_type`, `status`,`time`,`score`,`scookies`,`plat_type`)
	    		VALUES
					('$uid', '$dingdanhao', '$money', '{$t}','未付款', '$time','$score','$scookies','2')";
	    $query = $this->db->Query($sql);
	    if($query){
	        $this->db->Autocommit_commit();
	    }else{
	        $this->db->Autocommit_rollback();
	        $this->info['message'] = '操作失败,请稍后再试！';
	        $this->callBack();
	    }
		 
		/*var_dump($type);
		die;*/
	    if ($type == 'iapppay') {
	    	 	$web_iapppay = System::load_app_class('web_iapppay','pay');
		     	$config = array(
			        "shopid"=>1,
			        "code"=>$dingdanhao,
			        "money"=>$money,
			        "appuserid"=>"ZL".$uid,
			        "notifyurl"=> G_WEB_PATH . "/index.php/pay/webiapppay_url/houtai",    //异步通知
			        "redirecturl"=>"http://www.yz-db.com/yungou/index.html#/tab/payResultState?code=".$dingdanhao."&uid=".$uid  //同步通知
		     	);

		     	$web_iapppay->init($config);
		     	$pay_url = $web_iapppay->get_pay();

		  		$this->info = array('status'=>1, 'message'=>'操作成功','status_type'=>2, 'data'=>$pay_url);
					//var_dump($pay_url);
					//die;
	    }elseif ($type == 'wxpay') {

	    	 $wxpay = System::load_app_class('web_wxpay','pay');
	    	 $config = array(
		        "code1"=>$dingdanhao,
		        "money"=>$money,
		     );

		     $wxpay->init($config);
		     //$info = $wxpay->getInfo();
	    	 //$this->info = array('status'=>1, 'message'=>'操作成功','status_type'=>2, 'data'=>$info);
	    }elseif ($type == 'alipay') {
	    	 $alipay = System::load_app_class('web_ali','pay');
	    	 $config = array(
		        "code"=>$dingdanhao,
		        "money"=>$money,
		        'title'=>"云购商品",
		        "NotifyUrl"=> G_WEB_PATH . "/index.php/pay/alipay_url/houtai",    //异步通知
		        "ReturnUrl"=>"http://www.yz-db.com/yungou/index.html#/tab/payResultState?code=".$dingdanhao."&uid=".$uid  //同步通知
		     );
		     $alipay->init($config);
		     $info = $alipay->getInfo();
		     $this->info = array('status'=>1, 'message'=>'操作成功','status_type'=>2, 'data'=>$info);
	    }


		
	    //status_type  1表示余额支付成功    2表示余额不够，需要发起在线支付

	    $this->callBack();
	}

	/*
	* 获取支付方式列表
	*/
	public function do_get_pay_class()
	{
	    $indata = $this->getData();			//获取数据
	    $this->checkSign($indata);			//验证签名
	    $type = trim($indata['type']);			//启用本支付的客户端    wap web app
		$PayModel = System::load_app_model('Pay','Zapi');
		$pay_list = $PayModel->get_pay_type_list($type);
		
		//$pay_list = $this->get_pay_type_list();
		$this->info = array('status'=>1, 'message'=>'请求成功', 'data'=>$pay_list);
		$this->callBack();
	}

	/*
	*  创建用户充值订单
	* param >> uid,money,pay_type
	* return >> order_code,callback_url
	*/
	public function do_add_money_code()
	{
	    $indata = $this->getData();			//获取数据
	    $this->checkSign($indata);			//验证签名
	    $uid = isset($indata['uid']) ? intval($indata['uid']) : 0;		//会员ID
	    $money = isset($indata['money']) ? intval($indata['money']) : 0;		//充值金额,必需是正整数
	    $pay_type = isset($indata['pay_type']) ? trim($indata['pay_type']) : '';		//支付方式

		if( ! $uid ||  ! $money  ||  ! $pay_type){
			$this->info['message'] = '请求失败';
			$this->callBack();
		}
		//验证用户是否存在
		$is_member = $this->db->GetCount("SELECT uid FROM @#_member WHERE `uid` = '$uid'");
		if( ! $is_member){
			$this->info['message'] = '用户不存在！';
			$this->callBack();
		}
		//判断支付方式是否正确
		$pay_list = $this->get_pay_type_list();
		foreach($pay_list as $k => $v){
			$pay_class[] = $v['pay_class'];
			$pay_names[$v['pay_class']] = $v['pay_name'];
		}

		if(!in_array($pay_type,$pay_class)){
			$this->info['message'] = '请选择正确的支付方式！';
			$this->callBack();
		}

		$pay_name = $pay_names[$pay_type];
		$this->db->Autocommit_start();
		$member = $this->db->GetOne("SELECT * FROM `@#_member` where `uid` = '$uid' for update");
		$uid=$member['uid'];
		$dingdancode = pay_get_dingdan_code('C');		//订单号
		$time = time();
		$scookies = '';
		$score = '0';
		$sql = "INSERT INTO `@#_member_addmoney_record` (`uid`, `code`, `money`, `pay_type`, `status`,`time`,`score`,`scookies`,`plat_type`) VALUES ('$uid', '$dingdancode', '$money', '$pay_name','未付款', '$time','$score','$scookies','1')";
		$query = $this->db->Query($sql);
		$rid = $this->db->insert_id();
		if($query){
			$this->db->Autocommit_commit();
		}else{
			$this->db->Autocommit_rollback();
			$this->info['message'] = '系统异常,请稍后再试';
			$this->callBack();
		}
		$back_url  = G_WEB_PATH.'/index.php/pay/iapppay_url/houtai';		//后台回调
		//$back_url = "http://www.yz-db.com/iapppay/index.php";
		$data = array('order_code'=>$dingdancode, 'callback_url'=>$back_url,'rid'=>$rid);
		$this->info = array('status'=>1, 'message'=>'请求成功', 'data'=>$data);
		$this->callBack();
	}

	/*
	* 通过充值订单号来查找购买的商品 20151229
	*/
	public function do_get_shoplist()
	{
	    $indata = $this->getData();			//获取数据
	    $this->checkSign($indata);			//验证签名
	    $uid = isset($indata['uid']) ? intval($indata['uid']) : '';		//uid
	    $code = isset($indata['code']) ? trim($indata['code']) : '';		//订单号

	    if( ! $uid || ! $code){
	        $this->info['message'] = '参数错误';
	        $this->callBack();
	    }

	    $tag = substr($code,0,1);
	    if($tag == 'C'){ //通过wap端充值
	        $wh = " AND `cz_code`='$code' ";
	    }else{ //直接支付
	        $wh = " AND `code` = '$code' ";
	    }

	    $renci = 0;
	    $sql = "SELECT shopid,shopname,shopqishu,gonumber,goucode FROM @#_member_go_record WHERE `uid`='$uid' AND `status` like '%已付款%' $wh ";
	    $shoplist = $this->db->GetList($sql);
	    $count = count($shoplist);
	    if( ! $count){
	        $this->info['message'] = '操作失败';
	        $this->callBack();
	    }
	    foreach($shoplist as $k => $v){
	        $renci += intval($v['gonumber']);
	        if($v['gonumber'] > 6){
	            $shoplist[$k]['goucode'] = substr($v['goucode'],0,53);
	        }
	        $shoplist[$k]['shopname'] = str_replace('&nbsp;',' ',$v['shopname']);
	    }

	    $data['zongrenci'] = $renci;
	    $data['count'] = $count;
	    $data['list'] = $shoplist;

	    $this->info = array('status'=>1, 'message'=>'请求成功','data'=>$data);
	    $this->callBack();
	}

	/*
	* 不加入购物车 立即结算
	* 2016/01/16
	*/
	public function do_new_balance()
	{
	    $indata = $this->getData();			//获取数据
	    $this->checkSign($indata);			//验证签名
	    $uid = isset($indata['uid']) ? intval($indata['uid']) : '';		//uid
	    $shopid = isset($indata['shopid']) ? intval($indata['shopid']) : '';		//商品id
	    $number = isset($indata['number']) ? intval($indata['number']) : 0;		//购买人次

	    if(!$uid || !$shopid || !$number){
	        $this->info['message'] = "请求失败";
	        $this->callBack();
	    }

	    $uinfo = $this->get_user_fields($uid,'uid,money');
	    if(!isset($uinfo['uid'])){ //用户不存在
	        $this->info['messgae'] = "请求失败";
	        $this->callBack();
	    }

	    $shopinfo = $this->db->GetOne("SELECT id,title,shenyurenshu,is_shi,yunjiage FROM @#_shoplist WHERE `id`=$shopid LIMIT 1");
	    if($shopinfo['shenyurenshu'] == 0){    //该商品已卖完
	        $this->info['status'] = 2;
	        $this->info['message'] = "该商品已被抢空";
	        $this->callBack();
	    }

	    if($shopinfo['is_shi'] == 1){  //十元专区  对数量处理
	        $number = $number < 10 ? 10 : ($number % 10 == 0 ? $number : intval($number / 10) * 10);
	    }

	    $number = $number > $shopinfo['shenyurenshu'] ? $shopinfo['shenyurenshu'] : $number;   //与剩余数量对比

	    $total_money = $number * intval($shopinfo['yunjiage']);

	    $pay_money = $uinfo['money'] > $total_money ? 0 : $total_money - $uinfo['money'];  //余额扣除后，还需支付多少钱

	    $list[0]['id'] = $shopinfo['id'];
	    $list[0]['title'] = str_replace('&nbsp;', ' ', $shopinfo['title']);
	    $list[0]['gonumber'] = $number;

	    $data = array('total_money' => $total_money,'user_money' => intval($uinfo['money']), 'pay_money'=>$pay_money, 'list'=>$list);

	    $this->info = array('status'=>1, 'message'=>'请求成功', 'data'=>$data);
	    $this->callBack();
	}

	/*
	* 不加入购物车 立即结算>>发起余额支付
	* 2016/01/16
	*/
	public function do_new_pay()
	{
	    $indata = $this->getData();			//获取数据
	    $this->checkSign($indata);			//验证签名
	    $uid = isset($indata['uid']) ? intval($indata['uid']) : '';		//uid
	    $pwd = isset($indata['pwd']) ? trim($indata['pwd']) : '';		//密码 格式：6位随机字符+用户密码+6位随机字符
	    $intoken = isset($indata['token']) ? trim($indata['token']) : '';		//md5(uid+用户密码)
	    $shopid = isset($indata['shopid']) ? intval($indata['shopid']) : '';		//商品id
	    $number = isset($indata['number']) ? intval($indata['number']) : 0;		//购买人次

	    if(!$uid || !$pwd || !$intoken || !$shopid || !$number){
	        $this->info['message'] = "请求失败";
	        $this->callBack();
	    }

	    $this->db->Autocommit_start();
	    $uinfo = $this->db->GetOne("select * from `@#_member` where `uid`=$uid for update");
	    $this->members = $uinfo;

	    if(!isset($uinfo['uid'])){ //用户不存在
	        $this->db->Autocommit_rollback();
	        $this->info['message'] = "请求失败";
	        $this->callBack();
	    }

	    $is_ios = $this->is_ios($indata);
	    $is_android = $this->is_android($indata);
	    $temp_password = '';
	    if($is_ios  || $is_android){   //APP平台对密码的处理    (xxxxxx用户密码xxxxxx)
	        $temp_password = $this->decode_password($pwd);
	        $password = md5($temp_password);
	    }

	    $token = md5($uid.$temp_password);
	    if($token != $intoken){
	        $this->db->Autocommit_rollback();
	        $this->info['message'] = "请求失败";
	        $this->callBack();
	    }
	    if($password !== $uinfo['password']){  //密码错误
	        $this->db->Autocommit_rollback();
	        $this->info['message'] = "请求失败";
	        $this->callBack();
	    }

	    $shopinfo = $this->db->GetOne("SELECT * FROM @#_shoplist WHERE `id`=$shopid LIMIT 1");
	    if($shopinfo['shenyurenshu'] == 0){    //该商品已卖完
	        $this->db->Autocommit_rollback();
	        $this->info['message'] = "该商品已被抢空";
	        $this->callBack();
	    }
	    if($shopinfo['is_shi'] == 1){  //十元专区  对数量处理
	        $number = $number < 10 ? 10 : ($number % 10 == 0 ? $number : intval($number / 10) * 10);
	    }
	    $number = $number > $shopinfo['shenyurenshu'] ? $shopinfo['shenyurenshu'] : $number;   //与剩余数量对比
	    $total_money = $number * intval($shopinfo['yunjiage']);

	    //余额不足
	    if($uinfo['money'] < $total_money){
	        $this->db->Autocommit_rollback();
	        $this->info['message'] = "余额不足";
	        $this->callBack();
	    }

	    //购买的商品信息
	    $shoplist[] = array(
	        'id' => $shopinfo['id'],
	        'title' => str_replace('&nbsp;',' ',$shopinfo['title']),
	        'yunjiage' => $shopinfo['yunjiage'],
	        'codes_table' => $shopinfo['codes_table'],
	        'canyurenshu' => $shopinfo['canyurenshu'],
	        'zongrenshu' => $shopinfo['zongrenshu'],
	        'cart_gorenci' => $number,
	        'maxqishu' => $shopinfo['maxqishu'],
	        'qishu' => $shopinfo['qishu']
	    );

	    //余额充足时 支付
	    $this->shoplist = $shoplist;
	    $this->moneyCount = $total_money;

	    $shenyu_money = $this->members['money'] - $total_money;	//剩余金额
	    $time = time();

	    $query_1 = $this->set_app_dingdan('账户','A');	//添加购买记录并且生成众乐码

	    //修改会员金额
	    $query_2 = $this->db->Query("UPDATE `@#_member` SET `money`='$shenyu_money' WHERE (`uid`='$uid')");			//金额
	    $query_3 = $info = $this->db->GetOne("SELECT * FROM  `@#_member` WHERE (`uid`='$uid') LIMIT 1");
	    $query_4 = $this->db->Query("INSERT INTO `@#_member_account` (`uid`, `type`, `pay`, `content`, `money`, `time`) VALUES ('$uid', '-1', '账户', '众乐了商品', '$total_money', '$time')");
	    $query_5 = true;

	    //修改商品的信息
	    $goods_count_num = 0;
	    foreach($this->shoplist as $shop){
	        if($shop['canyurenshu'] >= $shop['zongrenshu'] && $shop['maxqishu'] >= $shop['qishu']){
	            $this->db->Query("UPDATE `@#_shoplist` SET `canyurenshu`=`zongrenshu`,`shenyurenshu` = '0' where `id` = '$shop[id]'");
	        }else{
	            $shenyurenshu = $shop['zongrenshu'] - $shop['canyurenshu'];
	            $query = $this->db->Query("UPDATE `@#_shoplist` SET `canyurenshu` = '$shop[canyurenshu]',`shenyurenshu` = '$shenyurenshu' WHERE `id`='$shop[id]'");
	            if(!$query)$query_5=false;
	        }
	        $goods_count_num += $shop['cart_gorenci'];	//这里应该是统计网站的购买人次
	    }

	    $dingdancode=$this->dingdancode;
	    //修改订单状态
	    $query_6 = $this->db->Query("UPDATE `@#_member_go_record` SET `status`='已付款,未发货,未完成' WHERE `code`='$dingdancode' and `uid` = '$uid'");
	    $query_7 = $this->dingdan_query;
	    $query_8 = $this->db->Query("UPDATE `@#_caches` SET `value`=`value` + $goods_count_num WHERE `key`='goods_count_num'");
	    $this->goods_count_num = $goods_count_num;

	    if($query_1 && $query_2 && $query_3 && $query_4 && $query_5 && $query_6 && $query_7 && $query_8){
	        if($info['money'] == $shenyu_money){
	            $this->db->Autocommit_commit();
	            foreach($this->shoplist as $shop){
	                if($shop['canyurenshu'] >= $shop['zongrenshu'] && $shop['maxqishu'] >= $shop['qishu']){
	                    /**时时彩LVDENG修改**/
	                    $post_arr= array("gid"=>$shop['id']);
	                    _g_triggerRequest(WEB_PATH.'/pay/pay_user_ssc_time/init',false,$post_arr);
	                    /**时时彩LVDENG修改**/
	                }
	            }
	            //如果使用余额支付成功，则返回参与信息
	            //status_type  1表示余额支付成功    2表示余额不够，需要发起在线支付
	            $canyu_info = $this->canyu_info($uid,$dingdancode);
	            $this->info = array('status'=>1, 'message'=>'支付成功', 'data'=>$canyu_info);
	            $this->callBack();
	        }else{
	            $this->db->Autocommit_rollback();
	            $this->info['message'] = '系统异常,请稍后再试';
	            $this->callBack();
	        }
	    }else{
	        $this->db->Autocommit_rollback();
	        $this->info['message'] = '系统异常,请稍后再试';
	        $this->callBack();
	    }

	}


	/*****************************H5相关接口 没有使用******************************************/
	/*
	* H5添加商品至购物车
	*/
	public function addToCart()
	{
		/*if( ! is_ajax() ){
			$this->info['message'] = '非AJAX访问';
			$this->callBack();
		}*/
		$indata = $this->getData();			//获取数据
		$id = isset($indata['id']) ? intval($indata['id']) : '';
		$num = isset($indata['num']) ? intval($indata['num']) : 1;
		$info = isset($indata['info']) ? trim($indata['info']) : '';		//多个商品格式如  id,num|id,num|id,num

		if( ( ! $id || ! $num) && ! $info){
			$this->info['message'] = '缺少参数';
			$this->callBack();
		}

		$data = $this->combineData($id,$num,$info);	//拼接data
		$temp = array();

		$cartlist = $this->getWapCart(1);	//获取cookie中的购物车信息
		$temp = $cartlist;
		$count = count($cartlist);

		foreach($data as $k => $v){
			$id = $v['id'];
			$num = $v['num'];
			$goods = $this->goods_info($id," id,qishu,zongrenshu,shenyurenshu,canyurenshu,default_renci,yunjiage,is_shi,title,thumb ");
			if(!isset($goods['id'])) continue;

			$num = $this->countGoodsNum($num, $goods['default_renci'], $goods['shenyurenshu']);

			if($count > 0 && isset($temp[$id])){
				$temp[$id]['gonumber'] += $num;
			}else{
				$temp[$id]['gonumber'] = $num;
				$count += 1;
			}
			$temp[$id]['id'] = $goods['id'];
			$temp[$id]['qishu'] = $goods['qishu'];
			$temp[$id]['title'] = str_replace('&nbsp;',' ',$goods['title']);
			$temp[$id]['shenyurenshu'] = $goods['shenyurenshu'];
			$temp[$id]['canyurenshu'] = $goods['canyurenshu'];
			$temp[$id]['zongrenshu'] = $goods['zongrenshu'];
			$temp[$id]['yunjiage'] = intval($goods['yunjiage']);
			$temp[$id]['is_ten'] = $goods['is_shi'];
			$temp[$id]['wap_ten'] = intval($goods['is_shi']) ? 10 : 1;	//h5需要的字段，判断是否是10元专区
			$temp[$id]['thumb'] = strpos($goods['thumb'],G_UPLOAD_PATH)===false?G_UPLOAD_PATH.'/'.$goods['thumb']:$goods['thumb'];
			$temp[$id]['default_renci'] = $goods['default_renci'];
			$temp[$id]['gonumber'] = $temp[$id]['gonumber'] > $goods['shenyurenshu'] ? $goods['shenyurenshu'] : $temp[$id]['gonumber'];	//不能大于剩余人数
		}

		setcookie('Cartlist',json_encode($temp));

		$this->info = array('status'=>1,'message'=>'添加成功','data'=>$count);
		$this->callBack();
	}

	/*
	* H5修改购物车信息(购买人次)
	*/
	public function modTheCart()
	{
		$indata = $this->getData();			//获取数据
		$id = isset($indata['id']) ? intval($indata['id']) : '';
		$num = isset($indata['num']) ? intval($indata['num']) : 1;
		$info = isset($indata['info']) ? trim($indata['info']) : '';		//多个商品格式如  id,num|id,num|id,num

		if( (! $id  ||  ! $num) &&  ! $info ){
			$this->info['message'] = '请求失败';
			$this->callBack();
		}

		$cartlist = $this->getWapCart();

		$data = $this->combineData($id,$num,$info);	//拼接data

		//从新组合购物车信息
		foreach($data as $k => $v){
			if(!isset($v['id']) ||  !isset($v['num'])) continue;

			$id = $v['id'];
			$num = $v['num'];

			if(!isset($cartlist[$id])) continue;

			$goods = $this->goods_info($id, ' id,shenyurenshu,default_renci ');
			if( ! isset($goods['shenyurenshu'])){
				$this->info['message'] = '系统异常,请稍后再试';
				$this->callBack();
			}

			$num = $this->countGoodsNum($num, $goods['default_renci'], $goods['shenyurenshu']);

			$cartlist[$id]['gonumber'] = $num;
			$cartlist[$id]['shenyurenshu'] = $goods['shenyurenshu'];
		}

		setcookie('Cartlist',json_encode($cartlist));

		$this->info = array('status'=>1, 'message'=>'修改成功');
		$this->callBack();
	}

	/*
	* H5获取购物车中商品列表
	*/
	public function showCartList()
	{
		$cartlist = $this->getWapCart();
		$this->info = array('status'=>1, 'message'=>'请求成功', 'data'=>$cartlist);
		$this->callBack();
	}

	/*
	* H5删除购物车中的商品
	*/
	public function delCart(){
		$indata = $this->getData();			//获取数据
		$ids = isset($indata['id']) ? trim($indata['id']) : '';		//多个请用逗号隔开   格式  id,id,id,id

		$cartlist = $this->getWapCart();
		if($ids){
			$ids = str_replace('，',',',$ids);
			$id = explode(',',$ids);
			foreach($id as $k=>$v){
				if(intval($v) && isset($cartlist[$v])){
					unset($cartlist[$v]);
				}
			}
		}else{
			$cartlist = array();
		}
		setcookie('Cartlist',json_encode($cartlist));
		$this->info = array('status'=>1,'message'=>'删除成功');
		$this->callBack();
	}

	/*****************************************************************************************/
	//获取某个商品的详细信息
	private function goods_info($id = 0, $field = '')
	{
		if(!$field){
			$field = '*';
		}
		$sql = "SELECT $field FROM @#_shoplist WHERE `id` = $id LIMIT 1";
		$data = $this->db->GetOne($sql);
		return $data;
	}

	//获取某个用户的清单列表及总数
	private function cart_list($cache_id='', $uid=''){
		$data = array();
		if( ! $cache_id &&  ! $uid){
			return $data;
		}
		if($cache_id)	$wh = " ac.cache_id = '$cache_id' ";
		if($uid) $wh = " ac.uid = '$uid' ";
		if($cache_id && $uid) $wh = " ac.cache_id = '$cache_id'  OR  ac.uid = '$uid'  ";
		$fields = " ac.*,sl.is_shi,sl.shenyurenshu as shenyu,sl.yunjiage as jiage,sl.canyurenshu as canyu,sl.codes_table,sl.qishu as qs,sl.maxqishu,sl.zongrenshu as zongshu ";
		$sql = "SELECT $fields FROM @#_app_cart AS ac LEFT JOIN @#_shoplist AS sl ON sl.id = ac.shopid WHERE $wh AND sl.shenyurenshu > 0 ";
		$list = $this->db->GetList($sql);
		$wh = '';
		if(count($list)){
			foreach($list as $k=>$v){
				$list[$k]['title'] = str_replace(array('&nbsp;',';'),array(' ',''),$v['title']);
				$list[$k]['yunjiage'] = intval($v['jiage']);
				$list[$k]['is_ten'] = intval($v['is_shi']) > 0 ? 1 : 0;	//是否是10元专区
				$list[$k]['maxqishu'] = intval($v['maxqishu']);
				$list[$k]['qishu'] = intval($v['qs']);
				$list[$k]['zongrenshu'] = intval($v['zongshu']);
				$list[$k]['canyurenshu'] = intval($v['canyu']);

				$config	= array(
					'id'	=>	$v['shopid'],
					'uid'	=>  $uid,
					'qishu'	=>	$list[$k]['qishu'],
					'syrs'	=>	$v['shenyu'],
					'is_ten'=>	$v['is_shi'],
				);
				//获取该期产品  用户所能购买的最大数量
				$list[$k]['wap_ten'] = $this->get_user_can_buy_number($config);

				if($v['gonumber'] > $list[$k]['wap_ten']){
					$list[$k]['gonumber'] = $list[$k]['wap_ten'];
					$this->db->Query("UPDATE @#_app_cart SET `gonumber` = " . $list[$k]['wap_ten'] . " , `canyurenshu` = ".$list[$k]['canyurenshu']." , `shenyurenshu` = " . $v['shenyu'] . " WHERE `id` = " . $v['id']);
				}else{
					$list[$k]['gonumber'] = intval($list[$k]['gonumber']);
				    $this->db->Query("UPDATE @#_app_cart SET `canyurenshu` = ".$list[$k]['canyurenshu']." , `shenyurenshu` = " . $v['shenyu'] . " WHERE `id` = " . $v['id']);
				}


				$not_del .= ',' . $v['id'];
				unset($list[$k]['jiage'],$list[$k]['canyu'],$list[$k]['zongshu'],$list[$k]['qs'],$list[$k]['is_shi']);
				if($list[$k]['shenyu'] <= 0){
				    unset($list[$k]);
				}else{
				    unset($list[$k]['shenyu']);
				}
				if($list[$k]['wap_ten'] == 0){

					$AppCartModel = System::load_app_model('AppCart','yunapi');
					$AppCartModel->del_one($v['id']);

					//$sql = 'delete from `@#_app_cart` where id='.$v['id'];
					//$this->db->Query($sql);
					unset($list[$k]);

				}
			}
			$data = $list;
			$wh = " AND `id` NOT IN(" . ltrim($not_del,',') . ") ";
		}
		//清空人数已满的商品
		$this->db->Query("DELETE FROM @#_app_cart WHERE `cache_id` = '$cache_id'  $wh");
		return $data;
	}

	/*生成订单*/
	private function set_app_dingdan($pay_type='',$dingdanzhui=''){
			$uid=$this->members['uid'];
			$uphoto = $this->members['img'];
			$username = addslashes(get_user_name($this->members));
			$insert_html='';
			$this->dingdancode = $dingdancode= pay_get_dingdan_code($dingdanzhui);		//订单号

			if(count($this->shoplist)>1){
					$dingdancode_tmp = 1;	//多个商品相同订单
			}else{
					$dingdancode_tmp = 0;	//单独商品订单
			}

			$ip =  $this->members['user_ip'];
			/*订单时间*/
			$time=sprintf("%.3f",microtime(true));
			$this->MoenyCount=0;
			foreach($this->shoplist as $key=>$shop){
					$ret_data = array();
					pay_get_shop_codes($shop['cart_gorenci'],$shop,$ret_data);
					$this->dingdan_query = $ret_data['query'];
					if(!$ret_data['query'])$this->dingdan_query = false;
					$codes = $ret_data['user_code'];									//得到的众乐码
					$codes_len= intval($ret_data['user_code_len']);
					$money=$codes_len * $shop['yunjiage'];								//单条商品的总价格
					$this->MoenyCount += $money;										//总价格
					$status='未付款,未发货,未完成';
					$shop['canyurenshu'] = intval($shop['canyurenshu']) + $codes_len;
					$shop['goods_count_num'] = $codes_len;
					$shop['title'] = str_replace('&nbsp;',' ',$shop['title']);

					$this->shoplist[$key] = $shop;
					if($codes_len){
						$insert_html.="('$dingdancode','$dingdancode_tmp','$uid','$username','$uphoto','$shop[id]','$shop[title]','$shop[qishu]','$codes_len','$money','$codes','$pay_type','$ip','$status','$time'),";
					}
			}
			$sql="INSERT INTO `@#_member_go_record` (`code`,`code_tmp`,`uid`,`username`,`uphoto`,`shopid`,`shopname`,`shopqishu`,`gonumber`,`moneycount`,`goucode`,`pay_type`,`ip`,`status`,`time`) VALUES ";
			$sql.=trim($insert_html,',');

			//$this->db->Query("set global max_allowed_packet = 2*1024*1024*10");
			return $this->db->Query($sql);
	}

	//获取指定会员指定订单的参与信息
	private function canyu_info($uid='', $code = ''){
		$num = 0;
		$renci = 0;
		$data = array('count'=>0, 'zongrenci'=>0, 'list'=>array());
		$info = $this->db->GetList(" SELECT shopid,shopname,shopqishu,gonumber,goucode FROM @#_member_go_record WHERE `uid` = '$uid' AND `code` = '$code' ");
		if( ! count($info) ){
			return $data;
		}
		foreach($info as $k => $v){
			$num += 1;
			$renci += intval($v['gonumber']);
			$goucode = $v['goucode'];
			if($v['gonumber'] >= 6){
				$goucode = '';
				$ucodes = explode(',',$v['goucode']);
				foreach($ucodes as $key => $val){
					if($key >= 6){
						break;
					}
					$goucode .= ',' . $val;
				}
			}
			$info[$k]['goucode'] = ltrim($goucode,',');
			$info[$k]['shopname'] = str_replace('&nbsp;', ' ', $v['shopname']);
		}
		$data['count'] = $num;
		$data['zongrenci'] = $renci;
		$data['list'] = $info;
		return $data;
	}

	//修改购物清单中的数量
	private function update_cart_info($uid='',$info = '',$cache_id='')
	{
		if(intval($uid) && ! empty($info)){
			$infos = explode('|',$info);
			foreach($infos as $k => $v){
				if( ! empty($v)){
					$goods = explode(',',$v);
					$id = isset($goods[0]) ? intval($goods[0]) : 0;
					$num = isset($goods[1]) ? intval($goods[1]) : 0;

					//android
					$numbers = $this->db->GetOne("SELECT sl.id,sl.is_shi FROM @#_app_cart as ac, @#_shoplist as sl WHERE ac.id='$id' AND sl.id = ac.shopid LIMIT 1");
					if(isset($numbers['id']) && $numbers['is_shi'] == 1 && $num > 0 ){
					    $num = $num > 5 ? 5 : $num;
					}

					if($id && $num){
						$this->db->Query("UPDATE @#_app_cart SET `gonumber` = '$num' WHERE `id` = '$id' AND ( `uid` = '$uid' OR `cache_id`='$cache_id' ) ");
					}
				}
			}
		}
	}

	//多个商品加入清单
	private function add_to_cart($uid = '', $cache_id = '', $arr = array())
	{
		$info = array('status'=>0, 'message'=>'添加失败', 'data'=>'');
		if(!$uid &&  !$cache_id){
			return $info;
		}
		$list = array();
		if(count($arr)){
			foreach($arr as $k=>$v){
				$goods = $this->goods_info($v['id'], 'id,title,thumb,zongrenshu,shenyurenshu,canyurenshu,yunjiage,qishu,is_shi');
				if( isset($goods['id']) && $goods['shenyurenshu'] > 0){
					$list[$k] = $goods;
					$list[$k]['num'] = $v['num'];
					if($goods['is_shi'] == 1){
					    $list[$k]['num'] = $list[$k]['num'] > 5 ? 5 : $list[$k]['num'];
					}
				}
			}
			if(count($list)){
				foreach($list as $k=>$goods){
					$id = $goods['id'];
					$num = $goods['num'];
					$title = str_replace('&nbsp',' ',$goods['title']);
					$thumb = strpos($goods['thumb'],G_UPLOAD_PATH) === false ? G_UPLOAD_PATH .'/'. $goods['thumb'] : $goods['thumb'];
					$zongrenshu = $goods['zongrenshu'];
					$shenyurenshu = $goods['shenyurenshu'];
					$canyurenshu = $goods['canyurenshu'];
					$yunjiage = $goods['yunjiage'];
					$qishu = $goods['qishu'];

					$or = '';
					if($uid) $or = " AND  `uid` = '$uid' ";
					if($cache_id) $or = " AND `cache_id` = '$cache_id' ";
					if($uid && $cache_id) $or = " AND (`uid` = '$uid' OR `cache_id` = '$cache_id') ";

					$cart_info = $this->db->GetOne("SELECT `id`,`gonumber` FROm @#_app_cart WHERE `shopid` = '$id' " . $or);
					if($cart_info['id']){
						$gonumber = $cart_info['gonumber'] + $num;
						$gonumber = $gonumber > $goods['shenyurenshu'] ? $goods['shenyurenshu'] : $gonumber;

						$wh = '';
						if( $uid) $wh = " AND `uid` = '$uid' ";
						if( $cache_id) $wh = " AND `cache_id` = '$cache_id' ";
						if( $uid && $cache_id) $wh = " AND `uid` = '$uid'  AND  `cache_id` = '$cache_id' ";

						$sql = "UPDATE @#_app_cart SET `gonumber` = $gonumber, `shenyurenshu` = $shenyurenshu , `canyurenshu` = $canyurenshu WHERE `shopid` = '$id'  " . $wh;
						$this->db->Query($sql);	//只判断是否执行了sql语句,不判断影响行数
					}else{
						$sql = "INSERT INTO @#_app_cart(shopid,title,thumb,zongrenshu,shenyurenshu,canyurenshu,uid,cache_id,gonumber,yunjiage,qishu) ".
								"VALUES($id,'$title','$thumb',$zongrenshu,$shenyurenshu,$canyurenshu,'$uid','$cache_id',$num,$yunjiage,$qishu)";
						$this->db->Query($sql);
					}
				}
				//统计购物车数量
				$where = " `cache_id` = '$cache_id' ";
				if($uid){
					$where .= " OR `uid` = '$uid' ";
				}
				$count = $this->db->GetCount("SELECT id FROM @#_app_cart WHERE $where ");	//购物车商品总数
				if($count){
					$info = array('status'=>1, 'message'=>'加入清单成功', 'data'=>intval($count) );
				}
			}
		}
		return $info;
	}
	/**
	 * 获取用户可以购买的数量
	 * @param 	(array)	$config array(
	 * 							id		=>	$id		产品的id			(int)
	 * 							uid 	=> 	$uid   	用户的id			(int)
	 * 							syrs	=>	$syrs	产品的剩余数量		(int)
	 * 							is_ten	=>	$is_ten 是否是限购产品		(int)
	 * 							qishu   =>  $qishu  商品期数			(int)
	 * 							cid		=>	$cid	购物车   当前数据的ID	(int)
	 * 							)
	 * @return	(int)	$wap_ten用户剩余的购买数量
	 */
	public function get_user_can_buy_number($config){

		$syrs	=	$config['syrs'];
		$xiangou_money = 5;			//限购数量

		if($config['uid'] > 0){
			//获取用户的购买数量
			$user_bought_number = $this->get_member_go_record($config['id'],$config['uid'],$config['qishu']);
			$user_bought_number = $user_bought_number['gonumber'];
		}else{
			$user_bought_number = 0;
		}
		$wap_ten = $syrs;
		//限购
		if($config['is_ten'] == 1){									//是否是限购专区
			$xiangou_number = $xiangou_money - $user_bought_number;
			if($xiangou_number > 0){
				//用户购买本期产品的剩余次数  与   本期产品剩余人次
				$wap_ten = ($xiangou_number > $syrs) ? $syrs : $xiangou_number;

			}else{
				//用户购买的次数已超额
				$wap_ten = 0;
			}
		}

		return $wap_ten;
	}


	/**************************************/
	//获取购物车中的商品
	protected function getWapCart($sy=0)
	{
		$cartlist = isset($_COOKIE['Cartlist']) ? json_decode(stripslashes($_COOKIE['Cartlist']),true) : array();
		$count = count($cartlist);
		if( ! $count && !$sy){
			$this->info['message'] = '购物车是空的';
			$this->callBack();
		}
		return $cartlist;
	}

	//拼接data
	protected function combineData($id,$num,$info)
	{
		if($info){
			$infos = explode('|',str_replace('，',',',$info));
			foreach($infos as $k => $v){
				if(!empty($v)){
					$temp = explode(',',$v);
					$id = isset($temp[0]) ? intval($temp[0]) : '';
					$num = isset($temp[1]) ? intval($temp[1]) : '';
					if(!$id || !$num) continue;
					$data[$k]['id'] = $id;
					$data[$k]['num'] = $num;
				}
			}
		}else{
			$data = array(array('id'=>$id, 'num'=>$num));	//默认
		}
		return $data;
	}

	//计算人数
	protected function countGoodsNum($num,$default,$total)
	{
		if($num <= $default){
			$num = $default;
		}elseif($num > $default && $num < $total){
			$num = $num % $default == 0 ? $num : intval($num / $default) * $default;
		}else{
			$num = $total;
		}
		return $num;
	}
}
