<?php
/*
* 会员相关API
* 后期需要优化的内容：1.不同场景选择不同尺寸的用户头像
*/
defined('G_IN_SYSTEM')or exit('no');
System::load_app_class('api');
System::load_app_fun('myfun');

class Member extends api{
	/*
	* 1、发送手机注册验证码
	* 参数>>手机号
	*/
	public function do_send_mobile_code()
	{
		$indata = $this->getData();	//获取数据
		//$this->checkSign($indata);		//验证签名
		$mobile = isset($indata['mobile']) ? trim($indata['mobile']) : 0;		//手机号
		
		//验证此号码是否已经注册过了
		$is_reg = $this->check_mobile_is_register($mobile);
		if($is_reg['status'] == 0){
			$this->info['status'] = 0;
			$this->info['message']=$is_reg['message'];
			$this->callBack();
			die;
		}
		$data = $this->send_mobile_code($mobile,'template_mobile_reg');	//发送验证码
		$this->info['status']  = isset($data[0]) && $data[0] == 1 ? 1 : 0;
		$this->info['message'] = $data[1];
		$this->callBack();
	}
	
	/*
	* 2、检验手机短信验证码
	* 参数>>手机号、验证码
	* 20151207  暂时取消这个接口
	*/
	/*public function do_check_mobile_code()
	{
		$indata = $this->getData();	//获取数据
		$this->checkSign($indata);		//验证签名
		$mobile = isset($indata['mobile']) ? trim($indata['mobile']) : 0;		//手机号
		$code = isset($indata['code']) ? trim($indata['code']) : 0;		//验证码
		
		$is_pass = $this->check_code_is_pass($mobile,$code);
		if($is_pass){
			$this->info = array('status'=>1, 'message'=>'验证码正确！');
		}else{
			$this->info['message'] = '验证码错误！';
		}
		$this->callBack();
	}*/
	
	/*
	* 3、通过手机号注册会员
	* 注册流程：填写手机号->获取验证码->填写验证码->如果验证码正确->填写密码->注册成功（并登录）
	* 参数>> 会员手机号、密码
	*/
	public function do_register()
	{
		$indata = $this->getData();	//获取数据
		$this->checkSign($indata);		//验证签名
		
		$mobile 	= isset($indata['mobile']) ? trim($indata['mobile']) : 0;		//手机号
		$password 	= isset($indata['password']) ? trim($indata['password']) : 0;		//密码
		$code 		= isset($indata['code']) ? trim($indata['code']) : 0;		//验证码
		$yaoqing 	= isset($indata['yaoqing']) ? intval($indata['yaoqing']) : 0;		//邀请码
		$su 		= isset($indata['su']) ? trim($indata['su']) : 'yyzl';    //渠道名称  非必填	默认yyzl
		$sk 		= isset($indata['sk']) ? trim($indata['sk']) : '';    //渠道关键词  非必填
		$reg_plat 	= $this->get_platform($indata['partner']);      //注册平台   如：安卓、苹果、手机网站、WEB网站
		$come 		= isset($indata['come'])? trim($indata['come']) : '';
		
		
		if( ! $mobile ||  ! $password ||  ! $code){
			$this->info['message'] = '手机号和密码以及验证码均不能为空';
			$this->callBack();
		}
		
		//验证此号码是否已经注册过了
		$is_reg = $this->check_mobile_is_register($mobile);
		if($is_reg['status'] == 0){
			$this->info['message'] = $is_reg['message'];
			$this->callBack();
		}
		
		//检验提交的验证码是否正确
		$is_pass = $this->check_code_is_pass($mobile,$code);
		if( ! $is_pass){
			$this->info['message'] = '验证码错误';
			$this->callBack();
		}
		
		//确认该手机是否填写过验证码
		$is_reg = $this->db->GetCount("SELECT `id` FROM `@#_mobile_code` WHERE `mobile` = '$mobile' AND `is_reg` = 1");	
		if( ! $is_reg){
			$this->info['message'] = '数据来源不合法,请稍后再试';
			$this->callBack();
		}
		
		if($indata['partner'] == 'ZL999IOSAPI' || $indata['partner'] == 'ZL888ANDROID'){ //ios平台对密码的处理    (xxxxxx用户密码xxxxxx)
		    $temp_password = $this->decode_password($password);
		    $password = md5($temp_password);
		}else{
		    $password = md5($password);
		}
		//活动注册赠送金额
		$money = 3;

		$time = time();
		$user_ip = _get_ip_dizhi();		//获取地址和ip 格式如：  广东深圳,192.168.1.50
		$img = 'photo/member.jpg';	//头像
        $username = '财神'.mt_rand(10000,99999);

		$sql = "INSERT INTO 
					`@#_member`(username,mobile,password,mobilecode,user_ip,time,reg_key,yaoqing,login_time,img,reg_plat,money) 
				VALUES
					('$username','$mobile','$password','1','$user_ip',$time,'$mobile','$yaoqing',$time,'$img','$reg_plat','$money')";
		$this->db->Query($sql);
		$id = $this->db->insert_id();
		$sql = "INSERT INTO 
						`@#_active`(uid,type,cash_value,is_active,r_time,mobile,is_use,money) 
					VALUES
						('$id',1,$money,1,'$time','$mobile',1,$money)";
		$this->db->Query($sql);					
		$id2 = $this->db->insert_id();
		if( ! $id || !$id2){	//注册失败
			$this->info['message'] = '注册失败,请稍后再试';
			$this->callBack();
		}
		
		//获取渠道信息  2016/01/02
		if(!empty($su)){
			/* 一些渠道名称对应关系：
					午夜艳视  wuyeyanshi
					腾讯应用宝 qq
					一元众乐  yyzl
					酷赚锁屏  kuzhuan 或 zlkzsp
					亿智蘑菇 	zlyzmg
					儒豹浏览器 	rbower
					百度推广 	zlbdtg
					信号增强器 	zlxhzqq
					陌陌 	zlmomo
			*/
			$su = $su == 'kuzhuan' ? 'zlkzsp' : $su;	//酷赚锁屏->kunzhuan或zlkzsp
			
		    $s_info = array('id'=>0,'s_name'=>'其他','s_ename'=>'');
		    $source = $this->db->GetOne("SELECT * FROM @#_source_list WHERE `s_ename`='$su' LIMIT 1");
		    if(isset($source['s_id'])){
		        $s_info = $source;
		    }
		    $addtime = time();
		    $insql = "INSERT INTO @#_data_statistics(s_id,s_name,s_ename,s_words,uid,addtime) ".
		            " VALUES('$s_info[s_id]','$s_info[s_name]','$s_info[s_ename]','$sk','$id','$addtime')";
		    
		    $this->db->Query($insql);
		}
		
		$data = array('uid'=>$id, 'username'=>$mobile, 'img'=>$img, 'mobile'=>$mobile, 'email'=>'');	//返回用户信息
		
		System::load_app_fun('member','member');
		//send_ticket($id,$mobile);
		
		if($indata['partner'] == 'ZLWEBAPITOWEB'){ //web 注册的话，直接存cookie
		    $userinfo = $this->get_user_fields($id,'uid,mobile,password,reg_key,email');
			_setcookie("uid",_encrypt($userinfo['uid']),60*60*24*7);
    		_setcookie("ushell",_encrypt(md5($userinfo['uid'].$userinfo['password'].$userinfo['reg_key'].$userinfo['email'])),60*60*24*7);
		}
		if($indata['partner'] == 'ZLAPITOH5WAP'){
			_setcookie("holdUID",$data['uid'],60*60*24*7);
		}
		$this->info = array('status'=>1, 'message'=>'注册成功！', 'data'=>$data);
		$this->callBack();
	}
	
	/*
	* 4、会员登录
	* 参数>> 会员手机号、密码
	* <<需要考虑对密码进行加密  2015.11.26 18:17>>
	*/
	public function do_login()
	{
		$indata = $this->getData();	//获取数据
		$this->checkSign($indata);		//验证签名
		$username = isset($indata['username']) ? trim($indata['username']) : '';
		$password = isset($indata['password']) ? trim($indata['password']) : '';
		
		if(!$username || !$password){
			$this->info['message'] = "用户名和密码均不能为空";
			$this->callBack();
		}
		if($indata['partner'] == 'ZL999IOSAPI'  || $indata['partner'] == 'ZL888ANDROID'){ //ios平台对密码的处理    (xxxxxx用户密码xxxxxx)  
		    $temp_password = $this->decode_password($password);
		    $password = md5($temp_password);
		}else{
		    $password = md5($password);
		}
		
		$logintype = '';
		if(strpos($username,'@')===false){	//手机
			$logintype='mobile';
			if( ! _checkmobile($username)){
				$this->info['message'] = "手机号格式不正确";
				$this->callBack();
			}				
		}else{		//邮箱
			$logintype='email';
			if( ! _checkemail($username)){
				$this->info['message'] = "邮箱格式不正确";
				$this->callBack();
			}
		}
		$member = $this->db->GetOne("SELECT uid,password,mobilecode,emailcode,username,email,mobile,money,img,reg_key,yaoqing,auto_user FROM `@#_member` WHERE `$logintype`='$username' LIMIT 1 ");
		if( ! $member){
			$this->info = array('status'=>0, 'message'=>'帐号不存在');
			$this->callBack();
		}
		if($password !== $member['password']){
			$this->info = array('status'=>0, 'message'=>'密码错误！');
			$this->callBack();
		}
		if($member[$logintype."code"] != 1){
			$this->info = array('status'=>0, 'message'=>'帐号未认证');
		}else{
			$member['username'] = $member['username'] ? $member['username'] : ($member['mobile'] ? $member['mobile'] : $member['email']);
			$member['img'] = $this->touimg_replace($member['img']);
			
			//统计已有多少人获得返利
			$member['rebate_total'] = $this->get_rebate_total();
			
			if($this->is_wap($indata)){
			    //推广二维码
			    $member['codeImg'] = $this->get_member_qrcode($member['uid'],'wap');
			}
			
			
			unset($member['reg_key'],$member['password']);
			
			file_put_contents('testimg.log', print_r($member,1));
			$this->info = array('status'=>1, 'message'=>'登陆成功', 'data'=>$member);
			
			//如果是本站后台自动注册的会员  则不更新ip地址
			$time = time();
			$mod_fields = " `login_time` = '$time' ";
			$user_ip = _get_ip_dizhi();
			if(intval($member['auto_user']) < 1){
			    $mod_fields .= " , `user_ip` = '$user_ip' ";
			}
			$this->db->Query("UPDATE `@#_member` SET $mod_fields WHERE `uid` = '$member[uid]'");
			
		}
		$this->callBack();
	}
	
	/*
	* 5、获取指定会员的基本信息(会员的头像、名称、ID)
	* 需要展示这个人的头像、id、名称（如无,就选注册方式）,进入默认展示“云购记录”列表
	*/
	public function get_member_center()
	{
		$indata = $this->getData();	//获取数据
		$this->checkSign($indata);		//验证签名
		$uid = isset($indata['uid']) ? intval($indata['uid']) : 0;
		
		if( ! $uid){
			$this->info['message'] = '请求失败';
			$this->callBack();
		}
		$sql = 'SELECT uid,username,reg_key,email,mobile,img FROM @#_member WHERE `uid` = ' . $uid;
		$uinfo = $this->db->GetOne($sql);
		if(isset($uinfo['uid'])){
			$uinfo['img'] = $this->touimg_replace($uinfo['img']);
			$uinfo['username'] = get_user_name($uinfo['uid']);
			$this->info = array('status'=>1, 'message'=>'获取成功', 'data'=>$uinfo);
		}else{
			$this->info['message'] = '用户不存在！';
		}
		$this->callBack();
	}
	
	/*
	* 6、获取会员的云购记录
	*/
	public function get_duobao_list()
	{

		$indata = $this->getData();	//获取数据
		$this->checkSign($indata);		//验证签名
		$uid = isset($indata['uid']) ? intval($indata['uid']) : 0;
		$size = isset($indata['size']) ? intval($indata['size']) : 10;	//分页的大小
		$page = isset($indata['page']) ? intval($indata['page']) : 1;	//第几页
		$type = isset($indata['type']) ? intval($indata['type']) : 0;	//类型   0全部   1进行中   2已揭晓
		
		
		if(!$uid){
			$this->info['message'] = '请求失败';
			$this->callBack();
		}
		
		if($type == 1){
			$_where = "AS m RIGHT JOIN @#_shoplist AS g ON g.id = m.shopid";
			$con = "AND g.shenyurenshu > 0  AND g.q_uid IS NULL GROUP BY shopid  ";
			$field = "m.id";
		}elseif($type == 2){
			$_where = "AS m RIGHT JOIN `@#_shoplist` AS g ON m.shopid= g.id";
			$con = "AND g.q_uid IS NOT NULL AND q_showtime='N' GROUP BY shopid ORDER BY g.q_end_time DESC ";
			$field = "m.id";
		}elseif($type == 3){
			
			$_where = "AS m RIGHT JOIN `@#_shoplist` AS g ON m.shopid= g.id ";
			$con 	= "AND g.q_uid IS NOT NULL AND g.q_showtime='Y' GROUP BY shopid ORDER BY g.q_end_time DESC ";
			$field 	= "m.id ";
			
		}else{
			$_where = "AS m RIGHT JOIN `@#_shoplist` AS g ON g.id = m.shopid";
			$con = "GROUP BY shopid ORDER BY g.shenyurenshu DESC, g.q_end_time DESC ";		//[追加]状态的商品排前面 然后是进行中 最后是已揭晓
			$field = "m.id";
		}
		
		//* 2016/01/04 苹果APP上架期间不允许显示苹果相关产品  审核通过后要去掉该代码 start */
	//	$version = isset($indata['version']) ? trim($indata['version']) : '';    //获取用户的版本号,最新版的在审核阶段是不允许显示的
		//if( $this->is_ios($indata) && $this->compare_version($version) ){
		//    $_where .= ' AND g.is_apple = 0 ';
	//	}
		/* 2016/01/04  苹果APP上架期间不允许显示苹果相关产品  审核通过后要去掉该代码 end */
		
		$res = "SELECT {$field} FROM `@#_member_go_record` {$_where} WHERE uid='$uid' {$con} ";
		//var_dump($res);die;
		$total=$this->db->GetList($res);
		$total = count($total);
		if( ! $total ){
			$this->info['status'] = 1;
			$this->info['message'] = '暂无数据';
			$this->callBack();
		}
		$page_count = intval($total / $size);
		$max_page = $total % $size == 0 ? $page_count : $page_count + 1;
		if($page > $max_page){
			$this->info['status'] = 1;
			$this->info['message'] = '后面没有数据了';
			$this->callBack();
		}
		$start = $page <= 1 ? 0 : ($page-1) * $size;	//从第几条数据开始取
		
		$sql = "SELECT shopname,shopid,shopqishu,sum(gonumber) as gonumber,g.yunjiage,g.default_renci FROM `@#_member_go_record`  {$_where} WHERE uid=$uid {$con} LIMIT $start,$size";
		$arr_list = $this->db->Getlist($sql);
		$num = count($arr_list);
		if($num>0){
			for($i=0;$i<$num;$i++){
				$arr_list[$i]['other'] 			= $this->get_dejiang_info($arr_list[$i]['shopid'],$arr_list[$i]['shopqishu'],$uid);
				$arr_list[$i]['tag'] 			=  $arr_list[$i]['other']['tag'];	//tag  0已揭晓 1进行中 2倒计时
				$arr_list[$i]['is_ten'] 		=  intval($arr_list[$i]['other']['is_shi']);	//判断是否是十元专区
				$arr_list[$i]['wap_ten'] 		=  intval($arr_list[$i]['other']['wap_ten']);	//h5需要的字段  判断是否是十元专区
				$arr_list[$i]['yunjiage'] 		=  intval($arr_list[$i]['yunjiage']);	//价格  追加的时候需要价格
				$arr_list[$i]['default_renci'] 	=  intval($arr_list[$i]['default_renci']);	//价格  追加的时候需要价格
				$arr_list[$i]['id'] 			=  intval($arr_list[$i]['shopid']);	//商品的id   (H5需要)
				$arr_list[$i]['zongrenshu'] 	= $arr_list[$i]['other']['zongrenshu'];
				$arr_list[$i]['canyurenshu'] 	= $arr_list[$i]['other']['canyurenshu'];
				$arr_list[$i]['ratio'] 			= ceil($arr_list[$i]['canyurenshu']/$arr_list[$i]['zongrenshu']   * 100 );	//H5需要的进度
				$arr_list[$i]['thumb'] 			= $arr_list[$i]['other']['thumb'];
				
				$arr_list[$i]['shopname'] = str_replace(';','',str_replace('&nbsp;', ' ', $arr_list[$i]['shopname']));
				if($arr_list[$i]['tag'] == 0){
					if($arr_list[$i]['other']['q_user']){	//如果没有就不传
						$arr_list[$i]['q_user'] =  $arr_list[$i]['other']['q_user'];
						$touimg =  $this->get_user_fields($arr_list[$i]['q_uid'], 'img');		//中奖人的头像
						$touimg = $touimg ? : $arr_list[$i]['q_user']['img'];
						$arr_list[$i]['q_user']['img'] = strpos($touimg['img'], G_UPLOAD_PATH) === false ? G_UPLOAD_PATH.'/'.$touimg['img'] : $touimg['img'];
						$arr_list[$i]['q_user']['username'] = get_user_name($arr_list[$i]['q_user']['uid']);
					}
				}
				
				$arr_list[$i]['q_end_time'] = $arr_list[$i]['other']['q_end_time'];
				$arr_list[$i]['q_showtime'] = $arr_list[$i]['other']['q_showtime'];
				//统计某个商品，该用户总共购买的次数
				//$gonumbers = $this->get_member_go_record($arr_list[$i]['shopid'],$uid,$arr_list[$i]['shopqishu']);
				//$arr_list[$i]['gonumber'] =  $gonumbers['gonumber'];  //已用sum代替   20151219
				unset($arr_list[$i]['other']);
			}
			$data['total'] = $total;
			$data['list'] = $arr_list;
			$this->info = array('status'=>1, 'message'=>'请求成功', 'data'=>$data);
		}else{
			$this->info['message'] = "暂未参加任何众乐活动";
		}
		$this->callBack();
	}
	
	/*
	* 7、获取某个会员的中奖记录
	* 参数>> 会员ID:uid   分页大小:size   第几页:page
	*/
	public function get_member_win_records()
	{
		$indata = $this->getData();	//获取数据
		$this->checkSign($indata);		//验证签名
		$uid  = isset($indata['uid']) ? intval($indata['uid']) : 0;
		$size = isset($indata['size']) ? intval($indata['size']) : 10;
		$page = isset($indata['page']) ? intval($indata['page']) : 1;
		
		if( ! $uid){
			$this->info['message'] = '请求失败';
			$this->callBack();
		}
		
		$total = $this->db->GetCount("SELECT `id` FROM @#_shoplist WHERE `q_uid` = '$uid' AND q_showtime='N'");	//统计总数
		if( ! $total ){
			$this->info['message'] =$total;
			$this->callBack();
		}
		$page_count = intval($total / $size);	//总页数取整
		$max_page = $total % $size == 0 ? $page_count : $page_count + 1;	//最大页数
		if($page > $max_page){
			$this->info['message'] = '后面没有数据了';
			$this->callBack();
		}
		
		$start = $page <= 1 ? 0 : ($page-1) * $size;	//从第几条数据开始取
		$fileds = 'm.id,s.id as shopid,s.title,s.zongrenshu,s.qishu,s.q_user_code,s.q_end_time,s.thumb,s.is_shi,m.company_time,m.confirm_time,m.confrim_addr_time';
		$sql ="SELECT 
						$fileds 
					FROM 
						@#_shoplist AS s 
					LEFT JOIN 
						@#_member_go_record as m 
					ON 
						m.shopid = s.id 
					WHERE  
						`q_uid` = '$uid' AND 
						s.q_showtime ='N'  AND 
						m.huode = s.q_user_code
					GROUP BY m.shopid
					ORDER BY 
						`q_end_time` DESC 
					LIMIT  
						$start, $size";
		
		
		//echo $sql;
		$record_list = $this->db->GetList($sql);
		if(count($record_list)){
			foreach($record_list as $k => $v){
				if( strpos($v['thumb'],G_UPLOAD_PATH) === false ){
					$record_list[$k]['thumb'] = G_UPLOAD_PATH . '/' . $v['thumb'];
				}
				$record_list[$k]['q_end_time'] = date('Y-m-d H:i',$record_list[$k]['q_end_time']);
				$record_list[$k]['is_ten'] = intval($v['is_shi']);	//是否是10元专区
				$record_list[$k]['wap_ten'] = 0;	//h5需要的字段  是否是10元专区
				$record_list[$k]['title'] = str_replace('&nbsp;',' ',$v['title']);
				$record_list[$k]['qianshou'] = ! $v['confrim_addr_time'] ? 3 : ( ! $v['company_time'] ? 2 : ($v['confirm_time'] ? 1 : 0 ));  //1 已签收   0待签收   2等待发货  3请确认收货地址
				$go_record = $this->get_member_go_record($record_list[$k]['shopid'], $uid, $record_list[$k]['qishu']);
				$record_list[$k]['renci'] = isset($go_record['gonumber']) ? $go_record['gonumber'] : 1 ; //该用户本期参与了几次
				unset($go_record,$record_list[$k]['confirm_time'],$record_list[$k]['company_time']);
			}
			$this->info = array('status'=>1, 'message'=>'获取成功', 'data'=>$record_list);
		}else{
			$this->info['message'] = '暂无数据';
		}
		$this->callBack();
	}
	
	/*
	* 8、获取某个会员的晒单分享
	* 参数>> 会员ID:uid   分页大小:size   第几页:page
	*/
	public function get_member_shaidan()
	{
		$indata = $this->getData();	//获取数据
		$this->checkSign($indata);		//验证签名
		$uid = isset($indata['uid']) ? intval($indata['uid']) : 0;		//会员ID
		$size = isset($indata['size']) ? intval($indata['size']) : 10;
		$page = isset($indata['page']) ? intval($indata['page']) : 1;
		
		$wh = " sd.sd_userid = '$uid' ";
		$t_sql = "SELECT sd.sd_id FROM @#_shaidan as sd WHERE $wh ";
		
		/* 2016/01/04 苹果APP上架期间不允许显示苹果相关产品  审核通过后要去掉该代码 start */
		//$version = isset($indata['version']) ? trim($indata['version']) : '';    //获取用户的版本号,最新版的在审核阶段是不允许显示的
		//if( $this->is_ios($indata) && $this->compare_version($version) ){
		//    $wh .= " AND sl.id=sd.sd_shopid AND sl.is_apple=0 ";
		//    $t_sql = "SELECT sd.sd_id FROM @#_shaidan as sd, @#_shoplist as sl WHERE " . $wh;
		//}
		/* 2016/01/04  苹果APP上架期间不允许显示苹果相关产品  审核通过后要去掉该代码 end */
		
		$total = $this->db->GetCount($t_sql);
		if( ! $total ){
			$this->info['message'] = '暂无数据';
			$this->callBack();
		}
		$page_count = intval($total / $size);
		$max_page = $total % $size == 0 ? $page_count : $page_count + 1;
		if($page > $max_page){
			$this->info['message'] = '后面没有数据了';
			$this->callBack();
		}
		
		$start = $page <= 1 ? 0 : ($page-1) * $size;	//从第几条数据开始取
		$field = 'sd.sd_id,sd.sd_shopid,sd.sd_shopsid,sd.sd_title,sd.sd_content,sd.sd_photolist,sd.sd_time,sl.title,sl.q_user,sl.qishu';	//is_audit 2表示审核通过
		$sql = "SELECT $field FROM @#_shaidan as sd LEFT JOIN @#_shoplist as sl ON sl.id = sd.sd_shopid WHERE sd.is_audit =2 AND $wh ORDER BY sd.`sd_time` DESC LIMIT $start,$size";
		$shaidan_list = $this->db->GetList($sql);
		if(count($shaidan_list)){		//strip_tags
			foreach($shaidan_list as $k => $v){
			    $sd_title = str_replace('&nbsp;',' ',$v['sd_title']);
				$shaidan_list[$k]['sd_title'] = $this->emj_decode($sd_title);
				$shaidan_list[$k]['q_user'] = unserialize($v['q_user']);
				$shaidan_list[$k]['q_user']['username'] = get_user_name($shaidan_list[$k]['q_user']['uid']);
				$shaidan_list[$k]['sd_time'] = date('m-d H:i',$v['sd_time']);
				$sd_photolist = rtrim($v['sd_photolist'],';');
				$sd_photolist = explode(';',$sd_photolist);
				foreach($sd_photolist as $k1 => $v1){
					if(strpos($v1,G_UPLOAD_PATH) === false){
						$sd_photolist[$k1] = G_UPLOAD_PATH . '/' . $v1;
					}
				}
				$shaidan_list[$k]['sd_photolist'] = $sd_photolist;
				$user_info = $this->get_user_fields($uid,'img');    //获取最新头像
				$shaidan_list[$k]['q_user']['img'] = $this->touimg_replace($user_info['img']);
				unset($sd_photolist);
				$sd_content = strip_tags($v['sd_content']);		//去除html标签
				$shaidan_list[$k]['sd_content'] = $this->emj_decode($sd_content);
				$shaidan_list[$k]['title'] = str_replace('&nbsp;',' ',$v['title']);		//去除html标签
				$shaidan_list[$k]['sd_link'] = G_WEB_PATH . '/yungou/#/tab/shareDetail?come=Android&sd_id=' . $v['sd_id'];		//晒单详情页面链接
			}
			$this->info = array('status'=>1, 'message'=>'请求成功', 'data'=>$shaidan_list);
		}else{
			$this->info['message'] = '暂无数据';
		}
		$this->callBack();
	}
	
	/*
	* 9、获取当前登录会员的充值记录
	* 参数>> 会员ID:uid
	*/
	/* 充值记录应该用member_addmoney_record
	public function get_member_recharge_records()
	{
		$indata = $this->getData();	//获取数据
		$this->checkSign($indata);		//验证签名
		$uid = isset($indata['uid']) ? intval($indata['uid']) : 0;		//默认取所有商品的晒图分享 按 时间最近的排序
		$size = isset($indata['size']) ? intval($indata['size']) : 10;
		$page = isset($indata['page']) ? intval($indata['page']) : 1;

		if(!$uid){
			$this->info['message'] = '请求失败';
			$this->callBack();
		}
		$total = $this->db->GetCount("SELECT `uid` FROM `@#_member_account` WHERE `uid`='$uid' AND `type` = -1");
		if( ! $total ){
			$this->info['message'] = '暂无数据';
			$this->callBack();
		}
		$page_count = intval($total / $size);
		$max_page = $total % $size == 0 ? $page_count : $page_count + 1;
		if($page > $max_page){
			$this->info['message'] = '后面没有数据了';
			$this->callBack();
		}
		
		$start = $page <= 1 ? 0 : ($page-1) * $size;	//从第几条数据开始取
		$sql = "SELECT uid,pay,money,time FROM `@#_member_account` WHERE uid='$uid' AND `type` = -1 ORDER BY `time` DESC LIMIT $start,$size";
		$arr_list = $this->db->GetList($sql);
		if(count($arr_list)){
			foreach($arr_list as $k => $v){
				$arr_list[$k]['time'] = date('Y-m-d H:i:s',$v['time']);
			}
			$this->info = array('status'=>1, 'message'=>'请求成功', 'data'=>$arr_list);
		}else{
			$this->info['message'] = '暂无数据';
		}
		$this->callBack();
	}*/
	public function get_member_recharge_records()
	{
		$indata = $this->getData();	//获取数据
		$this->checkSign($indata);		//验证签名
		$uid = isset($indata['uid']) ? intval($indata['uid']) : 0;		
		$size = isset($indata['size']) ? intval($indata['size']) : 10;
		$page = isset($indata['page']) ? intval($indata['page']) : 1;
		
		if(!$uid){
			$this->info['message'] = '请求失败';
			$this->callBack();
		}
		$total = $this->db->GetCount("SELECT `uid` FROM `@#_member_addmoney_record` WHERE `uid`='$uid' ");
		if( ! $total ){
			$this->info['message'] = '暂无数据';
			$this->callBack();
		}
		$page_count = intval($total / $size);
		$max_page = $total % $size == 0 ? $page_count : $page_count + 1;
		if($page > $max_page){
			$this->info['message'] = '后面没有数据了';
			$this->callBack();
		}
		
		$start = $page <= 1 ? 0 : ($page-1) * $size;	//从第几条数据开始取
		$sql = "SELECT uid,pay_type,money,status,time FROM `@#_member_addmoney_record` WHERE uid='$uid'  ORDER BY `time` DESC LIMIT $start,$size";
		$arr_list = $this->db->GetList($sql);
		//print_r($arr_list);
		if(count($arr_list)){
			foreach($arr_list as $k => $v){
			    $time = strtotime(date('Y-m-d 00:00:00'));
				$arr_list[$k]['status'] = $v['status'] == '未付款' ? ( $v['time'] < $time ? -1 : 0 ) : 1 ;		//1已付款   0未付款 -1已过期
				$arr_list[$k]['time'] = date('Y-m-d H:i:s',$v['time']);
			}
			$this->info = array('status'=>1, 'message'=>'请求成功', 'data'=>$arr_list);
		}else{
			$this->info['message'] = '暂无数据';
		}
		$this->callBack();
	}
	
	/*
	* 10、获取清单页面[猜你喜欢] 的商品列表
	* 参数>> 会员ID:uid
	*/
	public function get_guess_you_like()
	{
		$indata = $this->getData();		//获取数据
		$this->checkSign($indata);		//验证签名
		$wh = ' id > 0 AND sid != 3025 ';
		
		/* 2016/01/04 苹果APP上架期间不允许显示苹果相关产品  审核通过后要去掉该代码 start */
		$version = isset($indata['version']) ? trim($indata['version']) : '1.0.0';    //获取用户的版本号,最新版的在审核阶段是不允许显示的
		if( $this->is_ios($indata) && $this->compare_version($version) ){
		    $wh .= ' AND is_apple = 0 ';
		}
		/* 2016/01/04  苹果APP上架期间不允许显示苹果相关产品  审核通过后要去掉该代码 end */
		
		$sql = "SELECT count(id) AS count_num,sid, title,thumb,is_shi as is_ten FROM `@#_shoplist` WHERE $wh GROUP BY sid ORDER BY count_num DESC LIMIT 15";
		$arr_list = $this->db->GetList($sql);
		$num = count($arr_list);
		if($num > 0 ){
			foreach($arr_list AS $k=>$val){
				$arr = $this->get_zuixin_goods_shoplist($val['sid']);
				$arr_list[$k]['shopid'] = $arr['id'];
				$arr_list[$k]['is_ten'] = intval($val['is_ten']);
				$arr_list[$k]['wap_ten'] = intval($val['is_ten']) ? 10 : 1;		//h5需要的字段 是否是10元专区
				$arr_list[$k]['title'] = str_replace('&nbsp;',' ',$val['title']);
				if($arr['canyurenshu']==0){
					$arr_list[$k]['bilv']=0;
				}else{
					$arr_list[$k]['bilv'] = round($arr['canyurenshu']/$arr['zongrenshu'],2);
				}
				
				if(strpos($val['thumb'],G_UPLOAD_PATH) === false){
				    $temp = explode('.',$val['thumb']);
				    $houzhui = end($temp);
				    $arr_list[$k]['thumb'] = G_UPLOAD_PATH . '/' . $val['thumb']."_200200.".$houzhui;
				}
				unset($arr_list[$k]['sid'],$arr_list[$k]['count_num'],$arr);
			}
			$this->info = array('status'=>1,'message'=>'请求成功!','data'=>$arr_list);
		}else{
			$this->info['message'] = "暂无数据";
		}
		$this->callBack();
	}
	
	/*
	* 11、验证是否为本站会员(找回密码时候用到)
	* 参数>> 手机号或邮箱
	* 如果验证通过，就返回会员信息(名称、手机)
	* 已与 发送验证码 接口合并  20151209  kangpengfei
	*/
	/*public function check_is_member()
	{
		$indata = $this->getData();	//获取数据
		$this->checkSign($indata);		//验证签名
		$mobile = isset($indata['mobile']) ? trim($indata['mobile']) : 0;		//默认取所有商品的晒图分享 按 时间最近的排序
		
		if(_checkmobile($mobile)){
			$member = $this->db->GetOne("SELECT uid,username,mobile FROM @#_member WHERE `mobile` = '$mobile' LIMIT 1");
			if(isset($member['uid'])){
				$member['mobile'] = $this->nick_replace($member['mobile']);
				$member['username'] =  ! empty($member['username']) ? $member['username'] : $member['mobile'];
				$this->info = array('status'=>1, 'message'=>'验证通过！', 'data'=>$member);
			}else{
				$this->info['message'] = '用户不存在！';
			}
		}else{
			$this->info['message'] = '请求失败！';
		}
		$this->callBack();
	}*/
	
	/*
	* 12、发送[找回密码]的手机验证码
	* 参数>> 手机号:mobile
	*/
	public function do_send_found_code()
	{
		$indata = $this->getData();	//获取数据
		$this->checkSign($indata);		//验证签名
		$mobile = isset($indata['mobile']) ? trim($indata['mobile']) : '';
		if( ! $mobile){
			$this->info['message'] = '请求失败！';
			$this->callBack();
		}
		
		if(_checkmobile($mobile)){
			
			
			$is_member = $this->db->GetOne("SELECT uid FROM @#_member WHERE `mobile` = '$mobile' ");		//验证是否为本站会员
			if( ! isset($is_member['uid'])){
				$this->info['message'] = '账号不存在';
				$this->callBack();
			}
			$data = $this->send_mobile_code($mobile,'template_mobile_pwd');	//发送验证码		template_mobile_pwd表示调用找回密码的短信模板
			
			$this->info['status'] = isset($data[0]) && $data[0] == 1 ? 1 : 0;
			$this->info['message'] = isset($data[1]) ? '发送成功' : '发送失败';
		}else{
			$this->info['message'] = '请求失败！';
		}
		
		$this->callBack();
	}
	
	/*
	* 13、检验[找回密码]中用户提交的验证码是否正确
	* 参数>> 手机号:mobile    密码:password
	* 已与 保存新密码 接口合并    20151209  kangpengfei
	*/
	/*public function do_check_found_code()
	{
		$indata = $this->getData();	//获取数据
		$this->checkSign($indata);		//验证签名
		$mobile = isset($indata['mobile']) ? trim($indata['mobile']) : 0;		//手机号
		$code = isset($indata['code']) ? trim($indata['code']) : 0;		//验证码
		
		$is_pass = $this->check_code_is_pass($mobile,$code,'is_found');	//第三个参数，表示当前场景是：找回密码   is_found字段在@#_mobile_code里面
		if($is_pass){
			$this->info = array('status'=>1, 'message'=>'验证码正确！');	//验证码正确，那么android和ios那边就跳转至重新设置密码页面
		}else{
			$this->info['message'] = '验证码错误！';
		}
		$this->callBack();
	}*/
	
	/*
	* 14、保存会员[找回密码]中设置的新密码
	* 参数>> 手机号:mobile    密码:password
	*/
	public function do_save_new_password()
	{
		$indata = $this->getData();	//获取数据
		$this->checkSign($indata);		//验证签名
		$mobile = isset($indata['mobile']) ? trim($indata['mobile']) : 0;		//手机号
		$password = isset($indata['password']) ? trim($indata['password']) : 0;	
		$code = isset($indata['code']) ? trim($indata['code']) : 0;		//验证码
		
		if( ! $mobile ||  ! $password || !$code){
			$this->info['message'] = '请求失败！';
			$this->callBack();
		}
		
		//检查验证码是否正确
		$is_pass = $this->check_code_is_pass($mobile,$code,'is_found');	//第三个参数，表示当前场景是：找回密码   is_found字段在@#_mobile_code里面
		if( ! $is_pass){
			$this->info['message'] = '验证码错误！';
			$this->callBack();
		}
		
		//确认该手机是否填写过验证码
		$time = time();
		$time -= 7200;	//2小时内是否填写过验证码
		$is_found = $this->db->GetCount("SELECT `id` FROM @#_mobile_code WHERE `mobile` = '$mobile' AND `is_found` = 1 AND `time` > $time");	
		if( ! $is_found){
			$this->info['message'] = '数据来源不合法,请稍后再试！';
			$this->callBack();
		}
		
	    if($indata['partner'] == 'ZL999IOSAPI' || $indata['partner'] == 'ZL888ANDROID' ){ //ios&&android平台对密码的处理    (xxxxxx用户密码xxxxxx)
		    $temp_password = $this->decode_password($password);
		    $password = md5($temp_password);
		}else{
		    $password = md5($password);
		}
		
		$is_update = $this->db->Query("UPDATE @#_member SET `password`='$password' WHERE `mobile` = '$mobile' ");
		if($is_update){
			$this->info = array('status'=>1, 'message'=>'修改成功！');
		}else{
			$this->info['message'] = '修改失败,请稍后重试！';
		}
		$this->callBack();
	}
	
	/*
	* 15、获取当前登录会员的地址 列表/单个信息
	* 参数>> 会员ID:uid
	*/
	public function get_member_address()
	{
		$indata = $this->getData();	//获取数据
		$this->checkSign($indata);		//验证签名
		$uid = isset($indata['uid']) ? intval($indata['uid']) : 0;
		$id = isset($indata['id']) ? intval($indata['id']) : 0;	//查询单个时需传入id
		
		if(!$uid){
			$this->info['message'] = "请求失败";
			$this->callBack();
		}
		$wh = '';
		if($id) $wh = " AND `id`=$id ";
		
		$field = " id,uid,sheng,shi,xian,jiedao,shouhuoren,`default` as is_default,mobile";
		$sql = "SELECT $field FROM `@#_member_dizhi` WHERE `uid`='$uid' " . $wh;
		
		if($id){	//查询单个地址信息
			$address = $this->db->GetOne($sql);
			if(!isset($address['id'])){
			    $this->info['status'] = 1;
				$this->info['message'] = "暂无数据";
				$this->callBack();
			}
			$this->info = array('status'=>1, 'message'=>'请求成功', 'data'=>$address);
			$this->callBack();
		}
		
		$arr_list = $this->db->GetList($sql);
		$num = count($arr_list);
		if($num > 0){
			foreach($arr_list AS $k=>$val){
				$arr_list[$k]['address'] = $val['sheng'].$val['shi'].$val['xian'].$val['jiedao'];
			}
			$this->info = array('status'=>1, 'message'=>'请求成功', 'data'=>$arr_list);
			
		}else{
		    $this->info['status'] = 1;
			$this->info['message'] = "暂无数据";
		}
		$this->callBack();
	}
	
	/*
	* 16、添加/保存 当前登录会员的新增地址
	* 参数>> 会员ID、收货人、收货人手机号、省份、城市、地区、详细地址、是否设为默认，id(地址列表中，每一条地址的id)
	*/
	public function do_save_member_address()
	{
		$indata = $this->getData();	//获取数据
		$this->checkSign($indata);		//验证签名
		$uid = isset($indata['uid']) ? intval($indata['uid']) : 0;
		$id = isset($indata['id']) ? intval($indata['id']) : 0;	//如果是修改地址,那么需要传入id,否则就是添加

		if(!$uid){
			$this->info['message'] = "请求失败！";  
			$this->callBack();
		}
		
		$shouhuoren = isset($indata['shouhuoren']) ? trim($indata['shouhuoren']) : '';
		if(!$shouhuoren){
			$this->info['message'] = "收货人姓名不能为空！";
			$this->callBack();
		}
		$mobile = isset($indata['mobile']) ? trim($indata['mobile']) : '';
		if(!$mobile){
			$this->info['message'] = "手机号码不能为空！";
			$this->callBack();
		}
		if(!_checkmobile($mobile)){
			$this->info['message'] = "手机号格式不正确！";
			$this->callBack();
		}
		
		$sheng = isset($indata['sheng']) ? trim($indata['sheng']) : '';
		if(!$sheng){
			$this->info['message'] = "省份不能为空！";
			$this->callBack();
		}
		$shi = isset($indata['shi']) ? trim($indata['shi']) : '';
		if(!$shi){
			$this->info['message'] = "市不能为空！";
			$this->callBack();
		}
		$xian = isset($indata['xian']) ? trim($indata['xian']) : '';
		if(!$xian){
			$this->info['message'] = "县不能为空！";
			$this->callBack();
		}
		$jiedao = isset($indata['jiedao']) ? trim($indata['jiedao']) : '';
		if(!$jiedao){
			$this->info['message'] = "具体地址不能为空！";
			$this->callBack();
		}
		$default = intval($indata['default']) == 1 ? 'Y' : 'N';
		$time = time();
		
		//安卓乱码问题
		/*if($this->is_android($indata)){
		    $shouhuoren = base64_decode($shouhuoren);
		    $sheng = base64_decode($sheng);
		    $shi = base64_decode($shi);
		    $xian = base64_decode($xian);
		    $jiedao = base64_decode($jiedao);
		}*/
		
		$res = false;
		if($id > 0){
			$rows = $this->db->GetOne("SELECT * FROM @#_member_dizhi WHERE `id`=$id AND `uid`=$uid LIMIT 1");
			if(isset($rows['id']) && $rows['id']>0){
				if($shouhuoren == $rows['shouhuoren'] && $sheng == $rows['sheng'] && $shi == $rows['shi'] && $xian == $rows['xian'] && $jiedao == $rows['jiedao'] && $default == $rows['default']){
					$this->info['message'] = '没有做任何修改！';
					$this->callBack();
				}
				$res=true;
			}
		}
		if($res){
			$sql = "UPDATE `@#_member_dizhi` SET `shouhuoren`='$shouhuoren', `mobile`='$mobile', `sheng`='$sheng', `shi`='$shi', ".
					" `xian`='$xian', `jiedao`='$jiedao', `default`='$default', `time` = '$time' WHERE `id` = '$id' AND `uid` = $uid ";
			$this->db->Query($sql);
			$is_query = $this->db->affected_rows();
			$self_id = $id;
			$type = '修改';
		}else{
			$sql = "INSERT INTO @#_member_dizhi(uid,shouhuoren,mobile,sheng,shi,xian,jiedao,`default`,`time`) VALUES('$uid','$shouhuoren','$mobile','$sheng','$shi','$xian','$jiedao','$default','$time')";
			$this->db->Query($sql);
			$is_query = $this->db->insert_id();
			$self_id = $is_query;
			$type = '添加';
		}
		if($is_query){
			$this->info = array('status'=>1, 'message'=>$type.'成功！');
			if($default == 'Y'){
				$res = "UPDATE @#_member_dizhi SET `default`='N' WHERE `uid`=$uid AND `id` != $self_id " ;
				$this->db->Query($res);
			}
		}else{
			$this->info['message'] = $type.'失败！';
		}
		$this->callBack();
	}
	
	/*
	* 17、删除当前登录会员的指定地址
	* 参数>> 会员ID、数据ID:id
	*/
	public function do_del_member_address()
	{
		$indata = $this->getData();		//获取数据
		$this->checkSign($indata);		//验证签名
		$uid = isset($indata['uid']) ? intval($indata['uid']) : 0;
		$id = isset($indata['id']) ? intval($indata['id']) : 0;
		if(!$uid || !$id){
			$this->info['message'] = "请求失败！";
			$this->callBack();
		}
		
		$res = "SELECT `uid` FROM `@#_member_dizhi` WHERE `id`=$id";
		$arr_one = $this->db->GetOne($res);
		if($arr_one['uid'] != $uid){
			$this->info['message'] = "数据异常,请稍候再试！";
			$this->callBack();
		}
		
		$sql = "DELETE FROM `@#_member_dizhi` WHERE `id`=$id";
		$this->db->Query($sql);
		if($this->db->affected_rows()){
			$this->info = array('status'=>1,'message'=>'数据删除成功！');
		}else{
			$this->info = array('status'=>0,'message'=>'数据删除失败！');
		}
		$this->callBack();
	}
	
	/*
	* 18、获取当前登录会员中奖记录的详情
	*/
	public function get_win_records_info()
	{
		$indata = $this->getData();		//获取数据
		$this->checkSign($indata);		//验证签名
		$uid = isset($indata['uid']) ? intval($indata['uid']) : 0;	//当前用户ID
		$id = isset($indata['id']) ? intval($indata['id']) : 0;		//中奖记录的ID
		
		if( ! $uid ||  ! $id){
			$this->info['message'] = '请求失败！';
			$this->callBack();
		}
		$sql = "SELECT g.*,s.title,s.thumb,s.zongrenshu,s.qishu,s.q_user_code,s.q_end_time FROM @#_member_go_record AS g LEFT JOIN @#_shoplist AS s ON s.`id` = g.`shopid`  WHERE g.`id` = $id AND g.`uid` = $uid LIMIT 1";
		$info = $this->db->GetOne($sql);
		if( ! isset($info['id']) ||  ! $info['id']){
			$this->info['message'] = '记录不存在！';
			$this->callBack();
		}
		$addr_uinfo = explode('|',$info['confrim_addr_uinfo']);   //收货人信息，包含姓名和电话号码
		$address = $this->db->GetOne("SELECT * FROM @#_member_dizhi WHERE `uid` = $uid AND `default` = 'Y' LIMIT 1 ");
		//奖品状态		获得商品/确认收获地址/奖品派发/确认收获/已签收
		$status[] = array('status'=>1,'info'=>'获得商品','time'=>date('Y-m-d H:i',$info['time'])); 
		$a_status = 0;
		if( intval($info['confrim_addr_time']) ) $a_status = 1;
		$status[] = array('status'=>$a_status, 'info'=>'确认收获地址','time'=>($info['confrim_addr_time'] ? date('Y-m-d H:i',$info['confrim_addr_time']) : '') ); 
		
		$a_status = 0;
		if( intval($info['company_time']) > 0)  $a_status = 1;
		$status[] = array('status'=>$a_status, 'info'=>'奖品派发','time'=>($info['company_time'] ? date('Y-m-d H:i',$info['company_time']) : '') ); 
		
		$a_status = 0;
		if( intval($info['confirm_time']) > 0) $a_status = 1;
		if( ! intval($info['company_time']) )  $a_status = -1;		//-1未发货   0已发货未签收  1已签收
			$status[] = array('status'=>$a_status, 'info'=>'确认收货','time'=>($info['confirm_time'] ? date('Y-m-d H:i',$info['confirm_time']) : '' ) ); 
			$status[] = array('status'=>$a_status, 'info'=>'已签收','time'=>''); 

		
		//物流信息	物流公司/单号
		$company['company'] = $info['company'];
		$company['company_code'] = $info['company_code'];
		
		//地址信息	姓名/电话/地址
		$addr['shouhuoren'] = $addr_uinfo[0] ?  : '';
		$addr['mobile'] = $addr_uinfo[1] ? : '';
		$addr['address'] = $info['confrim_addr'];
		
		//奖品信息
		$goods['id'] = $info['shopid'];
		$goods['title'] = str_replace('&nbsp;', ' ', $info['title']);
		$goods['qishu'] = $info['qishu'];
		$goods['thumb'] = strpos($info['thumb'],G_UPLOAD_PATH) === false ? G_UPLOAD_PATH.'/'.$info['thumb'] : $info['thumb'];
		$goods['zongrenshu'] = $info['zongrenshu'];
		$goods['q_user_code'] = $info['q_user_code'];
		$goods['q_end_time'] = date('Y-m-d H:i:s',$info['q_end_time']);
		$records = $this->get_member_go_record($goods['id'], $uid, $goods['qishu']);
		$goods['gonumber'] = intval($records['gonumber']);
		
		//组合数据
		$data['jindu'] = $status;
		$data['company'] = $company;
		$data['address'] = $addr;
		$data['goods_info'] = $goods;
		
		$this->info = array('status'=>1, 'message'=>'请求成功','data'=>$data);
		$this->callBack();
	}
	
	/*
	* 19.确认收货按钮   2015/12/12 17:30
	*/
	public function do_confirm_receiving()
	{
		$indata = $this->getData();		//获取数据
		$this->checkSign($indata);		//验证签名
		$uid = isset($indata['uid']) ? intval($indata['uid']) : 0;	//当前用户ID
		$rid = isset($indata['rid']) ? intval($indata['rid']) : 0;	//参与记录的ID
		
		if(!$uid ||  !$rid){
			$this->info['message'] = '请求失败！';
			$this->callBack();
		}
		$rows = $this->db->GetCount("SELECT `id` FROM @#_member_go_record WHERE `id`='$rid' AND `uid`='$uid' AND `company_time` IS NOT NULL ");
		if( ! $rows){
			$this->info['message'] = '请求失败！';
			$this->callBack();
		}
		$time = time();
		$query = $this->db->Query("UPDATE @#_member_go_record SET `status` = '已付款,已发货,已完成' , `confirm_time` = '$time' WHERE `id` = '$rid' AND `uid` = '$uid' ");
		if( ! $query){
			$this->info['message'] = '网络异常,请稍后再试！';
			$this->callBack();
		}
		$this->info = array('status'=>1, 'message'=>'操作成功！', 'data'=>date('Y-m-d H:i',$time));
		$this->callBack();
	}
	
	/*
	* 20、修改当前登录用户的昵称
	* param >> uid,username
	*/
	public function do_mod_username()
	{
		$indata = $this->getData();		//获取数据
		$this->checkSign($indata);		//验证签名
		$uid = isset($indata['uid']) ? intval($indata['uid']) : 0;	//当前用户ID
		$username = isset($indata['username']) ? trim($indata['username']) : '';		
		
		if(!$uid || !$username){
			$this->info['message'] = '请求失败！';
			$this->callBack();
		}
		
		$username = mb_convert_encoding($username, 'UTF-8', 'auto');  //编码转换
		
		//安卓修改名称乱码问题  20160107
		/*if( $this->is_android($indata) ){
		    $username = base64_decode($username);
		}*/
		
		$len = strlen($username);
		if($len < 3 || $len > 30){
		    $this->info['message'] = '昵称为3-30个字符(可由汉字、字母、数字、“_”等字符自由组合)';
		    $this->callBack();
		}
		$res = $this->db->GetOne("SELECT uid,username FROM @#_member WHERE `uid` = '$uid' ");
		if(!isset($res['uid']) || !$res['uid']){
			$this->info['message'] = '用户不存在！';
			$this->callBack();
		}
		if($username == $res['username']){
			$this->info['message'] = '没有做任何修改！';
			$this->callBack();
		}
		$this->db->Query("UPDATE @#_member SET `username` = '$username' WHERE `uid` = '$uid' ");
		$is_mod = $this->db->affected_rows();
		if(!$is_mod){
			$this->info['message'] = '系统异常,请稍后再试！';
			$this->callBack();
		}
		$this->info = array('status'=>1, 'message'=>'修改成功！');
		$this->callBack();
	}
	
	/*
	* 21、发送修改手机号的短信验证码
	* param >> uid,mobile
	*/
	public function send_mod_mobile_code()
	{
		//print_r($_POST);
		$indata=$this->getData();		//获取数据
		//file_put_contents('testtdata.log', print_r($indata,1));
		$this->checkSign($indata);		//验证签名
		$mobile = isset($indata['mobile']) ? trim($indata['mobile']) : '';		//手机号
		$bind_type = isset($indata['bind_type']) ? trim($indata['bind_type']) : '';		//针对微信登录绑定手机号
		$uid = isset($_POST['uid']) ?intval($_POST['uid']) : "";	//当前用户ID
		if ($_SERVER['REQUEST_METHOD']=='GET') {
			$uid = intval($_GET['uid']);
		}else{
			$uid = intval($_POST['uid']);
		}
		
		//echo $uid;
		//print_r($indata);
		//file_put_contents("a.txt", $indata);
		if(!$uid||!$mobile){
			$this->info['message'] = '请求失败';
			$this->callBack();
		}
		if( ! _checkmobile($mobile)){
			$this->info['message'] = '手机号码格式错误！';
			$this->callBack();
		}
		$user_info = $this->db->GetOne("SELECT uid,mobile FROM @#_member WHERE `uid` = '$uid' ");
		if(!isset($user_info['uid'])){
			$this->info['message'] = '系统异常,请稍后再试！';
			$this->callBack();
		}
		if($mobile == $user_info['mobile']){
			$this->info['message'] = '未做修改，无需获取验证码！';
			$this->callBack();
		}
		
		$is_wap = $this->is_wap($indata);
		if( empty($bind_type) && ! $is_wap){    //如果是wap微信登录绑定手机 那么就不判断
    		$is_use = $this->db->GetCount("SELECT uid FROM @#_member WHERE `mobile`='$mobile' OR `reg_key` = '$mobile' ");
    		if($is_use){
    		    $this->info['message'] = '该手机号已被使用！';
    		    $this->callBack();
    		}
		}
		
		$data = $this->send_mobile_code($mobile,'template_mobile_change');	//发送验证码		template_mobile_change表示更换手机号码的短信模板
		$this->info['status'] = isset($data[0]) && $data[0] == 1 ? 1 : 0;
		$this->info['message'] = $data[1];
		$this->callBack();
	}
	
	/*
	* 22、保存更改的手机号
	* * param >> uid,mobile,code
	*/
	public function do_save_mobile()
	{
		$indata = $this->getData();		//获取数据
		$this->checkSign($indata);		//验证签名
		$uid = isset($indata['uid']) ? intval($indata['uid']) : 0;	//当前用户ID
		$mobile = isset($indata['mobile']) ? trim($indata['mobile']) : '';		//手机号
		//$old_mobile = isset($indata['old_mobile']) ? trim($indata['old_mobile']) : '';		//手机号
		$code = isset($indata['code']) ? intval($indata['code']) : '';		//验证码
		$bind_type = isset($indata['bind_type']) ? trim($indata['bind_type']) : '';		//针对微信登录绑定手机号
		$uid = $_GET['uid'];
		if( ! $uid ||  ! $mobile){
			$this->info['message'] = '请求失败！';
			$this->callBack();
		}
		if( ! $code){
		    $this->info['message'] = '验证码错误！';
		    $this->callBack();
		}
		if( ! _checkmobile($mobile)){
			$this->info['message'] = '手机号码格式错误！';
			$this->callBack();
		}
		$user_info = $this->db->GetOne("SELECT uid,mobile,reg_key FROM @#_member WHERE `uid` = '$uid' ");
		if( ! isset($user_info['uid'])){
			$this->info['message'] = '数据出错,请稍后再试！';
			$this->callBack();
		}
		if($mobile == $user_info['mobile']){
			$this->info['message'] = '没有做任何修改！';
			$this->callBack();
		}
		
		$is_pass = $this->check_code_is_pass($mobile,$code,'is_mod');
		//$is_pass=  1;
		if($is_pass){
		    $this->db->Autocommit_start();
		    $old_uid = $this->db->GetOne("SELECT uid FROM @#_member WHERE `mobile`='$mobile' for update");
		    
		    //如果是wap微信登录绑定手机号码
		    $is_wap = $this->is_wap($indata);
		    if( $bind_type=='weixin' && $is_wap && isset($old_uid['uid']) && intval($old_uid['uid']) ){
	            $q1 = $this->db->Query("UPDATE @#_member_band SET `b_uid`='$old_uid[uid]' WHERE `b_uid`='$uid' ");
	            $q2 = $this->db->Query("UPDATE @#_member SET `band`='weixin' WHERE `uid`='$old_uid[uid]' ");
	            $q3 = $this->db->Query("DELETE FROM @#_member WHERE `uid`='$uid' ");
	            $query = ($q1 && $q2 && $q3);
		    }else{
    			$this->db->Query("UPDATE @#_member SET `mobile` = '$mobile',`reg_key`='$mobile' ,`mobilecode`=1  WHERE `uid` = '$uid' ");
    			$query = $this->db->affected_rows();
		    }
			if( ! $query){
			    $this->db->Autocommit_rollback();
				$this->info['message'] = '系统异常,请稍后再试！';
				$this->callBack();
			}
			$this->db->Autocommit_commit();
			
			$return_data = array('status'=>1, 'message'=>'修改成功！');
			
			//如果是wap微信登录绑定手机号码
			if($is_wap && $bind_type=='weixin'){
			    $key = "YYZL89LaEB54,@%888";
			    $return_data['uid'] = $uid;
			    $return_data['wx_sign'] = md5($old_uid['uid'].$key);
			}
			
			$this->info = $return_data;
			
			//web微信登录绑定手机  20150106
			if($indata['partner'] == 'ZLWEBAPITOWEB'){
    	        $cookie = $this->get_user_fields($user_info['uid'],'uid,mobile,password,reg_key,email');
    	        _setcookie("uid",_encrypt($cookie['uid']),60*60*24*7);
    	        _setcookie("ushell",_encrypt(md5($cookie['uid'].$cookie['password'].$cookie['reg_key'].$cookie['email'])),60*60*24*7);
			}
			
		}else{
			$this->info['message'] = '验证码错误！';
		}
		
		$this->callBack();
	}
	
	/*
	* 23、头像上传(修改头像)
	* param >> uid,touimg(先把图片转为二进制,然后base64_encode)
	*/
	public function do_upload_touimg()
	{
		$indata = $this->getData();		//获取数据
		$this->checkSign($indata);		//验证签名
		$uid = isset($indata['uid']) ? intval($indata['uid']) : '';		//uid
		$tou_source = isset($indata['touimg']) ? $indata['touimg'] : '';	//头像二进制且base64加密
		
		if( ! $uid  ||  ! $tou_source){
			$this->info['message'] = '请求失败！';
			$this->callBack();
		}
		$userinfo = $this->db->GetOne("SELECT uid,img FROM @#_member WHERE `uid`='$uid' ");
		if( !isset($userinfo['uid']) ||  ! $userinfo['uid']){
			$this->info['message'] = '系统异常,请稍后再试！';
			$this->callBack();
		}
		$old_img = $userinfo['img'];		//旧头像地址
		$touimg = base64_decode($tou_source);	//图像资源
		$path =  '/touimg/' . date("Ymd");	//头像存放路径
		$dir = G_UPLOAD . $path;	//完整头像存放路径
		if( ! file_exists($dir)){
			@mkdir($dir);
		}
		$filename = '/' . time() . $uid . '.jpg';
		$filesname = $dir . $filename;		//头像完整路径
		@file_put_contents($filesname, $touimg);	//保存头像
		if( ! file_exists($filesname)){
			$this->info['message'] = '上传失败,请稍后再试！';
			$this->callBack();
		}
		
		//裁剪头像 3份
		$this->cut_image($filesname);
		//@unlink($filesname);		//不删原图
		
		$newimg = ltrim($path,'/') . $filename;	//新的头像地址
		$this->db->Query("UPDATE @#_member SET `img` = '$newimg' WHERE `uid` = '$uid' ");
		$is_mod = $this->db->affected_rows();
		if(!$is_mod){
			$this->info['message']='上传失败！';
			$this->callBack();
		}
		if($is_mod &&  $old_img !== 'photo/member.jpg'){		//删除旧的图片
			$size = getimagesize(G_UPLOAD .'/'. $old_img);
			if(isset($size['bits']) && $size['bits']>0){
				@unlink(G_UPLOAD . str_replace('.','._160160.jpg.',$old_img));
				@unlink(G_UPLOAD . str_replace('.','._3030.jpg.',$old_img));
				@unlink(G_UPLOAD . str_replace('.','._8080.jpg.',$old_img));
			}
		}
		$touimg_path = G_UPLOAD_PATH . $path . $filename;	//返回给客户端的完整头像路径
		$this->info = array('status'=>1, 'message'=>'上传成功', 'data'=>$touimg_path);
		$this->callBack();
	}
	
	/*
	* 24、获取当前登录会员的个人信息
	* param >> uid
	*/
	public function get_member_info()
	{
		
		$indata = $this->getData();		//获取数据
		
		$this->checkSign($indata);		//验证签名
		$uid = isset($indata['uid']) ? intval($indata['uid']) : '';		//uid
		
		if(!$uid){
			$this->info['message'] = '请求失败！';
			$this->callBack();
		}
		
		$user_info = $this->db->GetOne("SELECT uid,username,mobile,email,money,img,yaoqing,reg_key,yaoqing FROM @#_member WHERE `uid` = '$uid' LIMIT 1");
		if(!isset($user_info['uid'])){
			$this->info['message'] = '用户不存在！';
			$this->callBack();
		}
		$user_info['img'] = $this->touimg_replace($user_info['img']);
		$user_info['username'] = get_user_name($user_info['uid']);
		$user_info['codeImg'] = $this->get_member_qrcode($user_info['uid'],'wap');
		
		//统计已有多少人获得返利
		$user_info['rebate_total'] = $this->get_rebate_total();

		
		$return_data = array('status'=>1, 'message'=>'请求成功', 'data'=>$user_info);
		
		/* 如果是苹果设备 需要对其做特殊处理  20160107 start */
		$is_ios = $this->is_ios($indata);
		$version = isset($indata['version']) ? trim($indata['version']) : '1.0.0'; //1.0.0
		if($is_ios){
		    if( $this->compare_version($version) ){
		        $return_data['is_hidden'] = 1;   //1隐藏   2显示
		    }else{
		        $return_data['is_hidden'] = 2;
		    }
		    $return_data['do_link'] = 'http://www.yz-db.com/yungou/index.html#/tab/recharge';
		}
		/* 如果是苹果设备 需要对其做特殊处理  20160107 end */
		
		
		$this->info = $return_data;
		$this->callBack();
	}
	
	/*
	* 25、晒单
	* param >> uid
	*/
	public function do_shaidan()
	{
		$indata = $this->getData();		//获取数据
		$this->checkSign($indata);		//验证签名
		$uid = isset($indata['uid']) ? intval($indata['uid']) : '';		//uid
		$shopid = isset($indata['shopid']) ? intval($indata['shopid']) : '';		//商品ID
		$qishu = isset($indata['qishu']) ? intval($indata['qishu']) : '';		//商品期号
		$shopsid = isset($indata['shopsid']) ? intval($indata['shopsid']) : '';		//商品编号
		$title = isset($indata['title']) ? trim($indata['title']) : '';		//晒单标题
		$content = isset($indata['content']) ? trim($indata['content']) : '';		//晒单内容
		
		$title = $this->emj_encode($title);   //对表情做特殊处理
		$content = $this->emj_encode($content);
		
		if(!$uid ||  !$shopid  || !$shopsid ||  !$qishu ||  !$title ||  !$content){
			$this->info['message'] = '请求失败！';
			$this->callBack();
		}
		
		//查询是否中奖
		$is_win = $this->db->GetCount("SELECT id FROM @#_member_go_record WHERE `uid`='$uid' AND  `shopid`='$shopid' AND `huode`>10000000 ");
		if( ! $is_win){
			$this->info['message'] = '请求失败！';
			$this->callBack();
		}
		
		//查询是否晒过单
		$is_only = $this->db->GetCount("SELECT sd_id FROM @#_shaidan WHERE `sd_shopid`='$shopid' AND `sd_userid`='$uid' ");
		if($is_only){
			$this->info['message'] = '你已经晒过单了！';
			$this->callBack();
		}
		
		$num = isset($indata['num']) ? intval($indata['num']) : 1;		//图片的数量
		for($i=1;$i<=$num;$i++){
			$img_source[$i]['source'] = isset($indata['img'.$i]) ? trim($indata['img'.$i]) : '';		//图片的数量
			$img_source[$i]['filename'] = time() . mt_rand(10000,99999) . '.jpg';
		}
		
		//晒单的封面图尺寸为220*auto(取第一张图片为封面图)
		$path = 'shaidan/'.date('Ymd');
		if(!file_exists(G_UPLOAD .'/'. $path)){
			@mkdir(G_UPLOAD .'/'. $path);
		}
		
		$sd_thumbs = '';
		$sd_photolist = '';
		foreach($img_source as $k => $v){
			if(isset($v['filename'])){
			    $filename = G_UPLOAD .'/'. $path .'/'. $v['filename'];
				@file_put_contents($filename, base64_decode($v['source']));
				if(file_exists($filename)){
				    System::load_sys_class('upload','sys','no');
				    $m_size = getimagesize($filename);
    			    $t_width = 220;
    			    $t_height = $m_size[1]*($t_width/$m_size[0]);
    			    upload::thumbs($t_width, $t_height, false, $filename, '', '_220');
    			    
    			    if($k==1){
    			        upload::thumbs(220,intval($t_height),false, $filename);
    			        $new_name = str_replace(".",".jpg_220".intval($t_height).".", $v['filename']);
    			        $sd_thumbs = $path .'/'. $new_name;
    			    }
    			    
    			    $f_width = 540;
    			    $f_height=$m_size[1]*($f_width/$m_size[0]);
    			    upload::thumbs($f_width, $f_height, true, $filename);  //宽度为540的 覆盖原图  
				}
				$sd_photolist .= $path.'/'.$v['filename'] . ';';
			}
		}
		$sd_ip = _get_ip_dizhi();	//获取ip和地区
		$sd_time = time();
		
		$sql = " INSERT INTO @#_shaidan(sd_userid,sd_shopid,sd_shopsid,sd_qishu,sd_ip,sd_title,sd_thumbs,sd_content,sd_photolist,sd_time,is_audit) ".
			    " VALUES($uid, $shopid, $shopsid, $qishu, '$sd_ip', '$title', '$sd_thumbs', '$content', '$sd_photolist', '$sd_time','0')";
		$this->db->Query($sql);
		$res = $this->db->insert_id();
		
		if(!$res){
			$this->info['message'] = '晒单失败,请稍候再试！';
			$this->callBack();
		}
		
		//晒单成功送2元红包 20160108
		System::load_app_fun('member','member');
		shaidan_get_money($uid);
		
		$this->info = array('status'=>1, 'message'=>'晒单成功！');
		$this->callBack();		
	}
	
	/*
	* 26、我的晒单
	* param >> uid
	*/
	public function get_my_shaidan()
	{
		$indata = $this->getData();		//获取数据
		$this->checkSign($indata);		//验证签名
		$uid = isset($indata['uid']) ? intval($indata['uid']) : '';		//uid
		$size = isset($indata['size']) ? intval($indata['size']) : 10;		
		$page = isset($indata['page']) ? intval($indata['page']) : 1;	
		
		if(!$uid){
			$this->info['message'] = '请求失败！';
			$this->callBack();
		}
		
		$where = '';
		/* 2016/01/04 苹果APP上架期间不允许显示苹果相关产品  审核通过后要去掉该代码 start */
		//$version = isset($indata['version']) ? trim($indata['version']) : '';    //获取用户的版本号,最新版的在审核阶段是不允许显示的
	//	if( $this->is_ios($indata) && $this->compare_version($version) ){
		  //  $fnot = $this->db->GetList("SELECT id FROM @#_shoplist WHERE `is_apple` != 1 ");
		  //  $fnot_str = '';
		  //  foreach($fnot as $k=>$v){
		  //      $fnot_str .= ',' . $v['id'];
		//    }
		  //  $fnot_str = ltrim($fnot_str, ',');
		  //  $where = " g.shopid in( $fnot_str ) AND ";    //苹果相关产品
		//}
		/* 2016/01/04  苹果APP上架期间不允许显示苹果相关产品  审核通过后要去掉该代码 end */
		
		$fields = " g.shopid,g.shopqishu,s.sd_id,s.sd_userid,s.sd_shopid,s.sd_shopsid,s.sd_qishu,s.sd_title,s.sd_thumbs,s.sd_content,s.is_audit,s.sd_time,s.sd_photolist ";
		$where .= " g.uid='$uid' AND g.huode > '10000000' AND g.status LIKE '%已完成%' ";
		
		$order_by = "  s.sd_id ASC,s.sd_time DESC ";		//没有晒单的放前面
		
		$total = $this->db->GetCount("SELECT g.id FROM @#_member_go_record AS g LEFT JOIN @#_shaidan AS s ON s.sd_shopid = g.shopid WHERE $where ");
		if(!$total){
			$this->info = array('status'=> 1, 'message'=>'暂无数据','data'=>array('total'=>0));
			$this->callBack();
		}
		$page_count = intval($total / $size);
		$max_page = $total % $size == 0 ? $page_count : $page_count + 1;
		if($page > $max_page){
			$this->info = array('status'=> 1, 'message'=>'后面没有数据了','data'=>array('total'=>0));
			$this->callBack();
		}
		$start = $page <= 1 ? 0 : ($page-1) * $size;	//从第几条数据开始取
		$sql= "SELECT $fields FROM @#_member_go_record AS g LEFT JOIN @#_shaidan AS s ON s.sd_shopid = g.shopid WHERE $where ORDER BY $order_by LIMIT $start,$size ";
		
		$list = $this->db->GetList($sql);
		if(!count($list)){
			$this->info = array('status'=> 1, 'message'=>'没有数据了！','data'=>array('total'=>0));
			$this->callBack();
		}
		foreach($list as $k => $v){
			$list[$k]['sd_id'] = $v['sd_id'] ? : '';
			$list[$k]['sd_userid'] = $uid;   //会员uid
			$list[$k]['sd_username'] = get_user_name($uid);
			$userimg = $this->get_user_fields($uid,'img');
			$list[$k]['sd_img'] = $this->touimg_replace($userimg['img']);
			$list[$k]['uid'] = $uid.'';
			$list[$k]['sd_shopid'] = $v['sd_shopid'] ? : $list[$k]['shopid'];
			$list[$k]['sd_qishu'] = $v['sd_qishu'] ? : $list[$k]['shopqishu'];
			$sd_content = $v['sd_content'] ? strip_tags($v['sd_content']) : '';
			$list[$k]['sd_content'] = $sd_content ? $this->emj_decode($sd_content) : '';
			$sd_title =  $v['sd_title'] ? : '';
			$list[$k]['sd_title'] = $sd_title ? $this->emj_decode($sd_title) : '';
			$list[$k]['sd_time'] = $v['sd_time'] ? date("Y-m-d H:i",$v['sd_time']) : '';
			$list[$k]['is_audit'] = intval($v['is_audit']);	//审核状态   1 审核失败  0 待审核   2 审核通过
			$goods_info = $this->get_goods_fields($v['shopid'], $v['shopqishu'], ' id,title,sid,q_user,q_user_code,q_uid,q_end_time,thumb ');
			$list[$k]['sd_shopsid'] = $v['sd_shopsid'] ? : $goods_info['sid'];  //商品编号 sid
			$list[$k]['goods_title'] = isset($goods_info['title']) ? str_replace('&nbsp;',' ',trim($goods_info['title'])) : '';
			$list[$k]['q_user_code'] = $goods_info['q_user_code'];	//幸运号码
			$list[$k]['sd_thumbs'] = strpos($goods_info['thumb'],G_UPLOAD_PATH)===false ? G_UPLOAD_PATH . '/'. $goods_info['thumb'] : $goods_info['thumb'];
			$list[$k]['q_end_time'] = date("Y-m-d H:i",$goods_info['q_end_time']);	//揭晓时间
			$gonumber = $this->get_member_go_record($v['shopid'], $uid, $v['shopqishu']);
			$list[$k]['gonumber'] = $gonumber['gonumber'] ? : '';	//幸运者本期参与次数
			$list[$k]['is_shaidan'] = $v['sd_id'] ?  0 : 1;	//是否可以晒单
			
			if(!empty($v['sd_id'])){
    			$list[$k]['sd_link'] = G_WEB_PATH . '/yungou/index.html#/tab/shareDetail?come=Android&sd_id=' . $v['sd_id'];		//晒单详情页面链接
    			
    			$sd_photolist = rtrim($v['sd_photolist'],';');
    			$sd_photolist = explode(';',$sd_photolist);
    			foreach($sd_photolist as $k1 => $v1){
    			    if(strpos($v1,G_UPLOAD_PATH) === false){
    			        $sd_photolist[$k1] = G_UPLOAD_PATH . '/' . str_replace('.','.jpg_220.',$v1);
    			    }
    			}
			}else{
			    $list[$k]['sd_link'] = '';
			    $sd_photolist = array();
			}
			$list[$k]['sd_photolist'] = $sd_photolist;
			
			unset($list[$k]['shopid'],$list[$k]['shopqishu']);
		}
		$data['total'] = $total;
		$data['max_page'] = $max_page;
		$data['list'] = $list;
		
		$this->info = array('status'=>1, 'message'=>'请求成功', 'data'=>$data);
		$this->callBack();
	}
	
	/*
	* 第三方登录处理
	*/
	public function partner_login()
	{
	    $indata = $this->getData();		//获取数据
	    $this->checkSign($indata);		//验证签名
	    $type = isset($indata['type']) ? _htmtocode($indata['type']) : ''; //第三方类型   qq  weibo  weixin
	    $code = isset($indata['code']) ? _htmtocode($indata['code']) : '';   //微信的code
	    $nickname = isset($indata['nickname']) ? _htmtocode($indata['nickname']) : '';   //用户名称 
	    $touimg = isset($indata['touimg']) ? _htmtocode($indata['touimg']) : ''; //头像地址
	    
	    @file_put_contents('wxtestt.log', print_r($indata,1));
	    
	    if( ! $type || ! $code ){
	        $this->info['message'] = '请求失败！';
	        $this->callBack();
	    }
	    
	    $type_arr = array('qq','weibo','weixin');
	    
	    if(!in_array($type,$type_arr)){
	        $this->info['message'] = '请求失败！';
	        $this->callBack();
	    }
	    
	    //处理微信登录
	    if($type == 'weixin'){
    	    include G_SYSTEM."modules/api/lib/weixin/config.php";
    	    include G_SYSTEM."modules/api/lib/weixin/weixin.class.php";
    	    $this->weixin = new WeiXin( WX_APPID , WX_SECRET );
    	    $wx_token = $this->weixin->getAccessTokenURL($code);
    	    if( ! isset($wx_token['access_token'])){
    	        $this->info['message'] = '请求失败';
    	        $this->callBack();
    	    }
    	    $openid = $wx_token['openid'];
    	    $wx_userinfo = $this->weixin->getUserInfoURL($wx_token['access_token'], $openid);
    	    file_put_contents('wxtestlogin.log', print_r($wx_userinfo,1));
	    }
	    $this->info['message'] = '等会处理';
	    $this->callBack();
	    
	    
	    $uinfo = array('type'=>$type, 'code'=>$code, 'nickname'=>$nickname, 'touimg'=>$touimg);    //组合数据
	    
	    //查询绑定信息
	    $band = $this->db->GetOne("SELECT b_uid FROM @#_member_band WHERE `b_type`='$type' AND `b_code`='$code' LIMIT 1");
	    
	    if(!isset($band['b_uid'])){
	        
	        $data = $this->add_partner_user($uinfo); //添加
	        
	    }else{
	        
    	    $is_member = $this->get_user_fields($band['b_uid'],'*');      //查询用户是否存在
    	    
    	    if(!isset($is_member['uid'])){
    	        
    	        $this->db->Query("DELETE FROM @#_member_band WHERE `b_uid`='$band[b_uid]' LIMIT 1 ");
    	        
    	        $data = $this->add_partner_user($uinfo);   //添加会员
    	        
    	    }else{
    	        
    	        $uinfo['uid'] = $is_member['uid'];
    	        
    	        $data = $this->update_partner_user($uinfo);    //更新
    	        
    	    }
    	    
	    }
	    
	    $this->info = $data;
	    $this->callBack();  
	}
	
	
	//好友相关  20151217   ⅠⅡⅢⅣⅤⅥⅦⅧⅨⅩ
	/*
	* Ⅰ当前登录会员填写邀请码
	*/
	public function submit_yaoqing_code()
	{
		$indata = $this->getData();		//获取数据
		$this->checkSign($indata);		//验证签名
		$uid = isset($indata['uid']) ? intval($indata['uid']) : '';		//uid
		$code = isset($indata['code']) ? intval($indata['code']) : '';		//邀请码(邀请人的uid) 一经填写不可更改
		
		if(!$uid || !$code){
			$this->info['message'] = '请求失败！';
			$this->callBack();
		}
		$uinfo = $this->get_user_fields($uid, 'uid,yaoqing');
		if( ! isset($uinfo['uid'])){
			$this->info['message'] = '请求失败！';
			$this->callBack();
		}
		if($uinfo['yaoqing']){
			$this->info['message'] = '已经填写过了,不需要再填写！';
			$this->callBack();
		}
		$yaoqing_info = $this->get_user_fields($code, 'uid,yaoqing');
		if( ! isset($yaoqing_info['uid'])){
			$this->info['message'] = '邀请码是错的！';
			$this->callBack();
		}
		if( isset($yaoqing_info['yaoqing']) && $yaoqing_info['yaoqing'] == $uid){
			$this->info['message'] = '你不可以填写你邀请的好友的邀请码！';
			$this->callBack();
		}
		
		$this->db->Query("UPDATE `@#_member` SET `yaoqing` = '$code' WHERE `uid` = '$uid' ");
		$is_query = $this->db->affected_rows();
		if($is_query){
			System::load_app_fun('member','member');
			do_insert_friends($uid,$code);
			$this->info = array('status'=>1 , 'message'=>'操作成功！');
			$this->callBack();
		}
		
		$this->info['message'] = '操作失败,请稍后再试！';
		$this->callBack();
	}
	
	/*
	* Ⅱ获取当前登录会员的各级好友个数和各级总积分
	*/
	public function get_member_friendship()
	{
		$indata = $this->getData();		//获取数据
		$this->checkSign($indata);		//验证签名
		$uid = isset($indata['uid']) ? intval($indata['uid']) : '';		//uid
		
		if(!$uid){
			$this->info['message'] = '请求失败！';
			$this->callBack();
		}
		//好友个数及积分
		$ji = array('1'=>'one','2'=>'two','3'=>'three');
		$friends_info = $this->db->GetList("SELECT count(*) as num FROM `@#_member_friendship` WHERE `yaoqing` = '$uid' GROUP BY `lev` ORDER BY `lev` asc ");
		$friends = array('one'=>array('num'=>0,'jifen'=>0),'two'=>array('num'=>0,'jifen'=>0),'three'=>array('num'=>0,'jifen'=>0));		//默认值
		foreach($friends_info as $k => $v){
			$lev = $k + 1;
			$jifen_info  = $this->db->GetOne("SELECT sum(`jifen`) as jifen FROM `@#_member_jifen` WHERE `yaoqing` = '$uid' AND `lev` = '$lev' ");
			$friends[$ji[$k+1]]['num'] = $v['num'];
			$friends[$ji[$k+1]]['jifen'] = $jifen_info['jifen'] ? intval($jifen_info['jifen']) : 0;
		}
		//最新奖励详情
		$new_reward = array();
		$new_reward = $this->db->GetList("SELECT uid,jifen FROM `@#_member_jifen` WHERE `yaoqing` = '$uid' ORDER BY `addtime` DESC LIMIT 10  ");
		if(count($new_reward)){
			foreach($new_reward as $k => $v){
				$new_reward[$k]['jifen'] = $v['jifen'] ? intval($v['jifen']) : 0;
				$uinfo = $this->get_user_fields($v['uid'], 'mobile,email');		//获取手机号码
				$new_reward[$k]['mobile'] = $uinfo['mobile'] ? : $uinfo['email'];
			}
		}
		//组合数据
		$data = array('friends'=>$friends, 'reward'=>$new_reward);
		
		$this->info = array('status'=>1, 'message'=>'请求成功', 'data'=>$data);
		$this->callBack();
	}
	
	/*
	* Ⅲ获取更多奖励详情
	*/
	public function get_reward_list()
	{
		$indata = $this->getData();		//获取数据
		$this->checkSign($indata);		//验证签名
		$uid = isset($indata['uid']) ? intval($indata['uid']) : '';		//uid
		$size = isset($indata['size']) ? intval($indata['size']) : 20;		//分页大小
		$page = isset($indata['page']) ? intval($indata['page']) : 1;		//第几页
		
		if(!$uid){
			$this->info['message'] = '请求失败！';
			$this->callBack();
		}
		
		$wh = " `yaoqing` = '$uid' ";
		$total = $this->db->GetCount("SELECT id FROM @#_member_jifen WHERE $wh ");
		if(!$total){
			$this->info = array('status'=> 1, 'message'=>'暂无数据','data'=>array('total'=>0));
			$this->callBack();
		}
		$page_count = intval($total / $size);
		$max_page = $total % $size == 0 ? $page_count : $page_count + 1;
		if($page > $max_page){
			$this->info = array('status'=> 1, 'message'=>'后面没有数据了','data'=>array('total'=>0));
			$this->callBack();
		}
		$start = $page <= 1 ? 0 : ($page-1) * $size;	//从第几条数据开始取
		$sql = "SELECT uid,jifen,addtime FROM @#_member_jifen WHERE $wh ORDER BY `addtime` DESC LIMIT $start,$size";
		$list = $this->db->GetList($sql);
		if(!count($list)){
			$this->info = array('status'=> 1, 'message'=>'后面没有数据了','data'=>array('total'=>0));
			$this->callBack();
		}
		foreach($list as $k => $v){
			$list[$k]['addtime'] = date('Y-m-d H:i',$v['addtime']);
			$uinfo = $this->get_user_fields($v['uid'], 'mobile,email');
			$list[$k]['mobile'] = $uinfo['mobile'] ? : $uinfo['email'];
			$list[$k]['jifen'] = $v['jifen'] ? intval($v['jifen']) : 0;
			unset($list[$k]['uid']);
		}
		$data['total'] = $total;
		$data['max_page'] = $max_page;
		$data['list'] = $list;
		
		$this->info = array('status'=> 1, 'message'=>'请求成功','data'=>$data);
		$this->callBack();
	}
	
	//确认收货地址  20151228
	public function do_confirm_address()
	{
	    $indata = $this->getData();		//获取数据
	    $this->checkSign($indata);		//验证签名
	    $uid = isset($indata['uid']) ? intval($indata['uid']) : 0;	//当前用户ID
	    $rid = isset($indata['rid']) ? intval($indata['rid']) : 0;	//中奖记录ID
	    $aid = isset($indata['aid']) ? intval($indata['aid']) : 0;	//收货地址的ID
	     
	    if( ! $uid || ! $rid ||  ! $aid){
	        $this->info['message'] = '请求失败';
	        $this->callBack();
	    }
	     
	    $record = $this->db->GetOne("SELECT id FROM @#_member_go_record WHERE `uid`='$uid' AND `id`='$rid' LIMIT 1");
	    if(!$record){
	        $this->info['message'] = '请求失败';
	        $this->callBack();
	    }
	    $address = $this->db->GetOne("SELECT * FROM @#_member_dizhi WHERE `uid`='$uid' AND `id`='$aid' LIMIT 1");
	    if( ! $address){
	        $this->info['message'] = '请求失败';
	        $this->callBack();
	    }
	     
	    $confrim_addr_time = time();
	    $confrim_addr = $address['sheng'].$address['shi'].$address['xian'].$address['jiedao'];
	    $confrim_addr_uinfo = $address['shouhuoren'] .'|'. $address['mobile'];
	     
	    $sql = "UPDATE @#_member_go_record SET `confrim_addr` = '$confrim_addr',`confrim_addr_time`='$confrim_addr_time',".
	        "`confrim_addr_uinfo` = '$confrim_addr_uinfo' WHERE `id`='$rid' AND `uid`='$uid' AND `confrim_addr_time` IS NULL LIMIT 1";
	     
	    $rows = $this->db->Query($sql);
	    if(!$rows){
	        $this->info['message'] = '操作失败';
	        $this->callBack();
	    }
	     
	    $this->info = array('status'=>1, 'message'=>'操作成功', 'data'=>date('Y-m-d H:i',$confrim_addr_time));
	    $this->callBack();
	}
	
	//获取关于一元众乐、常见问题、通知的跳转链接
	public function get_setting_info()
	{
	    $indata = $this->getData();		//获取数据
	    $this->checkSign($indata);		//验证签名
	    if($this->is_android($indata)){
	    	$data = array(
		        //'about' => G_WEB_PATH.'/yungou/#/tab/about',   //关于一元众乐
		        //'problem' => G_WEB_PATH.'/yungou/#/tab/question', //常见问题
		        //'notice' => G_WEB_PATH.'/yungou/#/tab/notification',  //通知
		        'download'=>G_WEB_PATH .'/apk/1y_cs.apk'  //下载页面
		        //'yaoqing'=> G_WEB_PATH . '/yungou/#/tab/invite',     //邀请攻略
		        //'fanli'=> G_WEB_PATH . '/yungou/#/tab/friendRebate'  //什么是好友返利
		    );
	    }
		if($this->is_ios($indata)){
	    	$data = array(
		        //'about' => G_WEB_PATH.'/yungou/#/tab/about',   //关于一元众乐
		        //'problem' => G_WEB_PATH.'/yungou/#/tab/question', //常见问题
		        //'notice' => G_WEB_PATH.'/yungou/#/tab/notification',  //通知
		        'download'=>'https://itunes.apple.com/cn/app/yi-yuan-cai-shen/id1138754653?mt=8', //下载页面
		        //'yaoqing'=> G_WEB_PATH . '/yungou/#/tab/invite',     //邀请攻略
		        //'fanli'=> G_WEB_PATH . '/yungou/#/tab/friendRebate'  //什么是好友返利
		    );
	    } 
	    $this->info = array('status'=>1, 'message'=>'请求成功', 'data'=>$data);
	    $this->callBack();
	}
	
	//检查更新
	public function do_check_update()
	{
	    $indata = $this->getData();		//获取数据
	    $this->checkSign($indata);		//验证签名
	    
	    
	    $android = System::load_sys_config('android');
	    $ios = System::load_sys_config('ios');
	    
	    $data = array(
	        'android' => array(
	            'link'=>$android['ver_link'],
	            'name'=>$android['ver_name'],
	            'code'=>$android['ver_code']
	        ),
	        'ios' => array(
	            'link'=>$ios['ver_link'],
	            'name'=>$ios['ver_name'],
	            'code'=>$ios['ver_code']
	        )
	    );
	    
	    $this->info = array('status'=>1, 'message'=>'请求成功', 'data'=>$data);
	    $this->callBack();
	}
	
	//通过充值订单号，查看充值状态   2015/12/30
	public function get_recharge_status()
	{
	    $indata = $this->getData();		//获取数据
	    $this->checkSign($indata);		//验证签名
	    $uid = isset($indata['uid']) ? intval($indata['uid']) : 0;	//当前用户ID
	    $code = isset($indata['code']) ? trim($indata['code']) : '';	//充值订单号
	    $uid = $_REQUEST['uid'];
	    if( ! $uid || ! $code){
	       $this->info['message'] = '请求失败';
	       $this->callBack();
	    }
	    
	    $rows = $this->db->GetOne("SELECT id,status FROM @#_member_addmoney_record WHERE `uid`='$uid' AND `code`='$code' LIMIT 1");
	    if(!$rows){
	        $this->info['message'] = '记录不存在';
	        $this->callBack();
	    }
	    if($rows['status'] !== '已付款'){
	        $this->info['message'] = '充值失败';
	        $this->callBack();
	    }
	    $this->info = array('status'=>1,'message'=>'充值成功');
	    $this->callBack();
	}
	
	
/***********************************************************************************************/
	//发送验证码
	private function send_mobile_code($mobile = '', $type = 'template_mobile_reg'){
			
		$type_num = array(
						'template_mobile_shop'	=>1,	//发送中奖信息
						'template_mobile_reg'	=>2,	//发送注册验证码码
						'template_mobile_pwd'	=>3,	//发送找回密码验证码
						'template_mobile_change'=>4,	//发送绑定手机验证码
					);
					
		$info = array('0','手机号格式错误！');
		if(_checkmobile($mobile)){
            $checkcodes = rand(100000,999999);//验证码
		    $time = time();	
		  	//2分钟只能只能发一次验证码
			$sql="select time from @#_mobile_code where mobile='$mobile' and type='$type'";
			$result = $this->db->Query($sql);
			if(!empty($result)&& $result['time']>0){
				if($time - $result['time'] > 120){
					$info=array('0','2分钟以内您已收到注册验证码！');
					return $info;
					die;
				}
			}else{
				//将手机号、验证码、时间戳插入数据库
				$sql = "INSERT INTO `@#_mobile_code`(mobile,mobilecode,type,time) VALUES('$mobile','$checkcodes','$type_num','$time')";
				$this->db->Query($sql);
			}

			$type_num = $type_num[$type];
			
			
			//获取短信模板
			$SendCon = System::load_contorller('Send','Zapi');
			$SendCon->type = $type_num;			//验证码类型  int型
			$SendCon->mobile = $mobile;			//手机号码
			$SendCon->code   = $checkcodes;		//验证码
			$result = $SendCon->send_msg();		//发送内容
			if($result){
				$info = array('1','发送成功');				
			}else{
				$info = array('0','发送失败，联系技术人员');
			}
			//$info = _sendmobile($mobile,$content);		//老版本发送短信
		}
		return $info;
	}
	
	//验证用户提交的验证码是否正确
	public function check_code_is_pass($mobile = '', $code = '', $type = 'is_reg')
	{
		$status = false;
		if( ! $mobile ||  ! $code){
			return $status;
		}
		
		$time = time();
		$start_time = $time - 120;	//假设验证码2分钟内有效
		$code_info = $this->db->GetList("SELECT * FROM @#_mobile_code WHERE `mobile` = '$mobile' AND `time` > $start_time ORDER BY `time` DESC ");
		if(count($code_info)){
			foreach($code_info as $k => $v){
				$codes[] = $v['mobilecode'];
			}
			$status = in_array($code,$codes) ? true : false;
			if($status){
				$this->db->Query("UPDATE `@#_mobile_code` SET `$type`=1 where `mobile`='$mobile' AND `mobilecode` = '$code' ");
			}
		}
		return $status;
	}

	//验证手机号是否可以注册
	public function check_mobile_is_register($mobile = '')
	{
		$info = array('status'=>0, 'message'=>'该手机号已被注册');
		if(! _checkmobile($mobile)){
			$info['message'] = '手机号码不合法';
			return $info;
		}
		
		$reg_info = $this->db->GetCount("SELECT `uid` FROM @#_member WHERE `mobile` = '$mobile' ");
		if( ! $reg_info){
			$info['status'] = 1;
			$info['message'] = '恭喜，此号码可以注册';
		}
		return $info;
	}
	
	//获取改期该商品的得奖信息
	private function get_dejiang_info($shopid = '', $qishu = '',$uid){
		if(!$shopid || !$qishu){
			$this->info['message'] = '请求失败！';
			$this->callBack();
		}else{
            //,is_shi,shenyurenshu,qishu,id
			$sql = "SELECT thumb,zongrenshu,canyurenshu,q_user,q_uid,q_user_code,q_end_time,q_showtime,is_shi,shenyurenshu,qishu,id FROM `@#_shoplist` WHERE id=$shopid AND qishu='$qishu'";
			$arr_one = $this->db->GetOne($sql);
            $GoodListCon = System::load_contorller('GoodList','Zapi');
            $arr_one = $GoodListCon->list_add_user_bought_number($arr_one,$uid);
			if(intval($arr_one['q_uid'])>0){
				if($arr_one['q_showtime']=='Y'){
					$arr_one['tag'] = 2;	//tag  0已揭晓  1进行中  2倒计时
					//$arr_one['q_end_time'] = "请稍后，正在揭晓中......";
				}else{
					$arr_one['tag'] = 0;  //tag  0已揭晓  1进行中  2倒计时
					$arr_one['q_user'] = unserialize($arr_one['q_user']);
					$arr_one['q_user']['q_user_code'] = $arr_one['q_user_code'];
					$records_info = $this->get_member_go_record($shopid, $arr_one['q_user']['uid'], $qishu);
					$arr_one['q_user']['gonumber'] = $records_info['gonumber'];
					$arr_one['q_end_time'] = date("Y-m-d H:i:s",$arr_one['q_end_time']);
					$temp = !empty($arr_one['q_user']['mobile']) ? trim($arr_one['q_user']['mobile']) : trim($arr_one['q_user']['email']);
					$temp = $this->nick_replace($temp);
					$arr_one['q_user']['username'] = !empty($arr_one['q_user']['username']) ? trim($arr_one['q_user']['username']) : $temp;
					$arr_one['mobile'] = $arr_one['q_user']['mobile'];
					unset($arr_one['q_user_code'],$records_info,$temp,$arr_one['q_user']['img'],$arr_one['q_user']['email'],$arr_one['q_user']['mobile']);
				}
			}else{
				$arr_one['tag'] = 1;  //tag  0已揭晓  1进行中  2倒计时
				$arr_one['q_end_time'] = '进行中......';
			}
			$arr_one['thumb'] = strpos($arr_one['thumb'],G_UPLOAD_PATH) !== false ? $arr_one['thumb']  : G_UPLOAD_PATH . '/' . $arr_one['thumb'];
			$arr_one['q_showtime'] = $arr_one['q_showtime'];
		}
		return $arr_one;	
		
	}
	
	//获取某件商品最新一起的云购记录，这里主要是为了拿到最新一期的商品id，以便 猜你喜欢中 通过id来跳转到别的页面
	private function get_zuixin_goods_shoplist($sid){
		
		if(!$sid){
			$this->info['message'] = "请求失败！";
		}
		$sql = "SELECT `id`,`canyurenshu`,`zongrenshu` FROM `@#_shoplist` WHERE sid = '$sid' AND `q_uid` IS NULL ORDER BY `id` DESC LIMIT 1";
		$one_arr = $this->db->GetOne($sql);
		return $one_arr;
		
	}
	
	//裁剪图像
	private function cut_image($filesname)
	{
		System::load_sys_class('upload','sys','no');
		upload::thumbs(160,160,false,$filesname);
		upload::thumbs(80,80,false,$filesname);
		upload::thumbs(30,30,false,$filesname);	
	}
	
	//第三方登录注册新会员
	private function add_partner_user($data = array())
	{
	    $info = array('status'=>0, 'message'=>'登录失败');
	     
	    if(!$data) return $info;
	     
	    $this->db->Autocommit_start();
	     
	    //入库所需数据
	    $type = isset($data['type']) ? trim($data['type']) : '';
	    $username = isset($data['nickname']) ? trim($data['nickname']) : '';
	    $password = md5('123456');
	    $img = isset($data['touimg']) ? trim($data['touimg']) : 'photo/member.jpg';
	    $band = isset($data['type']) ? trim($data['type']) : '';   //qq weibo weixin
	    $time = time();
	    $user_ip = _get_ip_dizhi();
	    $code = isset($data['code']) ? trim($data['code']) : '';   //qq>openid  weibo>token  weixin>openid
	     
	    $query1 = $this->db->Query("INSERT INTO @#_member(username,password,img,band,time,user_ip,login_time) VALUES('$username','$password','$img','$band','$time','$user_ip','$time')");
	     
	    $uid = $this->db->insert_id();
	     
	    $query2 = $this->db->Query("INSERT INTO @#_member_band(b_uid,b_type,b_code,b_time) VALUES('$uid','$type','$code','$time')");
	
	    if( ! ( $query1 && $query2 ) ){
	        $this->db->Autocommit_rollback();
	        return $info;
	    }
	     
	    $this->db->Autocommit_commit();
	     
	    $userinfo = $this->get_user_fields($uid,'uid,mobilecode,emailcode,username,email,mobile,money,img,yaoqing');
	     
	    //统计已有多少人获得返利
	    $userinfo['rebate_total'] = $this->get_rebate_total();
	     
	    $info = array('status'=>1, 'message'=>'登录成功', 'data'=>$userinfo );
	    return $info;
	}
	
	//更新第三方登录的会员的信息
	private function update_partner_user($data = array())
	{
	    $info = array('status'=>0, 'message'=>'登录失败');
	    $uid = isset($data['uid']) ? intval($data['uid']) : '';
	    if(!$uid){
	        return $info;
	    }
	    $time = time();
	    $user_ip = _get_ip_dizhi();
	     
	    $query = $this->db->Query("UPDATE @#_member SET `user_ip`='$user_ip',`login_time`='$time' WHERE `uid`='$uid' LIMIT 1 ");
	    if($query){
	        $userinfo = $this->get_user_fields($uid,'uid,mobilecode,emailcode,username,email,mobile,money,img,yaoqing');
	        //统计已有多少人获得返利
	        $userinfo['rebate_total'] = $this->get_rebate_total();
	        $info = array('status'=>1, 'message'=>'登录成功', 'data'=>$userinfo );
	    }
	    return $info;
	}
	
	
	public function test_code()
	{
	   $this-> get_member_qrcode(1);
	}
	
}