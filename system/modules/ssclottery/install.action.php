<?php 

defined('G_IN_SYSTEM')or exit('');
System::load_app_class('admin',G_ADMIN_DIR,'no');

class install extends SystemAction {
	
	public function __construct(){
		$this->db=System::load_sys_class('model');
	}
		
	public function init(){	
		$q1 = $this->db->GetOne("
			SELECT * from `@#_caches` WHERE `key`='ssclottery'
			");
		if(!$q1){	
			$q1 = $this->db->Query("
				INSERT INTO `@#_caches` (`key`, `value`) VALUES ('ssclottery', '1')
				");
			}
		$q2 = $this->db->Query("
			ALTER TABLE `@#_shoplist`
			ADD COLUMN `q_ssccode`  char(20) NULL AFTER `q_showtime`,
			ADD COLUMN `q_sscopen`  varchar(100) NULL AFTER `q_ssccode`,
			ADD COLUMN `q_sscphase`  varchar(100) NULL AFTER `q_sscopen`,
			ADD COLUMN `q_djstime`  char(20) NULL AFTER `q_sscphase`
			");	
		if($q1&&$q2){
			// unset(__FILE__);
			_message("安装成功!");
		}

	}	
}