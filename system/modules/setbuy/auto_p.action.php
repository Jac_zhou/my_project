<?php
defined('G_IN_SYSTEM')or exit('no');
System::load_app_fun('global',G_ADMIN_DIR);

class auto_p extends SystemAction {
    private $db;
    private $categorys;
    private $pay;
    private $autodir = "setbuy";#模块文件名
    public function __construct(){
        $this->db = System::load_sys_class("model");
        $this->categorys=$this->db->GetList("SELECT * FROM `@#_category` WHERE 1 order by `parentid` ASC,`cateid` ASC",array('key'=>'cateid'));
        $this->pay = System::load_app_class("pay","pay");
    }
    #操作界面显示
    public function show(){
        /*$p = $this->segment(4);
        if($p == null ){
            $p = 1;
        }
        $num=110;
        $total=$this->db->GetCount("SELECT COUNT(*) FROM `@#_shoplist` WHERE `q_uid` is null  order by `id` DESC");
        $page=System::load_sys_class('page');
        #if(isset($_GET['p'])){$pagenum=$_GET['p'];}else{$pagenum=1;}
        $page->config($total,$num,1,"0");
        $shoplist=$this->db->GetPage("SELECT * FROM `@#_shoplist` WHERE `q_uid` is null  order by `id` DESC ",array("num"=>$num,"page"=>$p,"type"=>1,"cache"=>0));
        */

        $shoplist=$this->db->GetList("SELECT * FROM `@#_shoplist` WHERE `q_uid` is null  order by `id` DESC ");

        #获取配置文件信息
        $xml = $this->getxml();
        $times = $xml->times;//时间
        $endtimes = intval($xml->endtimes);
        $userid  = explode("-",$xml->userid);//用户ID段
        $number  = explode("-",$xml->number);//用户ID段
        $gonumber  =$xml->gonumber;
        $shopid = $xml->shopid;//商品ID 以“-” 分割
        $shopidarray = explode("-",$shopid);
        $oo = $xml->oo;//开启或关闭状态
        $runtime = $xml->runtime;//运行时间
        $autoadd = $xml->autoadd;//是否自动进入下一期
        $mshop = $xml->mshop;//是否购买多个商品
        $timeperiod = $xml->timeperiod;//时间段
        $tp = explode("-",$timeperiod);
        //线程是否异常停止
        $isstop = -1;
        //页码
        /*if($p == 1){
            $o_p =1;
            $n_p = 2;
        }else{
            $o_p = $p-1;
            $n_p = $p+1;
        }*/

        /*----------判断线程是否死掉----------*/
        if($oo == "on" && ($runtime+$endtimes+30) < time() ){
            $isstop = 0;//已经停止
        }else{
            $isstop = 1;//还在运行
        }
        include $this->tpl($this->autodir,'auto_p');
    }
    //获取配置文件信息
    public function getxml(){
        $xml = simplexml_load_file(G_APP_PATH.'system/modules/'.$this->autodir.'/auto_p.xml');
        return $xml->children();
    }
    /**
     *更新xml 配置文件信息
     * $xml  array
     **/
    public function updatexml($xml){
        $newxml = '<?xml version="1.0" encoding="utf-8"?>
<note>
<oo>'.$xml['oo'].'</oo>
<runtime>'.$xml['runtime'].'</runtime>
<times>'.$xml['times'].'</times>
<userid>'.$xml['userid'].'</userid>
<number>'.$xml['number'].'</number>
<gonumber>'.$xml['gonumber'].'</gonumber>
<shopid>'.$xml['shopid'].'</shopid>
<autoadd>'.$xml['autoadd'].'</autoadd>
<endtimes>'.$xml['endtimes'].'</endtimes>
<mshop>'.$xml['mshop'].'</mshop>
<timeperiod>'.$xml['timeperiod'].'</timeperiod>
</note>';
        file_put_contents(G_APP_PATH.'system/modules/'.$this->autodir.'/auto_p.xml',$newxml);
    }
    //功能开启
    public function start($xmlarray){
        //获取配置文件信息
        $xml = $this->getxml();
        if($xml->oo =="on"){//开启状态  只更新配置文件
            $this->updatexml($xmlarray);
            echo "更新配置文件成功";
        }else {//非开启状态
            $this->updatexml($xmlarray);
            $this ->xhaction();
        }
    }
    //功能关闭
    public function stop(){
        $xml = $this->getxml();

        //删除指定范围 20160113
        //$shopids = str_replace('-',',',$xml->shopid);
        //$this->db->Query("UPDATE @#_shoplist SET `zdrange`='' WHERE `shenyurenshu`>0 AND `id` in($shopids)");

        //关闭自动购买 20160113
        $this->db->Query("UPDATE `@#_caches` SET `value`='0' WHERE `key`='is_auto_buy' ");

        $xmlarray = array("oo"=>"off","runtime"=>$xml->runtime,"times"=>$xml->times,"userid"=>$xml->userid,"number"=>$xml->number,"gonumber"=>$xml->gonumber,"shopid"=>$xml->shopid,"autoadd"=>$xml->autoadd,'endtimes'=>$xml->endtimes,"mshop"=>$xml->mshop,"timeperiod"=>$xml->timeperiod);
        $this->updatexml($xmlarray);
    }
    //保存配置----并开起
    public function ajaxaction(){
        $neichun = memory_get_usage();
        file_put_contents("test.txt","开始内存：".$neichun."\r\n",FILE_APPEND);
        $m_shop_value = isset($_POST['m_shop_value'])?$_POST['m_shop_value']:-1;//随机购买多个商品
        $times = isset($_POST['times'])?intval($_POST['times']):-1;//最小间隔时间
        $endtimes = isset($_POST['endtimes'])?intval($_POST['endtimes']):-1;//最大间隔时间
        $f_userid = isset($_POST['f_userid'])?intval($_POST['f_userid']):-1;//用户段---开始IP  （包含此IP）
        $l_userid = isset($_POST['l_userid'])?intval($_POST['l_userid']):-1;//用户段----结束IP （包含此IP）
        $f_number = isset($_POST['f_number'])?intval($_POST['f_number']):1;
        $l_number = isset($_POST['l_number'])?intval($_POST['l_number']):2;
        $gonumber = isset($_POST['gonumber'])?trim($_POST['gonumber']):"1,2,3,4,5,6,7,8,9,10,12,15,18,20,30,50,100";	//随机购买次数
        $autoadd = isset($_POST['autoadd'])?intval($_POST['autoadd']):-1;//是否自动进入下一期
        $shopid = isset($_POST['shopid'])?$_POST['shopid']:-1;//商品ID群
        $timeperiod = isset($_POST['timeperiod'])?$_POST['timeperiod']:0;
        if((!@eregi('^[0-9]*$',$times)) || $times <= 0 || $endtimes <=0 || $endtimes <= $times ){
            echo  '时间参数错误';
            return;
        }
        if($f_userid <= 0 || $l_userid <= 0 ||  $l_userid <= $f_userid){
            echo "用户段参数错误";
            return;
        }
        if($f_number <= 0 || $l_number <= 0 ||  $l_number <= $f_number){
            echo "购买次数参数错误";
            return;
        }
        if($shopid <= 0){
            echo "商品信息错误";
            return;
        }
        #------时间区间
        $tp = explode("-",$timeperiod);
        if(count($tp) != 0){
            foreach($tp as $k=>$v){
                if(intval($v)>23 && intval($v)<0){
                    echo "时间区间错误";
                    exit;
                }
            }
        }else{
            echo "时间区间错误";
            exit;
        }
        $userid = $f_userid.'-'.$l_userid;
        $number = $f_number.'-'.$l_number;


        //开启自动购买 20160113
        $this->db->Query("UPDATE `@#_caches` SET `value`='1' WHERE `key`='is_auto_buy' ");

        //插入指定范围 20160113
        $this->db->Query("UPDATE `@#_shoplist` SET `zdrange`='' WHERE `shenyurenshu`>0 AND `zdrange` != '' ");
        $shopids = str_replace('-',',',$shopid);
        $this->db->Query("UPDATE `@#_shoplist` SET `zdrange`='$userid' WHERE `shenyurenshu`>0 AND `id` in($shopids)");


        //更新配置文件xml
        $xml = $this->getxml();
        $xmlarray = array("oo"=>"on","runtime"=>$xml->runtime,"times"=>$times,"userid"=>$userid,"number"=>$number,"gonumber"=>$gonumber,"shopid"=>$shopid,"autoadd"=>$autoadd,"endtimes"=>$endtimes,"mshop"=>$m_shop_value,"timeperiod"=>$timeperiod);
        $this->start($xmlarray);
    }

    
    /*
        获取商品id
        返回商品id一维数组
    */
    public function getshopid($xml,$mshop=1){
        $shopidarray = explode("-",$xml->shopid);#配置文件商品ID    数组
        $shopid = array();
        #生成商品ID数组
        #随机生成购买商品个数  多少个不同商品
        if($mshop == 0){
            $shopnum = 1;
            $num = rand(0,count($shopidarray)-1);
            $shopid[0] = $shopidarray[$num];
        }else{
            //需要购买多少产品(随机)
            $auto_shop_num = count($shopidarray);
            $shopnum = rand(1,$auto_shop_num);
            for($i=0;$i<$shopnum;$i++){
                $num = rand(0,$auto_shop_num-1);
                $shopid[] =$shopidarray[$num];
            }
        }
        return $shopid;
    }
    #程序异常时重启
    public function  errorrestart(){
        $xml = $this->getxml();
        $this->updatexml(array("oo"=>"off","runtime"=>$xml->runtime,"times"=>$xml->times,"userid"=>$xml->userid,"number"=>$xml->number,"gonumber"=>$xml->gonumber,"shopid"=>$xml->shopid,"autoadd"=>$xml->autoadd,'endtimes'=>$xml->endtimes,'mshop'=>$xml->mshop,"timeperiod"=>$xml->timeperiod));
        $this->start(array("oo"=>"on","runtime"=>$xml->runtime,"times"=>$xml->times,"userid"=>$xml->userid,"number"=>$xml->number,"gonumber"=>$xml->gonumber,"shopid"=>$xml->shopid,"autoadd"=>$xml->autoadd,'endtimes'=>$xml->endtimes,'mshop'=>$xml->mshop,"timeperiod"=>$xml->timeperiod));
    }

    #随机生成IP 中国区
    public function randip($member){
        if($member['user_ip']){
            $ip = explode(',',$member['user_ip']);
            return $ip[1];
        }else{
            $ip_1 = -1;
            $ip_2 = -1;
            $ip_3 = rand(0,255);
            $ip_4 = rand(0,255);
            $ipall = array(
                array(array(58,14),array(58,25)),
                array(array(58,30),array(58,63)),
                array(array(58,66),array(58,67)),
                array(array(60,200),array(60,204)),
                array(array(60,160),array(60,191)),
                array(array(60,208),array(60,223)),
                array(array(117,48),array(117,51)),
                array(array(117,57),array(117,57)),
                array(array(121,8),array(121,29)),
                array(array(121,192),array(121,199)),
                array(array(123,144),array(123,149)),
                array(array(124,112),array(124,119)),
                array(array(125,64),array(125,98)),
                array(array(222,128),array(222,143)),
                array(array(222,160),array(222,163)),
                array(array(220,248),array(220,252)),
                array(array(211,163),array(211,163)),
                array(array(210,21),array(210,22)),
                array(array(125,32),array(125,47))
            );
            $ip_p = rand(0,count($ipall)-1);#随机生成需要IP段
            $ip_1 = $ipall[$ip_p][0][0];
            if($ipall[$ip_p][0][1] == $ipall[$ip_p][1][1]){
                $ip_2 = $ipall[$ip_p][0][1];
            }else{
                $ip_2 = rand(intval($ipall[$ip_p][0][1]),intval($ipall[$ip_p][1][1]));
            }
            return $ip_1.'.'.$ip_2.'.'.$ip_3.'.'.$ip_4;
        }

    }
    
    public function xhaction(){
        ignore_user_abort(true);
        set_time_limit(0);
        #设定从新开起
        $xml = $this->getxml();
        while($xml->oo == 'on'){           
            log_append('log/robot.log','开启！');
            #获取配置文件信息判断是否退出
            $timeperiod_arr = explode('-',$xml->timeperiod);//开启自动购买的时段
            //用来判断当前是否为自动购买时间段
            $time_current = date('G');                      //当前时段
            if(in_array($time_current,$timeperiod_arr)){    //当前时段是否为自动购买时段
                #调用购买
                //查询区间用户机器人数组
                $userid_arr = $this->get_robot_user($xml);
                //log_append('log/robot.log','机器人数组',$userid_arr);
                if($userid_arr){
                    $robot_num = count($userid_arr);
                    $user_rand_id = rand(0,$robot_num-1);
                    $uid = $userid_arr[$user_rand_id];
                    $sql = "SELECT * FROM `@#_member` WHERE uid='$uid'";
                    $member = $this->db->GetOne($sql);
                    if($member){
                        log_append('log/robot.log','前去购买！',$member['username'].$member['uid']);
                        $this->auto_go_buy($xml,$member['uid']);
                    }else{
                        log_append('log/robot.log','没有用户信息');
                    }

                }else {
                    log_append('log/auto_buy_result.log', '用户区间没有机器人用户！');
                    exit;
                }
            }else{
                #不购买时只更新配置文件的时间
                $this->updatexml(array("oo"=>$xml->oo,"runtime"=>time(),"times"=>$xml->times,"userid"=>$xml->userid,"number"=>$xml->number,"gonumber"=>$xml->gonumber,"shopid"=>$xml->shopid,"autoadd"=>$xml->autoadd,'endtimes'=>$xml->endtimes,"mshop"=>$xml->mshop,"timeperiod"=>$xml->timeperiod));
            }
            //休眠时间  #最小间隔时间   #最大间隔时间
            sleep(rand(intval($xml->times),intval($xml->endtimes)));
            $xml = $this->getxml();     //循环完毕便再次获取一遍配置文件
        }
        exit;
    }

    /** 获取区间内的机器人
     * @param $xml
     * @return array|bool
     */
    public function get_robot_user($xml){
        $userid_arr = explode('-',$xml->userid);
        $sql = "SELECT uid  FROM `@#_member` WHERE auto_user=1 AND uid BETWEEN '{$userid_arr[0]}' AND '{$userid_arr[1]}'";
        //log_append('log/robot.log','查询语句：',$sql);
        $user_id_arr = $this->db->GetList($sql);
        $number = count($user_id_arr);
        if($number>0){
            foreach($user_id_arr as $key=>$value){
                $uid_arr[] = $value['uid'];
            }
            unset($user_id_arr);
            return $uid_arr;
        }else{
            return false;
        }
    }

    /** 自动购买
     * @param $xml
     * @param $member   自动购买的用户
     */
    public function auto_go_buy($xml,$uid){
        //是否购随机购买多个商品
        $mshop = intval($xml->mshop);
        if($mshop == 1){
            #生成商品ID数组
            $shopid = $this->getshopid($xml);
            foreach($shopid  as $key=>$value){
                $id = intval($value); 
                $post_arr = array('uid'=>$uid,'shopid'=>$id);
                _g_triggerRequest(WEB_PATH.'/setbuy/auto_p/buyshop', false, $post_arr);
            }
        }else{
            $shopid = $this->getshopid($xml,0);
            $id = $shopid[0];
            $post_arr = array('uid'=>$uid,'shopid'=>$id);
            _g_triggerRequest(WEB_PATH.'/setbuy/auto_p/buyshop', false, $post_arr);    
        }
    }

    

    //购买商品
    public function buyshop(){
        //uid,shopid,uid

        $xml = $this->getxml();
        $shopid = intval($_POST['shopid']);
        $uid = intval($_POST['uid']);
        //购买商品数量
        $sql = "SELECT * FROM `@#_member` WHERE uid='$uid'";
        $member = $this->db->GetOne($sql);
        
        $sql = "SELECT * FROM `@#_shoplist` WHERE id='$shopid'";
        $shopinfo = $this->db->GetOne($sql);
        
        //购买数量 方案1 ：给定范围 如1~100
        $number_array = explode("-",$xml->number);
        if($number_array[0]>0 && $number_array[1]>0 && $number_array[1]>$number_array[0]){
            $shopnum = rand(intval($number_array[0]), intval($number_array[1]));
        }else{
            //购买数量 方案2：固定次数，随机取 如1，2，3，4，5
            $gonumber_temp = str_replace('，',',',$xml->gonumber);
            $gonumber_arr = explode(',',$gonumber_temp);
            $shopnum = $gonumber_arr[array_rand($gonumber_arr)];
        }
        if(!$shopnum){
            log_append('log/auto_buy_result.log','购物数量出错！',$shopnum);
            exit;
        }
        $shopidarray = explode("-",$xml->shopid);//配置文件商品ID    数组
        $user_id = $member['uid'];
        $shopid = $shopinfo['id'];
        $time = time();
        $GoodListCon = System::load_contorller('GoodList','Zapi');
        $shopinfo = $GoodListCon->list_add_user_bought_number($shopinfo,$user_id);
        //判断商品是否购买完
        if($shopinfo['canyurenshu']<$shopinfo['zongrenshu']){
            if($shopinfo['wap_ten']>0){     //判断当前机器人用户是否可以继续购买
                if($shopnum>$shopinfo['wap_ten']){
                    $shopnum = $shopinfo['wap_ten'];
                }
                if((intval($shopinfo['yunjiage'])*$shopnum) > $member['money']){//商品价格大于用户金钱---给用户充值
                    $m = intval($shopinfo['yunjiage'])*$shopnum;
                    $this->db->Query(" UPDATE `@#_member` SET  `money` = '$m' WHERE `uid` = '$user_id' ");
                }
                //设置IP
                $_SERVER['HTTP_CLIENT_IP'] = $this->randip($member);
                //调用购买商品接口
                log_append('log/auto_buy_result.log','调用购买商品接口');      
                $this->pay->pay_user_go_shop($user_id,$shopid,$shopnum);
            }else{
                log_append('log/auto_buy_result.log','机器人购买本产品达到上限！');     
            }  
        }else{#如果已经购买完，就删除配置
            #implode(', ',$tags);
            $t = array();
            $x = -1;
            for($i = 0; $i<count($shopidarray);$i++){
                $x++;
                if($shopid != $shopidarray[$i]){
                    $t[$x] = $shopidarray[$i];
                }
            }
            if(count($shopidarray) == 1){
                $xml->shopid = '';
            }else{
                $xml->shopid = implode('-',$t);
            }
            $autoadd = intval($xml->autoadd);
            #判断是否进入下一期数
            if($autoadd == 1 ){
                #需要进入下一期
                #添加下一期的商品ID值
                $shoptem  = $this->db->GetOne("SELECT * FROM `@#_shoplist` WHERE `id` = '$shopid'  LIMIT 1");
                $nextshopsid = $shoptem['sid'];
                $nextshopinfo  = $this->db->GetOne("SELECT * FROM `@#_shoplist` WHERE `sid` = '$nextshopsid' ORDER BY  `qishu` DESC LIMIT 1");
                if($nextshopinfo['qishu'] < $nextshopinfo['maxqishu']){
                    if($xml->shopid == null || $xml->shopid == ""){
                        $xml->shopid = $nextshopinfo['id'];
                    }else{
                        $xml->shopid = ($xml->shopid).'-'.$nextshopinfo['id'];
                    }
                }
            }
        }
        $this->updatexml(array("oo"=>$xml->oo,"runtime"=>$time,"times"=>$xml->times,"userid"=>$xml->userid,"number"=>$xml->number,"gonumber"=>$xml->gonumber,"shopid"=>$xml->shopid,"autoadd"=>$xml->autoadd,'endtimes'=>$xml->endtimes,"mshop"=>$xml->mshop,"timeperiod"=>$xml->timeperiod));
        exit;
    }

    /* 网络操作函数 */
    public function g_triggerRequest($url,$io=false,$post_data = array(), $cookie = array()){
        $method = empty($post_data) ? 'GET' : 'POST';
        $url_array = parse_url($url);
        $port = isset($url_array['port'])? $url_array['port'] : 80;
        if(function_exists('fsockopen')){
            $fp = @fsockopen($url_array['host'], $port, $errno, $errstr, 30);
        }elseif(function_exists('pfsockopen')){
            $fp = @pfsockopen($url_array['host'], $port, $errno, $errstr, 30);
        }elseif(function_exists('stream_socket_client')){
            $fp = @stream_socket_client($url_array['host'].':'.$port,$errno,$errstr,30);
        } else {
            $fp = false;
        }
        if(!$fp){
            return false;
        }
        $getPath = $url_array['path'] ."?". $url_array['query'];

        $header  = $method . " " . $getPath." ";
        $header .= "HTTP/1.1\r\n";
        $header .= "Host: ".$url_array['host']."\r\n"; #HTTP 1.1 Host域不能省略
        $header .= "Pragma: no-cache\r\n";

        /*
			//以下头信息域可以省略
			$header .= "User-Agent: Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13 \r\n";
			$header .= "Accept: text/xml,application/xml,application/xhtml+xml,text/html;q=0.9,text/plain;q=0.8,image/png,q=0.5 \r\n";
			$header .= "Accept-Language: en-us,en;q=0.5 ";
			$header .= "Accept-Encoding: gzip,deflate\r\n";
        */
        if(!empty($cookie)){
            $_cookie_s = strval(NULL);
            foreach($cookie as $k => $v){
                $_cookie_s .= $k."=".$v."; ";
            }
            $_cookie_s = rtrim($_cookie_s,"; ");
            $cookie_str =  "Cookie: " . base64_encode($_cookie_s) ." \r\n";	#传递Cookie
            $header .= $cookie_str;
        }
        $post_str = '';
        if(!empty($post_data)){
            $_post = strval(NULL);
            foreach($post_data as $k => $v){
                $_post .= $k."=".urlencode($v)."&";
            }
            $_post = rtrim($_post,"&");
            $header .= "Content-Type: application/x-www-form-urlencoded\r\n";#POST数据
            $header .= "Content-Length: ". strlen($_post) ." \r\n";#POST数据的长度

            $post_str = $_post."\r\n"; #传递POST数据
        }
        $header .= "Connection: Close\r\n\r\n";
        $header .= $post_str;

        fwrite($fp,$header);
        if($io){
            while (!feof($fp)){
                echo fgets($fp,1024);
            }
        }
        fclose($fp);
        #echo $header;
        return true;
    }

}
?>