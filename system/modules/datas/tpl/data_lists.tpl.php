<?php defined('G_IN_ADMIN')or exit('No permission resources.'); ?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>渠道名称 - <?php echo $list['s_name']?></title>
<link rel="stylesheet" href="<?php echo G_GLOBAL_STYLE; ?>/global/css/global.css" type="text/css">
<link rel="stylesheet" href="<?php echo G_GLOBAL_STYLE; ?>/global/css/style.css" type="text/css">
<link rel="stylesheet" href="<?php echo G_PLUGIN_PATH; ?>/calendar_2/jquery.datetimepicker.css" type="text/css"> 
<script type="text/javascript" charset="utf-8" src="<?php echo G_PLUGIN_PATH; ?>/calendar_2/jquery.js"></script>
<script type="text/javascript" charset="utf-8" src="<?php echo G_PLUGIN_PATH; ?>/calendar_2/build/jquery.datetimepicker.full.js"></script>


<style>
tbody tr{ line-height:30px; height:30px;} 
table tr th{font-weight:bold !important;}
table tr td{text-align:center;}
.param{background:#c5e8f1;width:100%;height:35px;line-height:32px;text-indent:10px;}
</style>
</head>
<body>

<div class="bk10"></div>
<div class="table-list lr10">

<div class="param">

跟踪时间：
	<form action="" style="display:inline" method="post">
		<input name="start_date" type="text" id="posttime1" class="input-text posttime"  readonly="readonly" value="<?php if(!empty($start_date)){echo date("Y-m-d H:i:s",$start_date);}else{echo "";} ?>"/> -  
 		<input name="end_date" type="text" id="posttime2" class="input-text posttime"  readonly="readonly" value="<?php if(!empty($end_date)){echo date("Y-m-d H:i:s",$end_date);}else{echo "";} ?>"/>
		<input type="submit" value="查询"/>
		<input type="button" id="reset_button" value="清空"/>
	</form>

</div>

<!--start-->
  <table width="100%" cellspacing="0">
    <thead>
		<tr>
		  <th width="60px">编号</th>
		  <th>渠道名称</th>
		  <th width="140">当日注册人数</th>
		  <th width="140">投资总人数</th>
		  <th>投资总额</th>
		  <th>当日投资总人数</th>
		  <th>当日投资总额</th>
		  <th>首次投资人数</th>
		  <th>首次投资总额</th>
		  <th>首次充值人数</th>
		  <th>首次充值总额</th>
		  <th>充值总额</th>
		  <th>查看详情</th>
		  <th>操作</th>
		</tr>
    </thead>
    <tbody>
        <tr>
			<td><?php echo $list['s_id'];?></td>
			<td><?php echo $list['s_name'];?></td>
			<td><?php echo $list['reg_total'];?></td>
			<td><?php echo $list['invest_total_people'];?></td>
			 <td><?php echo $list['invest_total_num']?></td>
			<td><?php echo $list['now_register_invest_people'];?></td>
			<td><?php echo $list['now_register_invest_num']?></td>
			<td><?php echo $list['frist_invest_total_people'];?></td>
			<td><?php echo $list['frist_invest_total_num'];?></td>
			<td><?php echo $list['first_chong_zong_people'];?></td>
			<td><?php echo $list['first_chong_zong_num'];?></td>
			<td><?php echo $list['chong_zong_num'];?></td>
			<td><a href='<?php echo $list['this_path']?>' >查看详情</a></td>
			<td><a href='<?php echo $list['this_path_2']?>' >导出到excel</a></td>
		</tr>
  	</tbody>
</table>
</div><!--table-list end-->

<script>
$.datetimepicker.setLocale('zh');
var logic = function( currentDateTime ){
	if (currentDateTime && currentDateTime.getDay() == 6){
		this.setOptions({
			minTime:'11:00'
		});
	}else
		this.setOptions({
			minTime:'8:00'
		});
};
$('.posttime').datetimepicker({
	onChangeDateTime:logic,
	onShow:logic
});

$("#reset_button").click(function(){
	$(".posttime").val('');
	
});

</script>
</body>
</html> 