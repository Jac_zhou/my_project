<?php defined('G_IN_ADMIN')or exit('No permission resources.'); ?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<link rel="stylesheet" href="<?php echo G_GLOBAL_STYLE; ?>/global/css/global.css" type="text/css">
<link rel="stylesheet" href="<?php echo G_GLOBAL_STYLE; ?>/global/css/style.css" type="text/css">
<style>
tbody tr{ line-height:30px; height:30px;} 
</style>
</head>
<body>
<div class="header lr10">
	<?php echo $this->headerment();?>
</div>
<div class="bk10"></div>
<div class="table-list lr10">
<!--start-->
  <table width="100%" cellspacing="0">
    <thead>
		<tr>
		<th width="80px">id</th>
		<th width="" align="center">渠道名称</th>
		<th width="" align="center">渠道英文名称</th>
		<th width="" align="center">推广关键词</th>
		<th width="" align="center">注册推广参数</th>
		<th width="30%" align="center">操作</th>
		</tr>
    </thead>
    <tbody>
		<?php foreach($arr_list as $v){ ?>
		<?php 
		  $s_words = explode(',',$v['s_words']);
		?>
		<tr>
		    <td align="center"><?php echo $v['s_id']; ?></td>
			<td align="center"><?php echo $v['s_name']; ?></td>
			<td align="center"><?php echo $v['s_ename'];?></td>
			<td align="center"><?php 
			foreach($s_words as $key => $val){
			     echo str_replace('|','&nbsp;&nbsp;|&nbsp;&nbsp;',$val) . '<br/>';
			}
			?></td>
			<td align="center"><?php echo $v['s_reglink'];?></td>
			<td align="center">
			     <a href="<?php echo G_WEB_PATH;?>/index.php/datas/datas_admin/update_source?id=<?php echo $v['s_id']; ?>">修改</a> | 
				<a  onclick="return confirm('是否真的删除！');" href="<?php echo G_WEB_PATH;?>/index.php/datas/datas_admin/del_source?id=<?php echo $v['s_id']; ?>" >删除</a>
			</td>	
		</tr>
		<?php } ?>
  	</tbody>
</table>
</div><!--table-list end-->
<div id="pages" style="margin:10px 10px">		
	<ul><li>共 <?php echo $total; ?> 条</li><?php echo $page->show('one','li'); ?></ul>
</div>
<script>
</script>
</body>
</html> 