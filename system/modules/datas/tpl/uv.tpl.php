<?php defined('G_IN_ADMIN')or exit('No permission resources.'); ?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<link rel="stylesheet" href="<?php echo G_GLOBAL_STYLE; ?>/global/css/global.css" type="text/css">
<link rel="stylesheet" href="<?php echo G_GLOBAL_STYLE; ?>/global/css/style.css" type="text/css">
<style>
tbody tr{ line-height:30px; height:30px;} 
#uv_box tr th{border:1px solid #d5dfe8;}
#uv_box tr td{border:1px solid #d5dfe8;}
</style>
</head>
<body>
<div class="header lr10">
	<?php echo $this->headerment();?>
</div>
<div class="bk10"></div>
<!--start-->
<div class="table-list lr10">

<?php $rows = json_decode($data,true);?>
<table width="100%" cellspacing="0" id="uv_box">
<tr>
<th style="width:100px;">渠道</th>
<?php foreach($rows['categories'] as $k => $v){?>
    <th style="width:50px;"><?php echo $v;?></th>
<?php }?>
</tr>

<?php foreach($rows['series'] as $k => $v){?>
<tr>
<td style="width:100px;text-align:center;"><?php echo $v['name']?></td>
<?php foreach($v['data'] as $k1 => $v1){?>
<td align="center"><?php echo $v1?></td>
<?php }?>
</tr>
<?php }?>
</table>
</div>
<!--table-list end-->

</body>
</html> 