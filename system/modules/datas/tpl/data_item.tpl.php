<?php defined('G_IN_ADMIN')or exit('No permission resources.'); ?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<link rel="stylesheet" href="<?php echo G_GLOBAL_STYLE; ?>/global/css/global.css" type="text/css">
<link rel="stylesheet" href="<?php echo G_GLOBAL_STYLE; ?>/global/css/style.css" type="text/css">

<style>
tbody tr{ line-height:30px; height:30px;} 
table tr th{font-weight:bold !important;}
table tr td{text-align:center;}
.param{background:#c5e8f1;width:100%;height:35px;line-height:32px;text-indent:10px;}
</style>
</head>
<body>

<div class="bk10"></div>
<div class="table-list lr10">

<!--start-->
  <table width="100%" cellspacing="0">
    <thead>
		<tr>
		  <th width="10%">编号</th>
		  <th width="20%">渠道名称</th>
		  <th width="20%">外部自来源</th>
		  <th width="10%">注册用户</th>
		  <th width="10%">投资金额</th>
		  <th width="10%">充值总额</th>
		</tr>
    </thead>
    <tbody>
    <?php if(count($arr_list)){?>
        <?php foreach($arr_list as $k => $v){?>
            <tr>
				<td><?php echo $v['sn'];?></td>
                <td><?php echo $v['name'];?></td>
                <td><?php echo $v['words'];?></td>
                <td><?php echo $v['register_num_total'];?></td>
    		    <td><?php echo $v['invest_money_total'];?></td>
				 <td><?php echo $v['chong_money_total']?></td>
            </tr>
        <?php }?>
     <?php }else{ ?>
     
        <tr><td colspan="6">暂无数据</td></tr>
     <?php }?>
  	</tbody>
</table>
</div><!--table-list end-->

<script>
$.datetimepicker.setLocale('zh');
var logic = function( currentDateTime ){
	if (currentDateTime && currentDateTime.getDay() == 6){
		this.setOptions({
			minTime:'11:00'
		});
	}else
		this.setOptions({
			minTime:'8:00'
		});
};
$('.posttime').datetimepicker({
	onChangeDateTime:logic,
	onShow:logic
});

$("#reset_button").click(function(){
	$(".posttime").val('');
	
});
</script>
</body>
</html> 