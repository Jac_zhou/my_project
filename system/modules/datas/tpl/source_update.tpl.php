<?php defined('G_IN_ADMIN')or exit('No permission resources.'); ?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<link rel="stylesheet" href="<?php echo G_GLOBAL_STYLE; ?>/global/css/global.css" type="text/css">
<link rel="stylesheet" href="<?php echo G_GLOBAL_STYLE; ?>/global/css/style.css" type="text/css">
<script type="text/javascript" src="<?php echo G_GLOBAL_STYLE; ?>/global/js/jquery-1.8.3.min.js""></script>

<style>
table th{ border-bottom:1px solid #eee; font-size:12px; font-weight:100; text-align:right; width:200px;}
table td{ padding-left:10px;}
input.button{ display:inline-block}
</style>
</head>
<body>
<div class="header lr10">
	<?php echo $this->headerment();?>
</div>
<div class="bk10"></div>
<div class="table_form lr10">
<!--start-->
<form name="myform" action="" method="post" enctype="multipart/form-data">
<table width="100%" class="lr10">
  	 
       <tr>
			<td width="120" align="right">渠道中文名称：</td>
			<td><input type="text" name="s_name" value="<?php echo $source['s_name'] ?>"  class="input-text"><font color="red">*必选*</font></td>
		</tr>
		<tr>
			<td width="120" align="right">渠道英文名称：</td>
			<td><input type="text" name="s_ename" value="<?php echo $source['s_ename'] ?>"  class="input-text"><font color="red">*必选*</font></td>
		</tr>
		<tr>
			<td width="120" align="right">注册推广参数：</td>
			<td><input type="text" size="50" name="s_reglink" value="<?php echo $source['s_reglink'] ?>"  class="input-text">(如：?su=baidu&sk=zl-bd-001)</td>
		</tr>
		<?php
		  $s_words = $source['s_words'];
		  $s_words = explode(',',trim($s_words,','));
		  foreach($s_words as $key => $val){
		      $words = explode('|',$val);
		      $sw = isset($words[0]) ? trim($words[0]) : '';
		      $sew = isset($words[1]) ? trim($words[1]) : '';
		?>
		<tr class="add_keywords">
			<td width="120" align="right">关键字添加：</td>
			<td>
				<input type="text" name="s_words[]" value="<?php echo $sw;?>"  class="input-text" value="关键字添加">
				<input type="text" name="s_ewords[]" value="<?php echo $sew;?>"  class="input-text" value=""><font color="red">*必选*</font>
				<a id="add_keywords" href="#" style="padding:5px 8px;background:gray;color:#fff;text-align:center">+</a>
			</td>
		</tr>
		<?php 
		  }
		?>
		<tr>
        	<td width="120" align="right"><input type="hidden" name="s_id" value="<?php echo $source['s_id']?>"/></td>
            <td><input type="submit" class="button" name="submit"  value=" 提交 " ></td>
		</tr>
</table>
</form>
</div><!--table-list end-->
<script>
function upImage(){
	return document.getElementById('imgfield').click();
}
$("#add_keywords").click(function(){
	var _html = '<tr class="new_keywords">'
		_html += '<td width="120" align="right">关键字添加：</td>';		
		_html += '<td>';
		_html += '<input type="text" name="s_words[]"  class="input-text" value="关键字添加">';			
		_html +='<input type="text" name="s_ewords[]"  class="input-text" value="1001">';
		_html += '<a class="del_keywords" href="#" style="padding:5px 8px;background:gray;color:#fff;text-align:center">-</a>';
		_html += '</td>';
		_html += '</tr>';
	$(".add_keywords").after(_html);
	
});

$(".del_keywords").die('click').live('click',function(){
	$(this).parents(".new_keywords").remove();
	
});

</script>
</body>
</html> 