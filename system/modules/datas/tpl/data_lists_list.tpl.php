<?php defined('G_IN_ADMIN')or exit('No permission resources.'); ?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<link rel="stylesheet" href="<?php echo G_GLOBAL_STYLE; ?>/global/css/global.css" type="text/css">
<link rel="stylesheet" href="<?php echo G_GLOBAL_STYLE; ?>/global/css/style.css" type="text/css">
<link rel="stylesheet" href="<?php echo G_PLUGIN_PATH; ?>/calendar_2/jquery.datetimepicker.css" type="text/css"> 
<script type="text/javascript" charset="utf-8" src="<?php echo G_PLUGIN_PATH; ?>/calendar_2/jquery.js"></script>
<script type="text/javascript" charset="utf-8" src="<?php echo G_PLUGIN_PATH; ?>/calendar_2/build/jquery.datetimepicker.full.js"></script>


<style>
tbody tr{ line-height:30px; height:30px;} 
table tr th{font-weight:bold !important;}
table tr td{text-align:center;}
.param{background:#c5e8f1;width:100%;height:35px;line-height:32px;text-indent:10px;}
#tlist tbody tr:hover{background:#dfdfdf !important;}
</style>
</head>
<body>
<div class="header lr10">
	<?php echo $this->headerment();?>
</div>
<div class="bk10"></div>
<div class="table-list lr10">

<!--start-->
  <table width="100%" cellspacing="0" id="tlist">
    <thead>
		<tr>
		  <th width="60px">编号</th>
		  <th>渠道名称</th>
		  <th>查看详情</th>
		</tr>
    </thead>
    <tbody>
        <?php foreach($list as $k => $v){?>
            <tr>
                <td><?php echo $v['s_id'];?></td>
                <td><?php echo $v['s_name'];?></td>
    		    <td><a href='<?php echo $this_path.'/'.$v['s_id']?>' target='_blank'>查看详情</a></td>
            </tr>
        <?php }?>
  	</tbody>
</table>
</div><!--table-list end-->
</body>
</html> 