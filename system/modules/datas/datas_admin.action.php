<?php
defined('G_IN_SYSTEM')or exit('No permission resources.');
System::load_app_class('admin',G_ADMIN_DIR,'no');
class datas_admin extends admin {
	private $db;
	public function __construct(){
		parent::__construct();
		$this->db=System::load_sys_class("model");
		$this->ment=array(
						array("source_lists","渠道管理",ROUTE_M.'/'.ROUTE_C."/source_lists"),
						array("add_source","添加渠道",ROUTE_M.'/'.ROUTE_C."/add_source"),
		                array("data_lists","数据统计",ROUTE_M.'/'.ROUTE_C."/data_lists"),
                	    array("show_pv","查看pv",ROUTE_M.'/'.ROUTE_C."/show_pv"),
                	    array("show_uv","查看uv",ROUTE_M.'/'.ROUTE_C."/show_uv"),
		);	
	}
	
	//pv
	public function show_pv()
	{
	    
	    $data = $this->get_pv_data();
	    $data = json_encode($data);
	    include $this->tpl(ROUTE_M,'pv');
	}
	
	//uv
	public function show_uv()
	{
	    $data = $this->get_uv_data();
	    $data = json_encode($data);
	    include $this->tpl(ROUTE_M,'uv');
	}
	
	
	/*渠道列表*/
	public function source_lists(){
		$is_active = $this->segment(4);
		$is_active = $is_active ? trim($is_active) : 0 ;
		$is_use = $this->segment(5);
		$is_use = $is_use ? trim($is_use) : 0 ;
		
		$num=20;
		$total=$this->db->GetCount("SELECT COUNT(*) FROM `@#_source_list`"); 
		$page=System::load_sys_class('page');
		if(isset($_GET['p'])){$pagenum=$_GET['p'];}else{$pagenum=1;}	
		$page->config($total,$num,$pagenum,"0");
		
		$sql = "SELECT * FROM `@#_source_list` WHERE 1";
		$arr_list=$this->db->GetPage($sql,array("num"=>$num,"page"=>$pagenum,"type"=>1,"cache"=>0));
		
		include $this->tpl(ROUTE_M,'source_lists');
	
	}
	
	/*添加渠道*/
	public function add_source(){
		
		if(isset($_POST['submit'])){
			
			$s_name = isset($_POST['s_name']) ? trim($_POST['s_name']) : '';
			if($s_name == ''){
				_message('渠道名称不能为为空');
			}
			$s_ename = isset($_POST['s_ename']) ? trim($_POST['s_ename']) : '';
			if($s_ename == ''){
				_message('渠道名称不能为为空');
			}
			
			$time = time();
			
			$s_words = isset($_POST['s_words']) ? $_POST['s_words'] : '';
			if($s_words == ''){
				_message('关键字不能为为空');
			}
			
			$s_ewords = isset($_POST['s_ewords']) ? $_POST['s_ewords'] : '';
			$s_reglink = isset($_POST['s_reglink']) ? $_POST['s_reglink'] : '';
			if($s_words == ''){
				_message('注册推广链接不能为空');
			}
			foreach($s_words as $key=>$val){
				$key_wrods .= $val."|".$s_ewords[$key].",";
			}
			$sql = "INSERT INTO `@#_source_list`(s_name,s_ename,s_words,s_reglink) VALUES('$s_name','$s_ename','$key_wrods','$s_reglink')";
			$this->db->Query($sql);
			if($this->db->affected_rows()){
				_message("添加成功",WEB_PATH.'/'.ROUTE_M.'/'.ROUTE_C."/source_lists");
				
			}else{
				_message("添加失败");
			}
			
		}
		include $this->tpl(ROUTE_M,'source_add');
	}
	
	//删除渠道
	public function del_source()
	{
	    $id = isset($_GET['id']) ? intval($_GET['id']) : 0;
	    if( ! $id){
	        _message("参数错误");
	    }
	    $query = $this->db->Query("DELETE FROM @#_source_list WHERE s_id = $id LIMIT 1");
	    if($query){
	        _message("删除成功",WEB_PATH.'/'.ROUTE_M.'/'.ROUTE_C."/source_lists");
	    }else{
	        _message("删除失败");
	    }
	}
	
	//修改渠道
	public function update_source()
	{
	    $id = isset($_GET['id']) ? intval($_GET['id']) : 0;
	    if(isset($_POST['submit'])){
	        $s_id = isset($_POST['s_id']) ? trim($_POST['s_id']) : '';
	        
	        if(!$s_id) _message("参数错误");
	        
	        $s_name = isset($_POST['s_name']) ? trim($_POST['s_name']) : '';
	        if($s_name == ''){
	            _message('渠道名称不能为为空');
	        }
	        $s_ename = isset($_POST['s_ename']) ? trim($_POST['s_ename']) : '';
	        if($s_ename == ''){
	            _message('渠道名称不能为为空');
	        }
	        	
	        $time = time();
	        	
	        $s_words = isset($_POST['s_words']) ? $_POST['s_words'] : '';
	        if($s_words == ''){
	            _message('关键字不能为为空');
	        }
	        	
	        $s_ewords = isset($_POST['s_ewords']) ? $_POST['s_ewords'] : '';
	        $s_reglink = isset($_POST['s_reglink']) ? $_POST['s_reglink'] : '';
	        if($s_words == ''){
	            _message('注册推广链接不能为空');
	        }
	        foreach($s_words as $key=>$val){
	            $key_wrods .= $val."|".$s_ewords[$key].",";
	        }
	        $sql = "UPDATE `@#_source_list` SET `s_name`='$s_name',`s_ename`='$s_ename',`s_words`='$key_wrods',`s_reglink`='$s_reglink' WHERE `s_id`='$s_id'";
	        $this->db->Query($sql);
	        if($this->db->affected_rows()){
	            _message("修改成功",WEB_PATH.'/'.ROUTE_M.'/'.ROUTE_C."/source_lists");
	    
	        }else{
	            _message("修改失败");
	        }
	        	
	    }else{
	        if($id){
	            $source = $this->db->GetOne("SELECT * FROM @#_source_list WHERE `s_id`=$id LIMIT 1");
	        }else{
	            _message("参数错误");
	        } 
	    }
	    
	    include $this->tpl(ROUTE_M,'source_update');
	}
	
	public function data_list_list(){	
		  $list = $this->db->GetList("SELECT s_id,s_name,s_ename FROM @#_source_list");
		  $this_path = WEB_PATH."/datas/datas_admin/data_lists";
		  include $this->tpl(ROUTE_M,'data_lists_list');
	}
	//数据统计
	public function data_lists()
	{
		
	    $wh = '';
	    $start_date = (isset($_REQUEST['start_date']) && !empty($_REQUEST['start_date'])) ? strtotime(trim($_REQUEST['start_date'])) : strtotime(date('Y-m-d 00:00:00'));
	    $end_date = (isset($_REQUEST['end_date']) && !empty($_REQUEST['end_date'])) ? strtotime(trim($_REQUEST['end_date'])) : strtotime(date('Y-m-d 23:59:59'));;
		if($start_date > $end_date){
			_message('开始时间不能大于结束时间');
		}
	    
		$s_id = $this->segment(4);
		$list = $this->db->GetOne("SELECT s_id,s_name,s_ename FROM @#_source_list where s_id = $s_id");
		
	    //为了方便详情的路径
		$list['this_path'] = G_WEB_PATH."/index.php/".ROUTE_M."/".ROUTE_C."/".data_item."/".$s_id."?start_date=".date("Y-m-d H:i:s",$start_date)."&end_date=".date("Y-m-d H:i:s",$end_date);
		
		//导出到excel
		$list['this_path_2'] = G_WEB_PATH."/index.php/".ROUTE_M."/".ROUTE_C."/".get_excel."/".$s_id."?start_date=".date("Y-m-d H:i:s",$start_date)."&end_date=".date("Y-m-d H:i:s",$end_date);
		
		//获取指定渠道指定日期的注册总人数
		$list['reg_total'] = $this->get_reg_total($s_id, $start_date, $end_date);
		
		//获取指定渠道指定日期的投资人数
		$list['invest_total'] = $this->get_invest_total($s_id, $start_date, $end_date);

		//指定渠道指定时间投资总人数
		$arr_invest_count = $this->get_zong($s_id,$start_date,$end_date);
		$list['invest_total_people'] = $arr_invest_count['people'];
		
		//指定渠道指定时间投资总额
		$arr_invest_count2 = $this->get_zong($s_id,$start_date,$end_date,'money');
		$list['invest_total_num'] = $arr_invest_count2['money'];
		
		//当日投资额(当日注册且当日投资的)
		$now_register_arr = $this->get_current_invest($s_id,$start_date,$end_date,'money');
		$list['now_register_invest_num'] = $now_register_arr['money'];
		
		//当日投资总人数（当日注册且当日投资的）
		$now_register_arr_2 = $this->get_current_invest($s_id,$start_date,$end_date);
		$list['now_register_invest_people'] = $now_register_arr_2['people'];
		
		//首次投资总额
		$frist_arr = $this->get_frist_invest_total_num($s_id,$start_date,$end_date,'money');
		$list['frist_invest_total_num'] = $frist_arr['money'];
		
		//首次投资总人数
		$frist_arr_p = $this->get_frist_invest_total_num($s_id,$start_date,$end_date);
		$list['frist_invest_total_people'] = $frist_arr_p['people'];
		//充值总额
		$list['chong_zong_num'] = $this->get_chongzhi_total($s_id,$start_date,$end_date,'money');
		
		//首次充值总额
		$frist_chong_arr =  $this->get_frist_chong_total_num($s_id,$start_date,$end_date,'money');
		$list['first_chong_zong_num'] = $frist_chong_arr['money'];
		
		//首次充值总人数
		$frist_chong_arr =  $this->get_frist_chong_total_num($s_id,$start_date,$end_date);
		$list['first_chong_zong_people'] = $frist_chong_arr['people'];
	    
	    include $this->tpl(ROUTE_M,'data_lists');
	}
	
	//首次充值总额(人数)
	private function get_frist_chong_total_num($s_id,$start_date,$end_date,$field){
		$con = $this->get_where($start_date,$end_date,'addtime');
		$con_r = $this->get_where($start_date,$end_date,'time');
		$sql = "SELECT uid FROM `@#_data_statistics` WHERE s_id='$s_id'";
		$field = $field == '' ? '' : 'money';
		$frist_arr = array();
		$frist_arr['money'] = 0;
		$frist_arr['people']=0;
		$k = 0;
		$arr_list = $this->db->GetList($sql);
		if($arr_list){
			for($i=0;$i<count($arr_list);$i++){
				$one = $arr_list[$i]['uid'];
				if($one){
					$sqlx = "SELECT money FROM `@#_member_addmoney_record` WHERE uid='$one' AND `status` LIKE '%已付款%' $con_r ORDER BY time ASC LIMIT 1";
					$arr_one = $this->db->GetOne($sqlx);
					if($arr_one){
						if($field == "money"){
							$frist_arr['money'] = $frist_arr['money'] + $arr_one['money'];
						}else{
							$k++;
							$frist_arr['people'] = $k;
						}
					}
				}
			}
			
			
		}
		return $frist_arr;
	}
	
	//充值总额
	private function get_chongzhi_total($s_id,$start_date,$end_date,$field,$s_words=''){
		$con = $this->get_where($start_date,$end_date,'addtime');
		$con_r = $this->get_where($start_date,$end_date,'time');
		if($s_words != ''){
			$_where = " AND s_words='$s_words' ";
		}else{
			$_where = '';
		}
		$sql = "SELECT uid FROM `@#_data_statistics` WHERE s_id='$s_id' $_where";
		$field = $field == '' ? '' : 'money';
		$zong_chong = 0;
		$arr_list = $this->db->GetList($sql);
		if($arr_list){
			for($i=0;$i<count($arr_list);$i++){
				$one = $arr_list[$i]['uid'];
				
				if($one && $field == 'money'){
					$sqlx = "SELECT SUM(money) AS total FROM `@#_member_addmoney_record` WHERE uid='$one' AND `status` LIKE '%已付款%' AND code!='' $con_r";
					$arr_one = $this->db->GetOne($sqlx);
					$zong_chong = $zong_chong + $arr_one['total'];
				}else{
					$sqlx = "SELECT id FROM `@#_member_addmoney_record` WHERE uid='$one' AND `status` LIKE '%已付款%' AND code!='' $con_r GROUP BY uid";
					$arr_one = $this->db->GetOne($sqlx);
					$zong_chong = count($arr_one);
				}
			}
			
		}
		return $zong_chong;
	}
	//首次投资总额(人数)
	private function get_frist_invest_total_num($s_id,$start_date,$end_date,$field){
		$con = $this->get_where($start_date,$end_date,'addtime');
		$con_r = $this->get_where($start_date,$end_date,'time');
		$sql = "SELECT uid FROM `@#_data_statistics` WHERE s_id='$s_id'";
		//echo $sql;
		$field = $field == '' ? '' : 'money';
		$frist_arr = array();
		$frist_arr['money'] = 0;
		$frist_arr['people']=0;
		$k = 0;
		$arr_list = $this->db->GetList($sql);
		if($arr_list){
			for($i=0;$i<count($arr_list);$i++){
				$one = $arr_list[$i]['uid'];
				if($one){
					$sqlx = "SELECT moneycount FROM `@#_member_go_record` WHERE uid='$one' AND `status` LIKE '%已付款%' $con_r ORDER BY time ASC LIMIT 1";
					$arr_one = $this->db->GetOne($sqlx);
					if($arr_one){
						if($field == "money"){
							$frist_arr['money'] = $frist_arr['money'] + $arr_one['moneycount'];
						}else{
							$k++;
							$frist_arr['people'] = $k;
						}
					}
				}
			}

		}
		return $frist_arr;
	}
	
	//当日投资总额(当日注册且当日投资的)
	private function get_current_invest($s_id,$start_date,$end_date,$field){
		$con = $this->get_where($start_date,$end_date,'addtime');
		$sql = "SELECT uid,addtime FROM `@#_data_statistics` WHERE s_id='$s_id' $con";
		$field = $field == '' ? '' : 'money';
		$now_count_arr = array();
		$now_count_arr['money'] = 0;
		$now_count_arr['people'] = 0;
		$k = 0;
		$arr_list = $this->db->GetList($sql);
		if($arr_list){
			for($i=0;$i<count($arr_list);$i++){
				
				$one = $arr_list[$i]['uid'];
				$current_time = date("Y-m-d",$arr_list[$i]['addtime']);
				$current_time_c = strtotime($current_time."00:00:01");
				$current_time_end = strtotime($current_time."23:59:59");
				
				$con_2 = " AND `time`>= '$current_time_c' AND `time` <='$current_time_end' ";

				if($one){
					if($field == 'money'){
						$sqlx = "SELECT SUM(moneycount)  AS total_money FROM `@#_member_go_record` WHERE uid='$one' AND `status` LIKE '%已付款%' $con_2";
						//echo $sqlx;
						$arr_one = $this->db->GetOne($sqlx);
						$now_count_arr['money'] = $now_count_arr['money'] + $arr_one['total_money'];
					}else{
						$sqlr = "SELECT id FROM `@#_member_go_record` WHERE uid='$one' AND `status` LIKE '%已付款%' $con_2 LIMIT 1";
						$arr_one_people = $this->db->GetOne($sqlr);
						if($arr_one_people){
							$k++;
							$now_count_arr['people'] = $k;
						}
					}
				}
				
			}
			
		}
		return $now_count_arr;
	}

	
	//投资总人数 (总额)
	private function get_zong($s_id,$start_date,$end_date,$field,$s_words='')
	{
		$con = $this->get_where($start_date,$end_date,'addtime');
		$con_r = $this->get_where($start_date,$end_date,'time');
		if($s_words !=''){
			$_where = " AND s_words='$s_words'";
		}else{
			$_where = '';
		}
		$sql = "SELECT uid FROM `@#_data_statistics` WHERE s_id='$s_id' $_where";
		$field = $field == '' ? '' : 'money';
		$now_zong_arr = array();
		$now_zong_arr['money'] = 0;
		$now_zong_arr['people'] = 0;
		$k = 0;
		$arr_list = $this->db->GetList($sql);
		
		if(count($arr_list)){
			for($i=0;$i<count($arr_list);$i++){
				$one = $arr_list[$i]['uid'];
				if($one){
					if($field == 'money'){
						$sqlx = "SELECT SUM(moneycount) AS total_cont FROM `@#_member_go_record` WHERE uid='$one' AND `status` LIKE '%已付款%' $con_r";
						$arr_one = $this->db->GetOne($sqlx);
						$now_zong_arr['money'] = $now_zong_arr['money']+$arr_one['total_cont'];
					}else{
						$sql_f = "SELECT id FROM `@#_member_go_record` WHERE uid='$one' AND `status` LIKE '%已付款%' $con_r LIMIT 1";
						$arr_one_people = $this->db->GetOne($sql_f);
						if($arr_one_people){
							$k++;
							$now_zong_arr['people'] = $k;
						}
						
					}
				}
			}
			
		}
		return $now_zong_arr;
	}
	
	
	//获取指定渠道指定日期的注册总人数
	private function get_reg_total($s_id='', $start='', $end='',$s_word='')
	{
	    $wh = $this->get_where($start, $end, 'addtime');
		if($s_word != ''){
			$con = " AND `s_words` = '$s_word' ";
		}
		$sql = "SELECT id FROM @#_data_statistics WHERE `s_id`='$s_id' $wh $con ";
		//echo $sql;
	    $info = $this->db->GetCount($sql);
	    return $info;
	}
	
	//获取指定渠道指定日期的投资人数
	private function get_invest_total($s_id='', $start='', $end='', $field = 'count(id)')
	{
	    $wh = $this->get_where($start, $end, 'addtime');
	    $uwh = $this->get_where($start, $end, 'time');
	    $sql = "SELECT $field as total FROM @#_member_go_record WHERE " .
	          " `uid` in(SELECT uid FROM @#_data_statistics WHERE `s_id`='$s_id' $wh) $uwh";
	    
	    $info = $this->db->GetOne($sql);
	    return $info['total'] ? : 0;
	}
	
	//获取首次充值人数及总额
	private function get_first_addmoney($s_id, $start, $end)
	{
	    $wh = $this->get_where($start, $end, 'addtime');
	    $sql = "SELECT * FROM @#_member_addmoney_record where uid in(SELECT uid FROM @#_data_statistics WHERE `s_id`='$s_id' $wh) group by uid  ORDER BY `time` asc";
	    $rows = $this->db->GetList($sql);
	    $num = 0;
	    $money = 0;
	    if(count($rows)){
	        foreach($rows as $k => $v){
	            $bool = $this->check_bool($start,$end,$v['time']);
	            if($bool){
	                $num++;
	                $money += $v['money'];
	            }
	        }
	    }
	    $money = $money ? sprintf('%.2f',$money) : $money;
	    return array('num'=>$num,'money'=>$money);
	}
	
	//指定渠道指定范围充值总额
	private function get_addmoney_total($s_id,$start,$end)
	{
	    $wh = $this->get_where($start, $end, 'time');
	    $info = $this->db->GetOne("SELECT sum(money) as total FROM @#_member_addmoney_record WHERE `status`='已付款' AND `uid` in(SELECT uid FROM @#_data_statistics WHERE `s_id`=$s_id) $wh");
	    return $info['total'];
	}
	
	//验证是否合法
	private function check_bool($start,$end,$tag)
	{
	    $bool = false;
	    if($start && $end){
	        if($tag >= $start && $tag <= $end) $bool = true;
	    }else{
	        if($start && $tag >= $start) $bool = true;
	        if($end && $tag <= $end) $bool =  true;
	    }
	    return $bool;
	}
	
	//组合日期条件
	private function get_where($start,$end,$param)
	{
	    $wh = '';
		/*if(!$start) $wh .= " AND `$param` > $start";
	    if($end) $wh .= " AND `$param` <= $end";
	    if($start && $end){
	        if($start < $end){
	            $wh .= " AND `$param` > $start AND `$param` < $end";
	        }else{
	            $wh .= " AND `$param` > $start";
	        }
	    }*/
		if(!$start){
			$wh = " AND `$param` < '$end'";
		}else{
			 $wh .= " AND `$param` > '$start' AND `$param` < '$end'";
		}
	    return $wh;
	}
	
	//详情
	public function data_item(){
		//$this_path = WEB_PATH."/".ROUTE_M."/".ROUTE_C."/".ROUTE_A;
		
		$s_id = intval($this->segment(4));
		$start_date = isset($_REQUEST['start_date']) ? strtotime($_REQUEST['start_date']) : '';
		$end_date = isset($_REQUEST['end_date']) ? strtotime($_REQUEST['end_date']) : '';
		
		$wh = $this->get_where($start_date, $end_date, 'addtime');
		
		$s_words = array();
		
		$s_words = $this->db->GetList("SELECT s_name,s_words FROM @#_data_statistics WHERE `s_id`='$s_id' $wh GROUP BY `s_words`");
		
		if(count($s_words)){
		    foreach($s_words as $k => $v){
		        $arr_list[$k]['sn'] = $k+1;
		        $arr_list[$k]['name'] = $v['s_name'];
		        $arr_list[$k]['words'] = $v['s_words'];
		
		        //注册总人数
		        $arr_list[$k]['register_num_total'] = $this->get_reg_total($s_id,$start_date,$end_date,trim($v['s_words']));
		        //投资金额
				$arr_money =$this->get_zong($s_id,$start_date,$end_date,'money',$v['s_words']);
		        $arr_list[$k]['invest_money_total'] = $arr_money['money'];
		
		        //充值总额
		        $arr_list[$k]['chong_money_total'] = $this->get_chongzhi_total($s_id,$start_date,$end_date,'money',$v['s_words']);
		        	
		    }
		}
		
		include $this->tpl(ROUTE_M,'data_item');
	}
	
	
	//导出到excel
	public function get_excel(){
		header("Content-type:application/vnd.ms-excel");
		header("Content-Disposition:attachment;filename=渠道子来源统计.xls");
		$tab="\t"; 
		$br="\n";
		$head="渠道来源".$tab."外部子来源".$tab."注册用户".$tab."投资金额".$tab."充值总额".$br;
		
		$s_id = intval($this->segment(4));
		$start_date = isset($_REQUEST['start_date']) ? strtotime($_REQUEST['start_date']) : '';
		$end_date = isset($_REQUEST['end_date']) ? strtotime($_REQUEST['end_date']) : '';
	
		$s_words = array();
		
		$wh = $this->get_where($start_date, $end_date, 'addtime');
		
		$s_words = $this->db->GetList("SELECT s_name,s_words FROM @#_data_statistics WHERE `s_id`='$s_id' $wh GROUP BY `s_words`");
		
		if(count($s_words)){
		    foreach($s_words as $k => $v){
		        $arr_list[$k]['sn'] = $k+1;
		        $arr_list[$k]['name'] = $v['s_name'];
		        $arr_list[$k]['words'] = $v['s_words'];
		
		        //注册总人数
		        $arr_list[$k]['register_num_total'] = $this->get_reg_total($s_id,$start_date,$end_date,trim($v['s_words']));
		        //投资金额
				$arr_one = $this->get_zong($s_id,$start_date,$end_date,'money',$v['s_words']);
		        $arr_list[$k]['invest_money_total'] = $arr_one['money'];
		
		        //充值总额
		        $arr_list[$k]['chong_money_total'] = $this->get_chongzhi_total($s_id,$start_date,$end_date,'money',$v['s_words']);
		        	
		    }
		}
		//输出内容如下： 
		echo iconv("UTF-8","GBK",$head);
		foreach($arr_list AS $key=>$val){
			echo iconv("UTF-8", "GBK", $val['name']).$tab;
			echo iconv("UTF-8", "GBK", $val['words']).$tab;
			echo iconv("UTF-8", "GBK", $val['register_num_total']).$tab;
			echo iconv("UTF-8", "GBK", $val['invest_money_total']).$tab;
			echo iconv("UTF-8", "GBK", $val['chong_money_total']).$tab;
			echo $br;
		}
	}
	
	//统计pv数据
	private function get_pv_data()
	{
	    $su_list = array();
	    $su_list = $this->db->GetList("SELECT s_id,s_name,s_ename FROM @#_source_list ");
	    $su_list[] = array('s_id'=>0,'s_name'=>'其他','s_ename'=>'');
	     
	    $days = cal_days_in_month(CAL_GREGORIAN, date('m'), date('Y'));    //当前月的总天数
	    $to_date = date('d');
	     
	    $color = array('green','red','blue','#ff8502');
	     
	    //日期
	    $date = array();
	    for($i = 1; $i <= $days; $i++){
	        $date[] = $i;
	    }
	     
	    //数据
	    $arr = array();
	    foreach($su_list as $k => $v){
	        for($i = 1; $i <= $days; $i++){
	            $start_time = strtotime(date('Y-m-') . $i ." 00:00:00");
	            $end_time = strtotime(date('y-m-') . $i . " 23:59:59");
	            $pv = $this->db->GetCount("SELECT id FROM @#_pv WHERE `su`='$v[s_ename]' AND `time`> '$start_time' AND `time`<'$end_time' ");
	            $arr[$k]['name'] = $v['s_name'];
	            $arr[$k]['data'][] = ($i > $to_date) ? ' ' : $pv ? : 0;
	            //$arr[$k]['data'][] = $pv ? : rand(0,9);
	            //$arr[$k]['color'] = $color[$k];
	            unset($start_time,$end_time);
	        }
	    }
	     
	    //组合数据
	    $data = array('categories'=>$date, 'series'=>$arr);
	    return $data;
	}
	
	//统计uv数据
	private function get_uv_data()
	{
	    $su_list = array();
	    $su_list = $this->db->GetList("SELECT s_id,s_name,s_ename FROM @#_source_list ");
	    $su_list[] = array('s_id'=>0,'s_name'=>'其他','s_ename'=>'');
	    
	    $days = cal_days_in_month(CAL_GREGORIAN, date('m'), date('Y'));    //当前月的总天数
	    $to_date = date('d');
	    
	    
	    $color = array('green','red','blue','#ff8502');
	    
	    //日期
	    $date = array();
	    for($i = 1; $i <= $days; $i++){
	        $date[] = $i;
	    }
	    
	    //数据
	    $arr = array();
	    foreach($su_list as $k => $v){
	        for($i = 1; $i <= $days; $i++){
	            $start_time = strtotime(date('Y-m-') . $i ." 00:00:00");
	            $end_time = strtotime(date('y-m-') . $i . " 23:59:59");
	            $uv = $this->db->GetCount("SELECT id FROM @#_uv WHERE `su`='$v[s_ename]' AND `time`> '$start_time' AND `time`<'$end_time' ");
	            $arr[$k]['name'] = $v['s_name'];
	            $arr[$k]['data'][] = ($i > $to_date) ? ' ' : $uv ? : 0;
	            //$arr[$k]['data'][] = $uv ? : rand(0,9);
	            //$arr[$k]['color'] = $color[$k];
	            unset($start_time,$end_time);
	        }
	    }
	    
	    //组合数据
	    $data = array('categories'=>$date, 'series'=>$arr);
	    return $data;
	}
}











?>