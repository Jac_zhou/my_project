<?php defined('G_IN_ADMIN')or exit('No permission resources.'); ?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<link rel="stylesheet" href="<?php echo G_GLOBAL_STYLE; ?>/global/css/global.css" type="text/css">
<link rel="stylesheet" href="<?php echo G_GLOBAL_STYLE; ?>/global/css/style.css" type="text/css">
<style>
tbody tr{ line-height:30px; height:30px;} 
</style>
</head>
<body>
<div class="header lr10">
	<?php echo $this->headerment();?>
</div>
<div class="bk10"></div>
<div class="table-list lr10">
<!--start-->
  <table width="100%" cellspacing="0">
    <thead>
		<tr>
		<th width="80px">id</th>
		<th width="200px" align="center">注册号码</th>
		<th width="" align="center">充值金额</th>
		<th width="" align="center">得到红包</th>
		<th width="" align="center">是否兑换</th>
		<th width="30%" align="center">操作</th>
		</tr>
    </thead>
    <tbody>
		<?php foreach($arr_list as $v){ 
			if($v['is_use'] == 1){
				$use_status = '已兑换';
			}else{
				$use_status = '未兑换';
			}
		?>
		
		<tr>
			<td align="center"><?php echo $v['id']; ?></td>
			<td align="center"><?php echo $v['mobile']; ?></td>
			<th width="" align="center"><?php echo $v['money']; ?></th>
			<td align="center"><?php echo $v['cash_value'];?></td>
			<td align="center"><?php echo $use_status;?></td>
			<td align="center">
				<a href="<?php echo WEB_PATH;?>/activelist/activecash_admin/del_data/<?php echo $v['id']; ?>" onClick="return confirm('是否真的删除！');">删除</a>
			</td>	
		</tr>
		<?php } ?>
  	</tbody>
</table>
</div><!--table-list end-->
<div id="pages" style="margin:10px 10px">		
	<ul><li>共 <?php echo $total; ?> 条</li><?php echo $page->show('one','li'); ?></ul>
</div>
<script>
</script>
</body>
</html> 