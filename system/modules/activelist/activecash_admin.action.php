<?php

defined('G_IN_SYSTEM')or exit('No permission resources.');
System::load_app_class('admin',G_ADMIN_DIR,'no');
class activecash_admin extends admin {
	private $db;
	public function __construct(){
		parent::__construct();
		$this->db=System::load_sys_class("model");
		$this->ment=array(
						array("lists","未激活",ROUTE_M.'/'.ROUTE_C."/lists/0"),
						array("appadd","已激活（已兑换）",ROUTE_M.'/'.ROUTE_C."/lists/1"),
		);	
	}
	public function lists(){
		$is_active = $this->segment(4);
		$is_active = $is_active ? trim($is_active) : 0 ;
		
		$num=20;
		$total=$this->db->GetCount("SELECT COUNT(*) FROM `@#_active` WHERE type=1 AND is_active='$is_active'"); 
		$page=System::load_sys_class('page');
		if(isset($_GET['p'])){$pagenum=$_GET['p'];}else{$pagenum=1;}	
		$page->config($total,$num,$pagenum,"0");
		
		$sql = "SELECT * FROM `@#_active` WHERE type=1 AND is_active='$is_active' ORDER BY r_time DESC";
		$arr_list=$this->db->GetPage($sql,array("num"=>$num,"page"=>$pagenum,"type"=>1,"cache"=>0));
		
		include $this->tpl(ROUTE_M,'cash_list');
	}
	
	//热门搜索
	public function hot_words(){
		if(isset($_POST['submit'])){
			$hot_words = isset($_POST['hot_words']) ? htmlspecialchars(trim($_POST['hot_words'])) : '';
			$hot_words = str_replace('，',',',$hot_words);
			$html=<<<HTML
<?php 
	return '{$hot_words}';	//热门搜索 关键词
?>
HTML;
			if(!is_writable(G_CONFIG.'hot_words.inc.php')) _message('Please chmod  email  to 0777 !');
			$ok=file_put_contents(G_CONFIG.'hot_words.inc.php',$html);
			if($ok){
				_message("操作成功");
			}
		}
		
		$words = System::load_sys_config("hot_words");	
		$words = str_replace("，",',',$words);
		include $this->tpl(ROUTE_M,'hot_words');
	}
	
	public function del_data(){
		$del_id = $this->segment(4);
		if($del_id==''){
			_message("该条数据已经不存在！");
		}
		$sql = "DELETE FROM `@#go_active` WHERE id='$del_id'";
		$this->db->Query($sql);
		if($this->db->affected_rows()){
			_message("删除成功",WEB_PATH.'/'.ROUTE_M.'/'.ROUTE_C."/lists/0");
			
		}else{
			_message("删除失败");
		}
	}
	
	
}











?>