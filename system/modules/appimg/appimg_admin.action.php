<?php

defined('G_IN_SYSTEM')or exit('No permission resources.');
System::load_app_class('admin',G_ADMIN_DIR,'no');
class appimg_admin extends admin {
	private $db;
	public function __construct(){
		parent::__construct();
		$this->db=System::load_sys_class("model");
		$this->ment=array(
						array("lists","App图标管理",ROUTE_M.'/'.ROUTE_C."/lists"),
						array("appadd","App图标添加",ROUTE_M.'/'.ROUTE_C."/add_data"),
						array("hot_words","热门搜索",ROUTE_M.'/'.ROUTE_C."/hot_words"),
		);	
	}
	public function lists(){
		$arr=$this->db->Getlist("SELECT * FROM `@#_appimg` WHERE 1 ORDER BY order_num DESC");
		
		include $this->tpl(ROUTE_M,'list');
	}
	
	//热门搜索
	public function hot_words(){
		if(isset($_POST['submit'])){
			$hot_words = isset($_POST['hot_words']) ? htmlspecialchars(trim($_POST['hot_words'])) : '';
			$hot_words = str_replace('，',',',$hot_words);
			$html=<<<HTML
<?php 
	return '{$hot_words}';	//热门搜索 关键词
?>
HTML;
			if(!is_writable(G_CONFIG.'hot_words.inc.php')) _message('Please chmod  email  to 0777 !');
			$ok=file_put_contents(G_CONFIG.'hot_words.inc.php',$html);
			if($ok){
				_message("操作成功");
			}
		}
		
		$words = System::load_sys_config("hot_words");	
		$words = str_replace("，",',',$words);
		include $this->tpl(ROUTE_M,'hot_words');
	}
	
	//添加数据
	public function add_data(){
		if(isset($_POST['submit'])){
			$name 		= isset($_POST['name']) ? htmlspecialchars(trim($_POST['name'])) : '';
			$url  		= isset($_POST['url']) ? htmlspecialchars(trim($_POST['url'])) : '';
			$is_show	= isset($_POST['is_show']) ? intval(trim($_POST['is_show'])) :  1;
			$order_num	= isset($_POST['order_num']) ? intval(trim($_POST['order_num'])) : 1;
			$handel	= isset($_POST['handel']) ? trim($_POST['handel']) : '';
			
			if(empty($name) || empty($url) || !$handel){
				_message("插入失败");
			}
			
			$file_path = '';
			if(isset($_FILES['image'])){
				System::load_sys_class('upload','sys','no');
				upload::upload_config(array('png','jpg','jpeg','gif'),500000,'appimg');
				upload::go_upload($_FILES['image']);
				if(!upload::$ok){
					_message(upload::$error,WEB_PATH.'/'.ROUTE_M.'/'.ROUTE_C."/add_data");
				}
				$file_path = "appimg/".upload::$filedir."/".upload::$filename;
			}

			$sql = "INSERT INTO `@#_appimg`(name,url,img_path,is_show,order_num,handel) VALUES('$name','$url','$file_path','$is_show','$order_num','$handel')";
			$this->db->Query($sql);
			if($this->db->affected_rows()){
					_message("插入成功",WEB_PATH.'/'.ROUTE_M.'/'.ROUTE_C."/lists");
			}else{
					_message("插入失败");
			}
			
		}
		include $this->tpl(ROUTE_M,'add_data');
	}
	

	//修改数据
	public function update_data(){
		$id	= intval($this->segment(4));
		$onedate	= $this->db->GetOne("SELECT * FROM `@#_appimg` WHERE `id`='$id'");
		if(!$onedate) _message("参数不正确");
	
		if(isset($_POST['submit'])){
			$name 		= isset($_POST['name']) ? htmlspecialchars(trim($_POST['name'])) : '';
			$url  		= isset($_POST['url']) ? htmlspecialchars(trim($_POST['url'])) : '';
			$is_show	= isset($_POST['is_show']) ? intval(trim($_POST['is_show'])) : 1;
			$order_num	= isset($_POST['order_num']) ? intval(trim($_POST['order_num'])) : 1;
			$handel	= isset($_POST['handel']) ? trim($_POST['handel']) : '';
			
			if(!$name || !$handel || !$url){
				_message("插入失败");
			} 
			
			$old_imgpath = isset($_POST['old_imgpath']) ? htmlspecialchars(trim($_POST['old_imgpath'])): '';
			$file_path = '';
			
			if(isset($_FILES['image']['name']) && $_FILES['image']['name'] != ''){
				System::load_sys_class('upload','sys','no');
				upload::upload_config(array('png','jpg','jpeg','gif'),500000,'appimg');
				upload::go_upload($_FILES['image']);
				if(!upload::$ok){
					_message(upload::$error,WEB_PATH.'/'.ROUTE_M.'/'.ROUTE_C."/update_data");
				}
				$file_path = "appimg/".upload::$filedir."/".upload::$filename;
			}else{
				$file_path = $old_imgpath;
			}
			$this->db->Query("UPDATE `@#_appimg` SET `name`='$name',`order_num`='$order_num',`url`='$url',`is_show`='$is_show',`img_path`='$file_path',handel='$handel' WHERE `id`='$id'");
			if($this->db->affected_rows()){
				_message("修改成功",WEB_PATH.'/'.ROUTE_M.'/'.ROUTE_C."/lists");
			}else{
				_message("修改失败");
			}
		}
		
		include $this->tpl(ROUTE_M,'update_data');
	}
	//删除广告位
	public function del_data(){
		$delid = intval($this->segment(4));
		if($delid == ''){
			_message("该条数据已经不存在！");
		}
		
		$this->db->Query("DELETE FROM `@#_appimg` WHERE `id`='$delid'");
		if($this->db->affected_rows()){
			//$this->del_img($delid);
			
			_message("删除成功",WEB_PATH.'/'.ROUTE_M.'/'.ROUTE_C."/lists");
			
		}else{
			_message("删除失败");
		}
	}
	
	
	//删除图片
	private function del_img($id){
		if($id != ''){
			$sql = "SELECT img_path FROM `@#_appimg` WHERE `id`='$id'";
			$one = $this->db->GetOne($sql);
           		
			$file_path = '/statics/uploads/appimg/'.$one['img_path'];
			unlink($file_path);
		}
	}
	private function upload_file($file_name){

		upload::$error;			//返回上传错误消息
		upload::$ok;				//返回上传状态 true | false
		upload::$filedir;		//上传文件所在目录名
		upload::$filename;		//上传文件名
		
		$img_path= '';
		//引入上传类		
		System::load_sys_class('upload','sys','no');
		
		//配置上传参数
		upload::upload_config(array('png','jpg','jpeg','gif'),500000,'appimg');			
		
		//开始上传
		upload::go_upload($file_name);
		
		if(!upload::$ok){
			_message(upload::$error,WEB_PATH.'/'.ROUTE_M.'/'.ROUTE_C."/lists");				
		}else{
			$img_path = upload::$filedir."/".upload::$filename;
		}
	    return $img_path;
	}
}











?>