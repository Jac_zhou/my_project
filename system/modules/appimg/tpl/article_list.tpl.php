<?php defined('G_IN_ADMIN')or exit('No permission resources.'); ?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<link rel="stylesheet" href="<?php echo G_GLOBAL_STYLE; ?>/global/css/global.css" type="text/css">
<link rel="stylesheet" href="<?php echo G_GLOBAL_STYLE; ?>/global/css/style.css" type="text/css">
<style>
tbody tr{ line-height:30px; height:30px;} 
</style>
</head>
<body>
<div class="header lr10">
	<?php echo $this->headerment();?>
</div>
<div class="bk10"></div>
<div class="table-list lr10">
<!--start-->
  <table width="100%" cellspacing="0">
    <thead>
		<tr>
		<th width="80px">id</th>
		<th width="200px" align="center">文章标题</th>
		
		<th <?php if($rel_type !=1){echo "style='display:none'";}?> width="" align="center">文章图片</th>
		<th width="" align="center">内容详情</th>
		<th width="" align="center">文章排序</th>
		<th width="" align="center">是否显示</th>
		<th width="30%" align="center">操作</th>
		</tr>
    </thead>
    <tbody>
		<?php foreach($arr as $v){ ?>
		<tr>
			<td align="center"><?php echo $v['id']; ?></td>
			<td align="center"><?php echo _strcut($v['title'],20);?></td>
			<td align="center" <?php if($rel_type !=1){echo "style='display:none'";}?>><?php echo $v['img_path'];?></td>
			<td align="center"><?php echo _strcut($v['content'],20)?></td>
			<td align="center"><?php echo $v['order_num'];?></td>
			<td align="center"><?php if($v['is_show']==1){echo "是";}else{echo "否";}?></td>
			<td align="center">
				<a href="<?php echo WEB_PATH; ?>/appimg/apparticle_admin/update_article/<?php echo $v['id'];?>/<?php echo $v['type'];?>">修改</a>
                <span class='span_fenge lr5'>|</span>
				<a href="<?php echo WEB_PATH; ?>/appimg/apparticle_admin/del_article/<?php echo $v['id']; ?>/<?php echo $v['type'];?>" onClick="return confirm('是否真的删除！');">删除</a>
			</td>	
		</tr>
		<?php } ?>
  	</tbody>
</table>
</div><!--table-list end-->

<script>
</script>
</body>
</html> 