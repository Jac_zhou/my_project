<?php defined('G_IN_ADMIN')or exit('No permission resources.'); ?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<link rel="stylesheet" href="<?php echo G_GLOBAL_STYLE; ?>/global/css/global.css" type="text/css">
<link rel="stylesheet" href="<?php echo G_GLOBAL_STYLE; ?>/global/css/style.css" type="text/css">

<script src="<?php echo G_GLOBAL_STYLE; ?>/global/js/jquery-1.8.3.min.js"></script>
<script src="<?php echo G_PLUGIN_PATH; ?>/uploadify/api-uploadify.js" type="text/javascript"></script> 
<script type="text/javascript">
var editurl=Array();
editurl['editurl']='<?php echo G_PLUGIN_PATH; ?>/ueditor/';
editurl['imageupurl']='<?php echo G_ADMIN_PATH; ?>/ueditor/upimage/';
editurl['imageManager']='<?php echo G_ADMIN_PATH; ?>/ueditor/imagemanager';
</script>
<script type="text/javascript" charset="utf-8" src="<?php echo G_PLUGIN_PATH; ?>/ueditor/ueditor.config.js"></script>
<script type="text/javascript" charset="utf-8" src="<?php echo G_PLUGIN_PATH; ?>/ueditor/ueditor.all.min.js"></script>

<style>
table th{ border-bottom:1px solid #eee; font-size:12px; font-weight:100; text-align:right; width:200px;}
table td{ padding-left:10px;}
input.button{ display:inline-block}
</style>
</head>
<body>
<div class="header lr10">
	<?php echo $this->headerment();?>
</div>
<div class="bk10"></div>
<div class="table_form lr10">
<!--start-->
<form name="myform" action="" method="post" enctype="multipart/form-data">
<table width="100%" class="lr10">
  	<tr>
		<td width="120" align="right">文章标题：</td>
		<td><input type="text" name="title" class="input-text"><font color="red">*必选*</font></td>
	</tr>
	<tr>
		<td align="right">摘要：</td>
		<td><textarea name="description" class="wid400"  style="height:60px"></textarea>
		</td>
	</tr>	
	<tr <?php if($rel_type !=1){echo "style='display:none'";}?>>
		<td width="120" align="right">文章图片:</td>
		<td>
		<input type="text" id="imagetext"  class="input-text wid300">
		<input type="button" class="button" onClick="upImage()" value="选择图片"/>          
		<input type="file" id="imgfield" name="image" class="button" 
		onchange="document.getElementById('imagetext').value=this.value"
		style=" display:none;" />
		</td>
	</tr <?php if($rel_type !=1){echo "style='display:none'";}?>>
	<tr>
		<td height="300"  align="right"><font color="red">*</font>内容详情：</td>
		<td><script name="content" id="myeditor" type="text/plain"></script>
			 <style>
			.content_attr {
				border: 1px solid #CCC;
				padding: 5px 8px;
				background: #FFC;
				margin-top: 6px;
				width:915px;
			}
			</style>
			 <!--<div class="content_attr">
			<label><input name="sub_text_des" type="checkbox"  value="off" checked />是否截取内容</label>
			<input type="text" name="sub_text_len" class="input-text" value="250" size="3">字符至内容摘要<label>         
			</div>-->
		</td>  
	</tr>
	<tr>
		<td width="120" align="right">是否显示：</td>
		<td>
			<input type="radio" name="is_show" checked class="input-text" value="1">是
			<input type="radio" name="is_show" class="input-text" value="0">否
		</td>
	</tr>
	<tr>
		<td width="120" align="right">是否最新：</td>
		<td>
			<input type="radio" name="is_new" class="input-text" value="1">是
			<input type="radio" name="is_new" checked class="input-text" value="0">否
		</td>
	</tr>
	<tr>
		<td width="120" align="right">文章排序：</td>
		<td><input type="text" name="order_num" class="input-text"><font color="red">排序越大越靠前</font></td>
   </tr>
	<tr>
		<td width="120" align="right"></td>
		<td><input type="submit" class="button" name="submit"  value=" 提交 " ></td>
	</tr>
</table>
</form>
</div><!--table-list end-->
<script>
//实例化编辑器
var ue = UE.getEditor('myeditor');
ue.addListener('ready',function(){
	this.focus()
});

var info=new Array();
function gbcount(message,maxlen,id){
	
	if(!info[id]){
		info[id]=document.getElementById(id);
	}			
	var lenE = message.value.length;
	var lenC = 0;
	var enter = message.value.match(/\r/g);
	var CJK = message.value.match(/[^\x00-\xff]/g);//计算中文
	if (CJK != null) lenC += CJK.length;
	if (enter != null) lenC -= enter.length;		
	var lenZ=lenE+lenC;		
	if(lenZ > maxlen){
		info[id].innerHTML=''+0+'';
		return false;
	}
	info[id].innerHTML=''+(maxlen-lenZ)+'';
}
	
function upImage(){
	return document.getElementById('imgfield').click();
}
</script>
</body>
</html> 