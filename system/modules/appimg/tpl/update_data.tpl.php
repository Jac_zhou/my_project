<?php defined('G_IN_ADMIN')or exit('No permission resources.'); ?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<link rel="stylesheet" href="<?php echo G_GLOBAL_STYLE; ?>/global/css/global.css" type="text/css">
<link rel="stylesheet" href="<?php echo G_GLOBAL_STYLE; ?>/global/css/style.css" type="text/css">

<style>
table th{ border-bottom:1px solid #eee; font-size:12px; font-weight:100; text-align:right; width:200px;}
table td{ padding-left:10px;}
input.button{ display:inline-block}
</style>
</head>
<body>
<div class="header lr10">
	<?php echo $this->headerment();?>
</div>
<div class="bk10"></div>
<div class="table_form lr10">
<!--start-->
<form name="myform" action="" method="post" enctype="multipart/form-data">
<table width="100%" class="lr10">
  	 <tr>
			<td width="120" align="right">图标名称：</td>
			<td><input type="text" name="name" class="input-text" value="<?php echo $onedate['name']?>"><font color="red">*必选*</font></td>
	</tr>
		
       <tr>
			<td width="120" align="right">类型：</td>
			<td><input type="text" name="url"  class="input-text" value="<?php echo $onedate['url']?>"><font color="red">*必选*</font></td>
		</tr>
		<tr>
			<td width="120" align="right">操作：</td>
			<td><input type="text" name="handel"  class="input-text" value="<?php echo $onedate['handel']?>"><font color="red">*必选*</font></td>
		</tr>
		<tr>
        	<td width="120" align="right">链接图片:</td>
            <td>
           	<input type="text" id="imagetext"  class="input-text wid300" value="<?php echo $onedate['img_path']?>">
            <input type="button" class="button" onClick="upImage()" value="选择图片"/>          
			<input type="file" id="imgfield" name="image" class="button" 
            onchange="document.getElementById('imagetext').value=this.value"
            style=" display:none;" /> <font color="red">*必选*</font>
            </td>
        </tr>
		<tr>
			<td width="120" align="right">是否开启：</td>
			<td>
				<input type="radio" name="is_show" class="input-text" value="1" <?php if($onedate['is_show'] == 1){echo "checked";}?>>开启
				<input type="radio" name="is_show" class="input-text" value="0" <?php if($onedate['is_show'] == 0){echo "checked";}?>>关闭
			</td>
		</tr>
		 <tr>
			<td width="120" align="right">图标排序：</td>
			<td><input type="text" name="order_num" class="input-text" value="<?php echo $onedate['order_num']?>"><font color="red">排序越大越靠前</font></td>
	   </tr>
		<input type="hidden" name="old_imgpath" value="<?php echo $onedate['img_path']?>"/>
		<tr>
        	<td width="120" align="right"></td>
            <td><input type="submit" class="button" name="submit"  value=" 提交 " ></td>
		</tr>
</table>
</form>
</div><!--table-list end-->
<script>
function upImage(){
	return document.getElementById('imgfield').click();
}
</script>
</body>
</html> 