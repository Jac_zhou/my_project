<?php

defined('G_IN_SYSTEM')or exit('No permission resources.');
System::load_app_class('admin',G_ADMIN_DIR,'no');
class apparticle_admin extends admin {
	private $db;
	private $type;
	public function __construct(){
		parent::__construct();
		$this->db=System::load_sys_class("model");
		
		$this->type = $this->segment(5) ? intval($this->segment(5)) : intval($this->segment(4));

		$this->ment=array(
						array("lists","文章管理",ROUTE_M.'/'.ROUTE_C."/article_lists/".$this->type),
						array("appadd","文章添加",ROUTE_M.'/'.ROUTE_C."/add_article/".$this->type),
		);
		
	}
	public function article_lists(){
		$rel_type = $this->type;
		$sql = "SELECT * FROM `@#_app_article` WHERE type= ".$this->type." ORDER BY order_num DESC";
		$arr=$this->db->Getlist($sql);
		
		include $this->tpl(ROUTE_M,'article_list');
	}
	
	//添加数据
	public function add_article(){
		$rel_type = $this->type;
		if(isset($_POST['submit'])){
			$title 		= isset($_POST['title']) ? htmlspecialchars(trim($_POST['title'])) : '';
			$is_show	= isset($_POST['is_show']) ? intval(trim($_POST['is_show'])) :  1;
			$order_num	= isset($_POST['order_num']) ? intval(trim($_POST['order_num'])) : 1;
			$content = isset($_POST['content'])?editor_safe_replace(stripslashes($_POST['content'])):'';
			$description = isset($_POST['description'])?editor_safe_replace(stripslashes($_POST['description'])):'';
			$is_new	= isset($_POST['is_new']) ? trim($_POST['is_new']) :  0;
			$time = time();
			
			if(empty($title)){
				_message("插入失败");
			}
			
			$file_path = '';
			if(isset($_FILES['image'])){
				System::load_sys_class('upload','sys','no');
				upload::upload_config(array('png','jpg','jpeg','gif'),500000,'appimg');
				upload::go_upload($_FILES['image']);
				
				$file_path = "appimg/".upload::$filedir."/".upload::$filename;
			}
			
			$sql = "INSERT INTO `@#_app_article`(title,type,img_path,is_show,order_num,content,is_new,time,description) VALUES('$title','$rel_type','$file_path','$is_show','$order_num','$content','$is_new','$time','$description')";
			$this->db->Query($sql);
			if($this->db->affected_rows()){
					_message("插入成功",WEB_PATH.'/'.ROUTE_M.'/'.ROUTE_C."/article_lists"."/".$rel_type);
			}else{
					_message("插入失败");
			}
			
		}
		include $this->tpl(ROUTE_M,'add_article');
	}
	

	//修改数据
	public function update_article(){
		$rel_type = $this->type;
		$delid = intval($this->segment(4));
		
		$onedate	= $this->db->GetOne("SELECT * FROM `@#_app_article` WHERE `id`='$delid'");
		if(!$onedate) _message("参数不正确");
	
		if(isset($_POST['submit'])){
			$title 		= isset($_POST['title']) ? htmlspecialchars(trim($_POST['title'])) : '';
			$is_show	= isset($_POST['is_show']) ? intval(trim($_POST['is_show'])) : 1;
			$order_num	= isset($_POST['order_num']) ? intval(trim($_POST['order_num'])) : 1;
			$content = isset($_POST['content'])?editor_safe_replace(stripslashes($_POST['content'])):'';
			$description = isset($_POST['description'])?editor_safe_replace(stripslashes($_POST['description'])):'';
			$is_new	= isset($_POST['is_new']) ? trim($_POST['is_new']) :  0;
			
			$old_imgpath = isset($_POST['old_imgpath']) ? htmlspecialchars(trim($_POST['old_imgpath'])): '';
			$file_path = '';
			
			if(isset($_FILES['image']['name']) && $_FILES['image']['name'] != ''){
				System::load_sys_class('upload','sys','no');
				upload::upload_config(array('png','jpg','jpeg','gif'),500000,'appimg');
				upload::go_upload($_FILES['image']);
				
				$file_path = "appimg/".upload::$filedir."/".upload::$filename;
			}else{
				$file_path = $old_imgpath;
			}
			$this->db->Query("UPDATE `@#_app_article` SET `title`='$title',`order_num`='$order_num',`content`='$content',`is_show`='$is_show',`img_path`='$file_path',`is_new`='$is_new',`description`='$description' WHERE `id`='$delid'");
			if($this->db->affected_rows()){
				_message("修改成功",WEB_PATH.'/'.ROUTE_M.'/'.ROUTE_C."/article_lists"."/".$rel_type);
			}else{
				_message("修改失败");
			}
		}
		
		include $this->tpl(ROUTE_M,'update_article');
	}
	//删除广告位
	public function del_article(){
		$delid = intval($this->segment(4));
		$rel_type = $this->type;
		if($delid == ''){
			_message("该条数据已经不存在！");
		}
		
		$this->db->Query("DELETE FROM `@#_app_article` WHERE `id`='$delid'");
		if($this->db->affected_rows()){
			//$this->del_img($delid);
			
			_message("删除成功",WEB_PATH.'/'.ROUTE_M.'/'.ROUTE_C."/article_lists/".$rel_type);
			
		}else{
			_message("删除失败");
		}
	}
	
	
	//删除图片
	private function del_img($id){
		if($id != ''){
			$sql = "SELECT img_path FROM `@#_appimg` WHERE `id`='$id'";
			$one = $this->db->GetOne($sql);
           		
			$file_path = '/statics/uploads/appimg/'.$one['img_path'];
			unlink($file_path);
		}
	}
	private function upload_file($file_name){

		upload::$error;			//返回上传错误消息
		upload::$ok;				//返回上传状态 true | false
		upload::$filedir;		//上传文件所在目录名
		upload::$filename;		//上传文件名
		
		$img_path= '';
		//引入上传类		
		System::load_sys_class('upload','sys','no');
		
		//配置上传参数
		upload::upload_config(array('png','jpg','jpeg','gif'),500000,'appimg');			
		
		//开始上传
		upload::go_upload($file_name);
		
		if(!upload::$ok){
			_message(upload::$error,WEB_PATH.'/'.ROUTE_M.'/'.ROUTE_C."/lists");				
		}else{
			$img_path = upload::$filedir."/".upload::$filename;
		}
	    return $img_path;
	}
}











?>